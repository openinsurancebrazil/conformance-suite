package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/diverseRisks/GetPatrimonialDiverseRisksQuoteStatusOkV1.9.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n9());
    }
}