package net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetCustomersBusinessIdentificationListOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {
	@Test
	@UseResurce("opinResponses/customers/v1/businessIdentification/legalEntityIdentificationResponse.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetCustomersBusinessIdentificationListOASValidatorV1n5());
	}
}
