package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToWrongType;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToWrongTypeDiff;
import org.junit.Test;

import static org.junit.Assert.*;

public class SetClaimNotificationIdFieldToWrongTypeTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    public void testPolicyNumberToGroupCertificateId() {
        String policyId = "random_policy_id";
        environment.putString("policyId", policyId);
        environment.putString("resource_request_entity", "data.policyNumber", policyId);
        environment.putString("resource_request_entity", "data.documentType", "APOLICE_INDIVIDUAL");
        SetClaimNotificationIdFieldToWrongType cond = new SetClaimNotificationIdFieldToWrongType();

        run(cond);

        assertNull(environment.getString("resource_request_entity", "data.policyNumber"));
        assertEquals(policyId, environment.getString("resource_request_entity", "data.groupCertificateId"));
    }

    @Test
    public void testGroupCertificateIdToPolicyNumber() {
        String policyId = "random_policy_id";
        environment.putString("policyId", policyId);
        environment.putString("resource_request_entity", "data.groupCertificateId", policyId);
        environment.putString("resource_request_entity", "data.documentType", "CERTIFICADO");
        SetClaimNotificationIdFieldToWrongType cond = new SetClaimNotificationIdFieldToWrongType();

        run(cond);

        assertNull(environment.getString("resource_request_entity", "data.groupCertificateId"));
        assertEquals(policyId, environment.getString("resource_request_entity", "data.policyNumber"));
    }

    @Test
    public void testNoDocumentType() {
        environment.putString("policyId", "random_policy_id");
        environment.putString("resource_request_entity", "data.groupCertificateId", "random_policy_id");
        SetClaimNotificationIdFieldToWrongType cond = new SetClaimNotificationIdFieldToWrongType();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("documentType field not present"));
    }

    @Test
    public void testInvalidDocumentType() {
        environment.putString("policyId", "random_policy_id");
        environment.putString("resource_request_entity", "data.groupCertificateId", "random_policy_id");
        environment.putString("resource_request_entity", "data.documentType", "RANDOM");
        SetClaimNotificationIdFieldToWrongType cond = new SetClaimNotificationIdFieldToWrongType();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Invalid documentType"));
    }

    @Test
    public void testDiffPolicyIdFieldName() {
        String policyId = "random_policy_id";
        String documentType = "APOLICE_INDIVIDUAL_AUTOMOVEL";
        environment.putString("policyId", policyId);
        environment.putString("resource_request_entity", "data.policyId", policyId);
        environment.putString("resource_request_entity", "data.documentType", "APOLICE_INDIVIDUAL_AUTOMOVEL");
        SetClaimNotificationIdFieldToWrongTypeDiff cond = new SetClaimNotificationIdFieldToWrongTypeDiff();

        run(cond);
        assertNull(policyId, environment.getString("resource_request_entity", "data.policyId"));
        assertEquals(documentType, environment.getString("resource_request_entity", "data.documentType"));
    }
}
