package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n3.GetInsuranceResponsibilityPolicyInfoOASValidatorV1n3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceResponsibilityPolicyInfoOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceResponsibility/v1n4/OpinInsuranceResponsibilityPolicyInfoValidatorV1n4OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceResponsibilityPolicyInfoOASValidatorV1n4());
	}

}
