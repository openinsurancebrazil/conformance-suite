package net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostEndorsementOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/endorsement/PostEndorsementResponse.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostEndorsementOASValidatorV1());
    }
}