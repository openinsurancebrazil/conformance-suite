package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceResponsibilityPolicyInfoOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceResponsibility/OpinInsuranceResponsibilityPolicyInfoValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceResponsibilityPolicyInfoOASValidatorV1n3());
	}

}
