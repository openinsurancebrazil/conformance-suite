package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PostClaimNotificationPersonOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        setStatus(201);
    }

    @Test
    @UseResurce("opinResponses/claimNotification/PostClaimNotificationPersonResponse.json")
    public void testHappyPath() {
        run(new PostClaimNotificationPersonOASValidatorV1());
    }

    @Test
    @UseResurce("opinResponses/claimNotification/PostClaimNotificationPersonResponseNoRequestor.json")
    public void testNoRequestor() {
        ConditionError conditionError = runAndFail(new PostClaimNotificationPersonOASValidatorV1());
        assertTrue(conditionError.getMessage().contains("requestor is required when requestorIsInsured is false"));
    }
}