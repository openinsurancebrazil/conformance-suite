package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostClaimNotificationDamageOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/claimNotification/v1n4/PostClaimNotificationDamageResponseV1.4.0.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostClaimNotificationDamageOASValidatorV1n4());
    }
}
