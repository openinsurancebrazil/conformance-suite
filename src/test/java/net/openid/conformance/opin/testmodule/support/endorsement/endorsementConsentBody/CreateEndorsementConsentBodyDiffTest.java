package net.openid.conformance.opin.testmodule.support.endorsement.endorsementConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.*;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.EndorsementType;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateEndorsementConsentBodyDiffTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("policyId", "policy_id");
        JsonObject consentRequest = new JsonObject();
        consentRequest.add("data", new JsonObject());
        environment.putObject("consent_endpoint_request", consentRequest);
    }

    @Test
    public void testEndorsementAlteracao() {
        CreateEndorsementConsentBodyAlteracaoDiff cond = new CreateEndorsementConsentBodyAlteracaoDiff();

        run(cond);

        assertEquals("policy_id", environment.getString("consent_endpoint_request", "data.endorsementInformation.policyId"));
        assertEquals(EndorsementType.ALTERACAO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNotNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.requestDescription"));
        assertNotNull(environment.getElementFromObject("consent_endpoint_request","data.endorsementInformation.insuredObjectId"));
    }

    @Test
    public void testEndorsementCancelamento() {
        CreateEndorsementConsentBodyCancelamentoDiff cond = new CreateEndorsementConsentBodyCancelamentoDiff();

        run(cond);

        assertEquals("policy_id", environment.getString("consent_endpoint_request", "data.endorsementInformation.policyId"));
        assertEquals("216731531723",environment.getElementFromObject("consent_endpoint_request", "data.endorsementInformation.insuredObjectId").getAsString());
        assertEquals(EndorsementType.CANCELAMENTO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNotNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.requestDescription"));
        assertNotNull(environment.getElementFromObject("consent_endpoint_request","data.endorsementInformation.insuredObjectId"));
    }

    @Test
    public void testEndorsementInclusao() {
        CreateEndorsementConsentBodyInclusaoDiff cond = new CreateEndorsementConsentBodyInclusaoDiff();

        run(cond);

        assertEquals("policy_id", environment.getString("consent_endpoint_request", "data.endorsementInformation.policyId"));
        assertEquals(EndorsementType.INCLUSAO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNotNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.requestDescription"));
        assertNotNull(environment.getElementFromObject("consent_endpoint_request","data.endorsementInformation.insuredObjectId"));
    }

    @Test
    public void testEndorsementExclusao() {
        CreateEndorsementConsentBodyExclusaoDiff cond = new CreateEndorsementConsentBodyExclusaoDiff();

        run(cond);

        assertEquals("policy_id", environment.getString("consent_endpoint_request", "data.endorsementInformation.policyId"));
        assertEquals(EndorsementType.EXCLUSAO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNotNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.requestDescription"));
        assertNotNull(environment.getElementFromObject("consent_endpoint_request","data.endorsementInformation.insuredObjectId"));
    }
}
