package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceTransportClaimOASValidatorV1n2Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceTransport/OpinInsuranceTransportClaimValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceTransportClaimOASValidatorV1n2());
	}


}
