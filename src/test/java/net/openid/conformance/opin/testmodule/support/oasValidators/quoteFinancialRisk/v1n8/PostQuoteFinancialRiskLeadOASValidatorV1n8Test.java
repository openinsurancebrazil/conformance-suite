package net.openid.conformance.opin.testmodule.support.oasValidators.quoteFinancialRisk.v1n8;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostQuoteFinancialRiskLeadOASValidatorV1n8Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPostResponseOK.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostQuoteFinancialRiskLeadOASValidatorV1n8());
    }
}