package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.lead;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostPersonLeadOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/PostQuotePersonLeadOkV1.10.2.json")
    public void happyPathTest() {
        setStatus(201);
        run(new PostPersonLeadOASValidatorV1n10());
    }
}