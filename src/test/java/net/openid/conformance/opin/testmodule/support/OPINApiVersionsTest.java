package net.openid.conformance.opin.testmodule.support;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.mongodb.assertions.Assertions.assertNull;
import static org.junit.Assert.assertEquals;


public class OPINApiVersionsTest {

    @Test
    public void testApiVersions() {
        OPINApiVersions versions = new OPINApiVersions();

        Map<String, String> testCases = new HashMap<>();
        testCases.put("products-services", "v1");
        testCases.put("insurance-acceptance-and-branches-abroad", "v1");
        testCases.put("insurance-auto", "v1");
        testCases.put("insurance-financial-risk", "v1");
        testCases.put("insurance-housing", "v1");
        testCases.put("insurance-patrimonial", "v1");
        testCases.put("insurance-responsibility", "v1");
        testCases.put("insurance-rural", "v1");
        testCases.put("insurance-transport", "v1");
        testCases.put("resources", "v2");
        testCases.put("consents", "v2");
        testCases.put("customers", "v1");
        testCases.put("insurance-customer-business", "v1");
        testCases.put("insurance-customer-personal", "v1");
        testCases.put("insurance-capitalization-title", "v1");
        testCases.put("insurance-person", "v1");
        testCases.put("insurance-financial-assistance", "v1");
        testCases.put("insurance-life-pension", "v1");
        testCases.put("insurance-pension-plan", "v1");
        testCases.put("claim-notification", "v1");
        testCases.put("endorsement", "v1");
        testCases.put("quote-patrimonial", "v1");
        testCases.put("non-existent-key", null);

        for (Map.Entry<String, String> entry : testCases.entrySet()) {
            String apiVersion = versions.getApiVersion(entry.getKey());

            if (entry.getValue() == null) {
                assertNull(apiVersion);
            } else {
                assertEquals(entry.getValue(), apiVersion);
            }
        }
    }
}
