package net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetCustomersBusinessQualificationListOASValidatorV1n6Test extends AbstractJsonResponseConditionUnitTest {
	@Test
	@UseResurce("opinResponses/customers/v1/businessQualification/legalEntityQualificationResponse.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetCustomersBusinessQualificationListOASValidatorV1n6());
	}
}
