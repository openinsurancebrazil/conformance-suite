package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExtractModalityTypeTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/dynamicFields/GetDynamicFieldsCapitalizationTitleOkResponseV1.3.0.json")
    public void testHappyPath() {
        ExtractModalityType cond = new ExtractModalityType();
        run(cond);
        assertEquals("TRADICIONAL", environment.getString("modality"));
    }
}
