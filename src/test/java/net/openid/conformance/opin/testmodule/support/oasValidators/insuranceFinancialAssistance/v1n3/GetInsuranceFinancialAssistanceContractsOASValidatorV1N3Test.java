package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceFinancialAssistanceContractsOASValidatorV1N3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceFinancialAssistance/OpinInsuranceFinancialAssistanceContractsValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceFinancialAssistanceContractsOASValidatorV1n3());
	}
}
