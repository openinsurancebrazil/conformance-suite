package net.openid.conformance.opin.testmodule.support.oasValidators.quoteResponsibility.v1n9;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteResponsibility.v1n8.PatchQuoteResponsibilityLeadOASValidatorV1n8;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchQuoteResponsibilityLeadOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchLeadResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchQuoteResponsibilityLeadOASValidatorV1n9());
    }
}