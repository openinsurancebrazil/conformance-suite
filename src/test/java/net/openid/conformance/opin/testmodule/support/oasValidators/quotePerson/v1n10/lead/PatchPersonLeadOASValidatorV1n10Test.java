package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.lead;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchPersonLeadOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/PatchQuotePersonLeadOkV1.10.2.json")
    public void happyPathTest() {
        setStatus(200);
        run(new PatchPersonLeadOASValidatorV1n10());
    }
}