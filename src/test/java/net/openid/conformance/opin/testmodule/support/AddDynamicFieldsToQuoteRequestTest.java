package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AddDynamicFieldsToQuoteRequestTest extends AbstractJsonResponseConditionUnitTest {

    private JsonObject data;

    @Before
    public void setUp() {
        this.data = new JsonObject();
        JsonObject requestData = new JsonObject();
        requestData.add("data", this.data);
        environment.putObject("resource_request_entity", requestData);
    }

    @Test
    public void testHappyPath() {
        setUpDynamicFields();
        AddDynamicFieldsToQuoteRequest cond = new AddDynamicFieldsToQuoteRequest();
        run(cond);

        JsonArray generalInfo = data.getAsJsonObject("quoteCustomData").getAsJsonObject().getAsJsonArray("generalQuoteInfo");
        for(JsonElement el : generalInfo) {
            JsonObject obj = el.getAsJsonObject();
            switch (obj.get("fieldId").getAsString()) {
                case "boolean_id":
                    assertFalse(obj.get("value").getAsBoolean());
                    break;
                case "string_id":
                    assertEquals("", obj.get("value").getAsString());
                    break;
                case "integer_id":
                    assertEquals(0, obj.get("value").getAsInt());
                    break;
                case "number_id":
                    assertEquals(0.0, obj.get("value").getAsNumber());
                    break;
                case "array_id":
                    assertEquals(0, obj.get("value").getAsJsonArray().size());
                    break;
                default:
            }
        }
        assertEquals(5, data.getAsJsonObject("quoteCustomData").getAsJsonObject().getAsJsonArray("generalQuoteInfo").size());
        assertNotNull(environment.getString("resource_request_entity"));
    }


    @Test
    public void testAddModalityToPostRequest() {
        setUpDynamicFields();
        environment.putString("modality", "POPULAR");
        AddModalityToQuoteRequest cond = new AddModalityToQuoteRequest();
        run(cond);
        JsonObject requestBody = environment.getObject("resource_request_entity");
        assertEquals("POPULAR", requestBody.get("data").getAsJsonObject().get("modality").getAsString());
    }

    protected void setUpDynamicFields() {
        JsonObject booleanField = new JsonObject();
        booleanField.addProperty("fieldId", "boolean_id");
        booleanField.addProperty("type", "BOOLEAN");

        JsonObject stringField = new JsonObject();
        stringField.addProperty("fieldId", "string_id");
        stringField.addProperty("type", "STRING");

        JsonObject numberField = new JsonObject();
        numberField.addProperty("fieldId", "number_id");
        numberField.addProperty("type", "NUMBER");

        JsonObject integerField = new JsonObject();
        integerField.addProperty("fieldId", "integer_id");
        integerField.addProperty("type", "INTEGER");

        JsonObject arrayField = new JsonObject();
        arrayField.addProperty("fieldId", "array_id");
        arrayField.addProperty("type", "ARRAY");

        JsonArray dynamicFields = new JsonArray();
        dynamicFields.add(booleanField);
        dynamicFields.add(stringField);
        dynamicFields.add(numberField);
        dynamicFields.add(integerField);
        dynamicFields.add(arrayField);

        JsonObject dynamicField = new JsonObject();
        dynamicField.add("dynamic_fields", dynamicFields);
        environment.putObject("dynamic_field", dynamicField);
    }
}
