package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteLeadPatchRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchAcknRequestBody;
import net.openid.conformance.util.JsonUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateQuotePatchRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    public void testPersonalCreateQuoteLeadPatchRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteLeadPatchRequestBody cond = new CreateQuoteLeadPatchRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals("CPF", requestBody.getAsJsonObject("data").getAsJsonObject("author").get("identificationType").getAsString());
    }

    @Test
    public void testBusinessCreateQuoteLeadPatchRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        CreateQuoteLeadPatchRequestBody cond = new CreateQuoteLeadPatchRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals("CNPJ", requestBody.getAsJsonObject("data").getAsJsonObject("author").get("identificationType").getAsString());
    }

    @Test
    public void testCreateQuotePatchAcknRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        environment.putString("quote_id", "random_quote_id");
        CreateQuotePatchAcknRequestBody cond = new CreateQuotePatchAcknRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject data = requestBody.getAsJsonObject("data");
        assertEquals("CPF", data.getAsJsonObject("author").get("identificationType").getAsString());
        assertEquals("ACKN", data.get("status").getAsString());
        assertEquals("random_quote_id", data.get("insurerQuoteId").getAsString());
    }

}
