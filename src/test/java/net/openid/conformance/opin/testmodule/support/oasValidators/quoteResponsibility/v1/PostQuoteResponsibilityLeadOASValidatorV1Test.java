package net.openid.conformance.opin.testmodule.support.oasValidators.quoteResponsibility.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostQuoteResponsibilityLeadOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPostResponseOK.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostQuoteResponsibilityLeadOASValidatorV1());
    }
}