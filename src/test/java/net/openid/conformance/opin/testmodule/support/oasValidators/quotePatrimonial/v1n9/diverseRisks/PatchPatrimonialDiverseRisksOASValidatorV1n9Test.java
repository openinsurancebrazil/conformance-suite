package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchPatrimonialDiverseRisksOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchPatrimonialDiverseRisksOASValidatorV1n9());
    }
}