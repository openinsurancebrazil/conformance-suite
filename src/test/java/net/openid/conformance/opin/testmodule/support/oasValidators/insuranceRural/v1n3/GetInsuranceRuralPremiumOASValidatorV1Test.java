package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceRuralPremiumOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/rural/premium/premiumResponseStructure.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceRuralPremiumOASValidatorV1n3());
	}

}