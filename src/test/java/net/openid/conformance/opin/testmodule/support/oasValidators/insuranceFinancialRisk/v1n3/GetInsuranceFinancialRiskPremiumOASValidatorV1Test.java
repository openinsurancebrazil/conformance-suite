package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceFinancialRiskPremiumOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/financialRisk/OpinInsuranceFinancialRiskPremiumValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceFinancialRiskPremiumOASValidatorV1n3());
	}

}