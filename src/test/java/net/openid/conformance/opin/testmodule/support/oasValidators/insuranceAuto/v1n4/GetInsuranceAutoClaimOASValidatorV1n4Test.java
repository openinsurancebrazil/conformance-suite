package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceAutoClaimOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceAuto/v1n4/OpinInsuranceAutoClaimResponseV1.4.0.OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceAutoClaimOASValidatorV1n4());
	}

}
