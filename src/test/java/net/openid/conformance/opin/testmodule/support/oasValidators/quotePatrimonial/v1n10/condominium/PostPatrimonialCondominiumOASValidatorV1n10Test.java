package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.condominium;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostPatrimonialCondominiumOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPostResponseOK.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostPatrimonialCondominiumOASValidatorV1n10());
    }
}