package net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawal.v1n3;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitleWithdrawal.v1n3.PostCapitalizationTitleWithdrawalOASValidatorV1n3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThrows;

public class PostPensionWithdrawalOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/pensionWithdrawal/PostPensionWithdrawalResponseOkV1n3.json")
    public void testHappyPath() {
        setStatus(201);
        PostPensionWithdrawalOASValidatorV1n3 cond = new PostPensionWithdrawalOASValidatorV1n3();
        run(cond);
    }

    @Test
    @UseResurce("opinResponses/pensionWithdrawal/PostPensionWithdrawalResponseBadOUTROSV1n3.json")
    public void testUnhappyPath() {
        setStatus(201);
        PostPensionWithdrawalOASValidatorV1n3 cond = new PostPensionWithdrawalOASValidatorV1n3();
        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("withdrawalReasonOthers is required when withdrawalReason is 9_OUTROS"));
        JsonObject withdrawalInformation = new JsonObject();
        withdrawalInformation.addProperty("withdrawalReason", "9_OUTROS");
        assertThrows(ConditionError.class, () -> cond.assertWithdrawalInformation(withdrawalInformation));
    }
}
