package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;

public class EnsureStatusWasXTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    public void testEnsureStatusWasRjctHappyPath() {
        setStatus("RJCT");
        EnsureStatusWasRjct cond = new EnsureStatusWasRjct();

        run(cond);
    }

    @Test
    public void testEnsureStatusWasRjctUnhappyPath() {
        setStatus("RCVD");
        EnsureStatusWasRjct cond = new EnsureStatusWasRjct();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("not as expected"));
    }

    @Test
    public void testEnsureStatusWasRcvdOrEvalHappyPath() {
        EnsureStatusWasRcvdOrEval cond = new EnsureStatusWasRcvdOrEval();

        setStatus("RCVD");
        run(cond);
        assertTrue(environment.getBoolean(EnsureStatusWasRcvdOrEval.STATUS_KEY));

        setStatus("EVAL");
        run(cond);
        assertTrue(environment.getBoolean(EnsureStatusWasRcvdOrEval.STATUS_KEY));
    }

    @Test
    public void testEnsureStatusWasRcvdOrEvalUnhappyPath() {
        setStatus("ACNK");
        EnsureStatusWasRcvdOrEval cond = new EnsureStatusWasRcvdOrEval();

        run(cond);

        assertFalse(environment.getBoolean(EnsureStatusWasRcvdOrEval.STATUS_KEY));
    }

    @Test
    public void testEnsureStatusWasAcptHappyPath() {
        setStatus("ACPT");
        EnsureStatusWasAcpt cond = new EnsureStatusWasAcpt();

        run(cond);
    }

    @Test
    public void testEnsureStatusWasAcptUnhappyPath() {
        setStatus("RCVD");
        EnsureStatusWasAcpt cond = new EnsureStatusWasAcpt();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("not as expected"));
    }

    @Test
    public void testEnsureStatusWasAcknHappyPath() {
        setStatus("ACKN");
        EnsureStatusWasAckn cond = new EnsureStatusWasAckn();

        run(cond);
    }

    @Test
    public void testEnsureStatusWasAcknUnhappyPath() {
        setStatus("RCVD");
        EnsureStatusWasAckn cond = new EnsureStatusWasAckn();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("not as expected"));
    }
    @Test
    public void testEnsureStatusWasCancHappyPath() {
        setStatus("CANC");
        EnsureStatusWasCanc cond = new EnsureStatusWasCanc();

        run(cond);
    }

    @Test
    public void testEnsureStatusWasCancUnhappyPath() {
        setStatus("RCVD");
        EnsureStatusWasCanc cond = new EnsureStatusWasCanc();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("not as expected"));
    }

    @Test
    public void testEnsureStatusWasRcvdHappyPath() {
        setStatus("RCVD");
        EnsureStatusWasRcvd cond = new EnsureStatusWasRcvd();

        run(cond);
    }

    @Test
    public void testEnsureStatusWasRcvdUnhappyPath() {
        setStatus("CANC");
        EnsureStatusWasRcvd cond = new EnsureStatusWasRcvd();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("not as expected"));
    }

    @Test
    public void testNoBody() {
        EnsureStatusWasCanc cond = new EnsureStatusWasCanc();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("not extract body"));
    }

    @Test
    public void testBodyWithNoData() {
        JsonObject responseData = new JsonObject();
        environment.putString(AbstractEnsureStatusWasX.RESPONSE_ENV_KEY, "body", responseData.toString());
        EnsureStatusWasCanc cond = new EnsureStatusWasCanc();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("doesn't contain data"));
    }

    private void setStatus(String status) {
        JsonObject data = new JsonObject();
        data.addProperty("status", status);
        JsonObject responseData = new JsonObject();
        responseData.add("data", data);
        environment.putString(AbstractEnsureStatusWasX.RESPONSE_ENV_KEY, "body", responseData.toString());
    }
}
