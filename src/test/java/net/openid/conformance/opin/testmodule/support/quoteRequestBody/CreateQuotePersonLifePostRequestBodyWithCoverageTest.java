package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuotePersonLifePostRequestBodyWithCoverageTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePersonLifePostRequestBodyWithCoverage cond = new CreateQuotePersonLifePostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        JsonObject data = requestBody.getAsJsonObject("data");
        JsonObject quoteData = data.getAsJsonObject("quoteData");
        JsonArray requestedCoverages = quoteData.getAsJsonArray("requestedCoverages");
        JsonObject requestedCoverage = requestedCoverages.get(0).getAsJsonObject();
        JsonObject quoteCustomer = data.get("quoteCustomer").getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(quoteCustomer);
        assertNotNull(requestedCoverages);
        assertNotNull(requestedCoverage);

        assertEquals("random_cpf", quoteCustomer.getAsJsonObject("identificationData").get("cpfNumber").getAsString());

        assertNotNull(quoteData.get("termStartDate"));
        assertNotNull(quoteData.get("termEndDate"));
        assertEquals("ANUAL", quoteData.get("termType").getAsString());
        assertFalse(quoteData.get("includeAssistanceServices").getAsBoolean());
        assertEquals("0111", requestedCoverage.get("branch").getAsString());
        assertEquals("MORTE", requestedCoverage.get("code").getAsString());
        assertFalse(requestedCoverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject insuredCapital = requestedCoverage.getAsJsonObject("insuredCapital");
        assertNotNull(insuredCapital);
        assertEquals("90.85", insuredCapital.get("amount").getAsString());
        assertEquals("PORCENTAGEM", insuredCapital.get("unitType").getAsString());
    }
}
