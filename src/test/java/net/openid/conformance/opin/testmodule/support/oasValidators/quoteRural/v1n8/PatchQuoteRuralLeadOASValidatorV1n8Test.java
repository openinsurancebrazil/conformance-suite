package net.openid.conformance.opin.testmodule.support.oasValidators.quoteRural.v1n8;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchQuoteRuralLeadOASValidatorV1n8Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchLeadResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchQuoteRuralLeadOASValidatorV1n8());
    }
}