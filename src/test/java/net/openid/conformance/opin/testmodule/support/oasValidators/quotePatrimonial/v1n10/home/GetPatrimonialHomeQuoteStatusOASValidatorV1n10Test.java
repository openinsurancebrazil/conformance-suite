package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.home.GetPatrimonialHomeQuoteStatusOASValidatorV1n9;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPatrimonialHomeQuoteStatusOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/home/GetPatrimonialHomeQuoteStatusOkV1.10.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPatrimonialHomeQuoteStatusOASValidatorV1n10());
    }
}