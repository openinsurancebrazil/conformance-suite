package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.privateGuarantee.v1n3;


import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetProductsNServicesPrivateGuaranteeOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesPrivateGuaranteeResponseV1n3.json")
    public void testHappyPath() {
        setStatus(200);
        GetProductsNServicesPrivateGuaranteeOASValidatorV1n3 cond = new GetProductsNServicesPrivateGuaranteeOASValidatorV1n3();
        run(cond);
    }
}
