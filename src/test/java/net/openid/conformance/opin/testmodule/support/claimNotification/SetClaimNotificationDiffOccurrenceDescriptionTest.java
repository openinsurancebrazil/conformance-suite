package net.openid.conformance.opin.testmodule.support.claimNotification;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.OPINPhase3Keys;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationDiffOccurrenceDescription;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetClaimNotificationDiffOccurrenceDescriptionTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        JsonObject resourceRequestEntity = new JsonObject();
        resourceRequestEntity.add("data", new JsonObject());
        environment.putObject("resource_request_entity", resourceRequestEntity);
    }

    @Test
    public void testSetClaimNotificationDiffOccurrenceDescription() {
        SetClaimNotificationDiffOccurrenceDescription cond = new SetClaimNotificationDiffOccurrenceDescription();
        run(cond);

        assertEquals(environment.getString("resource_request_entity", "data.occurrenceDescription"), OPINPhase3Keys.Keys_diffOccurrenceDescription);
    }
}
