package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceLifePensionMovementsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceLifePension/OpinInsuranceLifePensionMovementsValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceLifePensionMovementsOASValidatorV1n4());
	}

}
