package net.openid.conformance.opin.testmodule.support.oasValidators.assistanceGeneralAssets.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetAssistanceGeneralAssetsOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/GetAssistanceGeneralAssetsResponseOkV1.3.0.json")
	public void validateStructure() {
		setStatus(200);
		run(new GetAssistanceGeneralAssetsOASValidatorV1n3());
	}
}
