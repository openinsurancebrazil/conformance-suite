package net.openid.conformance.opin.testmodule.support.oasValidators.housing;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/housing/GetHousingResponseV1.json")
public class GetHousingOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void testHappyPath() {
		setStatus(200);
		run(new GetHousingOASValidatorV1());
	}
}
