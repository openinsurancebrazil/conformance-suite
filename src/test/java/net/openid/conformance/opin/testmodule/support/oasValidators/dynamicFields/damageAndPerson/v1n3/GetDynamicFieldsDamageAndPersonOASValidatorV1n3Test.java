package net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.damageAndPerson.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


public class GetDynamicFieldsDamageAndPersonOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/dynamicFields/GetDynamicFieldsDamageAndPersonOkResponseV1.3.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetDynamicFieldsDamageAndPersonOASValidatorV1n3());
	}
}
