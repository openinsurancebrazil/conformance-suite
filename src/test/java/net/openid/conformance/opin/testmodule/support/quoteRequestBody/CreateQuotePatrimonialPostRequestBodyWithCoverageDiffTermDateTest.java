package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuotePatrimonialPostRequestBodyWithCoverageDiffTermDateTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuoteHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialPostRequestBodyWithCoverage_DiffTermDate cond = new CreateQuotePatrimonialPostRequestBodyWithCoverage_DiffTermDate();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject data = requestBody.getAsJsonObject("data");
        JsonObject quoteData = data.getAsJsonObject("quoteData");
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();
        JsonObject quoteCustomer = data.get("quoteCustomer").getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(quoteCustomer);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals(environment.getString("consent_id"), data.get("consentId").getAsString());
        assertEquals("random_cpf", quoteCustomer.getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        assertNotNull(quoteData.get("termStartDate"));
        assertNotNull(quoteData.get("termEndDate"));
        assertTrue(quoteData.get("termStartDate").getAsString().compareTo(quoteData.get("termEndDate").getAsString()) > 0);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject maxLMI = coverage.getAsJsonObject("maxLMI");
        assertNotNull(maxLMI);
        assertEquals("90.85", maxLMI.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMI.get("unitType").getAsString());
    }
}
