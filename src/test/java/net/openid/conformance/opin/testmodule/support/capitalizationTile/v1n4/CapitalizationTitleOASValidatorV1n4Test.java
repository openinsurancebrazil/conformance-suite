package net.openid.conformance.opin.testmodule.support.capitalizationTile.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitle.v1n4.GetCapitalizationTitleOASValidatorV1n4;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class CapitalizationTitleOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitleResponseOkV1.4.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetCapitalizationTitleOASValidatorV1n4());
	}
}
