package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceAutoPremiumOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceAuto/OpinInsuranceAutoPremiumValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceAutoPremiumOASValidatorV1n3());
	}

}