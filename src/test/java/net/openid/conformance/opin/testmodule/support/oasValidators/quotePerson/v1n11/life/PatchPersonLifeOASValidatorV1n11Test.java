package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.life;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchPersonLifeOASValidatorV1n11Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/PatchPersonLifeOkV1.11.0.json")
    public void happyPathTest() {
        setStatus(200);
        run(new PatchPersonLifeOASValidatorV1n11());
    }
}