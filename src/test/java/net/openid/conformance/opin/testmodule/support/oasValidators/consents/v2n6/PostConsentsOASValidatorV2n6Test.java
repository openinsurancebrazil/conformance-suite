package net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n5.PostConsentsOASValidatorV2n5;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostConsentsOASValidatorV2n6Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/consents/v2/PostConsentResponseOK2.6.0.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostConsentsOASValidatorV2n6());
    }
}
