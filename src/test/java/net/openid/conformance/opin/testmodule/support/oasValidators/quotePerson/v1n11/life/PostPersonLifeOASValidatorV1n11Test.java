package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.life;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostPersonLifeOASValidatorV1n11Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/PostPersonLifeOkV1.11.0.json")
    public void happyPathTest() {
        setStatus(201);
        run(new PostPersonLifeOASValidatorV1n11());
    }
}