package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServerTest;

public class GetPensionPlanV1EndpointTest extends AbstractGetXFromAuthServerTest {

    @Override
    protected String getApiFamilyType() {
        return "insurance-pension-plan";
    }

    @Override
    protected String getApiVersion() {
        return "1.0.0";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

    @Override
    protected String getEndpoint() {
        return "https://test.com/open-insurance/insurance-pension-plan/v1/insurance-pension-plan/contracts";
    }

    @Override
    protected AbstractGetXFromAuthServer getCondition() {
        return new GetPensionPlanV1Endpoint();
    }
}