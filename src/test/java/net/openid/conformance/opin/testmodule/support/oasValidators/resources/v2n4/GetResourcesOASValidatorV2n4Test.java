package net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetResourcesOASValidatorV2n4Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListValidatorV2OK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetResourcesOASValidatorV2n4());
    }
}
