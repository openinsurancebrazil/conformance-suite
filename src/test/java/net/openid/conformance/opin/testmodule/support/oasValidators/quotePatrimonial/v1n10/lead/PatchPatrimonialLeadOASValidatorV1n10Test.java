package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.lead;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.lead.PatchPatrimonialLeadOASValidatorV1n9;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchPatrimonialLeadOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchLeadResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchPatrimonialLeadOASValidatorV1n10());
    }
}