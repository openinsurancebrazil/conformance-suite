package net.openid.conformance.opin.testmodule.support;


import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.AbstractCondition;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AddPersonScopeTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putObject("client", new JsonObject());
    }

    @Test
    public void test() {
        AbstractCondition cond = new AddPersonScope();
        run(cond);
        assertTrue(environment.getString("client", "scope").contains("insurance-person"));
    }
}
