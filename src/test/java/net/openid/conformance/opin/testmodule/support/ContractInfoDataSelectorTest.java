package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static com.mongodb.assertions.Assertions.assertNotNull;
import static org.junit.Assert.assertEquals;

public class ContractInfoDataSelectorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfo.json")
    public void weCanExtractPmbacAmountAndData(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        run(cond);
        assertNotNull(environment.getObject("pmbacAmount_1"));
    }

    @Test
    public void weCantExtractPmbacAmountWithoutData(){
        environment.putString("resource_endpoint_response_full", "body", "{}");
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not find data in the body", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfoNoData.json")
    public void weCantExtractPmbacAmountWithEmptyData(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Data array cannot be empty to extract pmbacAmount", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfoNoSuseps.json")
    public void weCantExtractPmbacAmountWithoutSuseps(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract suseps array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfoEmptySuseps.json")
    public void weCantExtractPmbacAmountWithEmptySuseps(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Suseps array cannot be empty to extract pmbacAmount", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfoNoFIE.json")
    public void weCantExtractPmbacAmountWithoutFIE(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract FIE array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfoEmptyFIE.json")
    public void weCantExtractPmbacAmountWithEmptyFIE(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: FIE array cannot be empty to extract pmbacAmount", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfoNoPmbacAmount.json")
    public void weCantExtractPmbacAmountWithoutPmbacAmount(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract pmbacAmount object from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractInfoEmptyPmbacAmount.json")
    public void weCantExtractPmbacAmountWithEmptyPmbacAmount(){
        ContractInfoDataSelector cond = new ContractInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: PmbacAmount object cannot be empty", cond.getClass().getSimpleName()), e.getMessage());
    }

}