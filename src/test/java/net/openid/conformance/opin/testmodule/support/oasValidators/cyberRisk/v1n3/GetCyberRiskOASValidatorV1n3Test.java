package net.openid.conformance.opin.testmodule.support.oasValidators.cyberRisk.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetCyberRiskOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/GetCyberRiskResponseOkV1.3.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetCyberRiskOASValidatorV1n3());
	}
}
