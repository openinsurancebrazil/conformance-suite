package net.openid.conformance.opin.testmodule.support.oasValidators.equipmentBreakdown.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetEquipmentBreakdownOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/GetEquipmentBreakdownResponseOkV1.3.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetEquipmentBreakdownOASValidatorV1n3());
	}
}
