package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExtractDynamicFieldsFromCapitalizationTitleTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/dynamicFields/GetDynamicFieldsCapitalizationTitleOkResponseV1.3.0.json")
    public void testHappyPath() {
        environment.putString("modality", "TRADICIONAL");
        ExtractDynamicFieldsFromCapitalizationTitle cond = new ExtractDynamicFieldsFromCapitalizationTitle();

        run(cond);

        JsonObject dynamicField = environment.getObject("dynamic_field");
        JsonArray dynamicFields = dynamicField.getAsJsonArray("dynamic_fields");
        assertEquals(1, dynamicFields.size());
        assertEquals("eEuEL1VVVxUbZJoyUHCatyv", dynamicFields.get(0).getAsJsonObject().get("fieldId").getAsString());
    }

    @Test
    public void emptyResponse() {
        environment.putString("modality", "TRADICIONAL");
        ExtractDynamicFieldsFromCapitalizationTitle cond = new ExtractDynamicFieldsFromCapitalizationTitle();

        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Could not extract body from response"));

    }

    @Test
    @UseResurce("opinResponses/dynamicFields/GetDynamicFieldsCapitalizationTitleResponseEmptyFields.json")
    public void emptyResponseTest() {
        environment.putString("modality", "TRADICIONAL");
        ExtractDynamicFieldsFromCapitalizationTitle cond = new ExtractDynamicFieldsFromCapitalizationTitle();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("The response doesn't contain any dynamic field matching the required modality"));
    }

}
