package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServerTest;

public class GetLifePensionV1EndpointTest  extends AbstractGetXFromAuthServerTest {

    @Override
    protected String getApiFamilyType() {
        return "insurance-life-pension";
    }

    @Override
    protected String getApiVersion() {
        return "1.0.0";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

    @Override
    protected String getEndpoint() {
        return "https://test.com/open-insurance/insurance-life-pension/v1/insurance-life-pension/contracts";
    }

    @Override
    protected AbstractGetXFromAuthServer getCondition() {
        return new GetLifePensionV1Endpoint();
    }
}