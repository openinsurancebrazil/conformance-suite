package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawal.capitalizationTitleWithdrawalConsentBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.CreateCapitalizationTitleWithdrawalConsentBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.CreateCapitalizationTitleWithdrawalMockConsentBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.CreateCapitalizationTitleWithdrawalPerdaDeInteresseConsentBody;
import net.openid.conformance.opin.testmodule.support.quoteCapitalizationTitleRaffleRequestBody.CreateQuoteCapitalizationTitleRaffleConsentBody;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CreateCapitalizationTitleWithdrawalConsentBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("planId", "plan_id");

        JsonObject consentRequest = new JsonObject();
        consentRequest.add("data", new JsonObject());
        environment.putObject("consent_endpoint_request", consentRequest);

        JsonObject product = new JsonObject();
        product.addProperty("productName", "product_Name");
        environment.putObject("product_1", product);

        JsonObject prAmount = new JsonObject();
        prAmount.addProperty("amount", 2000);
        JsonObject technicalProvision = new JsonObject();
        technicalProvision.add("prAmount", prAmount);
        JsonArray technicalProvisions = new JsonArray();
        technicalProvisions.add(technicalProvision);

        JsonObject title = new JsonObject();
        title.addProperty("titleId","title_id");
        title.addProperty("termEndDate","2025-01-01T08:30:00Z");
        title.add("technicalProvisions",technicalProvisions);
        JsonArray titles = new JsonArray();
        titles.add(title);

        JsonObject series = new JsonObject();
        series.addProperty("seriesId", "series_id");
        series.add("titles", titles);
        environment.putObject("series_1", series);

        JsonObject withdrawalTotalAmount = new JsonObject();
        withdrawalTotalAmount.addProperty("amount", 2000.05);
        JsonObject unit = new JsonObject();
        unit.addProperty("code","R$");
        unit.addProperty("description","BRL");
        withdrawalTotalAmount.add("unit", unit);
        environment.putObject("withdrawalTotalAmount", withdrawalTotalAmount);
    }

    @Test
    public void testCapitalizationTitleWithdrawal() {
        CreateCapitalizationTitleWithdrawalConsentBody cond = new CreateCapitalizationTitleWithdrawalConsentBody();
        run(cond);

        assertEquals("plan_id", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.planId"));
        assertEquals("2025-01-01T08:30:00Z", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.termEndDate"));
        assertEquals("product_Name", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.capitalizationTitleName"));
        assertEquals("title_id", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.titleId"));
        assertEquals("series_id", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.seriesId"));
        assertEquals("INSATISFACAO_COM_CARACTERISTICAS_DO_PRODUTO", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalReason"));
        assertEquals("2000.00",environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalTotalAmount.amount"));
    }

    @Test
    public void testCapitalizationTitleWithdrawalPerdaDeInteresse() {
        CreateCapitalizationTitleWithdrawalPerdaDeInteresseConsentBody cond = new CreateCapitalizationTitleWithdrawalPerdaDeInteresseConsentBody();
        run(cond);

        assertEquals("plan_id", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.planId"));
        assertEquals("2025-01-01T08:30:00Z", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.termEndDate"));
        assertEquals("product_Name", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.capitalizationTitleName"));
        assertEquals("title_id", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.titleId"));
        assertEquals("series_id", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.seriesId"));
        assertEquals("PERDA_DE_INTERESSE", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalReason"));
        assertEquals("2000.00", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalTotalAmount.amount"));
    }

    @Test
    public void testCreateQuoteCapitalizationTitleRaffleConsentBody(){
        CreateQuoteCapitalizationTitleRaffleConsentBody cond = new CreateQuoteCapitalizationTitleRaffleConsentBody();
        run(cond);
        assertEquals("EMAIL", environment.getString("consent_endpoint_request", "data.raffleCaptalizationTitleInformation.contactType"));
        assertEquals("contact@email.com", environment.getString("consent_endpoint_request", "data.raffleCaptalizationTitleInformation.email"));
    }

    @Test
    public void testCreateCapitalizationTitleWithdrawalMockConsentBody() {
        CreateCapitalizationTitleWithdrawalMockConsentBody cond = new CreateCapitalizationTitleWithdrawalMockConsentBody();
        run(cond);

        assertEquals("string2", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.planId"));
        assertEquals("2025-04-01", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.termEndDate"));
        assertEquals("string", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.capitalizationTitleName"));
        assertEquals("string3", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.titleId"));
        assertEquals("string4", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.seriesId"));
        assertEquals("IMPOSSIBILIDADE_DE_PAGAMENTO_DAS_PARCELAS", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalReason"));
        assertEquals("2000.05", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalTotalAmount.amount"));
        assertEquals("R$", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalTotalAmount.unit.code"));
        assertEquals("BRL", environment.getString("consent_endpoint_request", "data.withdrawalCaptalizationInformation.withdrawalTotalAmount.unit.description"));

    }
}
