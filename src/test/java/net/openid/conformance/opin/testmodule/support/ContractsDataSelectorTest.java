package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContractsDataSelectorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContracts.json")
    public void weCanExtractCertificateIdAndData(){
        ContractsDataSelector cond = new ContractsDataSelector();
        run(cond);
        String selectedId = environment.getString("selected_id");
        assertEquals("42", selectedId);
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContracts2Ids.json")
    public void weCanExtract2CertificateIdsAndData(){
        ContractsDataSelector cond = new ContractsDataSelector();
        run(cond);
        String selectedId = environment.getString("selected_id");
        assertEquals("42", selectedId);
        assertEquals("43",environment.getString("certificateId2"));
    }


    @Test
    public void weCantExtractCertificateIdWithoutData(){
        environment.putString("resource_endpoint_response_full", "body", "{}");
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not find data in the body", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsEmptyData.json")
    public void weCantExtractCertificateIdWithEmptyData(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Data array cannot be empty to extract certificate ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsNoBrand.json")
    public void weCantExtractCertificateIdWithoutBrand(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract brand.companies array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsNoCompanies.json")
    public void weCantExtractCertificateIdWithoutCompanies(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract brand.companies array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsEmptyCompanies.json")
    public void weCantExtractCertificateIdWithEmptyCompanies(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Companies array cannot be empty to extract certificate ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsNoContracts.json")
    public void weCantExtractCertificateIdWithoutContracts(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract contracts array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsEmptyContracts.json")
    public void weCantExtractCertificateIdWithEmptyContracts(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: contracts array cannot be empty to extract certificate ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsNoCertificateId.json")
    public void weCantExtractCertificateIdWithoutCertificateId(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: could not find certificate ID in the contract object", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/GetLifePensionContractsNo2ndCertificateId.json")
    public void weCantExtract2ndCertificateIdWithout2ndCertificateId(){
        ContractsDataSelector cond = new ContractsDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: could not find certificate ID in the 2nd contract object", cond.getClass().getSimpleName()), e.getMessage());
    }

}