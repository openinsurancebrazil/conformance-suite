package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.AbstractCondition;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuildPersonConfigResourceUrlFromConsentUrlTest extends AbstractJsonResponseConditionUnitTest {
    private static final String CONSENT_URL = "https://localhost/open-insurance/consents/v3/consents";
    private static final String RESOURCE_URL = "https://localhost/open-insurance/insurance-person/v1/insurance-person";

    @Before
    public void setUp() {
        environment.putString("config", "resource.consentUrl", CONSENT_URL);
    }

    @Test
    public void test() {
        AbstractCondition cond = new BuildInsurancePersonUrlFromConsentUrl();
        run(cond);

        assertEquals(RESOURCE_URL, environment.getString("protected_resource_url"));
        assertEquals(RESOURCE_URL, environment.getString("config", "resource.resourceUrl"));
    }
}