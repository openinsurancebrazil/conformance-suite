package net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1n3.PostEndorsementOASValidatorV1n3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostEndorsementOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/endorsement/PostEndorsementResponseV1n3.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostEndorsementOASValidatorV1n3());
    }
}