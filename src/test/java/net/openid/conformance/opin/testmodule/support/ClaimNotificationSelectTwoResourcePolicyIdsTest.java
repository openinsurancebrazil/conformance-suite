package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClaimNotificationSelectTwoResourcePolicyIdsTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListValidatorV2OK.json")
    public void testDamageHappyPath() {
        environment.putString("claim_notification_endpoint_type", "damage");
        ClaimNotificationSelectTwoResourcePolicyIds cond = new ClaimNotificationSelectTwoResourcePolicyIds();

        run(cond);

        assertEquals("25cac914-d8ae-6789-b215-650a6215820c", environment.getString("first_policy_id"));
        assertEquals("25cac914-d8ae-6789-b215-650a6215820d", environment.getString("second_policy_id"));
    }

    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListValidatorV2OK.json")
    public void testPersonHappyPath() {
        environment.putString("claim_notification_endpoint_type", "person");
        ClaimNotificationSelectTwoResourcePolicyIds cond = new ClaimNotificationSelectTwoResourcePolicyIds();

        run(cond);

        assertEquals("25cac914-d8ae-6789-b215-650a6215820a", environment.getString("first_policy_id"));
        assertEquals("25cac914-d8ae-6789-b215-650a6215820b", environment.getString("second_policy_id"));
    }
}
