package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.generalLiability.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.prdoctsNServices.generalLiability.v1n3.GetProductsNServicesGeneralLiabilityOASValidatorV1n3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetProductsNServicesGeneralLiabilityOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesGeneralLiabilityResponseV1n3.json")
    public void testHappyPath() {
        setStatus(200);
        GetProductsNServicesGeneralLiabilityOASValidatorV1n3 cond = new GetProductsNServicesGeneralLiabilityOASValidatorV1n3();
        run(cond);
    }
}