package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceHousingPolicyInfoOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceHousing/OpinInsuranceHousingPolicyInfoValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceHousingPolicyInfoOASValidatorV1n3());
	}

}
