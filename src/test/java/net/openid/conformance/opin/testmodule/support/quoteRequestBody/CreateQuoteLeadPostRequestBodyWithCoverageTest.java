package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuoteLeadPostRequestBodyWithCoverageTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteAutoLeadPostRequestBodyWithCoverage cond = new CreateQuoteAutoLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("CASCO_COMPREENSIVA", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testHousingQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteHousingLeadPostRequestBodyWithCoverage cond = new CreateQuoteHousingLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("DANOS_ELETRICOS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testFinancialRiskQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteFinancialRiskLeadPostRequestBodyWithCoverage cond = new CreateQuoteFinancialRiskLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("PROTECAO_DE_BENS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialLeadPostRequestBodyWithCoverage cond = new CreateQuotePatrimonialLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testQuotePersonPatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePersonLeadPostRequestBodyWithCoverage cond = new CreateQuotePersonLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("MORTE", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testQuoteResponsibilityPatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteResponsibilityLeadPostRequestBodyWithCoverage cond = new CreateQuoteResponsibilityLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("DANOS_CAUSADOS_A_TERCEIROS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testQuoteRuralPatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteRuralLeadPostRequestBodyWithCoverage cond = new CreateQuoteRuralLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("GRANIZO", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testQuoteTransportPatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteTransportLeadPostRequestBodyWithCoverage cond = new CreateQuoteTransportLeadPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("ACIDENTES_PESSOAIS_COM_PASSAGEIROS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditQuoteLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuoteAutoLeadPostRequestBodyWithCoverage cond = new EditQuoteAutoLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("CASCO_COMPREENSIVA", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditQuoteFinancialRiskLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuoteFinancialRiskLeadPostRequestBodyWithCoverage cond = new EditQuoteFinancialRiskLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("PROTECAO_DE_BENS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditQuoteHousingLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuoteHousingLeadPostRequestBodyWithCoverage cond = new EditQuoteHousingLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("DANOS_ELETRICOS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditQuotePatrimonialPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuotePatrimonialLeadPostRequestBodyWithCoverage cond = new EditQuotePatrimonialLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditPersonLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuotePersonLeadPostRequestBodyWithCoverage cond = new EditQuotePersonLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("MORTE", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditQuoteResponsibilityLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuoteResponsibilityLeadPostRequestBodyWithCoverage cond = new EditQuoteResponsibilityLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("DANOS_CAUSADOS_A_TERCEIROS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditQuoteRuralLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuoteRuralLeadPostRequestBodyWithCoverage cond = new EditQuoteRuralLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("GRANIZO", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }

    @Test
    public void testEditQuoteTransportLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuoteTransportLeadPostRequestBodyWithCoverage cond = new EditQuoteTransportLeadPostRequestBodyWithCoverage();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data").get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("ACIDENTES_PESSOAIS_COM_PASSAGEIROS", coverage.get("code").getAsString());
        assertEquals("string", coverage.get("description").getAsString());
    }
}
