package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPersonLifeQuoteStatusOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/GetPersonLifeQuoteStatusOkV1.10.2.json")
    public void happyPathTest() {
        setStatus(200);
        run(new GetPersonLifeQuoteStatusOASValidatorV1n10());
    }
}