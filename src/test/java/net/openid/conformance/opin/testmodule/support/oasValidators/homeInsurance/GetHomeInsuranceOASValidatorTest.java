package net.openid.conformance.opin.testmodule.support.oasValidators.homeInsurance;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetHomeInsuranceOASValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/homeInsurance/GetHomeInsuranceResponse.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetHomeInsuranceOASValidator());
	}
}
