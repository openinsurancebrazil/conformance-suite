package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceFinancialAssistanceContractInfoOASValidatorV1N2Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceFinancialAssistance/OpinInsuranceFinancialAssistanceContractInfoValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceFinancialAssistanceContractInfoOASValidatorV1n2());
	}
}
