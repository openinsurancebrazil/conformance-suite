package net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.capitalizationTitle.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


public class GetDynamicFieldsCapitalizationTitleOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/dynamicFields/GetDynamicFieldsCapitalizationTitleOkResponseV1.3.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetDynamicFieldsCapitalizationTitleOASValidatorV1n3());
	}
}
