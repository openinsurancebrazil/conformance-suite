package net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetConsentsOASValidatorV2n7Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/consents/v2/GetConsentResponseOk2.7.0.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetConsentsOASValidatorV2n7());
    }
}
