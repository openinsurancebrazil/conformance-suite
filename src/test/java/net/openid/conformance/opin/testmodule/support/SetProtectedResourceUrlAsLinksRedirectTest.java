package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class SetProtectedResourceUrlAsLinksRedirectTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchResponseOK.json")
    public void testHappyPathLinksRedirect() {
        SetProtectedResourceUrlAsLinksRedirect cond = new SetProtectedResourceUrlAsLinksRedirect();

        run(cond);

        assertEquals("http://www.abcseguros.com/propostas_cotacao?propID=003", environment.getString("protected_resource_url"));
    }

    @Test
    @UseResurce("opinResponses/contractLifePension/PatchContractLifePensionOkV1.12.1.json")
    public void testHappyPathRedirectLink() {
        SetProtectedResourceUrlAsLinksRedirect cond = new SetProtectedResourceUrlAsLinksRedirect();

        run(cond);

        assertEquals("https://www.abcseguros.com/raffle?id=000123", environment.getString("protected_resource_url"));
    }

    @Test
    public void testNoBody() {
        SetProtectedResourceUrlAsLinksRedirect cond = new SetProtectedResourceUrlAsLinksRedirect();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("not extract body"));
    }

    @Test
    public void testBodyWithNoData() {
        JsonObject responseData = new JsonObject();
        environment.putString(SetProtectedResourceUrlAsLinksRedirect.RESPONSE_ENV_KEY, "body", responseData.toString());
        SetProtectedResourceUrlAsLinksRedirect cond = new SetProtectedResourceUrlAsLinksRedirect();

        ConditionError error = runAndFail(cond);

        assertThat(error.getMessage(), containsString("doesn't contain data"));
    }
}
