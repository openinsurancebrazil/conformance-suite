package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServerTest;
import org.junit.Before;

public class GetQuoteResourceV1EndpointTest extends AbstractGetXFromAuthServerTest {

    private final String apiFamilyType = "testApiFamily";
    private final String apiName = "testApi";
    private final String apiEndpoint = "testEndpoint";

    @Override
    @Before
    public void init() {
        this.environment.putString("api_family_type", apiFamilyType);
        this.environment.putString("api_name", apiName);
        this.environment.putString("api_endpoint", apiEndpoint);
        super.init();
    }


    @Override
    protected String getApiFamilyType() {
        return apiFamilyType;
    }

    @Override
    protected String getApiVersion() {
        return "1.0.0";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

    @Override
    protected String getEndpoint() {
        return "https://test.com/open-insurance/"+ apiName + "/v1/" + apiEndpoint;
    }

    @Override
    protected AbstractGetXFromAuthServer getCondition() {
        return new GetQuoteResourceV1Endpoint();
    }
}



