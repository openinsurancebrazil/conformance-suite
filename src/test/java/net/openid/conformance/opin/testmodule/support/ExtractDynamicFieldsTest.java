package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExtractDynamicFieldsTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/dynamicFields/GetDynamicFieldsDamageAndPersonOkResponseV1.3.0.json")
    public void testHappyPath() {
        environment.putString("dynamic_field_api_type", "QUOTE_AUTO");
        ExtractDynamicFields cond = new ExtractDynamicFields();

        run(cond);

        JsonObject dynamicField = environment.getObject("dynamic_field");
        JsonArray dynamicFields = dynamicField.getAsJsonArray("dynamic_fields");
        assertEquals(1, dynamicFields.size());
        assertEquals("BLtsbdIih6BELbBfByQuXqAErQMUNYMo3XHt", dynamicFields.get(0).getAsJsonObject().get("fieldId").getAsString());
    }
}
