package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsurancePensionPlanPortabilitiesOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePensionPlan/OpinInsurancePensionPlanPortabilitiesValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsurancePensionPlanPortabilitiesOASValidatorV1n4());
	}
}