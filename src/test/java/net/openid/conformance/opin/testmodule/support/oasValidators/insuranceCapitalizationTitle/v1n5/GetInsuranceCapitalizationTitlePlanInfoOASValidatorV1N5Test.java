package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1N5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceCapitalizationTitle/OpinInsuranceCapitalizationTitlePlanInfoValidatorV1.5.0.OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n5());
	}

}
