package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.lead;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchPatrimonialLeadOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchLeadResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchPatrimonialLeadOASValidatorV1n9());
    }
}