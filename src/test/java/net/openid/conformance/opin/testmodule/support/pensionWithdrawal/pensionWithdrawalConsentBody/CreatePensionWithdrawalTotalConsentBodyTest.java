package net.openid.conformance.opin.testmodule.support.pensionWithdrawal.pensionWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CreatePensionWithdrawalTotalConsentBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("certificateId", "certificate_id");
        environment.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());

        JsonObject consentRequest = new JsonObject();
        consentRequest.add("data", new JsonObject());
        environment.putObject("consent_endpoint_request", consentRequest);

        JsonObject contract = new JsonObject();
        contract.addProperty("productName", "product_Name");
        environment.putObject("contract_1", contract);

        JsonObject pmbacAmount = new JsonObject();
        pmbacAmount.addProperty("amount", "2000.00");
        pmbacAmount.add("unit", new JsonObject());
        environment.putObject("pmbacAmount_1", pmbacAmount);
    }

    @Test
    public void testPensionWithdrawalTotal() {
        CreatePensionWithdrawalTotalConsentBody cond = new CreatePensionWithdrawalTotalConsentBody();
        run(cond);

        assertEquals("certificate_id", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.certificateId"));
        assertEquals("product_Name", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.productName"));
        assertEquals("1_TOTAL", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalType"));
        assertEquals("2000.00", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.pmbacAmount.amount"));
        assertEquals("5_INSATISFACAO_COM_O_PRODUTO", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalReason"));
    }

    @Test
    public void testPensionWithdrawalParcial() {
        CreatePensionWithdrawalParcialConsentBody cond = new CreatePensionWithdrawalParcialConsentBody();
        run(cond);

        assertEquals("certificate_id", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.certificateId"));
        assertEquals("product_Name", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.productName"));
        assertEquals("1_TOTAL", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalType"));
        assertEquals("2000.00", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.pmbacAmount.amount"));
        assertEquals("1000.00", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.desiredTotalAmount.amount"));
        assertEquals("5_INSATISFACAO_COM_O_PRODUTO", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalReason"));
    }

    @Test
    public void testPensionWithdrawalTotalDouble() {
        CreatePensionWithdrawalTotalDoubledConsentBody cond = new CreatePensionWithdrawalTotalDoubledConsentBody();
        run(cond);

        assertEquals("certificate_id", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.certificateId"));
        assertEquals("product_Name", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.productName"));
        assertEquals("1_TOTAL", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalType"));
        assertEquals("4000.00", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.desiredTotalAmount.amount"));
        assertEquals("5_INSATISFACAO_COM_O_PRODUTO", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalReason"));
    }

    @Test
    public void testPensionWithdrawalTotalHalf() {
        CreatePensionWithdrawalTotalHalvedConsentBody cond = new CreatePensionWithdrawalTotalHalvedConsentBody();
        run(cond);

        assertEquals("certificate_id", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.certificateId"));
        assertEquals("product_Name", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.productName"));
        assertEquals("1_TOTAL", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalType"));
        assertEquals("1000.00", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.desiredTotalAmount.amount"));
        assertEquals("5_INSATISFACAO_COM_O_PRODUTO", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalReason"));
    }

    @Test
    public void testPensionWithdrawalMock() {
        CreatePensionWithdrawalMockConsentBody cond = new CreatePensionWithdrawalMockConsentBody();
        run(cond);

        assertEquals("string2", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.certificateId"));
        assertEquals("string", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.productName"));
        assertEquals("1_TOTAL", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalType"));
        assertEquals("9999.00", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.pmbacAmount.amount"));
        assertEquals("5_INSATISFACAO_COM_O_PRODUTO", environment.getString("consent_endpoint_request", "data.withdrawalLifePensionInformation.withdrawalReason"));
    }
}
