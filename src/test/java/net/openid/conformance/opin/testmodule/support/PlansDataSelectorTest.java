package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlansDataSelectorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void weCanExtractPlanIdAndData(){
        PlansDataSelector cond = new PlansDataSelector();
        run(cond);
        String selectedId = environment.getString("selected_id");
        assertEquals("test_plan_id", selectedId);
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans2Ids.json")
    public void weCanExtract2PlanIdsAndData(){
        PlansDataSelector cond = new PlansDataSelector();
        run(cond);
        String selectedId = environment.getString("selected_id");
        assertEquals("test_plan_id", selectedId);
        assertEquals("test_plan_id2",environment.getString("planId2"));
    }


    @Test
    public void weCantExtractPlanIdWithoutData(){
        environment.putString("resource_endpoint_response_full", "body", "{}");
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not find data in the body", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyData.json")
    public void weCantExtractPlanIdWithEmptyData(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Data array cannot be empty to extract plan ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoBrand.json")
    public void weCantExtractPlanIdWithoutBrand(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract brand.companies array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoCompanies.json")
    public void weCantExtractPlanIdWithoutCompanies(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract brand.companies array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyCompanies.json")
    public void weCantExtractPlanIdWithEmptyCompanies(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Companies array cannot be empty to extract plan ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoProducts.json")
    public void weCantExtractPlanIdWithoutProducts(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract products array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyProducts.json")
    public void weCantExtractPlanIdWithEmptyProducts(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Products array cannot be empty to extract plan ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoPlanId.json")
    public void weCantExtractPlanIdWithoutPlanId(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: could not find plan ID in the product object", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNo2ndPlanId.json")
    public void weCantExtract2ndPlanIdWithout2ndPlanId(){
        PlansDataSelector cond = new PlansDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: could not find plan ID in the 2nd product object", cond.getClass().getSimpleName()), e.getMessage());
    }

}