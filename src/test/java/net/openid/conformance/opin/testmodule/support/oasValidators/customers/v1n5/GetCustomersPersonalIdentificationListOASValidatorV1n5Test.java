
package net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetCustomersPersonalIdentificationListOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {
	@Test
	@UseResurce("opinResponses/customers/v1/personaIdentification/naturalPersonIdentificationResponseOK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetCustomersPersonalIdentificationListOASValidatorV1n5());
	}
}
