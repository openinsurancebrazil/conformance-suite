package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IdSelectorFromJsonPathTest extends AbstractJsonResponseConditionUnitTest{

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void testElementExists() {
        environment.putString("selected_id_json_path", "$.data[0].brand.companies[0].products[0].planId");
        IdSelectorFromJsonPath cond = new IdSelectorFromJsonPath();
        run(cond);

        assertEquals("test_plan_id", environment.getString("selected_id"));
    }

    @Test
    public void testNoBody() {
        environment.putString("selected_id_json_path", "");
        IdSelectorFromJsonPath cond = new IdSelectorFromJsonPath();
        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Could not find response body"));

    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void testElementNotFound() {

        environment.putString("selected_id_json_path", "$.data[0].invalid");
        IdSelectorFromJsonPath cond = new IdSelectorFromJsonPath();
        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("The json object does not correspond to the json path"));

    }

}
