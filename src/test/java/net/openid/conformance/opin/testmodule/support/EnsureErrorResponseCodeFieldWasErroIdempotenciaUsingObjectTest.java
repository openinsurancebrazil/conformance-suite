package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static com.mongodb.assertions.Assertions.assertTrue;

public class EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObjectTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void init() {
        environment.mapKey(EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
    }

    @Test
    @UseResurce("support/ErrorIdempotencyObjectResponse.json")
    public void happyPathErroIdempotenciaTest() {
        EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject condition = new EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject();
        run(condition);
    }

    @Test
    @UseResurce("support/ErrorIdempotencyArrayResponse.json")
    public void unhappyPathErroIdempotenciaIsNotObjectTest() {
        EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject condition = new EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject();
        ConditionError error = runAndFail(condition);

        String expectedMessage = "Errors is not JSON object";
        assertTrue(error.getMessage().contains(expectedMessage));
    }

    @Test
    @UseResurce("support/ErrorIdempotencyObjectWrongResponse.json")
    public void unhappyPathWrongErrorTest() {
        EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject condition = new EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject();
        ConditionError error = runAndFail(condition);

        String expectedMessage = "Could not find error with expected code in the errors JSON object";
        assertTrue(error.getMessage().contains(expectedMessage));
    }

}
