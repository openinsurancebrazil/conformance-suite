package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialClaimOASValidatorV1n4;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
import org.springframework.http.HttpMethod;
public class GetInsurancePersonListOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePerson/OpinInsurancePersonListValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsurancePersonListOASValidatorV1n5());
	}

}