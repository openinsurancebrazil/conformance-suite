package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceLifePensionPortabilitiesOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceLifePension/OpinInsuranceLifePensionPortabilitiesValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceLifePensionPortabilitiesOASValidatorV1n4());
	}

}