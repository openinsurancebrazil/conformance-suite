package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceRuralPolicyInfoOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/rural/policyInfo/policyInfoResponse.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceRuralPolicyInfoOASValidatorV1n3());
	}

}
