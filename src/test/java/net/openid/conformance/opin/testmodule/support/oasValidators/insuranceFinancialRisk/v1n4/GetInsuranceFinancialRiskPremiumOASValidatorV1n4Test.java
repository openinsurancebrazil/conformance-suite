package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceFinancialRiskPremiumOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/financialRisk/v1n4/OpinInsuranceFinancialRiskPremiumValidatorV1n4OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceFinancialRiskPremiumOASValidatorV1n4());
	}

}