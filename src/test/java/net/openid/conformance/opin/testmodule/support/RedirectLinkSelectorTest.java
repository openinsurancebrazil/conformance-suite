package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RedirectLinkSelectorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/capitalizationTitleWithdrawal/PostCapitalizationTitleWithdrawalResponseOkV1n3.json")
    public void weCanExtractRedirectLinkAndData(){
        RedirectLinkSelector cond = new RedirectLinkSelector();
        run(cond);
        String selectedId = environment.getString("protected_resource_url");
        assertEquals("https://www.abcseguros.com/withdrawal?id=000123", selectedId);
    }

    @Test
    public void weCantExtractRedirectLinkWithoutData(){
        environment.putString("resource_endpoint_response_full", "body", "{}");
        RedirectLinkSelector cond = new RedirectLinkSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not find data in the body", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitleWithdrawal/PostCapitalizationTitleWithdrawalResponseNoData.json")
    public void weCantExtractRedirectLinkWithEmptyData(){
        RedirectLinkSelector cond = new RedirectLinkSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Data object cannot be empty to extract redirectLink", cond.getClass().getSimpleName()), e.getMessage());
    }
    
    @Test
    @UseResurce("opinResponses/capitalizationTitleWithdrawal/PostCapitalizationTitleWithdrawalResponseNoRedirectLink.json")
    public void weCantExtractRedirectLinkWithoutRedirectLink(){
        RedirectLinkSelector cond = new RedirectLinkSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract redirectLink from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitleWithdrawal/PostCapitalizationTitleWithdrawalResponseEmptyRedirectLink.json")
    public void weCantExtractRedirectLinkWithEmptyRedirectLink(){
        RedirectLinkSelector cond = new RedirectLinkSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: RedirectLink cannot be empty", cond.getClass().getSimpleName()), e.getMessage());
    }

}