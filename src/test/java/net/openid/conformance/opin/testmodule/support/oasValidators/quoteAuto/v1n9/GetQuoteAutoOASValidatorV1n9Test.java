package net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetQuoteAutoOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/auto/GetQuoteAutoResponseOkV1.9.0.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetQuoteAutoOASValidatorV1n9());
    }
}