package net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitleWithdrawal.v1n3;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class PostCapitalizationTitleWithdrawalOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/capitalizationTitleWithdrawal/PostCapitalizationTitleWithdrawalResponseOkV1n3.json")
    public void testHappyPath() {
        setStatus(201);
        PostCapitalizationTitleWithdrawalOASValidatorV1n3 cond =new PostCapitalizationTitleWithdrawalOASValidatorV1n3();
        run(cond);
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitleWithdrawal/PostCapitalizationTitleWithdrawalResponseBadOUTROSV1n3.json")
    public void testUnhappyPath() {
        setStatus(201);
        PostCapitalizationTitleWithdrawalOASValidatorV1n3 cond = new PostCapitalizationTitleWithdrawalOASValidatorV1n3();
        runAndFail(cond);

        JsonObject withdrawalInformation = new JsonObject();
        withdrawalInformation.addProperty("withdrawalReason", "OUTROS");
        assertThrows(ConditionError.class, () -> cond.assertWithdrawalInformation(withdrawalInformation));
    }
}
