package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceAcceptanceAndBranchesAbroad/v1n4/OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1.4.0.OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n4());
	}
}