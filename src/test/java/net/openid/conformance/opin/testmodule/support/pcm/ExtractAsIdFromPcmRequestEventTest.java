package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class ExtractAsIdFromPcmRequestEventTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce(value = "pcmRequests/metricEventRequest.json", key = "pcm_event")
    public void weCanExtractAsIdFromPcmRequestEvent(){
        ExtractAsIdFromPcmRequestEvent cond = new ExtractAsIdFromPcmRequestEvent();
        run(cond);
        String authorisationServerId = environment.getString("authorisationServerId");
        Assert.assertNotNull(authorisationServerId);
        Assert.assertEquals("f8cd7b48-197d-419b-8680-f42226111b6f", authorisationServerId);
    }

}