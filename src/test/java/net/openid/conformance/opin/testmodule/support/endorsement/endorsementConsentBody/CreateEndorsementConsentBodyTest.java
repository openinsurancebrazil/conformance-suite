package net.openid.conformance.opin.testmodule.support.endorsement.endorsementConsentBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyAlteracao;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyCancelamento;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusao;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyInclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.EndorsementType;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateEndorsementConsentBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("policyId", "policy_id");
        JsonObject consentRequest = new JsonObject();
        consentRequest.add("data", new JsonObject());
        environment.putObject("consent_endpoint_request", consentRequest);
    }

    @Test
    public void testEndorsementAlteracao() {
        CreateEndorsementConsentBodyAlteracao cond = new CreateEndorsementConsentBodyAlteracao();

        run(cond);

        assertEquals("policy_id", environment.getString("consent_endpoint_request", "data.endorsementInformation.policyNumber"));
        assertEquals(EndorsementType.ALTERACAO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNotNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.requestDescription"));
        assertNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.insuredObjectId"));
    }

    @Test
    public void testEndorsementCancelamento() {
        CreateEndorsementConsentBodyCancelamento cond = new CreateEndorsementConsentBodyCancelamento();

        run(cond);

        assertEquals(EndorsementType.CANCELAMENTO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.insuredObjectId"));
    }

    @Test
    public void testEndorsementInclusao() {
        CreateEndorsementConsentBodyInclusao cond = new CreateEndorsementConsentBodyInclusao();

        run(cond);

        assertEquals(EndorsementType.INCLUSAO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.insuredObjectId"));

    }

    @Test
    public void testEndorsementExclusao() {
        CreateEndorsementConsentBodyExclusao cond = new CreateEndorsementConsentBodyExclusao();

        run(cond);

        assertEquals(EndorsementType.EXCLUSAO.name(), environment.getString("consent_endpoint_request", "data.endorsementInformation.endorsementType"));
        assertNull(environment.getString("consent_endpoint_request", "data.endorsementInformation.insuredObjectId"));

    }
}
