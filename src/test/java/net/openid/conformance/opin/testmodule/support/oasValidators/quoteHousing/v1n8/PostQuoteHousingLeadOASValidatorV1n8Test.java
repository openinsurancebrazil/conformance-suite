package net.openid.conformance.opin.testmodule.support.oasValidators.quoteHousing.v1n8;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostQuoteHousingLeadOASValidatorV1n8Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPostResponseOK.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostQuoteHousingLeadOASValidatorV1n8());
    }
}
