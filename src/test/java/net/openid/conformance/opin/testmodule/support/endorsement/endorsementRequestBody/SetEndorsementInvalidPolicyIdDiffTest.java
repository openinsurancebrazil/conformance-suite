package net.openid.conformance.opin.testmodule.support.endorsement.endorsementRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.OPINPhase3Keys;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIdToInvalidDiff;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetEndorsementInvalidPolicyIdDiffTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        JsonObject resourceRequestEntity = new JsonObject();
        resourceRequestEntity.add("data", new JsonObject());
        environment.putObject("resource_request_entity", resourceRequestEntity);

    }

    @Test
    public void testSetEndorsementRequestDescription() {
        SetEndorsementPolicyIdToInvalidDiff cond = new SetEndorsementPolicyIdToInvalidDiff();
        run(cond);

        assertEquals(environment.getString("resource_request_entity", "data.policyId"), OPINPhase3Keys.KEYS_INVALID_POLICY_ID);
    }
}
