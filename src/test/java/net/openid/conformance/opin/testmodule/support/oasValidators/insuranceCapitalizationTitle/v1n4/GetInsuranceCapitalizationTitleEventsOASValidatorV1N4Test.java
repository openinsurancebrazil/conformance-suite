package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceCapitalizationTitleEventsOASValidatorV1N4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceCapitalizationTitle/OpinInsuranceCapitalizationTitleEventsValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceCapitalizationTitleEventsOASValidatorV1n4());
	}

}
