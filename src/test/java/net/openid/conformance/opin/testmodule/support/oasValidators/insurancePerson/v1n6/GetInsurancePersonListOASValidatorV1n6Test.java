package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsurancePersonListOASValidatorV1n6Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePerson/OpinInsurancePersonListValidatorV1.6.0.OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsurancePersonListOASValidatorV1n6());
	}
}