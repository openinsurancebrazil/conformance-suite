package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuotePersonLifePostRequestBodyWithCoverageDiffTermDateTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuoteHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePersonLifePostRequestBodyWithCoverage_DiffTermDate cond = new CreateQuotePersonLifePostRequestBodyWithCoverage_DiffTermDate();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject data = requestBody.getAsJsonObject("data");
        JsonObject quoteData = data.getAsJsonObject("quoteData");
        JsonArray requestedCoverages = quoteData.getAsJsonArray("requestedCoverages");
        JsonObject requestedCoverage = requestedCoverages.get(0).getAsJsonObject();
        JsonObject quoteCustomer = data.get("quoteCustomer").getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(quoteCustomer);
        assertNotNull(requestedCoverages);
        assertNotNull(requestedCoverage);

        assertEquals(environment.getString("consent_id"), data.get("consentId").getAsString());
        assertEquals("random_cpf", quoteCustomer.getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        assertNotNull(quoteData.get("termStartDate"));
        assertNotNull(quoteData.get("termEndDate"));
        assertEquals("ANUAL", quoteData.get("termType").getAsString());
        assertFalse(quoteData.get("includeAssistanceServices").getAsBoolean());
        assertTrue(quoteData.get("termStartDate").getAsString().compareTo(quoteData.get("termEndDate").getAsString()) > 0);

        assertEquals("0111", requestedCoverage.get("branch").getAsString());
        assertEquals("MORTE", requestedCoverage.get("code").getAsString());
        assertFalse(requestedCoverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject insuredCapital = requestedCoverage.getAsJsonObject("insuredCapital");
        assertNotNull(insuredCapital);
        assertEquals("90.85", insuredCapital.get("amount").getAsString());
        assertEquals("PORCENTAGEM", insuredCapital.get("unitType").getAsString());
    }
}
