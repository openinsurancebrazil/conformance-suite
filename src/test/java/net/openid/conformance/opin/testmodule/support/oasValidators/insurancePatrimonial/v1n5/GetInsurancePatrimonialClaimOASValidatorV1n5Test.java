package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsurancePatrimonialClaimOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePatrimonial/OpinInsurancePatrimonialClaimValidatorV1.5.0.OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsurancePatrimonialClaimOASValidatorV1n5());
	}

}
