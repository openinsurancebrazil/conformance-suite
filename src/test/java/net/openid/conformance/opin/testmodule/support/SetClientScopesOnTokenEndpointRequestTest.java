package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetClientScopesOnTokenEndpointRequestTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    public void testHappyPath() {
        String scopes = "scope1 scope2";
        JsonObject requestParams = new JsonObject();
        environment.putObject("token_endpoint_request_form_parameters", requestParams);
        environment.putString("client", "scope", scopes);
        SetClientScopesOnTokenEndpointRequest cond = new SetClientScopesOnTokenEndpointRequest();

        run(cond);

        assertEquals(scopes, requestParams.get("scope").getAsString());
    }
}
