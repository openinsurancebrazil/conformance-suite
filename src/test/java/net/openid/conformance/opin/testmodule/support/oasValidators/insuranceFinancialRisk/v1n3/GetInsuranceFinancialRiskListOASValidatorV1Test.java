package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceFinancialRiskListOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/financialRisk/OpinInsuranceFinancialRiskListValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceFinancialRiskListOASValidatorV1n3());
	}

}
