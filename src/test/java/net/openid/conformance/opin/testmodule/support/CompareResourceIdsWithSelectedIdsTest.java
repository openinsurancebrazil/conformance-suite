package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CompareResourceIdsWithSelectedIdsTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        addResourceIds();
        addSelectedIdsToEnv();
    }

    @Test
    public void testSuccessful() {
        addSelectedIdsToEnv("random_id1");
        addResourceIds("random_id1");
        CompareResourceIdsWithSelectedIds cond = new CompareResourceIdsWithSelectedIds();
        run(cond);
    }

    @Test
    public void testOnlySelectedIdsIsEmpty() {
        addResourceIds("random_id");
        CompareResourceIdsWithSelectedIds cond = new CompareResourceIdsWithSelectedIds();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("API IDs array is empty"));
    }

    @Test
    public void testOnlyResourceIdsIsEmpty() {
        addSelectedIdsToEnv("random_id");
        CompareResourceIdsWithSelectedIds cond = new CompareResourceIdsWithSelectedIds();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Resources IDs array is empty"));
    }

    @Test
    public void testDifferentSizes() {
        addSelectedIdsToEnv("random_id1");
        addResourceIds("random_id1", "random_id2");
        CompareResourceIdsWithSelectedIds cond = new CompareResourceIdsWithSelectedIds();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Sizes of two resource lists are not equal"));
    }

    @Test
    public void testDifferentIds() {
        addSelectedIdsToEnv("random_id1");
        addResourceIds("random_id2");
        CompareResourceIdsWithSelectedIds cond = new CompareResourceIdsWithSelectedIds();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("API resources do not have a resource fetched from the resource endpoint response"));
    }

    private void addResourceIds(String... ids) {
        JsonObject resourceIdsObject = environment.getObject("extracted_resource_id");
        if(resourceIdsObject == null) {
            resourceIdsObject = new JsonObject();
        }

        JsonArray resourceIds = resourceIdsObject.getAsJsonArray("extractedResourceIds");
        if(resourceIds == null) {
            resourceIds = new JsonArray();
        }

        for(String selectedId : ids) {
            resourceIds.add(selectedId);
        }
        resourceIdsObject.add("extractedResourceIds", resourceIds);
        environment.putObject("extracted_resource_id", resourceIdsObject);

    }

    private void addSelectedIdsToEnv(String... ids) {
        JsonArray selectedIds = new JsonArray();
        for(String selectedId: ids) {
            selectedIds.add(selectedId);
        }
        environment.putString("selected_id", selectedIds.toString());
    }

}
