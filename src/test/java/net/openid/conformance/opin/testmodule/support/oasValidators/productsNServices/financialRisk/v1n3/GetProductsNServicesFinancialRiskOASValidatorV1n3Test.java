package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.financialRisk.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetProductsNServicesFinancialRiskOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesFinancialRiskResponseV1n3.json")
    public void testHappyPath() {
        setStatus(200);
        GetProductsNServicesFinancialRiskOASValidatorV1n3 cond = new GetProductsNServicesFinancialRiskOASValidatorV1n3();
        run(cond);
    }
}
