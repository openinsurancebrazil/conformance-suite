package net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.lead;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostContractLifePensionLeadOASValidatorV1n13Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/contractLifePension/PostContractLifePensionLeadOkV1.13.0.json")
    public void happyPathTest() {
        setStatus(201);
        run(new PostContractLifePensionLeadOASValidatorV1n13());
    }
}