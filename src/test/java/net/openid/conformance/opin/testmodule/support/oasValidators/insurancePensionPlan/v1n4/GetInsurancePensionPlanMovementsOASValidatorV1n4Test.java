package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsurancePensionPlanMovementsOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePensionPlan/OpinInsurancePensionPlanMovementsValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsurancePensionPlanMovementsOASValidatorV1n4());
	}

}