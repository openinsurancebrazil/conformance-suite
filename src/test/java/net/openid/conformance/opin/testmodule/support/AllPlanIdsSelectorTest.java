package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AllPlanIdsSelectorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void testWeCanExtractPlanId(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();

        run(cond);

        String selectedIdsString = environment.getString("selected_id");
        JsonArray selectedIds = JsonParser.parseString(selectedIdsString).getAsJsonArray();
        assertEquals(3, selectedIds.size());
        assertTrue(selectedIds.contains(new JsonPrimitive("test_plan_id")));
        assertTrue(selectedIds.contains(new JsonPrimitive("test_plan_id2")));
        assertTrue(selectedIds.contains(new JsonPrimitive("test_plan_id3")));
    }

    @Test
    public void weCanExtractPlanIdWithoutResponseBody() {
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("response body"));
    }

    @Test
    public void weCantExtractPlanIdWithoutData() {
        environment.putString("resource_endpoint_response_full", "body", "{}");
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Could not find data"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyData.json")
    public void testWeCantExtractPlanIdWithEmptyData(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Data field is empty"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoBrand.json")
    public void testWeCantExtractPlanIdWithoutBrand(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Could not extract brand.companies"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoCompanies.json")
    public void weCantExtractPlanIdWithoutCompanies(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Could not extract brand.companies"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyCompanies.json")
    public void testWeCantExtractPlanIdWithEmptyCompanies(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Companies array cannot be empty"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoProducts.json")
    public void testWeCantExtractPlanIdWithoutProducts(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Could not extract products"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyProducts.json")
    public void weCantExtractPlanIdWithEmptyProducts(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Products array cannot be empty"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoPlanId.json")
    public void testWeCantExtractPlanIdWithoutPlanId(){
        AllPlanIdsSelector cond = new AllPlanIdsSelector();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("Could not find plan ID"));
    }
}
