package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceTransportPremiumOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceTransport/v1n3/OpinInsuranceTransportPremiumValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceTransportPremiumOASValidatorV1n3());
	}


}