package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.*;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuotePostRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuoteLeadPostRequestBody cond = new CreateQuoteLeadPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData"));
    }

    @Test
    public void testBusinessQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        CreateQuoteLeadPostRequestBody cond = new CreateQuoteLeadPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cnpj", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").getAsJsonObject("document").get("businesscnpjNumber").getAsString());
    }

    @Test
    public void testPersonalCreateQuotePatrimonialHomePostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialHomePostRequestBody cond = new CreateQuotePatrimonialHomePostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");
        assertEquals(
                "random_cpf",
                quoteData.getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );
        assertTrue(quoteData.get("hasCommercialActivity").getAsBoolean());
        assertTrue(quoteData.get("hasOneRiskLocation").getAsBoolean());
        assertFalse(quoteData.get("isCollectiveStipulated").getAsBoolean());
        assertFalse(quoteData.get("includesAssistanceServices").getAsBoolean());
    }

    @Test
    public void testBusinessCreateQuotePatrimonialHomePostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        CreateQuotePatrimonialHomePostRequestBody cond = new CreateQuotePatrimonialHomePostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals(
                "random_cnpj",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );
    }

    @Test
    public void testPersonalCreateQuotePatrimonialCondominiumPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialCondominiumPostRequestBody cond = new CreateQuotePatrimonialCondominiumPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");

        JsonObject insuredObject = quoteData.getAsJsonObject("insuredObject");
        assertEquals(
                "random_cpf",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonObject("insuredObject")
                        .get("identification").getAsString()
        );


        assertTrue(quoteData.get("isLegallyConstituted").getAsBoolean());
        assertTrue(quoteData.get("condominiumType").getAsBoolean());
        assertFalse(quoteData.get("includesAssistanceServices").getAsBoolean());

        assertTrue(insuredObject.get("indenizationWithoutDepreciation").getAsBoolean());
        assertFalse(insuredObject.get("hasElevator").getAsBoolean());
        assertFalse(insuredObject.get("isFullyOrPartiallyListed").getAsBoolean());
        assertFalse(insuredObject.get("hasReuseOfWater").getAsBoolean());
        assertFalse(insuredObject.get("wasThereAClaim").getAsBoolean());
    }

    @Test
    public void testBusinessCreateQuotePatrimonialCondominiumPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        CreateQuotePatrimonialCondominiumPostRequestBody cond = new CreateQuotePatrimonialCondominiumPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals(
                "random_cnpj",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonObject("insuredObject")
                        .get("identification").getAsString()
        );
    }

    @Test
    public void testPersonalCreateQuotePatrimonialBusinessPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialBusinessPostRequestBody cond = new CreateQuotePatrimonialBusinessPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals(
                "random_cpf",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );
    }

    @Test
    public void testBusinessCreateQuotePatrimonialBusinessPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        CreateQuotePatrimonialBusinessPostRequestBody cond = new CreateQuotePatrimonialBusinessPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");

        assertEquals(
                "random_cnpj",
                quoteData.getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );

        assertTrue(quoteData.get("isCollectiveStipulated").getAsBoolean());
        assertTrue(quoteData.get("hasOneRiskLocation").getAsBoolean());
        assertFalse(quoteData.get("includesAssistanceServices").getAsBoolean());
    }

    @Test
    public void testPersonalCreateQuotePatrimonialDiverseRisksPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialDiverseRisksPostRequestBody cond = new CreateQuotePatrimonialDiverseRisksPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals(
                "random_cpf",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );
    }

    @Test
    public void testEditQuoteLeadPostRequestBody() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        EditQuoteLeadPostRequestBody cond = new EditQuoteLeadPostRequestBody();

        run(cond);

        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteCustomer = requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        assertEquals("Different Brand Name", identificationData.get("brandName").getAsString());
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData"));
    }
}
