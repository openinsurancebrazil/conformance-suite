package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class DetermineNumberOfEventsInPcmRequestTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    @UseResurce(value = "pcmRequests/metricEventRequest.json", key = "received_request", path = "body", isAsString = true)
    public void weCanDetermineNumberOfEventsInPcmEventRequest() {
        DetermineNumberOfEventsInPcmRequest cond = new DetermineNumberOfEventsInPcmRequest();
        run(cond);
        Integer numberOfEvents = environment.getInteger("number_of_events");
        Assert.assertNotNull(numberOfEvents);
        Assert.assertEquals(1, (int) numberOfEvents);
    }


    @Test
    @UseResurce(value = "pcmRequests/metricBatchRequest.json", key = "received_request", path = "body", isAsString = true)
    public void weCanDetermineNumberOfEventsInPcmBatchRequest() {
        environment.putString("config", "resource.brazilOrganizationId", "44a8b1fe-d990-47b7-85c4-30522e73a37b");
        DetermineNumberOfEventsInPcmRequest cond = new DetermineNumberOfEventsInPcmRequest();
        run(cond);
        Integer numberOfEvents = environment.getInteger("number_of_events");
        Assert.assertNotNull(numberOfEvents);
        Assert.assertEquals(2, (int) numberOfEvents);
    }


}