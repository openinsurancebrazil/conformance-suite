package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchQuoteCapitalizationTitleOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/PatchQuoteCapitalizationTitleStatusResponseOkV1.10.0.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchQuoteCapitalizationTitleOASValidatorV1n10());
    }
}
