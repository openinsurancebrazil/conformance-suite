package net.openid.conformance.opin.testmodule.support.oasValidators.channels.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetChannelsIntermediaryOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/channels/v1n4/GetChannelsIntermediaryResponseV1n4.json")
    public void testHappyPath() {
        setStatus(200);
        GetChannelsIntermediaryOASValidatorV1n4 cond = new GetChannelsIntermediaryOASValidatorV1n4();
        run(cond);
    }
}