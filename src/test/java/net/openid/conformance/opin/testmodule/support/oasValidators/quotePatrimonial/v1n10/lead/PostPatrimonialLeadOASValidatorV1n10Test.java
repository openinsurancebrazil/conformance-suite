package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.lead;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.lead.PostPatrimonialLeadOASValidatorV1n9;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostPatrimonialLeadOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPostResponseOK.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostPatrimonialLeadOASValidatorV1n10());
    }
}