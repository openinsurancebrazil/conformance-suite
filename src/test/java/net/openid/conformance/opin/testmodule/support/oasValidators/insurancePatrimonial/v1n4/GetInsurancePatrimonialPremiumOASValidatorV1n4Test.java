package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsurancePatrimonialPremiumOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePatrimonial/OpinInsurancePatrimonialPremiumValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsurancePatrimonialPremiumOASValidatorV1n4());
	}

}