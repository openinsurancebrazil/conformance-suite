package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@ApiName("Insurance Acceptance and Branches Abroad PolicyInfo V1")
public class GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceAcceptanceAndBranchesAbroad/OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n3());
	}

}
