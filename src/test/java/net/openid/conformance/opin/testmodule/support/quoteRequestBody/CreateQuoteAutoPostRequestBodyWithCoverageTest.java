package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuoteAutoPostRequestBodyWithCoverageTest extends AbstractJsonResponseConditionUnitTest {


    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");

    }

    @Test
    public void happyPathCpf() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");

        CreateQuoteAutoPostRequestBodyWithCoverage cond = new CreateQuoteAutoPostRequestBodyWithCoverage();
        run(cond);
        JsonObject resourceRequestEntity = environment.getObject("resource_request_entity");
        assertNotNull(resourceRequestEntity);
        JsonObject data = resourceRequestEntity.get("data").getAsJsonObject();
        assertNotNull(data);
        assertNotNull(data.get("consentId"));
        assertNotNull(data.get("expirationDateTime"));
        JsonObject quoteData = data.get("quoteData").getAsJsonObject();
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();
        JsonObject quoteCustomer = data.get("quoteCustomer").getAsJsonObject();

        assertNotNull(quoteData);
        assertNotNull(quoteCustomer);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertNotNull(quoteData.get("termStartDate"));
        assertNotNull(quoteData.get("termType"));
        assertNotNull(quoteData.get("insuranceType"));
        assertNotNull(quoteData.get("termEndDate"));
        assertNotNull(quoteData.get("currency"));
        assertNotNull(quoteData.get("includesAssistanceServices"));
        assertNull(quoteData.get("maxLMG"));
        assertNull(quoteData.get("policyId"));
        assertNull(quoteData.get("insurerId"));

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("CASCO_COMPREENSIVA", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject maxLMI = coverage.getAsJsonObject("maxLMI");
        assertNotNull(maxLMI);
        assertEquals("90.85", maxLMI.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMI.get("unitType").getAsString());

        JsonObject identificationData = quoteCustomer.get("identificationData").getAsJsonObject();

        assertNotNull(identificationData.get("updateDateTime"));
        assertNotNull(identificationData.get("brandName"));
        JsonObject companyInfo = identificationData.get("companyInfo").getAsJsonObject();
        assertNotNull(companyInfo);
        assertNotNull(identificationData.get("civilName"));
        assertNotNull(identificationData.get("hasBrazilianNationality"));
        JsonObject contact = identificationData.get("contact").getAsJsonObject();
        assertNotNull(contact);
        assertNotNull(identificationData.get("cpfNumber"));

        assertNotNull(companyInfo.get("name"));
        assertNotNull(companyInfo.get("cnpjNumber"));


        assertNotNull(contact);
        JsonArray postalAddresses = contact.get("postalAddresses").getAsJsonArray();
        assertNotNull(postalAddresses);
        assertFalse(postalAddresses.isEmpty());
        JsonObject postalAddress = postalAddresses.get(0).getAsJsonObject();
        assertNotNull(postalAddress.get("address"));
        assertNotNull(postalAddress.get("townName"));
        assertNotNull(postalAddress.get("countrySubDivision"));
        assertNotNull(postalAddress.get("postCode"));
        assertNotNull(postalAddress.get("country"));

        JsonObject qualificationData = quoteCustomer.get("qualificationData").getAsJsonObject();

        assertNotNull(qualificationData);

        assertNotNull(qualificationData.get("pepIdentification"));
        assertNotNull(qualificationData.get("updateDateTime"));
        assertNotNull(qualificationData.get("lifePensionPlans"));

        JsonObject complimentaryInformationData = quoteCustomer.get("complimentaryInformationData").getAsJsonObject();

        assertNotNull(complimentaryInformationData);
        assertNotNull(complimentaryInformationData.get("updateDateTime"));
        JsonArray productsServices = complimentaryInformationData.getAsJsonArray("productsServices");
        assertNotNull(productsServices);
        assertFalse(productsServices.isEmpty());

        JsonObject productService = productsServices.get(0).getAsJsonObject();
        assertNotNull(productService.get("contract"));
        assertNotNull(productService.get("type"));

        assertNotNull(complimentaryInformationData.get("startDate"));
    }
}