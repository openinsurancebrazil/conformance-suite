package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.extendedWarranty.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetProductsNServicesExtendedWarrantyOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesExtendedWarrantyResponseV1n3.json")
    public void testHappyPath() {
        setStatus(200);
        GetProductsNServicesExtendedWarrantyOASValidatorV1n3 cond = new GetProductsNServicesExtendedWarrantyOASValidatorV1n3();
        run(cond);
    }
}
