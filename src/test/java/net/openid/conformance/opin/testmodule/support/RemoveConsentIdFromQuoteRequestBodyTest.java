package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class RemoveConsentIdFromQuoteRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    public void testHappyPath() {
        JsonObject data = new JsonObject();
        data.addProperty("consentId", "random_consent_id");

        JsonObject requestData = new JsonObject();
        requestData.add("data", data);
        environment.putObject("resource_request_entity", requestData);

        RemoveConsentIdFromQuoteRequestBody cond = new RemoveConsentIdFromQuoteRequestBody();

        run(cond);

        assertNull(data.get("consentId"));
    }
}
