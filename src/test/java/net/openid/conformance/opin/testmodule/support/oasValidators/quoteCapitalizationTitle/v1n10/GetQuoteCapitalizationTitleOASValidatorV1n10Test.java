package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GetQuoteCapitalizationTitleOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        setStatus(200);
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/GetQuoteCapitalizationTitleStatusResponseOkV1.10.0.json")
    public void testHappyPath() {
        run(new GetQuoteCapitalizationTitleOASValidatorV1n10());
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/GetQuoteCapitalizationTitlePaymentTypeSingleErrorV1.10.0.json")
    public void testPaymentTypeUnicoAndSinglePaymentMissing() {
        ConditionError e = runAndFail(new GetQuoteCapitalizationTitleOASValidatorV1n10());
        assertTrue(e.getMessage().contains("singlePayment is required when paymentType is UNICO"));
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/GetQuoteCapitalizationTitlePaymentTypeMonthlyErrorV1.10.0.json")
    public void testPaymentTypeMensalAndMonthlyPaymentMissing() {
        ConditionError e = runAndFail(new GetQuoteCapitalizationTitleOASValidatorV1n10());
        assertTrue(e.getMessage().contains("monthlyPayment is required when paymentType is MENSAL"));
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/GetQuoteCapitalizationTitleStatusRJCTOk.json")
    public void testStatusRJCT() {
        run(new GetQuoteCapitalizationTitleOASValidatorV1n10());
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/GetQuoteCapitalizationTitleQuoteInfoMissingV1.10.0.json")
    public void testQuoteInfoMissing() {
        ConditionError e = runAndFail(new GetQuoteCapitalizationTitleOASValidatorV1n10());
        assertTrue(e.getMessage().contains("quoteInfo is required when status is ACPT"));
    }
}
