package net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchContractLifePensionOASValidatorV1n12Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/contractLifePension/PatchContractLifePensionOkV1.12.1.json")
    public void happyPathTest() {
        setStatus(200);
        run(new PatchContractLifePensionOASValidatorV1n12());
    }
}