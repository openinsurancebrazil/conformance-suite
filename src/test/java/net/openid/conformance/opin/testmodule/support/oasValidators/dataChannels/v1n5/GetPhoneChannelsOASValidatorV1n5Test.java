package net.openid.conformance.opin.testmodule.support.oasValidators.dataChannels.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


public class GetPhoneChannelsOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/channels/GetPhoneChannelsResponseOkV1.5.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetPhoneChannelsOASValidatorV1n5());
	}
}
