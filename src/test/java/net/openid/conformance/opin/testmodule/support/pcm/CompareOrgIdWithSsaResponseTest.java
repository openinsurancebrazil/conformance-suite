package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class CompareOrgIdWithSsaResponseTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce(value = "support/ssa.json", key = "software_statement_assertion", path = "claims")
    public void weCanCompareOrgIdWithSsaResponse(){
        environment.putString("config", "resource.brazilOrganizationId", "44a8b1fe-d990-47b7-85c4-30522e73a37b");
        CompareOrgIdWithSsaResponse cond = new CompareOrgIdWithSsaResponse();
        run(cond);
    }

    @Test
    @UseResurce(value = "support/ssa.json", key = "software_statement_assertion", path = "claims")
    public void errorIsThrownIfIdsAreDifferent(){
        environment.putString("config", "resource.brazilOrganizationId", "wrongId");
        CompareOrgIdWithSsaResponse cond = new CompareOrgIdWithSsaResponse();
        ConditionError e = runAndFail(cond);
        Assert.assertEquals("CompareOrgIdWithSsaResponse: Org IDs are not equal", e.getMessage());
    }

}