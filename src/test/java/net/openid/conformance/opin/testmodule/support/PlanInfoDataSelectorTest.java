package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlanInfoDataSelectorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfo.json")
    public void weCanExtractTitleIdAndData(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        run(cond);
        String selectedId = environment.getString("selected_id");
        assertEquals("string", selectedId);
    }

    @Test
    public void weCantExtractTitleIdWithoutData(){
        environment.putString("resource_endpoint_response_full", "body", "{}");
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not find data in the body", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoNoData.json")
    public void weCantExtractTitleIdWithEmptyData(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Data array cannot be empty to extract title ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoNoSeries.json")
    public void weCantExtractTitleIdWithoutSeries(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract series array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoEmptySeries.json")
    public void weCantExtractTitleIdWithEmptySeries(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Series array cannot be empty to extract title ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoNoTitles.json")
    public void weCantExtractTitleIdWithoutTitles(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract titles array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoEmptyTitles.json")
    public void weCantExtractTitleIdWithEmptyTitles(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Titles array cannot be empty to extract title ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoNoTechnicalProvisions.json")
    public void weCantExtractPrAmountWithoutTechnicalProvisions(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract technicalProvisions array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoEmptyTechnicalProvisions.json")
    public void weCantExtractPrAmountWithEmptyTechnicalProvisions(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: TechnicalProvisions array cannot be empty to extract prAmount", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoNoPrAmount.json")
    public void weCantExtractPrAmountWithoutPrAmount(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract prAmount object from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlanInfoEmptyPrAmount.json")
    public void weCantExtractPrAmountWithEmptyPrAmount(){
        PlanInfoDataSelector cond = new PlanInfoDataSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: PrAmount object cannot be empty", cond.getClass().getSimpleName()), e.getMessage());
    }

}