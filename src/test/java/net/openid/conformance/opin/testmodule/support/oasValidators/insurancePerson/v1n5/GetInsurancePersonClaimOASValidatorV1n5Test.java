package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsurancePersonClaimOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePerson/OpinInsurancePersonClaimValidatorV1OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsurancePersonClaimOASValidatorV1n5());
	}

}
