package net.openid.conformance.opin.testmodule.support.oasValidators.directorsOfficersLiability.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;



public class GetDirectorsOfficersLiabilityOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/GetDirectorsOfficersLiabilityResponseOkV1.3.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetDirectorsOfficersLiabilityOASValidatorV1n3());
	}
}
