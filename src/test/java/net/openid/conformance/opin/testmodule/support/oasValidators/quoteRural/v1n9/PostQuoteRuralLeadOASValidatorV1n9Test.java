package net.openid.conformance.opin.testmodule.support.oasValidators.quoteRural.v1n9;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteRural.v1n8.PostQuoteRuralLeadOASValidatorV1n8;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostQuoteRuralLeadOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPostResponseOK.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostQuoteRuralLeadOASValidatorV1n9());
    }
}