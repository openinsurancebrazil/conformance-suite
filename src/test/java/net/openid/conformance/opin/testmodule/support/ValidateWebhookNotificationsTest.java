package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

import static org.junit.Assert.assertTrue;

public class ValidateWebhookNotificationsTest extends AbstractJsonResponseConditionUnitTest {
    private JsonObject webhookNotification;

    @Before
    public void setUp() {
        String webhookType = "random_webhook";
        environment.putString("waiting_webhook_since", Instant.now().toString());
        environment.putString("webhook_type", webhookType);

        webhookNotification = new JsonObject();
        webhookNotification.addProperty("type", webhookType);
        webhookNotification.addProperty("received_at", Instant.now().toString());

        JsonArray notifications = new JsonArray();
        notifications.add(webhookNotification);

        JsonObject webhook = new JsonObject();
        webhook.add("notifications", notifications);
        environment.putObject("webhook", webhook);
    }

    @Test
    public void testHappyPath() {

        ValidateWebhookNotifications cond = new ValidateWebhookNotifications();

        run(cond);
    }

    @Test
    public void testNoWebhooks() {

        environment.removeObject("webhook");
        ValidateWebhookNotifications cond = new ValidateWebhookNotifications();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("No webhook received"));
    }

    @Test
    public void testReceivedMoreThanExpected() {

        ValidateWebhookNotifications cond = new ValidateWebhookNotifications();
        environment.putInteger("expected_number_of_webhooks", 0);

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Invalid number of webhook notifications received"));
    }

    @Test
    public void testInvalidWebhookType() {

        ValidateWebhookNotifications cond = new ValidateWebhookNotifications();
        webhookNotification.addProperty("type", "invalid");

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Invalid webhook type received"));
    }

    @Test
    public void testNotificationReceivedBeforeStartWaiting() {

        ValidateWebhookNotifications cond = new ValidateWebhookNotifications();
        webhookNotification.addProperty("received_at", Instant.now().minus(60, ChronoUnit.SECONDS).toString());

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Webhook arrived before waiting for it"));
    }
}
