package net.openid.conformance.opin.testmodule.support.oasValidators.rentGuarantee.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetRentGuaranteeOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/GetRentGuaranteeResponseOkV1.3.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetRentGuaranteeOASValidatorV1n3());
	}
}
