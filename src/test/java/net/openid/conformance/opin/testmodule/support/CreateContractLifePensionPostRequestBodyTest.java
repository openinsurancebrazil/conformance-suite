package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateContractLifePensionPostRequestBody;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateContractLifePensionPostRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateContractLifePensionPostRequestBody cond = new CreateContractLifePensionPostRequestBody();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData"));

        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonArray("products"));
        assertEquals(1, requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonArray("products").size());
        JsonObject product = requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonArray("products").get(0).getAsJsonObject();
        assertFalse(product.get("isContributeMonthly").getAsBoolean());

        JsonObject complementaryIdentification = requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("complementaryIdentification");
        assertFalse(complementaryIdentification.get("isNewPlanHolder").getAsBoolean());

        JsonObject investorProfile = requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("investorProfile");
        assertFalse(investorProfile.get("isQualifiedInvestor").getAsBoolean());

        assertFalse(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("pensionRiskCoverage").getAsBoolean());
        assertTrue(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("isPortabilityHiringQuote").getAsBoolean());
    }
}