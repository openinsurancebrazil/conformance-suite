package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrepareUrlForFetchingEndpointTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    public void weCanPrepareEndpointUrl(){
        String planId = "test_id";
        String api = "insurance-capitalization-title";
        String endpoint = "test_endpoint";

        environment.putString("selected_id", planId);
        environment.putString("api", api);
        environment.putString("endpoint", endpoint);
        environment.putString("config", "resource.consentUrl", "https://test.com/open-insurance/consents/v2/consents");
        PrepareUrlForFetchingEndpoint cond = new PrepareUrlForFetchingEndpoint();
        run(cond);
        String protectedResourceUrl = environment.getString("protected_resource_url");
        assertEquals(String.format("https://test.com/open-insurance/%s/v1/%s/%s/%s", api, api, planId, endpoint), protectedResourceUrl);
    }

}