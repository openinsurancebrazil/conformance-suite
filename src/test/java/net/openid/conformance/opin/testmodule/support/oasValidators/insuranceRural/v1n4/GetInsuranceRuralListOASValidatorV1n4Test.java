package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceRuralListOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/rural/v1n4/ruralResponseStructure.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceRuralListOASValidatorV1n4());
	}

}
