package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.AbstractValidateField;
import net.openid.conformance.opin.testmodule.support.validateField.ValidateConsentsFieldV2;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

public class ValidateConsentsFieldV2Test extends AbstractJsonResponseConditionUnitTest {

    protected static final String CONSENT_V2 = "https://www.example.com/consents/v2/consents";
    protected static final String CPF = "12345678901";
    protected static final String CNPJ = "12345678901234";

    @Test
    public void happyPathConsentsV2PersonalTest() {
        buildConfigForTest(CONSENT_V2, null, "personal", CPF, null, false);
        happyPathTest(new ValidateConsentsFieldV2());
    }

    @Test
    public void happyPathConsentsV2BusinessTest() {
        buildConfigForTest(CONSENT_V2, null, "business", CPF, CNPJ, false);
        happyPathTest(new ValidateConsentsFieldV2());
    }

    protected void happyPathTest(AbstractValidateField validateField) {
        run(validateField);
    }

    protected void buildConfigForTest(String consentUrl, String enrollmentUrl, String productType, String brazilCpf, String brazilCnpj, boolean isOperational) {
        JsonObjectBuilder configBuilder = new JsonObjectBuilder();

        addFieldIfNotNull(configBuilder, "resource.consentUrl", consentUrl);
        addFieldIfNotNull(configBuilder, "resource.enrollmentsUrl", enrollmentUrl);
        addFieldIfNotNull(configBuilder, "consent.productType", productType);

        if (isOperational) {
            addFieldIfNotNull(configBuilder, "resource.brazilCpfOperational", brazilCpf);
            addFieldIfNotNull(configBuilder, "resource.brazilCnpjOperationalBusiness", brazilCnpj);
        } else {
            addFieldIfNotNull(configBuilder, "resource.brazilCpf", brazilCpf);
            addFieldIfNotNull(configBuilder, "resource.brazilCnpj", brazilCnpj);
        }

        environment.putObject("config", configBuilder.build());
    }

    private void addFieldIfNotNull(JsonObjectBuilder builder, String key, String value) {
        if (value != null) {
            builder.addField(key, value);
        }
    }
}
