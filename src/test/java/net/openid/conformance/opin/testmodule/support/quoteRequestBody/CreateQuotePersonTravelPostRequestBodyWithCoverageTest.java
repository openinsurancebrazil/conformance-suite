package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

public class CreateQuotePersonTravelPostRequestBodyWithCoverageTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuotePatrimonialLeadHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePersonTravelPostRequestBodyWithCoverage cond = new CreateQuotePersonTravelPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject data = requestBody.getAsJsonObject("data");
        JsonObject quoteData = data.getAsJsonObject("quoteData");
        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();
        JsonObject quoteCustomer = data.get("quoteCustomer").getAsJsonObject();
        assertEquals(environment.getString("consent_id"), data.get("consentId").getAsString());
        assertNotNull(quoteData);
        assertNotNull(quoteCustomer);
        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("random_cpf", quoteCustomer.getAsJsonObject("identificationData").get("cpfNumber").getAsString());

        assertNotNull(quoteData.get("termStartDate"));
        assertNotNull(quoteData.get("termEndDate"));
        assertEquals("LAZER", quoteData.get("travelType").getAsString());
        assertFalse(quoteData.get("includeAssistanceServices").getAsBoolean());

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("MORTE", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());
    }
}
