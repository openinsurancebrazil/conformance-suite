package net.openid.conformance.opin.testmodule.support.oasValidators.channels.referencedNetwork.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetChannelsReferencedNetworkOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/channels/referencedNetwork/v1n4/GetChannelsReferencedNetworkResponseV1n4.json")
    public void testHappyPath() {
        setStatus(200);
        GetChannelsReferencedNetworkOASValidatorV1n4 cond = new GetChannelsReferencedNetworkOASValidatorV1n4();
        run(cond);
    }
}
