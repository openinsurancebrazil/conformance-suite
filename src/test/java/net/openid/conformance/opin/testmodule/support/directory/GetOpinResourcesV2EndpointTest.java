package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServerTest;

public class GetOpinResourcesV2EndpointTest  extends AbstractGetXFromAuthServerTest {

    @Override
    protected String getApiFamilyType() {
        return "resources";
    }

    @Override
    protected String getApiVersion() {
        return "2.0.0";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

    @Override
    protected String getEndpoint() {
        return "https://test.com/open-insurance/resources/v2/resources";
    }

    @Override
    protected AbstractGetXFromAuthServer getCondition() {
        return new GetOpinResourcesV2Endpoint();
    }
}