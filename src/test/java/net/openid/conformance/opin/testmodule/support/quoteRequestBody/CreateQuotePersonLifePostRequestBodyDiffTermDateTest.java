package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuotePersonLifePostRequestBodyDiffTermDateTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testPersonalQuoteHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePersonLifePostRequestBodyDiffTermDate cond = new CreateQuotePersonLifePostRequestBodyDiffTermDate();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData"));
        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("termStartDate"));
        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("termEndDate"));
        assertEquals("ANUAL", requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("termType").getAsString());
        assertFalse(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("includeAssistanceServices").getAsBoolean());
        assertTrue(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("termStartDate").getAsString().compareTo(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("termEndDate").getAsString()) > 0);
    }
}
