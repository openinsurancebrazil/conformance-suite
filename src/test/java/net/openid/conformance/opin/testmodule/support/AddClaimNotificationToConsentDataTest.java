package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AddClaimNotificationToConsentDataTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        JsonObject data = new JsonObject();
        JsonObject consentRequest = new JsonObject();
        consentRequest.add("data", data);
        environment.putObject("consent_endpoint_request", consentRequest);
        environment.putString("policyId", "random_policy_id");
        environment.putString("claim_notification_endpoint_type", "person");
    }

    @Test
    public void test() {
        AddClaimNotificationToConsentData cond = new AddClaimNotificationToConsentData();

        run(cond);
        JsonObject claimInfo = environment.getObject("consent_endpoint_request").getAsJsonObject("data").getAsJsonObject("claimNotificationInformation");
        assertEquals("random_policy_id", claimInfo.get("policyNumber").getAsString());
        assertNull(claimInfo.get("proposalId"));
    }

    @Test
    public void testDocumentTypeCertificado() {

        AddClaimNotificationToConsentData cond = new AddClaimNotificationToConsentData();

        environment.putString("documentType", "CERTIFICADO");
        run(cond);

        JsonObject claimInfo = environment.getObject("consent_endpoint_request").getAsJsonObject("data").getAsJsonObject("claimNotificationInformation");
        assertEquals("random_policy_id", claimInfo.get("groupCertificateId").getAsString());

        environment.putString("documentType", "CERTIFICADO_AUTOMOVEL");
        run(cond);

        claimInfo = environment.getObject("consent_endpoint_request").getAsJsonObject("data").getAsJsonObject("claimNotificationInformation");
        assertEquals("random_policy_id", claimInfo.get("groupCertificateId").getAsString());
    }

    @Test
    public void testDiffPolicyFieldName() {
        AddClaimNotificationToConsentDataDiff cond = new AddClaimNotificationToConsentDataDiff();

        run(cond);
        JsonObject claimInfo = environment.getObject("consent_endpoint_request").getAsJsonObject("data").getAsJsonObject("claimNotificationInformation");
        assertEquals("random_policy_id", claimInfo.get("policyId").getAsString());
        assertEquals("216731531723", claimInfo.getAsJsonArray("insuredObjectId").get(0).getAsString());
        assertEquals("987",claimInfo.get("proposalId").getAsString());
    }

    @Test
    public void testDiffProposalId() {

        environment.putString("claim_notification_endpoint_type", "damages");

        AddClaimNotificationToConsentDataDiff cond = new AddClaimNotificationToConsentDataDiff();

        run(cond);
        JsonObject claimInfo = environment.getObject("consent_endpoint_request").getAsJsonObject("data").getAsJsonObject("claimNotificationInformation");
        assertEquals("random_policy_id", claimInfo.get("policyId").getAsString());
        assertEquals("216731531723", claimInfo.getAsJsonArray("insuredObjectId").get(0).getAsString());
        assertNull(claimInfo.get("proposalId"));
    }
}
