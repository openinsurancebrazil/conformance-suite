package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class CompareClientIdWithSsaResponseTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce(value = "support/ssa.json", key = "software_statement_assertion", path = "claims")
    public void weCanCompareClientIdWithSsaResponse(){
        environment.putString("config", "directory.client_id", "2ea25ceb-6ea2-4fe0-9d81-494120e9226e");
        CompareClientIdWithSsaResponse cond = new CompareClientIdWithSsaResponse();
        run(cond);
    }

    @Test
    @UseResurce(value = "support/ssa.json", key = "software_statement_assertion", path = "claims")
    public void errorIsThrownIfIdsAreDifferent(){
        environment.putString("config", "directory.client_id", "wrongId");
        CompareClientIdWithSsaResponse cond = new CompareClientIdWithSsaResponse();
        ConditionError e = runAndFail(cond);
        Assert.assertEquals("CompareClientIdWithSsaResponse: Software Statement IDs are not equal", e.getMessage());
    }

}