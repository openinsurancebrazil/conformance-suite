package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServerTest;


public class GetHousingV1EndpointTest  extends AbstractGetXFromAuthServerTest {

    @Override
    protected String getApiFamilyType() {
        return "housing";
    }

    @Override
    protected String getApiVersion() {
        return "1.0.0";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

    @Override
    protected String getEndpoint() {
        return "https://test.com/open-insurance/insurance-housing/v1/insurance-housing";
    }

    @Override
    protected AbstractGetXFromAuthServer getCondition() {
        return new GetHousingV1Endpoint();
    }
}