package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.AbstractCondition;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AddWithdrawalPensionScopeTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putObject("client", new JsonObject());
    }

    @Test
    public void testScopeIsAdded() {
        AbstractCondition cond = new AddWithdrawalPensionScope();
        run(cond);

        String scope = environment.getString("client", "scope");
        assertNotNull(scope);
        assertTrue(scope.contains("withdrawal-pension"));
    }
}
