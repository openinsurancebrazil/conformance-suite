package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;

public class AddResourcesScopeTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putObject("client", new JsonObject());
    }

    @Test
    public void testHappyPath() {
        AddResourcesScope cond = new AddResourcesScope();

        run(cond);

        assertThat(environment.getString("client", "scope"), containsString("resources"));
    }
}