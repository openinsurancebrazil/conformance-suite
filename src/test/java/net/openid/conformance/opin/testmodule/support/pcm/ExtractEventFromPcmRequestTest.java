package net.openid.conformance.opin.testmodule.support.pcm;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class ExtractEventFromPcmRequestTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce(value = "pcmRequests/metricEventRequest.json", key = "received_request", path = "body", isAsString = true)
    @UseResurce(value = "pcmRequests/metricEventRequest.json", key = "expected_event")
    public void weCanExtractEventFromPcmEventRequest() {
        ExtractEventFromPcmRequest cond = new ExtractEventFromPcmRequest();
        run(cond);
        JsonObject extractedEvent = environment.getObject("pcm_event");
        JsonObject expectedEvent = environment.getObject("expected_event");
        Assert.assertEquals(expectedEvent, extractedEvent);
    }


    @Test
    @UseResurce(value = "pcmRequests/metricBatchRequest.json", key = "received_request", path = "body", isAsString = true)
    @UseResurce(value = "pcmRequests/expectedBatchEvent1.json", key = "expected_event")
    public void weCanExtractEventFromPcmBatchRequest1() {
        environment.putInteger("event_number", 0);
        ExtractEventFromPcmRequest cond = new ExtractEventFromPcmRequest();
        run(cond);
        JsonObject extractedEvent = environment.getObject("pcm_event");
        JsonObject expectedEvent = environment.getObject("expected_event");
        Assert.assertEquals(expectedEvent, extractedEvent);
    }

    @Test
    @UseResurce(value = "pcmRequests/metricBatchRequest.json", key = "received_request", path = "body", isAsString = true)
    @UseResurce(value = "pcmRequests/expectedBatchEvent2.json", key = "expected_event")
    public void weCanExtractEventFromPcmBatchRequest2() {
        environment.putInteger("event_number", 1);
        ExtractEventFromPcmRequest cond = new ExtractEventFromPcmRequest();
        run(cond);
        JsonObject extractedEvent = environment.getObject("pcm_event");
        JsonObject expectedEvent = environment.getObject("expected_event");
        Assert.assertEquals(expectedEvent, extractedEvent);
    }

}