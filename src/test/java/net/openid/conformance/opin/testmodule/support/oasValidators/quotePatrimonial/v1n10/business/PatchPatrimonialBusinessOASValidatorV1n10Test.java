package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.business;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchPatrimonialBusinessOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchPatrimonialBusinessOASValidatorV1n10());
    }
}