package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CheckOpinDirectoryDiscoveryUrlTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putObject("config", new JsonObject());
    }

    @Test
    public void testHappyPath() {
        CheckOpinDirectoryDiscoveryUrl cond = new CheckOpinDirectoryDiscoveryUrl();

        environment.putString("config", "directory.discoveryUrl", "https://auth.sandbox.directory.opinbrasil.com.br/.well-known/openid-configuration");
        run(cond);

        environment.putString("config", "directory.discoveryUrl", "https://auth.directory.opinbrasil.com.br/.well-known/openid-configuration");
        run(cond);
    }

    @Test
    public void testUnhappyPath() {
        environment.putString("config", "directory.discoveryUrl", "https://invalid.com/.well-known/openid-configuration");
        CheckOpinDirectoryDiscoveryUrl cond = new CheckOpinDirectoryDiscoveryUrl();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Testing for Brazil certification must be done using the Brazil directory"));
    }
}
