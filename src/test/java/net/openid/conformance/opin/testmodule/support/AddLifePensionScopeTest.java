package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.AbstractCondition;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AddLifePensionScopeTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putObject("client", new JsonObject());
    }

    @Test
    public void test() {
        AddLifePensionScope cond = new AddLifePensionScope();
        run(cond);
        assertTrue(environment.getString("client", "scope").contains("insurance-life-pension"));
    }
}
