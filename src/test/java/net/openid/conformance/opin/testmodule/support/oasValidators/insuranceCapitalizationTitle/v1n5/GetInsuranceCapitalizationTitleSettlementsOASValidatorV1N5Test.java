package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


public class GetInsuranceCapitalizationTitleSettlementsOASValidatorV1N5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceCapitalizationTitle/OpinInsuranceCapitalizationTitleSettlementsValidatorV1.5.0.OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n5());
	}

}