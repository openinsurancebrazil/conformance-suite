package net.openid.conformance.opin.testmodule.support;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EnumOpinResourcesTypeTest {
    @Test
    public void dummyTest() {
        for(EnumOpinResourcesType type : EnumOpinResourcesType.values()) {
            assertEquals(type, EnumOpinResourcesType.valueOf(type.name()));
        }
    }
}
