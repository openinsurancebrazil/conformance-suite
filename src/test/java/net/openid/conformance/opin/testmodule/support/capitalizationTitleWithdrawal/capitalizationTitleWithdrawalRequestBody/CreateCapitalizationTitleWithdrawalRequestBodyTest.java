package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawal.capitalizationTitleWithdrawalRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.CreateCapitalizationTitleWithdrawalMockPostRequestBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.CreateCapitalizationTitleWithdrawalCompromissosPessoaisEmergenciaisPostRequestBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.CreateCapitalizationTitleWithdrawalPostRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteCapitalizationTitleRaffleRequestBody.CreateQuoteCapitalizationTitleRafflePostRequestBody;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateCapitalizationTitleWithdrawalRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putObject("config", new JsonObject());

        environment.putString("planId", "plan_id");

        JsonObject product = new JsonObject();
        product.addProperty("productName", "product_Name");
        product.addProperty("planId", "plan_id");
        environment.putObject("product_1", product);

        JsonObject prAmount = new JsonObject();
        prAmount.addProperty("amount", 2000);
        JsonObject technicalProvision = new JsonObject();
        technicalProvision.add("prAmount", prAmount);
        JsonArray technicalProvisions = new JsonArray();
        technicalProvisions.add(technicalProvision);

        JsonObject title = new JsonObject();
        title.addProperty("titleId","title_id");
        title.addProperty("termEndDate","2025-01-01T08:30:00Z");
        title.add("technicalProvisions",technicalProvisions);
        JsonArray titles = new JsonArray();
        titles.add(title);

        JsonObject series = new JsonObject();
        series.addProperty("seriesId", "series_id");
        series.addProperty("modality", "1");
        series.addProperty("susepProcessNumber", "999");
        series.add("titles", titles);
        environment.putObject("series_1", series);

        environment.putString("config", "resource.brazilCpf", "NA");
    }

    @Test
    public void testCapitalizationTitleWithdrawal() {
        CreateCapitalizationTitleWithdrawalPostRequestBody cond = new CreateCapitalizationTitleWithdrawalPostRequestBody();
        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        assertEquals("1", environment.getString("resource_request_entity", "data.modality"));
        assertEquals("999", environment.getString("resource_request_entity", "data.susepProcessNumber"));
    }

    @Test
    public void testCapitalizationTitleWithdrawalCompromissosPessoaisEmergenciais() {
        CreateCapitalizationTitleWithdrawalCompromissosPessoaisEmergenciaisPostRequestBody cond = new CreateCapitalizationTitleWithdrawalCompromissosPessoaisEmergenciaisPostRequestBody();
        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        assertEquals("1", environment.getString("resource_request_entity", "data.modality"));
        assertEquals("999", environment.getString("resource_request_entity", "data.susepProcessNumber"));
    }


    @Test
    public void testCapitalizationTitleWithdrawalMock() {
        CreateCapitalizationTitleWithdrawalMockPostRequestBody cond = new CreateCapitalizationTitleWithdrawalMockPostRequestBody();
        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        assertEquals("TRADICIONAL", environment.getString("resource_request_entity", "data.modality"));
        assertEquals("54321", environment.getString("resource_request_entity", "data.susepProcessNumber"));
    }

    @Test
    public void testCreateQuoteCapitalizationTitleRafflePostRequestBody(){
        CreateQuoteCapitalizationTitleRafflePostRequestBody cond = new CreateQuoteCapitalizationTitleRafflePostRequestBody();
        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        assertEquals("1", environment.getString("resource_request_entity", "data.modality"));
        assertEquals("999", environment.getString("resource_request_entity", "data.susepProcessNumber"));
        assertEquals("NA", environment.getString("resource_request_entity", "data.cpfNumber"));
        assertEquals("EMAIL", environment.getString("resource_request_entity", "data.contactType"));
        assertEquals("contact@email.com", environment.getString("resource_request_entity", "data.email"));
    }
}
