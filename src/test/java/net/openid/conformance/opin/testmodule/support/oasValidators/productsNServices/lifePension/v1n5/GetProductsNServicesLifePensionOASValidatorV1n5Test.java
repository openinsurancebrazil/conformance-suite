package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.lifePension.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetProductsNServicesLifePensionOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesLifePensionResponseV1n5.json")
    public void testHappyPath() {
        setStatus(200);
        GetProductsNServicesLifePensionOASValidatorV1n5 cond = new GetProductsNServicesLifePensionOASValidatorV1n5();
        run(cond);
    }

    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesLifePensionResponseV1n5BadNoBiometric.json")
    public void unhappyPathNoBiometricTableBad() {
        setStatus(200);
        ConditionError e = runAndFail(new GetProductsNServicesLifePensionOASValidatorV1n5());
        assertThat(e.getMessage(), containsString("biometricTable is required when"));
    }

    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesLifePensionResponseV1n5GoodNoBiometric.json")
    public void unhappyPathNoBiometricTableGood() {
        setStatus(200);
        GetProductsNServicesLifePensionOASValidatorV1n5 cond = new GetProductsNServicesLifePensionOASValidatorV1n5();
        run(cond);
    }

    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesLifePensionResponseV1n5BiometricNonmandatory.json")
    public void unhappyPathBiometricTableNonmandatory() {
        setStatus(200);
        GetProductsNServicesLifePensionOASValidatorV1n5 cond = new GetProductsNServicesLifePensionOASValidatorV1n5();
        run(cond);
    }
}