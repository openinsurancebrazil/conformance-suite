package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class BuildInsuranceCapitalizationTitleUrlFromConsentUrlTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    public void weCanBuildCapitalizationTitleUrlFromConsentUrl(){
        String api = "insurance-capitalization-title";
        String endpoint = "insurance-capitalization-title/plans";

        environment.putString("config", "resource.consentUrl", "https://test.com/open-insurance/consents/v2/consents");
        BuildInsuranceCapitalizationTitleUrlFromConsentUrl cond = new BuildInsuranceCapitalizationTitleUrlFromConsentUrl();
        run(cond);
        String protectedResourceUrl = environment.getString("protected_resource_url");
        assertEquals(String.format("https://test.com/open-insurance/%s/v1/%s", api, endpoint), protectedResourceUrl);
    }

}