package net.openid.conformance.opin.testmodule.support.capitalizationTile.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitle.v1n3.GetCapitalizationTitleOASValidatorV1n3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class CapitalizationTitleOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitleResponseOkV1.3.0.json")
	public void happyPathTest() {
		setStatus(200);
		run(new GetCapitalizationTitleOASValidatorV1n3());
	}
}
