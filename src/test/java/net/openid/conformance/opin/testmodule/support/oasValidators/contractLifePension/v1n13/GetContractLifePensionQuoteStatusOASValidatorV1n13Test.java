package net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetContractLifePensionQuoteStatusOASValidatorV1n13Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/contractLifePension/GetContractLifePensionOkV1.13.0.json")
    public void happyPathTest() {
        setStatus(200);
        run(new GetContractLifePensionQuoteStatusOASValidatorV1n13());
    }
}