package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.condominium;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPatrimonialCondominiumQuoteStatusOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/condominium/GetPatrimonialCondominiumQuoteStatusOkV1.10.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPatrimonialCondominiumQuoteStatusOASValidatorV1n10());
    }
}