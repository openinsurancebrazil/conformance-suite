package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CheckPhase2PermissionGroupsToBeTestedTest extends AbstractJsonResponseConditionUnitTest {

    private void setConfig(String productType) {
        environment.putObject("config", new JsonObjectBuilder().addField("consent.productType", productType).build());
    }

    @Test
    @UseResurce(value = "support/authorisationServerResponse.json", key = "authorisation_server")
    public void happyPathTestCustomerPersonal() {
        setConfig("personal");
        run(new CheckPhase2PermissionGroupsToBeTested());
        assertEquals(expectedResult("personal"), getResult());
    }

    @Test
    @UseResurce(value = "support/authorisationServerResponse.json", key = "authorisation_server")
    public void happyPathTestCustomerBusiness() {
        setConfig("business");
        run(new CheckPhase2PermissionGroupsToBeTested());
        assertEquals(expectedResult("business"), getResult());
    }

    @Test(expected = AssertionError.class)
    public void unhappyPathTestMissingAuthorisationServer() {
        setConfig("personal");
        run(new CheckPhase2PermissionGroupsToBeTested());
    }

    @Test(expected = AssertionError.class)
    @UseResurce(value = "support/authorisationServerResponse.json", key = "authorisation_server")
    public void unhappyPathTestMissingConfig() {
        run(new CheckPhase2PermissionGroupsToBeTested());
    }

    @Test
    @UseResurce(value = "support/authorisationServerResponse.json", key = "authorisation_server")
    public void unhappyPathTestMissingConsent() {
        environment.putObject("config", new JsonObject());
        unhappyPathTest("The consent.productType has not been set in the config");
    }

    @Test
    @UseResurce(value = "support/authorisationServerResponse.json", key = "authorisation_server")
    public void unhappyPathTestMissingProductType() {
        environment.putObject("config", "consent", new JsonObject());
        unhappyPathTest("The consent.productType has not been set in the config");
    }

    @Test
    @UseResurce(value = "support/authorisationServerResponseMissingApiResources.json", key = "authorisation_server")
    public void unhappyPathTestMissingApiResources() {
        setConfig("personal");
        unhappyPathTest("There is no \"ApiResources\" inside authorisation server response");
    }

    @Test
    @UseResurce(value = "support/authorisationServerResponseMissingApiFamilyType.json", key = "authorisation_server")
    public void unhappyPathTestMissingApiFamilyType() {
        setConfig("personal");
        unhappyPathTest("There is no \"ApiFamilyType\" inside api resource");
    }

    private void unhappyPathTest(String message) {
        ConditionError error = runAndFail(new CheckPhase2PermissionGroupsToBeTested());
        assertTrue(error.getMessage().contains(message));
    }

    private List<String> getResult() {
        JsonArray permissionGroups = environment.getObject("permission_groups").getAsJsonArray("permissionGroups");
        List<String> result = new ArrayList<>();
        for (JsonElement permissionsGroup : permissionGroups) {
            result.add(OIDFJSON.getString(permissionsGroup));
        }
        result.sort(String::compareTo);
        return result;
    }

    private List<String> expectedResult(String productType) {
        List<String> permissionGroups = new ArrayList<>();
        permissionGroups.add("CAPITALIZATION_TITLE");
        permissionGroups.add("CUSTOMERS_" + productType.toUpperCase());
        permissionGroups.add("DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD");
        permissionGroups.add("DAMAGES_AND_PEOPLE_AUTO");
        permissionGroups.add("DAMAGES_AND_PEOPLE_FINANCIAL_RISKS");
        permissionGroups.add("DAMAGES_AND_PEOPLE_HOUSING");
        permissionGroups.add("DAMAGES_AND_PEOPLE_PATRIMONIAL");
        permissionGroups.add("DAMAGES_AND_PEOPLE_PERSON");
        permissionGroups.add("DAMAGES_AND_PEOPLE_RESPONSIBILITY");
        permissionGroups.add("DAMAGES_AND_PEOPLE_RURAL");
        permissionGroups.add("DAMAGES_AND_PEOPLE_TRANSPORT");
        permissionGroups.add("FINANCIAL_ASSISTANCE");
        permissionGroups.add("LIFE_PENSION");
        permissionGroups.add("PENSION_PLAN");
        return permissionGroups;
    }
}
