package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AllIdsSelectorFromJsonPathTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void testElementsExist() {
        environment.putString("selected_id_json_path", "$.data[*].brand.companies[*].products[*].planId");
        AllIdsSelectorFromJsonPath cond = new AllIdsSelectorFromJsonPath();

        run(cond);
        JsonArray selectedIds = JsonParser.parseString(environment.getString("selected_id")).getAsJsonArray();

        assertEquals(3, selectedIds.size());
        assertTrue(selectedIds.contains(new JsonPrimitive("test_plan_id")));
        assertTrue(selectedIds.contains(new JsonPrimitive("test_plan_id2")));
        assertTrue(selectedIds.contains(new JsonPrimitive("test_plan_id3")));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void testElementsDontExist() {
        environment.putString("selected_id_json_path", "$.data[*].brand.companies[*].invalid[*].planId");
        AllIdsSelectorFromJsonPath cond = new AllIdsSelectorFromJsonPath();

        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("No ID was found for json path"));
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void testInvalidPath() {
        environment.putString("selected_id_json_path", "$.invalid.brand.companies[*].invalid[*].planId");
        AllIdsSelectorFromJsonPath cond = new AllIdsSelectorFromJsonPath();

        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("json object does not correspond to the json path"));
    }
}
