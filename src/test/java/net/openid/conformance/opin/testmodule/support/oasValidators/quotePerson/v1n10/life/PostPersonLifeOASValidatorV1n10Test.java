package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostPersonLifeOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/PostPersonLifeOkV1.10.2.json")
    public void happyPathTest() {
        setStatus(201);
        run(new PostPersonLifeOASValidatorV1n10());
    }
}