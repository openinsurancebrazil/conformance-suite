package net.openid.conformance.opin.testmodule.support.oasValidators.person.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.person.v1n6.GetPersonOASValidatorV1n6;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPersonOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/phase1Update/GetPersonResponseOkV1.5.1.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetPersonOASValidatorV1n5());
	}
}
