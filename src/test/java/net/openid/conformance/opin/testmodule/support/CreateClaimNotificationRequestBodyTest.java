package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateClaimNotificationRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    public void testHappyPathPerson() {
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        environment.putString("policyId", "random_policy_id");
        environment.putString("documentType", "APOLICE_INDIVIDUAL");
        environment.putString("claim_notification_endpoint_type", "person");
        CreateClaimNotificationRequestBody cond = new CreateClaimNotificationRequestBody();

        run(cond);

        JsonObject requestData = environment.getObject("resource_request_entity").getAsJsonObject("data");
        assertEquals("random_policy_id", requestData.get("policyNumber").getAsString());
        assertNotNull(requestData.get("claimant"));
        assertEquals(requestData.getAsJsonObject("requestor").get("documentNumber").getAsString(), "random_cpf");
        assertEquals(requestData.getAsJsonObject("requestor").get("documentType").getAsString(), "CPF");
    }

    @Test
    public void testHappyPathBusinessPerson() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        environment.putString("policyId", "random_policy_id");
        environment.putString("documentType", "APOLICE_INDIVIDUAL");
        environment.putString("claim_notification_endpoint_type", "person");
        CreateClaimNotificationRequestBody cond = new CreateClaimNotificationRequestBody();

        run(cond);

        JsonObject requestData = environment.getObject("resource_request_entity").getAsJsonObject("data");
        assertEquals("random_policy_id", requestData.get("policyNumber").getAsString());
        assertNotNull(requestData.get("claimant"));
        assertEquals(requestData.getAsJsonObject("requestor").get("documentNumber").getAsString(), "random_cnpj");
        assertEquals(requestData.getAsJsonObject("requestor").get("documentType").getAsString(), "CNPJ");
    }

    @Test
    public void testHappyPathDamages() {
        environment.putString("policyId", "random_policy_id");
        environment.putString("documentType", "APOLICE_INDIVIDUAL");
        environment.putString("claim_notification_endpoint_type", "damages");
        CreateClaimNotificationRequestBody cond = new CreateClaimNotificationRequestBody();

        run(cond);

        JsonObject requestData = environment.getObject("resource_request_entity").getAsJsonObject("data");
        assertEquals("random_policy_id", requestData.get("policyNumber").getAsString());
    }

    @Test
    public void testHappyPathCertificate() {
        environment.putString("policyId", "random_policy_id");
        environment.putString("documentType", "CERTIFICADO");
        environment.putString("claim_notification_endpoint_type", "damages");
        CreateClaimNotificationRequestBody cond = new CreateClaimNotificationRequestBody();

        run(cond);

        JsonObject requestData = environment.getObject("resource_request_entity").getAsJsonObject("data");
        assertEquals("random_policy_id", requestData.get("groupCertificateId").getAsString());
    }

    @Test
    public void testHappyPathDiff() {
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        environment.putString("policyId", "random_policy_id");
        environment.putString("documentType", "APOLICE_INDIVIDUAL");
        environment.putString("claim_notification_endpoint_type", "person");

        CreateClaimNotificationRequestBodyDiff cond = new CreateClaimNotificationRequestBodyDiff();

        run(cond);

        JsonObject requestData = environment.getObject("resource_request_entity").getAsJsonObject("data");
        assertEquals("random_policy_id", requestData.get("policyId").getAsString());
        assertNotNull(requestData.get("claimant"));
        assertEquals(requestData.getAsJsonObject("requestor").get("documentNumber").getAsString(), "random_cpf");
        assertEquals(requestData.getAsJsonObject("requestor").get("documentType").getAsString(), "CPF");
        assertEquals("216731531723", requestData.getAsJsonArray("insuredObjectId").get(0).getAsString());
        assertEquals("987", requestData.get("proposalId").getAsString());
    }

    @Test
    public void testUnHappyPathProposalId() {
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        environment.putString("policyId", "random_policy_id");
        environment.putString("documentType", "APOLICE_INDIVIDUAL");
        environment.putString("claim_notification_endpoint_type", "damages");

        CreateClaimNotificationRequestBodyDiff cond = new CreateClaimNotificationRequestBodyDiff();

        run(cond);

        JsonObject requestData = environment.getObject("resource_request_entity").getAsJsonObject("data");
        assertEquals("random_policy_id", requestData.get("policyId").getAsString());
        assertEquals("216731531723", requestData.getAsJsonArray("insuredObjectId").get(0).getAsString());
        assertNull(requestData.get("proposalId"));
    }
}
