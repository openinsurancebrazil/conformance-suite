package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.condominium;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPatrimonialCondominiumQuoteStatusOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/condominium/GetPatrimonialCondominiumQuoteStatusOkV1.9.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPatrimonialCondominiumQuoteStatusOASValidatorV1n9());
    }
}