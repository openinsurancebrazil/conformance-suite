package net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.lead;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostContractLifePensionLeadOASValidatorV1n12Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/contractLifePension/PostContractLifePensionLeadOkV1.12.1.json")
    public void happyPathTest() {
        setStatus(201);
        run(new PostContractLifePensionLeadOASValidatorV1n12());
    }
}