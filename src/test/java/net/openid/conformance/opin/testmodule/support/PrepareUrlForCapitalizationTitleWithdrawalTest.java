package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrepareUrlForCapitalizationTitleWithdrawalTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    public void weCanPrepareEndpointUrl(){
        String api = "withdrawal";
        String api2 = "capitalization-title";
        String endpoint = "request";

        environment.putString("config", "resource.consentUrl", "https://test.com/open-insurance/consents/v2/consents");
        PrepareUrlForCapitalizationTitleWithdrawal cond = new PrepareUrlForCapitalizationTitleWithdrawal();
        run(cond);
        String protectedResourceUrl = environment.getString("protected_resource_url");
        assertEquals(String.format("https://test.com/open-insurance/%s/v1/%s/%s", api, api2, endpoint), protectedResourceUrl);
    }

}