package net.openid.conformance.opin.testmodule.support.pensionWithdrawal.pensionWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalMockPostRequestBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalParcialPostRequestBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalTotalPostRequestBody;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreatePensionWithdrawalRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putObject("config", new JsonObject());

        environment.putString("certificateId", "certificate_id");
        environment.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());

        JsonObject contract = new JsonObject();
        contract.addProperty("productName", "product_Name");
        contract.addProperty("certificateId", "certificate_id");
        environment.putObject("contract_1", contract);

        JsonObject pmbacAmount = new JsonObject();
        pmbacAmount.addProperty("amount", "2000.00");
        pmbacAmount.add("unit", new JsonObject());
        environment.putObject("pmbacAmount_1", pmbacAmount);
    }

    @Test
    public void testPensionWithdrawalTotal() {
        CreatePensionWithdrawalTotalPostRequestBody cond = new CreatePensionWithdrawalTotalPostRequestBody();
        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        assertEquals("certificate_id", environment.getString("resource_request_entity", "data.generalInfo.certificateId"));
        assertEquals("product_Name", environment.getString("resource_request_entity", "data.generalInfo.productName"));
        assertEquals("2000.00", environment.getString("resource_request_entity", "data.withdrawalInfo.pmbacAmount.amount"));
    }

    @Test
    public void testPensionWithdrawalParcial() {
        CreatePensionWithdrawalParcialPostRequestBody cond = new CreatePensionWithdrawalParcialPostRequestBody();
        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        assertEquals("certificate_id", environment.getString("resource_request_entity", "data.generalInfo.certificateId"));
        assertEquals("product_Name", environment.getString("resource_request_entity", "data.generalInfo.productName"));
        assertEquals("2000.00", environment.getString("resource_request_entity", "data.withdrawalInfo.pmbacAmount.amount"));
        assertEquals("1000.00", environment.getString("resource_request_entity", "data.withdrawalInfo.desiredTotalAmount.amount"));
        assertEquals(EnumWithdrawalType.PARCIAL.toString(), environment.getString("resource_request_entity", "data.withdrawalInfo.withdrawalType"));
    }

    @Test
    public void testPensionWithdrawalMock() {
        CreatePensionWithdrawalMockPostRequestBody cond = new CreatePensionWithdrawalMockPostRequestBody();
        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        assertEquals("string2", environment.getString("resource_request_entity", "data.generalInfo.certificateId"));
        assertEquals("string", environment.getString("resource_request_entity", "data.generalInfo.productName"));
        assertEquals("9999.00", environment.getString("resource_request_entity", "data.withdrawalInfo.pmbacAmount.amount"));
    }

}
