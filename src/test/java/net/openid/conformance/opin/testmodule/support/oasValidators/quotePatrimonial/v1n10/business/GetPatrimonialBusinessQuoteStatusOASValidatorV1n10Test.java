package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.business;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPatrimonialBusinessQuoteStatusOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/business/GetPatrimonialBusinessQuoteStatusOkV1.10.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPatrimonialBusinessQuoteStatusOASValidatorV1n10());
    }
}