package net.openid.conformance.opin.testmodule.support.consents;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EditPostConsentBodyTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        JsonObject consentRequest = new JsonObject();
        consentRequest.add("data", new JsonObject());
        environment.putObject("consent_endpoint_request", consentRequest);
        JsonObject data = new JsonObject();
        consentRequest.add("data", data);
        JsonObject loggedUser = new JsonObject();
        data.add("loggedUser", loggedUser);
        JsonObject document = new JsonObject();
        loggedUser.add("document", document);
        document.addProperty("identification", "12345678900");
    }

    @Test
    public void testEditPostConsentBody() {
        EditPostConsentBody cond = new EditPostConsentBody();
        run(cond);
        assertEquals("00000000000", environment.getString("consent_endpoint_request", "data.loggedUser.document.identification"));
    }

}
