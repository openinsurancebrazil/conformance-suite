package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TranslatePolicyTypeToApiTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    public void testHappyPath() {
        environment.putString("policy_type", "DAMAGES_AND_PEOPLE_PERSON");
        TranslatePolicyTypeToApi cond = new TranslatePolicyTypeToApi();

        run(cond);

        assertEquals("insurance-person", environment.getString("api"));
    }

    @Test
    public void testFinancialRisk() {
        environment.putString("policy_type", "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS");
        TranslatePolicyTypeToApi cond = new TranslatePolicyTypeToApi();

        run(cond);

        assertEquals("insurance-financial-risk", environment.getString("api"));
    }
}
