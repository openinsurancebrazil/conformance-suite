package net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class OpinScopesAndPermissionsBuilderTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setEnv(){
        JsonObject client = new JsonObject();
        environment.putObject("client", client);
    }

    @Test
    public void addScopesTest() {
        ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
        permissionsBuilder.addScopes(
                OPINScopesEnum.QUOTE_PATRIMONIAL_LEAD,
                OPINScopesEnum.QUOTE_RESPONSIBILITY_LEAD,
                OPINScopesEnum.QUOTE_FINANCIAL_RISK_LEAD,
                OPINScopesEnum.QUOTE_ACCEPTANCE_AND_BRANCHES_ABROAD_LEAD,
                OPINScopesEnum.QUOTE_PATRIMONIAL_HOME,
                OPINScopesEnum.QUOTE_PATRIMONIAL_BUSINESS,
                OPINScopesEnum.QUOTE_PATRIMONIAL_CONDOMINIUM,
                OPINScopesEnum.QUOTE_PATRIMONIAL_DIVERSE_RISKS
        );
        permissionsBuilder.build();
        JsonObject client = environment.getObject("client");
        List<String> scopes = Arrays.asList(OIDFJSON.getString(client.get("scope")).split(" "));
        assertTrue(
                scopes.contains("quote-patrimonial-lead")
                        && scopes.contains("quote-responsibility-lead")
                        && scopes.contains("quote-acceptance-and-branches-abroad-lead")
                        && scopes.contains("quote-financial-risk-lead")
                        && scopes.contains("quote-patrimonial-home")
                        && scopes.contains("quote-patrimonial-business")
                        && scopes.contains("quote-patrimonial-condominium")
                        && scopes.contains("quote-patrimonial-diverse-risks")
        );
    }
}
