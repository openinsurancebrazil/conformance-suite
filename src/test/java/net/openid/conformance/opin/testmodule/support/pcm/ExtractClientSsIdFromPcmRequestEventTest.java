package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class ExtractClientSsIdFromPcmRequestEventTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce(value = "pcmRequests/metricEventRequest.json", key = "pcm_event")
    public void weCanExtractSsIdFromPcmRequestEvent(){
        ExtractClientSsIdFromPcmRequestEvent cond = new ExtractClientSsIdFromPcmRequestEvent();
        run(cond);
        String clientId = environment.getString("config", "directory.client_id");
        Assert.assertNotNull(clientId);
        Assert.assertEquals("2a59c2a3-529f-41c6-97e3-77395e9951ca", clientId);
    }

}