package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class ExtractServerOrgIdFromPcmRequestEventTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    @UseResurce(value = "pcmRequests/metricEventRequest.json", key = "pcm_event")
    public void weCanExtractServerOrgIdFromPcmRequestEvent(){
        ExtractServerOrgIdFromPcmRequestEvent cond = new ExtractServerOrgIdFromPcmRequestEvent();
        run(cond);
        String orgId = environment.getString("config", "resource.brazilOrganizationId");
        Assert.assertNotNull(orgId);
        Assert.assertEquals("ff66b95a-d817-4fbe-949a-c5912e240189", orgId);
    }

}