package net.openid.conformance.opin.testmodule.support.oasValidators.lostProfit.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.lostProfit.v1n3.GetLostProfitOASValidatorV1n3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetLostProfitOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/productsServices/GetLostProfitResponseOkV1.3.0.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetLostProfitOASValidatorV1n3());
    }
}
