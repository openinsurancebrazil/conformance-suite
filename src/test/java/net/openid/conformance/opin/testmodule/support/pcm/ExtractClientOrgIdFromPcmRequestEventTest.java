package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class ExtractClientOrgIdFromPcmRequestEventTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    @UseResurce(value = "pcmRequests/metricEventRequest.json", key = "pcm_event")
    public void weCanExtractClientOrgIdFromPcmRequestEvent(){
        ExtractClientOrgIdFromPcmRequestEvent cond = new ExtractClientOrgIdFromPcmRequestEvent();
        run(cond);
        String orgId = environment.getString("config", "resource.brazilOrganizationId");
        Assert.assertNotNull(orgId);
        Assert.assertEquals("1fb79963-4bff-4204-9370-93aceb8a2f0d", orgId);
    }

}