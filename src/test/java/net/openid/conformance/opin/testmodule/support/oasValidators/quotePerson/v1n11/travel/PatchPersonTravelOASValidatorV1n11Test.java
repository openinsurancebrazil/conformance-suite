package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchPersonTravelOASValidatorV1n11Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/PatchPersonTravelOkV1.11.0.json")
    public void happyPathTest() {
        setStatus(200);
        run(new PatchPersonTravelOASValidatorV1n11());
    }
}