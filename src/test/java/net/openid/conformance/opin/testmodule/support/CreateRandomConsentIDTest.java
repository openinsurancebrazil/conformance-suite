package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateRandomConsentIDTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putObject("client", new JsonObject());
    }

    @Test
    public void testHappyPath() {
        CreateRandomConsentID cond = new CreateRandomConsentID();

        run(cond);

        assertNotNull(environment.getString("consent_id"));
    }
}
