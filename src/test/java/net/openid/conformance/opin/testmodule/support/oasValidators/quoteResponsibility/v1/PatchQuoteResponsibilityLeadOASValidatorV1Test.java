package net.openid.conformance.opin.testmodule.support.oasValidators.quoteResponsibility.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchQuoteResponsibilityLeadOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchLeadResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchQuoteResponsibilityLeadOASValidatorV1());
    }
}