package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PatchQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchQuoteCapitalizationTitleOASValidatorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/PatchQuoteCapitalizationTitleStatusResponseOk.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchQuoteCapitalizationTitleOASValidator());
    }

}
