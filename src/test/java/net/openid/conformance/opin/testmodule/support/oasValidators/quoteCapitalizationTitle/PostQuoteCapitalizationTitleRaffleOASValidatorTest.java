package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PostQuoteCapitalizationTitleRaffleOASValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostQuoteCapitalizationTitleRaffleOASValidatorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/PostQuoteCapitalizationTitleRaffleStatusResponseOk.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostQuoteCapitalizationTitleRaffleOASValidator());
    }

}
