package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PostQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostQuoteCapitalizationTitleOASValidatorTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/PostQuoteCapitalizationTitleStatusResponseOk.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostQuoteCapitalizationTitleOASValidator());
    }

}
