package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuotePatrimonialPostRequestBodyWithCoverageTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
    }

    @Test
    public void testCreateQuotePatrimonialHomePostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialHomePostRequestBodyWithCoverage cond = new CreateQuotePatrimonialHomePostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");
        assertEquals(
                "random_cpf",
                quoteData.getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );
        assertTrue(quoteData.get("hasCommercialActivity").getAsBoolean());
        assertTrue(quoteData.get("hasOneRiskLocation").getAsBoolean());
        assertFalse(quoteData.get("isCollectiveStipulated").getAsBoolean());
        assertFalse(quoteData.get("includesAssistanceServices").getAsBoolean());
        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("termStartDate"));
        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("termEndDate"));
        assertEquals("RENOVACAO", quoteData.get("insuranceType").getAsString());
        assertEquals("111111", quoteData.get("policyId").getAsString());
        assertEquals("insurer_id", quoteData.get("insurerId").getAsString());
        assertEquals("BRL", quoteData.get("currency").getAsString());
        assertNotNull(quoteData.getAsJsonObject("maxLMG"));
        assertEquals("90.85", quoteData.getAsJsonObject("maxLMG").get("amount").getAsString());
        assertEquals("PORCENTAGEM", quoteData.getAsJsonObject("maxLMG").get("unitType").getAsString());

        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject maxLMI = coverage.getAsJsonObject("maxLMI");
        assertNotNull(maxLMI);
        assertEquals("90.85", maxLMI.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMI.get("unitType").getAsString());
    }


    @Test
    public void testPersonalCreateQuotePatrimonialBusinessPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialBusinessPostRequestBodyWithCoverage cond = new CreateQuotePatrimonialBusinessPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals(
                "random_cpf",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );

        assertTrue(quoteData.get("isCollectiveStipulated").getAsBoolean());
        assertTrue(quoteData.get("hasOneRiskLocation").getAsBoolean());
        assertFalse(quoteData.get("includesAssistanceServices").getAsBoolean());

        assertEquals("RENOVACAO", quoteData.get("insuranceType").getAsString());
        assertEquals("111111", quoteData.get("policyId").getAsString());
        assertEquals("insurer_id", quoteData.get("insurerId").getAsString());
        assertEquals("BRL", quoteData.get("currency").getAsString());
        assertEquals("string", quoteData.get("claimDescription").getAsString());
        assertNotNull(quoteData.getAsJsonObject("maxLMG"));
        assertEquals("90.85", quoteData.getAsJsonObject("maxLMG").get("amount").getAsString());
        assertEquals("PORCENTAGEM", quoteData.getAsJsonObject("maxLMG").get("unitType").getAsString());

        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject maxLMI = coverage.getAsJsonObject("maxLMI");
        assertNotNull(maxLMI);
        assertEquals("90.85", maxLMI.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMI.get("unitType").getAsString());
    }

    @Test
    public void testBusinessCreateQuotePatrimonialBusinessPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        CreateQuotePatrimonialBusinessPostRequestBodyWithCoverage cond = new CreateQuotePatrimonialBusinessPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");

        assertEquals(
                "random_cnpj",
                quoteData.getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );

        assertTrue(quoteData.get("isCollectiveStipulated").getAsBoolean());
        assertTrue(quoteData.get("hasOneRiskLocation").getAsBoolean());
        assertFalse(quoteData.get("includesAssistanceServices").getAsBoolean());

        assertEquals("RENOVACAO", quoteData.get("insuranceType").getAsString());
        assertEquals("111111", quoteData.get("policyId").getAsString());
        assertEquals("insurer_id", quoteData.get("insurerId").getAsString());
        assertEquals("BRL", quoteData.get("currency").getAsString());
        assertEquals("string", quoteData.get("claimDescription").getAsString());
        assertNotNull(quoteData.getAsJsonObject("maxLMG"));
        assertEquals("90.85", quoteData.getAsJsonObject("maxLMG").get("amount").getAsString());
        assertEquals("PORCENTAGEM", quoteData.getAsJsonObject("maxLMG").get("unitType").getAsString());

        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject maxLMI = coverage.getAsJsonObject("maxLMI");
        assertNotNull(maxLMI);
        assertEquals("90.85", maxLMI.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMI.get("unitType").getAsString());
    }

    @Test
    public void testPersonalCreateQuotePatrimonialCondominiumPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage cond = new CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());

        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");

        JsonObject insuredObject = quoteData.getAsJsonObject("insuredObject");
        assertEquals(
                "random_cpf",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonObject("insuredObject")
                        .get("identification").getAsString()
        );

        assertEquals("COMERCIO", quoteData.get("mainActivity").getAsString());
        assertTrue(quoteData.get("isLegallyConstituted").getAsBoolean());
        assertTrue(quoteData.get("condominiumType").getAsBoolean());

        assertNotNull(quoteData.get("termStartDate"));
        assertNotNull(quoteData.get("termEndDate"));
        assertFalse(quoteData.get("includesAssistanceServices").getAsBoolean());
        assertTrue(insuredObject.get("indenizationWithoutDepreciation").getAsBoolean());
        assertFalse(insuredObject.get("hasElevator").getAsBoolean());
        assertFalse(insuredObject.get("isFullyOrPartiallyListed").getAsBoolean());
        assertFalse(insuredObject.get("hasReuseOfWater").getAsBoolean());
        assertFalse(insuredObject.get("wasThereAClaim").getAsBoolean());

        assertEquals("RENOVACAO", quoteData.get("insuranceType").getAsString());
        assertEquals("111111", quoteData.get("policyId").getAsString());
        assertEquals("insurer_id", quoteData.get("insurerId").getAsString());
        assertEquals("BRL", quoteData.get("currency").getAsString());
        assertEquals("SIMPLES", quoteData.get("basicCoverageIndex").getAsString());
        assertEquals("string", quoteData.get("claimDescription").getAsString());
        assertNotNull(quoteData.getAsJsonObject("insuredObject"));
        assertNotNull(quoteData.getAsJsonObject("maxLMG"));
        assertNotNull(quoteData.getAsJsonObject("claimAmount"));
        assertEquals("90.85", quoteData.getAsJsonObject("maxLMG").get("amount").getAsString());
        assertEquals("PORCENTAGEM", quoteData.getAsJsonObject("maxLMG").get("unitType").getAsString());

        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(coverages);
        assertNotNull(coverage);

        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());
        JsonObject maxLMI = coverage.getAsJsonObject("maxLMI");
        assertNotNull(maxLMI);
        assertEquals("90.85", maxLMI.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMI.get("unitType").getAsString());
    }

    @Test
    public void testBusinessCreateQuotePatrimonialCondominiumPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "business");
        environment.putString("config", "resource.brazilCnpj", "random_cnpj");
        CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage cond = new CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals(
                "random_cnpj",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonObject("insuredObject")
                        .get("identification").getAsString()
        );


    }

    @Test
    public void testPersonalCreateQuotePatrimonialDiverseRisksPostRequestBodyHappyPath() {
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
        CreateQuotePatrimonialDiverseRisksPostRequestBodyWithCoverage cond = new CreateQuotePatrimonialDiverseRisksPostRequestBodyWithCoverage();

        run(cond);

        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject quoteData = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData");
        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals(
                "random_cpf",
                requestBody.getAsJsonObject("data")
                        .getAsJsonObject("quoteData")
                        .getAsJsonArray("beneficiaries")
                        .get(0).getAsJsonObject()
                        .get("identification").getAsString()
        );

        JsonArray coverages = quoteData.getAsJsonArray("coverages");
        JsonObject coverage = coverages.get(0).getAsJsonObject();

        assertNotNull(coverages);
        assertNotNull(coverage);

        assertFalse(quoteData.get("isCollectiveStipulated").getAsBoolean());
        assertNotNull(quoteData.get("insuranceTermStartDate"));
        assertEquals("EQUIPAMENTO_MOVEL", quoteData.get("insuredObjectType").getAsString());
        assertEquals("0111", coverage.get("branch").getAsString());
        assertEquals("RESIDENCIAL_IMOVEL_BASICA", coverage.get("code").getAsString());
        assertFalse(coverage.get("isSeparateContractingAllowed").getAsBoolean());

        JsonObject maxLMG = quoteData.getAsJsonObject("maxLMG");
        assertEquals("90.85", maxLMG.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMG.get("unitType").getAsString());

        JsonObject maxLMI = coverage.getAsJsonObject("maxLMI");
        assertNotNull(maxLMI);
        assertEquals("90.85", maxLMI.get("amount").getAsString());
        assertEquals("PORCENTAGEM", maxLMI.get("unitType").getAsString());
    }
}
