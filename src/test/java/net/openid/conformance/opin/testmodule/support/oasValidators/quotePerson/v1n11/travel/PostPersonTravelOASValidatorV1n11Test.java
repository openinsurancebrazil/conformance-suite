package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostPersonTravelOASValidatorV1n11Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/PostPersonTravelOkV1.11.0.json")
    public void happyPathTest() {
        setStatus(201);
        run(new PostPersonTravelOASValidatorV1n11());
    }
}