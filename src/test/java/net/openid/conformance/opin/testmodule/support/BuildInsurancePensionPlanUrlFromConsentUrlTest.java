package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuildInsurancePensionPlanUrlFromConsentUrlTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    public void testBuildPensionPlanUrlFromConsentUrl(){
        String api = "insurance-pension-plan";
        String endpoint = "insurance-pension-plan/contracts";

        environment.putString("config", "resource.consentUrl", "https://test.com/open-insurance/consents/v2/consents");
        BuildInsurancePensionPlanUrlFromConsentUrl cond = new BuildInsurancePensionPlanUrlFromConsentUrl();
        run(cond);
        String protectedResourceUrl = environment.getString("protected_resource_url");
        assertEquals(String.format("https://test.com/open-insurance/%s/v1/%s", api, endpoint), protectedResourceUrl);
    }
}
