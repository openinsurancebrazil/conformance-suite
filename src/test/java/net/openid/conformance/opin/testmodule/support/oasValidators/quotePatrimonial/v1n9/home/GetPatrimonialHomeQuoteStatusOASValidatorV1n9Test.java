package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.home;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPatrimonialHomeQuoteStatusOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/home/GetPatrimonialHomeQuoteStatusOkV1.9.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPatrimonialHomeQuoteStatusOASValidatorV1n9());
    }
}