package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.*;
import net.openid.conformance.opin.testmodule.support.directory.GetInsuranceCapitalizationTitleV1Endpoint;

import static org.junit.Assert.assertEquals;

public class GetInsuranceCapitalizationTitleV1EndpointTest extends AbstractGetXFromAuthServerTest {
    @Override
    protected String getEndpoint() {
        return "https://consent.from.server/open-insurance/insurance-capitalization-title/v1/insurance-capitalization-title/plans";
    }

    @Override
    protected String getApiFamilyType() {
        return "insurance-capitalization-title";
    }

    @Override
    protected String getApiVersion() {
        return "1.0.0";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

    @Override
    protected AbstractGetXFromAuthServer getCondition() {
        return new GetInsuranceCapitalizationTitleV1Endpoint();
    }
}
