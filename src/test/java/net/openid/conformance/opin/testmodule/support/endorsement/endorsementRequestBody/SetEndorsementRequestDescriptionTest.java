package net.openid.conformance.opin.testmodule.support.endorsement.endorsementRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.OPINPhase3Keys;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementRequestDescription;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetEndorsementRequestDescriptionTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        JsonObject resourceRequestEntity = new JsonObject();
        resourceRequestEntity.add("data", new JsonObject());
        environment.putObject("resource_request_entity", resourceRequestEntity);

    }

    @Test
    public void testSetEndorsementRequestDescription() {
        SetEndorsementRequestDescription cond = new SetEndorsementRequestDescription();
        run(cond);

        assertEquals(environment.getString("resource_request_entity", "data.requestDescription"), OPINPhase3Keys.Keys_diffRequestDescription);
    }
}
