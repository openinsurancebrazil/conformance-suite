package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostClaimNotificationDamageOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/claimNotification/PostClaimNotificationDamageResponse.json")
    public void testHappyPath() {
        setStatus(201);
        run(new PostClaimNotificationDamageOASValidatorV1());
    }
}
