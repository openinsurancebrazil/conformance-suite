package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CheckOpinDirectoryApiBaseTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putObject("config", new JsonObject());
    }

    @Test
    public void testHappyPath() {
        CheckOpinDirectoryApiBase cond = new CheckOpinDirectoryApiBase();

        environment.putString("config", "directory.apibase", "https://matls-api.sandbox.directory.opinbrasil.com.br/");
        run(cond);

        environment.putString("config", "directory.apibase", "https://matls-api.directory.opinbrasil.com.br/");
        run(cond);
    }

    @Test
    public void testUnhappyPath() {
        environment.putString("config", "directory.apibase", "https://invalid.com/.well-known/openid-configuration");
        CheckOpinDirectoryApiBase cond = new CheckOpinDirectoryApiBase();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Testing for Brazil certification must be done using the Brazil directory"));
    }
}
