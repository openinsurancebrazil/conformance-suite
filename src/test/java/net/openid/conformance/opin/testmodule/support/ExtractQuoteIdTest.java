package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExtractQuoteIdTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/GetQuoteAutoQuoteStatusResponseOK.json")
    public void testHappyPath() {
        ExtractQuoteId cond = new ExtractQuoteId();

        run(cond);

        assertEquals("string", environment.getString("quote_id"));
    }
}
