package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.GetQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GetQuoteCapitalizationTitleOASValidatorTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        setStatus(200);
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/GetQuoteCapitalizationTitleStatusResponseOk.json")
    public void testHappyPath() {
        run(new GetQuoteCapitalizationTitleOASValidator());
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/GetQuoteCapitalizationTitlePaymentTypeSingleError.json")
    public void testPaymentTypeUnicoAndSinglePaymentMissing() {
        ConditionError e = runAndFail(new GetQuoteCapitalizationTitleOASValidator());
        assertTrue(e.getMessage().contains("singlePayment is required when paymentType is UNICO"));
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/GetQuoteCapitalizationTitlePaymentTypeMonthlyError.json")
    public void testPaymentTypeMensalAndMonthlyPaymentMissing() {
        ConditionError e = runAndFail(new GetQuoteCapitalizationTitleOASValidator());
        assertTrue(e.getMessage().contains("monthlyPayment is required when paymentType is MENSAL"));
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/GetQuoteCapitalizationTitleStatusRJCTOk.json")
    public void testStatusRJCT() {
        run(new GetQuoteCapitalizationTitleOASValidator());
    }

    @Test
    @UseResurce("opinResponses/quoteCapitalizationTitle/v1n10/GetQuoteCapitalizationTitleQuoteInfoMissingV1.10.0.json")
    public void testQuoteInfoMissing() {
        ConditionError e = runAndFail(new GetQuoteCapitalizationTitleOASValidator());
        assertTrue(e.getMessage().contains("quoteInfo is required when status is ACPT"));
    }
}
