package net.openid.conformance.opin.testmodule.support.oasValidators.quoteFinancialRisk.v1n9;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchQuoteFinancialRiskLeadOASValidatorV1n9Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/QuoteCommonPatchLeadResponseOK.json")
    public void testHappyPath() {
        setStatus(200);
        run(new PatchQuoteFinancialRiskLeadOASValidatorV1n9());
    }
}