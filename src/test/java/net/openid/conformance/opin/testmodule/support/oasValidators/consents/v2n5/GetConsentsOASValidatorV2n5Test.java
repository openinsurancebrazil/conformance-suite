package net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetConsentsOASValidatorV2n5Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/consents/v2/getConsent/getConsentByIdResponse.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetConsentsOASValidatorV2n5());
    }
}
