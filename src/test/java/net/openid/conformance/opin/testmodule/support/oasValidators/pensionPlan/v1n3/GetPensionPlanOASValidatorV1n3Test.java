package net.openid.conformance.opin.testmodule.support.oasValidators.pensionPlan.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionPlan.v1n4.GetPensionPlanOASValidatorV1n4;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPensionPlanOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/phase1Update/GetPensionPlanResponseOkV1.3.0.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPensionPlanOASValidatorV1n3());
    }
}