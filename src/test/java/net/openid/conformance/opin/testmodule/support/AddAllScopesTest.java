package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AddAllScopesTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putObject("client", new JsonObject());
    }

    @Test
    public void test() {
        AddAllScopes cond = new AddAllScopes();
        run(cond);

        String scopes = environment.getString("client", "scope");
        for(String scope : "consents resources customers insurance-acceptance-and-branches-abroad insurance-auto insurance-financial-risk insurance-patrimonial insurance-responsibility".split(" ")) {
            System.out.println("Checking scope: " + scope);
            assertTrue(scopes.contains(scope));
        }
    }
}
