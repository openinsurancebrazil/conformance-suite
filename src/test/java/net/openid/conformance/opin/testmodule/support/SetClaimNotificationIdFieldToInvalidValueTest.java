package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToInvalidValue;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToInvalidValueDiff;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToWrongType;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToWrongTypeDiff;
import org.junit.Test;

import static org.junit.Assert.*;

public class SetClaimNotificationIdFieldToInvalidValueTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    public void testPolicyNumberToGroupCertificateId() {
        String policyId = "random_policy_id";
        environment.putString("policyId", policyId);
        environment.putString("resource_request_entity", "data.groupCertificateId", policyId);
        environment.putString("resource_request_entity", "data.documentType", "CERTIFICADO");
        SetClaimNotificationIdFieldToInvalidValue cond = new SetClaimNotificationIdFieldToInvalidValue();

        run(cond);

        assertNull(environment.getString("resource_request_entity", "data.policyNumber"));
        assertEquals("00000000", environment.getString("resource_request_entity", "data.groupCertificateId"));
    }

    @Test
    public void testGroupCertificateIdToPolicyNumber() {
        String policyId = "random_policy_id";
        environment.putString("policyId", policyId);
        environment.putString("resource_request_entity", "data.policyNumber", policyId);
        environment.putString("resource_request_entity", "data.documentType", "APOLICE_INDIVIDUAL");
        SetClaimNotificationIdFieldToInvalidValue cond = new SetClaimNotificationIdFieldToInvalidValue();

        run(cond);

        assertNull(environment.getString("resource_request_entity", "data.groupCertificateId"));
        assertEquals("00000000", environment.getString("resource_request_entity", "data.policyNumber"));
    }

    @Test
    public void testNoDocumentType() {
        environment.putString("policyId", "random_policy_id");
        environment.putString("resource_request_entity", "data.groupCertificateId", "random_policy_id");
        SetClaimNotificationIdFieldToInvalidValue cond = new SetClaimNotificationIdFieldToInvalidValue();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("documentType field not present"));
    }

    @Test
    public void testInvalidDocumentType() {
        environment.putString("policyId", "random_policy_id");
        environment.putString("resource_request_entity", "data.groupCertificateId", "random_policy_id");
        environment.putString("resource_request_entity", "data.documentType", "RANDOM");
        SetClaimNotificationIdFieldToInvalidValue cond = new SetClaimNotificationIdFieldToInvalidValue();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Invalid documentType"));
    }

    @Test
    public void testDiffPolicyIdFieldName() {
        String policyId = "random_policy_id";
        environment.putString("policyId", policyId);
        environment.putString("resource_request_entity", "data.policyId", policyId);
        environment.putString("resource_request_entity", "data.documentType", "APOLICE_INDIVIDUAL_AUTOMOVEL");
        SetClaimNotificationIdFieldToInvalidValueDiff cond = new SetClaimNotificationIdFieldToInvalidValueDiff();

        run(cond);

        assertEquals("00000000", environment.getString("resource_request_entity", "data.policyId"));
    }
}
