package net.openid.conformance.opin.testmodule.support.oasValidators.autoExtendedWarranty.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


public class GetAutoExtendedWarrantyOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {

	@UseResurce("opinResponses/productsServices/GetAutoExtendedWarrantyResponseOkV1.3.0.json")
	@Test
	public void happyPathTest() {
		setStatus(200);
		run(new GetAutoExtendedWarrantyOASValidatorV1n3());
	}
}
