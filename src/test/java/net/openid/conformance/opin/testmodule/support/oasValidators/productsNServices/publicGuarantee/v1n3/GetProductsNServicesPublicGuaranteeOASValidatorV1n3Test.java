package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.publicGuarantee.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetProductsNServicesPublicGuaranteeOASValidatorV1n3Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/productsServices/GetProductsNServicesPublicGuaranteeResponseV1n3.json")
    public void testHappyPath() {
        setStatus(200);
        GetProductsNServicesPublicGuaranteeOASValidatorV1n3 cond = new GetProductsNServicesPublicGuaranteeOASValidatorV1n3();
        run(cond);
    }
}
