package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClaimNotificationSelectResourcePolicyIdTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListValidatorV2OK.json")
    public void testPersonHappyPath() {
        environment.putString("claim_notification_endpoint_type", "person");
        ClaimNotificationSelectResourcePolicyId cond = new ClaimNotificationSelectResourcePolicyId();

        run(cond);

        assertEquals("25cac914-d8ae-6789-b215-650a6215820a", environment.getString("policyId"));
    }

    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListValidatorV2OK.json")
    public void testDamagesHappyPath() {
        environment.putString("claim_notification_endpoint_type", "damage");
        ClaimNotificationSelectResourcePolicyId cond = new ClaimNotificationSelectResourcePolicyId();

        run(cond);

        assertEquals("25cac914-d8ae-6789-b215-650a6215820c", environment.getString("policyId"));
    }

    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListValidatorV2NotAvailable.json")
    public void testPersonNotAvailable() {
        environment.putString("claim_notification_endpoint_type", "person");
        ClaimNotificationSelectResourcePolicyId cond = new ClaimNotificationSelectResourcePolicyId();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("no resource of the correct type"));
    }
}
