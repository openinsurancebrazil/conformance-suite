package net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetCustomersPersonalQualificationListOASValidatorV1n6Test extends AbstractJsonResponseConditionUnitTest {
	@Test
	@UseResurce("opinResponses/customers/v1/personalQualification/naturalPersonQualificationResponse.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetCustomersPersonalQualificationListOASValidatorV1n6());
	}
}
