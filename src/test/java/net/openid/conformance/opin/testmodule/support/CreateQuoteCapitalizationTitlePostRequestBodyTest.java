package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.*;
import net.openid.conformance.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateQuoteCapitalizationTitlePostRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
    @Before
    public void setUp() {
        environment.putString("consent_id", "random_consent_id");
        environment.putString("config", "consent.productType", "personal");
        environment.putString("config", "resource.brazilCpf", "random_cpf");
    }

    @Test
    public void testSinglePayment() {
        CreateQuoteCapitalizationTitlePostRequestBodySinglePayment cond = new CreateQuoteCapitalizationTitlePostRequestBodySinglePayment();
        run(cond);
        happyPath("UNICO");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateSinglePayment(requestBody, "1000.00");
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("singlePayment").get("unitType"));

    }

    @Test
    public void testSinglePaymentWithUnitType() {
        CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentWithUnitType cond = new CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentWithUnitType();
        run(cond);
        happyPath("UNICO");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateSinglePayment(requestBody, "1000.00");
        assertEquals("MONETARIO", requestBody.getAsJsonObject("data").getAsJsonObject("quoteData")
                .getAsJsonObject("singlePayment").get("unitType").getAsString());
    }

    @Test
    public void testMonthlyPayment() {
        CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment cond = new CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment();
        run(cond);
        happyPath("MENSAL");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateMonthlyPayment(requestBody, "1000.00", "R$");
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("monthlyPayment").get("unitType"));
    }

    @Test
    public void testMonthlyPaymentWithUnitType() {
        CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPaymentWithUnitType cond = new CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPaymentWithUnitType();
        run(cond);
        happyPath("MENSAL");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateMonthlyPayment(requestBody, "1000.00", "R$");
        assertEquals("MONETARIO", requestBody.getAsJsonObject("data").getAsJsonObject("quoteData")
                .getAsJsonObject("monthlyPayment").get("unitType").getAsString());
    }

    @Test
    public void testSinglePaymentForRjctWithUnitType() {
        CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentForRjctWithUnitType cond = new CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentForRjctWithUnitType();
        run(cond);
        happyPath("UNICO");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateSinglePayment(requestBody, "1000.00");
        assertEquals("PORCENTAGEM", requestBody.getAsJsonObject("data").getAsJsonObject("quoteData")
                .getAsJsonObject("singlePayment").get("unitType").getAsString());

    }

    @Test
    public void testSinglePaymentForRjct() {
        CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentForRjct cond = new CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentForRjct();
        run(cond);
        happyPath("UNICO");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateSinglePayment(requestBody, "0.00");
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("singlePayment").get("unitType"));
    }

    @Test
    public void testPeriodicPayment() {
        CreateQuoteCapitalizationTitlePostRequestBodyPeriodicPayment cond = new CreateQuoteCapitalizationTitlePostRequestBodyPeriodicPayment();
        run(cond);
        happyPath("PERIODICO");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        assertFalse(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").has("monthlyPayment"));
        assertFalse(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").has("singlePayment"));
    }

    @Test
    public void testWrongMonthlyPayment() {
        CreateQuoteCapitalizationTitlePostRequestBodyWrongMonthlyPayment cond = new CreateQuoteCapitalizationTitlePostRequestBodyWrongMonthlyPayment();
        run(cond);
        happyPath("MENSAL");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateSinglePayment(requestBody, "1000.00");
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("singlePayment").get("unitType"));
    }

    @Test
    public void testWrongMonthlyPaymentWithUnitType() {
        CreateQuoteCapitalizationTitlePostRequestBodyWrongMonthlyPaymentWithUnitType cond = new CreateQuoteCapitalizationTitlePostRequestBodyWrongMonthlyPaymentWithUnitType();
        run(cond);
        happyPath("MENSAL");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateSinglePayment(requestBody, "1000.00");
        assertEquals("MONETARIO", requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("singlePayment").get("unitType").getAsString());

    }

    @Test
    public void testWrongSinglePayment() {
        CreateQuoteCapitalizationTitlePostRequestBodyWrongSinglePayment cond = new CreateQuoteCapitalizationTitlePostRequestBodyWrongSinglePayment();
        run(cond);
        happyPath("UNICO");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateMonthlyPayment(requestBody, "1000.00", "R$");
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("monthlyPayment").get("unitType"));
    }

    @Test
    public void testWrongSinglePaymentWithUnitType() {
        CreateQuoteCapitalizationTitlePostRequestBodyWrongSinglePaymentWithUnitType cond = new CreateQuoteCapitalizationTitlePostRequestBodyWrongSinglePaymentWithUnitType();
        run(cond);
        happyPath("UNICO");
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        validateMonthlyPayment(requestBody, "1000.00", "R$");
        assertEquals("MONETARIO", requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("monthlyPayment").get("unitType").getAsString());
    }

    @Test
    public void testCreatePaymentDetails() {
        CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment cond = new CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment();
        run(cond);
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();
        JsonObject paymentDetails = requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").getAsJsonObject("monthlyPayment");
        assertNotNull(paymentDetails);
        assertEquals("1000.00", paymentDetails.get("amount").getAsString());
    }

    protected void happyPath(String expectedPaymentType) {
        assertNotNull(environment.getString("resource_request_entity"));
        JsonObject requestBody = JsonUtils.createBigDecimalAwareGson().fromJson(environment.getString("resource_request_entity"), JsonElement.class).getAsJsonObject();

        assertEquals(environment.getString("consent_id"), requestBody.getAsJsonObject("data").get("consentId").getAsString());
        assertEquals("random_cpf", requestBody.getAsJsonObject("data").getAsJsonObject("quoteCustomer").getAsJsonObject("identificationData").get("cpfNumber").getAsString());
        assertNotNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData"));

        assertEquals("TRADICIONAL", requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("modality").getAsString());
        assertEquals(expectedPaymentType, requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("paymentType").getAsString());
    }

    private void validateSinglePayment(JsonObject requestBody, String expectedAmount) {
        JsonObject singlePayment = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData")
                .getAsJsonObject("singlePayment");

        assertNotNull(singlePayment);
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("monthlyPayment"));
        assertEquals(expectedAmount, singlePayment.get("amount").getAsString());
    }

    private void validateMonthlyPayment(JsonObject requestBody, String expectedAmount, String expectedUnitCode) {
        JsonObject monthlyPayment = requestBody.getAsJsonObject("data")
                .getAsJsonObject("quoteData")
                .getAsJsonObject("monthlyPayment");

        assertNotNull(monthlyPayment);
        assertNull(requestBody.getAsJsonObject("data").getAsJsonObject("quoteData").get("singlePayment"));
        assertEquals(expectedAmount, monthlyPayment.get("amount").getAsString());
        assertEquals(expectedUnitCode, monthlyPayment.getAsJsonObject("unit").get("code").getAsString());
    }


}
