package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {
    @Test
    @UseResurce("opinResponses/quote/diverseRisks/GetPatrimonialDiverseRisksQuoteStatusOkV1.10.json")
    public void testHappyPath() {
        setStatus(200);
        run(new GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10());
    }
}