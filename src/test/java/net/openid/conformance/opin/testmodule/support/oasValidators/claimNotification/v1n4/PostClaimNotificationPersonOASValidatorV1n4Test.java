package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PostClaimNotificationPersonOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        setStatus(201);
    }

    @Test
    @UseResurce("opinResponses/claimNotification/v1n4/PostClaimNotificationPersonResponseV1.4.0.json")
    public void testHappyPath() {
        run(new PostClaimNotificationPersonOASValidatorV1n4());
    }

    @Test
    @UseResurce("opinResponses/claimNotification/v1n4/PostClaimNotificationPersonResponseNoRequestorV1.4.0.json")
    public void testNoRequestor() {
        ConditionError conditionError = runAndFail(new PostClaimNotificationPersonOASValidatorV1n4());
        assertTrue(conditionError.getMessage().contains("requestor is required when requestorIsInsured is false"));
    }
}