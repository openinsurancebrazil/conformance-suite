package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
public class GetInsuranceFinancialRiskClaimOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/financialRisk/v1n4/OpinInsuranceFinancialRiskClaimValidatorV1n4OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceFinancialRiskClaimOASValidatorV1n4());
	}

}
