package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n5;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetInsuranceLifePensionContractInfoOASValidatorV1n5Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceLifePension/v1n5/OpinInsuranceLifePensionContractInfoValidatorV1n5OK.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetInsuranceLifePensionContractInfoOASValidatorV1n5());
	}

}
