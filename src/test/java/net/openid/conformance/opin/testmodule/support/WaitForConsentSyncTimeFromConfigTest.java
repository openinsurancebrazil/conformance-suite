package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;

public class WaitForConsentSyncTimeFromConfigTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putObject("config", new JsonObject());
        environment.putObject("config", "resource", new JsonObject());
    }

    @Test
    public void testHappyPath() {
        environment.putString("config", "resource.consentSyncTime", "10");
        WaitForConsentSyncTimeFromConfig cond = spy(new WaitForConsentSyncTimeFromConfig());
        doNothing().when(cond).pauseExecution(anyLong());
        run(cond);
        assertEquals("10", environment.getString("waitTime"));
    }

    @Test
    public void testNoConsentSyncTime() {
        WaitForConsentSyncTimeFromConfig cond = new WaitForConsentSyncTimeFromConfig();
        run(cond);
        assertEquals("0", environment.getString("waitTime"));
    }

    @Test
    public void testConsentSyncTimeGreaterThanMax() {
        environment.putString("config", "resource.consentSyncTime", "601");
        WaitForConsentSyncTimeFromConfig cond = spy(new WaitForConsentSyncTimeFromConfig());
        doNothing().when(cond).pauseExecution(anyLong());
        run(cond);
        assertEquals("300", environment.getString("waitTime"));
    }

    @Test
    public void testConsentSyncTimeLessThanZero() {
        environment.putString("config", "resource.consentSyncTime", "-1");
        WaitForConsentSyncTimeFromConfig cond = spy(new WaitForConsentSyncTimeFromConfig());
        doNothing().when(cond).pauseExecution(anyLong());
        run(cond);
        assertEquals("300", environment.getString("waitTime"));
    }


}
