package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuildInsuranceLifePensionUrlFromConsentUrlTest extends AbstractJsonResponseConditionUnitTest {
    @Test
    public void testBuildCapitalizationTitleUrlFromConsentUrl(){
        String api = "insurance-life-pension";
        String endpoint = "insurance-life-pension/contracts";

        environment.putString("config", "resource.consentUrl", "https://test.com/open-insurance/consents/v2/consents");
        BuildInsuranceLifePensionUrlFromConsentUrl cond = new BuildInsuranceLifePensionUrlFromConsentUrl();
        run(cond);
        String protectedResourceUrl = environment.getString("protected_resource_url");
        assertEquals(String.format("https://test.com/open-insurance/%s/v1/%s", api, endpoint), protectedResourceUrl);
    }
}
