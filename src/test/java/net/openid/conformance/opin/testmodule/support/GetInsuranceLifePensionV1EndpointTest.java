package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServerTest;
import net.openid.conformance.opin.testmodule.support.directory.GetInsuranceLifePensionV1Endpoint;

public class GetInsuranceLifePensionV1EndpointTest extends AbstractGetXFromAuthServerTest {
    @Override
    protected String getEndpoint() {
        return "https://consent.from.server/open-insurance/insurance-life-pension/v1/insurance-life-pension/contracts";
    }

    @Override
    protected String getApiFamilyType() {
        return "insurance-life-pension";
    }

    @Override
    protected String getApiVersion() {
        return "1.0.0";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

    @Override
    protected AbstractGetXFromAuthServer getCondition() {
        return new GetInsuranceLifePensionV1Endpoint();
    }
}
