package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static com.mongodb.assertions.Assertions.assertTrue;
import static org.junit.Assert.assertEquals;

public class SelectPolicyIdFromResourcesResponseTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListResponse.json")
    public void happyPathTest() {
        SelectPolicyIdFromResourcesResponse cond = new SelectPolicyIdFromResourcesResponse();
        run(cond);
        assertEquals("20230123", environment.getString("policyId"));
    }

    @Test
    @UseResurce("opinResponses/resources/OpinResourcesListCustomersOnlyResponse.json")
    public void noValidPolicyIdTest() {
        SelectPolicyIdFromResourcesResponse cond = new SelectPolicyIdFromResourcesResponse();
        ConditionError error = runAndFail(cond);
        assertTrue(error.getMessage().contains("No valid policyId found in resources API response"));
    }

}
