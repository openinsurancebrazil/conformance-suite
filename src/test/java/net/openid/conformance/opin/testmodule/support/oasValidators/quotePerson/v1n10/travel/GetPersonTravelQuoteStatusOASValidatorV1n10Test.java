package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPersonTravelQuoteStatusOASValidatorV1n10Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/quotePerson/GetPersonTravelQuoteStatusOkV1.10.2.json")
    public void happyPathTest() {
        setStatus(200);
        run(new GetPersonTravelQuoteStatusOASValidatorV1n10());
    }
}