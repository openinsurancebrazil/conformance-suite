package net.openid.conformance.opin.testmodule.support.oasValidators.person.v1n6;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetPersonOASValidatorV1n6Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/phase1Update/GetPersonResponseOkV1.6.0.json")
	public void testHappyPath() {
		setStatus(200);
		run(new GetPersonOASValidatorV1n6());
	}
}
