package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetEndpointFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetEndpointFromAuthServerTest;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class GetOpinConsentV2EndpointTest extends AbstractGetEndpointFromAuthServerTest {

    @Before
    public void init(){
        environment.putObject("config", "resource", new JsonObject());
    }

    @Test
    public void happyPathTest() {
        addApiResourcesToServer(List.of(createApiResource("consents", "2.0.0", List.of(getConsentUrl()))));
        run(getCondition());
        String actualConsentUrl = environment.getString("config", "resource.consentUrl");
        assertEquals(getConsentUrl(), actualConsentUrl);
    }

    private String getConsentUrl() {
        return "https://consent.from.server/open-insurance/consents/v2/consents";
    }

    @Override
    protected AbstractGetEndpointFromAuthServer getCondition() {
        return new GetOpinConsentV2Endpoint();
    }
}
