package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlanIdSelectorTest extends AbstractJsonResponseConditionUnitTest {


    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlans.json")
    public void weCanExtractPlanId(){
        PlanIdSelector cond = new PlanIdSelector();
        run(cond);
        String selectedId = environment.getString("selected_id");
        assertEquals("test_plan_id", selectedId);
    }


    @Test
    public void weCantExtractPlanIdWithoutData(){
        environment.putString("resource_endpoint_response_full", "body", "{}");
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not find data in the body", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyData.json")
    public void weCantExtractPlanIdWithEmptyData(){
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Data array cannot be empty to extract plan ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoBrand.json")
    public void weCantExtractPlanIdWithoutBrand(){
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract brand.companies array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoCompanies.json")
    public void weCantExtractPlanIdWithoutCompanies(){
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract brand.companies array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyCompanies.json")
    public void weCantExtractPlanIdWithEmptyCompanies(){
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Companies array cannot be empty to extract plan ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoProducts.json")
    public void weCantExtractPlanIdWithoutProducts(){
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Could not extract products array from the data", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansEmptyProducts.json")
    public void weCantExtractPlanIdWithEmptyProducts(){
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: Products array cannot be empty to extract plan ID", cond.getClass().getSimpleName()), e.getMessage());
    }

    @Test
    @UseResurce("opinResponses/capitalizationTitle/GetCapitalizationTitlePlansNoPlanId.json")
    public void weCantExtractPlanIdWithoutPlanId(){
        PlanIdSelector cond = new PlanIdSelector();
        ConditionError e = runAndFail(cond);
        assertEquals(String.format("%s: could not find plan ID in the product object", cond.getClass().getSimpleName()), e.getMessage());
    }

}