package net.openid.conformance.opin.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.EnsureResponseVersionHeaderMatchesRequested;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@UseResurce("support/OkConfigConsents.json")
public class EnsureResponseVersionHeaderMatchesRequestedTest extends  AbstractJsonResponseConditionUnitTest {

    @Test
    public void noHeaderXVOnEnv() {
        environment.putString("x-v","1.1");
        EnsureResponseVersionHeaderMatchesRequested condition = new EnsureResponseVersionHeaderMatchesRequested();
        ConditionError error = runAndFail(condition);
        String expected = "x-v header is missing from response";
        assertThat(error.getMessage(), containsString(expected));
    }
    @Test
    public void badXVOnEnv() {
        environment.putString("x-v","1.0.0");
        setHeaders("x-v","1.1");
        EnsureResponseVersionHeaderMatchesRequested condition = new EnsureResponseVersionHeaderMatchesRequested();
        ConditionError error = runAndFail(condition);
        String expected = "x-v must be in the format: ^\\\\d+\\\\.\\\\d+\\\\.\\\\d+$";
        assertThat(error.getMessage(), containsString(expected));
    }
    @Test
    public void badXVnoMinXVOnEnv() {
        environment.putString("x-v","1.0.0");
        setHeaders("x-v","1.1.1");
        EnsureResponseVersionHeaderMatchesRequested condition = new EnsureResponseVersionHeaderMatchesRequested();
        ConditionError error = runAndFail(condition);
        String expected = "x-v returned different from requested";
        assertThat(error.getMessage(), containsString(expected));
    }

    @Test//any version under x-v should be accepted?
    public void versionGreaterMinXVOnEnv() {
        environment.putString("x-v","1.0.0");
        environment.putString("x-v-min","2.0.0");
        setHeaders("x-v","1.0.1");
        EnsureResponseVersionHeaderMatchesRequested condition = new EnsureResponseVersionHeaderMatchesRequested();
        ConditionError error = runAndFail(condition);
        String expected = "x-v returned different from requested";
        assertThat(error.getMessage(), containsString(expected));
    }

    @Test
    public void versionIncompatibleXVOnEnv() {
        environment.putString("x-v","2.0.0");
        environment.putString("x-v-min","1.1.1");
        setHeaders("x-v","2.0.1");
        EnsureResponseVersionHeaderMatchesRequested condition = new EnsureResponseVersionHeaderMatchesRequested();
        ConditionError error = runAndFail(condition);
        String expected = "x-v returned is not compatible";
        assertThat(error.getMessage(), containsString(expected));
    }

    @Test
    public void goodXVnoMinXVOnEnv() {
        environment.putString("x-v","2.0.0");
        setHeaders("x-v","2.0.0");
        EnsureResponseVersionHeaderMatchesRequested condition = new EnsureResponseVersionHeaderMatchesRequested();
        run(condition);
    }

    @Test
    public void goodMinXVOnEnv() {
        environment.putString("x-v","2.0.0");
        environment.putString("x-v-min","1.0.1");
        setHeaders("x-v","1.1.1");
        EnsureResponseVersionHeaderMatchesRequested condition = new EnsureResponseVersionHeaderMatchesRequested();
        run(condition);
    }

}
