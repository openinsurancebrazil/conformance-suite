package net.openid.conformance.opin.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.AbstractEnsureNumberOfTotalRecordsFromMeta;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.AbstractEnsureNumberOfTotalRecordsFromMetaTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.TotalRecordsComparisonOperator;
import net.openid.conformance.opin.testmodule.support.ensureNumberOfRecords.EnsureNumberOfTotalRecordsIsAtLeast3FromMeta;

public class EnsureNumberOfTotalRecordsIsAtLeast3FromMetaTest extends AbstractEnsureNumberOfTotalRecordsFromMetaTest {
    @Override
    protected int getTotalRecordsComparisonAmount() {
        return 3;
    }

    @Override
    protected TotalRecordsComparisonOperator getComparisonMethod() {
        return TotalRecordsComparisonOperator.AT_LEAST;
    }

    @Override
    protected AbstractEnsureNumberOfTotalRecordsFromMeta getCondition() {
        return new EnsureNumberOfTotalRecordsIsAtLeast3FromMeta();
    }
}
