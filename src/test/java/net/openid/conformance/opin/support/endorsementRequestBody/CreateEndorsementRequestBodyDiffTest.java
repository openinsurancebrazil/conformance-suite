package net.openid.conformance.opin.support.endorsementRequestBody;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateEndorsementRequestBodyDiffTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putString("policyId", "policy_id");
    }

    @Test
    public void testRequestBodyAlteracao() {
        CreateEndorsementRequestBodyAlteracaoDiff cond = new CreateEndorsementRequestBodyAlteracaoDiff();

        run(cond);

        assertEquals(
                EndorsementType.ALTERACAO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertEquals("policy_id", environment.getString("resource_request_entity", "data.policyId"));
        assertEquals("216731531723",environment.getElementFromObject("resource_request_entity", "data.insuredObjectId").getAsString());
        assertNotNull(environment.getString("resource_request_entity"));
        assertNotNull(environment.getElementFromObject("resource_request_entity","data.insuredObjectId"));

    }

    @Test
    public void testRequestBodyCancelamento() {
        CreateEndorsementRequestBodyCancelamentoDiff cond = new CreateEndorsementRequestBodyCancelamentoDiff();

        run(cond);

        assertEquals(
                EndorsementType.CANCELAMENTO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertEquals("policy_id", environment.getString("resource_request_entity", "data.policyId"));
        assertNotNull(environment.getString("resource_request_entity"));
        assertNotNull(environment.getElementFromObject("resource_request_entity","data.insuredObjectId"));
    }

    @Test
    public void testRequestBodyExclusao() {
        CreateEndorsementRequestBodyExclusaoDiff cond = new CreateEndorsementRequestBodyExclusaoDiff();

        run(cond);

        assertEquals(
                EndorsementType.EXCLUSAO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertEquals("policy_id", environment.getString("resource_request_entity", "data.policyId"));
        assertNotNull(environment.getString("resource_request_entity"));
        assertNotNull(environment.getElementFromObject("resource_request_entity","data.insuredObjectId"));
    }

    @Test
    public void testRequestBodyInclusao() {
        CreateEndorsementRequestBodyInclusaoDiff cond = new CreateEndorsementRequestBodyInclusaoDiff();

        run(cond);

        assertEquals(
                EndorsementType.INCLUSAO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertEquals("policy_id", environment.getString("resource_request_entity", "data.policyId"));
        assertNotNull(environment.getString("resource_request_entity"));
        assertNotNull(environment.getElementFromObject("resource_request_entity","data.insuredObjectId"));
    }
}
