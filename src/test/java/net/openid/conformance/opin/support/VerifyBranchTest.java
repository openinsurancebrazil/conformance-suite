package net.openid.conformance.opin.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.VerifyBranch;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.*;

public class VerifyBranchTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("support/PatrimonialResourceEndpointResponse.json")
    public void testNonexistentBranchCode() {
        setTestBranch("0000");
        VerifyBranch condition = new VerifyBranch();
        ConditionError conditionError = runAndFail(condition);
        assertThat(conditionError.getMessage(),containsString("Non-existent branch code saved in the environment."));
    }

    @Test
    @UseResurce("support/PatrimonialResourceEndpointResponse.json")
    public void testBranchFoundLast() {
        setTestBranch("0112");
        VerifyBranch cond = new VerifyBranch();
        run(cond);
        assertTrue(environment.getBoolean("branch_found"));
    }

    @Test
    @UseResurce("support/PatrimonialResourceEndpointResponse.json")
    public void testGlobalBancosBranchNotFound() {
        setTestBranch("0173");
        VerifyBranch cond = new VerifyBranch();
        run(cond);
        assertFalse(environment.getBoolean("branch_found"));
    }

    @Test
    @UseResurce("support/PatrimonialResourceEndpointResponse.json")
    public void testBranchFoundFirst() {
        setTestBranch("0116");
        VerifyBranch cond = new VerifyBranch();
        run(cond);
        assertTrue(environment.getBoolean("branch_found"));
    }

    void setTestBranch(String branch) {
        environment.putString("branch", branch);
    }

}
