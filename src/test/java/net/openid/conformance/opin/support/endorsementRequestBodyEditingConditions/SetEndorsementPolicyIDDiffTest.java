package net.openid.conformance.opin.support.endorsementRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoFirstOneDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoSecondOneDiff;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetEndorsementPolicyIDDiffTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        JsonObject requestData = new JsonObject();
        requestData.add("data", new JsonObject());
        environment.putObject("resource_request_entity", requestData);
    }

    @Test
    public void testFirstIdHappyPath() {
        SetEndorsementPolicyIDtoFirstOneDiff cond = new SetEndorsementPolicyIDtoFirstOneDiff();
        environment.putString("firstPolicyId", "first_policy_id");
        environment.putString("policyId", "first_policy_id");

        run(cond);

        assertEquals("first_policy_id", environment.getString("resource_request_entity", "data.policyId"));
    }

    @Test
    public void testSecondIdHappyPath() {
        SetEndorsementPolicyIDtoSecondOneDiff cond = new SetEndorsementPolicyIDtoSecondOneDiff();
        environment.putString("secondPolicyId", "second_policy_id");
        environment.putString("policyId", "second_policy_id");

        run(cond);

        assertEquals("second_policy_id", environment.getString("resource_request_entity", "data.policyId"));
    }
}
