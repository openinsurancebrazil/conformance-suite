package net.openid.conformance.opin.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.opin.testmodule.support.CallResourceWithVersionHeaders;
import net.openid.conformance.opin.testmodule.support.EnsureResponseVersionHeaderMatchesRequested;
import net.openid.conformance.testmodule.Environment;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class CallResourceWithVersionHeadersTest extends AbstractJsonResponseConditionUnitTest {

    @Spy
    private Environment env = new Environment();
    @Mock
    private TestInstanceEventLog eventLog;
    private CallResourceWithVersionHeaders condition;


    @Before
    public void setUp() {
        condition = new CallResourceWithVersionHeaders();
        eventLog = mock(TestInstanceEventLog.class);
        condition.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
    }

    @Test
    public void EnsureVersionHeadersAreSetOnRequest() {
        env.putString("x-v","2.0.0");
        env.putString("x-min-v","2.0.0");
        env.putString("protected_resource_url","https://opin.tu.digiseg.com.br/open-insurance/products-services/v1/pension-plan");
        condition.execute(env);
    }


}
