package net.openid.conformance.opin.support.endorsementRequestBody;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateEndorsementRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putString("policyId", "policy_id");
    }

    @Test
    public void testRequestBodyAlteracao() {
        CreateEndorsementRequestBodyAlteracao cond = new CreateEndorsementRequestBodyAlteracao();

        run(cond);

        assertEquals(
                EndorsementType.ALTERACAO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertNotNull(environment.getString("resource_request_entity"));
        assertNull(environment.getString("resource_request_entity", "data.insuredObjectId"));
    }

    @Test
    public void testRequestBodyCancelamento() {
        CreateEndorsementRequestBodyCancelamento cond = new CreateEndorsementRequestBodyCancelamento();

        run(cond);

        assertEquals(
                EndorsementType.CANCELAMENTO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertNotNull(environment.getString("resource_request_entity"));
        assertNull(environment.getString("resource_request_entity", "data.insuredObjectId"));
    }

    @Test
    public void testRequestBodyExclusao() {
        CreateEndorsementRequestBodyExclusao cond = new CreateEndorsementRequestBodyExclusao();

        run(cond);

        assertEquals(
                EndorsementType.EXCLUSAO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertNotNull(environment.getString("resource_request_entity"));
        assertNull(environment.getString("resource_request_entity", "data.insuredObjectId"));
    }

    @Test
    public void testRequestBodyInclusao() {
        CreateEndorsementRequestBodyInclusao cond = new CreateEndorsementRequestBodyInclusao();

        run(cond);

        assertEquals(
                EndorsementType.INCLUSAO.name(),
                environment.getString("resource_request_entity", "data.endorsementType")
        );
        assertNotNull(environment.getString("resource_request_entity"));
        assertNull(environment.getString("resource_request_entity", "data.insuredObjectId"));
    }
}
