package net.openid.conformance.opin.support.endorsementRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoFirstOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoSecondOne;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetEndorsementPolicyIDTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        JsonObject requestData = new JsonObject();
        requestData.add("data", new JsonObject());
        environment.putObject("resource_request_entity", requestData);
    }

    @Test
    public void testFirstIdHappyPath() {
        SetEndorsementPolicyIDtoFirstOne cond = new SetEndorsementPolicyIDtoFirstOne();
        environment.putString("firstPolicyId", "first_policy_id");
        environment.putString("policyId", "first_policy_id");

        run(cond);

        assertEquals("first_policy_id", environment.getString("resource_request_entity", "data.policyNumber"));
    }

    @Test
    public void testSecondIdHappyPath() {
        SetEndorsementPolicyIDtoSecondOne cond = new SetEndorsementPolicyIDtoSecondOne();
        environment.putString("secondPolicyId", "second_policy_id");
        environment.putString("policyId", "second_policy_id");

        run(cond);

        assertEquals("second_policy_id", environment.getString("resource_request_entity", "data.policyNumber"));
    }
}
