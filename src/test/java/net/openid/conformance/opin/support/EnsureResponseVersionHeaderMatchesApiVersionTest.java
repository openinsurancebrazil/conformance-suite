package net.openid.conformance.opin.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.EnsureResponseVersionHeaderMatchesApiVersion;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@UseResurce("support/OkConfigConsents.json")
public class EnsureResponseVersionHeaderMatchesApiVersionTest extends  AbstractJsonResponseConditionUnitTest {

    @Test
    public void noVersionInResponseHeaders() {
        environment.putString("apiVersion","1.0.0");
        EnsureResponseVersionHeaderMatchesApiVersion condition = new EnsureResponseVersionHeaderMatchesApiVersion();
        ConditionError error = runAndFail(condition);
        String expected = "x-v header is missing from response";
        assertThat(error.getMessage(), containsString(expected));
    }

    @Test
    public void badFormatVersionInResponseHeaders() {
        environment.putString("apiVersion","1.1.0");
        setHeaders("x-v","1.1");
        EnsureResponseVersionHeaderMatchesApiVersion condition = new EnsureResponseVersionHeaderMatchesApiVersion();
        ConditionError error = runAndFail(condition);
        String expected = "x-v must be in the format: ^\\\\d+\\\\.\\\\d+\\\\.\\\\d+$";
        assertThat(error.getMessage(), containsString(expected));
    }
    @Test
    public void notCompatibleVersionInResponseHeaders() {
        environment.putString("apiVersion","2.0.0");
        setHeaders("x-v","1.0.0");
        EnsureResponseVersionHeaderMatchesApiVersion condition = new EnsureResponseVersionHeaderMatchesApiVersion();
        ConditionError error = runAndFail(condition);
        String expected = "Response version is lesser than API version";
        assertThat(error.getMessage(), containsString(expected));
    }
    @Test
    public void happyPath() {
        environment.putString("apiVersion","1.0.0");
        setHeaders("x-v","1.1.0");
        EnsureResponseVersionHeaderMatchesApiVersion condition = new EnsureResponseVersionHeaderMatchesApiVersion();
        run(condition);
    }

}
