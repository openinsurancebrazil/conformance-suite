package net.openid.conformance.opin.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.EnsurePageParameterIsIntegerAtLinks;
import net.openid.conformance.util.UseResurce;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;


public class EnsurePageParameterIsIntegerAtLinksTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("support/ProtectedResourceResponseWithInvalidPageUrlParameter.json")
    public void testInvalidPageParameterFormatFails() {
        EnsurePageParameterIsIntegerAtLinks condition = new EnsurePageParameterIsIntegerAtLinks();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(),containsString("URL Parameter is not valid"));
    }

    @Test
    @UseResurce("support/ProtectedResourceResponseWithValidPageUrlParameter.json")
    public void testValidPageParameterFormatSucceed() {
        EnsurePageParameterIsIntegerAtLinks condition = new EnsurePageParameterIsIntegerAtLinks();
        run(condition);
    }

}
