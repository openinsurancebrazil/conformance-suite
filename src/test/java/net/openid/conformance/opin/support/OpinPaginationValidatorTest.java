package net.openid.conformance.opin.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.OpinPaginationValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class OpinPaginationValidatorTest extends AbstractJsonResponseConditionUnitTest {

    @Before
    public void setUp() {
        environment.putString("api_type", AbstractOpinApiTestModule.API_TYPE);
        environment.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
    }

    //UNIT TESTS FOR SINGLE PAGE

    @Test
    @UseResurce("support/ProtectedResourcePaginationLifePensionContractsHappyPath.json")
    public void testHappyPathSingleLifePensionContracts() {
        environment.putString("protected_resource_url", "https://dev-api-mtls-mockin.opinbrasil.com.br/open-insurance/insurance-life-pension/v1/insurance-life-pension/contracts?page=1&page-size=25");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationInsurancePensionPlanContractHappyPath.json")
    public void testHappyPathSingleInsurancePensionPlanContracts() {
        environment.putString("protected_resource_url", "https://dev-api-mtls-mockin.opinbrasil.com.br/open-insurance/insurance-pension-plan/v1/insurance-pension-plan/contracts");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationLifePensionPortabilitiesHappyPath.json")
    public void testHappyPathSingleLifePensionPortabilities() {
        environment.putString("protected_resource_url", "https://dev-api-mtls-mockin.opinbrasil.com.br/open-insurance/insurance-life-pension/v1/insurance-life-pension/000250127118/portabilities?page=1&page-size=25");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationLifePensionPortabilitiesNoData.json")
    public void testHappyPathNoLifePensionPortabilities() {
        environment.putString("protected_resource_url", "https://dev-api-mtls-mockin.opinbrasil.com.br/open-insurance/insurance-life-pension/v1/insurance-life-pension/000250127118/portabilities?page=1&page-size=25");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationPensionPlanPortabilitiesHappyPath.json")
    public void testHappyPathSinglePensionPlanPortabilities() {
        environment.putString("protected_resource_url", "https://dev-api-mtls-mockin.opinbrasil.com.br/open-insurance/insurance-pension-plan/v1/insurance-pension-plan/000250127118/portabilities?page=1&page-size=25");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationPensionPlanPortabilitiesNoData.json")
    public void testHappyPathNoPensionPlanPortabilities() {
        environment.putString("protected_resource_url", "https://dev-api-mtls-mockin.opinbrasil.com.br/open-insurance/insurance-pension-plan/v1/insurance-pension-plan/000250127118/portabilities?page=1&page-size=25");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourceSinglePaginationHappyPath.json")
    public void testUnhappyPathSingleNoResourceUrl() {
        OpinPaginationValidator cond = new OpinPaginationValidator();

        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("resource url missing from configuration"));
    }

    @Test
    @UseResurce("support/ProtectedResourceSingleSelfDifferentFromFirstLink.json")
    public void testUnhappyPathSingleSelfDifferentFromFirstLink() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("Self link has to be the same as first/last links if they are present"));
    }

    @Test
    @UseResurce("support/ProtectedResourceSingleSelfDifferentFromLastLink.json")
    public void testUnhappyPathSingleSelfDifferentFromLastLink() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("Self link has to be the same as first/last links if they are present"));
    }

    @Test
    @UseResurce("opinResponses/consents/v2/createConsent/createConsentResponse.json")
    public void testHappyPathPostConsentSingle() {
        environment.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
        environment.putString("config", "resource.consentUrl", "https://api.organizacao.com.br/open-insurance/consents/v3/consents");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("opinResponses/consents/v2/getConsent/getConsentByIdResponse.json")
    public void testHappyPathNoAdditionalLinksExpected() {
        environment.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
        environment.putString("config", "resource.consentUrl", "https://api.organizacao.com.br/open-insurance/consents/v2/consents");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("There should not be prev/next links"));
    }

    @Test
    @UseResurce("opinResponses/consents/v2/createConsent/createConsentResponse.json")
    public void testHappyPathGetConsentSingle() {
        environment.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
        environment.putString("http_method", "GET");
        environment.putString("consent_url", "https://api.organizacao.com.br/open-insurance/consents/v3/consents");
        OpinPaginationValidator cond = new OpinPaginationValidator();

        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleHappyPathOnlySelfNoPageNum.json")
    public void testHappyPathSingleOnlySelfLinkNoPageNum() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleHappyPathOnlySelfNoPageNum.json")
    public void testHappyPathSelfLinkStartsWithRequestUrl() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleHappyPathSelfFirst.json")
    public void testHappyPathSingleSelfFirstSelfOnly() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourceSinglePaginationHappyPathEmptyDataObject.json")
    public void testHappyPathEmptyDataObject() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourceSinglePaginationHappyPathEmptyDataArray.json")
    public void testHappyPathEmptyDataArray() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleUnhappyPathWrongNbRecords.json")
    public void testUnhappyPathSingleWrongNbRecords() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("The actual number of records present in the response differs from the expected value provided on meta"));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleUnhappyPathWrongTotalRecords.json")
    public void testUnhappyPathSingleWrongTotalRecords() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("The actual number of records present in the response differs from the expected value provided on meta"));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleUnHappyPathPrevLink.json")
    public void testUnhappyPathSinglePrevLink() {
        environment.putString("protected_resource_url", "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("There should not be prev/next links"));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleUnHappyPathTotalRecordsPageSize.json")
    public void testUnhappyPathSingleTotalRecordsPageSize() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page-size=2");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("Number of records exceeds page size, but totalPages is not greater than 1"));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleHappyPathOnlySelfNoPageNum.json")
    public void testUnhappyPathSingleSelfDifferentFromRequest() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page-size=2");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("self link in the response is not the same as the request url."));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleHappyPathOnlySelfNoPageNum.json")
    public void testUnhappyPathSingleSelfDifferentFromGetRequest() {
        environment.putString("http_method", "GET");
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page-size=2");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("Request URI and 'self' link are not equal while request is not POST (when 'self' an ID, while request URI does not)"));
    }

    @Test
    @UseResurce("support/ProtectedResourceSinglePaginationUnhappyPathMismatchedSelfLink.json")
    public void testUnhappyPathSingleSelfMismatched() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("self link in the response is not the same as the request url."));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationHappyPathFirstPageAllLinks.json")
    public void testHappyPathMultipleFirstPage() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=1&page-size=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
       run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationHappyPathPageSize10Page2Records35.json")
    public void testHappyPathMultipleSecondPageNbRecords35Page4() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=2&page-size=10");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationHappyPathPageSize10Page4Records35.json")
    public void testHappyPathMultipleForthPageNbRecords35Page4() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=4&page-size=10");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationHappyPathPageSize10Page4Records35Last.json")
    public void testHappyPathMultipleForthPageNbRecords35Page4Last() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=4&page-size=10");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationHappyPathPageSize1Page2.json")
    public void testHappyPathMultipleSecondPage() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=2&page-size=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationHappyPathPageSizeSmallerThanInRequest.json")
    public void testHappyPathPageSizeSmallerThanInRequest() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=1&page-size=1000");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationUnhappyPathInvalidPrevLink.json")
    public void testUnhappyPathMultipleInvalidPrevLink() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=2&page-size=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("Prev link page does not point to the previous page."));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationUnhappyPathInvalidPageNumber.json")
    public void testUnhappyPathMultipleInvalidPageNum() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=2&page-size=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("The totalPages value is not equal to the amount of pages expected"));
    }

    @Test
    @UseResurce("support/ProtectedResourceSinglePaginationWithAdditionalField.json")
    public void validateAdditionalFields() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page=2&page-size=1");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("The element(s) [meta.requestDateTime] is(are) not present on the OpinPaginationValidator swagger and, therefore, should not be sent on the response since it does not accepts additional fields"));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationSingleUnhappyPathInvalidSelfLink.json")
    public void testUnhappyPathSingleInvalidSelfLink() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("OpinPaginationValidator: Value from element self doesn't match the required pattern on the OpinPaginationValidator API response"));

    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationUnhappyPathInvalidPageSize.json")
    public void testUnhappyPathSelfLinkWithInvalidPageSize() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("page-size cannot be greater than 1000"));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationUnhappyPathInvalidLastLink.json")
    public void testUnhappyPathBadLastLink() {
        environment.putString("protected_resource_url",  "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("Last link parameters are not according to the swagger specification"));
    }

    @Test
    @UseResurce("opinResponses/insurancePensionPlan/OpinInsurancePensionPlanPortabilitiesValidatorV1OK.json")
    public void testUnhappyPathPortabilityInfo() {
        environment.putString("protected_resource_url",  "https://api.organizacao.com.br/open-insurance/insurance-pension-plan/v1/insurance-pension-plan");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        ConditionError conditionError = runAndFail(cond);
        assertThat(conditionError.getMessage(), containsString("The actual number of records present in the response differs from the expected value provided on meta"));
    }

    @Test
    @UseResurce("opinResponses/insurancePensionPlan/OpinInsurancePensionPlanPortabilitiesValidatorV1OK2Records.json")
    public void testHappyPathPortabilityInfo() {
        environment.putString("protected_resource_url",  "https://api.organizacao.com.br/open-insurance/insurance-pension-plan/v1/insurance-pension-plan");
        OpinPaginationValidator cond = new OpinPaginationValidator();
        run(cond);
    }

}
