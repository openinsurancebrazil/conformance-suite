package net.openid.conformance.opin.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.FetchTwoPolicyIds;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FetchTwoPolicyIdsTest extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("support/ProtectedResourceSinglePaginationHappyPath.json")
    public void testHappyPath() {
        FetchTwoPolicyIds cond = new FetchTwoPolicyIds();
        run(cond);

        assertEquals("40d41d05-ab67-4fa3-8871-e8861c4fb31d", environment.getString("policyId"));
        assertEquals("40d41d05-ab67-4fa3-8871-e8861c4fb31d", environment.getString("firstPolicyId"));
        assertEquals("40d41d05-ab67-4fa3-8871-e8861c4fb31d", environment.getString("secondPolicyId"));
    }

    @Test
    @UseResurce("support/ProtectedResourcePaginationHappyPathPageSize1.json")
    public void testLessThanTwoResources() {
        FetchTwoPolicyIds cond = new FetchTwoPolicyIds();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("less then two resources"));
    }

    @Test
    public void testDataNotAnArray() {
        FetchTwoPolicyIds cond = new FetchTwoPolicyIds();
        environment.putString("resource_endpoint_response_full", "body", "{\"data\": {}}");

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("not a JsonArray"));
    }

    @Test
    public void testNoResponseBody() {
        FetchTwoPolicyIds cond = new FetchTwoPolicyIds();

        ConditionError error = runAndFail(cond);

        assertTrue(error.getMessage().contains("Could not extract body"));
    }
}
