package net.openid.conformance.opin.support;

import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

public class PermissionsGroupTest {

    @Test
    public void testGetPermissionsAll() {
        PermissionsGroup permissionsGroup = PermissionsGroup.ALL;
        String[] actualPermissions = permissionsGroup.getPermissions();
        for (String permission : actualPermissions) {
            System.out.println(permission);
        }
        String[] expectedPermissions = new String[]{
                "CAPITALIZATION_TITLE_EVENTS_READ",
                "CAPITALIZATION_TITLE_PLANINFO_READ",
                "CAPITALIZATION_TITLE_READ",
                "CAPITALIZATION_TITLE_SETTLEMENTS_READ",
                "CLAIM_NOTIFICATION_REQUEST_DAMAGE_CREATE",
                "CLAIM_NOTIFICATION_REQUEST_PERSON_CREATE",
                "CUSTOMERS_BUSINESS_ADDITIONALINFO_READ",
                "CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ",
                "CUSTOMERS_BUSINESS_QUALIFICATION_READ",
                "CUSTOMERS_PERSONAL_ADDITIONALINFO_READ",
                "CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ",
                "CUSTOMERS_PERSONAL_QUALIFICATION_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ",
                "DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_AUTO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ",
                "DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ",
                "DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_READ",
                "ENDORSEMENT_REQUEST_CREATE",
                "RESOURCES_READ",
                "PENSION_PLAN_READ",
                "PENSION_PLAN_CONTRACTINFO_READ",
                "PENSION_PLAN_MOVEMENTS_READ",
                "PENSION_PLAN_PORTABILITIES_READ",
                "PENSION_PLAN_WITHDRAWALS_READ",
                "PENSION_PLAN_CLAIM",
                "LIFE_PENSION_READ",
                "LIFE_PENSION_CONTRACTINFO_READ",
                "LIFE_PENSION_MOVEMENTS_READ",
                "LIFE_PENSION_PORTABILITIES_READ",
                "LIFE_PENSION_WITHDRAWALS_READ",
                "LIFE_PENSION_CLAIM",
                "FINANCIAL_ASSISTANCE_READ",
                "FINANCIAL_ASSISTANCE_CONTRACTINFO_READ",
                "FINANCIAL_ASSISTANCE_MOVEMENTS_READ",
                "QUOTE_PATRIMONIAL_LEAD_CREATE",
                "QUOTE_PATRIMONIAL_LEAD_UPDATE",
                "QUOTE_PERSON_LEAD_CREATE",
                "QUOTE_PERSON_LEAD_UPDATE",
                "QUOTE_AUTO_LEAD_CREATE",
                "QUOTE_AUTO_LEAD_UPDATE",
                "QUOTE_PATRIMONIAL_HOME_READ",
                "QUOTE_PATRIMONIAL_HOME_CREATE",
                "QUOTE_PATRIMONIAL_HOME_UPDATE",
                "QUOTE_PATRIMONIAL_BUSINESS_READ",
                "QUOTE_PATRIMONIAL_BUSINESS_CREATE",
                "QUOTE_PATRIMONIAL_BUSINESS_UPDATE",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_READ",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_CREATE",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_UPDATE",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_READ",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_CREATE",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_UPDATE",
                "QUOTE_PERSON_LIFE_READ",
                "QUOTE_PERSON_LIFE_CREATE",
                "QUOTE_PERSON_LIFE_UPDATE",
                "QUOTE_PERSON_TRAVEL_READ",
                "QUOTE_PERSON_TRAVEL_CREATE",
                "QUOTE_PERSON_TRAVEL_UPDATE",
                "CONTRACT_LIFE_PENSION_READ",
                "CONTRACT_LIFE_PENSION_CREATE",
                "CONTRACT_LIFE_PENSION_UPDATE",
                "CAPITALIZATION_TITLE_WITHDRAWAL_CREATE",
                "PENSION_WITHDRAWAL_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_READ",
                "QUOTE_CAPITALIZATION_TITLE_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_UPDATE",
                "QUOTE_CAPITALIZATION_TITLE_LEAD_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_LEAD_UPDATE",
                "QUOTE_CAPITALIZATION_TITLE_RAFFLE_CREATE",
                "PENSION_WITHDRAWAL_LEAD_CREATE"
        };


        assertThat(Arrays.asList(actualPermissions), containsInAnyOrder(expectedPermissions));
    }

    @Test
    public void testGetPermissionsAllPersonal() {
        PermissionsGroup permissionsGroup = PermissionsGroup.ALL_PERSONAL;
        String[] actualPermissions = permissionsGroup.getPermissions();
        String[] expectedPermissions = new String[]{
                "DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ",
                "CLAIM_NOTIFICATION_REQUEST_DAMAGE_CREATE",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ",
                "DAMAGES_AND_PEOPLE_PERSON_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_READ",
                "DAMAGES_AND_PEOPLE_RURAL_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ",
                "CUSTOMERS_PERSONAL_ADDITIONALINFO_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ",
                "DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ",
                "CUSTOMERS_PERSONAL_QUALIFICATION_READ",
                "DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ",
                "RESOURCES_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ",
                "CLAIM_NOTIFICATION_REQUEST_PERSON_CREATE",
                "CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ",
                "DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ",
                "ENDORSEMENT_REQUEST_CREATE",
                "DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ",
                "CAPITALIZATION_TITLE_READ",
                "CAPITALIZATION_TITLE_PLANINFO_READ",
                "CAPITALIZATION_TITLE_EVENTS_READ",
                "CAPITALIZATION_TITLE_SETTLEMENTS_READ",
                "PENSION_PLAN_READ",
                "PENSION_PLAN_CONTRACTINFO_READ",
                "PENSION_PLAN_MOVEMENTS_READ",
                "PENSION_PLAN_PORTABILITIES_READ",
                "PENSION_PLAN_WITHDRAWALS_READ",
                "PENSION_PLAN_CLAIM",
                "LIFE_PENSION_READ",
                "LIFE_PENSION_CONTRACTINFO_READ",
                "LIFE_PENSION_MOVEMENTS_READ",
                "LIFE_PENSION_PORTABILITIES_READ",
                "LIFE_PENSION_WITHDRAWALS_READ",
                "LIFE_PENSION_CLAIM",
                "FINANCIAL_ASSISTANCE_READ",
                "FINANCIAL_ASSISTANCE_CONTRACTINFO_READ",
                "FINANCIAL_ASSISTANCE_MOVEMENTS_READ",
                "QUOTE_PATRIMONIAL_LEAD_CREATE",
                "QUOTE_PATRIMONIAL_LEAD_UPDATE",
                "QUOTE_PERSON_LEAD_CREATE",
                "QUOTE_PERSON_LEAD_UPDATE",
                "QUOTE_AUTO_LEAD_CREATE",
                "QUOTE_AUTO_LEAD_UPDATE",
                "QUOTE_PATRIMONIAL_HOME_READ",
                "QUOTE_PATRIMONIAL_HOME_CREATE",
                "QUOTE_PATRIMONIAL_HOME_UPDATE",
                "QUOTE_PATRIMONIAL_BUSINESS_READ",
                "QUOTE_PATRIMONIAL_BUSINESS_CREATE",
                "QUOTE_PATRIMONIAL_BUSINESS_UPDATE",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_READ",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_CREATE",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_UPDATE",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_READ",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_CREATE",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_UPDATE",
                "QUOTE_PERSON_LIFE_READ",
                "QUOTE_PERSON_LIFE_CREATE",
                "QUOTE_PERSON_LIFE_UPDATE",
                "QUOTE_PERSON_TRAVEL_READ",
                "QUOTE_PERSON_TRAVEL_CREATE",
                "QUOTE_PERSON_TRAVEL_UPDATE",
                "CONTRACT_LIFE_PENSION_READ",
                "CONTRACT_LIFE_PENSION_CREATE",
                "CONTRACT_LIFE_PENSION_UPDATE",
                "CAPITALIZATION_TITLE_WITHDRAWAL_CREATE",
                "PENSION_WITHDRAWAL_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_READ",
                "QUOTE_CAPITALIZATION_TITLE_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_UPDATE",
                "QUOTE_CAPITALIZATION_TITLE_LEAD_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_LEAD_UPDATE",
                "QUOTE_CAPITALIZATION_TITLE_RAFFLE_CREATE",
                "PENSION_WITHDRAWAL_LEAD_CREATE"
        };

        assertThat(Arrays.asList(actualPermissions), containsInAnyOrder(expectedPermissions));
    }

    @Test
    public void testGetPermissionsAllBusiness() {
        PermissionsGroup permissionsGroup = PermissionsGroup.ALL_BUSINESS;
        String[] actualPermissions = permissionsGroup.getPermissions();
        String[] expectedPermissions = new String[]{
                "DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ",
                "CLAIM_NOTIFICATION_REQUEST_DAMAGE_CREATE",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ",
                "DAMAGES_AND_PEOPLE_PERSON_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_READ",
                "DAMAGES_AND_PEOPLE_RURAL_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ",
                "CUSTOMERS_BUSINESS_ADDITIONALINFO_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ",
                "DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ",
                "CUSTOMERS_BUSINESS_QUALIFICATION_READ",
                "DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ",
                "RESOURCES_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ",
                "CLAIM_NOTIFICATION_REQUEST_PERSON_CREATE",
                "CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ",
                "DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ",
                "ENDORSEMENT_REQUEST_CREATE",
                "DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ",
                "CAPITALIZATION_TITLE_READ",
                "CAPITALIZATION_TITLE_PLANINFO_READ",
                "CAPITALIZATION_TITLE_EVENTS_READ",
                "CAPITALIZATION_TITLE_SETTLEMENTS_READ",
                "PENSION_PLAN_READ",
                "PENSION_PLAN_CONTRACTINFO_READ",
                "PENSION_PLAN_MOVEMENTS_READ",
                "PENSION_PLAN_PORTABILITIES_READ",
                "PENSION_PLAN_WITHDRAWALS_READ",
                "PENSION_PLAN_CLAIM",
                "LIFE_PENSION_READ",
                "LIFE_PENSION_CONTRACTINFO_READ",
                "LIFE_PENSION_MOVEMENTS_READ",
                "LIFE_PENSION_PORTABILITIES_READ",
                "LIFE_PENSION_WITHDRAWALS_READ",
                "LIFE_PENSION_CLAIM",
                "FINANCIAL_ASSISTANCE_READ",
                "FINANCIAL_ASSISTANCE_CONTRACTINFO_READ",
                "FINANCIAL_ASSISTANCE_MOVEMENTS_READ",
                "QUOTE_PATRIMONIAL_LEAD_CREATE",
                "QUOTE_PATRIMONIAL_LEAD_UPDATE",
                "QUOTE_PERSON_LEAD_CREATE",
                "QUOTE_PERSON_LEAD_UPDATE",
                "QUOTE_AUTO_LEAD_CREATE",
                "QUOTE_AUTO_LEAD_UPDATE",
                "QUOTE_PATRIMONIAL_HOME_READ",
                "QUOTE_PATRIMONIAL_HOME_CREATE",
                "QUOTE_PATRIMONIAL_HOME_UPDATE",
                "QUOTE_PATRIMONIAL_BUSINESS_READ",
                "QUOTE_PATRIMONIAL_BUSINESS_CREATE",
                "QUOTE_PATRIMONIAL_BUSINESS_UPDATE",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_READ",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_CREATE",
                "QUOTE_PATRIMONIAL_DIVERSE_RISKS_UPDATE",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_READ",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_CREATE",
                "QUOTE_PATRIMONIAL_CONDOMINIUM_UPDATE",
                "QUOTE_PERSON_LIFE_READ",
                "QUOTE_PERSON_LIFE_CREATE",
                "QUOTE_PERSON_LIFE_UPDATE",
                "QUOTE_PERSON_TRAVEL_READ",
                "QUOTE_PERSON_TRAVEL_CREATE",
                "QUOTE_PERSON_TRAVEL_UPDATE",
                "CONTRACT_LIFE_PENSION_READ",
                "CONTRACT_LIFE_PENSION_CREATE",
                "CONTRACT_LIFE_PENSION_UPDATE",
                "CAPITALIZATION_TITLE_WITHDRAWAL_CREATE",
                "PENSION_WITHDRAWAL_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_READ",
                "QUOTE_CAPITALIZATION_TITLE_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_UPDATE",
                "QUOTE_CAPITALIZATION_TITLE_LEAD_CREATE",
                "QUOTE_CAPITALIZATION_TITLE_LEAD_UPDATE",
                "QUOTE_CAPITALIZATION_TITLE_RAFFLE_CREATE",
                "PENSION_WITHDRAWAL_LEAD_CREATE"
        };

        assertThat(Arrays.asList(actualPermissions), containsInAnyOrder(expectedPermissions));
    }

    @Test
    public void testGetPermissionsAllPersonalPhase2() {
        PermissionsGroup permissionsGroup = PermissionsGroup.ALL_PERSONAL_PHASE2;
        String[] actualPermissions = permissionsGroup.getPermissions();
        String[] expectedPermissions = new String[]{
                "DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ",
                "DAMAGES_AND_PEOPLE_PERSON_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_READ",
                "DAMAGES_AND_PEOPLE_RURAL_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ",
                "CUSTOMERS_PERSONAL_ADDITIONALINFO_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ",
                "DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ",
                "CUSTOMERS_PERSONAL_QUALIFICATION_READ",
                "DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ",
                "RESOURCES_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ",
                "CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ",
                "DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ",
                "CAPITALIZATION_TITLE_READ",
                "CAPITALIZATION_TITLE_PLANINFO_READ",
                "CAPITALIZATION_TITLE_EVENTS_READ",
                "CAPITALIZATION_TITLE_SETTLEMENTS_READ",
                "PENSION_PLAN_READ",
                "PENSION_PLAN_CONTRACTINFO_READ",
                "PENSION_PLAN_MOVEMENTS_READ",
                "PENSION_PLAN_PORTABILITIES_READ",
                "PENSION_PLAN_WITHDRAWALS_READ",
                "PENSION_PLAN_CLAIM",
                "LIFE_PENSION_READ",
                "LIFE_PENSION_CONTRACTINFO_READ",
                "LIFE_PENSION_MOVEMENTS_READ",
                "LIFE_PENSION_PORTABILITIES_READ",
                "LIFE_PENSION_WITHDRAWALS_READ",
                "LIFE_PENSION_CLAIM",
                "FINANCIAL_ASSISTANCE_READ",
                "FINANCIAL_ASSISTANCE_CONTRACTINFO_READ",
                "FINANCIAL_ASSISTANCE_MOVEMENTS_READ",
        };

        assertThat(Arrays.asList(actualPermissions), containsInAnyOrder(expectedPermissions));
    }

    @Test
    public void testGetPermissionsAllBusinessPhase2() {
        PermissionsGroup permissionsGroup = PermissionsGroup.ALL_BUSINESS_PHASE2;
        String[] actualPermissions = permissionsGroup.getPermissions();
        String[] expectedPermissions = new String[]{
                "DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ",
                "DAMAGES_AND_PEOPLE_PERSON_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_READ",
                "DAMAGES_AND_PEOPLE_RURAL_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ",
                "CUSTOMERS_BUSINESS_ADDITIONALINFO_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ",
                "DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ",
                "CUSTOMERS_BUSINESS_QUALIFICATION_READ",
                "DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ",
                "RESOURCES_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ",
                "CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ",
                "DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ",
                "CAPITALIZATION_TITLE_READ",
                "CAPITALIZATION_TITLE_PLANINFO_READ",
                "CAPITALIZATION_TITLE_EVENTS_READ",
                "CAPITALIZATION_TITLE_SETTLEMENTS_READ",
                "PENSION_PLAN_READ",
                "PENSION_PLAN_CONTRACTINFO_READ",
                "PENSION_PLAN_MOVEMENTS_READ",
                "PENSION_PLAN_PORTABILITIES_READ",
                "PENSION_PLAN_WITHDRAWALS_READ",
                "PENSION_PLAN_CLAIM",
                "LIFE_PENSION_READ",
                "LIFE_PENSION_CONTRACTINFO_READ",
                "LIFE_PENSION_MOVEMENTS_READ",
                "LIFE_PENSION_PORTABILITIES_READ",
                "LIFE_PENSION_WITHDRAWALS_READ",
                "LIFE_PENSION_CLAIM",
                "FINANCIAL_ASSISTANCE_READ",
                "FINANCIAL_ASSISTANCE_CONTRACTINFO_READ",
                "FINANCIAL_ASSISTANCE_MOVEMENTS_READ",
        };


        assertThat(Arrays.asList(actualPermissions), containsInAnyOrder(expectedPermissions));
    }

    @Test
    public void testGetPermissionsCustomersBusiness() {
        PermissionsGroup permissionsGroup = PermissionsGroup.CUSTOMERS_BUSINESS;
        String[] actualPermissions = permissionsGroup.getPermissions();
        String[] expectedPermissions = new String[]{
                "CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ", "CUSTOMERS_BUSINESS_QUALIFICATION_READ",
                "CUSTOMERS_BUSINESS_ADDITIONALINFO_READ", "RESOURCES_READ"
        };

        assertArrayEquals(expectedPermissions, actualPermissions);
    }

    @Test
    public void testGetPermissionsAllPhase2() {
        PermissionsGroup permissionsGroup = PermissionsGroup.ALL_PHASE2;
        String[] actualPermissions = permissionsGroup.getPermissions();
        for (String permission : actualPermissions) {
            System.out.println(permission);
        }
        String[] expectedPermissions = new String[]{
                "CAPITALIZATION_TITLE_EVENTS_READ",
                "CAPITALIZATION_TITLE_PLANINFO_READ",
                "CAPITALIZATION_TITLE_READ",
                "CAPITALIZATION_TITLE_SETTLEMENTS_READ",
                "CUSTOMERS_BUSINESS_ADDITIONALINFO_READ",
                "CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ",
                "CUSTOMERS_BUSINESS_QUALIFICATION_READ",
                "CUSTOMERS_PERSONAL_ADDITIONALINFO_READ",
                "CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ",
                "CUSTOMERS_PERSONAL_QUALIFICATION_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ",
                "DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_AUTO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_HOUSING_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ",
                "DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_PERSON_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ",
                "DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_RURAL_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ",
                "DAMAGES_AND_PEOPLE_TRANSPORT_READ",
                "RESOURCES_READ",
                "PENSION_PLAN_READ",
                "PENSION_PLAN_CONTRACTINFO_READ",
                "PENSION_PLAN_MOVEMENTS_READ",
                "PENSION_PLAN_PORTABILITIES_READ",
                "PENSION_PLAN_WITHDRAWALS_READ",
                "PENSION_PLAN_CLAIM",
                "LIFE_PENSION_READ",
                "LIFE_PENSION_CONTRACTINFO_READ",
                "LIFE_PENSION_MOVEMENTS_READ",
                "LIFE_PENSION_PORTABILITIES_READ",
                "LIFE_PENSION_WITHDRAWALS_READ",
                "LIFE_PENSION_CLAIM",
                "FINANCIAL_ASSISTANCE_READ",
                "FINANCIAL_ASSISTANCE_CONTRACTINFO_READ",
                "FINANCIAL_ASSISTANCE_MOVEMENTS_READ"
        };


        assertThat(Arrays.asList(actualPermissions), containsInAnyOrder(expectedPermissions));
    }
}