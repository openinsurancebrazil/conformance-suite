package net.openid.conformance.opin.PCM;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.PCM.consent.funnel.PcmConsentFunnelPostClientBatchPayloadValidatorV1;
import net.openid.conformance.opin.validator.PCM.consent.funnel.PcmConsentFunnelPostClientEventPayloadValidatorV1;
import net.openid.conformance.opin.validator.PCM.consent.funnel.PcmConsentFunnelPostServerBatchPayloadValidatorV1;
import net.openid.conformance.opin.validator.PCM.consent.funnel.PcmConsentFunnelPostServerEventPayloadValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PCMConsentFunnelApiPayloadTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value = "opinResponses/PCM/PostClientBatchPayload.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureClientBatch() {
		PcmConsentFunnelPostClientBatchPayloadValidatorV1 condition = new PcmConsentFunnelPostClientBatchPayloadValidatorV1();
		run(condition);
	}

	@Test
	@UseResurce(value = "opinResponses/PCM/PostClientEventPayload.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureClientEvent() {
		PcmConsentFunnelPostClientEventPayloadValidatorV1 condition = new PcmConsentFunnelPostClientEventPayloadValidatorV1();
		run(condition);
	}

	@Test
	@UseResurce(value = "opinResponses/PCM/PostServerBatchPayload.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureServerBatch() {
		PcmConsentFunnelPostServerBatchPayloadValidatorV1 condition = new PcmConsentFunnelPostServerBatchPayloadValidatorV1();
		run(condition);
	}

	@Test
	@UseResurce(value = "opinResponses/PCM/PostServerEventPayload.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureServerEvent() {
		PcmConsentFunnelPostServerEventPayloadValidatorV1 condition = new PcmConsentFunnelPostServerEventPayloadValidatorV1();
		run(condition);
	}
}
