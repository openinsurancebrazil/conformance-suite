package net.openid.conformance.opin.PCM;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.PCM.metrics.PcmMetricPostClientBatchPayloadValidatorV1;
import net.openid.conformance.opin.validator.PCM.metrics.PcmMetricPostClientEventPayloadValidatorV1;
import net.openid.conformance.opin.validator.PCM.metrics.PcmMetricPostServerBatchPayloadValidatorV1;
import net.openid.conformance.opin.validator.PCM.metrics.PcmMetricPostServerEventPayloadValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PCMMetriclApiPayloadTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value = "pcmRequests/metricBatchRequest.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureClientBatch() {
		PcmMetricPostClientBatchPayloadValidatorV1 condition = new PcmMetricPostClientBatchPayloadValidatorV1();
		run(condition);
	}

	@Test
	@UseResurce(value = "pcmRequests/metricEventRequest.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureClientEvent() {
		PcmMetricPostClientEventPayloadValidatorV1 condition = new PcmMetricPostClientEventPayloadValidatorV1();
		run(condition);
	}

	@Test
	@UseResurce(value = "pcmRequests/metricBatchRequest.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureServerBatch() {
		PcmMetricPostServerBatchPayloadValidatorV1 condition = new PcmMetricPostServerBatchPayloadValidatorV1();
		run(condition);
	}

	@Test
	@UseResurce(value = "pcmRequests/metricEventRequest.json", key = "received_request", path = "body", isAsString = true)
	public void validateStructureServerEvent() {
		PcmMetricPostServerEventPayloadValidatorV1 condition = new PcmMetricPostServerEventPayloadValidatorV1();
		run(condition);
	}
}
