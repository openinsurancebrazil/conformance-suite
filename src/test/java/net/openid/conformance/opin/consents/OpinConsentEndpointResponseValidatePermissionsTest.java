package net.openid.conformance.opin.consents;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.opin.testmodule.support.OpinConsentEndpointResponseValidatePermissions;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class OpinConsentEndpointResponseValidatePermissionsTest extends AbstractJsonResponseConditionUnitTest {


	@UseResurce("opinResponses/consents/v2/createConsent/createConsentResponseWithPatrimonialPermissions.json")
	@Test
	public void success() {
		OpinConsentEndpointResponseValidatePermissions condition = new OpinConsentEndpointResponseValidatePermissions();

		JsonArray requestPerms = new JsonArray();
		for (String s: new String[] { "RESOURCES_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ","DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ","DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ"}) {
			requestPerms.add(s);
		}
		JsonObject brazil = new JsonObject();
		brazil.add("requested_permissions", requestPerms);

		environment.putObject("brazil_consent", brazil);

		run(condition);
	}

	@UseResurce("opinResponses/consents/v2/createConsent/createConsentResponseWithPatrimonialPermissions.json")
	@Test
	public void successDifferentOrder() {
		OpinConsentEndpointResponseValidatePermissions condition = new OpinConsentEndpointResponseValidatePermissions();

		JsonArray requestPerms = new JsonArray();
		for (String s: new String[] { "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ", "RESOURCES_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ","DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ"}) {
			requestPerms.add(s);
		}
		JsonObject brazil = new JsonObject();
		brazil.add("requested_permissions", requestPerms);

		environment.putObject("brazil_consent", brazil);

		run(condition);
	}

	@UseResurce("opinResponses/consents/v2/createConsent/createConsentResponseWithPatrimonialPermissionsAndExtra.json")
	@Test
	public void extraGranted() {
		OpinConsentEndpointResponseValidatePermissions condition = new OpinConsentEndpointResponseValidatePermissions();

		JsonArray requestPerms = new JsonArray();
		for (String s: new String[] { "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ", "RESOURCES_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ","DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ"}) {
			requestPerms.add(s);
		}
		JsonObject brazil = new JsonObject();
		brazil.add("requested_permissions", requestPerms);

		environment.putObject("brazil_consent", brazil);

		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Consent endpoint response contains different permissions than requested"));
	}

	@UseResurce("opinResponses/consents/v2/createConsent/createConsentResponseWithMissingPatrimonialPermissions.json")
	@Test
	public void grantedFewerThanRequested() {
		OpinConsentEndpointResponseValidatePermissions condition = new OpinConsentEndpointResponseValidatePermissions();

		JsonArray requestPerms = new JsonArray();
		for (String s: new String[] { "DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ", "RESOURCES_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ", "DAMAGES_AND_PEOPLE_PATRIMONIAL_READ","DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ"}) {
			requestPerms.add(s);
		}
		JsonObject brazil = new JsonObject();
		brazil.add("requested_permissions", requestPerms);

		environment.putObject("brazil_consent", brazil);

		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Consent endpoint response is not a complete grouping"));
	}


}
