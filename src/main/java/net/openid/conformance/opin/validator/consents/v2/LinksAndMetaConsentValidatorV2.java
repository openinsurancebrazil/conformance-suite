package net.openid.conformance.opin.validator.consents.v2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

public class LinksAndMetaConsentValidatorV2 {

	private final AbstractJsonAssertingCondition validator;

	public LinksAndMetaConsentValidatorV2(AbstractJsonAssertingCondition validator) {
		this.validator = validator;
	}

	public void assertMetaAndLinks(JsonElement body) {
		validator.assertField(body,
			new ObjectField
				.Builder("links")
				.setValidator(this::assertLinks)
				.setOptional()
				.build());

		validator.assertField(body,
			new ObjectField
				.Builder("meta")
				.setValidator(this::assertMeta)
				.setOptional()
				.build());
	}

	public void assertLinks(JsonObject links) {
		validator.assertField(links,
			new StringField
				.Builder("self")
				.setMaxLength(2000)
				.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/consents\\/v\\d+)(\\/.*)?$")
				.build());

		validator.assertField(links,
			new StringField
				.Builder("first")
				.setOptional()
				.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/consents\\/v\\d+)(\\/.*)?$")
				.setMaxLength(2000)
				.build());

		validator.assertField(links,
			new StringField
				.Builder("prev")
				.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/consents\\/v\\d+)(\\/.*)?$")
				.setMaxLength(2000)
				.setOptional()
				.build());

		validator.assertField(links,
			new StringField
				.Builder("next")
					.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/consents\\/v\\d+)(\\/.*)?$")
				.setMaxLength(2000)
				.setOptional()
				.build());

		validator.assertField(links,
			new StringField
				.Builder("last")
				.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/consents\\/v\\d+)(\\/.*)?$")
				.setMaxLength(2000)
				.setOptional()
				.build());
	}

	public void assertMeta(JsonObject meta) {
		validator.assertField(meta,
			new IntField
				.Builder("totalRecords")
				.build());

		validator.assertField(meta,
			new IntField
				.Builder("totalPages")
				.build());
	}
}
