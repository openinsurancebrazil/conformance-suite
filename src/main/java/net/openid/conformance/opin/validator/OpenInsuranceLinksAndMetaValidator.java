package net.openid.conformance.opin.validator;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.opin.testmodule.support.OpinLinksValidator;
import net.openid.conformance.opin.testmodule.support.OpinMetaValidator;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

public class OpenInsuranceLinksAndMetaValidator {

    private final AbstractJsonAssertingCondition validator;
    private String pattern;

    public OpenInsuranceLinksAndMetaValidator(AbstractJsonAssertingCondition validator) {
        this.validator = validator;
    }

    public void assertMetaAndLinks(JsonElement body) {
        validator.assertField(body,
                new ObjectField
                        .Builder("links")
                        .setValidator(this::assertLinks)
                        .build());

        validator.assertField(body,
                new ObjectField
                        .Builder("meta")
                        .setValidator(this::assertMeta)
                        .build());
    }

    @Deprecated
    public void assertMetaAndLinksWithPattern(JsonElement body, String pattern) {
        this.pattern = pattern;
        validator.assertField(body,
                new ObjectField
                        .Builder("meta")
                        .setValidator(this::assertMeta)
                        .build());

        validator.assertField(body,
                new ObjectField
                        .Builder("links")
                        .setValidator(this::assertLinksWithPattern)
                        .build());
    }

    public void assertMetaAndLinksWithPattern(JsonElement body, String requestUri, String pattern) {
        this.pattern = pattern;
        new OpinMetaValidator(this.validator, true, true).assertMetaObject(body);
        new OpinLinksValidator(this.validator, this.pattern).assertLinksObject(body, requestUri, 1);
    }

    public void assertLinksWithPattern(JsonObject links) {
        validator.assertField(links,
                new StringField
                        .Builder("self")
                        .setPattern(this.pattern)
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("first")
                        .setPattern(this.pattern)
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("prev")
                        .setPattern(this.pattern)
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("next")
                        .setPattern(this.pattern)
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("last")
                        .setPattern(this.pattern)
                        .setOptional()
                        .build());
    }

    public void assertLinks(JsonObject links) {
        validator.assertField(links,
                new StringField
                        .Builder("self")
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("first")
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("prev")
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("next")
                        .setOptional()
                        .build());

        validator.assertField(links,
                new StringField
                        .Builder("last")
                        .setOptional()
                        .build());
    }

    public void assertMeta(JsonObject meta) {
        validator.assertField(meta,
                new IntField
                .Builder("totalRecords")
                .build());

        validator.assertField(meta,
                new IntField
                .Builder("totalPages")
                .build());
    }
}
