package net.openid.conformance.opin.validator.PCM.metrics;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.NumberField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;


/**
 * Api Swagger URL: https://br-openinsurance.github.io/areadesenvolvedor/files/swagger/ingestion.yaml
 * Api endpoint: /event-api/v1/client-event
 * Api version: 1.0.10
 */

@ApiName("PCM Server Event Payload")
public class PcmMetricPostClientEventPayloadValidatorV1 extends AbstractJsonAssertingCondition {

    private static final Set<String> ENDPOINT_ENUM = SetUtils.createSet("/open-insurance/channels/v1/branches, /open-insurance/channels/v1/electronic-channels, /open-insurance/channels/v1/intermediary/{countrySubDivision}, /open-insurance/channels/v1/phone-channels, /open-insurance/channels/v1/referenced-network/{countrySubDivision}/{serviceType}, /open-insurance/products-services/v1/assistance-general-assets, /open-insurance/products-services/v1/auto-extended-warranty, /open-insurance/products-services/v1/auto-insurance/{commercializationArea}/{fipeCode}/{year}, /open-insurance/products-services/v1/business, /open-insurance/products-services/v1/capitalization-title, /open-insurance/products-services/v1/condominium, /open-insurance/products-services/v1/cyber-risk, /open-insurance/products-services/v1/directors-officers-liability, /open-insurance/products-services/v1/domestic-credit, /open-insurance/products-services/v1/engineering, /open-insurance/products-services/v1/environmental-liability, /open-insurance/products-services/v1/equipment-breakdown, /open-insurance/products-services/v1/errors-omissions-liability, /open-insurance/products-services/v1/export-credit, /open-insurance/products-services/v1/extended-warranty, /open-insurance/products-services/v1/financial-risk, /open-insurance/products-services/v1/general-liability, /open-insurance/products-services/v1/global-banking, /open-insurance/products-services/v1/home-insurance/commercializationArea/{commercializationArea}, /open-insurance/products-services/v1/housing, /open-insurance/products-services/v1/life-pension, /open-insurance/products-services/v1/lost-profit, /open-insurance/products-services/v1/named-operational-risks, /open-insurance/products-services/v1/pension-plan, /open-insurance/products-services/v1/person, /open-insurance/products-services/v1/private-guarantee, /open-insurance/products-services/v1/public-guarantee, /open-insurance/products-services/v1/rent-guarantee, /open-insurance/products-services/v1/rural, /open-insurance/products-services/v1/stop-loss, /open-insurance/products-services/v1/transport, /open-insurance/consents/v1/consents, /open-insurance/consents/v1/consents/{consentId}, /open-insurance/resources/v1/resources, /open-insurance/customers/v1/personal/identifications, /open-insurance/customers/v1/personal/qualifications, /open-insurance/customers/v1/personal/complimentary-information, /open-insurance/customers/v1/business/identifications, /open-insurance/customers/v1/business/qualifications, /open-insurance/customers/v1/business/complimentary-information, /open-insurance/insurance-acceptance-and-branches-abroad/v1/insurance-acceptance-and-branches-abroad, /open-insurance/insurance-acceptance-and-branches-abroad/v1/insurance-acceptance-and-branches-abroad/{policyId}/policy-info, /open-insurance/insurance-acceptance-and-branches-abroad/v1/insurance-acceptance-and-branches-abroad/{policyId}/premium, /open-insurance/insurance-acceptance-and-branches-abroad/v1/insurance-acceptance-and-branches-abroad/{policyId}/claim, /open-insurance/insurance-auto/v1/insurance-auto, /open-insurance/insurance-auto/v1/insurance-auto/{policyId}/policy-info, /open-insurance/insurance-auto/v1/insurance-auto/{policyId}/premium, /open-insurance/insurance-auto/v1/insurance-auto/{policyId}/claim, /open-insurance/insurance-financial-risk/v1/insurance-financial-risk, /open-insurance/insurance-financial-risk/v1/insurance-financial-risk/{policyId}/policy-info, /open-insurance/insurance-financial-risk/v1/insurance-financial-risk/{policyId}/premium, /open-insurance/insurance-financial-risk/v1/insurance-financial-risk/{policyId}/claim, /open-insurance/insurance-patrimonial/v1/insurance-patrimonial, /open-insurance/insurance-patrimonial/v1/insurance-patrimonial/{policyId}/policy-info, /open-insurance/insurance-patrimonial/v1/insurance-patrimonial/{policyId}/premium, /open-insurance/insurance-patrimonial/v1/insurance-patrimonial/{policyId}/claim, /open-insurance/insurance-responsibility/v1/insurance-responsibility, /open-insurance/insurance-responsibility/v1/insurance-responsibility/{policyId}/policy-info, /open-insurance/insurance-responsibility/v1/insurance-responsibility/{policyId}/premium, /open-insurance/insurance-responsibility/v1/insurance-responsibility/{policyId}/claim, /open-insurance/insurance-rural/v1/insurance-rural, /open-insurance/insurance-rural/v1/insurance-rural/{policyId}/policy-info, /open-insurance/insurance-rural/v1/insurance-rural/{policyId}/premium, /open-insurance/insurance-rural/v1/insurance-rural/{policyId}/claim, /open-insurance/insurance-housing/v1/insurance-housing, /open-insurance/insurance-housing/v1/insurance-housing/{policyId}/policy-info, /open-insurance/insurance-housing/v1/insurance-housing/{policyId}/premium, /open-insurance/insurance-housing/v1/insurance-housing/{policyId}/claim, /open-insurance/insurance-transport/v1/insurance-transport, /open-insurance/insurance-transport/v1/insurance-transport/{policyId}/policy-info, /open-insurance/insurance-transport/v1/insurance-transport/{policyId}/premium, /open-insurance/insurance-transport/v1/insurance-transport/{policyId}/claim, /open-insurance/claim-notification/v1/request/damage/{consentId}, /open-insurance/claim-notification/v1/request/person/{consentId}, /open-insurance/endorsement/v1/request/{consentId}, /open-insurance/quote-patrimonial/v1/lead/request, /open-insurance/quote-patrimonial/v1/lead/request/{consentId}, /open-insurance/quote-patrimonial/v1/home/request/{consentId}/quote-status, /open-insurance/quote-patrimonial/v1/home/request, /open-insurance/quote-patrimonial/v1/home/request/{consentId}, /open-insurance/quote-patrimonial/v1/condominium/request/{consentId}/quote-status, /open-insurance/quote-patrimonial/v1/condominium/request, /open-insurance/quote-patrimonial/v1/condominium/request/{consentId}, /open-insurance/quote-patrimonial/v1/business/request/{consentId}/quote-status, /open-insurance/quote-patrimonial/v1/business/request, /open-insurance/quote-patrimonial/v1/business/request/{consentId}, /open-insurance/quote-patrimonial/v1/diverse-risks/request/{consentId}/quote-status, /open-insurance/quote-patrimonial/v1/diverse-risks/request, /open-insurance/quote-patrimonial/v1/diverse-risks/request/{consentId}, /open-insurance/insurance-capitalization-title/v1/insurance-capitalization-title, /open-insurance/insurance-capitalization-title/v1/insurance-capitalization-title/plans, /open-insurance/insurance-capitalization-title/v1/insurance-capitalization-title/{planId}/plan-info, /open-insurance/insurance-capitalization-title/v1/insurance-capitalization-title/{planId}/events, /open-insurance/insurance-capitalization-title/v1/insurance-capitalization-title/{planId}/settlements, /open-insurance/insurance-financial-assistance/v1/insurance-financial-assistance/contracts, /open-insurance/insurance-financial-assistance/v1/insurance-financial-assistance/{contractId}/contract-info, /open-insurance/insurance-financial-assistance/v1/insurance-financial-assistance/{contractId}/movements, /open-insurance/insurance-life-pension/v1/insurance-life-pension/contracts, /open-insurance/insurance-life-pension/v1/insurance-life-pension, /open-insurance/insurance-life-pension/v1/insurance-life-pension/{pensionIdentification}/contract-info, /open-insurance/insurance-life-pension/v1/insurance-life-pension/{pensionIdentification}/movements, /open-insurance/insurance-life-pension/v1/insurance-life-pension/{pensionIdentification}/portabilities, /open-insurance/insurance-life-pension/v1/insurance-life-pension/{pensionIdentification}/withdrawals, /open-insurance/insurance-life-pension/v1/insurance-life-pension/{pensionIdentification}/claim, /open-insurance/insurance-pension-plan/v1/insurance-pension-plan, /open-insurance/insurance-pension-plan/v1/insurance-pension-plan/contracts, /open-insurance/insurance-pension-plan/v1/insurance-pension-plan/{pensionIdentification}/contract-info, /open-insurance/insurance-pension-plan/v1/insurance-pension-plan/{pensionIdentification}/movements, /open-insurance/insurance-pension-plan/v1/insurance-pension-plan/{pensionIdentification}/portabilities, /open-insurance/insurance-pension-plan/v1/insurance-pension-plan/{pensionIdentification}/withdrawals, /open-insurance/insurance-pension-plan/v1/insurance-pension-plan/{pensionIdentification}/claim, /open-insurance/insurance-person/v1/insurance-person, /open-insurance/insurance-person/v1/insurance-person/{policyId}/policy-info, /open-insurance/insurance-person/v1/insurance-person/{policyId}/claim, /open-insurance/insurance-person/v1/insurance-person/{policyId}/movements, /open-insurance/consents/v2/consents, /open-insurance/consents/v2/consents/{consentId}, /open-insurance/resources/v2/resources");
    private static final Set<String> HTTP_METHOD_ENUM = SetUtils.createSet("GET, POST, PUT, DELETE, PATCH");


    @Override
    @PreEnvironment(required = "received_request")
    public Environment evaluate(Environment environment) {
        JsonElement body = bodyFrom(environment, "received_request");

        assertEventBody(body);

        logFinalStatus();
        return environment;
    }

    private void assertEventBody(JsonElement data) {
        assertField(data,
                new StringField
                        .Builder("fapiInteractionId")
                        .setPattern("^[a-zA-Z0-9][a-zA-Z0-9\\\\-]{0,99}$")
                        .setMaxLength(100)
                        .setMinLength(1)
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("endpoint")
                        .setEnums(ENDPOINT_ENUM)
                        .build());


        assertField(data,
                new StringField
                        .Builder("url")
                        .setPattern("^/[a-zA-Z-]{2,}(/.*)?$")
                        .setMinLength(2)
                        .build());


        assertField(data,
                new NumberField
                        .Builder("statusCode")
                        .setMinValue(100)
                        .setMaxValue(599)
                        .build());


        assertField(data,
                new StringField
                        .Builder("httpMethod")
                        .setEnums(HTTP_METHOD_ENUM)
                        .build());


        assertField(data,
                new StringField
                        .Builder("correlationId")
                        .setMinLength(1)
                        .build());


        assertField(data,
                new ObjectField
                        .Builder("additionalInfo")
                        .setValidator(this::assertAdditionalInfo)
                        .setOptional()
                        .build());


        assertField(data,
                new StringField
                        .Builder("timestamp")
                        .setPattern("^((?:(\\d{4}-\\d{2}-\\d{2})T(\\d{2}:\\d{2}:\\d{2}(?:\\.\\d+)?))(Z)?)$")
                        .setMaxLength(20)
                        .build());


        assertField(data,
                new NumberField
                        .Builder("processTimespan")
                        .setMinValue(1)
                        .build());


        assertField(data,
                new StringField
                        .Builder("clientOrgId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("clientSSId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .setOptional()
                        .build());


        assertField(data,
                new StringField
                        .Builder("serverOrgId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .build());

        assertField(data,
                new StringField
                        .Builder("serverASId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .build());


        assertField(data,
                new StringField
                        .Builder("endpointUriPrefix")
                        .setMinLength(8)
                        .setPattern("^(https?://)?([a-zA-Z0-9-]+\\.)+[a-zA-Z-]{2,}$")
                        .setOptional()
                        .build());


    }

    private void assertAdditionalInfo(JsonObject additionalInfo) {
        assertField(additionalInfo,
                new StringField
                        .Builder("consentId")
                        .setPattern("^urn:[a-zA-Z0-9][a-zA-Z0-9-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+$")
                        .setMaxLength(256)
                        .setOptional()
                        .build());

    }
}
