package net.openid.conformance.opin.validator.PCM.consent.funnel;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;


/**
 * Api Swagger URL: swagger/openinsurance/PCM/v1/consent-funnel-ingestion-v1.0.3.yaml
 * Api endpoint: /event-api/v1/client-event
 * Api version: 1.0.2
 */

@ApiName("PCM Client Event Payload")
public class PcmConsentFunnelPostClientEventPayloadValidatorV1 extends AbstractJsonAssertingCondition {

    private static final Set<String> STEP = SetUtils.createSet("consent-created, user-redirected, user-authentication-failed, user-authenticated, consent-authorized, consent-rejected, authorization-code-created, user-redirected-back, consent-token-generated, consent-token-received, refresh-token-used, resource-accessed, consent-revoked, consent-expired");

    @Override
    @PreEnvironment(required = "received_request")
    public Environment evaluate(Environment environment) {
        JsonElement body = bodyFrom(environment,"received_request");

        assertEventBody(body);

        logFinalStatus();
        return environment;
    }

    private void assertEventBody(JsonElement data) {
        assertField(data,
                new StringField
                        .Builder("consentId")
                        .setPattern("^urn:[a-zA-Z0-9][a-zA-Z0-9-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+$")
                        .setMaxLength(256)
                        .build());

        assertField(data,
                new StringField
                        .Builder("step")
                        .setEnums(STEP)
                        .build());

        assertField(data,
                new StringField
                        .Builder("correlationId")
                        .setMinLength(1)
                        .build());

        assertField(data,
                new ObjectField
                        .Builder("additionalInfo")
                        .setValidator(this::assertAdditionalInfo)
                        .setOptional()
                        .build());

        assertField(data,
                new DatetimeField
                        .Builder("timestamp")
                        .setPattern("^((?:(\\d{4}-\\d{2}-\\d{2})T(\\d{2}:\\d{2}:\\d{2}(?:\\.\\d+)?))(Z)?)$")
                        .setMaxLength(20)
                        .build());

        assertField(data,
                new StringField
                        .Builder("clientOrgId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .build());

        assertField(data,
                new StringField
                        .Builder("clientSSId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .build());

        assertField(data,
                new StringField
                        .Builder("serverOrgId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .build());

        assertField(data,
                new StringField
                        .Builder("serverASId")
                        .setPattern("^\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}$")
                        .build());
    }

    private void assertAdditionalInfo(JsonObject additionalInfo) {
        assertField(additionalInfo,
                new StringField
                        .Builder("consent-user")
                        .setEnums(SetUtils.createSet("user, non-user"))
                        .setOptional()
                        .build());

        assertField(additionalInfo,
                new StringField
                        .Builder("authentication-failure-reason")
                        .setEnums(SetUtils.createSet("invalid-credentials, invalid-mfa, other"))
                        .setOptional()
                        .build());

        assertField(additionalInfo,
                new StringField
                        .Builder("user-redirected-back-status")
                        .setEnums(SetUtils.createSet("success, failure"))
                        .setOptional()
                        .build());

        assertField(additionalInfo,
                new StringField
                        .Builder("token-kind")
                        .setEnums(SetUtils.createSet("consent-token, client-credential"))
                        .setOptional()
                        .build());

        assertField(additionalInfo,
                new StringField
                        .Builder("rejected-by")
                        .setEnums(SetUtils.createSet("user, system"))
                        .setOptional()
                        .build());

        assertField(additionalInfo,
                new StringField
                        .Builder("revoked-by")
                        .setEnums(SetUtils.createSet("user, system"))
                        .setOptional()
                        .build());

        assertField(additionalInfo,
                new StringField
                        .Builder("expired-by")
                        .setEnums(SetUtils.createSet("authorization-timeout, max-date-reached"))
                        .setOptional()
                        .build());
    }
}