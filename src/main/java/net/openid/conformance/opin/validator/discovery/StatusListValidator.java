package net.openid.conformance.opin.validator.discovery;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api Swagger URL: swagger/openinsurance/swagger-discovery.yaml
 * Api endpoint: /status/
 * Api version: 1.1.0
 * Api Git Hash:
 */

@ApiName("Discovery Status")
public class StatusListValidator extends AbstractJsonAssertingCondition {
	public static final Set<String> CODE = Sets.newHashSet("OK","PARTIAL_FAILURE","UNAVAILABLE","SCHEDULED_OUTAGE");

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment,"resource_endpoint_response_full");

		assertField(body,
			new ObjectField
				.Builder("data")
				.setValidator(data ->
					assertField(data,
						new ObjectArrayField
							.Builder("status")
							.setValidator(this::assertStatus)
							.build()))
				.build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/discovery\\/v\\d+)(\\/status.*)?$";
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertStatus(JsonObject status) {
		assertField(status,
			new StringField
				.Builder("code")
				.setEnums(CODE)
				.build());

		assertField(status,
			new StringField
				.Builder("explanation")
				.build());

		assertField(status,
			new StringField
				.Builder("detectionTime")
				.setOptional()
				.build());

		assertField(status,
			new StringField
				.Builder("expectedResolutionTime")
				.setOptional()
				.build());

		assertField(status,
			new StringField
				.Builder("updateTime")
				.setOptional()
				.build());

		assertField(status,
			new StringArrayField
				.Builder("unavailableEndpoints")
				.setOptional()
				.build());
	}
}
