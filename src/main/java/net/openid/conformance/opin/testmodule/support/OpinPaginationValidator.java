package net.openid.conformance.opin.testmodule.support;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.JsonHelper;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class OpinPaginationValidator extends AbstractJsonAssertingCondition {

    protected String selfLink;
    protected String firstLink;
    protected String prevLink;
    protected String nextLink;
    protected String lastLink;
    protected int realNumberOfRecords;
    protected int expectedNumberOfRecords;

    protected int pageSize;
    protected int totalRecords;
    protected int totalPages;
    private JsonObject linksObject;
    private JsonObject metaObject;
    private JsonElement body;
    protected int currentPageNumber;

    protected String requestUri;
    public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    public Environment evaluate(Environment env) {
        prepareRecordData(env);
        // Note: The variable realNumberOfRecords is set when calling this.validateRegex()
        validateRegex(env, requestUri, 1);

        //Verify if self link is the same as the request
        if (!isUrlEqual(requestUri, selfLink)) {
            if (getRequestMethod(env).equals("POST")) {
                if (!selfLink.startsWith(requestUri)) {
                        throw error("self link in the response is not the same as the request url.", args("requestUrl", requestUri, "selfLink", selfLink));
                }
            } else {
                throw error("Request URI and 'self' link are not equal while request is not POST (when 'self' an ID, while request URI does not)",
                        args("request URI", requestUri, "'self' link", selfLink));
            }
        }

        if (pageSize > 1000) {
            throw error("page-size cannot be greater than 1000");
        }

        if (isSinglePage()) {
            validateSinglePage();
        } else {
            //General validations for multiple pages
            if ((!Strings.isNullOrEmpty(lastLink) && getPageNumber(lastLink) != totalPages) || totalPages < 1) {
                throw error("Last link parameters are not according to the swagger specification.", args("links", linksObject));
            }

            if (currentPageNumber == 1) {
                validateFirstPage();
            } else if (currentPageNumber == totalPages) {
                validateLastPage();
            } else {
                validateMiddlePage();
            }
        }
        return env;
    }

    protected void validateSinglePage() {

        if (realNumberOfRecords != expectedNumberOfRecords) {
            throw error("The actual number of records present in the response differs from the expected value provided on meta", args("meta", metaObject));
        }

        if (!Strings.isNullOrEmpty(prevLink) || !Strings.isNullOrEmpty(nextLink)) {
            throw error("There should not be prev/next links", args("links", linksObject));
        }

        if ((!Strings.isNullOrEmpty(firstLink) && !selfLink.equals(firstLink)) ||
                (!Strings.isNullOrEmpty(lastLink) && !selfLink.equals(lastLink))) {
            throw error ("Self link has to be the same as first/last links if they are present.", args("links", linksObject));
        }
    }

    protected  void validateFirstPage() {

        if ((expectedNumberOfRecords != realNumberOfRecords) || (realNumberOfRecords != pageSize)) {
            throw error("Self link is the first page but does not have the right amount of records", args("meta", metaObject));
        }

        if ((!Strings.isNullOrEmpty(firstLink) && !selfLink.equals(firstLink))) {
            throw error ("Self link has to be the same as first link if it is present.", args("links", linksObject));
        }

        if (getPageNumber(nextLink) != currentPageNumber + 1) {
            throw error("Next link page does not point to the next page.", args("links", linksObject));
        }

        if (getPageNumber(lastLink) != totalPages) {
            throw error("Last link page is not equal to totalPages", args("meta", metaObject));
        }
    }

    protected void validateMiddlePage() {

        if ((expectedNumberOfRecords != realNumberOfRecords) || (realNumberOfRecords != pageSize)) {
            throw error("Self link does not have the right amount of records", args("meta", metaObject));
        }

        if (getPageNumber(prevLink) != currentPageNumber - 1) {
            throw error("Prev link page does not point to the previous page.", args("links", linksObject));
        }

        if (getPageNumber(nextLink) != currentPageNumber + 1) {
            throw error("Next link page does not point to the next page.", args("links", linksObject));
        }

        if (getPageNumber(lastLink) != totalPages) {
            throw error("Last link page is not equal to totalPages", args("meta", metaObject));
        }

        if (getPageNumber(firstLink) != 1) {
            throw error("First link page is not 1.", args("links", linksObject));
        }
    }

    protected void validateLastPage() {
        if (Strings.isNullOrEmpty(firstLink) || Strings.isNullOrEmpty(prevLink)) {
            throw error("Meta and Links point to a multiple page pagination but first/prev links were not found", args("links", linksObject));
        }

        if (!Strings.isNullOrEmpty(nextLink)) {
            throw error("Self link is the last page but next link was found", args("links", linksObject));
        }

        if (currentPageNumber != totalPages) {
            throw error("Self link is the last link but does not have totalPages amount of pages", args("links", linksObject));
        }

        if (expectedNumberOfRecords != realNumberOfRecords) {
            throw error("Self link does not have the right amount of records", args("meta", metaObject));
        }

        if (getPageNumber(prevLink) != currentPageNumber - 1) {
            throw error("Prev link page does not point to the previous page.", args("links", linksObject));
        }

        if (!Strings.isNullOrEmpty(lastLink) && (getPageNumber(lastLink) != currentPageNumber)) {
            throw error("Self link is the last link but does not have the same page number.", args("links", linksObject));
        }

        if (getPageNumber(firstLink) != 1) {
            throw error("First link page is not 1.", args("links", linksObject));
        }
    }
    private boolean isSinglePage() {
        return totalPages <= 1 && currentPageNumber == 1;
    }

    private String getRequestMethod(Environment env) {
        String requestMethod = env.getString("http_method");
        if (Strings.isNullOrEmpty(requestMethod)) {
            //Post is the standard http method used
            return "POST";
        }

        return requestMethod;
    }


    private boolean isUrlEqual(String requestUrl, String selfLink) {
        try {
            requestUrl = requestUrl.replaceAll("page-size=25&?","").replaceAll("page=1&?","");
            selfLink = selfLink.replaceAll("page-size=25&?","").replaceAll("page=1&?","");

            if(requestUrl.endsWith("?")) {
                requestUrl=requestUrl.substring(0,requestUrl.length()-1);
            }
            if(selfLink.endsWith("?")) {
                selfLink=selfLink.substring(0,selfLink.length()-1);
            }

            int requestPageSize = getPageSize(requestUrl);
            int selfLinkPageSize = getPageSize(selfLink);
            int requestPage = getPageNumber(requestUrl);
            int selfLinkPage = getPageNumber(selfLink);

            if(requestPageSize >= selfLinkPageSize) {
                requestUrl = requestUrl.replaceAll("page-size=[0-9]+&?","");
                selfLink = selfLink.replaceAll("page-size=[0-9]+&?","");
            } else {
                return false;
            }

            URI requestUri = new URI(requestUrl);
            URI selfLinkUri = new URI(selfLink);

            return requestUri.equals(selfLinkUri) && (requestUri.getPath().equals(selfLinkUri.getPath())) && (requestPage == selfLinkPage);
        } catch (URISyntaxException e) {
            return false;
        }
    }

    public void prepareRecordData(Environment env) {

        body = bodyFrom(env, RESPONSE_ENV_KEY);

        setRequestUri(env);

        linksObject = findByPath(body, "$.links").getAsJsonObject();
        selfLink = OIDFJSON.getString(findByPath(linksObject, "$.self"));

        if (JsonHelper.ifExists(linksObject, "$.first")) {
            firstLink = OIDFJSON.getString(findByPath(linksObject, "$.first"));
        }
        if (JsonHelper.ifExists(linksObject, "$.prev")) {
            prevLink = OIDFJSON.getString(findByPath(linksObject, "$.prev"));
        }
        if (JsonHelper.ifExists(linksObject, "$.next")) {
            nextLink = OIDFJSON.getString(findByPath(linksObject, "$.next"));
        }
        if (JsonHelper.ifExists(linksObject, "$.last")) {
            lastLink = OIDFJSON.getString(findByPath(linksObject, "$.last"));
        }

        currentPageNumber = getPageNumber(selfLink);
        pageSize = getPageSize(selfLink);

        metaObject = findByPath(body, "$.meta").getAsJsonObject();

        totalRecords = OIDFJSON.getInt(findByPath(metaObject, "$.totalRecords"));
        totalPages = OIDFJSON.getInt(findByPath(metaObject, "$.totalPages"));


        expectedNumberOfRecords = calculateExpectedNumberOfRecords();
    }

    protected int calculateExpectedNumberOfRecords() {
        if (currentPageNumber == totalPages) {
            return totalRecords - (totalPages - 1)*pageSize;
        }
        if(totalPages==0) {
            return totalRecords;
        }
        return pageSize;
    }

    protected void setRequestUri(Environment env) {
        if (env.getEffectiveKey(RESPONSE_ENV_KEY).contains("consent")) {

            if(getRequestMethod(env).equals("POST")){
                requestUri = env.getString("config", "resource.consentUrl");
            }
            else {
                requestUri = env.getString("consent_url");
            }
        } else {
            requestUri = env.getString("protected_resource_url");
            if (Strings.isNullOrEmpty(requestUri)) {
                throw error("resource url missing from configuration");
            }
        }
        log("Proceeding validation with " + requestUri);
    }

    protected void validateRegex(Environment env, String requestUri, int expectedVersion) {
        OpinLinksValidator regexLinksValidator = new OpinLinksValidator(this);
        OpinMetaValidator regexMetaValidator = new OpinMetaValidator(this);
        regexMetaValidator.setRequestUri(requestUri);

        regexMetaValidator.assertMetaObject(body);
        regexLinksValidator.assertLinksObjectPhase2(body, requestUri, expectedVersion);
        String errorMessage = regexMetaValidator.getErrorMessage();
        if (!Objects.equals(errorMessage, "")) {
            throw error(errorMessage, regexMetaValidator.getArgs());
        }

        // OpinMetaValidator already calculates the number of records in the response.
        realNumberOfRecords = regexMetaValidator.getNumberOfRecords();
    }

    protected int getPageNumber(String uri) {
        try {
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(uri), StandardCharsets.UTF_8);
            return Integer.parseInt(
                    params.stream()
                            .filter(p -> p.getName().equals("page"))
                            .findFirst()
                            .orElse(new BasicNameValuePair("page", "1"))
                            .getValue());

        } catch (URISyntaxException e) {
            throw error("Link is not a valid URI", Map.of("Link", uri));
        }
    }

    protected int getPageSize(String uri) {
        try {
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(uri), StandardCharsets.UTF_8);
            return Integer.parseInt(
                    params.stream()
                            .filter(p -> p.getName().equals("page-size"))
                            .findFirst()
                            .orElse(new BasicNameValuePair("page-size", "25"))
                            .getValue());

        } catch (URISyntaxException e) {
            throw error("Link is not a valid URI", Map.of("Link", uri));
        }
    }
}
