package net.openid.conformance.opin.testmodule.support.oasValidators.quoteResponsibility.v1n8;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Quote Responsibility V1.8.0")
public class PatchQuoteResponsibilityLeadOASValidatorV1n8 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteResponsibility/swagger-quote-responsibility-1.8.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lead/request/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.PATCH;
    }
}