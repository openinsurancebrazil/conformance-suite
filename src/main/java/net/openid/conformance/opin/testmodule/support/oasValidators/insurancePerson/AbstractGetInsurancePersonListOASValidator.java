package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetInsurancePersonListOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/insurance-person";
	}
	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
