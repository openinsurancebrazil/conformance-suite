package net.openid.conformance.opin.testmodule.v1.financialRisk;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPremiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Map;


@PublishTestModule(
	testName = "opin-financial-risk-api-wrong-permissions-test",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Financial Risk API (“DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET Financial Risk “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET Financial Risk policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET Financial Risk premium API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Financial Risk claim API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with only either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well\n" +
		"\u2022 Calls GET Financial Risk “/” API\n"+
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Financial Risk policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Financial Risk premium API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Financial Risk claim API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
public class OpinFinancialRiskWrongPermissionsTestModule extends AbstractOpinFinancialRiskWrongPermissionsTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getRootValidator() {
		return GetInsuranceFinancialRiskListOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator() {
		return GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator() {
		return GetInsuranceFinancialRiskPremiumOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator() {
		return GetInsuranceFinancialRiskClaimOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
		return IdSelectorFromJsonPath.class;
	}
}
