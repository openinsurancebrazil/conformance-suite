package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage extends AbstractCreateQuotePostRequestBody {

    @Override
    protected String getCode(){
        return "RESIDENCIAL_IMOVEL_BASICA";
    }

    @Override
    protected void editData(Environment env, JsonObject data) {
        addQuoteData(env, data);
    }

    private void addQuoteData(Environment env, JsonObject data) {

        JsonObject maxLmg = new JsonObject();
        maxLmg.addProperty("amount", "90.85");
        maxLmg.addProperty("unitType", "PORCENTAGEM");

        JsonObject quoteData = new JsonObject();
        quoteData.addProperty("mainActivity", "COMERCIO");
        quoteData.addProperty("isLegallyConstituted", true);
        quoteData.addProperty("condominiumType", true);//???
        String today = ZonedDateTime
                .now(ZoneOffset.UTC)
                .toLocalDateTime()
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT));
        quoteData.addProperty("termStartDate", today);
        quoteData.addProperty("termEndDate", today);
        quoteData.addProperty("insuranceType", "RENOVACAO");
        quoteData.addProperty("policyId", "111111");
        quoteData.addProperty("insurerId", "insurer_id");
        quoteData.addProperty("currency", "BRL");
        quoteData.addProperty("basicCoverageIndex", "SIMPLES");
        quoteData.add("maxLMG", maxLmg);
        quoteData.addProperty("includesAssistanceServices", false);
        quoteData.add("claimAmount", maxLmg);
        quoteData.addProperty("claimDescription", "string");
        quoteData.add("insuredObject", getInsuredObject(env));

        JsonArray coverages = getCoverages();

        quoteData.add("coverages", coverages);

        data.add("quoteData", quoteData);
    }

    protected JsonObject getInsuredObject(Environment env) {
        String productType = env.getString("config", "consent.productType");

        JsonArray securityProtection = new JsonArray();
        securityProtection.add("CAMERA_CFTV");

        JsonObject insuredObject = new JsonObject();
        if("business".equals(productType)) {
            insuredObject.addProperty("identification", env.getString("config", "resource.brazilCnpj"));
        } else {
            insuredObject.addProperty("identification", env.getString("config", "resource.brazilCpf"));
        }

        insuredObject.addProperty("structuringType","CONDOMINIO_VERTICAL");
        insuredObject.addProperty("propertyType","CONDOMINIO_RESIDENCIAL_COM_COMERCIO_NO_TERREO");
        insuredObject.addProperty("hasElevator",false);
        insuredObject.addProperty("isFullyOrPartiallyListed",false);
        insuredObject.addProperty("numberOfBlocks","3");
        insuredObject.addProperty("condominiumAge","20");
        insuredObject.addProperty("hasReuseOfWater",false);
        insuredObject.add("securityProtection",securityProtection);
        insuredObject.add("riskLocationInfo",new JsonObject());
        insuredObject.addProperty("indenizationWithoutDepreciation",true);
        insuredObject.addProperty("wasThereAClaim",false);

        return insuredObject;
    }
}
