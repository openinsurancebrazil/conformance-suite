package net.openid.conformance.opin.testmodule.support;

public class BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "insurance-acceptance-and-branches-abroad";
	}

	@Override
	protected String getEndpointReplacement() {
		return "insurance-acceptance-and-branches-abroad";
	}
}
