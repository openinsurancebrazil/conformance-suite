package net.openid.conformance.opin.testmodule.v1.customers;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinCustomerGranularPermissionsTestModule;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractOpinCustomerPersonalApiGranularPermissionsTest extends AbstractOpinCustomerGranularPermissionsTestModule {

	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalIdentificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalQualificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalComplimentaryInfoValidator();

	@Override
	protected void configureClient(){
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(PrepareConfigForCustomerPersonalTest.class);
		setProductType("personal");
		callAndStopOnFailure(AddScopesForCustomerApi.class);
		callAndStopOnFailure(BuildOpinPersonalCustomersConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {

		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL);
		permissionsBuilder.removePermission("CUSTOMERS_PERSONAL_ADDITIONALINFO_READ");
		permissionsBuilder.build();

		setIdentificationsValidator(getCustomerPersonalIdentificationValidator());
		setQualificationsValidator(getCustomerPersonalQualificationValidator());
		setComplimentaryInformationValidator(getCustomerPersonalComplimentaryInfoValidator());

		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
	}
}

