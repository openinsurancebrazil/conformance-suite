package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractPostPersonTravelOASValidator;

@ApiName("Quote Person 1.11.0")
public class PostPersonTravelOASValidatorV1n11 extends AbstractPostPersonTravelOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.11.0.yaml";
    }
}