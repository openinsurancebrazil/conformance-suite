package net.openid.conformance.opin.testmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.as.FAPIEnsureMinimumClientKeyLength;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.common.CheckDistinctKeyIdValueInClientJWKs;
import net.openid.conformance.condition.common.CheckForKeyIdInClientJWKs;
import net.openid.conformance.condition.common.FAPIBrazilCheckKeyAlgInClientJWKs;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.opin.testmodule.phase3.ClearIdempotencyKeyHeader;
import net.openid.conformance.opin.testmodule.support.ApiTypes;
import net.openid.conformance.opin.testmodule.support.OpinInsertMtlsCa;
import net.openid.conformance.opin.testmodule.support.OpinPaginationValidator;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantConfigurationFields;
import org.apache.http.HttpStatus;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@VariantConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
		"mtls.ca"
})

public abstract class OpinAbstractClientCredentialsGrantFunctionalTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	public static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		env.putString("api_type", API_TYPE);
		callAndContinueOnFailure(OpinInsertMtlsCa.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void validateLinksAndMeta(String responseFull) {

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
				.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
		if (!isEndpointCallSuccessful(status)) {
			return;
		}

		if (responseFull.contains("resource") || env.getElementFromObject(responseFull, "body_json.meta") != null) {
			if (responseFull.contains("resource")) {
				env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			}

			call(condition(OpinPaginationValidator.class)
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure());

			if (responseFull.contains("resource")) {
				env.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
			}
		}

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
		if (recursion.equals("false")) {
			env.putString("recursion", "true");
			env.mapKey("resource_endpoint_response_full", responseFull);

			if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
				validateLinks(responseFull);
			}

			env.unmapKey("resource_endpoint_response_full");
			env.putString("recursion", "false");
		}
	}

	protected void validateLinks(String responseFull) {
		call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
		env.mapKey("resource_endpoint_response_full", responseFull);

		ConditionSequence sequence = new ValidateSelfEndpoint();

		if (responseFull.contains("consent")) {
			sequence.insertAfter(ClearContentTypeHeaderForResourceEndpointRequest.class , condition(ClearIdempotencyKeyHeader.class));
			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
		}

		call(sequence);
	}

	private boolean isEndpointCallSuccessful(int status) {
		if (status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		if(shouldSkipSelfLinkValidation()) {
			eventLog.log("Skipping self link validation", Map.of("skip_self_link_validation", env.getBoolean("skip_self_link_validation")));
			return;
		}

		String resourceResponse = "resource_endpoint_response_full";
		String consentResponse = "consent_endpoint_response_full";

		String httpMethod = env.getString("http_method");

		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
				validateLinksAndMeta(resourceResponse);
			} else {
				validateLinksAndMeta(consentResponse);
			}
		}
		if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
				(CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass()) && !httpMethod.equals("DELETE"))) {
			validateLinksAndMeta(consentResponse);
		}
	}

	private boolean shouldSkipSelfLinkValidation() {
		boolean skipSelfLinkValidation = Optional.ofNullable(env.getBoolean("skip_self_link_validation")).orElse(false);
		// Make sure "skip_self_link_validation" must be set again to skip validation.
		env.putBoolean("skip_self_link_validation", false);
		return skipSelfLinkValidation;
	}

	protected void skipSelfLinkValidation() {
		env.putBoolean("skip_self_link_validation", true);
	}

	protected ConditionSequenceRepeater repeatSequence(Supplier<ConditionSequence> conditionSequenceSupplier) {
		return new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, conditionSequenceSupplier);
	}

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
		env.putString("base_url", baseUrl);
		env.putObject("config", config);
		preConfigure(config, baseUrl, externalUrlOverride);

		call(sequence(() -> createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence)));


		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(CreateIdempotencyKey.class);
		callAndStopOnFailure(AddIdempotencyKeyHeader.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		postConfigure(config, baseUrl, externalUrlOverride);
		setStatus(Status.CONFIGURED);
	}

	protected void validateClientConfiguration() {
		this.callAndStopOnFailure(ValidateClientJWKsPrivatePart.class, new String[]{"RFC7517-1.1"});
		this.callAndStopOnFailure(ExtractJWKsFromStaticClientConfiguration.class, new String[0]);
		this.callAndStopOnFailure(CheckForKeyIdInClientJWKs.class, new String[]{"OIDCC-10.1"});
		this.callAndContinueOnFailure(CheckDistinctKeyIdValueInClientJWKs.class, Condition.ConditionResult.FAILURE, new String[]{"RFC7517-4.5"});
		this.callAndContinueOnFailure(FAPIBrazilCheckKeyAlgInClientJWKs.class, Condition.ConditionResult.FAILURE, new String[]{"BrazilOB-6.1-1"});
		this.callAndContinueOnFailure(FAPIEnsureMinimumClientKeyLength.class, Condition.ConditionResult.FAILURE, new String[]{"FAPI1-BASE-5.2.2-5", "FAPI1-BASE-5.2.2-6"});
		this.callAndContinueOnFailure(ValidateMTLSCertificatesAsX509.class, Condition.ConditionResult.FAILURE, new String[0]);
	}
}
