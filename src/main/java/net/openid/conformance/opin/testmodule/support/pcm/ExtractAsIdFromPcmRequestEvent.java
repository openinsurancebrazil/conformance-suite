package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;


public class ExtractAsIdFromPcmRequestEvent extends AbstractCondition {


    @Override
    @PreEnvironment(required = "pcm_event")
    public Environment evaluate(Environment env) {
        String clientSsId = Optional.ofNullable(env.getString("pcm_event", "serverASId"))
                .orElseThrow(() -> error("Could not find serverASId in the request"));

        env.putString("authorisationServerId", clientSsId);
        logSuccess("Extracted serverASId from from the request", args("serverASId", clientSsId));
        return env;
    }
}
