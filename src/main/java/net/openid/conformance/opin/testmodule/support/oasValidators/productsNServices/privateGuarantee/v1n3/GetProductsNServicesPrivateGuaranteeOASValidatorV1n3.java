package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.privateGuarantee.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;



/**
 * Api Source: swagger/openinsurance/productsServices/swagger-private-guarantee-1.3.0.yaml
 * Api endpoint: /private-guarantee
 * Api version: 1.3.0
 */
@ApiName("ProductsServices Private Guarantee")
public class GetProductsNServicesPrivateGuaranteeOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/swagger-private-guarantee-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/private-guarantee";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

}