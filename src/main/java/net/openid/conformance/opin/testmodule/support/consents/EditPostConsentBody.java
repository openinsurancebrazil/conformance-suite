package net.openid.conformance.opin.testmodule.support.consents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EditPostConsentBody extends AbstractCondition {

    @Override
    @PreEnvironment(required = "consent_endpoint_request")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();
        JsonObject loggedUser = data.getAsJsonObject("loggedUser");
        JsonObject document = loggedUser.getAsJsonObject("document");
        document.addProperty("identification", "00000000000");
        logSuccess("identification was changed", args("data", data));
        return env;
    }
}