package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.extendedWarranty.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api Source: swagger/openinsurance/productsServices/swagger-extended-warranty-1.3.0.yaml
 * Api endpoint: /extended-warranty
 * Api version: 1.3.0
 */
@ApiName("ProductsServices Extended Warranty")
public class GetProductsNServicesExtendedWarrantyOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/swagger-extended-warranty-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/extended-warranty";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

}