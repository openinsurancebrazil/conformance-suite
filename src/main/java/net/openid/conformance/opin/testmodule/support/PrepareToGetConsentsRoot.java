package net.openid.conformance.opin.testmodule.support;

public class PrepareToGetConsentsRoot extends AbstractPrepareUrlForApi {

	@Override
	protected String getApiReplacement() {
		return "consents";
	}

	@Override
	protected String getEndpointReplacement() {
		return "consents";
	}
}
