package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;

public class AddWithdrawalCapitalizationTitleScope extends AbstractScopeAddingCondition {
    @Override
    protected String newScope() {
        return "withdrawal-capitalization-title";
    }
}
