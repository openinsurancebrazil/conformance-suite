package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportClaimOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportListOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportPolicyInfoOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportPremiumOASValidatorV1n2;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-transport-api-structural-test",
	displayName = "Validate structure of Transport API Endpoints 200 response",
	summary = "Validate structure of Transport API Endpoints 200 response \n"+
	"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"resource.consentUrl"
		}
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinTransportStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-transport";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);

		setRootValidator(GetInsuranceTransportListOASValidatorV1n2.class);
		setPolicyInfoValidator(GetInsuranceTransportPolicyInfoOASValidatorV1n2.class);
		setPremiumValidator(GetInsuranceTransportPremiumOASValidatorV1n2.class);
		setClaimValidator(GetInsuranceTransportClaimOASValidatorV1n2.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

