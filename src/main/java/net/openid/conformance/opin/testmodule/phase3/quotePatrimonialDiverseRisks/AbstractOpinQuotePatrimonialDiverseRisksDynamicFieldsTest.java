package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteWithDynamicFieldsTestModule;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAckn;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAcpt;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchAcknRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialDiverseRisksPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePatrimonialDiverseRisksDynamicFieldsTest extends AbstractOpinPhase3QuoteWithDynamicFieldsTestModule {

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_DIVERSE_RISKS;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-patrimonial-diverse-risks";
    }

    @Override
    protected String getApiName() {
        return "quote-patrimonial";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "diverse-risks/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialDiverseRisksPostRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchAcknRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasAckn.class;
    }

    @Override
    protected String getDynamicFieldApiType() {
        return "QUOTE_PATRIMONIAL";
    }
}
