package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyExclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoFirstOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoSecondOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoFirstOneDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoSecondOneDiff;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1n3.PostEndorsementOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.endorsement.AbstractEndorsementApiWrongPolicyIDTestModule;

public class AbstractOpinEndorsementApiWrongPolicyIDTestV1n extends AbstractEndorsementApiWrongPolicyIDTestModule {

        @Override
        protected AbstractJsonAssertingCondition validator() {
                return new PostEndorsementOASValidatorV1n3();
        }

        @Override
        protected AbstractJsonAssertingCondition postConsentValidator() {
                return new PostConsentsOASValidatorV2n7();
        }

        @Override
        protected void validateGetConsentResponse() {
                callAndContinueOnFailure(GetConsentsOASValidatorV2n7.class, Condition.ConditionResult.FAILURE);
        }

        protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody(){
                return new CreateEndorsementRequestBodyExclusaoDiff();
        }

        @Override
        protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
                return CreateEndorsementConsentBodyExclusaoDiff.class;
        }

        protected AbstractSetEndorsementPolicyIDtoSecondOne setEndorsementPolicyIDtoSecondOne(){
                return new SetEndorsementPolicyIDtoSecondOneDiff();
        }
        protected AbstractSetEndorsementPolicyIDtoFirstOne setEndorsementPolicyIDtoFirstOne(){
                return new SetEndorsementPolicyIDtoFirstOneDiff();
        }
}






