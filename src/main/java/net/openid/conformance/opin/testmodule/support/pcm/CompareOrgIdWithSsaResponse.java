package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class CompareOrgIdWithSsaResponse extends AbstractCondition {
    @Override
    @PreEnvironment(required = {"config", "software_statement_assertion"})
    public Environment evaluate(Environment env) {
        String expectedOrgId = Optional.ofNullable(env.getString("software_statement_assertion", "claims.org_id"))
                .orElseThrow(() -> error("Could not extract Org ID from the Software Statement Assertion Directory response"));

        String actualOrgId = Optional.ofNullable(env.getString("config", "resource.brazilOrganizationId"))
                .orElseThrow(() -> error("Could not find org id in the environment"));

        if(!expectedOrgId.equals(actualOrgId)){
            throw error("Org IDs are not equal", args("expected", expectedOrgId, "actual", actualOrgId));
        }

        logSuccess("Org IDs are equal");
        return env;
    }
}
