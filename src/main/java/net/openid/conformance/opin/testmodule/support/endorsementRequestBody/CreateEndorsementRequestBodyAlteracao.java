package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyAlteracao extends AbstractCreateEndorsementRequestBody {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.ALTERACAO;
    }
}
