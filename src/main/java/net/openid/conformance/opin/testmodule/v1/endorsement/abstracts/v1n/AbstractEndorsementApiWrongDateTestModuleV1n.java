package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n;

import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyInclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyInclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.ChangeDateToNextDay;

public abstract class AbstractEndorsementApiWrongDateTestModuleV1n extends AbstractEndorsementApiINegativeTestModuleV1n {

    @Override
    protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
        return new CreateEndorsementRequestBodyInclusaoDiff();
    }

    @Override
    protected void editEndorsementRequestBody() {
        callAndStopOnFailure(ChangeDateToNextDay.class);
    }

    @Override
    protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
        return CreateEndorsementConsentBodyInclusaoDiff.class;
    }
}
