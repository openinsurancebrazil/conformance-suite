package net.openid.conformance.opin.testmodule.phase3.contractLifePension.v1n13;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.contractLifePension.AbstractOpinContractLifePensionCore;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasCanc;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.GetContractLifePensionQuoteStatusOASValidatorV1n12;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.PatchContractLifePensionOASValidatorV1n12;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.PostContractLifePensionOASValidatorV1n12;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.GetContractLifePensionQuoteStatusOASValidatorV1n13;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.PatchContractLifePensionOASValidatorV1n13;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.PostContractLifePensionOASValidatorV1n13;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchCancRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-contract-life-pension_api_core-canc_test-module_v1.13.0",
        displayName = "Ensure that a  Life Pension contract request can be successfully created and rejected afterwards.",
        summary = """
            Ensure that a  Life Pension contract request can be successfully created and rejected afterwards. This test applies to both personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, travelal information will be used.
            • Call POST /request endpoint sending personal or business information, following what is defined at the config
            • Expect 201 - Validate Response and ensure status is RCVD
            • Poll the GET request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Respones and ensure status is ACPT
            • Call PATCH request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC
            • Expect 200 - Validate Response and ensure status is CANC
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinContractLifePensionCoreCancTestModuleV1n13 extends AbstractOpinContractLifePensionCore {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostContractLifePensionOASValidatorV1n13.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetContractLifePensionQuoteStatusOASValidatorV1n13.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchCancRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasCanc.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchContractLifePensionOASValidatorV1n13.class;
    }
}