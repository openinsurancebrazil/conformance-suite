package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Responsibility 1.4.0")
public class GetInsuranceResponsibilityPremiumOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceResponsibility/v1n/swagger-insurance-responsibility-1.4.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-responsibility/{policyId}/premium";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}