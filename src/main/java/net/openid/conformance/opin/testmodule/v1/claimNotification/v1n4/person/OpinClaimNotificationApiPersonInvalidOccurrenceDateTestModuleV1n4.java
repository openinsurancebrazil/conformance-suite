package net.openid.conformance.opin.testmodule.v1.claimNotification.v1n4.person;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AddClaimNotificationToConsentDataDiff;
import net.openid.conformance.opin.testmodule.support.CreateClaimNotificationRequestBodyDiff;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1n4.PostClaimNotificationPersonOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.v1.claimNotification.AbstractOpinClaimNotificationApiPersonInvalidOccurrenceDateTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "opin-claim-notification-api-person-invalid-occurrenceDate-test-v1.4.0",
    displayName = "Ensure that Claim notification for person cannot be created with an occurrenceDate in the future",
    summary = "Ensure that Claim notification for person cannot be created with an occurrenceDate in the future\n" +
        "\u2022 Call the POST Consents API with damages and people person permissions\n" +
        "\u2022 Expects 201 - Validate Response \n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET resources API\n" +
        "\u2022 Expects 200 - Validate Response - For person expect on resource shared with type DAMAGES_AND_PEOPLE_PERSON\n" +
        "\u2022 Select one of the returned resources and save the policyID\n" +
        "\u2022 Call the GET Shared API policy-info endpoint with the extracted resourceId, where the API to be called is to be determined by the Type of the extracted policyID\n" +
        "\u2022 Expect a 200 - Extract the field data.documentType from the response_body\n" +
        "\u2022 Call the DELETE Consents Endpoint for tha created Consent\n" +
        "\u2022 Call the POST Consents Endpoint with Claim Notification permissions and the extracted policyID\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Redirect the user to authorize consent - Expect successful authorization and obtain a token via authorization_code grant\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AUTHORIZED\"\n" +
        "\u2022 Prepare the POST Claim request_body with the following payload\n" +
        "— Set requestorIsInsured to false, and claimant to \"SEGURADO\"\n" +
        "— If the extracted documentType is equal to \"CERTIFICADO\" send groupCertificateId as the extracted policyID and don’t send policyNumber field. Else, send only the policyNumber which should be equal to extracted policyID\n" +
        "— Set documentType as the extracted documentType\n" +
        "— Set occurrenceDate as CurrentDateTime-1 and the occurrenceTime as the CurrentTime\n" +
        "\u2022 Call the POST Claim person endpoint with the the pre-set request_body replacing one of the potential fields occurrenceDate, documentType, policyNumber with an occurrenceDate in the future\n" +
        "\u2022 Expects 422 - Validate Error response\n" +
        "\u2022 Call The GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"CONSUMED\"",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "resource.consentUrl",
        "resource.brazilCpf",
        "consent.productType"
    }
)
public class OpinClaimNotificationApiPersonInvalidOccurrenceDateTestModuleV1n4 extends AbstractOpinClaimNotificationApiPersonInvalidOccurrenceDateTestModule {
    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n7();
    }

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostClaimNotificationPersonOASValidatorV1n4();
    }

    protected void createClaimNotificationRequestBody() {
        callAndStopOnFailure(CreateClaimNotificationRequestBodyDiff.class);
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                        condition(AddClaimNotificationToConsentDataDiff.class).skipIfStringMissing("policyId"));
    }

    @Override
    protected void validateGetConsentResponse() {
        callAndContinueOnFailure(GetConsentsOASValidatorV2n7.class, Condition.ConditionResult.FAILURE);
    }
}
