package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class EditQuoteLeadPostRequestBody extends AbstractCreateQuotePostRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        data.remove("quoteData");
        JsonObject quoteCustomer = data.getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        identificationData.addProperty("brandName", "Different Brand Name");
    }
}