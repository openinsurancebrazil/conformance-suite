package net.openid.conformance.opin.testmodule.v1.consents.v2n7;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentsApiCrossClientTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-consent-api-test-client-limits-v2n7",
	displayName = "Validate that clients cannot obtain one another's consents",
	summary = "Validates that clients cannot obtain one another's consents\n" +
		"• Confirm if we are already selecting it to send either business or personal permissions \n" +
		"• If the Consent is for Business Account, call the POST consent APIs with permission CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ, CUSTOMERS_BUSINESS_QUALIFICATION_READ, and CUSTOMERS_BUSINESS_ADDITIONALINFO_READ  \n" +
		"• If the Consent is for Personal Account, call the POST consent APIs with permission CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ, CUSTOMERS_PERSONAL_QUALIFICATION_READ, and CUSTOMERS_PERSONAL_ADDITIONALINFO_READ  \n" +
		"• Creates Consent with all of the Business/ Personal, besides all existing additional permissions \n" +
		"• Expects 201 - Validate response\n" +
		"• Calls the GET Consents with the Consent ID that has been created\n" +
		"• Expects 200 - Validate response and status is AWAITING_AUTHORISATION\n" +
		"• Checks all of the fields sent on the consent API are specification compliant\n" +
		"• Calls the Token endpoint using the 2nd client provided on the configuration file\n" +
		"• Calls the GET Consents with the first Consent ID created\n" +
		"• Expects 403 - Forbidden\n" +
		"• Expects the test to return a 403 - Forbidden\n" +
		"• Calls the DELETE Consents with the first Consent ID created, using the 2nd client\n" +
		"• Expects the test to return a 403 - Forbidden\n" +
		"• Calls the DELETE Consents with the first Consent ID created, using the 1st client\n" +
		"• Expects 204 - NO_CONTENT\n" +
		"• Calls the GET Consents with the 1st Consent ID created\n" +
		"• Expects 200 - Validate Response and Status is REJECTED, rejectedBy is USER and reason is CUSTOMER_MANUALLY_REJECTED",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"client2.client_id",
		"client2.jwks",
		"mtls2.key",
		"mtls2.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
public class OpinConsentsApiCrossClientTestModuleV2n7 extends AbstractOpinConsentsApiCrossClientTestModule {

	@Override
	protected AbstractJsonAssertingCondition postValidator() {
		return new PostConsentsOASValidatorV2n7();
	}

	@Override
	protected AbstractJsonAssertingCondition getValidator() {
		return new GetConsentsOASValidatorV2n7();
	}
}
