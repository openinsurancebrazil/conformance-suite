package net.openid.conformance.opin.testmodule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractPermissionsCheckingFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.opin.testmodule.support.ApiTypes;
import net.openid.conformance.opin.testmodule.support.OpinPaginationValidator;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;

import java.util.Optional;

public class AbstractOpinPermissionsCheckingFunctionalTestModule extends AbstractPermissionsCheckingFunctionalTestModule {

    protected String api;
    public static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

    @Override
    protected void preFetchResources() {};
    @Override
    protected void prepareCorrectConsents() {};
    @Override
    protected void prepareIncorrectPermissions() {};
    @Override
    protected void requestResourcesWithIncorrectPermissions() {};

    @Override
    protected void call(ConditionCallBuilder builder) {
        super.call(builder);

        String resourceResponse = "resource_endpoint_response_full";
        String consentResponse = "consent_endpoint_response_full";
        String methodString = env.getString("http_method");
        HttpMethod method;
        if(methodString != null) {
            method = HttpMethod.valueOf(methodString);
            if (method.equals(HttpMethod.DELETE)) {
                return;
            }
        }

        if (CallProtectedResource.class.equals(builder.getConditionClass())) {
            if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
                validateLinksAndMeta(resourceResponse);
            } else {
                validateLinksAndMeta(consentResponse);
            }
        }
        if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
                CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass())) {
            validateLinksAndMeta("consent_endpoint_response_full");
        }
    }

    private boolean isEndpointCallSuccessful(int status) {
        if (status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK) {
            return true;
        }
        return false;
    }

    private void validateLinksAndMeta(String responseFull) {

        int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
                .orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
        if (!isEndpointCallSuccessful(status)) {
            return;
        }

        if (responseFull.contains("resource") || env.getElementFromObject(responseFull, "body_json.meta") != null) {
            if (responseFull.contains("resource")) {
                env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
            }

            call(condition(OpinPaginationValidator.class)
                    .onFail(Condition.ConditionResult.FAILURE)
                    .dontStopOnFailure());

            if (responseFull.contains("resource")) {
                env.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
            }
        }

        String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
        if (recursion.equals("false")) {
            env.putString("recursion", "true");
            env.mapKey("resource_endpoint_response_full", responseFull);

            if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
                validateLinks(responseFull);
            }

            env.unmapKey("resource_endpoint_response_full");
            env.putString("recursion", "false");
        }
    }

    private void validateLinks(String responseFull) {
        call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
        env.mapKey("resource_endpoint_response_full", responseFull);

        ConditionSequence sequence = new ValidateSelfEndpoint();

        if (responseFull.contains("consent")) {
            sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
        }

        call(sequence);
    }

    protected void setApi(String api) {
        this.api = api;
        env.putString("api", api);
        env.putString("api_type", API_TYPE);
    }

}
