package net.openid.conformance.opin.testmodule.v1.structural.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.AddConsentIdFromConfigToEnvironment;
import net.openid.conformance.opin.testmodule.support.QuotePatrimonialTypes;
import net.openid.conformance.opin.testmodule.support.quotePatrimonialStructuralBuildResourceUrl.AddQuoteStatusToEndpoint;
import net.openid.conformance.opin.testmodule.support.quotePatrimonialStructuralBuildResourceUrl.BuildResourceUrlFromStructuralResourceUrlQuotePatrimonial;
import net.openid.conformance.opin.testmodule.support.sequences.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;

public abstract class QuotePatrimonialStructuralTestAbstractClass extends AbstractNoAuthFunctionalTestModule {

    @Override
    protected void runTests() {
        call(new ValidateOpinWellKnownUriSteps());

        callAndStopOnFailure(AddConsentIdFromConfigToEnvironment.class);
        addQuotePatrimonialTypeToEnvironment();
        callRequestEndpoint();
        callQuoteStatusEndpoint();
    }

    protected abstract QuotePatrimonialTypes quotePatrimonialType();

    protected abstract Class<? extends Condition> validatorRoot();

    protected abstract Class<? extends Condition> validatorQuoteStatus();

    protected void addQuotePatrimonialTypeToEnvironment() {
        String type = quotePatrimonialType().name().toLowerCase().replace("_", "-");
        env.putString("quote_patrimonial_type", type);
    }

    protected void callRequestEndpoint() {
        runInBlock(String.format("Validate %s/request endpoint", env.getString("quote_patrimonial_type")), () -> {
            callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrlQuotePatrimonial.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validatorRoot(), Condition.ConditionResult.FAILURE);
        });
    }

    protected void callQuoteStatusEndpoint() {
        runInBlock("Validate quote-status endpoint", () -> {
            callAndStopOnFailure(AddQuoteStatusToEndpoint.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validatorQuoteStatus(), Condition.ConditionResult.FAILURE);
        });
    }
}
