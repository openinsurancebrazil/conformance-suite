package net.openid.conformance.opin.testmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.GetConsentsOASValidatorV2n6;

import java.util.Map;


public abstract class AbstractOpinApiTestModuleV2 extends AbstractOpinFunctionalTestModule {


    public static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        env.putString("api", getApi());
        env.putString("api_type", API_TYPE);
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(getPermissionsGroup()).build();
        permissionsBuilder.removePermission(getIndividualPermissionsToBeRemoved()).build();
    }

    @Override
    protected void requestProtectedResource() {
        if(checkConsentAuthorizedStatusAfterRedirect()) {
            fetchConsentToCheckAuthorisedStatus();
            validateGetConsentResponse();
        }
        this.useAuthorizationCodeToken();
        super.requestProtectedResource();
    }

    @Override
    protected void fetchConsentToCheckAuthorisedStatus() {
        this.eventLog.startBlock("Checking the created consent - Expecting AUTHORISED status");
        this.useClientCredentialsToken();
        this.callAndStopOnFailure(PaymentConsentIdExtractor.class);
        this.callAndStopOnFailure(AddJsonAcceptHeaderRequest.class);
        this.callAndStopOnFailure(PrepareToFetchConsentRequest.class);
        this.callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        this.callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        this.callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
        this.callAndContinueOnFailure(EnsurePaymentConsentStatusWasAuthorised.class, Condition.ConditionResult.WARNING);
        this.eventLog.endBlock();
    }

    @Override
    protected void validateGetConsentResponse() {
        this.eventLog.startBlock("Validate GET consents response");
        callAndContinueOnFailure(GetConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);
        this.eventLog.endBlock();
    }

    protected boolean checkConsentAuthorizedStatusAfterRedirect() {
        return false;
    }

    @Override
    protected void configureClient() {
        super.configureClient();
        callAndStopOnFailure(getScopeCondition());
        callAndStopOnFailure(getBuildConfigResourceUrlCondition());
    }

    @Override
    protected void validateResponse() {
        callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(getIdSelector());

        getEndpoints().forEach((endpoint, validator) -> {
            env.putString("endpoint", endpoint);
            runInBlock(String.format("Calling %s %s", getApi(), endpoint), () -> callResource(endpoint));
            runInBlock(String.format("Validating %s %s", getApi(), endpoint), () -> validate(validator, endpoint));
        });

    }

    protected void callResource(String endpoint) {
        callAndStopOnFailure(PrepareUrlForFetchingEndpoint.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
    }

    protected void validate(Class<? extends Condition> validator, String endpoint) {
        callAndStopOnFailure(getExpectedResponseCode(endpoint));
        callAndContinueOnFailure(CheckForDateHeaderInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
        callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
        callAndContinueOnFailure(EnsureResourceResponseReturnedJsonContentType.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-9", "FAPI1-BASE-6.2.1-10");
        callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
    }

    protected Class<? extends Condition> getExpectedResponseCode(String endpoint){
        return EnsureResourceResponseCodeWas200.class;
    }

    protected abstract PermissionsGroup getPermissionsGroup();

    protected String[] getIndividualPermissionsToBeRemoved() {
        return new String[0];
    }

    protected abstract Class<? extends AbstractScopeAddingCondition> getScopeCondition();

    protected abstract Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition();


    protected abstract Class<? extends AbstractJsonAssertingCondition> getRootValidator();

    protected abstract String getApi();

    protected abstract Class<? extends AbstractIdSelector> getIdSelector();


    /**
     * Returns the map of endpoints and their corresponding validator classes.
     * These endpoints are called after the root.
     *
     * For example, if we want to call and validate the following endpoint - /{api}/{planId}/plan-info
     * then we should add the validator class under the plan-info key
     *
     * @return The map of endpoints and their validator classes.
     */
    protected abstract Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints();

}
