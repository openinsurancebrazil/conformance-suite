package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

public class SetEndorsementPolicyIDtoSecondOne extends AbstractSetEndorsementPolicyIDtoSecondOne {

    @Override
    protected String getPolicyFieldName() {
        return "policyNumber";
    }
}
