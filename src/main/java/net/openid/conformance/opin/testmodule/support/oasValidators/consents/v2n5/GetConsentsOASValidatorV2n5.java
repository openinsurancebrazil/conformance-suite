package net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Consents 2.5.1")
public class GetConsentsOASValidatorV2n5 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/consents/v2n5/swagger-consents-api-v2.yml";
    }

    @Override
    protected String getEndpointPath() {
        return "/consents/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

    @Override
    protected ResponseEnvKey getResponseEnvKey(){
        return ResponseEnvKey.FullConsentResponseEnvKey;
    }
}
