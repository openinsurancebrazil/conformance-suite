package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingPremium extends AbstractPrepareUrlForApi {

	private String api;
	private String policyId;

	@Override
	@PreEnvironment(strings = {"policyId", "api"})
	public Environment evaluate(Environment env) {
		policyId = env.getString("policyId");
		api = env.getString("api");
		return super.evaluate(env);
	}

	@Override
	protected String getApiReplacement() {
		return api;
	}

	@Override
	protected String getEndpointReplacement() {
		return String.format("%s/%s/premium", api, policyId);
	}
}
