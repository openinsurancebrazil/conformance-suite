package net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.v1n11;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.AbstractOpinQuotePersonTravelNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel.PostPersonTravelOASValidatorV1n11;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePersonTravelPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-travel_api_negative-quote_test-module_v1.11.0",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = """
                Ensure a consent cannot be created in unhappy requests.
                • Call POST travel/request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload
                • Expect 400 or 422 - Validate Error Response
                • Call POST travel/request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with "quote-person-lead" scope
                • Expect 403 - Validate Error Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonTravelNegativeTestModuleV1n11 extends AbstractOpinQuotePersonTravelNegativeTestModule {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return PostPersonTravelOASValidatorV1n11.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePersonTravelPostRequestBodyWithCoverage.class;
    }
}
