package net.openid.conformance.opin.testmodule.v1.customers.v1n6;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6.GetCustomersPersonalComplimentaryInformationListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6.GetCustomersPersonalIdentificationListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6.GetCustomersPersonalQualificationListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.v1.customers.AbstractOpinCustomerPersonalApiGranularPermissionsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-customer-personal-api-granular-permissions-test-v1n6",
	displayName = "Validate structure of all personal customer data API resources V1n6",
	summary = "Ensure specific methods can only be called if the respective permissions is granted at the customer personal api.\n" +
			"\u2022 Call the POST Consents with the Customer personal IDENTIFICATIONS and QUALIFICATION permissions, and RESOURCES_READ\n" +
			"\u2022 Expects 201 - Validate response\n" +
			"\u2022 Redirects the user to Authorize consent\n" +
			"\u2022 Call the GET  Customer personal identifications endpoint\n" +
			"\u2022 Expects 200 - Validate Response\n" +
			"\u2022 Call the GET Customer personal qualifications endpoint\n" +
			"\u2022 Expects 200 - Validate Response\n" +
			"\u2022 Call the GET Customer personal complimentary-information endpoint\n" +
			"\u2022 Expects a 403 response  - Validate error response ",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks","consent.productType"
})
public class OpinCustomerPersonalApiGranularPermissionsTestModuleV1n6 extends AbstractOpinCustomerPersonalApiGranularPermissionsTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalIdentificationValidator(){
		return GetCustomersPersonalIdentificationListOASValidatorV1n6.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalQualificationValidator(){
		return GetCustomersPersonalQualificationListOASValidatorV1n6.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalComplimentaryInfoValidator(){
		return GetCustomersPersonalComplimentaryInformationListOASValidatorV1n6.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}

