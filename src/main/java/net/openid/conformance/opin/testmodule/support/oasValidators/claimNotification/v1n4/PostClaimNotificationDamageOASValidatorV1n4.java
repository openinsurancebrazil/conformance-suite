package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.AbstractPostClaimNotificationDamageOASValidator;

@ApiName("Post Claim Notification Damage V1.4.0")
public class PostClaimNotificationDamageOASValidatorV1n4 extends AbstractPostClaimNotificationDamageOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/claimNotification/swagger-claim-notification-1.4.0.yaml";
    }
}
