package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.AbstractOpinQuotePatrimonialHomeNegativeTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PostPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialHomePostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-home_api_negative-quote_test-module_v1n10",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = "Ensure a consent cannot be created in unhappy requests.\n" +
                "\u2022 Call POST home/request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload\n" +
                "\u2022 Expect 400 or 422 - Validate Error Response\n" +
                "\u2022 Call POST home/request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with \"quote-patrimonial-lead\" scope\n" +
                "\u2022 Expect 403 - Validate Error Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialHomeNegativeTestModuleV1n10 extends AbstractOpinQuotePatrimonialHomeNegativeTest {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return PostPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialHomePostRequestBodyWithCoverage.class;
    }
}
