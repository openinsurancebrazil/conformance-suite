package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.opin.testmodule.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinConsentApiNegativeTests extends OpinAbstractClientCredentialsGrantFunctionalTestModule {

    private OpinConsentPermissionsBuilder permissionsBuilder;
    protected abstract AbstractJsonAssertingCondition postValidator();

    @Override
    protected void runTests() {
        permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);

        validateBadPermission("Resource read only", "RESOURCES_READ");

        validateBadPermission("non-existent permission group", "BAD_PERMISSION");

        permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.ALL_PHASE2).build();
        validateBadExpiration(ConsentExpiryDateTimeGreaterThanAYear.class, "DateTime greater than 1 year from now");
        validateBadExpiration(ConsentExpiryDateTimeInThePast.class, "DateTime in the past");
        validateBadExpiration(ConsentExpiryDateTimePoorlyFormed.class, "DateTime poorly formed");

    }

    private void validateBadPermission(String description, String permissions) {
        String logMessage = String.format("Check for HTTP 400 response from consent api request for %s", description);
        runInBlock(logMessage, () -> {
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            new ScopesAndPermissionsBuilder(env, eventLog).addScopes(OPINScopesEnum.CONSENTS).build();
            callAndStopOnFailure(PrepareToPostConsentRequest.class);
            permissionsBuilder.set(permissions).build();
            callAndStopOnFailure(FAPIBrazilOpenInsuranceCreateConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
            callAndStopOnFailure(CreateIdempotencyKey.class);
            callAndStopOnFailure(AddIdempotencyKeyHeader.class);
            callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureConsentResponseCodeWas400.class);
            env.putString("resource_endpoint_response", env.getObject("consent_endpoint_response").toString());
            callAndContinueOnFailure(postValidator().getClass(), Condition.ConditionResult.FAILURE);
        });
    }

    private void validateBadExpiration(Class<? extends Condition> setupClass, String description) {
        String logMessage = String.format("Check for HTTP 400 response from consent api request for %s.", description);
        runInBlock(logMessage, () -> {
            callAndStopOnFailure(PrepareToPostConsentRequest.class);
            callAndStopOnFailure(CreateIdempotencyKey.class);
            callAndStopOnFailure(AddIdempotencyKeyHeader.class);
            callAndStopOnFailure(setupClass);
            callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureConsentResponseCodeWas400.class);
            env.putString("resource_endpoint_response", env.getObject("consent_endpoint_response").toString());
            callAndContinueOnFailure(postValidator().getClass(), Condition.ConditionResult.FAILURE);
        });
    }
}