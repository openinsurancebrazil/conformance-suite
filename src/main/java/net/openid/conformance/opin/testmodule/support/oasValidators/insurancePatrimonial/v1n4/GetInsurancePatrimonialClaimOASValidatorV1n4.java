package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.AbstractGetInsurancePatrimonialClaimOASValidator;

@ApiName("Insurance Patrimonial 1.4.0")
public class GetInsurancePatrimonialClaimOASValidatorV1n4 extends AbstractGetInsurancePatrimonialClaimOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insurancePatrimonial/v1/swagger-insurance-patrimonial.yaml";
	}
}
