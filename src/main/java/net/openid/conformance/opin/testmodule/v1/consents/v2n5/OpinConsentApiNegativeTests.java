package net.openid.conformance.opin.testmodule.v1.consents.v2n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n5.PostConsentsOASValidatorV2n5;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentApiNegativeTests;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-consent-api-test-negative",
	displayName = "Runs various negative tests",
	summary = "Validates the structure of all consent API resources in scenarios where it should return a 400 error due to incomplete permissions groups:\n" +
		"• Call the POST consent API with different scenarios\n" +
		"• Only RESOURCES_READ permissions\n" +
		"• Non-existent permission group 2 (\"BAD_PERMISSION\")\n" +
		"• API request for DateTime greater than 1 year from now\n" +
		"• API request for DateTime in the past\n" +
		"• API request for DateTime poorly formed\n" +
		"• For All Scenartios above, ensure resposne Code is 400 - Validate error response\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinConsentApiNegativeTests extends AbstractOpinConsentApiNegativeTests {

	@Override
	protected AbstractJsonAssertingCondition postValidator() {
		return new PostConsentsOASValidatorV2n5();
	}
}

