package net.openid.conformance.opin.testmodule.phase3.contractLifePension.v1n11;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.contractLifePension.AbstractOpinContractLifePensionLeadTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.lead.PatchContractLifePensionLeadOASValidatorV1n12;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.lead.PostContractLifePensionLeadOASValidatorV1n12;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-contract-life-pension-lead_api_core_test-module_v1",
        displayName = "Ensure that a Person Life Lead request can be successfully created and deleted afterwards.",
        summary = """
            Ensure that after an initial Lead request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idempotency key the request fails
            · Call POST lead/request endpoint sending personal or business information, following what is defined at the config
            · Expect 201 - Validate Response and ensure status is RCVD
            · Call the POST lead/request Endpoint with the same payload and idempotency id
            · Expects 201 - Validate Response
            · Call the POST lead/request, with the same payload as the previous request but a different idempotency id
            · Expects 422 ERRO_IDEMPOTENCIA - Validate Response
            · Call PATCH lead/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinContractLifePensionLeadTestModule extends AbstractOpinContractLifePensionLeadTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteLeadResponse() {
        return PostContractLifePensionLeadOASValidatorV1n12.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteLeadResponse() {
        return PatchContractLifePensionLeadOASValidatorV1n12.class;
    }

}