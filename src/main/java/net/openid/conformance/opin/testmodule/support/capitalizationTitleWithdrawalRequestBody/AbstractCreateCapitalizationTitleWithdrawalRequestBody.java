package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateCapitalizationTitleWithdrawalRequestBody extends AbstractCreateCapitalizationTitleXRequestBody {

    @Override
    @PreEnvironment(required = {"config","series_1","product_1"}, strings = "planId")
    @PostEnvironment(required = "resource_request_entity", strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject requestData = new JsonObject();
        JsonObject data = new JsonObject();

        JsonObject series = env.getObject("series_1");
        JsonObject product = env.getObject("product_1");

        JsonObject productInformation = new JsonObject();
        productInformation.addProperty("capitalizationTitleName", product.get("productName").getAsString());
        productInformation.addProperty("planId",product.get("planId").getAsString());
        productInformation.addProperty("titleId",series.get("titles").getAsJsonArray().get(0).getAsJsonObject().get("titleId").getAsString());
        productInformation.addProperty("seriesId",series.get("seriesId").getAsString());
        productInformation.addProperty("termEndDate",series.get("titles").getAsJsonArray().get(0).getAsJsonObject().get("termEndDate").getAsString());

        JsonObject withdrawalInformation = new JsonObject();

        JsonObject withdrawalTotalAmount = new JsonObject();
        withdrawalTotalAmount.addProperty("amount", series.get("titles").getAsJsonArray().get(0).getAsJsonObject().get("technicalProvisions").getAsJsonArray().get(0).getAsJsonObject().get("prAmount").getAsJsonObject().get("amount").toString());
        JsonObject unit = new JsonObject();
        unit.addProperty("code","R$");
        unit.addProperty("description","BRL");
        withdrawalTotalAmount.add("unit", unit);

        withdrawalInformation.addProperty("withdrawalReason","INSATISFACAO_COM_CARACTERISTICAS_DO_PRODUTO");
        withdrawalInformation.add("withdrawalTotalAmount",withdrawalTotalAmount);

        data.addProperty("modality", series.get("modality").getAsString());
        data.addProperty("susepProcessNumber", series.get("susepProcessNumber").getAsString());
        data.add("productInformation", productInformation);
        data.add("withdrawalInformation", withdrawalInformation);

        editData(env, data);

        requestData.add("data", data);
        logSuccess("The request body was created", args("body", requestData));
        env.putObject("resource_request_entity", requestData);
        env.putString("resource_request_entity", requestData.toString());
        return env;
    }


}
