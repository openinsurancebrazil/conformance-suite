package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AddPensionPlanScope;
import net.openid.conformance.opin.testmodule.support.AddWithdrawalPensionScope;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForPensionWithdrawal;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawal.v1n3.PostPensionWithdrawalOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.CreatePensionWithdrawalMockConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalMockPostRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-pension-withdraw_api_invalid-certificateID_test-module_v1",
        displayName = "Validates the structure of Pension Withdrawal API",
        summary = """
            Ensure a life pension withdrawal request cannot be successfully executed with na invalid certificateId
            
            · Call the POST Consents with PENSION_WITHDRAWAL_CREATE permission, sending the withdrawalLifePensionInformation with the mocked information
            · Expect a 201 - Validate the response
            · Redirect the User
            
            If an error is not returned at the redirect:
            
            · Call the POST pension/request endpoint, sending the same information as the consent
            · Expect a 422 - Validate the response
            · Call the GET Consents endpoint
            · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3PensionWithdrawApiInvalidCertificateIdTestModule extends AbstractOpinPhase3XWithdrawInvalidIDTestModule {
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddWithdrawalPensionScope.class;
    }

    @Override
    protected String getApi() {
        return "insurance-pension-plan";
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.PENSION_WITHDRAWAL;
    }

    @Override
    protected AbstractCondition createWithdrawalConsentBody() {
        return new CreatePensionWithdrawalMockConsentBody();
    }

    @Override
    protected Class<? extends Condition> prepareUrl() {
        return PrepareUrlForPensionWithdrawal.class;
    }

    @Override
    protected Class<? extends Condition> createWithdrawalPostRequestBody() {
        return CreatePensionWithdrawalMockPostRequestBody.class;
    }

    @Override
    protected Class<? extends Condition> validator() {
        return PostPensionWithdrawalOASValidatorV1n3.class;
    }

    @Override
    protected void validateResponse() {
        //not needed here
    }
}
