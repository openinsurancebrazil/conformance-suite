package net.openid.conformance.opin.testmodule.v1.resources.v2n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n5.GetResourcesOASValidatorV2n5;
import net.openid.conformance.opin.testmodule.v1.resources.AbstractOpinResourcesApiPaginationTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
	testName = "opin-resources-api-pagination-test-v2n5",
	displayName = "Validates pagination rules on the resources API ",
	summary = "There should be at least 3 resources on the Resources API and less than 25, regardless of the status of it\n" +
		"• Call the POST Consents\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirect the user\n" +
		"• Calls GET Resources API\n" +
		"• Expects a 200  - Validate Response\n" +
		"• Ensure  “totalRecords” from Meta has at least 3 records\n" +
		"• Calls GET Resources API with page size=1000\n" +
		"• Expects a 200 - Validate Response\n" +
		"•Ensure links and meta attributes don’t display the next page nor prev\n" +
		"• Calls GET Resources API with page size=1\n" +
		"• Expects a 200 - Validate Response\n" +
		"• Ensure links and meta attributes display the next page, but don’t display prev page\n" +
		"• Calls Next Page URI with page size=1\n" +
		"• Expects a 200 - Validate response\n" +
		"• Ensure links and meta attributes display the next page and the prev page, and that the data array has only 1 item\n" +
		"• Calls GET Resources API with page size=1001\n" +
		"• Expects a 422 response  - Validate error message",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinResourcesApiPaginationTestModuleV2n5 extends AbstractOpinResourcesApiPaginationTest {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetResourcesOASValidatorV2n5.class;
	}

	//This Method is not used required for this Test Module
	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		return null;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}
