package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.AbstractGetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidator;

@ApiName("Insurance Acceptance and Branches Abroad 1.3.0")
public class GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n3 extends AbstractGetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceAcceptanceAndBranchesAbroad/v1/swagger-insurance-acceptance-and-branches-abroad.yaml";
	}

}
