package net.openid.conformance.opin.testmodule.support.endorsementConsentBody;

import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.EndorsementType;

public class CreateEndorsementConsentBodyAlteracao extends AbstractCreateEndorsementConsentBody {
    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.ALTERACAO;
    }
}
