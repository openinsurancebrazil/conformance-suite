package net.openid.conformance.opin.testmodule;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

public abstract class AbstractOpinWrongPermissionsTestModuleV2 extends AbstractOpinApiTestModuleV2 {
    protected boolean goingThroughHappyPath = true;

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        super.onConfigure(config, baseUrl);
        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
        callAndStopOnFailure(RememberOriginalScopes.class);
    }

    @Override
    protected void requestProtectedResource() {
        if(goingThroughHappyPath) {
            super.requestProtectedResource();
        } else {
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            validateResponse();
        }
    }

    @Override
    protected void validateResponse() {
        if(goingThroughHappyPath) {
            super.validateResponse();
            return;
        }

        // Validate forbidden response from the root endpoint.
        callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);

        // Call and validate forbidden response from endpoints.
        getEndpoints().forEach((endpoint, validator) -> {
            env.putString("endpoint", endpoint);
            runInBlock(String.format("Calling %s %s", getApi(), endpoint), () -> callResource(endpoint));
            runInBlock(String.format("Validating %s %s", getApi(), endpoint), () -> validate(validator, endpoint));
        });
    }

    @Override
    protected Class<? extends Condition> getExpectedResponseCode(String endpoint) {
        if(goingThroughHappyPath) {
            return super.getExpectedResponseCode(endpoint);
        }

        return EnsureResourceResponseCodeWas403.class;
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
        if(goingThroughHappyPath) {
            // The happy path has finished, so we'll execute the unhappy one.
            goingThroughHappyPath = false;
            performAuthorizationFlowUnhappyPath();
        } else {
            super.onPostAuthorizationFlowComplete();
        }
    }

    protected void performAuthorizationFlowUnhappyPath() {
        callAndStopOnFailure(getBuildConfigResourceUrlCondition());
        callAndStopOnFailure(ResetScopesToConfigured.class);
        prepareIncorrectPermissions();
        performAuthorizationFlow();
    }

    protected void prepareIncorrectPermissions() {

        String productType = env.getString("config", "consent.productType");
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        if(!Strings.isNullOrEmpty(productType) && productType.equals("business")) {
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS).build();
        } else if(!Strings.isNullOrEmpty(productType) && productType.equals("personal")){
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL).build();
        }

    }
}
