package net.openid.conformance.opin.testmodule.v1.housing;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.housing.abstracts.AbstractOpinHousingResourcesApiTest;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-housing-resources-api-test",
	displayName = "Makes sure that the Resource API and the Housing API are returning the same available IDs",
	summary = "Makes sure that the Resource API and the Housing API are returning the same available IDs\n" +
			"• Creates a consent with all the permissions needed to access the housing API (“DAMAGES_AND_PEOPLE_HOUSING_READ”, “DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ”,  “RESOURCES_READ”)\n" +
			"• Expects the server to create the consent with 201\n" +
			"• Redirect the user to authorize at the financial institution\n" +
			"• Call the Housing “/” API\n" +
			"• Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the policy id provided by this API\n" +
			"• Call the resources API\n" +
			"• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
public class OpinHousingResourcesApiTestModule extends AbstractOpinHousingResourcesApiTest {


	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsuranceHousingListOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*]");
		return AllIdsSelectorFromJsonPath.class;
	}
}
