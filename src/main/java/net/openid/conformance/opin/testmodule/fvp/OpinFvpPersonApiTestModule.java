package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddPersonScope;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetPersonV1Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonClaimOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonPolicyInfoOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonPremiumOASValidatorV1n5;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Map;

@PublishTestModule(
        testName = "opin-fvp-person-api-test-module",
        displayName = "Validates the structure of all person API resources",
        summary ="Validates the structure of all Person API \n" +
                "\u2022 Call the POST consents with all the permissions needed to access the Person API (“DAMAGES_AND_PEOPLE_PERSON_READ”, “DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ”, “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 -  Validate Response \n" +
                "\u2022 Redirect the user to authorize consent \n" +
                "\u2022 Calls GET Person \"/\"  Endpoint \n" +
                "\u2022 Expects 200 - Fetches one of the policy IDs returned \n" +
                "\u2022 Calls GET Person Policy Info Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Person Premium  Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Person Claim Endpoint \n" +
                "\u2022 Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.consentSyncTime",
                "consent.productType"
        }
)
public class OpinFvpPersonApiTestModule  extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetPersonV1Endpoint.class));
    }
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddPersonScope.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_PERSON;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePersonListOASValidatorV1n5.class;
    }

    @Override
    protected String getApi() {
        return "insurance-person";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].policies[0].policyId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", GetInsurancePersonPolicyInfoOASValidatorV1n5.class,
                "premium", GetInsurancePersonPremiumOASValidatorV1n5.class,
                "claim", GetInsurancePersonClaimOASValidatorV1n5.class
        );
    }
}