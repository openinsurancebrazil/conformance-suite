package net.openid.conformance.opin.testmodule.v1.structural;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.BuildResourceUrlFromStructuralResourceUrl;
import net.openid.conformance.opin.testmodule.support.PrepareToGetConsentsDetailsById;
import net.openid.conformance.opin.testmodule.support.PrepareToGetConsentsRoot;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.GetConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.sequences.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-consents-api-structural-test",
	displayName = "Validate structure of Consents API Endpoint 200 response",
	summary = "\u2022 Call the “/\" endpoint - Expect 200 and validate the response \n"+
		"\u2022 Call the “/{consentId}\" endpoint - Expect 200 and validate response \n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"resource.consentUrl"
		}
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinConsentsStructuralTestModule extends AbstractNoAuthFunctionalTestModule {

	private final String API = "consents";

	@Override
	protected void runTests() {

		call(new ValidateOpinWellKnownUriSteps());

		env.putString("api_base", API);
		callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrl.class);

		runInBlock("Validate Consents - Root", () -> {
			callAndContinueOnFailure(PrepareToGetConsentsRoot.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			env.mapKey("consent_endpoint_response_full", "resource_endpoint_response_full");
			callAndContinueOnFailure(PostConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);
			env.unmapKey("consent_endpoint_response_full");
		});
		runInBlock("Validate Consents - Id", () -> {
			callAndContinueOnFailure(ConsentIdExtractor.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(PrepareToGetConsentsDetailsById.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			env.mapKey("consent_endpoint_response_full", "resource_endpoint_response_full");
			callAndContinueOnFailure(GetConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);
			env.unmapKey("consent_endpoint_response_full");
		});
	}
}
