package net.openid.conformance.opin.testmodule.phase3.quotePersonLife.v1n9;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.phase3.quotePersonLife.AbstractOpinQuotePersonLifeNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life.PostPersonLifeOASValidatorV1n10;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-life_api_negative-quote_test-module_v1",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = """
                Ensure a consent cannot be created in unhappy requests.
                • Call POST life/request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload
                • Expect 400 or 422 - Validate Error Response
                • Call POST life/request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with "quote-person-lead" scope
                • Expect 403 - Validate Error Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonLifeNegativeTestModule extends AbstractOpinQuotePersonLifeNegativeTestModule {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return PostPersonLifeOASValidatorV1n10.class;
    }
}