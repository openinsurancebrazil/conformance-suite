package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts;


import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyExclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoFirstOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoSecondOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoFirstOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoSecondOne;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1.PostEndorsementOASValidatorV1;
import net.openid.conformance.opin.testmodule.v1.endorsement.AbstractEndorsementApiWrongPolicyIDTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

public abstract class AbstractOpinEndorsementApiWrongPolicyIDTest extends AbstractEndorsementApiWrongPolicyIDTestModule {

        @Override
        protected AbstractJsonAssertingCondition validator() {
                return new PostEndorsementOASValidatorV1();
        }

        @Override
        protected AbstractJsonAssertingCondition postConsentValidator() {
                return new PostConsentsOASValidatorV2n6();
        }

        protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody(){
                return new CreateEndorsementRequestBodyExclusao();
        }

        @Override
        protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
                return CreateEndorsementConsentBodyExclusao.class;
        }

        protected AbstractSetEndorsementPolicyIDtoSecondOne setEndorsementPolicyIDtoSecondOne(){
                return new SetEndorsementPolicyIDtoSecondOne();
        }

        protected AbstractSetEndorsementPolicyIDtoFirstOne setEndorsementPolicyIDtoFirstOne(){
                return new SetEndorsementPolicyIDtoFirstOne();
        }
}






