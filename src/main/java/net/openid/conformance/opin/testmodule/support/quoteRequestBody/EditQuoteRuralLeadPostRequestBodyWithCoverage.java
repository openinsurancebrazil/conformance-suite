package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuoteRuralLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "GRANIZO";
    }
}