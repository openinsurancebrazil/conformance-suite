package net.openid.conformance.opin.testmodule.support;

public class BuildInsuranceLifePensionUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {
    @Override
    protected String getApiReplacement() {
        return "insurance-life-pension";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-life-pension/contracts";
    }
}
