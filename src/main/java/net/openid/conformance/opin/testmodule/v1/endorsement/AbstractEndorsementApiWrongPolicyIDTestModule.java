package net.openid.conformance.opin.testmodule.v1.endorsement;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.support.FetchTwoPolicyIds;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoFirstOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoSecondOne;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;

public abstract class AbstractEndorsementApiWrongPolicyIDTestModule extends AbstractEndorsementNegativeTest {

    /**
     * Three flows will happen.
     * 1. A flow to call the resources API and extract two policy IDs.
     * 2. A flow to post an endorsement with the second policy ID that expects 422.
     * 3. A flow to post an endorsement with the first policy ID that expects 201.
     */

    private Integer flowCounter = 1;

    protected abstract AbstractSetEndorsementPolicyIDtoSecondOne setEndorsementPolicyIDtoSecondOne();
    protected abstract AbstractSetEndorsementPolicyIDtoFirstOne setEndorsementPolicyIDtoFirstOne();

    @Override
    protected void validateGetResourcesResponse() {
        runInBlock("Validating resources response", () -> {
            callAndContinueOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(FetchTwoPolicyIds.class);
        });
    }

    @Override
    protected void executeTest() {
        // This method is only called from the second flow on.
        if(flowCounter == 2) {
            eventLog.startBlock("POST endorsement - Expecting 422");
        } else {
            eventLog.startBlock("POST endorsement - Expecting 201");
        }
        postEndorsement();
        eventLog.endBlock();

        eventLog.startBlock("Validate response");
        if(flowCounter == 2) {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        } else {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        }
        eventLog.endBlock();

        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
    }

    @Override
    protected void editEndorsementRequestBody() {
        // This method is only called from the second flow on.
        if(flowCounter == 2){
            callAndStopOnFailure(setEndorsementPolicyIDtoSecondOne().getClass());
        } else {
            callAndStopOnFailure(setEndorsementPolicyIDtoFirstOne().getClass());
        }
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
        flowCounter++;
        if(flowCounter == 2) {
            // Execute the second flow.
            isPolicyIdExtractionFlow = false;
            setupSecondFlow();
            performAuthorizationFlow();
        } else if (flowCounter == 3) {
            // Execute the third and last flow.
            setupSecondFlow();
            performAuthorizationFlow();
        } else {
            // The (third) last flow finished.
            this.fireTestFinished();
        }
    }
}
