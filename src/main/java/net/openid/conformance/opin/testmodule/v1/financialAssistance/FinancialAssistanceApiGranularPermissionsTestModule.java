package net.openid.conformance.opin.testmodule.v1.financialAssistance;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n2.GetInsuranceFinancialAssistanceContractInfoOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n2.GetInsuranceFinancialAssistanceContractsOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n2.GetInsuranceFinancialAssistanceMovementsOASValidatorV1n2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "financial-assistance_api_granular-permissions_test-module_v1",
        displayName = "Validates the structure of all Financial Assistance API",
        summary ="Validates the structure of all Financial Assistance API \n" +
                "• Call the POST Consents with the RESOURCES_READ, FINANCIAL_ASSISTANCE_READ and FINANCIAL_ASSISTANCE_CONTRACTINFO_READ permissions\n" +
                "• Expects 201 -  Validate Response \n" +
                "• Redirect the user to authorize consent \n" +
                "• Calls the GET Consents\n" +
                "• Expects 200 - Validate Response and Status is Authorized\n" +
                "• Calls GET Financial Assistance contracts Endpoint \n" +
                "• Expects 200 - Fetches one of the contract IDs returned \n" +
                "• Calls GET Financial Assistance {contractId} contract-info Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Financial Assistance {contractId} movements Endpoint \n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class FinancialAssistanceApiGranularPermissionsTestModule extends AbstractFinancialAssistanceApiGranularPermissionsTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceFinancialAssistanceContractsOASValidatorV1n2.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getContractInfoValidator() {
        return GetInsuranceFinancialAssistanceContractInfoOASValidatorV1n2.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getMovementsValidator() {
        return GetInsuranceFinancialAssistanceMovementsOASValidatorV1n2.class;
    }
}
