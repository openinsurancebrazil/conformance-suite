package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateCapitalizationTitleWithdrawalPostRequestBody extends AbstractCreateCapitalizationTitleWithdrawalRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {}
}
