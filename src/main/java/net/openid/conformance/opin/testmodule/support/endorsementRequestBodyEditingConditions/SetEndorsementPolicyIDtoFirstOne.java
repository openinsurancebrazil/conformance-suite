package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

public class SetEndorsementPolicyIDtoFirstOne extends AbstractSetEndorsementPolicyIDtoFirstOne {

    protected String getPolicyFieldName(){
        return "policyNumber";
    }
}
