package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;

public abstract class AbstractIdSelector extends AbstractCondition {

    @Override
    @PostEnvironment(strings = "selected_id")
    public Environment evaluate(Environment env) {
        String id = getId(env);
        env.putString("selected_id", id);
        logSuccess("Successfully selected ID", args("id", id));
        return env;
    }

    protected abstract String getId(Environment env);

    protected JsonElement bodyFrom(Environment env, String key) {
        try {
            return BodyExtractor.bodyFrom(env, key)
                    .orElseThrow(() -> error("Could not find response body in the environment"));
        } catch (ParseException e) {
            throw error("Could not parse response body");
        }
    }

}
