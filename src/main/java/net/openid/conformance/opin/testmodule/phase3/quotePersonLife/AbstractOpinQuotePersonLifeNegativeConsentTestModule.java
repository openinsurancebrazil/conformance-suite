package net.openid.conformance.opin.testmodule.phase3.quotePersonLife;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeConsentTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;


public abstract class AbstractOpinQuotePersonLifeNegativeConsentTestModule extends AbstractOpinPhase3QuoteNegativeConsentTestModule {
    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.QUOTE_PERSON_LIFE;
    }

    @Override
    protected void runTests() {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);

        permissionsBuilder.addPermissionsGroup(PermissionsGroup.QUOTE_PERSON_LEAD);
        String productType = env.getString("config", "consent.productType");
        if(productType != null && productType.equals("business")) {
            permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS);
        } else {
            permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL);
        }
        permissionsBuilder.build();
        postConsentExpectingFailure("Call POST Consents with permission for Customer Data (PF or PJ) and Person Lead");

        permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.QUOTE_PERSON_LEAD).addPermissionsGroup(getPermissionsGroup()).build();
        postConsentExpectingFailure("Call POST Consents with permission for Person Lead and the permission for this specified Patrimonial Branch");

        permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.QUOTE_PERSON_LEAD).build();
        postConsentExpectingFailure("Call POST Consents with permission for Person Lead");

    }
}