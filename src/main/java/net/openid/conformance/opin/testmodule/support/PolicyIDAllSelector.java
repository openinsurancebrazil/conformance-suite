package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PolicyIDAllSelector extends AbstractCondition {

	@Override
	@PostEnvironment(strings = "selected_id")
	public Environment evaluate(Environment env) {
		String entityString = env.getString("resource_endpoint_response");
		JsonObject patrimonialList = JsonParser.parseString(entityString).getAsJsonObject();

		JsonArray data = patrimonialList.getAsJsonArray("data");
		if(data.isEmpty()) {
			throw error("Data field is empty, no further processing required.");
		}

		JsonArray allPolicies = new JsonArray();
		for (JsonElement brand : data) {

			if (!brand.isJsonObject()) {
				throw error("Unexpected error: element of data object is not a real Json.");
			}

			JsonArray companies = brand.getAsJsonObject().getAsJsonArray("companies");
			for (JsonElement company : companies) {
				if (!company.isJsonObject()) {
					throw error("Unexpected error: element company is not a real Json.");
				}

				JsonArray policies = company.getAsJsonObject().getAsJsonArray("policies");
				allPolicies.addAll(policies);
			}
		}
		env.putString("selected_id", allPolicies.toString());


		logSuccess(String.format("All policies have been retrieved", allPolicies));
		return env;
	}

}
