package net.openid.conformance.opin.testmodule.v1.rural;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.AbstractBuildConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.AddRuralScope;
import net.openid.conformance.opin.testmodule.support.BuildRuralConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import java.util.Map;

public abstract class AbstractOpinRuralApiTest extends AbstractOpinApiTestModuleV2 {

    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator();

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_RURAL;
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddRuralScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildRuralConfigResourceUrlFromConsentUrl.class;
    }

    @Override
    protected String getApi() {
        return "insurance-rural";
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", getPolicyInfoValidator(),
                "premium", getPremiumValidator(),
                "claim", getClaimValidator()
        );
    }
}
