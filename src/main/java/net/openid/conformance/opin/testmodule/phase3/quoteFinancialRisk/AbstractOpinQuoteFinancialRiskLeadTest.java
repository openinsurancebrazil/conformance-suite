package net.openid.conformance.opin.testmodule.phase3.quoteFinancialRisk;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteFinancialRiskLeadTest extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_FINANCIAL_RISK_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-financial-risk";
    }

    @Override
    protected String getApiName() {
        return "quote-financial-risk";
    }
}
