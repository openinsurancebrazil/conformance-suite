package net.openid.conformance.opin.testmodule.fvp.quote.abstracts;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.AddOpenIdScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetQuoteResourceV1Endpoint;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractOpinFvpPhase3QuoteLeadTestModule extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        super.preConfigure(config, baseUrl, externalUrlOverride);
        configureClient();
    }

    protected void configureClient() {
        callAndStopOnFailure(GetStaticClientConfiguration.class);
        callAndStopOnFailure(AddOpenIdScope.class);
        exposeEnvString("client_id");
        // Test won't pass without MATLS, but we'll try anyway (for now)
        callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
        callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);
        validateClientConfiguration();

    }

    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetQuoteResourceV1Endpoint.class));
    }

    @Override
    protected void runTests() {
        createLead();
        call(new ValidateRegisteredEndpoints(getEndpointCondition()));

        if (testIdempotencyLead()) {
            idempotencyLead();
        }
        patchLead();
    }

}
