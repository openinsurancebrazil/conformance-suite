package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpStatus;

public class EnsureResponseCodeWas406 extends AbstractCondition {

        @Override
        @PreEnvironment(required = "resource_endpoint_response_full" )
        public Environment evaluate(Environment env) {

            int status = env.getInteger("resource_endpoint_response_full", "status");

            if(status != HttpStatus.NOT_ACCEPTABLE.value()) {
                throw error("Was expecting a 406 response");
            } else {
                logSuccess("406 response status, as expected");
            }
            return env;

    }
}
