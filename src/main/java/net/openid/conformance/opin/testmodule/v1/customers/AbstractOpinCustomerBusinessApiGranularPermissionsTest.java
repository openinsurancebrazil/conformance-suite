package net.openid.conformance.opin.testmodule.v1.customers;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBusinessProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinCustomerGranularPermissionsTestModule;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractOpinCustomerBusinessApiGranularPermissionsTest extends AbstractOpinCustomerGranularPermissionsTestModule {

	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessIdentificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessQualificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessComplimentaryInfoValidator();

	@Override
	protected void configureClient(){
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(PrepareConfigForCustomerBusinessTest.class);
		setProductType("business");
		callAndStopOnFailure(AddScopesForCustomerApi.class);
		callAndStopOnFailure(BuildOpinBusinessCustomersConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {

		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS);
		permissionsBuilder.removePermission("CUSTOMERS_BUSINESS_ADDITIONALINFO_READ");
		permissionsBuilder.build();

		setIdentificationsValidator(getCustomerBusinessIdentificationValidator());
		setQualificationsValidator(getCustomerBusinessQualificationValidator());
		setComplimentaryInformationValidator(getCustomerBusinessComplimentaryInfoValidator());

		callAndStopOnFailure(AddDummyBusinessProductTypeToConfig.class);
	}
}

