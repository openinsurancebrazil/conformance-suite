package net.openid.conformance.opin.testmodule.v1.capitalization;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitleEventsOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitlePlansOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "capitalization-title_api_wrong-permissions_test-module_v1",
        displayName = "Ensures Capitalization Title API cannot be called with wrong permissions",
        summary = "Ensures API  cannot be called with wrong permissions - there will be two browser interactions with this test.\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“CAPITALIZATION_TITLE_READ”, “CAPITALIZATION_TITLE_PLANINFO_READ”, “CAPITALIZATION_TITLE_EVENTS_READ”, “CAPITALIZATION_TITLE_SETTLEMENTS_READ”, “RESOURCES_READ”)\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Capitalization Title Plans Endpoint\n" +
                "• Expects 200 - Fetches one of the Plan IDs returned\n" +
                "• Calls GET Capitalization Title {planId} plan-info Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} events Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} settlements Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field \n" +
                "• Expects a success 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Capitalization Title Plans Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Capitalization Title {planId} plan-info Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Capitalization Title {planId} events Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Capitalization Title {planId} settlements Endpoint\n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class CapitalizationTitleApiWrongPermissionsTestModuleV1 extends AbstractCapitalizationTitleApiWrongPermissionsTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPlanInfoValidator() {
        return GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getEventsValidator() {
        return GetInsuranceCapitalizationTitleEventsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getSettlementsValidator() {
        return GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceCapitalizationTitlePlansOASValidatorV1n4.class;
    }
}
