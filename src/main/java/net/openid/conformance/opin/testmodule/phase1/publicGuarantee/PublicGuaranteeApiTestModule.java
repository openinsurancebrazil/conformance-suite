package net.openid.conformance.opin.testmodule.phase1.publicGuarantee;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.publicGuarantee.v1n3.GetProductsNServicesPublicGuaranteeOASValidatorV1n3;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - Public Guarantee API test",
        displayName = "Validate structure of Public Guarantee response",
        summary = "Validate structure of Public Guarantee response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class PublicGuaranteeApiTestModule extends AbstractNoAuthFunctionalTestModule {
    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices Public Guarantee response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetProductsNServicesPublicGuaranteeOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
        });
    }
}
