package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.life;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractGetPersonLifeQuoteStatusOASValidator;

@ApiName("Quote Person 1.11.0")
public class GetPersonLifeQuoteStatusOASValidatorV1n11 extends AbstractGetPersonLifeQuoteStatusOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.11.0.yaml";
    }
}