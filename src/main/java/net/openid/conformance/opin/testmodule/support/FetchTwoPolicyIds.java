package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class FetchTwoPolicyIds extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_endpoint_response_full")
    @PostEnvironment(strings = {"firstPolicyId","secondPolicyId","policyId"})
    public Environment evaluate(Environment env) {

        JsonElement data = bodyFrom(env).getAsJsonObject().get("data");
        if (!data.isJsonArray()) {
            throw error("data object is not a JsonArray.", args("data", data));
        }

        JsonArray dataArray = new JsonArray();
        for (JsonElement dataEl : data.getAsJsonArray()) {
            if(!OIDFJSON.getString(dataEl.getAsJsonObject().get("type")).startsWith("CUSTOMERS_")) {
                dataArray.add(dataEl);
            }
        }

        if(dataArray.size() < 2) {
            throw error("There are less then two resources available", args("data", data));
        }

        String policyId1 = OIDFJSON.getString(Optional.ofNullable(
                        dataArray.get(0).getAsJsonObject().get("resourceId"))
                .orElseThrow(() -> error("Resource does not have resourceId field.")));

        env.putString("firstPolicyId", policyId1);

        String policyId2 = OIDFJSON.getString(Optional.ofNullable(
                        dataArray.get(1).getAsJsonObject().get("resourceId"))
                .orElseThrow(() -> error("Resource does not have resourceId field.")));

        env.putString("secondPolicyId", policyId2);

        env.putString("policyId",policyId1);

        logSuccess(String.format("Two Policy IDs Available: %s and %s",policyId1,policyId2));

        return env;
    }

    protected JsonElement bodyFrom(Environment environment) {
        try {
            return BodyExtractor.bodyFrom(environment, "resource_endpoint_response_full")
                    .orElseThrow(() -> error("Could not extract body from the request"));
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }
    }
}
