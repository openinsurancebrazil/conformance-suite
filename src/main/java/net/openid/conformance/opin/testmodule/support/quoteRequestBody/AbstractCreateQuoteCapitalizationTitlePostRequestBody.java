package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public abstract class AbstractCreateQuoteCapitalizationTitlePostRequestBody extends AbstractCreateQuotePostRequestBody {

    protected abstract String getPaymentType();
    protected abstract String getAmount();
    protected abstract boolean isMonthlyPayment();
    protected abstract boolean isSinglePayment();
    protected abstract boolean isUnitTypePresent();
    protected boolean shouldModifyTermDates() {
        return false;
    }

    private JsonObject createPaymentDetails() {
        JsonObject payment = new JsonObject();
        payment.addProperty("amount", getAmount());
        if (isUnitTypePresent()) {
            payment.addProperty("unitType", "MONETARIO");
        }

        JsonObject unit = new JsonObject();
        unit.addProperty("code", "R$");
        unit.addProperty("description", "BRL");

        payment.add("unit", unit);
        return payment;
    }
    @Override
    protected void editData(Environment env, JsonObject data) {
        this.addQuoteData(env, data);
    }

    private void addQuoteData(Environment env, JsonObject data) {
        JsonObject quoteData = new JsonObject();
        if (shouldModifyTermDates()) {
            var today = ZonedDateTime
                    .now(ZoneOffset.UTC)
                    .toLocalDateTime();
            quoteData.addProperty("termStartDate", today
                    .plusDays(1)
                    .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
            quoteData.addProperty("termEndDate", today
                    .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
        }
        quoteData.addProperty("modality", "TRADICIONAL");
        quoteData.addProperty("paymentType", getPaymentType());


        JsonObject paymentDetails = createPaymentDetails();
        if (isMonthlyPayment()) {
            quoteData.add("monthlyPayment", paymentDetails);
        }
        if (isSinglePayment()) {
            quoteData.add("singlePayment", paymentDetails);
        }
        data.add("quoteData", quoteData);
    }
}
