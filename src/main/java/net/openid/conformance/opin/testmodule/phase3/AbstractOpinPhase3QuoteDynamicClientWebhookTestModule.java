package net.openid.conformance.opin.testmodule.phase3;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AddWebhookUrisToDynamicRegistrationRequest;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.CheckClientConfigurationDoesNotContainWebhookUri;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureSoftwareStatementContainsWebhook;
import net.openid.conformance.opin.testmodule.support.ValidateWebhookNotifications;
import net.openid.conformance.opin.testmodule.support.sequences.OpinExecuteDCR;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;

public abstract class AbstractOpinPhase3QuoteDynamicClientWebhookTestModule extends AbstractOpinPhase3QuoteTestModule {
    protected String expectedWebhookType() {
        return "quote";
    }

    protected boolean expectingWebhookNotification = false;

    @Override
    protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
        return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence)
                .skip(GetDynamicServerConfiguration.class, "")
                .skip(ExtractMTLSCertificatesFromConfiguration.class, "")
                .skip(AddMTLSEndpointAliasesToEnvironment.class, "")
                .skip(GetStaticClientConfiguration.class, "we'll use the client created with DCR")
                .skip(ExtractJWKsFromStaticClientConfiguration.class, "");
    }

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        super.preConfigure(config, baseUrl, externalUrlOverride);
        callAndStopOnFailure(CreateRedirectUri.class);
        if(shouldAlsoDoDCM()) {
            doDCRAndDCM();
        } else {
            doDCR();
        }
    }

    protected boolean shouldAlsoDoDCM() {
        return false;
    }

    private void doDCR() {
        eventLog.startBlock("Register the client using DCR");
        callAndStopOnFailure(GetDynamicServerConfiguration.class);
        call(new OpinExecuteDCR(false, getVariant(ClientAuthType.class), env)
                .insertAfter(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class, condition(EnsureSoftwareStatementContainsWebhook.class))
                .insertBefore(CallDynamicRegistrationEndpoint.class, condition(AddWebhookUrisToDynamicRegistrationRequest.class))
        );
        eventLog.endBlock();
    }

    private void doDCRAndDCM() {
        eventLog.startBlock("Register the client using DCR");
        callAndStopOnFailure(GetDynamicServerConfiguration.class);
        call(new OpinExecuteDCR(false, getVariant(ClientAuthType.class), env)
                .insertAfter(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class, condition(EnsureSoftwareStatementContainsWebhook.class))
        );
        env.mapKey("registration_client_endpoint_response", "dynamic_registration_endpoint_response");
        callAndStopOnFailure(CheckClientConfigurationDoesNotContainWebhookUri.class);
        env.unmapKey("registration_client_endpoint_response");
        eventLog.endBlock();

        // DCM
        eventLog.startBlock("Update the client using DCM");
        callAndStopOnFailure(CreateClientConfigurationRequestFromDynamicClientRegistrationResponse.class);
        callAndStopOnFailure(AddSoftwareStatementToClientConfigurationRequest.class);
        env.mapKey("dynamic_registration_request", "registration_client_endpoint_request_body");
        callAndStopOnFailure(AddWebhookUrisToDynamicRegistrationRequest.class);
        env.unmapKey("dynamic_registration_request");
        callAndStopOnFailure(CallClientConfigurationEndpoint.class);
        callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
        callAndContinueOnFailure(CheckRegistrationClientEndpointContentType.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
        callAndContinueOnFailure(CheckClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
        callAndContinueOnFailure(CheckClientConfigurationUriFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
        callAndContinueOnFailure(CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
        eventLog.endBlock();
    }

    @Override
    protected void waitQuote() {
        eventLog.startBlock("The test will now wait during 1 minute for the webhook to be received");
        expectingWebhookNotification = true;
        env.putString("waiting_webhook_since", Instant.now().toString());
        setStatus(Status.WAITING);
        try {
            Thread.sleep(60 * 1000);
        } catch (InterruptedException e) {
            throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
        }
        setStatus(Status.RUNNING);
        expectingWebhookNotification = false;
        eventLog.endBlock();

        eventLog.startBlock("Validate webhooks received");
        env.putString("webhook_type", expectedWebhookType());
        env.putInteger("expected_number_of_webhooks", 1);
        callAndStopOnFailure(ValidateWebhookNotifications.class);
        eventLog.endBlock();
    }

    @Override
    public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
        String consentId = env.getString("consent_id");
        String pathPattern = String.format("^open-insurance\\/webhook\\/v\\d+\\/quote\\/v\\d+\\/request/%s/quote-status$", consentId);
        if(!expectingWebhookNotification || !path.matches(pathPattern)) {
            throw new TestFailureException(getId(), "Got a webhook response we weren't expecting");
        }

        registerWebhookNotification(expectedWebhookType());
        return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
    }

    private void registerWebhookNotification(String webhookType) {
        JsonObject webhook = env.getObject("webhook");
        if(webhook == null) {
            webhook = new JsonObject();
            webhook.add("notifications", new JsonArray());
            env.putObject("webhook", webhook);
        }

        JsonObject notification = new JsonObject();
        notification.addProperty("received_at", Instant.now().toString());
        notification.addProperty("type", webhookType);

        JsonArray notifications = webhook.getAsJsonArray("notifications");
        notifications.add(notification);
    }

    @Override
    public void cleanup() {
        callAndContinueOnFailure(UnregisterDynamicallyRegisteredClient.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2.3");
    }
}
