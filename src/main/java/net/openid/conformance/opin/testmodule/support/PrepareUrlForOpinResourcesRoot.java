package net.openid.conformance.opin.testmodule.support;

public class PrepareUrlForOpinResourcesRoot extends AbstractPrepareUrlForApi {

	@Override
	protected String getApiReplacement() {
		return "resources";
	}

	@Override
	protected String getEndpointReplacement() {
		return "resources";
	}
}
