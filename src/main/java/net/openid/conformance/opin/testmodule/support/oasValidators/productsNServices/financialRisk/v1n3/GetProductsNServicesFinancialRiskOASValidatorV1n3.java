package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.financialRisk.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api Source: swagger/openinsurance/productsServices/swagger-financial-risk-1.3.0.yaml
 * Api endpoint: /financial-risk
 * Api version: 1.3.0
 */
@ApiName("ProductsServices Financial Risk")
public class GetProductsNServicesFinancialRiskOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/swagger-financial-risk-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/financial-risk";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

}