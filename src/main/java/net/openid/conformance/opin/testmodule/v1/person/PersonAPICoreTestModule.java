package net.openid.conformance.opin.testmodule.v1.person;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonClaimOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonPolicyInfoOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonPremiumOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.v1.person.abstracts.AbstractPersonAPICoreTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "person_api_core_test-module_v1",
        displayName = "Validates the structure of all person API resources",
        summary ="Validates the structure of all Person API \n" +
                "\u2022 Call the POST consents with all the permissions needed to access the Person API (“DAMAGES_AND_PEOPLE_PERSON_READ”, “DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ”, “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 -  Validate Response \n" +
                "\u2022 Redirect the user to authorize consent \n" +
                "\u2022 Calls GET Person \"/\"  Endpoint \n" +
                "\u2022 Expects 200 - Fetches one of the policy IDs returned \n" +
                "\u2022 Calls GET Person Policy Info Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Person Premium  Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Person Claim Endpoint \n" +
                "\u2022 Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class PersonAPICoreTestModule extends AbstractPersonAPICoreTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePersonListOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
        return GetInsurancePersonPolicyInfoOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
        return GetInsurancePersonPremiumOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsurancePersonClaimOASValidatorV1n5.class;
    }
}
