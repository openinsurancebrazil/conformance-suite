package net.openid.conformance.opin.testmodule.support;

public class CreateClaimNotificationRequestBody extends AbstractCreateClaimNotificationRequestBody {

    @Override
    protected String getPolicyFieldName() {
        return "policyNumber";
    }
}
