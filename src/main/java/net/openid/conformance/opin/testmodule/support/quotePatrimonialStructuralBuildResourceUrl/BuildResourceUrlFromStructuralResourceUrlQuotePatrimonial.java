package net.openid.conformance.opin.testmodule.support.quotePatrimonialStructuralBuildResourceUrl;

import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.OpinStructuralResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class BuildResourceUrlFromStructuralResourceUrlQuotePatrimonial extends OpinStructuralResourceBuilder {

    private String type;

    @Override
    @PreEnvironment(required = "config", strings = "quote_patrimonial_type")
    @PostEnvironment(strings = "protected_resource_url")
    public Environment evaluate(Environment env) {
        setApi("quote-patrimonial");
        type = env.getString("quote_patrimonial_type");
        return super.evaluate(env);
    }

    @Override
    protected String getEndpointReplacement() {
        return String.format("%s/request", type);
    }
}
