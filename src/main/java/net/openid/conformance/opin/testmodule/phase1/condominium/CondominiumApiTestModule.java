package net.openid.conformance.opin.testmodule.phase1.condominium;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.condominium.v1n3.GetCondominiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - Condominium API test",
        displayName = "Validate structure of Condominium response",
        summary = "Validate structure of Condominium response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class CondominiumApiTestModule extends AbstractNoAuthFunctionalTestModule {
    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices Condominium response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            updateResourceUrlWithCommercializationArea("12345");
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetCondominiumOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
        });
    }

    private void updateResourceUrlWithCommercializationArea(String commercializationArea) {
        String resourceUrl = env.getString("protected_resource_url");
        if (resourceUrl.endsWith("condominium") || resourceUrl.endsWith("condominium/")) {
            resourceUrl += "/" + commercializationArea;
        }
        env.putString("protected_resource_url", resourceUrl);
    }
}