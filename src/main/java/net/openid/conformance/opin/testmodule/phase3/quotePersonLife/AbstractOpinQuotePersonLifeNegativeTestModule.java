package net.openid.conformance.opin.testmodule.phase3.quotePersonLife;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePersonLifePostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;


public abstract class AbstractOpinQuotePersonLifeNegativeTestModule extends AbstractOpinPhase3QuoteNegativeTestModule {

    @Override
    protected OPINScopesEnum getLeadScope() {
        return OPINScopesEnum.QUOTE_PERSON_LEAD;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PERSON_LIFE;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-person-life";
    }

    @Override
    protected String getApiName() {
        return "quote-person";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "life/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePersonLifePostRequestBody.class;
    }
}