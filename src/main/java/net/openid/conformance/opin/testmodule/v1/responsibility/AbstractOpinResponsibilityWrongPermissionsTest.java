package net.openid.conformance.opin.testmodule.v1.responsibility;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModuleV2;
import net.openid.conformance.opin.testmodule.support.AbstractBuildConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.AddResponsibilityScope;
import net.openid.conformance.opin.testmodule.support.BuildResponsibilityConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import java.util.Map;

public abstract class AbstractOpinResponsibilityWrongPermissionsTest extends AbstractOpinWrongPermissionsTestModuleV2 {

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator();

	@Override
	protected PermissionsGroup getPermissionsGroup() {
		return PermissionsGroup.DAMAGES_AND_PEOPLE_RESPONSIBILITY;
	}

	@Override
	protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
		return AddResponsibilityScope.class;
	}

	@Override
	protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
		return BuildResponsibilityConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected String getApi() {
		return "insurance-responsibility";
	}

	@Override
	protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
		return Map.of(
				"policy-info", getPolicyInfoValidator(),
				"premium", getPremiumValidator(),
				"claim", getClaimValidator()
		);
	}
}
