package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateQuotePatrimonialDiverseRisksPostRequestBody extends AbstractCreateQuotePostRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        editQuoteData(env, data);
    }

    private void editQuoteData(Environment env, JsonObject data) {
        JsonObject quoteData = data.getAsJsonObject("quoteData");

        quoteData.addProperty("isCollectiveStipulated", false);
        quoteData.add("insuranceTermStartDate", quoteData.get("termStartDate"));
        quoteData.addProperty("insuredObjectType", "EQUIPAMENTO_MOVEL");
        quoteData.add("beneficiaries", getBeneficiaries(env));
    }
}
