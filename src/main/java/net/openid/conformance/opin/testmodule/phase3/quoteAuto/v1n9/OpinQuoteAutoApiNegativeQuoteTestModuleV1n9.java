package net.openid.conformance.opin.testmodule.phase3.quoteAuto.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.phase3.quoteAuto.AbstractOpinQuoteAutoApiNegativeQuoteTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n8.GetQuoteAutoOASValidatorV1n8;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.GetQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteAutoPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
        testName = "opin-quote-auto_api_negative-quote_test-module_v1.9.0",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = """
            Ensure a consent cannot be created in unhappy requests.
            • Call POST Auto request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload
            • Expect 422 - Validate Error Response
            • Call POST Auto request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with "quote-auto-lead" scope
            • Expect 403 - Validate Error Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)

public class OpinQuoteAutoApiNegativeQuoteTestModuleV1n9 extends AbstractOpinQuoteAutoApiNegativeQuoteTestModule {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return GetQuoteAutoOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteAutoPostRequestBodyWithCoverage.class;
    }
}
