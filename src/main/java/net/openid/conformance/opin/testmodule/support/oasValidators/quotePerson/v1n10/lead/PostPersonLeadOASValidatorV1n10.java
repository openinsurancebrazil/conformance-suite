package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.lead;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractPostPersonLeadOASValidator;

@ApiName("Quote Person 1.10.2")
public class PostPersonLeadOASValidatorV1n10 extends AbstractPostPersonLeadOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.10.2.yaml";
    }

}