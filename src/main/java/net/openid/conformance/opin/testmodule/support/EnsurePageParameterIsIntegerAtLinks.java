package net.openid.conformance.opin.testmodule.support;

public class EnsurePageParameterIsIntegerAtLinks extends AbstractEnsureUrlParameterIsValidAtLinks {
    @Override
    protected String getUrlParameter() {
        return "page";
    }
    @Override
    protected boolean urlParameterIsValid(String urlParameter) {
        String positiveIntegerRegex = "\\d+";
        return urlParameter == null || urlParameter.matches(positiveIntegerRegex);
    }
}
