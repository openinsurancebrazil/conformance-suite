package net.openid.conformance.opin.testmodule.phase1.referenceNetwork;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.oasValidators.channels.referencedNetwork.v1n4.GetChannelsReferencedNetworkOASValidatorV1n4;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - Channels - Referenced Network API test",
	displayName = "Validate structure of Channels - Referenced Network response",
	summary = "Validate structure of Channels - Referenced Network response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class ReferencedNetworkTestModule extends AbstractNoAuthFunctionalTestModule {
	@Override
	protected void runTests() {
		runInBlock("Validate Channels Referenced Network response", () -> {
			callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetChannelsReferencedNetworkOASValidatorV1n4.class, Condition.ConditionResult.FAILURE);
		});
	}
}
