package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetInsurancePensionPlanContractInfoOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/insurance-pension-plan/{pensionIdentification}/contract-info";
	}
	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
