package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.condition.client.SetResourceMethodToPost;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteWithDynamicFieldsTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.capitalizationTitle.v1n3.GetDynamicFieldsCapitalizationTitleOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchAcknRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;


public abstract class AbstractOpinQuoteCapitalizationTitleDynamicFieldsTestModule extends AbstractOpinPhase3QuoteWithDynamicFieldsTestModule {
    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_CAPITALIZATION_TITLE;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getApiName() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "/request";
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchAcknRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasAckn.class;
    }


    // Capitalization Title does not have api type field
    @Override
    protected String getDynamicFieldApiType() {
        return null;
    }

    @Override
    protected void getDynamicFields() {
        runInBlock("Get dynamic fields", () -> {

            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            call(setHeadersSequence());
            callAndStopOnFailure(SetResourceMethodToGet.class);
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
            callAndStopOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            getDynamicFieldsValidator();
            callAndStopOnFailure(ExtractModalityType.class, Condition.ConditionResult.FAILURE);
            env.getString("modality");
            callAndContinueOnFailure(ExtractDynamicFieldsFromCapitalizationTitle.class, Condition.ConditionResult.FAILURE);
        });
    }

    @Override
    protected void getDynamicFieldsValidator() {
        callAndContinueOnFailure(GetDynamicFieldsCapitalizationTitleOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void postQuote() {
        runInBlock("Create a quote", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            call(setHeadersSequence());
            call(setIdempotencyKey());
            callAndStopOnFailure(createPostQuoteRequestBody());
            callAndStopOnFailure(AddDynamicFieldsToQuoteRequest.class);
            callAndStopOnFailure(AddModalityToQuoteRequest.class);
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(SetResourceMethodToPost.class);
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the quote creation response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas201.class);
            callAndContinueOnFailure(ensurePostQuoteStatus(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validatePostQuoteResponse(), Condition.ConditionResult.FAILURE);
        });
    }

}