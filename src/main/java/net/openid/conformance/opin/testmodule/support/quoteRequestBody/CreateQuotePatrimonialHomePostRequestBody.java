package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CreateQuotePatrimonialHomePostRequestBody extends AbstractCreateQuotePostRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        addQuoteData(env, data);
    }

    private void addQuoteData(Environment env, JsonObject data) {
        JsonObject unit = new JsonObject();
        unit.addProperty("code", "R$");
        unit.addProperty("description", "BRL");

        JsonObject maxLmg = new JsonObject();
        maxLmg.addProperty("amount", "2000.00");
        maxLmg.add("unit", unit);

        JsonObject quoteData = new JsonObject();
        quoteData.addProperty("hasCommercialActivity", true);
        quoteData.addProperty("isCollectiveStipulated", false);
        quoteData.addProperty("hasOneRiskLocation", true);
        String today = ZonedDateTime
                .now(ZoneOffset.UTC)
                .toLocalDateTime()
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT));
        quoteData.addProperty("termStartDate", today);
        quoteData.addProperty("termEndDate", today);
        quoteData.addProperty("insuranceType", "RENOVACAO");
        quoteData.addProperty("policyId", "111111");
        quoteData.addProperty("insurerId", "insurer_id");
        quoteData.addProperty("currency", "BRL");
        quoteData.add("maxLMG", maxLmg);
        quoteData.addProperty("includesAssistanceServices", false);
        quoteData.add("beneficiaries", getBeneficiaries(env));

        data.add("quoteData", quoteData);
    }
}
