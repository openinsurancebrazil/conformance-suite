package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetInsuranceAutoPremiumOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/insurance-auto/{policyId}/premium";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}