package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteFinancialRiskLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "PROTECAO_DE_BENS";
    }
}
