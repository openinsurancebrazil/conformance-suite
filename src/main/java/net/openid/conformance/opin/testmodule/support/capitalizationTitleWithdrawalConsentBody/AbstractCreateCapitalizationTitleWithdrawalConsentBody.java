package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateCapitalizationTitleWithdrawalConsentBody extends AbstractCreateCapitalizationTitleXConsentBody {

    @Override
    @PreEnvironment(required = {"consent_endpoint_request","series_1","product_1"}, strings = "planId")
    public Environment evaluate(Environment env) {
        String planId = env.getString("planId");
        JsonObject data = new JsonObject();
        JsonObject consentData = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();
        JsonObject series = env.getObject("series_1");
        JsonObject product = env.getObject("product_1");

        data.addProperty("capitalizationTitleName", product.get("productName").getAsString());
        data.addProperty("planId", planId);
        data.addProperty("titleId",series.get("titles").getAsJsonArray().get(0).getAsJsonObject().get("titleId").getAsString());
        data.addProperty("seriesId",series.get("seriesId").getAsString());
        data.addProperty("termEndDate",series.get("titles").getAsJsonArray().get(0).getAsJsonObject().get("termEndDate").getAsString());
        data.addProperty("withdrawalReason","INSATISFACAO_COM_CARACTERISTICAS_DO_PRODUTO");

        JsonObject withdrawalTotalAmount = new JsonObject();
        String amount = series.get("titles").getAsJsonArray().get(0).getAsJsonObject().get("technicalProvisions").getAsJsonArray().get(0).getAsJsonObject().get("prAmount").getAsJsonObject().get("amount").toString();
        withdrawalTotalAmount.addProperty("amount", ensureTwoDecimalPlaces(amount));
        JsonObject unit = new JsonObject();
        unit.addProperty("code","R$");
        unit.addProperty("description","BRL");
        withdrawalTotalAmount.add("unit", unit);

        data.add("withdrawalTotalAmount",withdrawalTotalAmount);

        editData(env, data);

        consentData.add("withdrawalCaptalizationInformation", data);
        logSuccess("Consent body created and added",args("consent_endpoint_request", consentData));
        return env;
    }

    protected abstract void editData(Environment env, JsonObject data);

    protected String ensureTwoDecimalPlaces(String input) {
        double number = Double.parseDouble(input);
        return String.format("%.2f", number);
    }
}
