package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.testmodule.Environment;

public class OpinAddDummyCpfToConfig extends AbstractCondition {

		@PreEnvironment(required = {"config"})
		public Environment evaluate(Environment env) {
			String dummyCPF = "27801076001";
			env.putString("config", "resource.brazilCpf", dummyCPF);
			this.logSuccess("Dummy CPF added successfully", this.args(new Object[]{"dummy CPF", dummyCPF}));
			return env;
		}
	}
