package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class RemoveConsentIdFromQuoteRequestBody extends AbstractCondition {
    @Override
    @PreEnvironment(required = "resource_request_entity")
    @PostEnvironment(required = "resource_request_entity", strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject requestData = env.getObject("resource_request_entity");
        JsonObject data = requestData.getAsJsonObject("data");
        data.remove("consentId");

        logSuccess("Removed the consent ID from the request body", args(
                "request data",
                requestData
        ));

        env.putString("resource_request_entity", requestData.toString());

        return env;
    }
}
