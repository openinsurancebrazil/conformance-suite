package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddLifePensionScope;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.directory.GetLifePensionV1Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Map;

@PublishTestModule(
        testName = "opin-fvp-life-pension-api-test-module",
        displayName = "Validates the structure of all Life Pension API",
        summary ="Validates the structure of all Life Pension API \n" +
                "\u2022 Call the POST consents with all the permissions needed to access the Capitalization Title API (“LIFE_PENSION_READ”, “LIFE_PENSION_CONTRACTINFO_READ”, “LIFE_PENSION_MOVEMENTS_READ”, “LIFE_PENSION_PORTABILITIES_READ”,“LIFE_PENSION_WITHDRAWALS_READ”,“LIFE_PENSION_CLAIM”,“RESOURCES_READ”)\n" +
                "\u2022 Expects 201 -  Validate Response \n" +
                "\u2022 Redirect the user to authorize consent \n" +
                "\u2022 Calls GET Life Pension contracts Endpoint \n" +
                "\u2022 Expects 200 - Fetches one of the certificate IDs returned \n" +
                "\u2022 Calls GET Life Pension {certificateId} contract-info Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} movements Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} portabilities Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} withdrawals Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} claim Endpoint \n" +
                "\u2022 Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.consentSyncTime",
                "consent.productType"
        }
)
public class OpinFvpLifePensionApiTestModule extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetLifePensionV1Endpoint.class));
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddLifePensionScope.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.LIFE_PENSION;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceLifePensionContractsOASValidatorV1n4.class;
    }

    @Override
    protected String getApi() {
        return "insurance-life-pension";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].contracts[0].certificateId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "contract-info", GetInsuranceLifePensionContractInfoOASValidatorV1n4.class,
                "movements", GetInsuranceLifePensionMovementsOASValidatorV1n4.class,
                "portabilities", GetInsuranceLifePensionPortabilitiesOASValidatorV1n4.class,
                "withdrawals", GetInsuranceLifePensionWithdrawalsOASValidatorV1n4.class,
                "claim", GetInsuranceLifePensionClaimOASValidatorV1n4.class
        );
    }
}
