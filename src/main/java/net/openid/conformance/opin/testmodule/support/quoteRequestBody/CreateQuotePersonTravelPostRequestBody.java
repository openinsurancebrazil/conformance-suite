package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CreateQuotePersonTravelPostRequestBody extends AbstractCreateQuotePostRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        this.addQuoteData(env, data);
    }

    private void addQuoteData(Environment env, JsonObject data) {
        JsonObject quoteData = new JsonObject();
        var today = ZonedDateTime
                .now(ZoneOffset.UTC)
                .toLocalDateTime();
        quoteData.addProperty("termStartDate", today
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
        quoteData.addProperty("termEndDate", today
                .plusDays(1)
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
        quoteData.addProperty("travelType", "LAZER");
        quoteData.addProperty("includeAssistanceServices", false);

        data.add("quoteData", quoteData);
    }
}