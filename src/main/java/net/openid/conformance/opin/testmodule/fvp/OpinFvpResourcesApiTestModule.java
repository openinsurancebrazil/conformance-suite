package net.openid.conformance.opin.testmodule.fvp;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractOBBrasilFunctionalTestModuleOptionalErrors;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.EnsureDcrClientExists;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinResourcesV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;

import java.util.Optional;


@PublishTestModule(
	testName = "opin-fvp-resources-api-test",
	displayName = "Validate structure of all resources API resources",
	summary = "Validates the structure of all resources API resources\n" +
		"• Creates a Consent will all of the existing permissions \n" +
		"• Checks all of the fields sent on the consent API are specification compliant\n" +
		"• Calls the GET resources API\n" +
		"• Expects a 200 - Validate Reponse",
	profile = OBBProfile.OPIN_PROFILE_PROD_FVP,
	configurationFields = {
			"server.discoveryUrl",
			"resource.brazilCpf",
			"resource.brazilCnpj"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
		"resource.consentUrl","resource.brazilOrganizationId"
})
public class OpinFvpResourcesApiTestModule extends AbstractOBBrasilFunctionalTestModuleOptionalErrors {

	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	@Override
	protected void configureClient(){
		callAndStopOnFailure(EnsureBrazilCpf.class);
		callAndStopOnFailure(EnsureDcrClientExists.class);
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetOpinResourcesV2Endpoint.class))));
		super.configureClient();
		env.putString("api_type", API_TYPE);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps(){
		OpinPreAuthorizationConsentApi steps = new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
		return steps;
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
		permissionsBuilder.buildFromEnv();

		callAndStopOnFailure(AddResourcesScope.class);
	}

	@Override
	protected void validateResponse() {

		runInBlock("Validate resources api request", () -> {
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
			callAndStopOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);
		});

	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		String resourceResponse = "resource_endpoint_response_full";
		String consentResponse = "consent_endpoint_response_full";
		String methodString = env.getString("http_method");
		HttpMethod method;
		if(methodString != null) {
			method = HttpMethod.valueOf(methodString);
			if (method.equals(HttpMethod.DELETE)) {
				return;
			}
		}

		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
				validateLinksAndMeta(resourceResponse);
			} else {
				validateLinksAndMeta(consentResponse);
			}
		}
		if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
				CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass())) {
			validateLinksAndMeta("consent_endpoint_response_full");
		}
	}

	private void validateLinksAndMeta(String responseFull) {

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
				.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
		if (!isEndpointCallSuccessful(status)) {
			return;
		}

		if (responseFull.contains("resource") || env.getElementFromObject(responseFull, "body_json.meta") != null) {
			if (responseFull.contains("resource")) {
				env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			}

			call(condition(OpinPaginationValidator.class)
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure());

			if (responseFull.contains("resource")) {
				env.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
			}
		}

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
		if (recursion.equals("false")) {
			callAndStopOnFailure(SaveOldValues.class);
			env.putString("recursion", "true");
			env.mapKey("resource_endpoint_response_full", responseFull);

			if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
				validateLinks(responseFull);
			}

			env.unmapKey("resource_endpoint_response_full");
			callAndStopOnFailure(LoadOldValues.class);
			env.putString("recursion", "false");
		}
	}

	private boolean isEndpointCallSuccessful(int status) {
		return status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK;
	}

	private void validateLinks(String responseFull) {
		call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
		env.mapKey("resource_endpoint_response_full", responseFull);

		ConditionSequence sequence = new ValidateSelfEndpoint();

		if (responseFull.contains("consent")) {
			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
		}

		call(sequence);
	}
}
