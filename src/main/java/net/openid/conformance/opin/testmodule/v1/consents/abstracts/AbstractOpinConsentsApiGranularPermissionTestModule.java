package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractOpinConsentsApiGranularPermissionTestModule extends AbstractOpinFunctionalTestModule {

        protected abstract AbstractJsonAssertingCondition postValidator();
        private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

        @Override
        protected void configureClient() {
                super.configureClient();
                env.putString("api_type", API_TYPE);
                callAndStopOnFailure(BuildOpinResourcesConfigResourceUrlFromConsentUrl.class);
        }

        @Override
        protected void onConfigure(JsonObject config, String baseUrl){
                OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
                callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
                permissionsBuilder.buildFromEnv();

                callAndStopOnFailure(StoreScope.class);
                callAndStopOnFailure(AddResourcesScope.class);
        }

        @Override
        protected void requestProtectedResource() {
                preCallProtectedResource("GET resources API to select first resource available");
                validateGetResourcesResponse();

                eventLog.startBlock("Deleting consent");
                call(deleteConsent());
                eventLog.endBlock();
        }

        protected void executeTest(){
                String firstResourcePermissions = env.getString("firstResourceType");

                OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);

                if(firstResourcePermissions.equals("CUSTOMERS_PERSONAL") || firstResourcePermissions.equals("CUSTOMERS_BUSINESS")) {

                        runInBlock("Call the POST Consents with the RESOURCES_READ and IDENTIFICATIONS_READ permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_QUALIFICATION_READ")
                                        .removePermission(firstResourcePermissions + "_ADDITIONALINFO_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ and QUALIFICATION_READ permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_IDENTIFICATIONS_READ")
                                        .removePermission(firstResourcePermissions + "_ADDITIONALINFO_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ, QUALIFICATION_READ and ADDITIONALINFO_READ permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_IDENTIFICATIONS_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });
                }
                else {
                        runInBlock("Call the POST Consents with the RESOURCES_READ and READ permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_POLICYINFO_READ")
                                        .removePermission(firstResourcePermissions + "_CLAIM_READ")
                                        .removePermission(firstResourcePermissions + "_PREMIUM_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ and POLICYINFO permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_READ")
                                        .removePermission(firstResourcePermissions + "_CLAIM_READ")
                                        .removePermission(firstResourcePermissions + "_PREMIUM_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ and PREMIUM permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_READ")
                                        .removePermission(firstResourcePermissions + "_CLAIM_READ")
                                        .removePermission(firstResourcePermissions + "_POLICYINFO_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ and CLAIM permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_READ")
                                        .removePermission(firstResourcePermissions + "_POLICYINFO_READ")
                                        .removePermission(firstResourcePermissions + "_PREMIUM_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ, READ and POLICYINFO permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_CLAIM_READ")
                                        .removePermission(firstResourcePermissions + "_PREMIUM_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ, READ and CLAIM permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_POLICYINFO_READ")
                                        .removePermission(firstResourcePermissions + "_PREMIUM_READ")
                                        .build();
                                validateGranularPermissions();
                                permissionsBuilder.resetPermissions().build();
                        });

                        runInBlock("Call the POST Consents with the RESOURCES_READ, READ, PREMIUM and POLICYINFO permissions", () -> {
                                permissionsBuilder.addPermissionsGroup(PermissionsGroup.valueOf(firstResourcePermissions))
                                        .removePermission(firstResourcePermissions + "_CLAIM_READ")
                                        .build();
                                validateGranularPermissions();
                                deleteConsent();
                                permissionsBuilder.resetPermissions().build();
                        });

                }
        }

        protected void validateGetResourcesResponse() {
                runInBlock("Validating resources response", () -> {
                        callAndContinueOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);
                        callAndStopOnFailure(GetResourceTypeOfFirstResourceAvailable.class);
                });
        }

        protected ConditionSequence deleteConsent() {
                return sequenceOf(
                        condition(LoadConsentsAccessToken.class),
                        condition(PrepareToFetchConsentRequest.class),
                        condition(PrepareToDeleteConsent.class),
                        condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
                );
        }

        protected void validateGranularPermissions(){
                callAndStopOnFailure(PrepareToPostConsentRequest.class);
                new ScopesAndPermissionsBuilder(env, eventLog).addScopes(OPINScopesEnum.CONSENTS).build();
                callAndStopOnFailure(SetContentTypeApplicationJson.class);
                callAndStopOnFailure(FAPIBrazilOpenInsuranceCreateConsentRequest.class);
                callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
                callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class,Condition.ConditionResult.FAILURE);
                callAndStopOnFailure(postValidator().getClass());
        }

        @Override
        protected void onPostAuthorizationFlowComplete() {
                executeTest();
                super.onPostAuthorizationFlowComplete();
        }

        @Override
        protected void validateResponse() {}
}
