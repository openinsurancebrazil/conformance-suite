package net.openid.conformance.opin.testmodule.v1.patrimonial;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts.AbstractOpinPatrimonialBranchTestModule;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "opin-patrimonial-riscos-engenharia-api-branch-test",
	displayName = "Validates if at least one Policy Id returned is from “Riscos de Engenharia” branch",
	summary = "Validates if at least one Policy Id returned is from “Riscos de Engenharia” branch\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Patrimonial API (“DAMAGES_AND_PEOPLE_PATRIMONIAL_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ”,  “RESOURCES_READ”) \n" +
		"\u2022 Expects 201 - Expects Success on Redirect\n" +
		"\u2022 Calls GET Patrimonial “/” API\n" +
		"\u2022 Expects 201 - Loops through all of the Policy IDs returned\n" +
		"\u2022 Calls GET Patrimonial policy-Info API with each Policy ID\n" +
		"\u2022 Expects 200 - Validate if at least one of the branch object on the response body has the value of  “0167” (Riscos de Engenharia)\n" +
		"\u2022 Return success if at least one of the policy IDs is of ”Riscos de Engenharia”, return an warning otherwise",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"resource.brazilCnpj",
		"consent.productType"
	}
)

public class OpinPatrimonialRiscosDeEngenhariaBranchTestModule extends AbstractOpinPatrimonialBranchTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		setBranch(PatrimonialBranches.RISCOS_ENGENHARIA);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsurancePatrimonialListOASValidatorV1n4.class;
	}
}
