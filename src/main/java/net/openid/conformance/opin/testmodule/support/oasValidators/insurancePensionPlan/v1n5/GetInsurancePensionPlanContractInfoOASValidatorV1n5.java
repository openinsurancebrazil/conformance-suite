package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.AbstractGetInsurancePensionPlanContractInfoOASValidator;

@ApiName("Insurance Pension Plan 1.5.0")
public class GetInsurancePensionPlanContractInfoOASValidatorV1n5 extends AbstractGetInsurancePensionPlanContractInfoOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insurancePensionPlan/v1/swagger-insurance-pension-plan-v1.5.0.yaml";
	}
}
