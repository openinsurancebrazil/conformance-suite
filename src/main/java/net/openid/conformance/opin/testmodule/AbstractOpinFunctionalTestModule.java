package net.openid.conformance.opin.testmodule;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.EnsureRefreshTokenNotRotated;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ExtractAndAddRefreshTokenToEnv;
import net.openid.conformance.opin.testmodule.phase3.ClearIdempotencyKeyHeader;
import net.openid.conformance.opin.testmodule.support.OpinInsertMtlsCa;
import net.openid.conformance.opin.testmodule.support.OpinPaginationValidator;
import net.openid.conformance.opin.testmodule.support.WaitForConsentSyncTimeFromConfig;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;

import java.util.Map;
import java.util.Optional;

public abstract  class AbstractOpinFunctionalTestModule extends AbstractOBBrasilFunctionalTestModule {

	@Override
	protected void configureClient() {
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		super.configureClient();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		call(createOBBPreauthSteps());
	}

	protected void useClientCredentialsToken() {
		env.mapKey("access_token", "consents_access_token");
		eventLog.log("Loading client credentials token", Map.of("access_token", env.getObject("access_token")));
	}

	protected void useAuthorizationCodeToken() {
		unmapClientCredentialsToken();
		eventLog.log("Loading authorization code token", Map.of("access_token", env.getObject("access_token")));
	}

	protected void unmapClientCredentialsToken() {
		env.unmapKey("access_token");
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication)
				.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
						sequenceOf(
								condition(CreateRandomFAPIInteractionId.class),
								condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
						));
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		if(shouldSkipSelfLinkValidation()) {
			eventLog.log("Skipping self link validation", Map.of("skip_self_link_validation", true));
			return;
		}

		String resourceResponse = "resource_endpoint_response_full";
		String consentResponse = "consent_endpoint_response_full";
		String methodString = env.getString("http_method");
		HttpMethod method;
		if(methodString != null) {
			method = HttpMethod.valueOf(methodString);
			if (method.equals(HttpMethod.DELETE)) {
				return;
			}
		}

		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
				validateLinksAndMeta(resourceResponse);
			} else {
				validateLinksAndMeta(consentResponse);
			}
		}
		if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
				CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass())) {
			validateLinksAndMeta("consent_endpoint_response_full");
		}
	}

	private boolean shouldSkipSelfLinkValidation() {
		boolean skipSelfLinkValidation = Optional.ofNullable(env.getBoolean("skip_self_link_validation")).orElse(false);
		// Make sure "skip_self_link_validation" must be set again to skip validation.
		env.putBoolean("skip_self_link_validation", false);
		return skipSelfLinkValidation;
	}

	protected void skipSelfLinkValidation() {
		env.putBoolean("skip_self_link_validation", true);
	}

	private boolean isEndpointCallSuccessful(int status) {
		if (status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}

	private void validateLinksAndMeta(String responseFull) {

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
				.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
		if (!isEndpointCallSuccessful(status)) {
			return;
		}

		if (responseFull.contains("resource") || env.getElementFromObject(responseFull, "body_json.meta") != null) {
			if (responseFull.contains("resource")) {
				env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			}

			call(condition(OpinPaginationValidator.class)
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure());

			if (responseFull.contains("resource")) {
				env.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
			}
		}

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
		if (recursion.equals("false")) {
			callAndStopOnFailure(SaveOldValues.class);
			env.putString("recursion", "true");
			env.mapKey("resource_endpoint_response_full", responseFull);

			if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
				validateLinks(responseFull);
			}

			env.unmapKey("resource_endpoint_response_full");
			callAndStopOnFailure(LoadOldValues.class);
			env.putString("recursion", "false");
		}
	}

	protected void validateLinks(String responseFull) {
		call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
		env.mapKey("resource_endpoint_response_full", responseFull);

		ConditionSequence sequence = new ValidateSelfEndpoint();

		if (responseFull.contains("consent")) {
			sequence.insertAfter(ClearContentTypeHeaderForResourceEndpointRequest.class , condition(ClearIdempotencyKeyHeader.class));
			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
		}

		call(sequence);
	}

	protected void syncWaitTime(){
		eventLog.startBlock("Waiting for Consent Sync Time");
		callAndStopOnFailure(WaitForConsentSyncTimeFromConfig.class);
		eventLog.endBlock();
		eventLog.startBlock("Refreshing Token");
		makeRefreshTokenCall();
		eventLog.endBlock();
	}

	protected void makeRefreshTokenCall() {
		unmapClientCredentialsToken();
		callAndStopOnFailure(ExtractAndAddRefreshTokenToEnv.class);
		call(new RefreshTokenRequestSteps(false,addTokenEndpointClientAuthentication,false)
				.skip(EnsureAccessTokenValuesAreDifferent.class, "No need to make this check"));
		callAndStopOnFailure(EnsureRefreshTokenNotRotated.class);
	}

}
