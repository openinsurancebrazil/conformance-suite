package net.openid.conformance.opin.testmodule.v1.patrimonial;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts.AbstractOpinPatrimonialApiTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-patrimonial-api-test",
	displayName = "Validates the structure of all Patrimonial API resources",
	summary = "Validates the structure of all Patrimonial API resources\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Patrimonial API (“DAMAGES_AND_PEOPLE_PATRIMONIAL_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Patrimonial “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET Patrimonial policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET patrimonial premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET patrimonial claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinPatrimonialApiTestModule extends AbstractOpinPatrimonialApiTestModule {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsurancePatrimonialListOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
		return GetInsurancePatrimonialPolicyInfoOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
		return GetInsurancePatrimonialPremiumOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
		return GetInsurancePatrimonialClaimOASValidatorV1n4.class;
	}
}
