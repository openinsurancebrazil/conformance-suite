package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class PrepareConfigForCustomerBusinessTest extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {
        env.putString("config","consent.productType","business");
        logSuccess("productType has been set to business",
            args("productType", OIDFJSON.getString(env.getElementFromObject("config", "consent.productType"))));
        return env;
    }
}
