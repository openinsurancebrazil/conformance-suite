package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.StringUtils;

public class PrepareToGetHomeInsuranceApi extends AbstractPrepareUrlForApi {

	private final String fullRegex = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+\\/home-insurance\\/commercializationArea\\/)(\\S+\\/?)$";
	private final String validatorRegex = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+\\/home-insurance\\/commercializationArea)$";
	private String commercializationArea;

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String baseURL = env.getString("config", "resource.resourceUrl");

		if (!baseURL.matches(fullRegex)) {
			baseURL = StringUtils.removeEnd(baseURL, "/");
			commercializationArea = env.getString("config", "resource.commercializationArea");

			if (!baseURL.matches(validatorRegex)) {
				throw error("resourceUrl is not valid, please ensure that url matches " + validatorRegex, args("resourceUrl", baseURL));
			}

			baseURL = String.format("%s/%s", baseURL, getEndpointReplacement());
		}

		log("Constructed resource URL", args("resourceUrl", baseURL));
		env.putString("protected_resource_url", baseURL);
		return env;
	}

	@Override
	protected String getApiReplacement() {
		return "products-services";
	}

	@Override
	protected String getEndpointReplacement() {
		return commercializationArea;
	}
}
