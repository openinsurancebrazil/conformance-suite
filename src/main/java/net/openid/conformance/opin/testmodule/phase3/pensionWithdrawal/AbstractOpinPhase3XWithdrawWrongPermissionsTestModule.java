package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.ApiTypes;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.OpinSetPermissionsBuilderForAllPhase2Permissions;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinPhase3XWithdrawWrongPermissionsTestModule extends AbstractOpinFunctionalTestModule {

    public static String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

    @Override
    protected void configureClient() {
        super.configureClient();
        callAndStopOnFailure(getScopeCondition());
    }

    protected abstract Class<? extends AbstractScopeAddingCondition> getScopeCondition();

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        env.putString("api", getApi());
        env.putString("api_type", API_TYPE);
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
        permissionsBuilder.buildFromEnv();
    }

    protected abstract String getApi();

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertAfter(SetContentTypeApplicationJson.class,
                        sequenceOf(
                                condition(CreateRandomFAPIInteractionId.class),
                                condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
                );
    }

    @Override
    protected void requestProtectedResource() {
        useAuthorizationCodeToken();
        executeTest();
    }

    protected void executeTest() {
        fetchConsent("AUTHORIZED", EnsurePaymentConsentStatusWasAuthorised.class);
        postWithdrawal("403");
        validateWithdrawalResponse();
        fetchConsent("AUTHORIZED", EnsurePaymentConsentStatusWasAuthorised.class);
    }

    protected void postWithdrawal(String status) {
        eventLog.startBlock(String.format("POST Pension Withdrawal - Expecting %s", status));
        callAndStopOnFailure(prepareUrl());
        callAndStopOnFailure(createWithdrawalPostRequestBody());
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        setIdempotencyKey();
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(AddJsonAcceptHeaderRequest.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
    }

    protected abstract Class<? extends Condition> prepareUrl();
    protected abstract Class<? extends Condition> createWithdrawalPostRequestBody();


    protected void setIdempotencyKey() {
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
    }

    protected void validateWithdrawalResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

    protected abstract Class<? extends Condition> validator();


    protected void fetchConsent(String status, Class<? extends Condition> statusCheckingCondition) {
        runInBlock(String.format("Checking the created consent - Expecting %s status", status), () -> {
            useClientCredentialsToken();
            callAndStopOnFailure(PaymentConsentIdExtractor.class);
            callAndStopOnFailure(AddJWTAcceptHeader.class);
            callAndStopOnFailure(ExpectJWTResponse.class);
            callAndStopOnFailure(PrepareToFetchConsentRequest.class);
            callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(statusCheckingCondition, Condition.ConditionResult.FAILURE);
            unmapClientCredentialsToken();
        });
    }

    @Override
    protected void validateResponse() {
        //not needed here
    }

}
