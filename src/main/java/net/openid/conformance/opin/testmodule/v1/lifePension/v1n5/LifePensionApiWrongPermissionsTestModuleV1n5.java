package net.openid.conformance.opin.testmodule.v1.lifePension.v1n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n5.*;
import net.openid.conformance.opin.testmodule.v1.lifePension.AbstractLifePensionApiWrongPermissionsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "life-pension_api_wrong-permissions_test-module_v1n5",
        displayName = "Ensures API  cannot be called with wrong permissions",
        summary = "Ensures API  cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“LIFE_PENSION_READ”, “LIFE_PENSION_CONTRACTINFO_READ”, “LIFE_PENSION_MOVEMENTS_READ”, “LIFE_PENSION_PORTABILITIES_READ”,“LIFE_PENSION_WITHDRAWALS_READ”,“LIFE_PENSION_CLAIM”,“RESOURCES_READ”)\n" +
                "• Expects 201 -  Validate Response \n" +
                "• Redirect the user to authorize consent \n" +
                "• Calls GET Life Pension contracts Endpoint \n" +
                "• Expects 200 - Fetches one of the certificate IDs returned \n" +
                "• Calls GET Life Pension {certificateId} contract-info Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Life Pension {certificateId} movements Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Life Pension {certificateId} portabilities Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Life Pension {certificateId} withdrawals Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Life Pension {certificateId} claim Endpoint \n" +
                "• Expects 200 - Validate Response\n" +
                "• Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field \n" +
                "• Expects a success 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Life Pension contracts Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} contract-info Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} movements Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} portabilities Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} withdrawals Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} claim Endpoint \n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class LifePensionApiWrongPermissionsTestModuleV1n5 extends AbstractLifePensionApiWrongPermissionsTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceLifePensionContractsOASValidatorV1n5.class;
    }
    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator() {
        return GetInsuranceLifePensionContractInfoOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getMovementsValidator() {
        return GetInsuranceLifePensionMovementsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator() {
        return GetInsuranceLifePensionPortabilitiesOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator() {
        return GetInsuranceLifePensionWithdrawalsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsuranceLifePensionClaimOASValidatorV1n5.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }
}
