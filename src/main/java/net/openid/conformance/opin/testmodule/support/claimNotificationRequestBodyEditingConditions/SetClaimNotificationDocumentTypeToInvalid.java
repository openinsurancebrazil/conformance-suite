package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;

import java.util.Optional;
import java.util.Set;

public class SetClaimNotificationDocumentTypeToInvalid extends AbstractCondition {

    private final Set<String> DOCUMENT_TYPE_GROUP_CERTIFICATE_ID = SetUtils.createSet("CERTIFICADO, CERTIFICADO_AUTOMOVEL");
    private final Set<String> DOCUMENT_TYPE_POLICY_NUMBER = SetUtils.createSet("APOLICE_INDIVIDUAL, BILHETE, APOLICE_INDIVIDUAL_AUTOMOVEL, APOLICE_FROTA_AUTOMOVEL");

    @Override
    @PreEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        String documentType = OIDFJSON.getString(Optional.ofNullable(
            data.get("documentType"))
            .orElseThrow(() -> error("documentType field not present in response body")));

        Set<String> possibleDocumentTypes;
        if (DOCUMENT_TYPE_GROUP_CERTIFICATE_ID.contains(documentType)) {
            possibleDocumentTypes = DOCUMENT_TYPE_GROUP_CERTIFICATE_ID;
        } else if (DOCUMENT_TYPE_POLICY_NUMBER.contains(documentType)) {
            possibleDocumentTypes = DOCUMENT_TYPE_POLICY_NUMBER;
        } else {
            throw error("Invalid documentType.", args("documentType", documentType));
        }
        possibleDocumentTypes.remove(documentType);
        data.addProperty("documentType", possibleDocumentTypes.stream().findAny().get());

        logSuccess("documentType changed to an invalid value", args("data", data));
        return env;
    }
}
