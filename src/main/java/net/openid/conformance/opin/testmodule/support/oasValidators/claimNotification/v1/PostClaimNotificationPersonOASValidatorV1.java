package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.AbstractPostClaimNotificationPersonOASValidator;

@ApiName("Post Claim Notification Person V1")
public class PostClaimNotificationPersonOASValidatorV1 extends AbstractPostClaimNotificationPersonOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/claimNotification/swagger-claim-notification-1.3.0.yaml";
    }
}
