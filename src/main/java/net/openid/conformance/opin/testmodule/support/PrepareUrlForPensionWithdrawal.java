package net.openid.conformance.opin.testmodule.support;

public class PrepareUrlForPensionWithdrawal extends AbstractPrepareUrlForApi {

    @Override
    protected String getApiReplacement() {
        return "withdrawal";
    }

    @Override
    protected String getEndpointReplacement() {
        return "pension/request";
    }
}
