package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ContractsDataSelector extends AbstractCondition {

    @Override
    public Environment evaluate(Environment env) {
        JsonObject body = bodyFrom(env).getAsJsonObject();
        JsonArray data = Optional.ofNullable(body.getAsJsonArray("data"))
                .orElseThrow(() -> error("Could not find data in the body", args("body", body)));

        if (data.isEmpty()) {
            throw error("Data array cannot be empty to extract certificate ID");
        }

        JsonArray companies = Optional.ofNullable(data.get(0).getAsJsonObject().getAsJsonObject("brand"))
                .map(brand ->  brand.getAsJsonArray("companies"))
                .orElseThrow(() -> error("Could not extract brand.companies array from the data", args("data", data)));

        if (companies.isEmpty()) {
            throw error("Companies array cannot be empty to extract certificate ID");
        }

        JsonArray contracts = Optional.ofNullable(companies.get(0).getAsJsonObject().getAsJsonArray("contracts"))
                .orElseThrow(() -> error("Could not extract contracts array from the data", args("data", data)));

        if (contracts.isEmpty()) {
            throw error("contracts array cannot be empty to extract certificate ID");
        }

        JsonObject contract1 = contracts.get(0).getAsJsonObject();
        env.putObject("contract_1",contract1);

        env.putString("certificateId",Optional.ofNullable(contract1.get("certificateId"))
                .map(OIDFJSON::getString)
                .orElseThrow(() -> error("could not find certificate ID in the contract object", args("contract", contract1))));
        env.putString("selected_id",env.getString("certificateId"));

        if(contracts.size()>1){
            JsonObject contract2 = contracts.get(1).getAsJsonObject();
            env.putObject("contract_2",contract2);

            env.putString("certificateId2",Optional.ofNullable(contract2.get("certificateId"))
                    .map(OIDFJSON::getString)
                    .orElseThrow(() -> error("could not find certificate ID in the 2nd contract object", args("contract", contract2))));
            log("Successfully found both elements.", args("certificate ID 1", env.getString("certificateId"),"certificate ID 2", env.getString("certificateId2")));

            return env;
        }
        log("Contract array second element was not found. Proceeding with 1st element only", args("certificate ID", env.getString("certificateId")));

        return env;
    }

        private JsonElement bodyFrom(Environment env) {
            try {
                return BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
                        .orElseThrow(() -> error("Could not find response body in the environment"));
            } catch (ParseException e) {
                throw error("Could not parse response body");
            }
        }

    }
