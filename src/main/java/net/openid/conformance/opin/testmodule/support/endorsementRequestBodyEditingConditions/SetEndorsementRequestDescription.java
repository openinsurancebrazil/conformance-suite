package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.OPINPhase3Keys;
import net.openid.conformance.testmodule.Environment;

public class SetEndorsementRequestDescription extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        data.addProperty("requestDescription", OPINPhase3Keys.Keys_diffRequestDescription);
        logSuccess("requestDescription was changed", args("data", data));
        return env;
    }
}
