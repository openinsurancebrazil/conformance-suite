package net.openid.conformance.opin.testmodule.phase1.branches;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.dataChannels.v1n5.GetBranchesOASValidatorV1n5;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - Channels - Branches API test",
	displayName = "Validate structure of Channels - Branches API Api resources",
	summary = "Validate structure of Channels - Branches Api resources",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1)
public class BranchesApiTestModule extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate Channels - Branches response", () -> {
			callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetBranchesOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
		});
	}
}
