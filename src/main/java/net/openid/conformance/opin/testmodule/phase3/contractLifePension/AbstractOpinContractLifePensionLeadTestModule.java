package net.openid.conformance.opin.testmodule.phase3.contractLifePension;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;


public abstract class AbstractOpinContractLifePensionLeadTestModule extends AbstractOpinPhase3QuoteLeadTestModule {
    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.CONTRACT_LIFE_PENSION_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "contract-life-pension";
    }

    @Override
    protected String getApiName() {
        return "contract-life-pension";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}