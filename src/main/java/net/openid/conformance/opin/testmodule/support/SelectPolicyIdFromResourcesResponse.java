package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class SelectPolicyIdFromResourcesResponse extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_endpoint_response_full")
    @PostEnvironment(strings = "policyId")
    public Environment evaluate(Environment env) {
        JsonObject response = env.getObject("resource_endpoint_response_full");
        JsonObject body = JsonParser.parseString(OIDFJSON.getString(response.get("body"))).getAsJsonObject();
        JsonArray dataArray = body.getAsJsonArray("data");

        for (JsonElement element : dataArray) {
            JsonObject resource = element.getAsJsonObject();
            String type = OIDFJSON.getString(resource.get("type"));
            if (!type.startsWith("CUSTOMERS")) {
                String policyId = OIDFJSON.getString(resource.get("resourceId"));
                env.putString("policyId", policyId);
                logSuccess("policyID extracted from resources API response", args("policyId", policyId));
                return env;
            }
        }
        throw error("No valid policyId found in resources API response");
    }
}


