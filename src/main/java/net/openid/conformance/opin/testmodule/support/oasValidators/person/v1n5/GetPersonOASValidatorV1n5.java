package net.openid.conformance.opin.testmodule.support.oasValidators.person.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Person 1.5.1")
public class GetPersonOASValidatorV1n5 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/person/v1/v1n5/swagger-person-1.5.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/person";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}