package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AddDamageEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationDocumentTypeToANonExistent;

public abstract class AbstractOpinClaimNotificationApiDamagesInvalidDocumentTypeTestModule extends AbstractClaimNotificationsNegativeTest{

    @Override
    protected void editClaimNotificationRequestBody() {
        callAndStopOnFailure(SetClaimNotificationDocumentTypeToANonExistent.class);
    }

    @Override
    protected AbstractAddEndpointTypeToEnvironment addEndpointType() {
        return new AddDamageEndpointTypeToEnvironment();
    }
}
