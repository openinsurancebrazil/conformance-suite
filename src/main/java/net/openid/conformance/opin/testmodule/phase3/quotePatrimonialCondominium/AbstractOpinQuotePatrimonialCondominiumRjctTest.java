package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvdOrRjct;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRjct;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePostOnlyWithIdentificationRequestBody;

public abstract class AbstractOpinQuotePatrimonialCondominiumRjctTest extends AbstractOpinQuotePatrimonialCondominiumCoreTest {

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePostOnlyWithIdentificationRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvdOrRjct.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasRjct.class;
    }

    // The quote won't be patched during this test.
    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return null;
    }
    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return null;
    }
    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return null;
    }
}
