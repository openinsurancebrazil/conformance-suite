package net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitle.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Capitalization 1.4.0")
public class GetCapitalizationTitleOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/capitalizationTitle/v1n4/swagger-capitalization-title-1.4.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/capitalization-title";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}