package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1.PostClaimNotificationPersonOASValidatorV1;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "opin-claim-notification-api-person-multiple-claim-notifications-test",
    displayName = "Ensure that Claim notification person cannot be created multiple times with a unique consent.",
    summary = "Ensure that Claim notification person cannot be created multiple times with a unique consent\n" +
        "\u2022 Call the POST Consents API with damages and people person permissions\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET resources API\n" +
        "\u2022 Expects 200 - Validate Response\n" +
        "\u2022 Call the GET Shared API policy-info endpoint with the extracted resourceId, where the API to be called is to be determined by the Type of the extracted policyID\n" +
        "\u2022 Expect a 200 - Extract the field data.documentType from the response_body\n" +
        "\u2022 Call the POST Consents Endpoint with Claim Notification permissions\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "\u2022 Call the POST Claim Notification at the person endpoint\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"CONSUMED\"\n" +
        "\u2022 Call the POST Claim Notification with a different payload and a different idempotency key\n" +
        "\u2022 Expects 401 - Validate Error Message",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "resource.consentUrl",
        "resource.brazilCpf",
        "consent.productType"
    }
)
public class OpinClaimNotificationApiPersonMultipleClaimNotificationsTestModule extends AbstractOpinClaimNotificationApiPersonMultipleClaimNotificationsTestModule {

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostClaimNotificationPersonOASValidatorV1();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }
}
