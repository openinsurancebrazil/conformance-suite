package net.openid.conformance.opin.testmodule.support.oasValidators.exportCredit.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Export Credit 1.3.0")
public class GetExportCreditOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/exportCredit/v1n3/swagger-export-credit-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/export-credit";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}