package net.openid.conformance.opin.testmodule.v1.financialAssistance.v1n3;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n3.GetInsuranceFinancialAssistanceContractsOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.financialAssistance.AbstractFinancialAssistanceApiResourcesTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "financial-assistance_api_resources_test-module_v1n3",
        displayName = "Makes sure that the Resource API and the Financial Assistance API are returning the same available IDs",
        summary = "Makes sure that the Resource API and the Financial Assistance API are returning the same available IDs\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“FINANCIAL_ASSISTANCE_READ”, “FINANCIAL_ASSISTANCE_CONTRACTINFO_READ”, “FINANCIAL_ASSISTANCE_MOVEMENTS_READ”, “RESOURCES_READ”)\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Financial Assistance contracts Endpoint\n" +
                "• Validate response of the response and make sure that an id is returned - Fetch the contract ID provided by this API\n" +
                "• Call the resources API\n" +
                "• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class FinancialAssistanceApiResourcesTestModuleV1n3 extends AbstractFinancialAssistanceApiResourcesTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceFinancialAssistanceContractsOASValidatorV1n3.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }

}
