package net.openid.conformance.opin.testmodule.v1.endorsement;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForEndorsement;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;

public abstract class AbstractEndorsementHappyPathTest extends AbstractOpinEndorsementTestModule {

    protected abstract AbstractJsonAssertingCondition validator();

    @Override
    protected void executeTest() {
        postEndorsement();
        validateEndorsementResponse();
        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
    }

    protected abstract AbstractCreateEndorsementRequestBody createEndorsementRequestBody();

    protected void postEndorsement() {
        eventLog.startBlock("POST endorsement");
        callAndStopOnFailure(PrepareUrlForEndorsement.class);
        callAndStopOnFailure(createEndorsementRequestBody().getClass());
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        setIdempotencyKey();
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(AddJsonAcceptHeaderRequest.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
    }

    protected void validateEndorsementResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

    protected void setIdempotencyKey() {
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
    }
}
