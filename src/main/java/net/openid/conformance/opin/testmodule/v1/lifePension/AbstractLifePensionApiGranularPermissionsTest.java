package net.openid.conformance.opin.testmodule.v1.lifePension;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;

public abstract class AbstractLifePensionApiGranularPermissionsTest extends AbstractOpinApiTestModuleV2 {

    protected abstract Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getMovementsValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getClaimValidator();

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddLifePensionScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceLifePensionUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.LIFE_PENSION;
    }

    @Override
    protected String[] getIndividualPermissionsToBeRemoved() {
        return new String[]{"LIFE_PENSION_MOVEMENTS_READ", "LIFE_PENSION_PORTABILITIES_READ", "LIFE_PENSION_WITHDRAWALS_READ", "LIFE_PENSION_CLAIM"};
    }

    @Override
    protected String getApi() {
        return "insurance-life-pension";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].contracts[0].certificateId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "contract-info", getContractInfoValidator(),
                "movements", getMovementsValidator(),
                "portabilities", getPortabilitiesValidator(),
                "withdrawals", getWithdrawalsValidator(),
                "claim", getClaimValidator()
        );
    }

    @Override
    protected Class<? extends Condition> getExpectedResponseCode(String endpoint) {
        Map<String, Class<? extends Condition>> endpointToStatusCodeMap = Map.of(
                "contract-info", EnsureResourceResponseCodeWas200.class,
                "movements", EnsureResourceResponseCodeWas403.class,
                "portabilities", EnsureResourceResponseCodeWas403.class,
                "withdrawals", EnsureResourceResponseCodeWas403.class,
                "claim", EnsureResourceResponseCodeWas403.class
        );
        return endpointToStatusCodeMap.get(endpoint);
    }
}
