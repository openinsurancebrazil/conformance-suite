package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteRuralLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "GRANIZO";
    }
}
