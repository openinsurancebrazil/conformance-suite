package net.openid.conformance.opin.testmodule.support;

public class PrepareUrlForCapitalizationTitleWithdrawal extends AbstractPrepareUrlForApi {

    @Override
    protected String getApiReplacement() {
        return "withdrawal";
    }

    @Override
    protected String getEndpointReplacement() {
        return "capitalization-title/request";
    }
}
