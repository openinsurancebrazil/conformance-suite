package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.List;

public abstract class AbstractEnsureStatusWasX extends AbstractCondition {
    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";


    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    public Environment evaluate(Environment env) {
        JsonObject responseBody;
        try {
            responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }

        JsonElement data = responseBody.get("data");
        if(data == null) {
            throw error("The response body doesn't contain data");
        }

        String status = OIDFJSON.getString(data.getAsJsonObject().get("status"));
        if(getStatuses().contains(status)) {
            informSuccess(env);
        } else {
            informFailure(env);
        }

        return env;
    }

    private void informSuccess(Environment env) {
        String keyToInformResult = getKeyToInformResult();
        if(!StringUtils.isBlank(keyToInformResult)) {
            env.putBoolean(keyToInformResult, true);
        }

        logSuccess("The status is as expected", args(
                "expected", getStatuses()
        ));
    }

    private void informFailure(Environment env) {
        String keyToInformResult = getKeyToInformResult();
        if(!StringUtils.isBlank(keyToInformResult)) {
            env.putBoolean(keyToInformResult, false);
            log("The status was not as expected", args(
                    "expected", getStatuses()
            ));
            return;
        }

        throw error("The status was not as expected", args(
                "expected", getStatuses()
        ));
    }

    protected abstract List<String> getStatuses();

    protected String getKeyToInformResult() {
        return null;
    }
}
