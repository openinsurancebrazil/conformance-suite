package net.openid.conformance.opin.testmodule.v1.lifePension;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;

public abstract class AbstractLifePensionAPICoreTest extends AbstractOpinApiTestModuleV2 {

    protected abstract Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getMovementsValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getClaimValidator();


    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddLifePensionScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceLifePensionUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.LIFE_PENSION;
    }

    @Override
    protected String getApi() {
        return "insurance-life-pension";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].contracts[0].certificateId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "contract-info", getContractInfoValidator(),
                "movements", getMovementsValidator(),
                "portabilities", getPortabilitiesValidator(),
                "withdrawals", getWithdrawalsValidator(),
                "claim", getClaimValidator()
        );
    }
}
