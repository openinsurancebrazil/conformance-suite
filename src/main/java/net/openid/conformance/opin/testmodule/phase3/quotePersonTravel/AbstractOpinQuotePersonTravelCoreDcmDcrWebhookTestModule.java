package net.openid.conformance.opin.testmodule.phase3.quotePersonTravel;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteDynamicClientWebhookTestModule;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAckn;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAcpt;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchAcknRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePersonTravelPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePersonTravelCoreDcmDcrWebhookTestModule extends AbstractOpinPhase3QuoteDynamicClientWebhookTestModule {

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PERSON_TRAVEL;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-person-travel";
    }

    @Override
    protected String getApiName() {
        return "quote-person";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "travel/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePersonTravelPostRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchAcknRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasAckn.class;
    }
}