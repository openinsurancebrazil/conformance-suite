package net.openid.conformance.opin.testmodule.v1.customers;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractOpinCustomerPersonalDataApiTest extends AbstractOpinFunctionalTestModule {

	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalIdentificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalQualificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalComplimentaryInfoValidator();

	@Override
	protected void configureClient(){
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(PrepareConfigForCustomerPersonalTest.class);
		callAndStopOnFailure(BuildOpinPersonalCustomersConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {

		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL);
		permissionsBuilder.build();

		callAndStopOnFailure(AddScopesForCustomerApi.class);
		callAndContinueOnFailure(PrepareToGetPersonalIdentifications.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
	}

	@Override
	protected void requestProtectedResource() {
		validateResponse();
	}

	@Override
	protected void validateResponse() {
		runInBlock("Validating personal identifications response", () -> {
			callAndStopOnFailure(PrepareToGetPersonalIdentifications.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getCustomerPersonalIdentificationValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Validating personal qualifications response", () -> {
			callAndStopOnFailure(PrepareToGetPersonalQualifications.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getCustomerPersonalQualificationValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Validating personal complimentary-information response", () -> {
			callAndStopOnFailure(PrepareToGetPersonalComplimentaryInformation.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getCustomerPersonalComplimentaryInfoValidator(), Condition.ConditionResult.FAILURE);
		});

	}
}
