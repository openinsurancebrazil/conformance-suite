package net.openid.conformance.opin.testmodule.support;

public class BuildInsuranceFinancialAssistanceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {
    @Override
    protected String getApiReplacement() {
        return "insurance-financial-assistance";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-financial-assistance/contracts";
    }
}
