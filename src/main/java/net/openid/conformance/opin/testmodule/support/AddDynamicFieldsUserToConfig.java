package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractAddVariantUserToConfig;

public class AddDynamicFieldsUserToConfig extends AbstractAddVariantUserToConfig {
    @Override
    protected String getVariantUserKey() {
        return "brazilCpfDynamicFields";
    }

    @Override
    protected String getVariantBusinessKey() {
        return "brazilCnpjDynamicFields";
    }
}
