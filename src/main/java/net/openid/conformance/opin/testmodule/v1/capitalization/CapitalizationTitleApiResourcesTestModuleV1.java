package net.openid.conformance.opin.testmodule.v1.capitalization;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitlePlansOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "capitalization-title_api_resources_test-module_v1",
        displayName = "Makes sure that the Resource API and the Capitalization Title API are returning the same available IDs",
        summary = "Makes sure that the Resource API and the Capitalization Title API are returning the same available IDs.\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API  (“CAPITALIZATION_TITLE_READ”, “CAPITALIZATION_TITLE_PLANINFO_READ”, “CAPITALIZATION_TITLE_EVENTS_READ”, “CAPITALIZATION_TITLE_SETTLEMENTS_READ”, “RESOURCES_READ”)\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Capitalization Title Plans Endpoint\n" +
                "• Validate response of the response and make sure that an id is returned - Fetch the plan id provided by this API\n" +
                "• Call the resources API\n" +
                "• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class CapitalizationTitleApiResourcesTestModuleV1 extends AbstractCapitalizationTitleApiResourcesTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceCapitalizationTitlePlansOASValidatorV1n4.class;
    }
}
