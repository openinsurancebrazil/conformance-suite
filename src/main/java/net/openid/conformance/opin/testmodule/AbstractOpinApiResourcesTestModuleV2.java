package net.openid.conformance.opin.testmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus;
import net.openid.conformance.opin.testmodule.support.AddResourcesScope;
import net.openid.conformance.opin.testmodule.support.BuildOpinResourcesConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.CompareResourceIdsWithSelectedIds;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForOpinResourcesRoot;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;

import java.util.Collections;
import java.util.Map;

public abstract class AbstractOpinApiResourcesTestModuleV2 extends AbstractOpinApiTestModuleV2 {

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        super.onConfigure(config, baseUrl);
        callAndStopOnFailure(AddResourcesScope.class);
    }

    @Override
    protected void validateResponse() {
        super.validateResponse();

        callAndStopOnFailure(BuildOpinResourcesConfigResourceUrlFromConsentUrl.class);
        callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
        preCallProtectedResource("Call Resources API");

        runInBlock("Validate Resources response", () -> {
            callAndStopOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);
        });

        eventLog.startBlock("Compare active resourceId's with API resources");
        env.putString("resource_type", getResourceType());
        env.putString("resource_status", getResourceStatus());
        callAndStopOnFailure(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.class);
        callAndContinueOnFailure(CompareResourceIdsWithSelectedIds.class, Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Collections.emptyMap();
    }

    protected abstract String getResourceType();
    protected abstract String getResourceStatus();
}
