package net.openid.conformance.opin.testmodule.v1.financialRisk;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AllIdsSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskListOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-financial-risk-resources-api-test",
	displayName = "Makes sure that the Resource API and the financial risk API are returning the same available IDs",
	summary = "Makes sure that the Resource API and the financial risk API are returning the same available IDs\n" +
		"\u2022 Creates a consent with all the permissions needed to access the financial risk API (“DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects server to create the consent with 201\n" +
		"\u2022 Redirect the user to authorize at the financial institution\n" +
		"\u2022 Call the financial risk “/” API\n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that a policy id is returned - Fetch the policy id provided by this API\n" +
		"\u2022 Call the resources API \n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the policy id provided by this API",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
public class OpinFinancialRiskResourcesApiTestModule extends AbstractOpinFinancialRiskResourcesApiTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getRootValidator() {
		return GetInsuranceFinancialRiskListOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*]");
		return AllIdsSelectorFromJsonPath.class;
	}
}
