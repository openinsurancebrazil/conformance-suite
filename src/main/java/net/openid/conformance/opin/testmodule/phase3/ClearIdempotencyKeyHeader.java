package net.openid.conformance.opin.testmodule.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ClearIdempotencyKeyHeader extends AbstractCondition {

    @PreEnvironment(
            required = {"resource_endpoint_request_headers"}
    )
    @PostEnvironment(
            required = {"resource_endpoint_request_headers"}
    )
   public Environment evaluate(Environment env) {
        JsonObject headers = env.getObject("resource_endpoint_request_headers");
        headers.remove("x-idempotency-key");
        this.logSuccess("Created Empty x-idempotency-key");
        return env;
    }
}
