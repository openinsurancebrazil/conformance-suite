package net.openid.conformance.opin.testmodule.support.validateField;

import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.AbstractValidateField;

public class ValidateConsentsFieldV2 extends AbstractValidateField {

    @Override
    protected int getConsentVersion() {
        return 2;
    }
}
