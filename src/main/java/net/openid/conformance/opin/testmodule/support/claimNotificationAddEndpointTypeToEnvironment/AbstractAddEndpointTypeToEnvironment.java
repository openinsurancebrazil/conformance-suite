package net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractAddEndpointTypeToEnvironment extends AbstractCondition {

    @Override
    @PostEnvironment(strings = "claim_notification_endpoint_type")
    public Environment evaluate(Environment env) {
        env.putString("claim_notification_endpoint_type", endpointType());
        return env;
    }

    protected abstract String endpointType();
}
