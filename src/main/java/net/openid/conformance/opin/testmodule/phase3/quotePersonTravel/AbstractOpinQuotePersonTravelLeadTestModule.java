package net.openid.conformance.opin.testmodule.phase3.quotePersonTravel;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePersonTravelLeadTestModule extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PERSON_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-person-travel";
    }

    @Override
    protected String getApiName() {
        return "quote-person";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}
