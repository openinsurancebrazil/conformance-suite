package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.*;

public class CheckPhase2PermissionGroupsToBeTested extends AbstractCondition {

    @Override
    @PreEnvironment(required = {"authorisation_server", "config"})
    @PostEnvironment(required = "permission_groups")
    public Environment evaluate(Environment env) {
        String productType = Optional.ofNullable(env.getElementFromObject("config", "consent"))
                .map(OIDFJSON::toObject)
                .map(consent -> consent.get("productType"))
                .map(OIDFJSON::getString)
                .orElseThrow(() -> error("The consent.productType has not been set in the config"));
        JsonObject authServer = env.getObject("authorisation_server");
        JsonArray apiResources = Optional.ofNullable(authServer.getAsJsonArray("ApiResources"))
                .orElseThrow(() -> error("There is no \"ApiResources\" inside authorisation server response"));

        List<String> apiFamilyTypeList = getApiFamilyTypeList(apiResources);
        Map<String, String> familyTypeToPermissionGroupMap = getFamilyTypeToPermissionGroupMap(productType);
        JsonArray permissionGroups = new JsonArray();

        for (String apiFamilyType : apiFamilyTypeList) {
            if (familyTypeToPermissionGroupMap.containsKey(apiFamilyType)) {
                permissionGroups.add(familyTypeToPermissionGroupMap.get(apiFamilyType));
            }
        }

        JsonObject permissionGroupsObj = new JsonObjectBuilder().addField("permissionGroups", permissionGroups).build();
        env.putObject("permission_groups", permissionGroupsObj);
        logSuccess("Checked every phase 2 permission groups that will be tested", args("permission groups", permissionGroups));
        return env;
    }

    private List<String> getApiFamilyTypeList(JsonArray apiResources) {
        List<String> apiFamilyTypeList = new ArrayList<>();
        for (JsonElement resourceElement : apiResources) {
            String apiFamilyType = Optional.ofNullable(resourceElement.getAsJsonObject())
                    .map(resource -> resource.get("ApiFamilyType"))
                    .map(OIDFJSON::getString)
                    .orElseThrow(() -> error("There is no \"ApiFamilyType\" inside api resource"));
            apiFamilyTypeList.add(apiFamilyType);
        }
        return apiFamilyTypeList;
    }

    private Map<String, String> getFamilyTypeToPermissionGroupMap(String productType) {
        Map<String, String> familyTypeToPermissionGroup = new HashMap<>();
        familyTypeToPermissionGroup.put("acceptance-and-branches-abroad", PermissionsGroup.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD.name());
        familyTypeToPermissionGroup.put("auto", PermissionsGroup.DAMAGES_AND_PEOPLE_AUTO.name());
        if (productType.equals("personal")) {
            familyTypeToPermissionGroup.put("customers-personal", PermissionsGroup.CUSTOMERS_PERSONAL.name());
        } else if (productType.equals("business")) {
            familyTypeToPermissionGroup.put("customers-business", PermissionsGroup.CUSTOMERS_BUSINESS.name());
        }
        familyTypeToPermissionGroup.put("insurance-capitalization-title", PermissionsGroup.CAPITALIZATION_TITLE.name());
        familyTypeToPermissionGroup.put("financial-risk", PermissionsGroup.DAMAGES_AND_PEOPLE_FINANCIAL_RISKS.name());
        familyTypeToPermissionGroup.put("insurance-financial-assistance", PermissionsGroup.FINANCIAL_ASSISTANCE.name());
        familyTypeToPermissionGroup.put("housing", PermissionsGroup.DAMAGES_AND_PEOPLE_HOUSING.name());
        familyTypeToPermissionGroup.put("insurance-life-pension", PermissionsGroup.LIFE_PENSION.name());
        familyTypeToPermissionGroup.put("patrimonial", PermissionsGroup.DAMAGES_AND_PEOPLE_PATRIMONIAL.name());
        familyTypeToPermissionGroup.put("insurance-pension-plan", PermissionsGroup.PENSION_PLAN.name());
        familyTypeToPermissionGroup.put("insurance-person", PermissionsGroup.DAMAGES_AND_PEOPLE_PERSON.name());
        familyTypeToPermissionGroup.put("responsibility", PermissionsGroup.DAMAGES_AND_PEOPLE_RESPONSIBILITY.name());
        familyTypeToPermissionGroup.put("rural", PermissionsGroup.DAMAGES_AND_PEOPLE_RURAL.name());
        familyTypeToPermissionGroup.put("transport", PermissionsGroup.DAMAGES_AND_PEOPLE_TRANSPORT.name());
        return familyTypeToPermissionGroup;
    }
}
