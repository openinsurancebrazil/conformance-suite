package net.openid.conformance.opin.testmodule.support.oasValidators.prdoctsNServices.generalLiability.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api Source: swagger/openinsurance/productsServices/swagger-general-liability-1.3.0.yaml
 * Api endpoint: /general-liability
 * Api version: 1.3.0
 */
@ApiName("ProductsServices General Liability")
public class GetProductsNServicesGeneralLiabilityOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/swagger-general-liability-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/general-liability";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

}