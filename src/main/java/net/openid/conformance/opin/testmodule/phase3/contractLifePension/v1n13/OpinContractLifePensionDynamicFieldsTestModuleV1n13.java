package net.openid.conformance.opin.testmodule.phase3.contractLifePension.v1n13;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.contractLifePension.AbstractOpinContractLifePensionDynamicFieldsTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.GetContractLifePensionQuoteStatusOASValidatorV1n13;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.PatchContractLifePensionOASValidatorV1n13;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.PostContractLifePensionOASValidatorV1n13;
import net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.damageAndPerson.v1n4.GetDynamicFieldsDamageAndPersonOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-contract-life-pension_api_conditional-dynamic-fields_test-module_v1.13.0",
        displayName = "Ensure that a Life Pension Contract request can be successfully created and accepted afterwards using dynamic field.",
        summary = """
                Ensure that a Life Pension Contract request can be successfully created and accepted afterwards using dynamic field.\s
                 This test is conditional and will only be executed if the fields "Conditional - CPF for Dynamic fields" or "Conditional - CNPJ for Dynamic Fields" are filled. This test applies to both personal and Business products, depending on the config.
                • Call GET /damage-and-travel Dynamic Fields endpoint\s
                • Expect 200 - Validate response and validade if api is CONTRACT_LIFE_PENSION
                • Call POST /request endpoint sending personal or business information, following what is defined at the config, and the dynamicFields at quoteCustomData
                • Expect 201 - Validate Response and ensure status is RCVD
                • Poll the GET request/{consentId}/quote-status  endpoint while status is RCVD or EVAL for 1 minute, and fai the test if the quote status is not conclusive
                • Call GET request/{consentId}/quote-status endpoint
                • Expect 200 - Validate Respones and ensure status is ACPT and that quoteCustomData is sent back
                • Call PATCH request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
                • Expect 200 - Validate Response and ensure status is ACKN
                • Call GET links.redirect endpoint
                • Expect 200
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "resource.brazilCpfDynamicFields",
                "resource.brazilCnpjDynamicFields",
                "consent.productType"
        }
)
public class OpinContractLifePensionDynamicFieldsTestModuleV1n13 extends AbstractOpinContractLifePensionDynamicFieldsTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostContractLifePensionOASValidatorV1n13.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetContractLifePensionQuoteStatusOASValidatorV1n13.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchContractLifePensionOASValidatorV1n13.class;
    }

    @Override
    protected void getDynamicFieldsValidator() {
        callAndContinueOnFailure(GetDynamicFieldsDamageAndPersonOASValidatorV1n4.class, Condition.ConditionResult.FAILURE);
    }
}