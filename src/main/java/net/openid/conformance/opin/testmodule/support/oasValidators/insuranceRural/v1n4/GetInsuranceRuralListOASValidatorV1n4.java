package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Rural 1.4.0")
public class GetInsuranceRuralListOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/rural/v1n4/swagger-insurance-rural-api-1.4.0.yaml";
	}
	@Override
	protected String getEndpointPath() {
		return "/insurance-rural";
	}
	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
