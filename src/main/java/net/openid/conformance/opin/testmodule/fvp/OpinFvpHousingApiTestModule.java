package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddHousingScope;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.directory.GetHousingV1Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingPremiumOASValidatorV1n3;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.util.Map;

@PublishTestModule(
        testName = "opin-fvp-housing-api-test-module",
        displayName = "Validates the structure of all housing API resources",
        summary = "Validates the structure of all housing API resources\n" +
                "• Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_HOUSING_READ”, “DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "• Calls GET housing “/” API\n" +
                "• Expects 200 - Fetches one of the Policy IDs returned\n" +
                "• Calls GET housing policy-Info API \n" +
                "• Expects 200 - Validate all the fields\n" +
                "• Calls GET housing premium API \n" +
                "• Expects 200- Validate all the fields\n" +
                "• Calls GET housing claim API \n" +
                "• Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.consentSyncTime",
                "resource.brazilCpf",
                "consent.productType"
        }

)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})

public class OpinFvpHousingApiTestModule extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetHousingV1Endpoint.class));
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddHousingScope.class;
    }


    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_HOUSING;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceHousingListOASValidatorV1n3.class;
    }

    @Override
    protected String getApi() {
        return "insurance-housing";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", GetInsuranceHousingPolicyInfoOASValidatorV1n3.class,
                "premium", GetInsuranceHousingPremiumOASValidatorV1n3.class,
                "claim", GetInsuranceHousingClaimOASValidatorV1n3.class
        );
    }
}