package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetInsurancePatrimonialPolicyInfoOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/insurance-patrimonial/{policyId}/policy-info";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
