package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.AbstractPostClaimNotificationDamageOASValidator;

@ApiName("Post Claim Notification Damage V1.3.0")
public class PostClaimNotificationDamageOASValidatorV1 extends AbstractPostClaimNotificationDamageOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/claimNotification/swagger-claim-notification-1.3.0.yaml";
    }
}
