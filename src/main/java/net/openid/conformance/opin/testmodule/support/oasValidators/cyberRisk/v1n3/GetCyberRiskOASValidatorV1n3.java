package net.openid.conformance.opin.testmodule.support.oasValidators.cyberRisk.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Cyber Risk 1.3.0")
public class GetCyberRiskOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/cyberRisk/v1n3/swagger-cyber-risk-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/cyber-risk";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}