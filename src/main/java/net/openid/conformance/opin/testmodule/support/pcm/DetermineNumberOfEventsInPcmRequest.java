package net.openid.conformance.opin.testmodule.support.pcm;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;
import java.util.Optional;

public class DetermineNumberOfEventsInPcmRequest extends AbstractCondition {

    protected JsonElement bodyFrom(Environment environment) {
        try {
            return BodyExtractor.bodyFrom(environment, "received_request")
                    .orElseThrow(() -> error("Could not extract body from the request"));
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }
    }


    @Override
    @PreEnvironment(required = "received_request")
    public Environment evaluate(Environment env) {
        int eventsSize = Optional.ofNullable(bodyFrom(env).getAsJsonObject().get("events"))
                .map(JsonElement::getAsJsonArray)
                .map(JsonArray::size)
                .orElse(1);

        logSuccess("Determined number of event in the PCM request", args("number_of_events", eventsSize));
        env.putInteger("number_of_events", eventsSize);
        return env;
    }

}
