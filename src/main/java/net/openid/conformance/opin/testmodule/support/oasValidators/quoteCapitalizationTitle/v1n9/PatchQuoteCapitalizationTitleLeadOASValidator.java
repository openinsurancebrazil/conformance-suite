package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.AbstractPatchQuoteCapitalizationTitleLeadOASValidator;

@ApiName("Quote Capitalization Title 1.9.4")
public class PatchQuoteCapitalizationTitleLeadOASValidator extends AbstractPatchQuoteCapitalizationTitleLeadOASValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteCapitalizationTitle/swagger-quote-capitalization-title.yaml";
    }

}
