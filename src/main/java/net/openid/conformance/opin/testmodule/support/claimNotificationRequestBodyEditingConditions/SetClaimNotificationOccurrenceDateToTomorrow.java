package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class SetClaimNotificationOccurrenceDateToTomorrow extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        String occurrenceDate = LocalDateTime.now(ZoneOffset.UTC)
            .plusDays(1)
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        data.addProperty("occurrenceDate", occurrenceDate);
        logSuccess("occurrenceDate set to tomorrow", args("data", data));
        return env;
    }
}
