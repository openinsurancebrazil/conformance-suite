package net.openid.conformance.opin.testmodule.support.oasValidators.channels.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api source: swagger/openinsurance/channels/v1/swagger-channels-1.4.0.yml
 * Api endpoint: /intermediary/{countrySubDivision}
 * Api version: 1.4.0
 */

@ApiName("Channels Intermediary")
public class GetChannelsIntermediaryOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/channels/v1/swagger-channels-1.4.0.yml";
    }

    @Override
    protected String getEndpointPath() {
        return "/intermediary/{countrySubDivision}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

}

