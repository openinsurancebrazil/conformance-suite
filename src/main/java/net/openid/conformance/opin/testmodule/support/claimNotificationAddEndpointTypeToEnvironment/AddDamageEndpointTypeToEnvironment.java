package net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment;

public class AddDamageEndpointTypeToEnvironment extends AbstractAddEndpointTypeToEnvironment {

    @Override
    protected String endpointType() {
        return "damage";
    }
}
