package net.openid.conformance.opin.testmodule.v1.capitalization;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;

public abstract class AbstractCapitalizationTitleApiCoreTestModule extends AbstractOpinApiTestModuleV2 {


    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddCapitalizationTitleScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceCapitalizationTitleUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.CAPITALIZATION_TITLE;
    }

    @Override
    protected String getApi() {
        return "insurance-capitalization-title";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        return PlanIdSelector.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "plan-info", getPlanInfoValidator(),
                "events", getEventsValidator(),
                "settlements", getSettlementsValidator()
        );
    }

    protected abstract Class<? extends AbstractJsonAssertingCondition> getPlanInfoValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getEventsValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getSettlementsValidator();

}
