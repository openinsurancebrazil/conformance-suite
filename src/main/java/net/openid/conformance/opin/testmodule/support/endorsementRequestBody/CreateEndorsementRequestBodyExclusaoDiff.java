package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyExclusaoDiff extends AbstractEndorsementRequestBodyDiff {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.EXCLUSAO;
    }
}
