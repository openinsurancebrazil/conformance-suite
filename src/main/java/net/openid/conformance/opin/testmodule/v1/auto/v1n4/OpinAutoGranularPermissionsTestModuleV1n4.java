package net.openid.conformance.opin.testmodule.v1.auto.v1n4;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.auto.AbstractOpinAutoGranularPermissionsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
        testName = "opin-auto-api-granular-permissions-test-v1.4.0",
        displayName = "Ensure specific methods can only be called if the respective permissions is granted.",
        summary ="Ensure specific methods can only be called if the respective permissions is granted.\n" +
                "\u2022 Call the POST Consents with the RESOURCES_READ, READ and POLICY_INFO permissions\n" +
                "\u2022 Expects 201 - Validate response\n" +
                "\u2022 Redirects the user to Authorize consent\n" +
                "\u2022 Calls the GET Consents\n" +
                "\u2022 Expects 200 - Validate Response and Status is Authorized\n" +
                "\u2022 Call the GET Root Endpoint\n" +
                "\u2022 Expects 200 - Validate Response\n" +
                "\u2022 Call the GET Policy Info Endpoint\n" +
                "\u2022 Expects 200 - Validate Response\n" +
                "\u2022 Call the GET Premium Endpoint\n" +
                "\u2022 Expects a 403 response  - Validate error response \n" +
                "\u2022 Call the GET Claim Endpoint\n" +
                "\u2022 Expects a 403 response  - Validate error response ",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})
public class OpinAutoGranularPermissionsTestModuleV1n4 extends AbstractOpinAutoGranularPermissionsTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceAutoListOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
        return GetInsuranceAutoPolicyInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
        return GetInsuranceAutoPremiumOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsuranceAutoClaimOASValidatorV1n4.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
        return IdSelectorFromJsonPath.class;
    }
}
