package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawal.v1n3.PostPensionWithdrawalOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.AbstractCreatePensionWithdrawalConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.CreatePensionWithdrawalParcialConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.AbstractCreatePensionWithdrawalRequestBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalParcialPostRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-pension-withdraw_api_parcial-core_test-module_v1",
        displayName = "Validates the structure of Pension Withdrawal API",
        summary = """
                Ensure a life pension withdrawal request can be successfully executed with withdrawalType as 2_PARCIAL
                
                · Execute a Customer Data Sharing Journey for the Life Pension Product, obtaining the first productName and certificateId from the insurance-life-pension/contracts endpoint, and the corresponding data.suseps.FIE.pmbacAmount from the contract-info endpoint
                · Call the POST Consents with PENSION_WITHDRAWAL_CREATE permission, sending the withdrawalLifePensionInformation with the pre-saved information, withdrawalType as 2_PARCIAL, and  the desiredTotalAmount field as half of the value of pmbacAmount
                · Expect a 201 - Validate the response
                · Redirect the User to Authorize theConsent
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response and ensure the status is "AUTHORIZED"
                · Call the POST pension/request endpoint, sending the same information as the consent request
                · Expect a 201 - Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                · Call the GET at data.redirectLink
                · Expect a 200
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3PensionWithdrawApiParcialCoreTestModule extends AbstractOpinPhase3PensionWithdrawHappyPathTestModule {
    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostPensionWithdrawalOASValidatorV1n3();
    }

    @Override
    protected void configureClient() {
        super.configureClient();
        env.putString("withdrawalType", EnumWithdrawalType.PARCIAL.toString());
    }

    @Override
    protected AbstractCreatePensionWithdrawalRequestBody createPensionWithdrawalRequestBody() {
        return new CreatePensionWithdrawalParcialPostRequestBody();
    }

    @Override
    protected AbstractCreatePensionWithdrawalConsentBody createPensionWithdrawalConsentBody() {
        return new CreatePensionWithdrawalParcialConsentBody();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }
}
