package net.openid.conformance.opin.testmodule.v1.person.abstracts;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;


public abstract class AbstractPersonApiResourcesTestModule extends AbstractOpinApiResourcesTestModuleV2 {
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddPersonScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsurancePersonUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_PERSON;

    }

    @Override
    protected String getApi() {
        return "insurance-person";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[*].brand.companies[*].policies[*].policyId");
        return AllIdsSelectorFromJsonPath.class;
    }

    @Override
    protected String getResourceType() {
        return EnumOpinResourcesType.DAMAGES_AND_PEOPLE_PERSON.name();
    }

    @Override
    protected String getResourceStatus() {
        return EnumResourcesStatus.AVAILABLE.name();
    }
}
