package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AddPersonEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationDocumentTypeToInvalid;


public abstract class AbstractOpinClaimNotificationApiPersonWrongDocumentTypeTestModule extends AbstractClaimNotificationsNegativeTest {

    @Override
    protected void editClaimNotificationRequestBody() {
        callAndStopOnFailure(SetClaimNotificationDocumentTypeToInvalid.class);
    }

    @Override
    protected AbstractAddEndpointTypeToEnvironment addEndpointType() {
        return new AddPersonEndpointTypeToEnvironment();
    }

    @Override
    protected void setupPermissions() {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_PERSON).build();
    }

}
