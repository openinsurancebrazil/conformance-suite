package net.openid.conformance.opin.testmodule.v1.endorsement;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.AbstractEndorsementApiMultipleEndorsementsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "opin-endorsement-api-multiple-endorsements-test",
    displayName = "Ensure that endorsement cannot be created multiple times with a unique consent",
    summary = "Ensure that endorsement cannot be created multiple times with a unique consent\n" +
        "\u2022 Call the POST Consents API with all the existing Phase 2 permissions\n" +
        "\u2022 Expects 201 - Validate Response \n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "\u2022 Call the resources API\n" +
        "\u2022 Extract a shared policy ID on the resources APIs\n" +
        "\u2022 Call the POST Consents Endpoint , sending the policyNumber as the extracted policyID\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for EXCLUSAO, set policy ID for the extracted policyID\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"CONSUMED\"\n" +
        "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for EXCLUSAO, set policy ID for the extracted policyID and a different idempotency key\n" +
        "\u2022 Expects 401 - Validate Error response",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "resource.consentUrl",
        "resource.brazilCpf",
        "consent.productType"
    }
)
public class OpinEndorsementApiMultipleEndorsementsTestModule extends AbstractEndorsementApiMultipleEndorsementsTestModule {

}
