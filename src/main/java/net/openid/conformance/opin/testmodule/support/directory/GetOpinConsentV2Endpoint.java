package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetOpinConsentV2Endpoint extends AbstractGetXFromAuthServer {

    @Override
    protected String getEndpointRegex() {
        return "^(https:\\/\\/)(.*?)(\\/open-insurance\\/consents\\/v\\d+/consents)$";
    }

    @Override
    protected String getApiFamilyType() {
        return "consents";
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(2.[0-9].[0-9])$";
    }

    @Override
    protected boolean isResource() {
        return false;
    }
}
