package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteTransportLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "ACIDENTES_PESSOAIS_COM_PASSAGEIROS";
    }
}
