package net.openid.conformance.opin.testmodule.phase1.lifePension;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.lifePension.v1n5.GetProductsNServicesLifePensionOASValidatorV1n5;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - Life Pension API test",
	displayName = "Validate structure of Life Pension response",
	summary = "Validate structure of Life Pension response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class LifePensionTestModule extends AbstractNoAuthFunctionalTestModule {
	@Override
	protected void runTests() {
		runInBlock("Validate ProductsNServices Life Pension response", () -> {
			callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetProductsNServicesLifePensionOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
		});
	}
}
