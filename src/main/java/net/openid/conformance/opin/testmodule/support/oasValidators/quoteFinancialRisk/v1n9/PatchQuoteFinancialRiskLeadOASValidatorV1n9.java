package net.openid.conformance.opin.testmodule.support.oasValidators.quoteFinancialRisk.v1n9;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Quote Financial Risks V1.9.0")
public class PatchQuoteFinancialRiskLeadOASValidatorV1n9 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteFinancialRisks/swagger-quote-financial-risk-1.9.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lead/request/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.PATCH;
    }
}
