package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

public class SetClaimNotificationIdFieldToInvalidValueDiff extends AbstractSetClaimNotificationIdFieldToInvalidValue {

    @Override
    protected String getPolicyIdFieldName() {
        return "policyId";
    }
}

