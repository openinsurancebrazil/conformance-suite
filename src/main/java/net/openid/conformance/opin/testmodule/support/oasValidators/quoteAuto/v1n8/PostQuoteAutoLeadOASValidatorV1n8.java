package net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n8;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.AbstractPostQuoteAutoLeadOASValidator;

@ApiName("Quote Auto 1.8.0")
public class PostQuoteAutoLeadOASValidatorV1n8 extends AbstractPostQuoteAutoLeadOASValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteAuto/quote-auto-v1.8.0.yaml";
    }
}
