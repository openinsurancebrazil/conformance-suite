package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;

import java.util.Optional;
import java.util.Set;

public abstract class AbstractSetClaimNotificationIdFieldToWrongType extends AbstractCondition {

    private final Set<String> DOCUMENT_TYPE_GROUP_CERTIFICATE_ID = SetUtils.createSet("APOLICE_INDIVIDUAL, BILHETE, APOLICE_INDIVIDUAL_AUTOMOVEL, APOLICE_FROTA_AUTOMOVEL");
    private final Set<String> DOCUMENT_TYPE_POLICY_NUMBER = SetUtils.createSet("CERTIFICADO, CERTIFICADO_AUTOMOVEL");

    @Override
    @PreEnvironment(required = "resource_request_entity", strings = "policyId")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        String documentType = OIDFJSON.getString(Optional.ofNullable(
            data.get("documentType"))
            .orElseThrow(() -> error("documentType field not present in response body")));
        String policyId = env.getString("policyId");

        if (DOCUMENT_TYPE_GROUP_CERTIFICATE_ID.contains(documentType)) {
            data.remove(getPolicyIdFieldName());
            data.addProperty("groupCertificateId", policyId);
        } else if (DOCUMENT_TYPE_POLICY_NUMBER.contains(documentType)) {
            data.remove("groupCertificateId");
            data.addProperty(getPolicyIdFieldName(), policyId);
        } else {
            throw error("Invalid documentType.", args("documentType", documentType));
        }

        logSuccess("The wrong ID field has been set", args("data", data));
        return env;
    }

    protected abstract String getPolicyIdFieldName();
}
