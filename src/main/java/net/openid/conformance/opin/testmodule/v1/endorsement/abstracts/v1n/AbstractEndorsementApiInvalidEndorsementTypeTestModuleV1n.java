package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n;

import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyInclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyInclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementTypeToInvalid;

public abstract class AbstractEndorsementApiInvalidEndorsementTypeTestModuleV1n extends AbstractEndorsementApiINegativeTestModuleV1n {

    @Override
    protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
        return new CreateEndorsementRequestBodyInclusaoDiff();
    }

    @Override
    protected void editEndorsementRequestBody() {
        callAndStopOnFailure(SetEndorsementTypeToInvalid.class);
    }

    @Override
    protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
        return CreateEndorsementConsentBodyInclusaoDiff.class;
    }
}
