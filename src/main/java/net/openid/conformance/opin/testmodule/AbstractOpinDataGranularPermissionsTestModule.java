package net.openid.conformance.opin.testmodule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.opin.testmodule.support.*;

public class AbstractOpinDataGranularPermissionsTestModule extends AbstractOpinApiTestModule{

    Boolean isError=false;
    @Override
    protected void validateResponse(){


        callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(PolicyIDSelector.class);

        validate(PrepareUrlForFetchingPolicyInfo.class, policyInfoValidator, String.format("Fetch %s policy info", api));
        isError= true;
        validate(PrepareUrlForFetchingPremium.class, premiumValidator, String.format("Fetch %s premium, expect error response", api));
        validate(PrepareUrlForFetchingClaim.class, claimValidator, String.format("Fetch %s claim, expect error response", api));
    }

    @Override
    protected void validate(Class<? extends Condition> prepareUrlCondition, Class<? extends Condition> validator, String logMsg) {
        callAndStopOnFailure(prepareUrlCondition);
        preCallProtectedResource(logMsg);
        callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
        if(isError){
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        }
        else {
            callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
        }
    }
    @Override
    protected void preCallProtectedResource(String blockHeader) {
        TestInstanceEventLog var10000 = this.eventLog;
        String var10001 = this.currentClientString();
        var10000.startBlock(var10001 + blockHeader);
        call(new CallProtectedResourceExpectingFailureSequence());
        this.eventLog.endBlock();
    }
}


