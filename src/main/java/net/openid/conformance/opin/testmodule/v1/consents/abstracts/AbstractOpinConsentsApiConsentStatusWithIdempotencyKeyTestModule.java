package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.opin.testmodule.support.OpinConsentEndpointResponseValidatePermissions;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractOpinConsentsApiConsentStatusWithIdempotencyKeyTestModule extends AbstractOpinConsentsApiConsentStatusTestModule {

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        OpenBankingBrazilPreAuthorizationSteps steps = new OpenBankingBrazilPreAuthorizationSteps(false,
                false, addTokenEndpointClientAuthentication, false, true,false);
        steps
                .insertAfter(CreateEmptyResourceEndpointRequestHeaders.class, setIdempotencyKey())
                .replace(FAPIBrazilConsentEndpointResponseValidatePermissions.class,condition(OpinConsentEndpointResponseValidatePermissions.class))
                .then(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"),
                        condition(postValidator().getClass()).dontStopOnFailure());
        return steps;
    }

    protected ConditionSequence setIdempotencyKey() {
        return sequenceOf(
                condition(CreateIdempotencyKey.class),
                condition(AddIdempotencyKeyHeader.class)
        );
    }
}
