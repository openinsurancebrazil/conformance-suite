package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyCancelamentoDiff extends AbstractEndorsementRequestBodyDiff {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.CANCELAMENTO;
    }
}
