package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.testmodule.Environment;

public class GetQuoteResourceV1Endpoint extends AbstractGetXFromAuthServer {
    private String apiFamilyType;
    private String apiName;
    private String apiEndpoint;

    @Override
    public Environment evaluate(Environment env) {
        apiFamilyType = env.getString("api_family_type");
        apiName = env.getString("api_name");
        apiEndpoint = env.getString("api_endpoint").replaceFirst("^/", "");
        return super.evaluate(env);
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(1\\.[0-9]+\\.[0-9]+)$";
    }

    @Override
    protected String getApiFamilyType() {
        return apiFamilyType;
    }

    @Override
    protected String getEndpointRegex() {
        return "^https:\\/\\/(.*?)(\\/open-insurance\\/" + apiName + "\\/v[0-9]+\\/" + apiEndpoint + ")(\\/)?$";
    }

    @Override
    protected boolean isResource() {
        return true;
    }
}
