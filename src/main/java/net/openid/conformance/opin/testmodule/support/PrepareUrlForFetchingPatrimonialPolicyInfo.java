package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingPatrimonialPolicyInfo extends AbstractPrepareUrlForApi {

    private String policyId;

    @PreEnvironment(strings = "policyId")
    public Environment evaluate(Environment env) {
        policyId = env.getString("policyId");
        return super.evaluate(env);
    }

    @Override
    protected String getApiReplacement() {
        return "insurance-patrimonial";
    }

    @Override
    protected String getEndpointReplacement() {
        return String.format("insurance-patrimonial/%s/policy-info", policyId);
    }
}