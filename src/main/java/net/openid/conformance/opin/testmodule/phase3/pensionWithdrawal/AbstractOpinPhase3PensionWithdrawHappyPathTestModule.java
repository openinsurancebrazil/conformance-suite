package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForPensionWithdrawal;
import net.openid.conformance.opin.testmodule.support.RedirectLinkSelector;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.AbstractCreatePensionWithdrawalRequestBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalTotalPostRequestBody;

public abstract class AbstractOpinPhase3PensionWithdrawHappyPathTestModule extends AbstractOpinPhase3PensionWithdrawTestModule {

    @Override
    protected void validateGetResourcesResponse() {
        //endpoint validated in other step
    }

    @Override
    protected void executeTest() {
        postPensionWithdrawal();
        validatePensionWithdrawalResponse();
        env.putObject("old_resource_endpoint_response", env.getObject("resource_endpoint_response_full"));
        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
        getRedirectLinkAndValidateResponse();
    }

    @Override
    protected AbstractCreatePensionWithdrawalRequestBody createPensionWithdrawalRequestBody() {
        return new CreatePensionWithdrawalTotalPostRequestBody();
    }

    protected void postPensionWithdrawal() {
        eventLog.startBlock("POST Pension Withdrawal - Expecting 201");
        callAndStopOnFailure(PrepareUrlForPensionWithdrawal.class);
        callAndStopOnFailure(createPensionWithdrawalRequestBody().getClass());
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        setIdempotencyKey();
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(AddJsonAcceptHeaderRequest.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
    }

    protected void validatePensionWithdrawalResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

    protected void getRedirectLinkAndValidateResponse() {
        eventLog.startBlock("GET redirectLink - Expecting 200");
        env.mapKey("resource_endpoint_response_full", "old_resource_endpoint_response");
        callAndStopOnFailure(RedirectLinkSelector.class);
        env.unmapKey("resource_endpoint_response_full");
        callProtectedResource();
        eventLog.endBlock();
        fireTestFinished();
    }

    protected abstract AbstractJsonAssertingCondition validator();

    protected void setIdempotencyKey() {
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
    }

}
