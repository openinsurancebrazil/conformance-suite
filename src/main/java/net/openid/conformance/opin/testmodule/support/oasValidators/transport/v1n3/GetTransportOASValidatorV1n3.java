package net.openid.conformance.opin.testmodule.support.oasValidators.transport.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Transport 1.3.0")
public class GetTransportOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/transport/v1n3/swagger-transport-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/transport";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}