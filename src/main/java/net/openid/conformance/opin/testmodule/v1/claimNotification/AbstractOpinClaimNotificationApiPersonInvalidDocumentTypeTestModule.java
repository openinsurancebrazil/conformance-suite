package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AddPersonEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationDocumentTypeToANonExistent;

public abstract class AbstractOpinClaimNotificationApiPersonInvalidDocumentTypeTestModule extends AbstractClaimNotificationsNegativeTest {

        @Override
        protected AbstractAddEndpointTypeToEnvironment addEndpointType() {
                return new AddPersonEndpointTypeToEnvironment();
        }

        @Override
        protected void editClaimNotificationRequestBody() {
                callAndStopOnFailure(SetClaimNotificationDocumentTypeToANonExistent.class);
        }
        @Override
        protected void setupPermissions() {
                OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
                permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_PERSON).build();
        }
}
