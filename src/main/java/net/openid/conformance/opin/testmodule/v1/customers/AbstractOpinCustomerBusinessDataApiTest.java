package net.openid.conformance.opin.testmodule.v1.customers;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBusinessProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractOpinCustomerBusinessDataApiTest extends AbstractOpinFunctionalTestModule {

	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessIdentificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessQualificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessComplimentaryInfoValidator();

	@Override
	protected void configureClient(){
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(PrepareConfigForCustomerBusinessTest.class);
		callAndStopOnFailure(BuildOpinBusinessCustomersConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS);
		permissionsBuilder.build();

		callAndStopOnFailure(AddScopesForCustomerApi.class);
		callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
		callAndStopOnFailure(AddDummyBusinessProductTypeToConfig.class);
	}

	@Override
	protected void requestProtectedResource() {
		validateResponse();
	}

	@Override
	protected void validateResponse() {
		runInBlock("Validating business identifications response V1", () -> {
			callAndStopOnFailure(PrepareToGetBusinessIdentifications.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getCustomerBusinessIdentificationValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Validating business qualifications response V1", () -> {
			callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getCustomerBusinessQualificationValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Validating business complimentary-information response V1", () ->{
			callAndStopOnFailure(PrepareToGetBusinessComplimentaryInformation.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getCustomerBusinessComplimentaryInfoValidator(), Condition.ConditionResult.FAILURE);
		});

	}
}
