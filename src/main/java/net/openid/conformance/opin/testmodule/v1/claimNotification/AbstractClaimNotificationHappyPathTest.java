package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.support.CreateClaimNotificationRequestBody;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForClaimNotification;

public abstract class AbstractClaimNotificationHappyPathTest extends AbstractOpinClaimNotificationTestModule {

    @Override
    protected void executeTest() {
        postClaimNotification();
        validateClaimNotificationResponse();
        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
    }

    protected void postClaimNotification() {
        eventLog.startBlock("POST claim notification - Expecting 201");
        callAndStopOnFailure(PrepareUrlForClaimNotification.class);
        createClaimNotificationRequestBody();
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        setIdempotencyKey();
        setupPostClaimNotification();
        eventLog.endBlock();
    }

    protected void createClaimNotificationRequestBody() {
        callAndStopOnFailure(CreateClaimNotificationRequestBody.class);
    }

    protected void validateClaimNotificationResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE, new String[]{});
        callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

    protected abstract AbstractJsonAssertingCondition validator();

    protected void setIdempotencyKey() {
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
    }

    protected void setupPostClaimNotification() {
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
    }
}
