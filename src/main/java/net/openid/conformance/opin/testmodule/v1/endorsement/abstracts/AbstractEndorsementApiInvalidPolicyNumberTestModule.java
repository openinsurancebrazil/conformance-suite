package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts;

import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyAlteracao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIdToInvalid;

public abstract class AbstractEndorsementApiInvalidPolicyNumberTestModule extends AbstractEndorsementApiNegativeTestModule {

    @Override
    protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
        return new CreateEndorsementRequestBodyAlteracao();
    }

    @Override
    protected void editEndorsementRequestBody() {
        callAndStopOnFailure(SetEndorsementPolicyIdToInvalid.class);
    }

    @Override
    protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
        return CreateEndorsementConsentBodyExclusao.class;
    }
}

