package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasCanc;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchCancRequestBody;

public abstract class AbstractOpinQuotePatrimonialCondominiumCoreCancTest extends AbstractOpinQuotePatrimonialCondominiumCoreTest {

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchCancRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasCanc.class;
    }
}
