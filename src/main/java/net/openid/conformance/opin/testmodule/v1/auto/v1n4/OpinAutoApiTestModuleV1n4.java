package net.openid.conformance.opin.testmodule.v1.auto.v1n4;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.auto.AbstractOpinAutoApiTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-auto-api-test-v1.4.0",
        displayName = "Validates the structure of all auto API resources",
        summary ="Validates the structure of all auto API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_AUTO_READ”, “DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "\u2022 Calls GET auto “/” API \n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
                "\u2022 Calls GET auto policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields \n" +
                "\u2022 Calls GET auto premium API \n" +
                "\u2022 Expects 200- Validate all the fields \n" +
                "\u2022 Calls GET auto claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }

)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})

public class OpinAutoApiTestModuleV1n4 extends AbstractOpinAutoApiTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
        return GetInsuranceAutoPolicyInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
        return GetInsuranceAutoPremiumOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsuranceAutoClaimOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceAutoListOASValidatorV1n4.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
        return IdSelectorFromJsonPath.class;
    }
}