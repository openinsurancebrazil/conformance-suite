package net.openid.conformance.opin.testmodule.v1.consents.v2n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n5.PostConsentsOASValidatorV2n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentsApiGranularPermissionTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
        testName = "opin-consent-api-granular-permissions-test",
        displayName = "Ensure a consent can be created with different permissions groups, containing both mandatory and optional permissions. This test requires at least one resource be available at the resources API",
        summary = "Ensure a consent can be created with different permissions groups, containing both mandatory and optional permissions. This test requires at least one resource be available at the resources API\n" +
                "• Call the POST Consents with all phase 2 permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Redirects the user to Authorize consent\n" +
                "• Calls the GET Consents\n" +
                "• Expects 200 - Validate Response and Status is Authorized\n" +
                "• Call the resources API, and identify the first resource being shared\n" +
                "If the resource is customer business or personal related:\n" +
                "• Call the POST Consents with the RESOURCES_READ and IDENTIFICATIONS_READ permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ and QUALIFICATION_READ permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ, QUALIFICATION_READ and ADDITIONALINFO_READ permissions\n" +
                "• Expects 201 - Validate response\n" +
                "If the resource is not from customer:" +
                "• Call the POST Consents with the RESOURCES_READ and READ permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ and POLICYINFO permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ and PREMIUM permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ and CLAIM permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ, READ and POLICYINFO permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ, PREMIUM and CLAIM permissions\n" +
                "• Expects 201 - Validate response\n" +
                "• Call the POST Consents with the RESOURCES_READ, READ, POLICYINFO and PREMIUM permissions\n" +
                "• Expects 201 - Validate response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})
public class OpinConsentsApiGranularPermissionTestModule extends AbstractOpinConsentsApiGranularPermissionTestModule {

        @Override
        protected AbstractJsonAssertingCondition postValidator() {
                return new PostConsentsOASValidatorV2n5();
        }

        @Override
        protected ConditionSequence createOBBPreauthSteps() {
                return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n5.class));
        }
}