package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Life Pensions 1.5.0")
public class GetInsuranceLifePensionContractInfoOASValidatorV1n5 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceLifePension/v1/swagger-insurance-life-pension-1.5.0.yaml";
	}
	@Override
	protected String getEndpointPath() {
		return "/insurance-life-pension/{certificateId}/contract-info";
	}
	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
