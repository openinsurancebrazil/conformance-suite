package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateQuotePatrimonialDiverseRisksPostRequestBodyWithCoverage extends AbstractCreateQuotePostRequestBody {

    @Override
    protected String getCode(){
        return "RESIDENCIAL_IMOVEL_BASICA";
    }

    @Override
    protected void editData(Environment env, JsonObject data) {
        editQuoteData(env, data);
    }

    private void editQuoteData(Environment env, JsonObject data) {
        JsonObject quoteData = data.getAsJsonObject("quoteData");

        quoteData.addProperty("isCollectiveStipulated", false);
        quoteData.add("insuranceTermStartDate", quoteData.get("termStartDate"));
        quoteData.addProperty("insuredObjectType", "EQUIPAMENTO_MOVEL");
        quoteData.add("beneficiaries", getBeneficiaries(env));

        JsonObject maxLmg = new JsonObject();
        maxLmg.addProperty("amount", "90.85");
        maxLmg.addProperty("unitType", "PORCENTAGEM");

        JsonArray coverages = getCoverages();

        quoteData.add("maxLMG", maxLmg);
        quoteData.add("coverages", coverages);
    }
}
