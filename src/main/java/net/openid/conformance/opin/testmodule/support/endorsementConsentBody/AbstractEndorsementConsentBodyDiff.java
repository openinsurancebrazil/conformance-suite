package net.openid.conformance.opin.testmodule.support.endorsementConsentBody;

import com.google.gson.JsonArray;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.EndorsementType;

public abstract class AbstractEndorsementConsentBodyDiff extends AbstractCreateEndorsementConsentBody {

    @Override
    protected String getPolicyFieldName(){
        return "policyId";
    }

    @Override
    protected JsonArray getInsuredObjectId() {
        JsonArray insuredObjectIds = new JsonArray();
        insuredObjectIds.add("216731531723");
        return insuredObjectIds;
    }
}
