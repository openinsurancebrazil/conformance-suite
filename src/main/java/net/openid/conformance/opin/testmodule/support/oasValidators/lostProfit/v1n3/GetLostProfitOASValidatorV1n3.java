package net.openid.conformance.opin.testmodule.support.oasValidators.lostProfit.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Lost Profit 1.3.0")
public class GetLostProfitOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/lostProfit/v1/swagger-lost-profit-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lost-profit";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
