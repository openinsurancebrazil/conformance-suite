package net.openid.conformance.opin.testmodule.fvp;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.ExtractMTLSCertificatesFromConfiguration;
import net.openid.conformance.condition.client.GetStaticClientConfiguration;
import net.openid.conformance.condition.client.ValidateMTLSCertificatesHeader;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBusinessProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.AddOpenIdScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.directory.GetCustomerBusinessV1Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessIdentificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessQualificationListOASValidatorV1n5;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-fvp-customer-business-data-api-test",
        displayName = "Validate structure of all business customer data API resources V1",
        summary = "Validates the structure of all business customer data API resources V1\n" +
                "\u2022 Creates a Consent with the customer business permissions (\"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\",\"CUSTOMERS_BUSINESS_ADDITIONALINFO_READ\",\"RESOURCES_READ\")\n" +
                "\u2022 Expects a success 201 - Validate Response\n" +
                "\u2022 Calls GET Business Identifications Endpoint\n" +
                "\u2022 Expects a success 200 - Validate Response\n" +
                "\u2022 Calls GET Business Qualifications Endpoint\n" +
                "\u2022 Expects a success 200 - Validate Response\n" +
                "\u2022 Calls GET Business Complimentary-Information Endpoint\n" +
                "\u2022 Expects a success 200 - Validate Response\n",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.consentSyncTime",
                "resource.brazilCpf"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks","consent.productType"
})
public class OpinFvpCustomerBusinessApiTestModule extends AbstractOpinFunctionalTestModule {

    private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

    @Override
    protected void configureClient(){
        call(new ValidateRegisteredEndpoints(sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetCustomerBusinessV1Endpoint.class))));
        env.putString("api_type", API_TYPE);
        callAndStopOnFailure(PrepareConfigForCustomerBusinessTest.class);
        callAndStopOnFailure(BuildOpinBusinessCustomersConfigResourceUrlFromConsentUrl.class);
        callAndStopOnFailure(GetStaticClientConfiguration.class);
        callAndStopOnFailure(AddOpenIdScope.class);
        exposeEnvString("client_id");

        // Test won't pass without MATLS, but we'll try anyway (for now)
        callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
        callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);
        validateClientConfiguration();

    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS);
        permissionsBuilder.build();

        callAndStopOnFailure(AddScopesForCustomerApi.class);
        callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
        callAndStopOnFailure(AddDummyBusinessProductTypeToConfig.class);
    }

    @Override
    protected void requestProtectedResource() {
        syncWaitTime();
        validateResponse();
    }

    @Override
    protected void validateResponse() {
        runInBlock("Validating business identifications response V1", () -> {
            callAndStopOnFailure(PrepareToGetBusinessIdentifications.class);
            callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetCustomersBusinessIdentificationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("Validating business qualifications response V1", () -> {
            callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
            callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetCustomersBusinessQualificationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("Validating business complimentary-information response V1", () ->{
            callAndStopOnFailure(PrepareToGetBusinessComplimentaryInformation.class);
            callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
        });

    }
}
