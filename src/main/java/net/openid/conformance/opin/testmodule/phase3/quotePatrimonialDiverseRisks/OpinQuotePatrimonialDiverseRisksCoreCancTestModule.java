package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks.PatchPatrimonialDiverseRisksOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks.PostPatrimonialDiverseRisksOASValidatorV1n9;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-diverse-risks_api_core-canc_test-module_v1",
        displayName = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and rejected afterwards.",
        summary = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and rejected afterwards. This test applies to both Personal and diverse-risks products. If the \"brazilCnpj\" field is filled out, it will transmit diverse-risks information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST diverse-risks/request endpoint sending personal or diverse-risks information, following what is defined at the config\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Poll the GET diverse-risks/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET diverse-risks/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respones and ensure status is ACPT\n" +
                "\u2022 Call PATCH diverse-risks/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC\n" +
                "\u2022 Expect 200 - Validate Response and ensure status is CANC",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialDiverseRisksCoreCancTestModule extends AbstractOpinQuotePatrimonialDiverseRisksCoreCancTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialDiverseRisksOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialDiverseRisksOASValidatorV1n9.class;
    }
}
