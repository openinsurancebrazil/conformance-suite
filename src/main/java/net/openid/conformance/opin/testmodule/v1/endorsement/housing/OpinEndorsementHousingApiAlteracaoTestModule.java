package net.openid.conformance.opin.testmodule.v1.endorsement.housing;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyAlteracao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyAlteracao;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1.PostEndorsementOASValidatorV1;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.AbstractEndorsementApiAlteracaoTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-endorsement-housing-api-alteracao-test",
        displayName = "Ensure an endorsement is successful on a happy path flow with endorsementType as ALTERACAO",
        summary = "Ensure an endorsement is successful on a happy path flow with endorsementType as ALTERACAO\n" +
                "\u2022 Call the POST Consents API with HOUSING Phase 2 permissions\n" +
                "\u2022 Expect 201 a with status on \"AWAITING_AUTHORISATION\" - Validate Response \n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the resources API\n" +
                "\u2022 Extract one policy ID on the resources APIs\n" +
                "\u2022 Call the DELETE Consents Endpoint for tha data Shared\n" +
                "\u2022 Expect a 204 NO Content\n" +
                "\u2022 Call the POST Consents Endpoint with the Endorsement permissions, sending the policyID as one the extracted policyID\n" +
                "\u2022 Expects 201 - Validate Response is \"AWAITING_AUTHORISATION\"\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AWAITING_AUTHORISATION\"\n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for ALTERACAO, set policy ID for the Extracted policyID, set requestDate as the currentdate\n" +
                "\u2022 Expects 201 - Validate Response\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"CONSUMED\"\n" +
                "\u2022 Redirects the user\n" +
                "\u2022 Expects Success",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinEndorsementHousingApiAlteracaoTestModule extends AbstractEndorsementApiAlteracaoTestModule {

    @Override
    protected void setupPermissions() {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_HOUSING).build();
    }
}
