package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Housing 1.4.0")
public class GetInsuranceHousingPolicyInfoOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceHousing/v1/swagger-insurance-housing-v1.4.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-housing/{policyId}/policy-info";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
