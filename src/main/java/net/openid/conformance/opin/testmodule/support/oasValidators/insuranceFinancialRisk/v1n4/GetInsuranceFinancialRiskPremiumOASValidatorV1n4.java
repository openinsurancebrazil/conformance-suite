package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Financial Risk 1.4.0")
public class GetInsuranceFinancialRiskPremiumOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceFinancialRisk/v1/swagger-insurance-financial-risk-1.4.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-financial-risk/{policyId}/premium";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}