package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks.AbstractOpinQuotePatrimonialDiverseRisksRjctTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.PostPatrimonialDiverseRisksOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks.PostPatrimonialDiverseRisksOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialPostRequestBodyWithCoverage_DiffTermDate;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-diverse-risks_api_rejected_test-module_v1n10",
        displayName = "Ensure that a Patrimonial diverse-risks quotation request can be rejected.",
        summary = "Ensure that a Patrimonial diverse-risks quotation request can be rejected. This test module will send the quoteData with only the required fields, and expect the quotation to be rejected. This test applies to both Personal and diverse-risks products. If the \"brazilCnpj\" field is filled out, it will transmit diverse-risks information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST diverse-risks/request endpoint sending personal or diverse-risks information, following what is defined at the config, and sending the quoteData with only the required fields, and setting termStartDate to date after termEndDate\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD or RJCT\n" +
                "\u2022 Poll the GET diverse-risks/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET diverse-risks/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respone and ensure status is RJCT",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialDiverseRisksRjctTestModuleV1n10 extends AbstractOpinQuotePatrimonialDiverseRisksRjctTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialDiverseRisksOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialPostRequestBodyWithCoverage_DiffTermDate.class;
    }
}
