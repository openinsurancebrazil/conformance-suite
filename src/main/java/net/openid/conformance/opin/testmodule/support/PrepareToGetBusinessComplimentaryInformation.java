package net.openid.conformance.opin.testmodule.support;

public class PrepareToGetBusinessComplimentaryInformation extends AbstractPrepareUrlForApi {

	@Override
	protected String getApiReplacement() {
		return "customers";
	}

	@Override
	protected String getEndpointReplacement() {
		return "business/complimentary-information";
	}
}
