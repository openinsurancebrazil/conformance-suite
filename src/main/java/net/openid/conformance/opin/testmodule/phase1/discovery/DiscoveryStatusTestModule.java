package net.openid.conformance.opin.testmodule.phase1.discovery;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.PrepareToGetDiscoveryOpenInsuranceApi;
import net.openid.conformance.opin.validator.discovery.StatusListValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Discovery - Status API test module ",
	displayName = "Validate structure of Discovery - Status response",
	summary = "Validate structure of Discovery - Status response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class DiscoveryStatusTestModule extends AbstractNoAuthFunctionalTestModule {
	@Override
	protected void runTests() {
		runInBlock("Validate Discovery Status response", () -> {
			callAndStopOnFailure(PrepareToGetDiscoveryOpenInsuranceApi.class, "status");
			preCallResource();
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(StatusListValidator.class, Condition.ConditionResult.FAILURE);
		});
	}
}
