package net.openid.conformance.opin.testmodule.support.pcm;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;
import java.util.Optional;

public class ExtractEventFromPcmRequest extends AbstractCondition {

    protected JsonElement bodyFrom(Environment environment) {
        try {
            return BodyExtractor.bodyFrom(environment, "received_request")
                    .orElseThrow(() -> error("Could not extract body from the request"));
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }
    }


    @Override
    @PreEnvironment(required = "received_request")
    @PostEnvironment(required = "pcm_event")
    public Environment evaluate(Environment env) {
        JsonObject body = bodyFrom(env).getAsJsonObject();

        if(body.has("events")){
            Integer eventNumber = Optional.ofNullable(env.getInteger("event_number"))
                    .orElseThrow(() -> error("event_number is missing in the environment"));
            JsonArray events = body.getAsJsonArray("events");
            JsonObject extractedElement = events.get(eventNumber).getAsJsonObject();
            env.putObject("pcm_event", extractedElement);
            logSuccess("Extracted event from events PCM Request JSON array",
                    args("element_number", eventNumber, "extracted_event", extractedElement));
            return env;
        }

        env.putObject("pcm_event", body);
        logSuccess("Extracted event from PCM request", args("extracted_event", body));
        return env;
    }

}
