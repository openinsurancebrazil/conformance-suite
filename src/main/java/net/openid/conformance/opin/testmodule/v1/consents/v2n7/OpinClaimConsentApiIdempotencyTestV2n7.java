package net.openid.conformance.opin.testmodule.v1.consents.v2n7;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinClaimConsentApiIdempotencyTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-consent-api_idempotency_test-v2n7",
        displayName = "opin-claim-consent-api_idempotency_test-v2n7",
        summary = "Ensure that after an initial Consent request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idempotency key the request fails.\n" +
                "• Call the POST Consents API with either the customer business or the customer personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
                "• Expect a 201 - Validate all of the fields of response_body of the POST Consents API response\n" +
                "• Call the POST Consents API with the same payload and idempotency id\n" +
                "• Expects 201 - Validate Response\n" +
                "• Call the POST Consents API with a different payload as the previous request but the same idempotency id\n" +
                "• Expects 422 ERRO_IDEMPOTENCIA - Validate Response\n",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.customerUrl",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})
public class OpinClaimConsentApiIdempotencyTestV2n7 extends AbstractOpinClaimConsentApiIdempotencyTestModule {

    @Override
    protected AbstractJsonAssertingCondition postValidator() {
        return new PostConsentsOASValidatorV2n7();
    }

    @Override
    protected AbstractJsonAssertingCondition getValidator() {
        return new GetConsentsOASValidatorV2n7();
    }

}
