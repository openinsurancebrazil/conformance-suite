package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuoteFinancialRiskLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {
    @Override
    protected String getCode() {
        return "PROTECAO_DE_BENS";
    }
}