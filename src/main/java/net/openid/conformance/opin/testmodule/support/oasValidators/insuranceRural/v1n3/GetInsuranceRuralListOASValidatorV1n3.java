package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Rural 1.3.0")
public class GetInsuranceRuralListOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/rural/v1n/swagger-insurance-rural-api.yaml";
	}
	@Override
	protected String getEndpointPath() {
		return "/insurance-rural";
	}
	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
