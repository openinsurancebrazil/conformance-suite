package net.openid.conformance.opin.testmodule.support.oasValidators.dataChannels.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Phone Channels 1.5.0")
public class GetPhoneChannelsOASValidatorV1n5 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/dataChannels/v1n5/swagger-data_channels-1.5.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/phone-channels";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}