package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Transport 1.2.0")
public class GetInsuranceTransportListOASValidatorV1n2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceTransport/v1/swagger-insurance-transport.yaml";
	}
	@Override
	protected String getEndpointPath() {
		return "/insurance-transport";
	}
	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
