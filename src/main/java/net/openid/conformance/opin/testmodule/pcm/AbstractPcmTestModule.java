package net.openid.conformance.opin.testmodule.pcm;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureWellKnownUriIsRegistered;
import net.openid.conformance.openbanking_brasil.testmodules.support.MapDirectoryValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.UnmapDirectoryValues;
import net.openid.conformance.opin.testmodule.support.OpinSetDirectoryInfo;
import net.openid.conformance.opin.testmodule.support.sequences.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testmodule.support.pcm.*;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantConfigurationFields;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import net.openid.conformance.variant.VariantParameters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@VariantParameters({ClientAuthType.class})
@VariantConfigurationFields(
        parameter = ClientAuthType.class,
        value = "none",
        configurationFields = {
                "mtls.key",
                "mtls.cert",
                "server.discoveryUrl"
        }
)
@VariantHidesConfigurationFields(
        parameter = ClientAuthType.class,
        value = "none",
        configurationFields = {"resource.resourceUrl", "resource.brazilOrganizationId"}
)

public abstract class AbstractPcmTestModule extends AbstractNoAuthFunctionalTestModule {

    private boolean isRequestReceived = false;

    protected void waitForRequest() {
        long delaySeconds = 5;
        int attempts = 0;
        eventLog.startBlock("The test will now wait for the request to be received");
        setStatus(Status.WAITING);
        while (attempts < 20 && !isRequestReceived) {
            eventLog.log(getName(), "Waiting for request to be received at with an interval of " + delaySeconds + " seconds");
            try {
                Thread.sleep(delaySeconds * 1000);
            } catch (InterruptedException e) {
                throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
            }
            attempts++;
        }
        setStatus(Status.RUNNING);
        eventLog.endBlock();
    }

    @Override
    protected void runTests() {
        call(new ValidateOpinWellKnownUriSteps());
        callAndStopOnFailure(OpinSetDirectoryInfo.class);

        eventLog.log(getName(), "Please send a request to the following endpoint - /" + getExpectedPath());
        waitForRequest();
        validateRequest();
    }

    @Override
    public Object handleHttp(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
        if (isRequestReceived || !path.equals(getExpectedPath())) {
            super.handleHttp(path, req, res, session, requestParts);
        }

        env.putString("received_request", "body", OIDFJSON.getString(requestParts.get("body")));

        isRequestReceived = true;
        return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
    }

    protected void validateRequest() {
        runInBlock("Request validation", () -> {
            callAndStopOnFailure(getRequestValidator());
            callAndStopOnFailure(DetermineNumberOfEventsInPcmRequest.class);

            int numberOfEvents = Optional.ofNullable(env.getInteger("number_of_events"))
                    .orElseThrow(() -> new TestFailureException(getId(), "number_of_events is missing in the environment"));

            for (int eventNumber = 0; eventNumber < numberOfEvents; eventNumber++) {
                env.putInteger("event_number", eventNumber);
                callAndStopOnFailure(ExtractEventFromPcmRequest.class);
                callAndStopOnFailure(ExtractAsIdFromPcmRequestEvent.class);
                callAndStopOnFailure(ExtractClientSsIdFromPcmRequestEvent.class);
                validateJwt(); // org ID depends on endpoint
                validateSsId(); // always client org ID
                validateAs(); // always server org ID
            }
        });
    }

    protected void validateAs() {
        callAndStopOnFailure(ExtractServerOrgIdFromPcmRequestEvent.class);
        callAndStopOnFailure(EnsureWellKnownUriIsRegistered.class);
    }


    private void validateSsId() {
        callAndStopOnFailure(ExtractClientOrgIdFromPcmRequestEvent.class);

        env.mapKey("access_token", "directory_access_token");

        callAndStopOnFailure(ExtractDirectoryConfiguration.class);
        callAndStopOnFailure(ExtractMTLSCertificatesFromConfiguration.class);

        callAndContinueOnFailure(OpinCheckDirectoryDiscoveryUrl.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");

        callAndContinueOnFailure(OpinCheckDirectoryApiBase.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");

        callAndStopOnFailure(MapDirectoryValues.class);

        callAndStopOnFailure(GetDynamicServerConfiguration.class);

        // this overwrites the non-directory values; we will have to replace them below
        callAndContinueOnFailure(AddMTLSEndpointAliasesToEnvironment.class, Condition.ConditionResult.FAILURE, "RFC8705-5");

        callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);

        callAndStopOnFailure(SetDirectorySoftwareScopeOnTokenEndpointRequest.class);

        // MTLS client auth
        callAndStopOnFailure(AddClientIdToTokenEndpointRequest.class);

        callAndStopOnFailure(CallTokenEndpoint.class);

        callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);

        callAndStopOnFailure(CheckForAccessTokenValue.class);

        callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);

        callAndStopOnFailure(UnmapDirectoryValues.class);

        callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);

        // use access token to get ssa
        callAndStopOnFailure(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class);
        env.unmapKey("access_token");

        callAndStopOnFailure(CompareClientIdWithSsaResponse.class);
        callAndStopOnFailure(CompareOrgIdWithSsaResponse.class);
    }

    private void validateJwt() {

        if (isClientEndpoint()) {
            callAndStopOnFailure(ExtractClientOrgIdFromPcmRequestEvent.class);
        } else {
            callAndStopOnFailure(ExtractServerOrgIdFromPcmRequestEvent.class);
        }

        callAndStopOnFailure(FAPIBrazilGetKeystoreJwksUri.class);

        env.mapKey("endpoint_response", "received_request");
        env.mapKey("endpoint_response_jwt", "consent_endpoint_response_jwt");

        callAndStopOnFailure(ExtractSignedJwtFromResourceResponse.class);

        callAndStopOnFailure(FAPIBrazilValidateResourceResponseSigningAlg.class);

        callAndStopOnFailure(FAPIBrazilValidateResourceResponseTyp.class);


        env.mapKey("server", "org_server");
        env.mapKey("server_jwks", "org_server_jwks");
        callAndStopOnFailure(FetchServerKeys.class);
        env.unmapKey("server");
        env.unmapKey("server_jwks");
        callAndContinueOnFailure(ValidateResourceResponseSignature.class, Condition.ConditionResult.FAILURE);
    }

    abstract protected boolean isClientEndpoint();

    abstract protected Class<? extends Condition> getRequestValidator();

    abstract protected String getExpectedPath();
}
