package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteHousingLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "DANOS_ELETRICOS";
    }
}
