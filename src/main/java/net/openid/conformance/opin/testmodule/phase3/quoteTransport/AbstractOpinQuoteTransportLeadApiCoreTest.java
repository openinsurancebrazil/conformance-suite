package net.openid.conformance.opin.testmodule.phase3.quoteTransport;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteTransportLeadApiCoreTest extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_TRANSPORT_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-transport";
    }

    @Override
    protected String getApiName() {
        return "quote-transport";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}
