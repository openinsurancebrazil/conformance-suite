package net.openid.conformance.opin.testmodule.v1.claimNotification;

import com.google.common.base.Strings;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.opin.testmodule.support.ClaimNotificationSelectTwoResourcePolicyIds;
import net.openid.conformance.opin.testmodule.support.ExtractDocumentTypeFromPolicyInfoResponse;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForFetchingPolicyInfo;
import net.openid.conformance.opin.testmodule.support.TranslatePolicyTypeToApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;


public abstract class AbstractOpinClaimNotificationApiWrongPolicyIDTestModule extends AbstractClaimNotificationsNegativeTest {

    private boolean isThirdFlow = false;

    @Override
    protected void editClaimNotificationRequestBody() {
        //
    }

    @Override
    protected void validateGetResourcesResponse() {
        runInBlock("Validating resources response", () -> {
            callAndContinueOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);

            callAndStopOnFailure(addEndpointType().getClass());
            callAndContinueOnFailure(ClaimNotificationSelectTwoResourcePolicyIds.class, Condition.ConditionResult.INFO);
            if(Boolean.FALSE.equals(env.getBoolean("continue_test"))) {
                fireTestSkipped("Test skipped due to there being no resource with valid 'DAMAGES_AND_PEOPLE' type.");
            }

            callAndContinueOnFailure(TranslatePolicyTypeToApi.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(PrepareUrlForFetchingPolicyInfo.class);
        });

        preCallProtectedResource("Calling the correspondent API policy-info endpoint");

        callAndStopOnFailure(ExtractDocumentTypeFromPolicyInfoResponse.class);
    }

    protected void swapToFirstPolicyId() {
        if (!Strings.isNullOrEmpty(env.getString("first_policy_id"))) {
            eventLog.log(getName(), "Setting to first policy ID");
            env.putString("policyId", env.getString("first_policy_id"));
        }
    }

    protected void swapToSecondPolicyId() {
        if (!Strings.isNullOrEmpty(env.getString("second_policy_id"))) {
            eventLog.log(getName(), "Setting to second policy ID");
            env.putString("policyId", env.getString("second_policy_id"));
        }
    }

    @Override
    protected void postClaimNotification(String status) {
        if (!isThirdFlow) {
            swapToSecondPolicyId();
            super.postClaimNotification("422");
        } else {
            super.postClaimNotification("201");
        }
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
        if (isPolicyIdExtractionFlow) {
            isPolicyIdExtractionFlow = false;
            setupSecondFlow();
            performAuthorizationFlow();
        } else if (!isThirdFlow) {
            isThirdFlow = true;
            setupThirdFlow();
            performAuthorizationFlow();
        } else {
            super.onPostAuthorizationFlowComplete();
        }
    }

    @Override
    protected void validateClaimNotificationResponse() {
        if (!isThirdFlow) {
            super.validateClaimNotificationResponse();
        } else {
            eventLog.startBlock("Validate response");
            callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
            eventLog.endBlock();
        }
    }

    protected void setupThirdFlow() {
        swapToFirstPolicyId();
        setupSecondFlow();
    }
}
