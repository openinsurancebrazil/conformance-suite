package net.openid.conformance.opin.testmodule.support;

public class BuildOpinPersonalCustomersConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "customers";
	}

	@Override
	protected String getEndpointReplacement() {
		return "personal/identifications";
	}
}
