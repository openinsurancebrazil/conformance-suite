package net.openid.conformance.opin.testmodule.v1.person.v1n6;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonClaimOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonPolicyInfoOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonPremiumOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.v1.person.abstracts.AbstractPersonApiWrongPermissionsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "person_api_wrong-permissions_test-module_v1.6.0",
        displayName = "Ensures Capitalization Title API cannot be called with wrong permissions",
        summary = "Ensures API  cannot be called with wrong permissions - there will be two browser interactions with this test.\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“CAPITALIZATION_TITLE_READ”, “CAPITALIZATION_TITLE_PLANINFO_READ”, “CAPITALIZATION_TITLE_EVENTS_READ”, “CAPITALIZATION_TITLE_SETTLEMENTS_READ”, “RESOURCES_READ”)\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Person \"/\"  Endpoint\n" +
                "• Expects 200 - Fetches one of the policy IDs returned\n" +
                "• Calls GET Person Policy Info Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Person Premium  Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Person Claim Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field \n" +
                "• Expects a success 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Person \"/\"  Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Person Policy Info Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Person Premium  Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Person Claim Endpoint\n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class PersonApiWrongPermissionsTestModuleV1n6 extends AbstractPersonApiWrongPermissionsTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePersonListOASValidatorV1n6.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
        return GetInsurancePersonPolicyInfoOASValidatorV1n6.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
        return GetInsurancePersonPremiumOASValidatorV1n6.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsurancePersonClaimOASValidatorV1n6.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }
}
