package net.openid.conformance.opin.testmodule.fvp;


import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddFinancialAssistanceScope;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.directory.GetFinancialAssistanceV1Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n2.GetInsuranceFinancialAssistanceContractInfoOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n2.GetInsuranceFinancialAssistanceContractsOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n2.GetInsuranceFinancialAssistanceMovementsOASValidatorV1n2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Map;

@PublishTestModule(
        testName = "opin-fvp-financial-assistance-api-test-module",
        displayName = "Validates the structure of all Financial Assistance API",
        summary ="Validates the structure of all Financial Assistance API \n" +
                "\u2022 Call the POST consents with all the permissions needed to access the Capitalization Title API (“FINANCIAL_ASSISTANCE_READ”, “FINANCIAL_ASSISTANCE_CONTRACTINFO_READ”, “FINANCIAL_ASSISTANCE_MOVEMENTS_READ”, “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 -  Validate Response \n" +
                "\u2022 Redirect the user to authorize consent \n" +
                "\u2022 Calls GET Financial Assistance contracts Endpoint \n" +
                "\u2022 Expects 200 - Fetches one of the contract IDs returned \n" +
                "\u2022 Calls GET Financial Assistance {contractId} contract-info Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Financial Assistance {contractId} movements Endpoint \n" +
                "\u2022 Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.consentSyncTime",
                "consent.productType"
        }
)
public class OpinFvpFinancialAssistanceApiTestModule extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetFinancialAssistanceV1Endpoint.class));
    }
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddFinancialAssistanceScope.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.FINANCIAL_ASSISTANCE;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceFinancialAssistanceContractsOASValidatorV1n2.class;
    }

    @Override
    protected String getApi() {
        return "insurance-financial-assistance";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].contracts[0].contractId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "contract-info", GetInsuranceFinancialAssistanceContractInfoOASValidatorV1n2.class,
                "movements", GetInsuranceFinancialAssistanceMovementsOASValidatorV1n2.class
        );
    }
}
