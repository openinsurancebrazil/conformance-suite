package net.openid.conformance.opin.testmodule.v1.auto.v1n4;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AllIdsSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.auto.AbstractOpinAutoResourcesApiTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-auto-resources-api-test-v1.4.0",
	displayName = "Makes sure that the Resource API and the auto API are returning the same available IDs",
	summary = "Makes sure that the Resource API and the auto API are returning the same available IDs\n" +
			"\u2022 Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_AUTO_READ”, “DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ”,  “RESOURCES_READ”)\n" +
			"\u2022 Expects the server to create the consent with 201\n" +
			"\u2022 Redirect the user to authorize at the financial institution\n" +
			"\u2022 Call the auto “/” API\n" +
			"\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the policy id provided by this API\n" +
			"\u2022 Call the resources API\n" +
			"\u2022 Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
public class OpinAutoResourcesApiTestModuleV1n4 extends AbstractOpinAutoResourcesApiTestModule {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsuranceAutoListOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*].policyId");
		return AllIdsSelectorFromJsonPath.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}
