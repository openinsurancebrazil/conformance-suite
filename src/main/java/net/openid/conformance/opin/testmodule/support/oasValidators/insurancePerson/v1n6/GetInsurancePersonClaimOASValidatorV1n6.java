package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.AbstractGetInsurancePersonClaimOASValidator;

@ApiName("Insurance Person 1.6.0")
public class GetInsurancePersonClaimOASValidatorV1n6 extends AbstractGetInsurancePersonClaimOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insurancePerson/v1/swagger-insurance-person-1.6.0.yaml";
	}
}
