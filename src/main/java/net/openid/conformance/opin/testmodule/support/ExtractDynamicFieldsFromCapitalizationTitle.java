package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class ExtractDynamicFieldsFromCapitalizationTitle extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(strings = "modality")
    @PostEnvironment(required = "dynamic_field")
    public Environment evaluate(Environment env) {
        JsonObject responseBody;
        try {
            responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }

        String modality = env.getString("modality");
        JsonArray dynamicFields = new JsonArray();
        JsonArray dataArray = responseBody.getAsJsonArray("data");

        for (JsonElement dataElement : dataArray) {
            JsonObject data = dataElement.getAsJsonObject();
            if (modality.equals(OIDFJSON.getString(data.get("modality")))) {
                JsonArray fields = data.getAsJsonArray("fields");
                    dynamicFields.addAll(fields);
            }
        }

        if (dynamicFields.isEmpty()) {
            throw error("The response doesn't contain any dynamic field matching the required modality", args(
                    "modality", modality,
                    "data", dataArray
            ));
        }

        logSuccess("Found dynamic fields", args(
                "dynamic_fields", dynamicFields
        ));
        JsonObject dynamicField = new JsonObject();
        dynamicField.add("dynamic_fields", dynamicFields);
        env.putObject("dynamic_field", dynamicField);

        return env;
    }
}
