package net.openid.conformance.opin.testmodule.support.oasValidators.assistanceGeneralAssets.v1n3;


import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Assistance General Assets 1.3.0")
public class GetAssistanceGeneralAssetsOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/assistanceGeneralAssets/v1n3/swagger-assistance-general-assets-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/assistance-general-assets";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}