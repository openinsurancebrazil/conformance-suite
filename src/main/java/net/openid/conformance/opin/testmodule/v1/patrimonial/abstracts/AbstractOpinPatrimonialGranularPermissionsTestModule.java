package net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;

import java.util.Map;

public abstract class AbstractOpinPatrimonialGranularPermissionsTestModule extends AbstractOpinPatrimonialApiTestModule {

    @Override
    protected String[] getIndividualPermissionsToBeRemoved() {
        return new String[]{"DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ\",\"DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ"};
    }

    @Override
    protected Class<? extends Condition> getExpectedResponseCode(String endpoint) {
        Map<String, Class<? extends Condition>> endpointToStatusCodeMap = Map.of(
                "policy-info", EnsureResourceResponseCodeWas200.class,
                "premium", EnsureResourceResponseCodeWas403.class,
                "claim", EnsureResourceResponseCodeWas403.class
        );
        return endpointToStatusCodeMap.get(endpoint);
    }

    @Override
    protected boolean checkConsentAuthorizedStatusAfterRedirect() {
        return true;
    }
}
