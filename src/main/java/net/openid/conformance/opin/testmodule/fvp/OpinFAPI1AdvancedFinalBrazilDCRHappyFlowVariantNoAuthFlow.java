package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddSupportedOpenIdScopesToClientConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckScopesFromDynamicRegistrationEndpointDoNotExceedRequestedOpenBankingScopes;
import net.openid.conformance.opin.testmodule.support.CheckOpinDirectoryApiBase;
import net.openid.conformance.opin.testmodule.support.CheckOpinDirectoryDiscoveryUrl;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
		testName = "dcr_api_fvp-happy-path_test_module",
		displayName = "Opin-FAPI1-Advanced-Final: Brazil DCR happy flow variant without authentication flow",
		summary = "\u2022 Obtains a software statement from the Brazil directory (using the client MTLS certificate and directory client id provided in the test configuration).\n" +
				"\u2022 Registers a new client on the target authorization server.\n" +
				"\u2022 The registration request has the members of the 'grant_types' in a different order to the normal happy flow test, and includes the optional 'scope' parameter.",
		profile = OBBProfile.OPIN_PROFILE_PROD_FVP,
		configurationFields = {
				"server.discoveryUrl",
				"resource.brazilCpf",
				"resource.brazilCnpj"
		}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
		"resource.consentUrl","resource.brazilOrganizationId"
})
public class OpinFAPI1AdvancedFinalBrazilDCRHappyFlowVariantNoAuthFlow extends AbstractFAPI1AdvancedFinalBrazilDCR {

	@Override
	protected void call(ConditionCallBuilder builder) {
		if(FAPIBrazilOpenInsuranceCheckDirectoryDiscoveryUrl.class.equals(builder.getConditionClass())) {
			this.callAndContinueOnFailure(CheckOpinDirectoryDiscoveryUrl.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");
			return;
		}
		if(FAPIBrazilOpenInsuranceCheckDirectoryApiBase.class.equals(builder.getConditionClass())) {
			this.callAndContinueOnFailure(CheckOpinDirectoryApiBase.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");
			return;
		}
		super.call(builder);
	}

	@Override
	protected void callRegistrationEndpoint() {
		callAndStopOnFailure(AddSupportedOpenIdScopesToClientConfig.class);

		callAndStopOnFailure(ReorderGrantTypesInDynamicRegistrationRequest.class, "RFC7591-2");

		callAndStopOnFailure(AddScopeToDynamicRegistrationRequest.class, "RFC7591-2");

		call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));
		callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");
		validateDcrResponseScope();
		eventLog.endBlock();

		callAndContinueOnFailure(CheckScopesFromDynamicRegistrationEndpointDoNotExceedRequestedOpenBankingScopes.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-", "RFC7591-2", "RFC7591-3.2.1");
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);
		super.onPostAuthorizationFlowComplete();
	}

	@Override
	protected void setupResourceEndpoint() {
		// not needed as resource endpoint won't be called
	}
	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
