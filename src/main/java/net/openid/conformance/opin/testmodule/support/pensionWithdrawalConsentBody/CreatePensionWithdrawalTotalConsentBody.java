package net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreatePensionWithdrawalTotalConsentBody extends AbstractCreatePensionWithdrawalConsentBody {
    @Override
    protected void editData(Environment env, JsonObject data) {}
}
