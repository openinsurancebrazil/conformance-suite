package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Responsibility 1.3.0")
public class GetInsuranceResponsibilityClaimOASValidatorV1n3 extends OpenAPIJsonSchemaValidator{

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceResponsibility/v1n/swagger-insurance-responsibility.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-responsibility/{policyId}/claim";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
