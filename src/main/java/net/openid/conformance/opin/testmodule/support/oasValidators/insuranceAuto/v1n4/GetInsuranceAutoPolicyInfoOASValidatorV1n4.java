package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.AbstractGetInsuranceAutoPolicyInfoOASValidator;

@ApiName("Insurance Auto 1.4.0")
public class GetInsuranceAutoPolicyInfoOASValidatorV1n4 extends AbstractGetInsuranceAutoPolicyInfoOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceAuto/v1/swagger-insurance-auto-api-1.4.0.yaml";
	}
}
