package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForPensionWithdrawal;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.AbstractCreatePensionWithdrawalRequestBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalTotalPostRequestBody;

public abstract class AbstractOpinPhase3PensionWithdrawNegativeTestModule extends AbstractOpinPhase3PensionWithdrawTestModule {

    @Override
    protected void validateGetResourcesResponse() {
        //endpoint validated in other step
    }

    @Override
    protected void executeTest() {
        postPensionWithdrawal("422");
        validatePensionWithdrawalResponse();
        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
    }

    protected AbstractCreatePensionWithdrawalRequestBody createPensionWithdrawalRequestBody() {
        return new CreatePensionWithdrawalTotalPostRequestBody();
    }

    protected void postPensionWithdrawal(String status) {
        eventLog.startBlock(String.format("POST Pension Withdrawal - Expecting %s", status));
        callAndStopOnFailure(PrepareUrlForPensionWithdrawal.class);
        callAndStopOnFailure(createPensionWithdrawalRequestBody().getClass());
        editPensionWithdrawalRequestBody();
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        setIdempotencyKey();
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(AddJsonAcceptHeaderRequest.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
    }

    protected abstract void editPensionWithdrawalRequestBody();


    protected void validatePensionWithdrawalResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }
    protected void setIdempotencyKey() {
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
    }

    protected abstract AbstractJsonAssertingCondition validator();

}
