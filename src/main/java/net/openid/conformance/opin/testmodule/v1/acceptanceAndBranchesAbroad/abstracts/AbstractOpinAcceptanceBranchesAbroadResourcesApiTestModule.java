package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.abstracts;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;


public abstract class AbstractOpinAcceptanceBranchesAbroadResourcesApiTestModule extends AbstractOpinApiResourcesTestModuleV2 {

	@Override
	protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
		return AddAcceptanceBranchesAbroadScope.class;
	}

	@Override
	protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
		return BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected PermissionsGroup getPermissionsGroup() {
		return PermissionsGroup.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD;
	}

	@Override
	protected String getApi() {
		return "insurance-acceptance-and-branches-abroad";
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*]");
		return AllIdsSelectorFromJsonPath.class;
	}

	@Override
	protected String getResourceType() {
		return EnumOpinResourcesType.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD.name();
	}

	@Override
	protected String getResourceStatus() {
		return EnumResourcesStatus.AVAILABLE.name();
	}

}
