package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts;

import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyExclusao;

public abstract class AbstractEndorsementApiExclusaoTestModule extends AbstractEndorsementApiHappyTestModule {

    @Override
    protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
        return new CreateEndorsementRequestBodyExclusao();
    }

    @Override
    protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
        return CreateEndorsementConsentBodyExclusao.class;
    }
}