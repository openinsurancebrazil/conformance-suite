package net.openid.conformance.opin.testmodule.v1.endorsement;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.opin.testmodule.support.EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementRequestDescription;

public abstract class AbstractEndorsementIdempotencyTest extends AbstractEndorsementHappyPathTest {

    @Override
    protected void executeTest() {
        postEndorsement();
        validateEndorsementResponse();

        eventLog.startBlock("POST endorsement again with the same idempotency key - Expecting 201");
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
        validateEndorsementResponse();

        eventLog.startBlock("POST endorsement with a different payload and same idempotency key - Expecting 422");
        callAndStopOnFailure(SetEndorsementRequestDescription.class);
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
        callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject.class, Condition.ConditionResult.FAILURE);
        call(exec().unmapKey("endpoint_response"));
        eventLog.endBlock();
    }
}
