package net.openid.conformance.opin.testmodule.support.oasValidators.condominium.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Condominium 1.3.0")
public class GetCondominiumOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/condominium/v1n3/swagger-condominium-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/condominium/{commercializationArea}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}