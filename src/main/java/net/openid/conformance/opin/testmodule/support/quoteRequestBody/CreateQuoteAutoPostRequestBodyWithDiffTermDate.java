package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateQuoteAutoPostRequestBodyWithDiffTermDate extends AbstractCreateQuotePostRequestBody {

    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject quoteData = data.getAsJsonObject("quoteData");
        quoteData.remove("maxLMG");
        quoteData.remove("policyId");
        quoteData.remove("insurerId");
        quoteData.addProperty("termType", "ANUAL");

        quoteData.addProperty("termStartDate", "2024-08-21");
        quoteData.addProperty("termEndDate", "2024-08-20");
    }
}
