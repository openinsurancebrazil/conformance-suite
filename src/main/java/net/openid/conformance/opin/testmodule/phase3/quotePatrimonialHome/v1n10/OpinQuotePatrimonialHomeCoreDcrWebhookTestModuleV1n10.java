package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.AbstractOpinQuotePatrimonialHomeCoreWebhookTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.GetPatrimonialHomeQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PatchPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PostPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialHomePostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-home_api_core-webhook-dcr_test-module_v1n10",
        displayName = "Ensure that a Patrimonial Home quotation request can be successfully created and accepted afterwards, and webhook is sent as required.",
        summary = "Ensure that a Patrimonial Home quotation request can be successfully created and accepted afterwards, and webhook is sent as required. This test applies to both Personal and Business products. If the \"brazilCnpj\" field is filled out, it will transmit business information; otherwise, personal information will be used.\n" +
                "For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>.\n" +
                "\u2022 Obtain a SSA from the Directory\n" +
                "\u2022 Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration\n" +
                "\u2022 Call the Registration Endpoint, also sending the field \"webhook_uris\":[\"https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>\"]\n" +
                "\u2022 Expect a 201 - Validate Response\n" +
                "\u2022 Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
                "\u2022 Set the consents webhook notification endpoint to be equal to https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>/open-insurance/webhook/v1/quote/v1/request/{consentId}/quote-status, where the alias is to be obtained from the field alias on the test configuration\n" +
                "\u2022 Call POST home/request endpoint sending personal or business information, following what is defined at the config, and the consentId as the webhook defined on the test\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Wait for webhook incoming call until 1 minute and fail the test if not received\n" +
                "\u2022 Respond 202 with an empty body, validate if timestamp is after the POST home/request call\n" +
                "\u2022 Call GET home/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respones and ensure status is ACPT\n" +
                "\u2022 Call PATCH home/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN\n" +
                "\u2022 Expect 200 - Validate Response and ensure status is ACKN\n" +
                "\u2022 Call GET links.redirect endpoint\n" +
                "\u2022 Expect 200\n" +
                "\u2022 Call the Delete Registration Endpoint\n" +
                "\u2022 Expect a 204 - No Content\n",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "mtls.ca",
                "directory.client_id",
                "directory.discoveryUrl",
                "directory.apibase",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialHomeCoreDcrWebhookTestModuleV1n10 extends AbstractOpinQuotePatrimonialHomeCoreWebhookTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialHomeQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialHomePostRequestBodyWithCoverage.class;
    }
}
