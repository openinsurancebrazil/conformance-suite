package net.openid.conformance.opin.testmodule.support;

public enum EnumWithdrawalType {
	TOTAL("1_TOTAL"),
	PARCIAL("2_PARCIAL");

	private final String value;
	EnumWithdrawalType(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return String.valueOf(this.value);
	}

	public static EnumWithdrawalType findByValue(String value){
		for(EnumWithdrawalType v : values()){
			if(v.toString().equals(value)){
				return v;
			}
		}
		throw new IllegalArgumentException("No EnumWithdrawalType with value:"+ value);
	}
}
