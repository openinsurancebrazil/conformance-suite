package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;

public class CreateClaimNotificationRequestBodyDiff extends AbstractCreateClaimNotificationRequestBody {

    @Override
    protected String getPolicyFieldName() {
        return "policyId";
    }

    @Override
    protected JsonArray getInsuredObjectId(){
        JsonArray insuredObjectIds = new JsonArray();
        insuredObjectIds.add("216731531723");
        return insuredObjectIds;
    }

    @Override
    protected String getProposalId() {
        return "987";
    }
}
