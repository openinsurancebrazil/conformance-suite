package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuoteAutoLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "CASCO_COMPREENSIVA";
    }
}