package net.openid.conformance.opin.testmodule.fvp.quote;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.fvp.quote.abstracts.AbstractOpinFvpQuoteNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.GetQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteAutoPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
        testName = "opin-fpv-quote-auto_api_negative-quote_test-module",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = """
            Ensure a consent cannot be created in unhappy requests.
            • Call POST Auto request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload
            • Expect 422 - Validate Error Response
            • Call POST Auto request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with "quote-auto-lead" scope
            • Expect 403 - Validate Error Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)

public class OpinFvpQuoteAutoApiNegativeQuoteTestModule extends AbstractOpinFvpQuoteNegativeTestModule {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return GetQuoteAutoOASValidatorV1n9.class;
    }

    @Override
    protected OPINScopesEnum getLeadScope() {
        return OPINScopesEnum.QUOTE_AUTO_LEAD;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_AUTO;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-auto";
    }

    @Override
    protected String getApiName() {
        return "quote-auto";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteAutoPostRequestBody.class;
    }
}
