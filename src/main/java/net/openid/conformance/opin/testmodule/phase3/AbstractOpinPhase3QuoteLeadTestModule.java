package net.openid.conformance.opin.testmodule.phase3;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToPatch;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.opin.testmodule.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.CreateRandomConsentID;
import net.openid.conformance.opin.testmodule.support.EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasCanc;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.directory.GetQuoteResourceV1Endpoint;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteLeadPatchRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteLeadPostRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.EditQuoteLeadPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantConfigurationFields;

/**
 * This class is intended to test the creation and patching of lead quotes.
 * It will post to POST /lead/request with a random consent ID and, after, patch it with PATCH /lead/request/{consentId}.
 */

@VariantConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "resource.brazilOrganizationId"
})
public abstract class AbstractOpinPhase3QuoteLeadTestModule extends OpinAbstractClientCredentialsGrantFunctionalTestModule {

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        super.preConfigure(config, baseUrl, externalUrlOverride);
        env.putObject("client", config.getAsJsonObject("client"));
        env.putString("api_type", API_TYPE);
        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
        call(new ValidateWellKnownUriSteps());
        callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
        callAndStopOnFailure(CreateRandomConsentID.class);
        env.putString("api_family_type", getApiFamilyType());
        env.putString("api_name", getApiName());
        env.putString("api_endpoint", "lead/request");
        callAndStopOnFailure(GetQuoteResourceV1Endpoint.class);
        ScopesAndPermissionsBuilder builder = new ScopesAndPermissionsBuilder(env, eventLog);
        builder.addScopes(getScope());
        builder.build();
    }

    @Override
    protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
        return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence).skip(
                SetConsentsScopeOnTokenEndpointRequest.class,
                "the consents scope is not needed"
        );
    }

    @Override
    protected void runTests() {
        createLead();

        if (testIdempotencyLead()) {
            idempotencyLead();
        }

        patchLead();
    }

    protected boolean testIdempotencyLead() {
        return false;
    }

    protected void idempotencyLead() {
        runInBlock("Create a lead with the same payload and idempotency id", () -> {
            call(addIdempotencyKey(setHeadersSequence(), false));
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });

        runInBlock("Validate the lead creation response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas201.class);
            callAndContinueOnFailure(validatePostQuoteLeadResponse(), Condition.ConditionResult.FAILURE);
        });

        runInBlock("Create a lead with a different payload as the previous request but the same idempotency id", () -> {
            callAndStopOnFailure(editQuoteLeadPostRequestBody());
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });

        runInBlock("Validate the lead creation response expecting 422 - ERRO_IDEMPOTENCIA", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas422.class);
            env.mapKey("endpoint_response", "resource_endpoint_response_full");
            callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject.class, Condition.ConditionResult.FAILURE);
            env.unmapKey("endpoint_response");
            callAndContinueOnFailure(validatePostQuoteLeadResponse(), Condition.ConditionResult.FAILURE);
        });

    }

    protected void createLead() {
        runInBlock("Create a lead", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            callAndStopOnFailure(createPostQuoteRequestBody());
            call(addIdempotencyKey(setHeadersSequence(), true));
            callAndStopOnFailure(SetResourceMethodToPost.class);
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the lead creation response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas201.class);
            callAndContinueOnFailure(EnsureStatusWasRcvd.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validatePostQuoteLeadResponse(), Condition.ConditionResult.FAILURE);
        });
    }

    protected void patchLead() {
        runInBlock("Patch the lead", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            env.putString("protected_resource_url", env.getString("protected_resource_url") + "/" + env.getString("consent_id"));
            callAndStopOnFailure(CreateQuoteLeadPatchRequestBody.class);
            call(setHeadersSequence());
            callAndStopOnFailure(SetResourceMethodToPatch.class);
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the lead patching response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
            callAndContinueOnFailure(EnsureStatusWasCanc.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validatePatchQuoteLeadResponse(), Condition.ConditionResult.FAILURE);
        });
    }

    private ConditionSequence addIdempotencyKey(ConditionSequence sequence, boolean isUnique) {
        if (!testIdempotencyLead()) {
            return sequence;
        }

        if (isUnique) {
            sequence.then(condition(CreateIdempotencyKey.class));
        }

        sequence.then(condition(AddIdempotencyKeyHeader.class));
        return sequence;
    }

    protected ConditionSequence setHeadersSequence() {
        return sequenceOf(
                condition(CreateEmptyResourceEndpointRequestHeaders.class),
                condition(AddJsonAcceptHeaderRequest.class),
                condition(SetContentTypeApplicationJson.class),
                condition(AddFAPIAuthDateToResourceEndpointRequest.class),
                condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
                condition(CreateRandomFAPIInteractionId.class),
                condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
        );
    }

    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteLeadPostRequestBody.class;
    }

    protected Class<? extends AbstractCondition> editQuoteLeadPostRequestBody() {
        return EditQuoteLeadPostRequestBody.class;
    }

    protected abstract OPINScopesEnum getScope();

    protected abstract String getApiFamilyType();

    protected abstract String getApiName();

    protected abstract Class<? extends AbstractCondition> validatePostQuoteLeadResponse();

    protected abstract Class<? extends AbstractCondition> validatePatchQuoteLeadResponse();

}
