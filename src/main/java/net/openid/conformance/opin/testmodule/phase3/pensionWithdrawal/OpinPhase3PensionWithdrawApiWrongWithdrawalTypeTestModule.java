package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawal.v1n3.PostPensionWithdrawalOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.AbstractCreatePensionWithdrawalConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.CreatePensionWithdrawalTotalConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.AbstractCreatePensionWithdrawalRequestBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalParcialPostRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-pension-withdraw_api_wrong-withdrawalType_test-module_v1",
        displayName = "Validates the structure of Pension Withdrawal API",
        summary = """
                Ensure a life pension withdrawal request cannot be successfully executed with a mismatching withdrawalType\s

                · Execute a Customer Data Sharing Journey for the Life Pension Product, obtaining the first productName and certificateId from the insurance-life-pension/contracts endpoint, and the corresponding data.suseps.FIE.pmbacAmount from the contract-info endpoint
                · Call the POST Consents with PENSION_WITHDRAWAL_CREATE permission, sending the withdrawalLifePensionInformation with the pre-saved information, withdrawalType as 1_TOTAL, and not sending the desiredTotalAmount field
                · Expect a 201 - Validate the response
                · Redirect the User to Authorize the Consent
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response and ensure the status is "AUTHORIZED"
                · Call the POST pension/request endpoint, sending the  withdrawalType as 2_PARCIAL, and  the desiredTotalAmount field as half of the value of pmbacAmount
                · Expect a 422- Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3PensionWithdrawApiWrongWithdrawalTypeTestModule extends AbstractOpinPhase3PensionWithdrawNegativeTestModule {

    @Override
    protected void configureClient() {
        super.configureClient();
        env.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());
    }

    @Override
    protected void editPensionWithdrawalRequestBody() {
        env.putString("withdrawalType", EnumWithdrawalType.PARCIAL.toString());
    }

    @Override
    protected AbstractCreatePensionWithdrawalRequestBody createPensionWithdrawalRequestBody() {
        return new CreatePensionWithdrawalParcialPostRequestBody();
    }

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostPensionWithdrawalOASValidatorV1n3();
    }

    @Override
    protected AbstractCreatePensionWithdrawalConsentBody createPensionWithdrawalConsentBody() {
        return new CreatePensionWithdrawalTotalConsentBody();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }
}
