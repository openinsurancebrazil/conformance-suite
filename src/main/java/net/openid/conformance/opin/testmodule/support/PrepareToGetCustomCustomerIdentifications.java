package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Objects;

public class PrepareToGetCustomCustomerIdentifications extends AbstractPrepareUrlForApi {

	private String productType;

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		productType = env.getString("config", "consent.productType");
		if(!Objects.equals(productType, "personal") && !Objects.equals(productType, "business")) {
			throw error("productType is not valid, it must be either personal or business", args("productType", productType));
		}
		return super.evaluate(env);
	}

	@Override
	protected String getApiReplacement() {
		return "customers";
	}

	@Override
	protected String getEndpointReplacement() {
		return String.format("%s/identifications", productType);
	}
}
