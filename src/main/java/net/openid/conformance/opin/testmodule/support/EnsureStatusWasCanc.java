package net.openid.conformance.opin.testmodule.support;

import java.util.List;

public class EnsureStatusWasCanc extends AbstractEnsureStatusWasX {
    @Override
    protected List<String> getStatuses() {
        return List.of("CANC");
    }
}
