package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractGetPersonTravelQuoteStatusOASValidator;

@ApiName("Quote Person 1.10.2")
public class GetPersonTravelQuoteStatusOASValidatorV1n10 extends AbstractGetPersonTravelQuoteStatusOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.10.2.yaml";
    }

}