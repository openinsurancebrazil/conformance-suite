package net.openid.conformance.opin.testmodule.phase3.quotePersonLife.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonLife.AbstractOpinQuotePersonLifeCoreCancTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life.GetPersonLifeQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life.PatchPersonLifeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life.PostPersonLifeOASValidatorV1n10;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-life_api_core-canc_test-module_v1",
        displayName = "Ensure that a person life quotation request can be successfully created and rejected afterwards.",
        summary = """
            Ensure that a person life quotation request can be successfully created and rejected afterwards. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            • Call POST life/request endpoint sending personal or business information, following what is defined at the config
            • Expect 201 - Validate Response and ensure status is RCVD
            • Poll the GET life/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET life/request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Respones and ensure status is ACPT
            • Call PATCH life/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC
            • Expect 200 - Validate Response and ensure status is CANC
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonLifeCoreCancTestModule extends AbstractOpinQuotePersonLifeCoreCancTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPersonLifeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPersonLifeQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPersonLifeOASValidatorV1n10.class;
    }
}