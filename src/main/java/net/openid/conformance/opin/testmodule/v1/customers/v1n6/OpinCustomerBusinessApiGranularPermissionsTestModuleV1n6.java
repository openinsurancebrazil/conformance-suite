package net.openid.conformance.opin.testmodule.v1.customers.v1n6;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6.GetCustomersBusinessComplimentaryInformationListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6.GetCustomersBusinessIdentificationListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6.GetCustomersBusinessQualificationListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.v1.customers.AbstractOpinCustomerBusinessApiGranularPermissionsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-customer-business-api-granular-permissions-test-v1n6",
	displayName = "Validate structure of all business customer data API resources V1n6",
	summary = "Ensure specific methods can only be called if the respective permissions is granted at the customer business api.\n" +
			"\u2022 Call the POST Consents with the Customer business IDENTIFICATIONS and QUALIFICATION permissions, and RESOURCES_READ\n" +
			"\u2022 Expects 201 - Validate response\n" +
			"\u2022 Redirects the user to Authorize consent\n" +
			"\u2022 Call the GET  Customer business identifications endpoint\n" +
			"\u2022 Expects 200 - Validate Response\n" +
			"\u2022 Call the GET Customer business qualifications endpoint\n" +
			"\u2022 Expects 200 - Validate Response\n" +
			"\u2022 Call the GET Customer business complimentary-information endpoint\n" +
			"\u2022 Expects a 403 response  - Validate error response ",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks","consent.productType"
})
public class OpinCustomerBusinessApiGranularPermissionsTestModuleV1n6 extends AbstractOpinCustomerBusinessApiGranularPermissionsTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessIdentificationValidator(){
		return GetCustomersBusinessIdentificationListOASValidatorV1n6.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessQualificationValidator(){
		return GetCustomersBusinessQualificationListOASValidatorV1n6.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessComplimentaryInfoValidator(){
		return GetCustomersBusinessComplimentaryInformationListOASValidatorV1n6.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}

