package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.pensionWithdrawalLead;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.AbstractOpinPhase3PensionWithdrawNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawalLead.v1n3.PostPensionWithdrawalLeadOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.CreatePensionWithdrawalTotalConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.CreatePensionWithdrawalTotalDoubledConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.CreatePensionWithdrawalTotalHalvedConsentBody;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-pension-withdraw-lead_api_negative-consents_test-module_v1",
        displayName = "Validates the structure of Pension Withdrawal Lead API",
        summary = """
                Ensure a consent for withdraw cannot be created  if the appropriate rules are not followed
                
                · Execute a Customer Data Sharing Journey for the Life Pension Product, obtaining the first productName and certificateId from the insurance-life-pension/contracts endpoint, and the corresponding data.suseps.FIE.pmbacAmount from the contract-info endpoint
                · Call the POST Consents Endpoints with Phase 2 and PENSION_WITHDRAWAL_LEAD_CREATE permissions, sending the withdrawalLifePensionInformation with the pre-saved information
                · Expect 422 - Validate Response
                · Call the POST Consents Endpoints with PENSION_WITHDRAWAL_LEAD_CREATE permission, but without the withdrawalLifePensionInformation field
                · Expect 400 or 422 - Validate Response
                · Call the POST Consents Endpoints with PENSION_WITHDRAWAL_LEAD_CREATE and CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permission, sending the withdrawalLifePensionInformation with the pre-saved information
                · Expect 422 - Validate Response
                · Call the POST Consents with PENSION_WITHDRAWAL_LEAD_CREATE permission, sending the withdrawalLifePensionInformation with the pre-saved information, withdrawalType as 2_PARCIAL, and  the desiredTotalAmount field as the double of the value of pmbacAmount
                · Expect 422 - Validate Response
                · Call the POST Consents with PENSION_WITHDRAWAL_LEAD_CREATE permission, sending the withdrawalLifePensionInformation with the pre-saved information, withdrawalType as 1_TOTAL, and  the desiredTotalAmount field as the half of the value of pmbacAmount
                · Expect 422 - Validate Response
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinPhase3PensionWithdrawLeadApiNegativeConsentsTestModule extends AbstractOpinPhase3PensionWithdrawLeadNegativeTestModule {
    @Override
    protected void editPensionWithdrawalRequestBody() {}

    @Override
    protected void setupSecondFlow() {
        performTestSteps();
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
        setupSecondFlow();
        fireTestFinished();
    }

    @Override
    protected void executeTest() {
        //not needed in this test
    }

    protected void performTestSteps() {
        env.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());
        runInBlock("POST Consents Endpoints with Phase 2 and PENSION_WITHDRAWAL_LEAD_CREATE permissions and pre-saved withdrawalLifePensionInformation - Expects 422", () ->{
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
            permissionsBuilder.resetPermissions().buildFromEnv();
            permissionsBuilder.addPermissionsGroup(PermissionsGroup.PENSION_WITHDRAWAL_LEAD).build();

            call(postConsentSequence()
                    .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                            condition(CreatePensionWithdrawalTotalConsentBody.class)));
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("POST Consents Endpoints with PENSION_WITHDRAWAL_LEAD_CREATE permissions and no withdrawalLifePensionInformation - Expects 400 or 422", () -> {
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.PENSION_WITHDRAWAL_LEAD).build();

            call(postConsentSequence());
            callAndContinueOnFailure(EnsureConsentResponseCodeWas400Or422.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("POST Consents Endpoints with PENSION_WITHDRAWAL_LEAD_CREATE and CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permissions and pre-saved withdrawalLifePensionInformation - Expects 422", () -> {
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CAPITALIZATION_TITLE_WITHDRAWAL).addPermissionsGroup(PermissionsGroup.PENSION_WITHDRAWAL_LEAD).build();
            call(postConsentSequence()
                    .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                            condition(CreatePensionWithdrawalTotalConsentBody.class)));
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("POST Consents Endpoints with PENSION_WITHDRAWAL_LEAD_CREATE permissions and pre-saved withdrawalLifePensionInformation, withdrawalType as 2_PARCIAL, desiredTotalAmount doubled - Expects 422", () -> {
            env.putString("withdrawalType", EnumWithdrawalType.PARCIAL.toString());
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.PENSION_WITHDRAWAL_LEAD).build();
            call(postConsentSequence()
                    .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                            condition(CreatePensionWithdrawalTotalDoubledConsentBody.class)));
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("POST Consents Endpoints with PENSION_WITHDRAWAL_LEAD_CREATE permissions and pre-saved withdrawalLifePensionInformation, withdrawalType as 1_TOTAL, desiredTotalAmount halfed - Expects 422", () -> {
            env.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.PENSION_WITHDRAWAL_LEAD).build();
            call(postConsentSequence()
                    .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                            condition(CreatePensionWithdrawalTotalHalvedConsentBody.class)));
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        });

    }

    protected ConditionSequence postConsentSequence() {
        return new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication, false)
                .insertAfter(CallConsentEndpointWithBearerTokenAnyHttpMethod.class,
                        sequenceOf(
                                condition(postConsentValidator().getClass()).dontStopOnFailure(),
                                condition(SaveConsentsAccessToken.class))
                );
    }
    }
