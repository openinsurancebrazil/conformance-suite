package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadRequestBodyAsString extends AbstractCondition {
    @Override
    @PreEnvironment(required = "resource_request_entity")
    @PostEnvironment(strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        env.putString("resource_request_entity", env.getObject("resource_request_entity").toString());
        return env;
    }
}
