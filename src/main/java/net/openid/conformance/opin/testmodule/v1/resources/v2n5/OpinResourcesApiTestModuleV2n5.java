package net.openid.conformance.opin.testmodule.v1.resources.v2n5;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n5.GetResourcesOASValidatorV2n5;
import net.openid.conformance.opin.testmodule.v1.resources.AbstractOpinResourcesApiTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
	testName = "opin-resources-api-test-v2n5",
	displayName = "Validate structure of all resources API resources",
	summary = "Validates the structure of all resources API resources\n" +
		"• Creates a Consent will all of the existing permissions \n" +
		"• Checks all of the fields sent on the consent API are specification compliant\n" +
		"• Calls the GET resources API\n" +
		"• Expects a 200 - Validate Reponse",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks", "directory.keystore"
})
public class OpinResourcesApiTestModuleV2n5 extends AbstractOpinResourcesApiTest {
	
	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getResourceValidator() {
		return GetResourcesOASValidatorV2n5.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}
