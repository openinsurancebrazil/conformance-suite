package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CreateContractLifePensionRJCTPostRequestBody extends AbstractCreateQuotePostRequestBody {

        @Override
        protected void editData(Environment env, JsonObject data) {
            this.addQuoteData(env, data);
        }

        private void addQuoteData(Environment env, JsonObject data) {
            JsonObject complementaryIdentification = new JsonObject();
            complementaryIdentification.addProperty("isNewPlanHolder", false);

            JsonObject investorProfile = new JsonObject();
            investorProfile.addProperty("isQualifiedInvestor", false);

            JsonObject initialContribution = new JsonObject();
            initialContribution.addProperty("amount", "1000.00");
            initialContribution.addProperty("unitType", "PORCENTAGEM");

            JsonObject product = new JsonObject();
            product.add("initialContribution", initialContribution);
            product.addProperty("isContributeMonthly", false);
            product.addProperty("planType", "PGBL");

            JsonArray products = new JsonArray();
            products.add(product);

            JsonObject quoteData = new JsonObject();

            quoteData.addProperty("pensionRiskCoverage", false);
            quoteData.add("complementaryIdentification", complementaryIdentification);
            quoteData.add("products", products);
            quoteData.add("investorProfile", investorProfile);

            data.add("quoteData", quoteData);
        }
}

