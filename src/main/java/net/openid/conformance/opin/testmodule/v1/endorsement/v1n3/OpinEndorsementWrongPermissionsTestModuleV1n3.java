package net.openid.conformance.opin.testmodule.v1.endorsement.v1n3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n.AbstractOpinEndorsementWrongPermissionsTestV1n;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-endorsement-api-wrong-permissions-test-v1n3",
        displayName = "Ensure the endorsement API cannot be accessed without needed permissions",
        summary = "Ensure the endorsement API cannot be accessed without needed permissions\n" +
        "• Call the POST Consents API with all the existing Phase 2 permissions \n" +
        "• Expect 201 a with status on \"AWAITING_AUTHORISATION\"  - Validate Response\n" +
        "• Redirect the user to authorize consent\n" +
        "• Call the GET Consents Endpoint\n" +
        "• Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "• Call the resources API\n" +
        "• Extract a shared policy ID on the resources APIs\n" +
        "• Call the POST Endorsement Endpoint with pre-defined Payload - " +
        "• Change endorsementType for EXCLUSAO, set policy ID for the Extracted policyID. The token obtained after the redirect is to be used here\n" +
        "• Expects 403 - Validate Error response\n" +
        "• Call the POST Consents Endpoint with Claim Notification permissions, set policy ID for the Extracted policyID\n" +
        "• Expects 201 - Validate Response\n" +
        "• Redirect the user to authorize consent\n" +
        "• Call the GET Consents Endpoint\n" +
        "• Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "• Call the POST Endorsement Endpoint\n" +
        "• Expects 403 - Validate Error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf"
        }
)
public class OpinEndorsementWrongPermissionsTestModuleV1n3 extends AbstractOpinEndorsementWrongPermissionsTestV1n {

}
