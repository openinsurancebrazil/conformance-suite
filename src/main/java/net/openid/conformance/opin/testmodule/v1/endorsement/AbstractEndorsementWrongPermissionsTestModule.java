package net.openid.conformance.opin.testmodule.v1.endorsement;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.opin.testmodule.support.AbstractAddClaimNotificationToConsentData;
import net.openid.conformance.opin.testmodule.support.AddClaimNotificationScope;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractEndorsementWrongPermissionsTestModule extends AbstractEndorsementNegativeTest {

    protected abstract AbstractAddClaimNotificationToConsentData setClaimNotificationData();

    @Override
    protected void executeTest() {
        eventLog.startBlock("POST endorsement - Expecting 403");
        postEndorsement();
        validateEndorsementResponse();
    }

    @Override
    protected void validateEndorsementResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

    protected void editEndorsementRequestBody() {
        //
    }

    @Override
    protected void requestProtectedResource() {
        if (isPolicyIdExtractionFlow) {
            preCallProtectedResource("GET resources API to extract a policy ID");
            validateGetResourcesResponse();
            executeTest();
        } else {
            executeTest();
        }
    }


    @Override
    protected void setupSecondFlow() {
        runInBlock("Creating consent with Claim Notification permissions", () -> {
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.addPermissionsGroup(PermissionsGroup.CLAIM_NOTIFICATION_REQUEST);
            permissionsBuilder.removePermission("CLAIM_NOTIFICATION_REQUEST_DAMAGE_CREATE");
            permissionsBuilder.build();
            callAndStopOnFailure(SetScope.class);
            callAndStopOnFailure(AddClaimNotificationScope.class);
        });
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        if(!isPolicyIdExtractionFlow) {
            env.putString("documentType", "APOLICE_INDIVIDUAL");
            return super.createOBBPreauthSteps().insertAfter(
                    FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                    condition(setClaimNotificationData().getClass()).skipIfStringMissing("policyId")
            );
        }
        return super.createOBBPreauthSteps();
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
        if (isPolicyIdExtractionFlow) {
            isPolicyIdExtractionFlow = false;
            setupSecondFlow();
            performAuthorizationFlow();
        } else {
            super.onPostAuthorizationFlowComplete();
        }
    }
}