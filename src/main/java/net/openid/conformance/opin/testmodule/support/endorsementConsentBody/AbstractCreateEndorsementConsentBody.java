package net.openid.conformance.opin.testmodule.support.endorsementConsentBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.OPINPhase3Keys;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.EndorsementType;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateEndorsementConsentBody extends AbstractCondition {

    @Override
    @PreEnvironment(required = "consent_endpoint_request", strings = "policyId")
    public Environment evaluate(Environment env) {
        String policyId = env.getString("policyId");
        JsonObject endorsementData = new JsonObject();
        JsonObject consentData = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();

        endorsementData.addProperty(getPolicyFieldName(), policyId);
        endorsementData.addProperty("endorsementType", endorsementType().name());
        endorsementData.addProperty("requestDescription", OPINPhase3Keys.Keys_requestDescription);

        JsonArray insuredObjectIds = getInsuredObjectId();
        if (insuredObjectIds != null) {
            endorsementData.add("insuredObjectId", insuredObjectIds);
        }

        consentData.add("endorsementInformation", endorsementData);

        return env;
    }

    protected String getPolicyFieldName(){
        return "policyNumber";
    }

    protected JsonArray getInsuredObjectId() {
        return null;
    }

    protected abstract EndorsementType endorsementType();
}
