package net.openid.conformance.opin.testmodule.phase3.quoteAuto;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n8.GetQuoteAutoOASValidatorV1n8;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n8.PatchQuoteAutoOASValidatorV1n8;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n8.PostQuoteAutoOASValidatorV1n8;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-auto_api_conditional-dynamic-fields_test-module_v1",
        displayName = "Ensure that a Patrimonial Auto quotation request can be successfully created and accepted afterwards using dynamic field.",
        summary = """
                Ensure that a Patrimonial Auto quotation request can be successfully created and accepted afterwards using dynamic field.
                This test is conditional and will only be executed if the fields "Conditional - CPF for Dynamic fields" or "Conditional - CNPJ for Dynamic Fields" are filled. This test applies to both Personal and Business products, depending on the config.
                · Call GET /damage-and-person Dynamic Fields endpoint
                · Expect 200 - Validate response and validade if api is QUOTE_AUTO
                · Call POST auto request endpoint sending personal or business information, following what is defined at the config, and the dynamicFields at quoteCustomData.generalQuoteInfo
                · Expect 201 - Validate Response and ensure status is RCVD
                · Poll the GET auto quote-status endpoint while status is RCVD or EVAL
                · Call GET auto quote-status endpoint
                · Expect 200 - Validate Respones and ensure status is ACPT and that quoteCustomData is sent back
                · Call PATCH auto request endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
                · Expect 200 - Validate Response and ensure status is ACKN
                · Call GET links.redirect endpoint
                · Expect 200
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteAutoApiConditionalDynamicFieldsTestModuleV1 extends AbstractOpinQuoteAutoApiConditionalDynamicFieldsTestModule {

        @Override
        protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
                return PostQuoteAutoOASValidatorV1n8.class;
        }


        @Override
        protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
                return GetQuoteAutoOASValidatorV1n8.class;
        }


        @Override
        protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
                return PatchQuoteAutoOASValidatorV1n8.class;
        }

}
