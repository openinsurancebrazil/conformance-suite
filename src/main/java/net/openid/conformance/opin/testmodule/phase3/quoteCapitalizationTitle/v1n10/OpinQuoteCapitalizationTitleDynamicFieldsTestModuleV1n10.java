package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleDynamicFieldsTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.capitalizationTitle.v1n4.GetDynamicFieldsCapitalizationTitleOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10.GetQuoteCapitalizationTitleOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10.PatchQuoteCapitalizationTitleLeadOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10.PostQuoteCapitalizationTitleOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPaymentWithUnitType;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title_api_conditional-dynamic-fields_test-module_v1.10.0",
        displayName = "opin-quote-capitalization-title_api_conditional-dynamic-fields_test-module_v1",
        summary = """
                Ensure that a Capitalization Title quotation request can be successfully created and accepted afterwards using dynamic field. This test is conditional and will only be executed if the fields "Conditional - CPF for Dynamic fields" or "Conditional - CNPJ for Dynamic Fields" are filled. This test applies to both Personal and Business products, depending on the config.\s
                • Call GET /capitalization-title Dynamic Fields endpoint\s
                • Expect 200 - Validate Response and extract the modality from the first dynamic fields object returned along with its associated dynamic fields.
                • Call POST request endpoint sending personal or business information, following what is defined at the config, and the modality as selected at the dynamicFields Endpoint
                • Expect 201 - Validate Response and ensure status is RCVD
                • Poll the GET request/{consentId}/quote-status  endpoint while status is RCVD or EVAL for 1 minute, and fail the test if the quote status is not conclusive
                • Call GET request/{consentId}/quote-status endpoint
                • Expect 200 - Validate Respones and ensure status is ACPT and that quoteCustomData is sent back
                • Call PATCH request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
                • Expect 200 - Validate Response and ensure status is ACKN
                • Call GET links.redirect endpoint
                • Expect 200
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "resource.brazilCpfDynamicFields",
                "resource.brazilCnpjDynamicFields",
                "consent.productType"
        }
)
public class OpinQuoteCapitalizationTitleDynamicFieldsTestModuleV1n10 extends AbstractOpinQuoteCapitalizationTitleDynamicFieldsTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostQuoteCapitalizationTitleOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetQuoteCapitalizationTitleOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchQuoteCapitalizationTitleLeadOASValidatorV1n10.class;
    }

    @Override
    protected void getDynamicFieldsValidator() {
        callAndContinueOnFailure(GetDynamicFieldsCapitalizationTitleOASValidatorV1n4.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPaymentWithUnitType.class;
    }
}