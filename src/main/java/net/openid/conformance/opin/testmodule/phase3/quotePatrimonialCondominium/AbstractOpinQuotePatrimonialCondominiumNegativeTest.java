package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialCondominiumPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePatrimonialCondominiumNegativeTest extends AbstractOpinPhase3QuoteNegativeTestModule {

    @Override
    protected OPINScopesEnum getLeadScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_LEAD;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_CONDOMINIUM;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-patrimonial-condominium";
    }

    @Override
    protected String getApiName() {
        return "quote-patrimonial";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "condominium/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialCondominiumPostRequestBody.class;
    }
}
