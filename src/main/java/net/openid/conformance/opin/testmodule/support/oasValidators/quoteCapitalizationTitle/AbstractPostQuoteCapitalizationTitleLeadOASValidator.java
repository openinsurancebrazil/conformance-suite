package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public abstract class AbstractPostQuoteCapitalizationTitleLeadOASValidator extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getEndpointPath() {
        return "/lead/request";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }

}
