package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForEndorsement;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyExclusao;

public abstract class AbstractEndorsementApiMultipleEndorsementsTestModule extends AbstractEndorsementApiHappyTestModule {

    @Override
    protected void executeTest() {
        super.executeTest();
        postRepeatedEndorsement();
    }

    @Override
    protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
        return new CreateEndorsementRequestBodyExclusao();
    }

    @Override
    protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
        return CreateEndorsementConsentBodyExclusao.class;
    }

    protected void postRepeatedEndorsement() {
        runInBlock("POST repeated endorsement - Expecting 401", () -> {
            callAndStopOnFailure(PrepareUrlForEndorsement.class);
            callAndStopOnFailure(createEndorsementRequestBody().getClass());
            callAndStopOnFailure(LoadRequestBodyAsString.class);
            callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
            callAndStopOnFailure(CreateIdempotencyKey.class);
            callAndStopOnFailure(AddIdempotencyKeyHeader.class);
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
            callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
            callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
            callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
            callAndStopOnFailure(SetResourceMethodToPost.class);
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        });
    }
}
