package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.AbstractGetInsurancePatrimonialClaimOASValidator;

@ApiName("Insurance Patrimonial 1.5.0")
public class GetInsurancePatrimonialClaimOASValidatorV1n5 extends AbstractGetInsurancePatrimonialClaimOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insurancePatrimonial/v1/swagger-insurance-patrimonial-v1.5.0.yaml";
	}
}
