package net.openid.conformance.opin.testmodule.phase3.contractLifePension;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateContractLifePensionPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinContractLifePensionNegativeTestModule extends AbstractOpinPhase3QuoteNegativeTestModule {

    @Override
    protected OPINScopesEnum getLeadScope() {
        return OPINScopesEnum.CONTRACT_LIFE_PENSION_LEAD;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.CONTRACT_LIFE_PENSION;
    }

    @Override
    protected String getApiFamilyType() {
        return "contract-life-pension";
    }

    @Override
    protected String getApiName() {
        return "contract-life-pension";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateContractLifePensionPostRequestBody.class;
    }

}