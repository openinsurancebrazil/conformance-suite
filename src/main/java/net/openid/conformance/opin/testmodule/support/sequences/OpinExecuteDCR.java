package net.openid.conformance.opin.testmodule.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.common.CheckDistinctKeyIdValueInClientJWKs;
import net.openid.conformance.opin.testmodule.support.CheckOpinDirectoryApiBase;
import net.openid.conformance.opin.testmodule.support.CheckOpinDirectoryDiscoveryUrl;
import net.openid.conformance.sequence.AbstractConditionSequence;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.variant.ClientAuthType;

public class OpinExecuteDCR extends AbstractConditionSequence {
    protected boolean shouldUseJARM;
    protected ClientAuthType clientAuthType;

    public OpinExecuteDCR(boolean shouldUseJARM, ClientAuthType clientAuthType, Environment env) {
        this.shouldUseJARM = shouldUseJARM;
        this.clientAuthType = clientAuthType;
        String clientAuth = clientAuthType.toString();
        if (clientAuthType == ClientAuthType.MTLS) {
            // the FAPI auth type variant doesn't use the name required for the registration request as per
            // https://datatracker.ietf.org/doc/html/rfc8705#section-2.1.1
            clientAuth = "tls_client_auth";
        }
        env.putString("client_auth_type", clientAuth);
    }
    @Override
    public void evaluate() {

        callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
        callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);

        // normally our DCR tests create a key on the fly to use, but in this case the key has to be registered
        // manually with the central directory so we must use user supplied keys
        callAndStopOnFailure(ExtractJWKSDirectFromClientConfiguration.class);

        callAndContinueOnFailure(CheckDistinctKeyIdValueInClientJWKs.class, Condition.ConditionResult.FAILURE, "RFC7517-4.5");

        getSsa();

        call(exec().startBlock("Perform Dynamic Client Registration"));

        callAndStopOnFailure(StoreOriginalClientConfiguration.class);
        callAndStopOnFailure(ExtractClientNameFromStoredConfig.class);

        // the jwks is hosted on the directory, we must use the url in the software statement
        callAndStopOnFailure(FAPIBrazilExtractJwksUriFromSoftwareStatement.class, "BrazilOBDCR-7.1-5");

        // create basic dynamic registration request
        callAndStopOnFailure(CreateEmptyDynamicRegistrationRequest.class);

        callAndStopOnFailure(AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest.class);
        if (!shouldUseJARM) {
            // implicit is only required when id_token is returned in frontchannel
            callAndStopOnFailure(AddImplicitGrantTypeToDynamicRegistrationRequest.class);
        }
        callAndStopOnFailure(AddRefreshTokenGrantTypeToDynamicRegistrationRequest.class);
        callAndStopOnFailure(AddClientCredentialsGrantTypeToDynamicRegistrationRequest.class);

        if (clientAuthType == ClientAuthType.MTLS) {
            callAndStopOnFailure(AddTlsClientAuthSubjectDnToDynamicRegistrationRequest.class);
        }

        callAndStopOnFailure(AddJwksUriToDynamicRegistrationRequest.class, "RFC7591-2", "BrazilOBDCR-7.1-5");
        callAndStopOnFailure(AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment.class);
        if (shouldUseJARM) {
            callAndStopOnFailure(SetResponseTypeCodeInDynamicRegistrationRequest.class);
        } else {
            callAndStopOnFailure(SetResponseTypeCodeIdTokenInDynamicRegistrationRequest.class);
        }
        // the jwks is hosted on the directory, we must use the url in the software statement
        callAndStopOnFailure(FAPIBrazilExtractJwksUriFromSoftwareStatement.class, "BrazilOBDCR-7.1-5");
        callAndStopOnFailure(AddRedirectUriToDynamicRegistrationRequest.class);

        callAndStopOnFailure(AddSoftwareStatementToDynamicRegistrationRequest.class);

        call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));

        callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");

        callAndContinueOnFailure(CheckScopesFromDynamicRegistrationEndpointContainRequiredScopes.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1.1", "RFC7591-2", "RFC7591-3.2.1");

        // The tests expect scope to be part of the 'client' object, but it may not be in the dcr response so copy across
        callAndStopOnFailure(CopyScopeFromDynamicRegistrationTemplateToClientConfiguration.class);
        callAndStopOnFailure(CopyOrgJwksFromDynamicRegistrationTemplateToClientConfiguration.class);

        call(exec().endBlock());
    }

    protected void getSsa() {
        call(exec().startBlock("Obtain access token for directory and retrieve a software statement"));

        callAndStopOnFailure(ExtractDirectoryConfiguration.class);

        callAndContinueOnFailure(CheckOpinDirectoryDiscoveryUrl.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");
        callAndContinueOnFailure(CheckOpinDirectoryApiBase.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");

        call(exec().mapKey("config", "directory_config"));
        call(exec().mapKey("server", "directory_server"));
        call(exec().mapKey("client", "directory_client"));
        call(exec().mapKey("access_token", "directory_access_token"));
        call(exec().mapKey("discovery_endpoint_response", "directory_discovery_endpoint_response"));
        callAndStopOnFailure(GetDynamicServerConfiguration.class);

        // this overwrites the non-directory values; we will have to replace them below
        callAndContinueOnFailure(AddMTLSEndpointAliasesToEnvironment.class, Condition.ConditionResult.FAILURE, "RFC8705-5");

        callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);

        callAndStopOnFailure(SetDirectorySoftwareScopeOnTokenEndpointRequest.class);

        // MTLS client auth
        callAndStopOnFailure(AddClientIdToTokenEndpointRequest.class);

        callAndStopOnFailure(CallTokenEndpoint.class);

        callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);

        callAndStopOnFailure(CheckForAccessTokenValue.class);

        callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);

        call(exec().unmapKey("server"));
        call(exec().unmapKey("client"));
        call(exec().unmapKey("discovery_endpoint_response"));
        call(exec().unmapKey("config"));
        call(exec().unmapKey("access_token"));

        // restore MTLS aliases to the values for the server being tested
        callAndContinueOnFailure(AddMTLSEndpointAliasesToEnvironment.class, Condition.ConditionResult.FAILURE, "RFC8705-5");

        callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);

        // use access token to get ssa
        // https://matls-api.sandbox.directory.openbankingbrasil.org.br/organisations/${ORGID}/softwarestatements/${SSID}/assertion
        callAndStopOnFailure(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class);

        call(exec().endBlock());
    }
}
