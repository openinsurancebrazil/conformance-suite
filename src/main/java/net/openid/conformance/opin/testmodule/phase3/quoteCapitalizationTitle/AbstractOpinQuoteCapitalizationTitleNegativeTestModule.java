package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.condition.client.SetResourceMethodToPost;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400or422;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PostQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteCapitalizationTitleNegativeTestModule extends AbstractOpinPhase3QuoteNegativeTestModule {

    @Override
    protected OPINScopesEnum getLeadScope() {
        return OPINScopesEnum.QUOTE_CAPITALIZATION_TITLE_LEAD;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_CAPITALIZATION_TITLE;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getApiName() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "request";
    }

    protected abstract Class<? extends AbstractCondition> createPostQuoteRequestBodyWrongMonthlyPayment();
    protected abstract Class<? extends AbstractCondition> createPostQuoteRequestBodyWrongSinglePayment();

    @Override
    protected void postQuoteCalls() {
        this.postQuoteWithoutConsentId();
        this.postQuoteWithWrongMonthlyRequestBody();
        this.postQuoteWithWrongSingleRequestBody();
        this.postQuoteWithWrongScope();
    }

    protected void postQuoteWithWrongMonthlyRequestBody() {
        runInBlock("Call POST /request endpoint sending paymentType as MENSAL, and sending the singlePayment field", () -> {
            stepBeforePostQuoteRequestCreation();
            callAndStopOnFailure(createPostQuoteRequestBodyWrongMonthlyPayment());
            stepsAfterPostQuoteRequestCreation();
        });
    }

    protected void postQuoteWithWrongSingleRequestBody() {
        runInBlock("Call POST /request endpoint sending paymentType as UNICO, and sending the monthlyPayment field", () -> {
            stepBeforePostQuoteRequestCreation();
            callAndStopOnFailure(createPostQuoteRequestBodyWrongSinglePayment());
            stepsAfterPostQuoteRequestCreation();
        });
    }

    protected void stepBeforePostQuoteRequestCreation() {
        callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
        call(setHeadersSequence());
    }

    protected void stepsAfterPostQuoteRequestCreation() {
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");

        runInBlock("Validate the quote creation error response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas400or422.class);
            callAndStopOnFailure(PostQuoteCapitalizationTitleOASValidator.class);
        });
    }
}