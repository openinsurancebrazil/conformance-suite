package net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractOpinPatrimonialResourcesApiTestModule extends AbstractOpinApiResourcesTestModuleV2 {

	@Override
	protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
		return AddPatrimonialScope.class;
	}

	@Override
	protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
		return BuildPatrimonialConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected PermissionsGroup getPermissionsGroup() {
		return PermissionsGroup.DAMAGES_AND_PEOPLE_PATRIMONIAL;
	}

	@Override
	protected String getApi() {
		return "insurance-patrimonial";
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*]");
		return AllIdsSelectorFromJsonPath.class;
	}

	@Override
	protected String getResourceType() {
		return EnumOpinResourcesType.DAMAGES_AND_PEOPLE_PATRIMONIAL.name();
	}

	@Override
	protected String getResourceStatus() {
		return EnumResourcesStatus.AVAILABLE.name();
	}
}
