package net.openid.conformance.opin.testmodule.v1.structural.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.QuotePatrimonialTypes;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.diverseRisks.PostPatrimonialDiverseRisksOASValidatorV1n9;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
    testName = "opin-quote-patrimonial-api-diverse-risks-structural-test",
    displayName = "Validate structure of Quote Patrimonial API Diverse Risks Endpoint successful response",
    summary = "Validate structure of Quote Patrimonial API Diverse Risks Endpoint successful response\n" +
        "\u2022 Call the \"/diverse-risks/request\" endpoint using GET method\n" +
        "\u2022 Expect 200 - validate response\n" +
        "\u2022 Call the \"/diverse-risks/request/{consentId}/quote-status\" endpoint using GET method\n" +
        "\u2022 Expect 200 - validate response",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "resource.consentUrl",
                "resource.consentId"
        }
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})
public class OpinQuotePatrimonialApiDiverseRisksStructuralTestModule extends QuotePatrimonialStructuralTestAbstractClass {

    @Override
    protected QuotePatrimonialTypes quotePatrimonialType() {
        return QuotePatrimonialTypes.DIVERSE_RISKS;
    }

    @Override
    protected Class<? extends Condition> validatorRoot() {
        return PostPatrimonialDiverseRisksOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends Condition> validatorQuoteStatus() {
        return GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n9.class;
    }
}
