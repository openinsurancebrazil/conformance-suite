package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CreateQuotePersonTravelPostRequestBodyWithCoverage_DiffTermDate extends AbstractCreateQuotePostRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        this.addQuoteData(env, data);
    }

    private void addQuoteData(Environment env, JsonObject data) {
        JsonObject quoteData = new JsonObject();
        var today = ZonedDateTime
                .now(ZoneOffset.UTC)
                .toLocalDateTime();
        quoteData.addProperty("termStartDate", today
                .plusDays(1)
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
        quoteData.addProperty("termEndDate", today
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));

        JsonArray coverages = getCoverages();

        quoteData.add("coverages", coverages);

        data.add("quoteData", quoteData);
    }

    @Override
    protected JsonArray getCoverages() {
        JsonArray coverages = new JsonArray();
        JsonObject coverage = new JsonObject();
        coverage.addProperty("branch", "0111");
        coverage.addProperty("code", "MORTE");
        coverage.addProperty("isSeparateContractingAllowed", false);
        coverages.add(coverage);
        return coverages;
    }
}