package net.openid.conformance.opin.testmodule.support.oasValidators.pensionPlan.v1n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetPensionPlanOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/pensionPlan/v1n4/swagger-pension-plan-1.4.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/pension-plan";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}