package net.openid.conformance.opin.testmodule.support;

public class BuildInsuranceCapitalizationTitleUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl{
    @Override
    protected String getApiReplacement() {
        return "insurance-capitalization-title";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-capitalization-title/plans";
    }
}
