package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddCapitalizationTitleScope;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.PlanIdSelector;
import net.openid.conformance.opin.testmodule.support.directory.GetCapitalizationTitleV1Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitleEventsOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitlePlansOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Map;

@PublishTestModule(
        testName = "opin-fvp-capitalization-title-api-test-module",
        displayName = "Validates the structure of all Capitalization Tile API",
        summary = "Validates the structure of all Capitalization Tile API\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“CAPITALIZATION_TITLE_READ”, “CAPITALIZATION_TITLE_PLANINFO_READ”, “CAPITALIZATION_TITLE_EVENTS_READ”, “CAPITALIZATION_TITLE_SETTLEMENTS_READ”, “RESOURCES_READ”)\n" +
                "• Expects 201 -  Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Capitalization Title Plans Endpoint\n" +
                "• Expects 200 - Fetches one of the Plan IDs returned\n" +
                "• Calls GET Capitalization Title {planId} plan-info Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} events Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} settlements Endpoint\n" +
                "• Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.authorisationServerId",
                "resource.consentSyncTime"
        }
)
public class OpinFvpCapitalizationTitleApiTestModule extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetCapitalizationTitleV1Endpoint.class));
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddCapitalizationTitleScope.class;
    }


    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.CAPITALIZATION_TITLE;

    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceCapitalizationTitlePlansOASValidatorV1n4.class;
    }

    @Override
    protected String getApi() {
        return "insurance-capitalization-title";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        return PlanIdSelector.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "plan-info", GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n4.class,
                "events", GetInsuranceCapitalizationTitleEventsOASValidatorV1n4.class,
                "settlements", GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n4.class

        );
    }

}