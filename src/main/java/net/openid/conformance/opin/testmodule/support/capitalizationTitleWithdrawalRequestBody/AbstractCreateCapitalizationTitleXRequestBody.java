package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateCapitalizationTitleXRequestBody extends AbstractCondition {

    @PreEnvironment(required = "config")
    @Override
    protected Environment evaluate(Environment env) {
        JsonObject data = new JsonObject();

        editData(env, data);

        return env;
    }

    protected abstract void editData(Environment env, JsonObject data);
}
