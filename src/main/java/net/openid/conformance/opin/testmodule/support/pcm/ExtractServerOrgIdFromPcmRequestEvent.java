package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;


public class ExtractServerOrgIdFromPcmRequestEvent extends AbstractCondition {

    @Override
    @PreEnvironment(required = "pcm_event")
    public Environment evaluate(Environment env) {
        String serverOrgId = Optional.ofNullable(env.getString("pcm_event", "serverOrgId"))
                .orElseThrow(() -> error("Could not find serverOrgId in the request"));


        env.putString("config", "resource.brazilOrganizationId", serverOrgId);
        logSuccess("Extracted serverOrgidFrom from the request", args("serverOrgId", serverOrgId));
        return env;
    }
}
