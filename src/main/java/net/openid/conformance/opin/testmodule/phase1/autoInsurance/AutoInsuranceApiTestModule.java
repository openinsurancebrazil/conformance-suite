package net.openid.conformance.opin.testmodule.phase1.autoInsurance;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.PrepareToGetAutoInsuranceApi;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testmodule.support.oasValidators.autoInsurance.v1n5.GetAutoInsuranceOASValidatorV1n5;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - ProductsNServices - Auto Insurance API test",
	displayName = "Validate structure of ProductsNServices - Auto Insurance API Api resources",
	summary = "Validate structure of ProductsNServices - Auto Insurance Api resources",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1,
	configurationFields = {
		"resource.commercializationArea",
		"resource.fipeCode",
		"resource.year"
	}
)
public class AutoInsuranceApiTestModule extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate ProductsNServices - Auto Insurance response", () -> {
			callAndStopOnFailure(PrepareToGetAutoInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetAutoInsuranceOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
		});
	}
}
