package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForCustomerIdentifications extends AbstractPrepareUrlForApi {

	private String productType;

	@Override
	@PreEnvironment(strings = {"product_type"})
	public Environment evaluate(Environment env) {
		productType = env.getString("product_type");
		return super.evaluate(env);
	}

	@Override
	protected String getApiReplacement() {
		return "customers";
	}

	@Override
	protected String getEndpointReplacement() {
		return String.format("%s/identifications", productType);
	}
}
