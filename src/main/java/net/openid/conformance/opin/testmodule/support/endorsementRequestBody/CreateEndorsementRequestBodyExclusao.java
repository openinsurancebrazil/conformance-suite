package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyExclusao extends AbstractCreateEndorsementRequestBody {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.EXCLUSAO;
    }
}
