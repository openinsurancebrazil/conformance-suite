package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.v1n4;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.abstracts.AbstractOpinAcceptanceAndBranchesAbroadApiTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-acceptance-and-branches-abroad-api-test-v1n4",
	displayName = "Validates the structure of all financial Acceptance and Branches Abroad API resources",
	summary = "Validates the structure of all financial Acceptance and Branches Abroad API resources\n" +
		"\u2022 (“DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinAcceptanceAndBranchesAbroadApiTestModuleV1n4 extends AbstractOpinAcceptanceAndBranchesAbroadApiTestModule {


	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadClaimOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadListOASValidatorV1n4.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
		return IdSelectorFromJsonPath.class;
	}
}