package net.openid.conformance.opin.testmodule.v1.rural;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralPremiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-rural-api-test",
        displayName = "Validates the structure of all rural API resources",
        summary = "Validates the structure of all rural API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the rural API (“DAMAGES_AND_PEOPLE_RURAL_READ”, “DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "\u2022 Calls GET rural “/” API\n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
                "\u2022 Calls GET rural policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields\n" +
                "\u2022 Calls GET rural premium API \n" +
                "\u2022 Expects 200- Validate all the fields\n" +
                "\u2022 Calls GET rural claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})
public class OpinRuralApiTestModule extends AbstractOpinRuralApiTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceRuralListOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator() {
        return GetInsuranceRuralPolicyInfoOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator() {
        return GetInsuranceRuralPremiumOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator() {
        return GetInsuranceRuralClaimOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
        return IdSelectorFromJsonPath.class;
    }
}
