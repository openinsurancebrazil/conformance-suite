package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AddWithdrawalPensionScope;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForPensionWithdrawal;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawal.v1n3.PostPensionWithdrawalOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.CreatePensionWithdrawalMockPostRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-pension-withdraw_api_wrong-permissions_test-module_v1",
        displayName = "Validates the structure of Pension Withdrawal API",
        summary = """
                    Ensure a life pension withdrawal request cannot be successfully executed without the needed permission
                    
                    • Call the POST Consents Endpoint, sending phase 2 permissions
                    • Expect 201 - Validate Response
                    • Redirect the User to Authorize Consent
                    • Call the GET Consent Endpoint
                    • Expect 200 - Validate Response and ensure status is "AUTHORIZED"
                    • Call the POST pension/request endpoint
                    • Expect 403 - Validate Response
                    • Call the GET Consent Endpoint
                    • Expect 200 - Validate Response and ensure status is "AUTHORIZED"    
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3PensionWithdrawApiWrongPermissionsTestModule extends AbstractOpinPhase3XWithdrawWrongPermissionsTestModule {

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddWithdrawalPensionScope.class;
    }

    @Override
    protected String getApi() {
        return "insurance-pension-plan";
    }

    @Override
    protected void configureClient() {
        super.configureClient();
        env.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());
    }

    @Override
    protected Class<? extends Condition> validator() {
        return PostPensionWithdrawalOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends Condition> prepareUrl() {
        return PrepareUrlForPensionWithdrawal.class;
    }

    @Override
    protected Class<? extends Condition> createWithdrawalPostRequestBody() {
        return CreatePensionWithdrawalMockPostRequestBody.class;
    }
}
