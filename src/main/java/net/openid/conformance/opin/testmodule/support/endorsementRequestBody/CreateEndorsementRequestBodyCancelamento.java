package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyCancelamento extends AbstractCreateEndorsementRequestBody {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.CANCELAMENTO;
    }
}
