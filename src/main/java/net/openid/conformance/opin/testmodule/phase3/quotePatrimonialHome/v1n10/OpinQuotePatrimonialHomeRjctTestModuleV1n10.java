package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.v1n10;


import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.AbstractOpinQuotePatrimonialHomeRjctTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.GetPatrimonialHomeQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PostPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialPostRequestBodyWithCoverage_DiffTermDate;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-home_api_rejected_test-module_v1n10",
        displayName = "Ensure that a Patrimonial Home quotation request can be rejected.",
        summary = "Ensure that a Patrimonial Home quotation request can be rejected. This test module will send the quoteData with only the required fields, and expect the quotation to be rejected. This test applies to both Personal and Business products. If the \"brazilCnpj\" field is filled out, it will transmit business information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST home/request endpoint sending personal or business information, following what is defined at the config, and sending the quoteData with only the required fields, and setting termStartDate to date after termEndDate\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD or RJCT\n" +
                "\u2022 Poll the GET home/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET home/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respone and ensure status is RJCT",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialHomeRjctTestModuleV1n10 extends AbstractOpinQuotePatrimonialHomeRjctTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialHomeQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialPostRequestBodyWithCoverage_DiffTermDate.class;
    }
}
