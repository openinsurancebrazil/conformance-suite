package net.openid.conformance.opin.testmodule.support.oasValidators.stopLoss.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Stop Loss 1.3.0")
public class GetStopLossOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/stopLoss/v1n3/swagger-stop-loss-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/stop-loss";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}