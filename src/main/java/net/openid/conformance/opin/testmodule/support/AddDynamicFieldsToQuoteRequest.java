package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddDynamicFieldsToQuoteRequest extends AbstractCondition {
    @Override
    @PreEnvironment(required = {"resource_request_entity", "dynamic_field"})
    public Environment evaluate(Environment env) {
        JsonObject requestData = env.getObject("resource_request_entity");

        JsonObject dynamicField = env.getObject("dynamic_field");
        JsonArray dynamicFields = dynamicField.getAsJsonArray("dynamic_fields");

        JsonObject data = requestData.getAsJsonObject("data");
        data.add("quoteCustomData", getQuoteCustomData(dynamicFields));

        logSuccess("Added customData to request body", args(
                "data", data
        ));

        env.putString("resource_request_entity", requestData.toString());
        return env;
    }

    private JsonObject getQuoteCustomData(JsonArray dynamicFields) {
        JsonArray generalQuoteInfo = new JsonArray();
        for(JsonElement dynamicFieldElement : dynamicFields) {

            JsonObject dynamicField = dynamicFieldElement.getAsJsonObject();
            JsonObject quoteInfo = new JsonObject();
            quoteInfo.addProperty("fieldId", dynamicField.get("fieldId").getAsString());
            switch (dynamicField.get("type").getAsString()) {
                case "BOOLEAN":
                    quoteInfo.addProperty("value", false);
                    break;
                case "STRING":
                    quoteInfo.addProperty("value", "");
                    break;
                case "NUMBER":
                    quoteInfo.addProperty("value", 0.0);
                    break;
                case "INTEGER":
                    quoteInfo.addProperty("value", 0);
                    break;
                case "ARRAY":
                    quoteInfo.add("value", new JsonArray());
                    break;
            }

            generalQuoteInfo.add(quoteInfo);
        }

        JsonObject quoteCustomData = new JsonObject();
        quoteCustomData.add("generalQuoteInfo", generalQuoteInfo);
        return quoteCustomData;
    }
}
