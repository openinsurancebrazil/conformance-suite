package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreateCapitalizationTitleWithdrawalMockConsentBody extends AbstractCreateCapitalizationTitleWithdrawalConsentBody{

    @Override
    public Environment evaluate(Environment env) {
        JsonObject capitalizationTitleWithdrawData = new JsonObject();
        JsonObject consentData = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();

        capitalizationTitleWithdrawData.addProperty("capitalizationTitleName", "string");
        capitalizationTitleWithdrawData.addProperty("planId", "string2");
        capitalizationTitleWithdrawData.addProperty("titleId", "string3");
        capitalizationTitleWithdrawData.addProperty("seriesId","string4");
        capitalizationTitleWithdrawData.addProperty("termEndDate", "2025-04-01");
        capitalizationTitleWithdrawData.addProperty("withdrawalReason","IMPOSSIBILIDADE_DE_PAGAMENTO_DAS_PARCELAS");

        JsonObject withdrawalTotalAmount = new JsonObject();
        withdrawalTotalAmount.addProperty("amount", "2000.05");

        JsonObject unit = new JsonObject();
        unit.addProperty("code","R$");
        unit.addProperty("description","BRL");

        withdrawalTotalAmount.add("unit", unit);
        capitalizationTitleWithdrawData.add("withdrawalTotalAmount", withdrawalTotalAmount);
        editData(env, capitalizationTitleWithdrawData);

        consentData.add("withdrawalCaptalizationInformation", capitalizationTitleWithdrawData);
        logSuccess("CapitalizationTitleWithdrawal mock consent body created and added",args("consent_endpoint_request", consentData));
        return env;
    }
    @Override
    protected void editData(Environment env, JsonObject data) {}
}
