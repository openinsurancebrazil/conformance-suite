package net.openid.conformance.opin.testmodule.support;

public class BuildAutoConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

    @Override
    protected String getApiReplacement() {
        return "insurance-auto";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-auto";
    }
}
