package net.openid.conformance.opin.testmodule.phase3.capitalizationTitleWithdrawal;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.opin.testmodule.support.OpinSetPermissionsBuilderForAllPhase2Permissions;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.CreateCapitalizationTitleWithdrawalConsentBody;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitleWithdrawal.v1n3.PostCapitalizationTitleWithdrawalOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-capitalization-title-withdraw_api_negative-consents_test-module_v1",
        displayName = "Validates the structure of Capitalization Title Withdrawal API",
        summary = """
                Ensure a consent for withdraw cannot be created  if the appropriate rules are not followed

                · Execute a Customer Data Sharing Journey for the Capitalization Title Product, obtaining the first productName and planId from the insurance-capitalization-title/plans endpoint, and the corresponding modality, susepProcessNumber, titleId, seriesId, termEndDate and prAmount from the plan-info endpoint
                · Call the POST Consents Endpoints with Phase 2 and CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permissions, sending the withdrawalCaptalizationInformation with the pre-saved information
                · Expect 422 - Validate Response
                · Call the POST Consents Endpoints with CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permission, but without the withdrawalCaptalizationInformation with field
                · Expect 422 - Validate Response
                · Call the POST Consents Endpoints with PENSION_WITHDRAWAL_CREATE and CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permission, sending the withdrawalCaptalizationInformation with with the pre-saved information
                · Expect 422 - Validate Response
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinPhase3CapitalizationTitleWithdrawApiNegativeConsentsTestModule extends AbstractOpinPhase3CapitalizationTitleWithdrawNegativeTestModule{

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }

    @Override
    protected void editCapitalizationTitleWithdrawalRequestBody() {
    }


    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostCapitalizationTitleWithdrawalOASValidatorV1n3();
    }

    @Override
    protected void setupSecondFlow() {
        performTestSteps();
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
            setupSecondFlow();
            fireTestFinished();
        }

    @Override
    protected void executeTest() {
        //not needed in this test
    }

    protected void performTestSteps() {
        runInBlock("POST Consents Endpoints with Phase 2 and CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permissions and pre-saved withdrawalCaptalizationInformation - Expects 422", () -> {
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
            permissionsBuilder.resetPermissions().buildFromEnv();
            permissionsBuilder.addPermissionsGroup(PermissionsGroup.CAPITALIZATION_TITLE_WITHDRAWAL).build();

            call(postConsentSequence()
                    .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                            condition(CreateCapitalizationTitleWithdrawalConsentBody.class)));
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("POST Consents Endpoints with CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permissions and no withdrawalCaptalizationInformation - Expects 422", () -> {
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CAPITALIZATION_TITLE_WITHDRAWAL).build();

            call(postConsentSequence());
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("POST Consents Endpoints with PENSION_WITHDRAWAL_CREATE and CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permissions and pre-saved withdrawalCaptalizationInformation - Expects 422", () -> {
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CAPITALIZATION_TITLE_WITHDRAWAL).addPermissionsGroup(PermissionsGroup.PENSION_WITHDRAWAL).build();
            call(postConsentSequence()
                    .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                            condition(CreateCapitalizationTitleWithdrawalConsentBody.class)));
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        });
    }

    protected ConditionSequence postConsentSequence() {
        return new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication, false)
                .insertAfter(CallConsentEndpointWithBearerTokenAnyHttpMethod.class,
                        sequenceOf(
                                condition(PostConsentsOASValidatorV2n6.class).dontStopOnFailure(),
                                condition(SaveConsentsAccessToken.class))
                );
    }

}
