package net.openid.conformance.opin.testmodule.phase1.environmentalLiability;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.environmentalLiability.v1n3.GetEnvironmentalLiabilityOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - ProductsServices - Environmental Liability API test",
        displayName = "Validate structure of ProductsServices - Environmental Liability API Api resources",
        summary = "Validate structure of ProductsServices - Environmental Liability Api resources",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1)

public class EnvironmentalLiabilityApiTestModule extends AbstractNoAuthFunctionalTestModule {

    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices - Environmental Liability response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetEnvironmentalLiabilityOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
        });
    }
}
