package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyAlteracaoDiff extends AbstractEndorsementRequestBodyDiff {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.ALTERACAO;
    }
}
