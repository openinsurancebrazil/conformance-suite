package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleNegativeConsentTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title_api_negative-consent_test-module_v1.10.0",
        displayName = "opin-quote-capitalization-title_api_negative-consent_test-module_v1",
        summary = "Ensure a consent cannot be created in unhappy requests.\n" +
                "\u2022 Call POST Consents with permission for Customer Data, PF or PJ, and Patrimonial Lead.\n" +
                "\u2022 Expect 422 - Validate Error Response\n" +
                "\u2022 Call POST Consents with permission for Capitalization Title Lead and Capitalization Title Quotation\n" +
                "\u2022 Expect 422 - Validate Error Response.\n" +
                "\u2022 Call POST Consents with permission for Capitalization Title Lead.\n" +
                "\u2022 Expect 422 - Validate Error Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteCapitalizationTitleNegativeConsentTestModuleV1n10 extends AbstractOpinQuoteCapitalizationTitleNegativeConsentTestModule {

        @Override
        protected void postConsentValidator() {
                callAndStopOnFailure(PostConsentsOASValidatorV2n7.class);
        }
}
