package net.openid.conformance.opin.testmodule.support.oasValidators.directorsOfficersLiability.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Directors Officers Liability 1.3.0")
public class GetDirectorsOfficersLiabilityOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/directorsOfficersLiability/v1n3/swagger-directors-officers-liability-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/directors-officers-liability";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}