package net.openid.conformance.opin.testmodule.support;

public class BuildHousingConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

    @Override
    protected String getApiReplacement() {
        return "insurance-housing";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-housing";
    }
}
