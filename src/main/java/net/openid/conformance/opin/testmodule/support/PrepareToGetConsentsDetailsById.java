package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareToGetConsentsDetailsById extends AbstractPrepareUrlForApi {

	private String consentId;

	@Override
	@PreEnvironment(strings = "consent_id")
	public Environment evaluate(Environment env) {
		consentId = env.getString("consent_id");
		return super.evaluate(env);
	}

	@Override
	protected String getApiReplacement() {
		return "consents";
	}

	@Override
	protected String getEndpointReplacement() {
		return String.format("consents/%s", consentId);
	}
}
