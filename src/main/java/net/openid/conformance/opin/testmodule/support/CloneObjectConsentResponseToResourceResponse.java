package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CloneObjectConsentResponseToResourceResponse extends AbstractCondition {

    @PreEnvironment(
            required = {"consent_endpoint_response"}
    )
    @PostEnvironment(
            strings = "resource_endpoint_response"
    )
    public Environment evaluate(Environment env) {
        JsonObject consentEndpointResponse = env.getObject("consent_endpoint_response");
        env.putString("resource_endpoint_response", consentEndpointResponse.toString());
        this.logSuccess("Cloning value");
        return env;
    }
}