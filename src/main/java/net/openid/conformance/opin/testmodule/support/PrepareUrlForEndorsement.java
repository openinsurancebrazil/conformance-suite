package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForEndorsement extends AbstractPrepareUrlForApi {

    private String consentId;

    @Override
    protected String getApiReplacement() {
        return "endorsement";
    }

    @Override
    protected String getEndpointReplacement() {
        return String.format("request/%s", consentId);
    }

    @Override
    @PreEnvironment(strings = "consent_id")
    public Environment evaluate(Environment env) {
        consentId = env.getString("consent_id");
        return super.evaluate(env);
    }
}
