package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle;

import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.opin.testmodule.phase3.capitalizationTitleWithdrawal.AbstractOpinPhase3CapitalizationTitleWithdrawHappyPathTestModule;
import net.openid.conformance.opin.testmodule.support.AddQuoteCapitalizationTitleRaffleScope;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForQuoteCapitalizationTitleRaffle;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.AbstractCreateCapitalizationTitleXConsentBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.AbstractCreateCapitalizationTitleXRequestBody;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.quoteCapitalizationTitleRaffleRequestBody.CreateQuoteCapitalizationTitleRaffleConsentBody;
import net.openid.conformance.opin.testmodule.support.quoteCapitalizationTitleRaffleRequestBody.CreateQuoteCapitalizationTitleRafflePostRequestBody;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractOpinQuoteCapitalizationTitleRaffleApiCoreTestModule extends AbstractOpinPhase3CapitalizationTitleWithdrawHappyPathTestModule {


    @Override
    protected void prepareUrlForPostCall() {
        callAndStopOnFailure(PrepareUrlForQuoteCapitalizationTitleRaffle.class);
    }

    @Override
    protected PermissionsGroup getPermissionsGroup2() {
        return PermissionsGroup.QUOTE_CAPITALIZATION_TITLE_RAFFLE;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class, sequenceOf(
                        condition(CreateQuoteCapitalizationTitleRaffleConsentBody.class).skipIfObjectMissing("series_1")
                ));
    }

    @Override
    protected AbstractCreateCapitalizationTitleXRequestBody createRequestBody() {
        return new CreateQuoteCapitalizationTitleRafflePostRequestBody();
    }
    protected AbstractCreateCapitalizationTitleXConsentBody createConsentBody() {
        return new CreateQuoteCapitalizationTitleRaffleConsentBody();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }

    @Override
    protected void addCapitalizationTitleScope() {
        callAndStopOnFailure(AddQuoteCapitalizationTitleRaffleScope.class);
    }
}
