package net.openid.conformance.opin.testmodule.support;

public enum QuotePatrimonialTypes {

    HOME, CONDOMINIUM, BUSINESS, DIVERSE_RISKS
}
