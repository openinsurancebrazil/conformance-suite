package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleRaffleApiCoreTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10.PostQuoteCapitalizationTitleRaffleOASValidatorV1n10;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title-raffle_api_core-test-module_v1.10.0",
        displayName = "opin-quote-capitalization-title-raffle_api_core-test-module_v1",
        summary = """
                Ensure a Capitalization Title raffle request can be successfully executed
                · Execute a Customer Data Sharing Journey for the Capitalization Title Product, obtaining the first productName and planId from the insurance-capitalization-title/plans endpoint, and the corresponding modality, susepProcessNumber from the plan-info endpoint
                · Call the POST Consents with QUOTE_CAPITALIZATION_TITLE_RAFFLE_CREATE permission, sending the raffleCapitalizationTitleInformation with contactType as EMAIL, and email as contact@email.com
                · Expect a 201 - Validate the response
                · Redirect the User to Authorize the Consent
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response and ensure the status is "AUTHORIZED"
                · Call the POST raffle/request endpoint, sending the same information as the consent request, and the information retrieved from the phase 2 endpoints
                · Expect a 201 - Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                · Call the GET at data.redirectLink
                · Expect a 200
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinQuoteCapitalizationTitleRaffleApiCoreTestModuleV1n10 extends AbstractOpinQuoteCapitalizationTitleRaffleApiCoreTestModule {
    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostQuoteCapitalizationTitleRaffleOASValidatorV1n10();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n7();
    }

}
