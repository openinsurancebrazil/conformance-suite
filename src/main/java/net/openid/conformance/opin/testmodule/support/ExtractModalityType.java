package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class ExtractModalityType extends AbstractCondition {
    @Override
    @PreEnvironment(required = "resource_endpoint_response_full")
    @PostEnvironment(strings = "modality")
    public Environment evaluate(Environment env) {
        JsonObject response = env.getObject("resource_endpoint_response_full");
        JsonObject body = JsonParser.parseString(OIDFJSON.getString(response.get("body"))).getAsJsonObject();
        JsonArray dataArray = body.getAsJsonArray("data");

        if (!dataArray.isEmpty()) {
            JsonObject firstDataObject = dataArray.get(0).getAsJsonObject();
            String modality = OIDFJSON.getString(firstDataObject.get("modality"));
            env.putString("modality", modality);
            logSuccess("Modality extracted from resources API response", args("modality", modality));
            return env;
        }

        throw error("No modality found in resources API response");
    }
}
