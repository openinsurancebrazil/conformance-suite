package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateCapitalizationTitleWithdrawalPerdaDeInteresseConsentBody extends AbstractCreateCapitalizationTitleWithdrawalConsentBody{
    @Override
    protected void editData(Environment env, JsonObject data) {
        data.addProperty("withdrawalReason","PERDA_DE_INTERESSE");
    }
}
