package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.v1n4;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4.GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.abstracts.AbstractOpinAcceptanceAndBranchesAbroadWrongPermissionsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-acceptance-and-branches-abroad-api-wrong-permissions-test-v1n4",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Acceptance and Branches Abroad API (“DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad premium API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad claim API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with only either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad “/” API\n"+
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad premium API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad claim API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
public class OpinAcceptanceAndBranchesAbroadWrongPermissionsTestModuleV1n4 extends AbstractOpinAcceptanceAndBranchesAbroadWrongPermissionsTestModule {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadClaimOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadListOASValidatorV1n4.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
		return IdSelectorFromJsonPath.class;
	}
}
