package net.openid.conformance.opin.testmodule.support.oasValidators.business.v1n3;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.*;
import org.springframework.http.HttpMethod;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/productsServices/business.yaml
 * Api endpoint: /business/
 * Api version: 1.2.0
 * Git hash:
 */

@ApiName("Business 1.3.0")
public class GetBusinessOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/business/v1n3/swagger-business-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/business";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}