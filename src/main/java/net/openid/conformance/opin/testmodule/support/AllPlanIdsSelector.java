package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class AllPlanIdsSelector extends AbstractIdSelector {

    @Override
    protected String getId(Environment env) {

        JsonArray data = getData(env);
        JsonArray allPlanIds = new JsonArray();

        for (JsonElement brandElement : data) {

            if (!brandElement.isJsonObject()) {
                throw error("Unexpected error: element of data object is not a real Json.");
            }
            allPlanIds.addAll(getPlanIdsFromBrand(brandElement.getAsJsonObject()));
        }

        return allPlanIds.toString();
    }

    private JsonArray getData(Environment env) {
        JsonObject body = bodyFrom(env, "resource_endpoint_response_full").getAsJsonObject();
        JsonArray data = Optional.ofNullable(body.getAsJsonArray("data"))
                .orElseThrow(() -> error("Could not find data in the body", args("body", body)));
        if(data.isEmpty()) {
            throw error("Data field is empty, no further processing required.");
        }

        return data;
    }

    private JsonArray getPlanIdsFromBrand(JsonObject brand) {

        JsonArray companies = getCompanies(brand);
        JsonArray planIds = new JsonArray();

        for (JsonElement company : companies) {
            if (!company.isJsonObject()) {
                throw error("Unexpected error: element company is not a real Json.");
            }
            planIds.addAll(getPlanIdsFromCompany(company.getAsJsonObject()));
        }

        return planIds;
    }

    private JsonArray getCompanies(JsonObject brandObject) {
        JsonArray companies = Optional.ofNullable(brandObject.getAsJsonObject("brand"))
                .map(brand ->  brand.getAsJsonArray("companies"))
                .orElseThrow(() -> error("Could not extract brand.companies array from the data", args("brand", brandObject)));
        if (companies.isEmpty()) {
            throw error("Companies array cannot be empty to extract plan ID");
        }

        return companies;
    }

    private JsonArray getPlanIdsFromCompany(JsonObject company) {
        JsonArray products = getProducts(company);
        JsonArray planIds = new JsonArray();

        for(JsonElement product : products) {
            if (!product.isJsonObject()) {
                throw error("Unexpected error: element product is not a real Json.");
            }
            String planId = Optional.ofNullable(product.getAsJsonObject().get("planId"))
                    .map(OIDFJSON::getString)
                    .orElseThrow(() -> error("Could not find plan ID in the product object", args("product", product)));
            planIds.add(planId);
        }

        return planIds;
    }

    private JsonArray getProducts(JsonObject company) {
        JsonArray products = Optional.ofNullable(company.getAsJsonArray("products"))
                .orElseThrow(() -> error("Could not extract products array from the data", args("company", company)));
        if (products.isEmpty()) {
            throw error("Products array cannot be empty to extract plan ID");
        }

        return products;
    }
}
