package net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreatePensionWithdrawalConsentBody extends AbstractCondition {

    @Override
    @PreEnvironment(required = {"consent_endpoint_request","contract_1","pmbacAmount_1"}, strings = {"certificateId","withdrawalType"})
    public Environment evaluate(Environment env) {
        String certificateId = env.getString("certificateId");
        JsonObject pensionWithdrawData = new JsonObject();
        JsonObject consentData = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();
        JsonObject contract = env.getObject("contract_1");
        JsonObject pmbacAmount = env.getObject("pmbacAmount_1");

        pensionWithdrawData.addProperty("productName", contract.get("productName").getAsString());
        pensionWithdrawData.addProperty("certificateId", certificateId);
        pensionWithdrawData.addProperty("withdrawalType", EnumWithdrawalType.findByValue(env.getString("withdrawalType")).toString());
        pensionWithdrawData.addProperty("withdrawalReason","5_INSATISFACAO_COM_O_PRODUTO");
        pensionWithdrawData.add("pmbacAmount",pmbacAmount);

        editData(env, pensionWithdrawData);

        consentData.add("withdrawalLifePensionInformation", pensionWithdrawData);
        logSuccess("CapitalizationTitleWithdrawal consent body created and added",args("consent_endpoint_request", consentData));
        return env;
    }

    protected abstract void editData(Environment env, JsonObject data);
}
