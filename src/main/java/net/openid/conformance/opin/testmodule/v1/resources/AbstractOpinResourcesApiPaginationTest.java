package net.openid.conformance.opin.testmodule.v1.resources;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.ensureNumberOfRecords.EnsureNumberOfTotalRecordsIsAtLeast3FromMeta;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Map;

public abstract class AbstractOpinResourcesApiPaginationTest extends AbstractOpinApiTestModuleV2 {

	@Override
	protected ConditionSequence createOBBPreauthSteps(){
		return new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
	}

	@Override
	protected void validateResponse() {

		runInBlock("Validate Resources API request", () -> {

			callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast3FromMeta.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
			env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(OpinPaginationValidator.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Call Resources API page size = 1000", () -> {

			callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
			preCallProtectedResource();
			callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast3FromMeta.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
			env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(OpinPaginationValidator.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Call Resources API page size = 1", () -> {

			callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1.class);
			preCallProtectedResource();
			callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast3FromMeta.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
			env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(OpinPaginationValidator.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Call Next Endpoint page size = 1", () -> {

			callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
			callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureDataArrayHasOnlyOneElement.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast3FromMeta.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
			env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(OpinPaginationValidator.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Call Resources API with page size = 1001", () -> {

			callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1001.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResourceResponseCodeWas422.class);
			callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected PermissionsGroup getPermissionsGroup() {
		callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
		String permissionType = env.getString("permissions_builder");
		if(permissionType.equals("ALL_BUSINESS_PHASE2")) {
			return PermissionsGroup.ALL_BUSINESS_PHASE2;
		}
		else {
			return PermissionsGroup.ALL_PERSONAL_PHASE2;
		}
	}

	@Override
	protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
		return AddAllScopes.class;
	}

	@Override
	protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
		return BuildOpinResourcesConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected String getApi() {
		return "resources";
	}

	//This Method is not used required for this Test Module
	@Override
	protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
		return Map.of();
	}
}
