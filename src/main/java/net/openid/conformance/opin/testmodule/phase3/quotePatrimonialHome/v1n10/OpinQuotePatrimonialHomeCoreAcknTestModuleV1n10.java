package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.AbstractOpinQuotePatrimonialHomeCoreAcknTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.GetPatrimonialHomeQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PatchPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PostPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialHomePostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-home_api_core-ackn_test-module_v1n10",
        displayName = "Ensure that a Patrimonial Home quotation request can be successfully created and accepted afterwards.",
        summary = "Ensure that a Patrimonial Home quotation request can be successfully created and accepted afterwards. This test applies to both Personal and Business products. If the \"brazilCnpj\" field is filled out, it will transmit business information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST home/request endpoint sending personal or business information, following what is defined at the config\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Poll the GET home/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET home/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate response and ensure status is ACPT\n" +
                "\u2022 Call PATCH home/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN\n" +
                "\u2022 Expect 200 - Validate response and ensure status is ACKN\n" +
                "\u2022 Call GET links.redirect endpoint\n" +
                "\u2022 Expect 200",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialHomeCoreAcknTestModuleV1n10 extends AbstractOpinQuotePatrimonialHomeCoreAcknTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialHomeQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialHomePostRequestBodyWithCoverage.class;
    }
}
