package net.openid.conformance.opin.testmodule.phase3.contractLifePension.v1n13;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.phase3.contractLifePension.AbstractOpinContractLifePensionNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.PostContractLifePensionOASValidatorV1n13;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-contract-life-pension_api_negative-quote_test-module_v1.13.0",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = """
                Ensure a consent cannot be created in unhappy requests.
                • Call POST /request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload
                • Expect 400 or 422 - Validate Error Response
                • Call POST request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with "contract-life-pension-lead" scope
                • Expect 403 - Validate Error Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinContractLifePensionNegativeTestModuleV1n13 extends AbstractOpinContractLifePensionNegativeTestModule {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return PostContractLifePensionOASValidatorV1n13.class;
    }
}