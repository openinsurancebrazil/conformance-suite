package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class CompareClientIdWithSsaResponse extends AbstractCondition {
    @Override
    @PreEnvironment(required = {"config", "software_statement_assertion"})
    public Environment evaluate(Environment env) {
        String expectedSsId = Optional.ofNullable(env.getString("software_statement_assertion", "claims.software_id"))
                .orElseThrow(() -> error("Could not extract Software Statement ID from the Software Statement Assertion Directory response"));

        String actualSsId = Optional.ofNullable(env.getString("config", "directory.client_id"))
                .orElseThrow(() -> error("Could not find client id in the environment"));

        if(!expectedSsId.equals(actualSsId)){
            throw error("Software Statement IDs are not equal", args("expected", expectedSsId, "actual", actualSsId));
        }

        logSuccess("Software Statement IDs are equal");
        return env;
    }
}
