package net.openid.conformance.opin.testmodule.support.directory;


import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetCapitalizationTitleV1Endpoint extends AbstractGetXFromAuthServer {

    @Override
    protected String getEndpointRegex() {
        return "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-capitalization-title\\/v\\d+/insurance-capitalization-title/plans)$";
    }

    @Override
    protected String getApiFamilyType() {
        return "insurance-capitalization-title";
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(1.[0-9].[0-9])$";
    }

    @Override
    protected boolean isResource() {
        return true;
    }
}
