package net.openid.conformance.opin.testmodule.support.oasValidators.housing;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetHousingOASValidatorV1 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/housing.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/housing";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
