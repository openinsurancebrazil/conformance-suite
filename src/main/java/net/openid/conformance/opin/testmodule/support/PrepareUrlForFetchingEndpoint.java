package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingEndpoint extends AbstractPrepareUrlForApi {

    private String api;
    private String selectedId;
    private String endpoint;

    @Override
    @PreEnvironment(strings = {"selected_id", "api", "endpoint"})
    public Environment evaluate(Environment env) {
        selectedId = env.getString("selected_id");
        api = env.getString("api");
        endpoint = env.getString("endpoint");
        return super.evaluate(env);
    }

    @Override
    protected String getApiReplacement() {
        return api;
    }

    @Override
    protected String getEndpointReplacement() {
        return String.format("%s/%s/%s", api, selectedId, endpoint);
    }
}
