package net.openid.conformance.opin.testmodule.support;

public abstract class OpinStructuralResourceBuilder extends AbstractBuildConfigResourceUrlFromConsentUrl {

	private String api;

	public void setApi(String api) {
		this.api = api;
	}

	@Override
	protected String getApiReplacement() {
		return api;
	}

	@Override
	protected String getEndpointReplacement() {
		return api;
	}
}


