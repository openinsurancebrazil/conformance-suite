package net.openid.conformance.opin.testmodule.phase1.capitalization;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase1.AbstractOpenDataDynamicVersionTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitle.v1n3.GetCapitalizationTitleOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - ProductsNServices - Capitalization Title API test",
	displayName = "Validate structure of ProductsNServices - Capitalization Title API Api resources",
	summary = "Validate if the response for the API is on the swagger\n" +
			"• Call the GET Endpoint with the x-v header for the intended version\n" +
			"• Expects 200 - Validate response and validate if the x-v header on the response match what was requested\n" +
			"• Call the GET Endpoint with the x-v header for the intended version, and x-min-v as the intended version\n" +
			"• Expects 200 - Validate response and validate if the x-v header on the response match what was requested\n" +
			"• Call the GET Endpoint with the x-v header for the intended version, and x-min-v as “5.0.0”\n" +
			"• Expects 200 - Validate response and validate if the x-v header on the response match what was requested\n" +
			"• Call the GET Endpoint with no headers\n" +
			"• Expects 200 - Do not validate the response, and check if x-v header response is equal or greater than the version being tested\n" +
			"• Call the GET endpoint with the x-v header as “5.0.0”\n" +
			"• Expects 406 - Validate Error response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)

public class CapitalizationTitleApiTestModule extends AbstractOpenDataDynamicVersionTestModule {

	private final String headerVersion = "1.3.0";
	private final String minVersion = "1.3.0";
	private final String apiVersion = "1.3.0";

	@Override
	protected Class<? extends Condition> getValidator() {
		return GetCapitalizationTitleOASValidatorV1n3.class;
	}

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
		setVersionsInEnv(headerVersion,minVersion,apiVersion);
		super.configure(config,baseUrl,externalUrlOverride);
	}

}