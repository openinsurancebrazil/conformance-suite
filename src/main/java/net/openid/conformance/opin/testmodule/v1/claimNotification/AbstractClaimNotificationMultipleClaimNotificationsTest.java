package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.opin.testmodule.support.CreateClaimNotificationRequestBody;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForClaimNotification;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationDiffOccurrenceDescription;

public abstract class AbstractClaimNotificationMultipleClaimNotificationsTest extends AbstractClaimNotificationHappyPathTest {

    @Override
    protected void executeTest() {
        super.executeTest();
        postRepeatedClaimNotification();
        validateErrorResponse();
    }

    protected void validateErrorResponse() {
        runInBlock("Validate response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        });
    }

    protected void postRepeatedClaimNotification() {
        eventLog.startBlock("POST claim notification - Expecting 401");
        callAndStopOnFailure(PrepareUrlForClaimNotification.class);
        createClaimNotificationRequestBody();
        callAndStopOnFailure(SetClaimNotificationDiffOccurrenceDescription.class);
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        setIdempotencyKey();
        setupPostClaimNotification();
        eventLog.endBlock();
    }
}
