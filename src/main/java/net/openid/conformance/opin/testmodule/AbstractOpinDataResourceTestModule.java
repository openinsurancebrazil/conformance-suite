package net.openid.conformance.opin.testmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;

@Deprecated
/**
 * @deprecated
 * This class is deprecated use {@link AbstractOpinApiResourcesTestModuleV2} instead
 */
public abstract class AbstractOpinDataResourceTestModule extends AbstractOpinFunctionalTestModule {

	protected Class<? extends Condition> rootValidator;
	private String resourcesType;
	private String resourceStatus;

	public static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(AddResourcesScope.class);
	}
	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PolicyIDAllSelector.class);

		callAndStopOnFailure(BuildOpinResourcesConfigResourceUrlFromConsentUrl.class);
		callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
		preCallProtectedResource("Call Resources API");

		runInBlock("Validate Resources response", () -> {
			callAndStopOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);
		});

		eventLog.startBlock("Compare active resourceId's with API resources");
		env.putString("resource_type", resourcesType);
		env.putString("resource_status", resourceStatus);
		callAndStopOnFailure(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.class);
		callAndStopOnFailure(CompareResourceIdsWithSelectedIds.class);
		eventLog.endBlock();
	}


	public void setResourcesType(String resourcesType) {
		this.resourcesType = resourcesType;
	}

	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	public void setRootValidator(Class<? extends Condition> rootValidator) {
		this.rootValidator = rootValidator;
	}

}
