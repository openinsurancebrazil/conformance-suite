package net.openid.conformance.opin.testmodule.support.quoteCapitalizationTitleRaffleRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.AbstractCreateCapitalizationTitleXConsentBody;
import net.openid.conformance.testmodule.Environment;

public class CreateQuoteCapitalizationTitleRaffleConsentBody extends AbstractCreateCapitalizationTitleXConsentBody {

    @Override
    @PreEnvironment(required = {"consent_endpoint_request"})
    public Environment evaluate(Environment env) {
        JsonObject consentData = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();
        JsonObject raffleCaptalizationTitleInformation = new JsonObject();
        raffleCaptalizationTitleInformation.addProperty("contactType", "EMAIL");
        raffleCaptalizationTitleInformation.addProperty("email", "contact@email.com");

        consentData.add("raffleCaptalizationTitleInformation", raffleCaptalizationTitleInformation);
        logSuccess("Consent body created and added", args("consent_endpoint_request", consentData));
        env.putString("contactType", "EMAIL");
        return env;
    }

    @Override
    protected void editData(Environment env, JsonObject data) {

    }
}