package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuoteTransportLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "ACIDENTES_PESSOAIS_COM_PASSAGEIROS";
    }
}