package net.openid.conformance.opin.testmodule.fvp;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.AddOpenIdScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

@PublishTestModule(
    testName = "opin-fvp-consents-api-test-module",
    displayName = "opin-fvp-consents-api-test-module",
    summary = "Validates that consent API accepts the consent groups\n" +
        "• Verify APIs supported by the server being tested in the directory, based on the well-known informed\n" +
        "• Call POST consent for each API supported by the server with the respective permission group\n" +
        "• Expect the server to return a 201 for each request - Validate the response and ensure that the permissions are not widened",
    profile = OBBProfile.OBB_PROFILE,
    configurationFields = {
        "server.discoveryUrl",
        "server.authorisationServerId",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "resource.brazilCnpj",
        "resource.brazilCpf",
        "consent.productType"
    }
)
public class OpinFvpConsentsApiTestModule extends AbstractOpinFunctionalTestModule {

    private boolean passed = false;
    private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        super.onConfigure(config, baseUrl);
        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
    }

    @Override
    protected void configureClient(){
        callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
        callAndStopOnFailure(GetOpinConsentV2Endpoint.class);
        env.putString("api_type", API_TYPE);
        callAndStopOnFailure(GetStaticClientConfiguration.class);
        callAndStopOnFailure(AddOpenIdScope.class);
        exposeEnvString("client_id");
        callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
        callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);
        validateClientConfiguration();
    }

    @Override
    protected void performPreAuthorizationSteps() {
        runInBlock("Create client_credentials access token", () -> {
            call(new ObtainAccessTokenWithClientCredentials(addTokenEndpointClientAuthentication));

        });

        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);

        callAndStopOnFailure(CheckPhase2PermissionGroupsToBeTested.class);
        JsonArray permissionGroups = env.getElementFromObject("permission_groups", "permissionGroups").getAsJsonArray();
        for (JsonElement permissionGroupElement : permissionGroups) {
            PermissionsGroup permissionsGroup = PermissionsGroup.valueOf(OIDFJSON.getString(permissionGroupElement));
            permissionsBuilder.resetPermissions().addPermissionsGroup(permissionsGroup).build();
            validatePermissions(permissionsGroup);
        }

        if (!passed) {
            throw new TestFailureException(getId(), "All resources returned a 422 when at least one set of permissions should have passed");
        }

        fireTestFinished();
    }

    private void validatePermissions(PermissionsGroup permissionsGroup) {
        String logMessage = String.format("Validate consent api request for '%s' permission group(s)", permissionsGroup.name());
        runInBlock(logMessage, () -> {

            callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
            callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
            callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
            callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
            callAndStopOnFailure(PrepareToPostConsentRequest.class);
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(FAPIBrazilOpenInsuranceCreateConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);

            callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.SUCCESS);
            call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));

            String body = env.getString("resource_endpoint_response_full", "body");
            if(Strings.isNullOrEmpty(body)) {
                throw new TestFailureException(getId(),"Empty body from endpoint");
            }

            //if resource_endpoint_response isn't empty we also need to check if error 422 wasn't given for other reason
            if (!body.equals("{}") && env.getInteger("resource_endpoint_response_full", "status") != HttpStatus.UNPROCESSABLE_ENTITY.value()) {
                passed = true;
                callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
                callAndContinueOnFailure(PostConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);
                callAndContinueOnFailure(ValidateRequestedPermissionsAreNotWidened.class, Condition.ConditionResult.FAILURE);
            } else {
                callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
                callAndContinueOnFailure(PostConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);
            }
        });
    }

    @Override
    protected void setupResourceEndpoint() {
        callAndStopOnFailure(GetResourceEndpointConfiguration.class);
    }

    @Override
    protected void requestProtectedResource() {}

    @Override
    protected void validateResponse() {}
}
