package net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n13.lead;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Contract Life Pension 1.13.0")
public class PostContractLifePensionLeadOASValidatorV1n13 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/contractLifePension/contract-life-pension-1.13.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lead/request";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }
}