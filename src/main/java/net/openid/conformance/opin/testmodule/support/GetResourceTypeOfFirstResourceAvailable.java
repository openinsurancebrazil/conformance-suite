package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class GetResourceTypeOfFirstResourceAvailable extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_endpoint_response_full")
    @PostEnvironment(strings = "firstResourceType")
    public Environment evaluate(Environment env) {

        boolean resourceFound = false;
        JsonObject response = env.getObject("resource_endpoint_response_full");
        String bodyString = OIDFJSON.getString(Optional.ofNullable(response.get("body"))
                .orElseThrow(() -> error("body not present in the api response.")));
        JsonObject body = JsonParser.parseString(bodyString).getAsJsonObject();
        JsonElement data = body.get("data");

        if (data.isJsonArray()) {

            for(JsonElement type : data.getAsJsonArray()) {
                String permissionType = OIDFJSON.getString(Optional.ofNullable(
                                type.getAsJsonObject().get("type"))
                        .orElseThrow(() -> error("Resource does not have type field.")));

                if(permissionType.startsWith("DAMAGES_AND_PEOPLE") || permissionType.startsWith("CUSTOMERS_")){
                    if(permissionType.startsWith("CUSTOMERS_PERSONAL")){
                        env.putString("firstResourceType", "CUSTOMERS_PERSONAL");
                        logSuccess("firstResourceType extracted from resources API response.", args("firstResourceType", permissionType));
                        resourceFound = true;
                        break;
                    } else if (permissionType.startsWith("CUSTOMERS_BUSINESS")){
                        env.putString("firstResourceType", "CUSTOMERS_BUSINESS");
                        logSuccess("firstResourceType extracted from resources API response.", args("firstResourceType", permissionType));
                        resourceFound = true;
                        break;
                    }
                    env.putString("firstResourceType", permissionType);
                    logSuccess("firstResourceType extracted from resources API response.", args("firstResourceType", permissionType));
                    resourceFound = true;
                    break;
                }

            }

        } else {
            throw error("data object is not a JsonArray.", args("data", data));
        }

        if(!resourceFound){
            throw error("No Valid Resource Type Found");
        }

        return env;

    }
}
