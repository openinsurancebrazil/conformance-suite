package net.openid.conformance.opin.testmodule.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.opin.testmodule.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinPhase3QuoteNegativeConsentTestModule extends OpinAbstractClientCredentialsGrantFunctionalTestModule {

    protected abstract PermissionsGroup getPermissionsGroup();

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        super.preConfigure(config, baseUrl, externalUrlOverride);
        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
    }

    @Override
    protected void runTests() {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);

        permissionsBuilder.addPermissionsGroup(PermissionsGroup.QUOTE_PATRIMONIAL_LEAD);
        String productType = env.getString("config", "consent.productType");
        if(productType != null && productType.equals("business")) {
            permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS);
        } else {
            permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL);
        }
        permissionsBuilder.build();
        postConsentExpectingFailure("Call POST Consents with permission for Customer Data (PF or PJ) and Patrimonial Lead");

        permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.QUOTE_PATRIMONIAL_LEAD).addPermissionsGroup(getPermissionsGroup()).build();
        postConsentExpectingFailure("Call POST Consents with permission for Patrimonial Lead and the permission for this specified Patrimonial Branch");

        permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.QUOTE_PATRIMONIAL_LEAD).build();
        postConsentExpectingFailure("Call POST Consents with permission for Patrimonial Lead");

    }

    protected void postConsentExpectingFailure(String description) {
        runInBlock(description, () -> {
            call(setHeadersSequence());
            setIdempotencyKey();
            callAndStopOnFailure(PrepareToPostConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
            callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
            env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
            callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
            postConsentValidator();
            env.unmapKey("resource_endpoint_response_full");
        });
    }

    protected void postConsentValidator() {
        callAndStopOnFailure(PostConsentsOASValidatorV2n6.class);
    }

    protected ConditionSequence setHeadersSequence() {
        return sequenceOf(
                condition(CreateEmptyResourceEndpointRequestHeaders.class),
                condition(AddJsonAcceptHeaderRequest.class),
                condition(AddFAPIAuthDateToResourceEndpointRequest.class),
                condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
                condition(CreateRandomFAPIInteractionId.class),
                condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
                condition(ClearRequestObjectFromEnvironment.class)
        );
    }

    protected void setIdempotencyKey() {
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
    }
}
