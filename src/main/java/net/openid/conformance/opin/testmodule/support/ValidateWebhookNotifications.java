package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.Instant;
import java.util.Optional;

public class ValidateWebhookNotifications extends AbstractCondition {
    @Override
    @PreEnvironment(strings = {"webhook_type", "waiting_webhook_since"})
    public Environment evaluate(Environment env) {

        Instant waitingSince = Instant.parse(env.getString("waiting_webhook_since"));
        String webhookType = env.getString("webhook_type");
        int expectedNumberOfNotifications = Optional.ofNullable(env.getInteger("expected_number_of_webhooks")).orElse(1);

        JsonObject webhook = env.getObject("webhook");
        if(webhook == null) {
            throw error("No webhook received");
        }
        JsonArray webhookNotifications = webhook.getAsJsonArray("notifications");
        if(webhookNotifications.size() != expectedNumberOfNotifications) {
            throw error("Invalid number of webhook notifications received", args(
                    "expected", expectedNumberOfNotifications,
                    "received", webhookNotifications.size()
            ));
        }

        for(JsonElement notification : webhookNotifications) {
            validateNotification(notification.getAsJsonObject(), webhookType, waitingSince);
        }

        return env;
    }

    private void validateNotification(JsonObject notification, String webhookType, Instant waitingSince) {
        if(!webhookType.equals(notification.get("type").getAsString())) {
            throw error("Invalid webhook type received");
        }

        Instant receivedAt = Instant.parse(notification.get("received_at").getAsString());
        if(waitingSince.isAfter(receivedAt)) {
            throw error("Webhook arrived before waiting for it");
        }
    }
}
