package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.opin.testmodule.AbstractOpinPhase3TestModule;
import net.openid.conformance.opin.testmodule.phase3.ClearIdempotencyKeyHeader;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.directory.GetInsuranceLifePensionV1Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.GetConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4.GetInsuranceLifePensionContractInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4.GetInsuranceLifePensionContractsOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.AbstractCreatePensionWithdrawalConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody.CreatePensionWithdrawalTotalConsentBody;
import net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody.AbstractCreatePensionWithdrawalRequestBody;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractOpinPhase3PensionWithdrawTestModule extends AbstractOpinPhase3TestModule {

    protected boolean callSecondResource = false;

    protected String getApi() {
        return "insurance-life-pension";
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        env.putString("api", getApi());
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
        permissionsBuilder.addPermissionsGroup(getPermissionsGroup()).build();
    }

    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.LIFE_PENSION;
    }


    @Override
    protected void configureClient() {
        callAndStopOnFailure(OpinInsertMtlsCa.class);
        callAndStopOnFailure(GetStaticClientConfiguration.class);
        ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
        scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.OPEN_ID).build();
        exposeEnvString("client_id");
        // Test won't pass without MATLS, but we'll try anyway (for now)
        callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
        callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);
        validateClientConfiguration();

        callAndStopOnFailure(AddLifePensionScope.class);
        env.putString("api_type", getApi());
        callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
        callAndStopOnFailure(GetInsuranceLifePensionV1Endpoint.class);
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertAfter(CreateEmptyResourceEndpointRequestHeaders.class,
                        sequenceOf(
                                condition(CreateIdempotencyKey.class),
                                condition(AddIdempotencyKeyHeader.class)
                        )
                )
                .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                        sequenceOf(
                                condition(createPensionWithdrawalConsentBody().getClass()).skipIfObjectMissing("pmbacAmount_1")
                        )
                );
    }

    @Override
    protected void requestProtectedResource() {
        if (isPolicyIdExtractionFlow) {

            String firstEndpoint = "contracts";
            env.putString("endpoint", firstEndpoint);
            runInBlock(String.format("Calling %s %s", getApi(), firstEndpoint), () -> callResource());
            runInBlock(String.format("Validating %s %s", getApi(), firstEndpoint), () -> validate(initialJourneyPlansValidator(), firstEndpoint));
            callAndStopOnFailure(ContractsDataSelector.class);

            String secondEndpoint = "contract-info";
            env.putString("endpoint", secondEndpoint);
            callAndStopOnFailure(PrepareUrlForFetchingEndpoint.class);
            runInBlock(String.format("Calling %s %s", getApi(), secondEndpoint), () -> callResource());
            runInBlock(String.format("Validating %s %s", getApi(), secondEndpoint), () -> validate(initialJourneyPlanInfoValidator(), secondEndpoint));
            callAndStopOnFailure(ContractInfoDataSelector.class);

            if(callSecondResource){
                env.mapKey("certificateId","certificateId2");
                env.putString("selected_id", env.getString("certificateId"));
                callAndStopOnFailure(PrepareUrlForFetchingEndpoint.class);
                runInBlock(String.format("Calling %s %s", getApi(), secondEndpoint), () -> callResource());
                runInBlock(String.format("Validating %s %s", getApi(), secondEndpoint), () -> validate(initialJourneyPlanInfoValidator(), secondEndpoint));
                callAndStopOnFailure(ContractInfoDataSelector.class);
            }
            eventLog.startBlock("Deleting consent");
            call(deleteConsent());
            eventLog.endBlock();
        } else {
            executeTest();
        }
    }

    @Override
    protected void setupSecondFlow() {
        runInBlock("Creating consent with Pension Withdrawal permissions", () -> {
            callAndStopOnFailure(OpinOverrideScopeWithOpenId.class);
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(getPermissionsGroup2()).build();
            callAndStopOnFailure(AddWithdrawalPensionScope.class);
        });
        callAndStopOnFailure(createPensionWithdrawalConsentBody().getClass());
    }

    protected AbstractCreatePensionWithdrawalConsentBody createPensionWithdrawalConsentBody() {
        return new CreatePensionWithdrawalTotalConsentBody();
    }

    protected PermissionsGroup getPermissionsGroup2() {
        return PermissionsGroup.PENSION_WITHDRAWAL;
    }

    protected void callResource() {
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
    }

    protected void validate(Class<? extends Condition> validator, String endpoint) {
        callAndStopOnFailure(getExpectedResponseCode(endpoint));
        callAndContinueOnFailure(CheckForDateHeaderInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
        callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
        callAndContinueOnFailure(EnsureResourceResponseReturnedJsonContentType.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-9", "FAPI1-BASE-6.2.1-10");
        callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
    }

    protected Class<? extends Condition> getExpectedResponseCode(String endpoint){
        return EnsureResourceResponseCodeWas200.class;
    }

    protected abstract AbstractCreatePensionWithdrawalRequestBody createPensionWithdrawalRequestBody();

    protected Class<? extends AbstractCondition> initialJourneyPlansValidator() {
        return GetInsuranceLifePensionContractsOASValidatorV1n4.class;
    }
    protected Class<? extends AbstractCondition> initialJourneyPlanInfoValidator() {
        return GetInsuranceLifePensionContractInfoOASValidatorV1n4.class;
    }

    @Override
    protected void validateGetConsentResponse() {
        callAndContinueOnFailure(GetConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void validateLinks(String responseFull) {
        call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
        env.mapKey("resource_endpoint_response_full", responseFull);

        ConditionSequence sequence = new ValidateSelfEndpoint();

        if (responseFull.contains("consent")) {
            sequence.insertAfter(ClearContentTypeHeaderForResourceEndpointRequest.class , condition(ClearIdempotencyKeyHeader.class));
            sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
        }

        call(sequence);
    }

}
