package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;

import java.util.Optional;
import java.util.Set;

public class ClaimNotificationSelectTwoResourcePolicyIds extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_endpoint_response_full", strings = "claim_notification_endpoint_type")
    public Environment evaluate(Environment env) {
        JsonObject response = env.getObject("resource_endpoint_response_full");
        String bodyString = OIDFJSON.getString(Optional.ofNullable(response.get("body"))
            .orElseThrow(() -> error("body not present in the api response.")));
        JsonObject body = JsonParser.parseString(bodyString).getAsJsonObject();
        JsonElement data = body.get("data");

        Set<String> possibleResourceTypes;
        switch (env.getString("claim_notification_endpoint_type")) {
            case "person":
                possibleResourceTypes = SetUtils.createSet("DAMAGES_AND_PEOPLE_PERSON");
                break;
            case "damage":
                possibleResourceTypes = SetUtils.createSet(
                        "DAMAGES_AND_PEOPLE_PATRIMONIAL, DAMAGES_AND_PEOPLE_RESPONSIBILITY, " +
                                "DAMAGES_AND_PEOPLE_TRANSPORT, DAMAGES_AND_PEOPLE_FINANCIAL_RISKS, " +
                                "DAMAGES_AND_PEOPLE_RURAL, DAMAGES_AND_PEOPLE_AUTO, " +
                                "DAMAGES_AND_PEOPLE_HOUSING, DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD"
                );
                break;
            default:
                throw error("Invalid claim_notification_endpoint_type.");
        }

        if (data.isJsonArray()) {
            String firstPolicyId = "";
            String secondPolicyId = "";
            for (JsonElement element : data.getAsJsonArray()) {
                JsonObject elementObject = element.getAsJsonObject();
                String type = OIDFJSON.getString(elementObject.get("type"));
                if (possibleResourceTypes.contains(type)) {
                     String policyId = OIDFJSON.getString(elementObject.get("resourceId"));
                    if (firstPolicyId.isEmpty()) {
                        firstPolicyId = policyId;
                        env.putString("policyId", policyId);
                        env.putString("first_policy_id", policyId);
                        env.putString("policy_type", type);
                        log("First policyId extracted from resources API response.", args("first_policy_id", policyId));
                    } else if (secondPolicyId.isEmpty()) {
                        secondPolicyId = policyId;
                        env.putString("second_policy_id", policyId);
                        log("Second policyId extracted from resources API response.", args("second_policy_id", policyId));
                    } else {
                        break;
                    }
                }
            }
            if (!firstPolicyId.isEmpty() && !secondPolicyId.isEmpty()) {
                logSuccess("Two resources of the required policy type found", args("first_policy_id", firstPolicyId, "second_policy_id", secondPolicyId));
            } else {
                throw error("Resources response does not contain 2 resources of the required policy type");
            }
        } else {
            throw error("data object is not a JsonArray.", args("data", data));
        }
        return env;
    }
}
