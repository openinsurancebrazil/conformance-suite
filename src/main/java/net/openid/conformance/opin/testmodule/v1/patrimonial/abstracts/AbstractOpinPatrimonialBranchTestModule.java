package net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.v1.patrimonial.PatrimonialBranches;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantNotApplicable;

@VariantNotApplicable(parameter = FAPI1FinalOPProfile.class, values = {"openbanking_uk", "plain_fapi", "consumerdataright_au"})
public abstract class AbstractOpinPatrimonialBranchTestModule extends AbstractOpinFunctionalTestModule {

	private OpinConsentPermissionsBuilder permissionsBuilder;

	private PatrimonialBranches branch;
	public static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	@Override
	protected void configureClient() {
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(BuildPatrimonialConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddPatrimonialScope.class);

		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_PATRIMONIAL);
		permissionsBuilder.build();
	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(getRootValidator(), Condition.ConditionResult.FAILURE);
		getIdSelector();

		fetchUntilBranchIsFound();

		if(!env.getBoolean("branch_found")) {
			env.putString("warning_message", String.format("All policies ID were verified but none matched the branch %s", branch.name()));
			callAndContinueOnFailure(ChuckWarning.class, Condition.ConditionResult.WARNING);
		}
	}

	void fetchUntilBranchIsFound() {

		JsonArray policies = JsonParser.parseString(env.getString("selected_id")).getAsJsonArray();

		call(exec().startBlock(String.format("Start looking for a policyID of type %s", branch.name())));

		env.putBoolean("branch_found", false);
		for(int i = 0; i < policies.size(); i++) {
			if (env.getBoolean("branch_found")) {
				break;
			}
			env.putString("policyId", OIDFJSON.getString(policies.get(i)));
			callAndContinueOnFailure(PrepareUrlForFetchingPatrimonialPolicyInfo.class, Condition.ConditionResult.FAILURE);
			preCallProtectedResource();
			callAndContinueOnFailure(VerifyBranch.class, Condition.ConditionResult.INFO);

			if (i % 10 == 0) {
				call(exec().startBlock(String.format("[%d] PolicyID of type %s still not found, keep looking,", i, branch.name())));
			}
		}
	}
	protected void setBranch(PatrimonialBranches branch) {
		this.branch = branch;
		env.putString("branch", branch.getBranchCode());
	}

	PatrimonialBranches getBranch() {
		return this.branch;
	}

	protected abstract Class<? extends AbstractJsonAssertingCondition> getRootValidator();

	protected void getIdSelector() {
		callAndStopOnFailure(PolicyIDAllSelector.class);
	}
}
