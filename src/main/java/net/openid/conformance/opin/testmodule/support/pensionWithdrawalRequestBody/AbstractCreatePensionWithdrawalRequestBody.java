package net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreatePensionWithdrawalRequestBody extends AbstractCondition {

    @Override
    @PreEnvironment(required = {"config","pmbacAmount_1","contract_1"}, strings = {"certificateId","withdrawalType"})
    @PostEnvironment(required = "resource_request_entity", strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject requestData = new JsonObject();
        JsonObject data = new JsonObject();

        JsonObject pmbacAmount = env.getObject("pmbacAmount_1");
        JsonObject contract = env.getObject("contract_1");

        JsonObject generalInformation = new JsonObject();
        generalInformation.addProperty("productName", contract.get("productName").getAsString());
        generalInformation.addProperty("certificateId",contract.get("certificateId").getAsString());

        JsonObject withdrawalInformation = new JsonObject();
        withdrawalInformation.addProperty("withdrawalReason","5_INSATISFACAO_COM_O_PRODUTO");
        withdrawalInformation.addProperty("withdrawalType", EnumWithdrawalType.findByValue(env.getString("withdrawalType")).toString());
        withdrawalInformation.add("pmbacAmount",pmbacAmount);
        
        data.add("generalInfo", generalInformation);
        data.add("withdrawalInfo", withdrawalInformation);

        editData(env, data);

        requestData.add("data", data);
        logSuccess("The request body was created", args("body", requestData));
        env.putObject("resource_request_entity", requestData);
        env.putString("resource_request_entity", requestData.toString());
        return env;
    }

    protected abstract void editData(Environment env, JsonObject data);
}
