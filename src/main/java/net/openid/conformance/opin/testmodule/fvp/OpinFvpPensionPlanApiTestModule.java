package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddPensionPlanScope;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetPensionPlanV1Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n4.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Map;


@PublishTestModule(
        testName = "opin-fvp-pension-plan-api-test-module_v1",
        displayName = "Validates the structure of Pension Plan API",
        summary ="Validates the structure of Pension Plan API \n" +
                "\u2022 Call the POST consents with all the permissions needed to access the Capitalization Title API (“PENSION_PLAN_READ”, “PENSION_PLAN_CONTRACTINFO_READ”, “PENSION_PLAN_MOVEMENTS_READ”, “PENSION_PLAN_PORTABILITIES_READ”,“PENSION_PLAN_WITHDRAWALS_READ”,“PENSION_PLAN_CLAIM”, “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 -  Validate Response \n" +
                "\u2022 Redirect the user to authorize consent \n" +
                "\u2022 Calls GET Pension Plan contracts Endpoint \n" +
                "\u2022 Expects 200 - Fetches one of the pension Identifications returned \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} contract-info Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} movements Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} portabilities Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} withdrawals Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} claim Endpoint \n" +
                "\u2022 Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.consentSyncTime",
                "consent.productType"
        }
)


public class OpinFvpPensionPlanApiTestModule extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetPensionPlanV1Endpoint.class));
    }
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddPensionPlanScope.class;
    }


    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.PENSION_PLAN;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePensionPlanContractsOASValidatorV1n4.class;
    }

    @Override
    protected String getApi() {
        return "insurance-pension-plan";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].contracts[0].pensionIdentification");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "contract-info", GetInsurancePensionPlanContractInfoOASValidatorV1n4.class,
                "movements", GetInsurancePensionPlanMovementsOASValidatorV1n4.class,
                "portabilities", GetInsurancePensionPlanPortabilitiesOASValidatorV1n4.class,
                "withdrawals", GetInsurancePensionPlanWithdrawalsOASValidatorV1n4.class,
                "claim", GetInsurancePensionPlanClaimOASValidatorV1n4.class
        );
    }

}