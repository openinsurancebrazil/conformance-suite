package net.openid.conformance.opin.testmodule.v1.structural.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1.PostClaimNotificationPersonOASValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
    testName = "opin-claim-notification-person-api-structural-test",
    displayName = "Validate structure of Claim Notification API Person Endpoints 201 response",
    summary = "Validate structure of Claim Notification API Person Endpoints 201 response\n" +
        "\u2022 Call the \"/request/person/{consentID}\" endpoint using GET\n" +
        "\u2022 Expect 200 - validate response",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "resource.consentUrl",
                "resource.consentId"
        }
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinClaimNotificationPersonApiStructuralTest extends AbstractOpinDataStructuralPhase3TestModule {

    @Override
    public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
        setApi("claim-notification");
        setEndpointType("person");
        setValidator(PostClaimNotificationPersonOASValidatorV1.class);
        super.configure(config, baseUrl, externalUrlOverride);
    }
}
