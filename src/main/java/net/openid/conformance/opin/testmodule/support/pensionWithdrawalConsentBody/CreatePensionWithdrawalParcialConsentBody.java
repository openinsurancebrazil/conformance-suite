package net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreatePensionWithdrawalParcialConsentBody extends AbstractCreatePensionWithdrawalConsentBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject desiredTotalAmount = data.get("pmbacAmount").getAsJsonObject().deepCopy();
        String amount = desiredTotalAmount.get("amount").getAsString();
        Float partialAmount = Float.parseFloat(amount);
        partialAmount = partialAmount/2;
        desiredTotalAmount.addProperty("amount", String.format("%.2f",partialAmount).replaceAll(",","."));
        data.add("desiredTotalAmount", desiredTotalAmount);
    }
}
