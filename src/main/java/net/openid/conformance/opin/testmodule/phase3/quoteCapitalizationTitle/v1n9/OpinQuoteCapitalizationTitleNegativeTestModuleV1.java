package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.GetQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyWrongMonthlyPayment;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyWrongMonthlyPaymentWithUnitType;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyWrongSinglePayment;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title_api_negative-quote_test-module_v1",
        displayName = "opin-quote-capitalization-title_api_negative-quote_test-module_v1",
        summary = """
                Ensure a consent cannot be created in unhappy requests.
                • Call POST /request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload
                • Expect 400 or 422 - Validate Error Response
                • Call POST /request endpoint sending personal or business information, following what is defined at the config, paymentType as MENSAL, and send the singlePayment field
                • Expect 400 or 422 - Validate Error Response
                • Call POST /request endpoint sending personal or business information, following what is defined at the config, paymentType as UNICO, and send the monthlyPayment field
                • Expect 400 or 422 - Validate Error Response
                • Call POST request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with "quote-capitalization-title-lead" scope
                • Expect 403 - Validate Error Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteCapitalizationTitleNegativeTestModuleV1 extends AbstractOpinQuoteCapitalizationTitleNegativeTestModule {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return GetQuoteCapitalizationTitleOASValidator.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPayment.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBodyWrongMonthlyPayment() {
        return CreateQuoteCapitalizationTitlePostRequestBodyWrongMonthlyPayment.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBodyWrongSinglePayment() {
        return CreateQuoteCapitalizationTitlePostRequestBodyWrongSinglePayment.class;
    }
}