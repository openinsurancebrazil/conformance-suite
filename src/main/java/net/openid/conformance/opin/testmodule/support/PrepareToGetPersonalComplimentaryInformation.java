package net.openid.conformance.opin.testmodule.support;

public class PrepareToGetPersonalComplimentaryInformation extends AbstractPrepareUrlForApi {

	@Override
	protected String getApiReplacement() {
		return "customers";
	}

	@Override
	protected String getEndpointReplacement() {
		return "personal/complimentary-information";
	}
}
