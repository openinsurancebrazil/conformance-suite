package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;


public abstract class AbstractOpinQuoteCapitalizationTitleLeadApiCoreTestModule extends AbstractOpinPhase3QuoteLeadTestModule{

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_CAPITALIZATION_TITLE_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getApiName() {
        return "quote-capitalization-title";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}