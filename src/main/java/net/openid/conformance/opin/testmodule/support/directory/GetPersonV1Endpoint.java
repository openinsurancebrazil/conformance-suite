package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetPersonV1Endpoint extends AbstractGetXFromAuthServer {

    @Override
    protected String getEndpointRegex() {
        return "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-person\\/v\\d+/insurance-person)$";
    }

    @Override
    protected String getApiFamilyType() {
        return "insurance-person";
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(1.[0-9].[0-9])$";
    }

    @Override
    protected boolean isResource() {
        return true;
    }

}