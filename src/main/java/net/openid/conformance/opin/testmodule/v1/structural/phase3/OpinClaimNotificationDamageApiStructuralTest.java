package net.openid.conformance.opin.testmodule.v1.structural.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1.PostClaimNotificationDamageOASValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
    testName = "opin-claim-notification-damage-api-structural-test",
    displayName = "Validate structure of Claim Notification API Damage Endpoints 201 response",
    summary = "Validate structure of Claim Notification API Damage Endpoints 201 response\n" +
        "\u2022 Call the \"/request/damage/{consentID}\" endpoint using GET\n" +
        "\u2022 Expect 200 - validate response",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "resource.consentUrl",
                "resource.consentId"
        }
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinClaimNotificationDamageApiStructuralTest extends AbstractOpinDataStructuralPhase3TestModule {

    @Override
    public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
        setApi("claim-notification");
        setEndpointType("damages");
        setValidator(PostClaimNotificationDamageOASValidatorV1.class);
        super.configure(config, baseUrl, externalUrlOverride);
    }
}
