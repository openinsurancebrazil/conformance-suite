package net.openid.conformance.opin.testmodule.v1.pensionPlan;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n4.*;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.abstracts.AbstractPensionPlanAPICoreTestModule;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
        testName = "pension-plan_api_core_test-module_v1",
        displayName = "Validates the structure of Pension Plan API",
        summary ="Validates the structure of Pension Plan API \n" +
                "\u2022 Call the POST consents with all the permissions needed to access the Capitalization Title API (“PENSION_PLAN_READ”, “PENSION_PLAN_CONTRACTINFO_READ”, “PENSION_PLAN_MOVEMENTS_READ”, “PENSION_PLAN_PORTABILITIES_READ”,“PENSION_PLAN_WITHDRAWALS_READ”,“PENSION_PLAN_CLAIM”, “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 -  Validate Response \n" +
                "\u2022 Redirect the user to authorize consent \n" +
                "\u2022 Calls GET Pension Plan contracts Endpoint \n" +
                "\u2022 Expects 200 - Fetches one of the pension Identifications returned \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} contract-info Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} movements Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} portabilities Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} withdrawals Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Pension Plan {pensionIdentification} claim Endpoint \n" +
                "\u2022 Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class PensionPlanAPICoreTestModule extends AbstractPensionPlanAPICoreTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePensionPlanContractsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator() {
        return GetInsurancePensionPlanContractInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getMovementsValidator() {
        return GetInsurancePensionPlanMovementsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator() {
        return GetInsurancePensionPlanPortabilitiesOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator() {
        return GetInsurancePensionPlanWithdrawalsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsurancePensionPlanClaimOASValidatorV1n4.class;
    }
}
