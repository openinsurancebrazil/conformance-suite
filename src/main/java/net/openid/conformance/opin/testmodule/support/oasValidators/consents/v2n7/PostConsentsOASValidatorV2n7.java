package net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Consents 2.7.0")
public class PostConsentsOASValidatorV2n7 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/consents/v2n7/swagger-consents-api-2.7.0.yaml";
    }
    @Override
    protected String getEndpointPath() {
        return "/consents";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }

    @Override
    protected ResponseEnvKey getResponseEnvKey(){
        return ResponseEnvKey.FullConsentResponseEnvKey;
    }

}
