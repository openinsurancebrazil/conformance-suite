package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Financial Risk 1.3.1")
public class GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceFinancialRisk/v1/swagger-insurance-financial-risk.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-financial-risk/{policyId}/policy-info";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
