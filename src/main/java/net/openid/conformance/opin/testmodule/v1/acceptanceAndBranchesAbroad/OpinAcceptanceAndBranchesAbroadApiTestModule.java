package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n3.GetInsuranceAcceptanceAndBranchesAbroadClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n3.GetInsuranceAcceptanceAndBranchesAbroadListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n3.GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n3.GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.abstracts.AbstractOpinAcceptanceAndBranchesAbroadApiTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-acceptance-and-branches-abroad-api-test",
	displayName = "Validates the structure of all financial Acceptance and Branches Abroad API resources",
	summary = "Validates the structure of all financial Acceptance and Branches Abroad API resources\n" +
		"\u2022 (“DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinAcceptanceAndBranchesAbroadApiTestModule extends AbstractOpinAcceptanceAndBranchesAbroadApiTestModule {


	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadPremiumOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadClaimOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsuranceAcceptanceAndBranchesAbroadListOASValidatorV1n3.class;
	}
}