package net.openid.conformance.opin.testmodule.phase1.transport;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.transport.v1n3.GetTransportOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - Transport API test",
        displayName = "Validate structure of Transport response",
        summary = "Validate structure of Transport response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class TransportApiTestModule extends AbstractNoAuthFunctionalTestModule {
    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices Transport response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetTransportOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
        });
    }
}
