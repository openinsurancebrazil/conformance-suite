package net.openid.conformance.opin.testmodule.v1.housing.v1n4;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n4.GetInsuranceHousingClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n4.GetInsuranceHousingListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n4.GetInsuranceHousingPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n4.GetInsuranceHousingPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.housing.abstracts.AbstractOpinHousingWrongPermissionsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-housing-api-wrong-permissions-test-v1.4.0",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
			"Creates a consent with all the permissions needed to access the housing API (“DAMAGES_AND_PEOPLE_HOUSING_READ”, “DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"• Calls GET housing “/” API \n" +
		"• Expects 201 - Fetches one of the Policy IDs returned \n" +
		"• Calls GET housing policy-Info API specifying an Policy ID \n" +
		"• Expects 200 - Validate all the fields \n" +
		"• Calls GET housing premium API specifying an Policy ID \n" +
		"• Expects 200- Validate all the fields \n" +
		"• Calls GET housing claim API specifying an Policy ID \n" +
		"• Expects 200- Validate all the fields \n" +
		"• Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field " +
		"• Expects a success 201 - Expects a success on Redirect as well \n" +
		"• Calls GET housing “/” API \n" +
		"• Expects a 403 response  - Validate error response \n" +
		"• Calls GET housing policy-Info API specifying an Policy ID \n" +
		"• Expects a 403 response  - Validate error response \n" +
		"• Calls GET housing premium API specifying an Policy ID \n" +
		"• Expects a 403 response  - Validate error response \n" +
		"• Calls GET housing claim API specifying an Policy ID \n" +
		"• Expects a 403 response  - Validate error response\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"client.client_id",
			"client.jwks",
			"mtls.key",
			"mtls.cert",
			"resource.consentUrl",
			"resource.brazilCpf",
			"consent.productType"
	}
)
public class OpinHousingWrongPermissionsTestModuleV1n4 extends AbstractOpinHousingWrongPermissionsTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator() {
		return GetInsuranceHousingPolicyInfoOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator() {
		return GetInsuranceHousingPremiumOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator() {
		return GetInsuranceHousingClaimOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsuranceHousingListOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
		return IdSelectorFromJsonPath.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}
