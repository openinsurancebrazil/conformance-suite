package net.openid.conformance.opin.testmodule.v1.rural;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AllIdsSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralListOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-rural-resources-api-test",
        displayName = "Makes sure that the Resource API and the rural API are returning the same available IDs",
        summary = "Makes sure that the Resource API and the rural API are returning the same available IDs\n" +
                "\u2022 Creates a consent with all the permissions needed to access the rural API (“DAMAGES_AND_PEOPLE_RURAL_READ”, “DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects server to create the consent with 201\n" +
                "\u2022 Redirect the user to authorize at the financial institution\n" +
                "\u2022 Call the rural “/” API\n" +
                "\u2022 Expect a success - Validate the fields of the response and Make sure that a policy id is returned - Fetch the policy id provided by this API\n" +
                "\u2022 Call the resources API \n" +
                "\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the policy id provided by this API",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf"
        }
)
public class OpinRuralResourcesApiTestModule extends AbstractOpinRuralResourcesApiTest {
    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceRuralListOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*]");
        return AllIdsSelectorFromJsonPath.class;
    }
}
