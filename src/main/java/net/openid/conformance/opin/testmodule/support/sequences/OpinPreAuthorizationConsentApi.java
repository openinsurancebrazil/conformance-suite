package net.openid.conformance.opin.testmodule.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.opin.testmodule.support.OpinConsentEndpointResponseValidatePermissions;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.sequence.AbstractConditionSequence;
import net.openid.conformance.sequence.ConditionSequence;

public class OpinPreAuthorizationConsentApi extends AbstractConditionSequence {

	private boolean validateResponse;
	private Class<? extends ConditionSequence> addClientAuthenticationToTokenEndpointRequest;

	public OpinPreAuthorizationConsentApi(Class<? extends ConditionSequence> addClientAuthenticationToTokenEndpointRequest) {
		this.addClientAuthenticationToTokenEndpointRequest = addClientAuthenticationToTokenEndpointRequest;
		this.validateResponse = true;
	}

	public OpinPreAuthorizationConsentApi(Class<? extends ConditionSequence> addClientAuthenticationToTokenEndpointRequest, boolean validateResponse) {
		this.addClientAuthenticationToTokenEndpointRequest = addClientAuthenticationToTokenEndpointRequest;
		this.validateResponse = validateResponse;
	}

	@Override
	public void evaluate() {

		call(exec().startBlock("Use client_credentials grant to obtain Brazil consent"));

		/* create client credentials request */

		callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);

		callAndStopOnFailure(SetConsentsScopeOnTokenEndpointRequest.class);

		call(sequence(addClientAuthenticationToTokenEndpointRequest));

		/* get access token */

		callAndStopOnFailure(CallTokenEndpoint.class);

		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);

		callAndStopOnFailure(CheckForAccessTokenValue.class);

		callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);

		callAndContinueOnFailure(ExtractExpiresInFromTokenEndpointResponse.class, "RFC6749-4.4.3", "RFC6749-5.1");

		call(condition(ValidateExpiresIn.class)
			.skipIfObjectMissing("expires_in")
			.onSkip(Condition.ConditionResult.INFO)
			.requirements("RFC6749-5.1")
			.onFail(Condition.ConditionResult.FAILURE)
			.dontStopOnFailure());

		callAndStopOnFailure(SaveConsentsAccessToken.class);

		/* create consent request */
		callAndStopOnFailure(PrepareToPostConsentRequest.class);

		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);

		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);

		callAndStopOnFailure(FAPIBrazilOpenInsuranceCreateConsentRequest.class);

		callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);

		callAndStopOnFailure(SetContentTypeApplicationJson.class);

		callAndStopOnFailure(CreateIdempotencyKey.class);

		callAndStopOnFailure(AddIdempotencyKeyHeader.class);

		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(SaveOldValues.class);
		if (validateResponse) {
			call(exec().mapKey("endpoint_response", "consent_endpoint_response_full"));
			callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
			call(exec().unmapKey("endpoint_response"));

			callAndStopOnFailure(LoadOldValues.class);

			callAndContinueOnFailure(OpinConsentEndpointResponseValidatePermissions.class, Condition.ConditionResult.INFO);

			callAndContinueOnFailure(PostConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);

			callAndStopOnFailure(ExtractConsentIdFromConsentEndpointResponse.class);
			callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
			callAndStopOnFailure(FAPIBrazilAddConsentIdToClientScope.class);
		}
		call(exec().endBlock());
	}
}
