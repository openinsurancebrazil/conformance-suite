package net.openid.conformance.opin.testmodule.support.oasValidators.autoInsurance.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Auto Insurance 1.5.0")
public class GetAutoInsuranceOASValidatorV1n5 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/autoInsurance/v1n5/swagger-auto-insurance-1.5.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/auto-insurance/{vehicleOvernightZipCode}/{fipeCode}/{year}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}