package net.openid.conformance.opin.testmodule.support.oasValidators.channels.referencedNetwork.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api source: swagger/openinsurance/channels/v1/referencedNetwork/swagger-channels-referenced-network-1.4.0.yaml
 * Api endpoint: /referenced-network/{countrySubDivision}/{serviceType}
 * Api version: 1.4.0
 */

@ApiName("Channels Referenced Network")
public class GetChannelsReferencedNetworkOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/channels/v1/referencedNetwork/swagger-channels-referenced-network-1.4.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/referenced-network/{countrySubDivision}/{serviceType}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

}

