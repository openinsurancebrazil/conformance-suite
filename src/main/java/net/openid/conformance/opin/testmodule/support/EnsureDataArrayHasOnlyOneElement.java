package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureDataArrayHasOnlyOneElement extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {

		JsonObject body = JsonParser.parseString(env.getString("resource_endpoint_response_full", "body"))
			.getAsJsonObject();

		if(!body.has("data")) {
			throw error("Data object not found in response body.", args("body", body));
		}

		JsonElement dataElem = body.get("data");
		if (dataElem.isJsonNull()) {
			throw error("Data object cannot be null.", args("body", body));
		}

		JsonArray dataArray = body.getAsJsonArray("data");
		if(dataArray.size() != 1) {
			throw error("Data array size should be 1, but it was not.", args("data", dataArray));
		}

		logSuccess("Data array size is 1.");
		return env;
	}
}
