package net.openid.conformance.opin.testmodule.phase3.quoteHousing;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteHousingLeadApiCoreTest extends AbstractOpinPhase3QuoteLeadTestModule{

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_HOUSING_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-housing";
    }

    @Override
    protected String getApiName() {
        return "quote-housing";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}