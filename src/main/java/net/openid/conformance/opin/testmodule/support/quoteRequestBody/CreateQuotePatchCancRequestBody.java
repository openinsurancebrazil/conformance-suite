package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateQuotePatchCancRequestBody extends AbstractCreateQuotePatchRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        data.addProperty("status", "CANC");
    }
}
