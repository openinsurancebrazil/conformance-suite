package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;


public class GetAcceptanceAndBranchesAbroadV1Endpoint extends AbstractGetXFromAuthServer {

    @Override
    protected String getEndpointRegex() {
        return "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-acceptance-and-branches-abroad\\/v\\d+/insurance-acceptance-and-branches-abroad)$";
    }

    @Override
    protected String getApiFamilyType() {
        return "acceptance-and-branches-abroad";
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(1.[0-9].[0-9])$";
    }

    @Override
    protected boolean isResource() {
        return true;
    }
}
