package net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitleWithdrawal.v1n3;

import com.google.gson.JsonObject;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Capitalization Title Withdrawal 1.3.0")
public class PostCapitalizationTitleWithdrawalOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/capitalizationTitleWithdrawal/v1n3/swagger-capitalization-title-withdrawal-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/capitalization-title/request";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject withdrawalInformation = body.getAsJsonObject("data").get("withdrawalInformation").getAsJsonObject();
		assertWithdrawalInformation(withdrawalInformation);
	}

	protected void assertWithdrawalInformation(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
				data,
				"withdrawalReasonOthers",
				"withdrawalReason",
				"OUTROS"
		);
	}
}