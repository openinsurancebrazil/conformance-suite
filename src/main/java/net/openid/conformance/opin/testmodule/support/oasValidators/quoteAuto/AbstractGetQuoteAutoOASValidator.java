package net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public abstract class AbstractGetQuoteAutoOASValidator extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getEndpointPath() {
        return "/request/{consentId}/quote-status";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
