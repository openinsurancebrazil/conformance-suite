package net.openid.conformance.opin.testmodule.v1.consents.v2n6;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.GetConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentsApiConsentStatusIfDeclinedTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-consent-api-status-declined-test-v2n6",
	displayName = "Validates that consents are rejected on decline",
	summary = "Validates that consents are rejected on the decline\n" +
		"• Call the POST Consents\n" +
		"• Expect 201 - Validate Response\n" +
		"• Redirects the User to Reject the Consent\n" +
		"• Checks if rejection resulted in an error message on the redirect\n" +
		"• Calls the GET Consents\n" +
		"• Expects 200 - Validate Response and Status is REJECTED, rejectedBy is USER and reason is CUSTOMER_MANUALLY_REJECTED",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
public class OpinConsentsApiConsentStatusIfDeclinedTestModuleV2n6 extends AbstractOpinConsentsApiConsentStatusIfDeclinedTestModule {

	@Override
	protected AbstractJsonAssertingCondition postValidator() {
		return new PostConsentsOASValidatorV2n6();
	}

	@Override
	protected AbstractJsonAssertingCondition getValidator() {
		return new GetConsentsOASValidatorV2n6();
	}
}