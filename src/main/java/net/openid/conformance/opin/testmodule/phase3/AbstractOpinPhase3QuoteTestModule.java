package net.openid.conformance.opin.testmodule.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.opin.testmodule.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.directory.GetQuoteResourceV1Endpoint;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinPhase3QuoteTestModule extends OpinAbstractClientCredentialsGrantFunctionalTestModule {
    private static final String API_TYPE =  ApiTypes.DATA_API_PHASE3.toString();

    // We only should patch a quote that was accepted.
    protected boolean shouldPatchQuote = false;

    // We only should check the redirect link if the patch status is ACKN.
    protected boolean shouldCheckRedirectLinkAfterPatch = false;

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        env.putObject("client", config.getAsJsonObject("client"));
        env.putString("client", "scope", "");
        super.preConfigure(config, baseUrl, externalUrlOverride);

        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
        call(new ValidateWellKnownUriSteps());
        callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
        callAndStopOnFailure(CreateRandomConsentID.class);

        ScopesAndPermissionsBuilder builder = new ScopesAndPermissionsBuilder(env,eventLog);
        builder.addScopes(getScope());
        builder.build();
    }

    @Override
    protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
        return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence).replace(
                SetConsentsScopeOnTokenEndpointRequest.class,
                condition(SetClientScopesOnTokenEndpointRequest.class)
        );
    }

    @Override
    protected void runTests() {

        env.putString("api_family_type", getApiFamilyType());
        env.putString("api_name", getApiName());
        env.putString("api_endpoint", getApiBaseEndpoint());
        callQuoteResourceEndpoint();

        this.postQuote();

        this.waitQuote();

        this.getQuote();

        if(shouldPatchQuote) {
            this.patchQuote();
        }

        if(shouldCheckRedirectLinkAfterPatch) {
            this.checkRedirect();
        }
    }

    protected void callQuoteResourceEndpoint() {
        callAndStopOnFailure(GetQuoteResourceV1Endpoint.class);
    }

    protected void postQuote() {
        runInBlock("Create a quote", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            call(setHeadersSequence());
            call(setIdempotencyKey());
            callAndStopOnFailure(createPostQuoteRequestBody());
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(SetResourceMethodToPost.class);
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the quote creation response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas201.class);
            callAndContinueOnFailure(ensurePostQuoteStatus(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validatePostQuoteResponse(), Condition.ConditionResult.FAILURE);
        });
    }

    protected void waitQuote() {
        runInBlock("Poll the quote while its status is RCVD or EVAL", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            env.putString("protected_resource_url", env.getString("protected_resource_url") + String.format("/%s/quote-status", env.getString("consent_id")));
            env.mapKey("consent_endpoint_response_full", "resource_endpoint_response_full");
            repeatSequence(
                    () -> {
                        skipSelfLinkValidation();
                        return sequenceOf(
                                setHeadersSequence(),
                                condition(SetResourceMethodToGet.class),
                                condition(CallProtectedResource.class),
                                condition(EnsureResourceResponseCodeWas200.class),
                                condition(EnsureStatusWasRcvdOrEval.class)
                        );
                    }
            ).untilFalse(EnsureStatusWasRcvdOrEval.STATUS_KEY)
                    .trailingPause(12)
                    // The test should wait for one minute, then times is set to 6.
                    // If it was 5, the 5th iteration, which happens after 48 seconds ( (5-1)*12 = 48 ), would be the last one.
                    // So what happened between 48 and 60 seconds would be ignored.
                    // By setting times to 6, the last iteration happens after 60 seconds which is what we expect.
                    .times(6)
                    .validationSequence(() -> sequenceOf(
                            condition(validateGetQuoteResponse()).dontStopOnFailure(),
                            condition(OpinPaginationValidator.class).dontStopOnFailure()
                    ))
                    .onTimeout(sequenceOf(
                            condition(TestTimedOut.class),
                            condition(ChuckWarning.class)))
                    .run();
            env.unmapKey("consent_endpoint_response_full");
        });
    }

    protected void getQuote() {
        runInBlock("Fetch the quote", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            env.putString("protected_resource_url", env.getString("protected_resource_url") + String.format("/%s/quote-status", env.getString("consent_id")));
            call(setHeadersSequence());
            callAndStopOnFailure(SetResourceMethodToGet.class);
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the quote response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
            callAndContinueOnFailure(ensureGetQuoteStatus(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validateGetQuoteResponse(), Condition.ConditionResult.FAILURE);
            if(ensureGetQuoteStatus() == EnsureStatusWasAcpt.class) {
                shouldPatchQuote = true;
                // The quote was accepted, then we can extract the quote ID.
                callAndStopOnFailure(ExtractQuoteId.class);
            }
        });
    }

    protected void patchQuote() {
        runInBlock("Patch the quote", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            env.putString("protected_resource_url", env.getString("protected_resource_url") + "/" + env.getString("consent_id"));
            call(setHeadersSequence());
            callAndStopOnFailure(createPatchQuoteRequestBody());
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(SetResourceMethodToPatch.class);
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the patch quote response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(ensurePatchQuoteStatus(), Condition.ConditionResult.FAILURE);
            if(ensurePatchQuoteStatus() == EnsureStatusWasAckn.class) {
                // The quote was acknowledged, then we can check the response redirect link.
                shouldCheckRedirectLinkAfterPatch = true;
            }
            callAndContinueOnFailure(validatePatchQuoteResponse(), Condition.ConditionResult.FAILURE);
        });
    }

    protected void checkRedirect() {
        runInBlock("Call the redirect link", () -> {
            callAndContinueOnFailure(SetProtectedResourceUrlAsLinksRedirect.class, Condition.ConditionResult.FAILURE);
            call(setHeadersSequence());
            callAndStopOnFailure(SetResourceMethodToGet.class);
            callAndStopOnFailure(CallResource.class);
        });
        runInBlock("Validate the redirect link response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
        });
    }

    protected ConditionSequence setHeadersSequence() {
        return sequenceOf(
                condition(CreateEmptyResourceEndpointRequestHeaders.class),
                condition(AddJsonAcceptHeaderRequest.class),
                condition(AddFAPIAuthDateToResourceEndpointRequest.class),
                condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
                condition(CreateRandomFAPIInteractionId.class),
                condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
                condition(ClearRequestObjectFromEnvironment.class)
        );
    }

    protected ConditionSequence setIdempotencyKey() {
        return sequenceOf(
            condition(CreateIdempotencyKey.class),
            condition(AddIdempotencyKeyHeader.class)
        );
    }

    /** Abstract Methods */

    // Meta Data
    protected abstract OPINScopesEnum getScope();
    protected abstract String getApiFamilyType();
    protected abstract String getApiName();
    protected abstract String getApiBaseEndpoint();

    // Post.
    protected abstract Class<? extends AbstractCondition> createPostQuoteRequestBody();
    protected abstract Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus();
    protected abstract Class<? extends AbstractCondition> validatePostQuoteResponse();

    // Get.
    protected abstract Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus();
    protected abstract Class<? extends AbstractCondition> validateGetQuoteResponse();

    // Patch.
    protected abstract Class<? extends AbstractCondition> createPatchQuoteRequestBody();
    protected abstract Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus();
    protected abstract Class<? extends AbstractCondition> validatePatchQuoteResponse();

}
