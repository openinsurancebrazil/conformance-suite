package net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Customers 1.5.0")
public class GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/customers/v1/swagger-customers-api.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/business/complimentary-information";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
