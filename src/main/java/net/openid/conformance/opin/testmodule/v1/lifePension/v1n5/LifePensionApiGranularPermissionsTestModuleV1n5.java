package net.openid.conformance.opin.testmodule.v1.lifePension.v1n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n5.*;
import net.openid.conformance.opin.testmodule.v1.lifePension.AbstractLifePensionApiGranularPermissionsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "life-pension_api_granular-permissions_test-module_v1n5",
        displayName = "Ensure specific methods can only be called if the respective permissions is granted.",
        summary = "Ensure specific methods can only be called if the respective permissions is granted.\n" +
                "• Call the POST Consents with the RESOURCES_READ, LIFE_PENSION_READ and LIFE_PENSION_CONTRACTINFO_READ permissions\n" +
                "• Expects 201 -  Validate Response\n" +
                "• Redirects the user to Authorize consent\n" +
                "• Calls GET Life Pension contracts Endpoint \n" +
                "• Expects 200 - Fetches one of the certificate IDs returned \n" +
                "• Calls GET Life Pension {certificateId} contract-info Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Life Pension {certificateId} movements Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} portabilities Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} withdrawals Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Life Pension {certificateId} claim Endpoint \n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class LifePensionApiGranularPermissionsTestModuleV1n5 extends AbstractLifePensionApiGranularPermissionsTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceLifePensionContractsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator() {
        return GetInsuranceLifePensionContractInfoOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getMovementsValidator() {
        return GetInsuranceLifePensionMovementsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator() {
        return GetInsuranceLifePensionPortabilitiesOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator() {
        return GetInsuranceLifePensionWithdrawalsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsuranceLifePensionClaimOASValidatorV1n5.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }

}
