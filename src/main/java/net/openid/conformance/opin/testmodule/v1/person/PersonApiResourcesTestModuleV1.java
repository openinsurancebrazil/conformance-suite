package net.openid.conformance.opin.testmodule.v1.person;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5.GetInsurancePersonListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.v1.person.abstracts.AbstractPersonApiResourcesTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "person_api_resources_test-module_v1",
        displayName = "Makes sure that the Resource API and the Person API are returning the same available IDs",
        summary = "Makes sure that the Resource API and the Person API are returning the same available IDs\n" +
                "• Call the POST consents with all the permissions needed to access the Person API (“DAMAGES_AND_PEOPLE_PERSON_READ”, “DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PERSON_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PERSON_CLAIM_READ”, “RESOURCES_READ”)\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Person \"/\"  Endpoint\n" +
                "• Validate response of the response and make sure that an id is returned - Fetch the policy id provided by this API\n" +
                "• Call the resources API\n" +
                "• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class PersonApiResourcesTestModuleV1 extends AbstractPersonApiResourcesTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePersonListOASValidatorV1n5.class;
    }
}
