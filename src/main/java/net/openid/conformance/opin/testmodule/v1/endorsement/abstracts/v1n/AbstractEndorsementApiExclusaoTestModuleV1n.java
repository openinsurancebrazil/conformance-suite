package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n;

import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyExclusaoDiff;

public abstract class AbstractEndorsementApiExclusaoTestModuleV1n extends AbstractEndorsementApiHappyTestModuleV1n {

    @Override
    protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
        return new CreateEndorsementRequestBodyExclusaoDiff();
    }

    @Override
    protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
        return CreateEndorsementConsentBodyExclusaoDiff.class;
    }
}