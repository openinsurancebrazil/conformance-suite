package net.openid.conformance.opin.testmodule.support;

import java.util.List;

public class EnsureStatusWasAckn extends AbstractEnsureStatusWasX {
    @Override
    protected List<String> getStatuses() {
        return List.of("ACKN");
    }
}
