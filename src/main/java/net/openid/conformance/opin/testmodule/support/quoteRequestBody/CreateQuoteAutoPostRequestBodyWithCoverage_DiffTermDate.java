package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CreateQuoteAutoPostRequestBodyWithCoverage_DiffTermDate extends AbstractCreateQuotePostRequestBody {

    @Override
    protected String getCode(){
        return "CASCO_COMPREENSIVA";
    }

    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject quoteData = data.getAsJsonObject("quoteData");
        quoteData.remove("maxLMG");
        quoteData.remove("policyId");
        quoteData.remove("insurerId");
        quoteData.addProperty("termType", "ANUAL");

        var today = ZonedDateTime
                .now(ZoneOffset.UTC)
                .toLocalDateTime();
        quoteData.addProperty("termStartDate", today
                .plusDays(1)
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
        quoteData.addProperty("termEndDate", today
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));

        JsonArray coverages = getCoverages();

        quoteData.add("coverages", coverages);
    }
}
