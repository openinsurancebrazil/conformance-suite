package net.openid.conformance.opin.testmodule.support.oasValidators.pensionPlan.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("PensionPlan 1.3.0")
public class GetPensionPlanOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/pensionPlan/v1n3/swagger-pension-plan-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/pension-plan";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}