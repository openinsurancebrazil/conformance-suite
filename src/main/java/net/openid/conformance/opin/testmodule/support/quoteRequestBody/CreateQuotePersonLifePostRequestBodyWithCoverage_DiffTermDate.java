package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CreateQuotePersonLifePostRequestBodyWithCoverage_DiffTermDate extends AbstractCreateQuotePostRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        this.addQuoteData(env, data);
    }

    private void addQuoteData(Environment env, JsonObject data) {
        JsonObject quoteData = new JsonObject();
        var today = ZonedDateTime
                .now(ZoneOffset.UTC)
                .toLocalDateTime();
        quoteData.addProperty("termStartDate", today
                .plusDays(1)
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
        quoteData.addProperty("termEndDate", today
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT)));
        quoteData.addProperty("termType", "ANUAL");
        quoteData.addProperty("includeAssistanceServices", false);

        JsonArray requestedCoverages = getRequestedCoverages();

        quoteData.add("requestedCoverages", requestedCoverages);

        data.add("quoteData", quoteData);
    }

    private JsonArray getRequestedCoverages() {
        JsonArray requestedCoverages = new JsonArray();
        JsonObject requestedCoverage = new JsonObject();
        requestedCoverage.addProperty("branch", "0111");
        requestedCoverage.addProperty("code", "MORTE");
        requestedCoverage.addProperty("isSeparateContractingAllowed", false);
        JsonObject insuredCapital = new JsonObject();
        insuredCapital.addProperty("amount", "90.85");
        insuredCapital.addProperty("unitType", "PORCENTAGEM");
        requestedCoverage.add("insuredCapital", insuredCapital);
        requestedCoverages.add(requestedCoverage);
        return requestedCoverages;
    }
}