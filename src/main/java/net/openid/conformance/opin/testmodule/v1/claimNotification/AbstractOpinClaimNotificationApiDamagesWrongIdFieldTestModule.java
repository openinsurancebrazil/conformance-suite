package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AddDamageEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToWrongType;

public abstract class AbstractOpinClaimNotificationApiDamagesWrongIdFieldTestModule extends AbstractClaimNotificationsNegativeTest {

    @Override
    protected void editClaimNotificationRequestBody() {
        callAndStopOnFailure(SetClaimNotificationIdFieldToWrongType.class);
    }

    @Override
    protected AbstractAddEndpointTypeToEnvironment addEndpointType() {
        return new AddDamageEndpointTypeToEnvironment();
    }
}
