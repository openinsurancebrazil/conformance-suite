package net.openid.conformance.opin.testmodule.fvp.quote;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.fvp.quote.abstracts.AbstractFvpOpinQuoteApiTestModule;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAcpt;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasCanc;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.GetQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.PatchQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.PostQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteAutoPostRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchCancRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-fvp-quote-auto_api_core-canc_test-module",
        displayName = "Ensure that a Auto quotation request can be successfully created and rejected afterwards.",
        summary = """
                Ensure that a Auto quotation request can be successfully created and rejected afterwards. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
                · Call POST auto request endpoint sending personal or business information, following what is defined at the config
                · Expect 201 - Validate Response and ensure status is RCVD
                · Poll the GET quote-status endpoint while status is RCVD or EVAL
                · Call GET quote-status endpoint
                · Expect 200 - Validate Respones and ensure status is ACPT
                · Call PATCH auto request endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC
                · Expect 200 - Validate Response and ensure status is CANC
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinFvpQuoteAutoApiCoreCancTestModule extends AbstractFvpOpinQuoteApiTestModule {

        @Override
        protected OPINScopesEnum getScope() {
                return OPINScopesEnum.QUOTE_AUTO;
        }

        @Override
        protected String getApiFamilyType() {
                return "quote-auto";
        }

        @Override
        protected String getApiName() {
                return "quote-auto";
        }

        @Override
        protected String getApiBaseEndpoint() {
                return "/request";
        }

        @Override
        protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
                return CreateQuoteAutoPostRequestBody.class;
        }

        @Override
        protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
                return EnsureStatusWasRcvd.class;
        }

        @Override
        protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
                return PostQuoteAutoOASValidatorV1n9.class;
        }

        @Override
        protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
                return EnsureStatusWasAcpt.class;
        }

        @Override
        protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
                return GetQuoteAutoOASValidatorV1n9.class;
        }

        @Override
        protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
                return CreateQuotePatchCancRequestBody.class;
        }

        @Override
        protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
                return EnsureStatusWasCanc.class;
        }

        @Override
        protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
                return PatchQuoteAutoOASValidatorV1n9.class;
        }
}
