package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public abstract class AbstractGetInsurancePatrimonialClaimOASValidator extends OpenAPIJsonSchemaValidator{

	@Override
	protected String getEndpointPath() {
		return "/insurance-patrimonial/{policyId}/claim";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
