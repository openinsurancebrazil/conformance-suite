package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleApiCoreDcmDcrWebhookTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.GetQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PatchQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PostQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodySinglePayment;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title_api_core-webhook-drc_test-module_v1",
        displayName = "opin-quote-capitalization-title_api_core-webhook-drc_test-module_v1",
        summary = """
                Ensure that a Capitalization Title quotation request can be successfully created and accepted afterwards, and webhook is sent as required. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
                For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>.
                • Obtain a SSA from the Directory
                • Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration
                • Call the Registration Endpoint, also sending the field "webhook_uris":["https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>"]
                • Expect a 201 - Validate Response
                • Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds
                • Set the consents webhook notification endpoint to be equal to https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>/open-insurance/webhook/v1/quote-capitalization-title/v1/request/{consentId}/quote-status, where the alias is to be obtained from the field alias on the test configuration
                • Call POST /request endpoint sending personal or business information, following what is defined at the config, and the consentId as the webhook defined on the test
                • Expect 201 - Validate Response and ensure status is RCVD
                • Wait for webhook incoming call until 1 minute and fail the test if not received
                • Respond 202 with an empty body, validate if timestamp is after the POST /request call
                • Call GET request/{consentId}/quote-status endpoint
                • Expect 200 - Validate Respones and ensure status is ACPT
                • Call PATCH request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
                • Expect 200 - Validate Response and ensure status is ACKN
                • Call GET links.redirect endpoint
                • Expect 200
                • Call the Delete Registration Endpoint
                • Expect a 204 - No Content
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "mtls.ca",
                "directory.client_id",
                "directory.discoveryUrl",
                "directory.apibase",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteCapitalizationTitleApiCoreDcrWebhookTestModuleV1 extends AbstractOpinQuoteCapitalizationTitleApiCoreDcmDcrWebhookTestModule {

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteCapitalizationTitlePostRequestBodySinglePayment.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostQuoteCapitalizationTitleOASValidator.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetQuoteCapitalizationTitleOASValidator.class;
    }


    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchQuoteCapitalizationTitleOASValidator.class;
    }
}