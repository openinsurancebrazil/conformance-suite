package net.openid.conformance.opin.testmodule.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpHeaders;

public class CallResourceWithVersionHeaders extends CallNoCacheResource {
    @Override
    @PreEnvironment(strings = "x-v")
    protected HttpHeaders getHeaders(Environment env) {
        HttpHeaders headers = super.getHeaders(env);
        String version = env.getString("x-v");
        String minVersion = env.getString("x-min-v");
        headers.set("x-v", version);
        if(!Strings.isNullOrEmpty(minVersion)){
            headers.set("x-min-v", version);
        }
        return headers;
    }

    @Override
    protected boolean treatAllHttpStatusAsSuccess() {
        // So it will not throw an exception when we get a 406.
        return true;
    }
}
