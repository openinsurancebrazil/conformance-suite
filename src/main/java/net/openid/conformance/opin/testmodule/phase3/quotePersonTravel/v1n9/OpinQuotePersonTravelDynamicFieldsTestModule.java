package net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.AbstractOpinQuotePersonTravelDynamicFieldsTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel.GetPersonTravelQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel.PatchPersonTravelOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel.PostPersonTravelOASValidatorV1n10;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-travel_api_conditional-dynamic-fields_test-module_v1",
        displayName = "Ensure that a person travel quotation request can be successfully created and accepted afterwards using dynamic field.",
        summary = """
                Ensure that a person travel quotation request can be successfully created and accepted afterwards using dynamic field.\s
                 This test is conditional and will only be executed if the fields "Conditional - CPF for Dynamic fields" or "Conditional - CNPJ for Dynamic Fields" are filled. This test applies to both Personal and Business products, depending on the config.
                • Call GET /damage-and-person Dynamic Fields endpoint\s
                • Expect 200 - Validate response and validade if api is QUOTE_PERSON
                • Call POST travel/request endpoint sending personal or business information, following what is defined at the config, and the dynamicFields at quoteCustomData.generalQuoteInfo
                • Expect 201 - Validate Response and ensure status is RCVD
                • Poll the GET travel/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL for 1 minute, and fai the test if the quote status is not conclusive
                • Call GET travel/request/{consentId}/quote-status endpoint
                • Expect 200 - Validate Respones and ensure status is ACPT and that quoteCustomData is sent back
                • Call PATCH travel/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
                • Expect 200 - Validate Response and ensure status is ACKN
                • Call GET links.redirect endpoint
                • Expect 200
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "resource.brazilCpfDynamicFields",
                "resource.brazilCnpjDynamicFields",
                "consent.productType"
        }
)
public class OpinQuotePersonTravelDynamicFieldsTestModule  extends AbstractOpinQuotePersonTravelDynamicFieldsTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPersonTravelOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPersonTravelQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPersonTravelOASValidatorV1n10.class;
    }

}
