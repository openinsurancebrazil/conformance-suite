package net.openid.conformance.opin.testmodule.v1.lifePension;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4.GetInsuranceLifePensionContractsOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "life-pension_api_resources_test-module_v1",
        displayName = "Makes sure that the Resource API and the Life Pension API are returning the same available IDs",
        summary = "Makes sure that the Resource API and the Life Pension API are returning the same available IDs\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“LIFE_PENSION_READ”, “LIFE_PENSION_CONTRACTINFO_READ”, “LIFE_PENSION_MOVEMENTS_READ”, “LIFE_PENSION_PORTABILITIES_READ”,“LIFE_PENSION_WITHDRAWALS_READ”,“LIFE_PENSION_CLAIM”,“RESOURCES_READ”)\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Life Pension contracts Endpoint\n" +
                "• Expect 200  - Validate response and Make sure that an id is returned - Fetch the certificate id provided by this API\n" +
                "• Call the resources API\n" +
                "• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class LifePensionApiResourcesTestModuleV1 extends AbstractLifePensionApiResourcesTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceLifePensionContractsOASValidatorV1n4.class;
    }

}
