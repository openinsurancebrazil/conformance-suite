package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public abstract class AbstractCreateClaimNotificationRequestBody extends AbstractCondition {

    @Override
    @PreEnvironment(strings = {"policyId", "documentType", "claim_notification_endpoint_type"})
    @PostEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        String policyId = env.getString("policyId");
        String documentType = env.getString("documentType");
        String endpointType = env.getString("claim_notification_endpoint_type");
        String occurrenceDate = LocalDateTime.now(ZoneOffset.UTC)
            .minusDays(1)
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        JsonObject data = new JsonObject();

        if (Objects.equals(endpointType, "person")) {

            JsonObject requestor = buildRequestorObject(env);
            data.addProperty("requestorIsInsured", false);
            data.add("requestor", requestor);
            data.addProperty("claimant", "SEGURADO");
            String proposalId = getProposalId();
            if(proposalId != null) {
                data.addProperty("proposalId", proposalId);
            }
        }

        if (Objects.equals(documentType, "CERTIFICADO") || Objects.equals(documentType, "CERTIFICADO_AUTOMOVEL")) {
            data.addProperty("groupCertificateId", policyId);
        } else {
            data.addProperty(getPolicyFieldName(), policyId);
        }

        JsonArray insuredObjectIds = getInsuredObjectId();
        if (insuredObjectIds != null) {
            data.add("insuredObjectId", insuredObjectIds);
        }

        data.addProperty("documentType", documentType);
        data.addProperty("occurrenceDate", occurrenceDate);
        data.addProperty("occurrenceTime", "12:00:00");
        data.addProperty("occurrenceDescription", OPINPhase3Keys.Keys_occurrenceDescription);

        JsonObject requestBody = new JsonObject();
        requestBody.add("data", data);
        env.putObject("resource_request_entity", requestBody);
        env.putString("resource_request_entity", requestBody.toString());
        logSuccess("Request body created for claim notification call", args("data", data));

        return env;
    }

    private JsonObject buildRequestorObject(Environment env) {
        JsonObject requestor = new JsonObject();
        String productType = env.getString("config", "consent.productType");

        requestor.addProperty("name", "string");
        if ("business".equals(productType)) {
            requestor.addProperty("documentType", "CNPJ");
            requestor.addProperty("documentNumber", env.getString("config", "resource.brazilCnpj"));
        } else {
            requestor.addProperty("documentType", "CPF");
            requestor.addProperty("documentNumber", env.getString("config", "resource.brazilCpf"));
        }

        return requestor;
    }

    protected abstract String getPolicyFieldName();

    protected JsonArray getInsuredObjectId() {
        return null;
    }

    protected String getProposalId() {
        return null;
    }
}
