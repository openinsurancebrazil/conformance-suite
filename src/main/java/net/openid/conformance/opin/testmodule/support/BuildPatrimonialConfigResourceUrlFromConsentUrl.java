package net.openid.conformance.opin.testmodule.support;

public class BuildPatrimonialConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "insurance-patrimonial";
	}

	@Override
	protected String getEndpointReplacement() {
		return "insurance-patrimonial";
	}
}
