package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPremiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-financial-risks-api-structural-test",
	displayName = "Validate structure of Financial Risks API Endpoints 200 response",
	summary = "Validate structure of Financial Risks API Endpoints 200 response \n"+
	"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"resource.consentUrl"
		}
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinFinancialRisksStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-financial-risk";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);
		setRootValidator(GetInsuranceFinancialRiskListOASValidatorV1n3.class);
		setPolicyInfoValidator(GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3.class);
		setPremiumValidator(GetInsuranceFinancialRiskPremiumOASValidatorV1n3.class);
		setClaimValidator(GetInsuranceFinancialRiskClaimOASValidatorV1n3.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

