package net.openid.conformance.opin.testmodule.phase3.capitalizationTitleWithdrawal;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.AbstractOpinPhase3XWithdrawInvalidIDTestModule;
import net.openid.conformance.opin.testmodule.support.AddCapitalizationTitleScope;
import net.openid.conformance.opin.testmodule.support.AddWithdrawalCapitalizationTitleScope;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForCapitalizationTitleWithdrawal;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.CreateCapitalizationTitleWithdrawalMockConsentBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.CreateCapitalizationTitleWithdrawalMockPostRequestBody;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitleWithdrawal.v1n3.PostCapitalizationTitleWithdrawalOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-capitalization-title-withdraw_api_invalid-planId_test-module_v1",
        displayName = "Validates the structure of Capitalization Title Withdrawal API",
        summary = """
                Ensure a capitalization title withdrawal request cannot be successfully executed with na invalid planId
                                
                · Call the POST Consents with CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permission, sending the withdrawalCaptalizationInformation with the mocked information
                · Expect a 201 - Validate the response
                · Redirect the User
                                
                If an error is not returned at the redirect:
                · Call the POST capitalization-title/request endpoint, sending the same information as the consent
                · Expect a 422- Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is ""CONSUMED""
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3CapitalizationTitleWithdrawApiInvalidPlanIdTestModule extends AbstractOpinPhase3XWithdrawInvalidIDTestModule {
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddWithdrawalCapitalizationTitleScope.class;
    }

    @Override
    protected String getApi() {
        return "insurance-capitalization-title";
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.CAPITALIZATION_TITLE_WITHDRAWAL;
    }

    @Override
    protected AbstractCondition createWithdrawalConsentBody() {
        return new CreateCapitalizationTitleWithdrawalMockConsentBody();
    }

    @Override
    protected Class<? extends Condition> prepareUrl() {
        return PrepareUrlForCapitalizationTitleWithdrawal.class;
    }

    @Override
    protected Class<? extends Condition> createWithdrawalPostRequestBody() {
        return CreateCapitalizationTitleWithdrawalMockPostRequestBody.class;
    }

    @Override
    protected Class<? extends Condition> validator() {
        return PostCapitalizationTitleWithdrawalOASValidatorV1n3.class;
    }

    @Override
    protected void validateResponse() {
        //not needed here
    }
}
