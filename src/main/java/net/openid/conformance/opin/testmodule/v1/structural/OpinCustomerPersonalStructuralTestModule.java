package net.openid.conformance.opin.testmodule.v1.structural;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.BuildResourceUrlFromStructuralResourceUrl;
import net.openid.conformance.opin.testmodule.support.PrepareToGetPersonalComplimentaryInformation;
import net.openid.conformance.opin.testmodule.support.PrepareToGetPersonalIdentifications;
import net.openid.conformance.opin.testmodule.support.PrepareToGetPersonalQualifications;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersPersonalComplimentaryInformationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersPersonalIdentificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersPersonalQualificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.sequences.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-customer-personal-api-structural-test",
	displayName = "Validate structure of Customer - Personal API resources",
	summary = "Call the “/personal/identifications\" endpoint - Expect 200 and validate response\n" +
		      "Call the “/personal/qualifications\" endpoint - Expect 200 and validate response\n" +
		      "Call the “/personal/complimentary-information\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"resource.consentUrl"
		}
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})
public class OpinCustomerPersonalStructuralTestModule extends AbstractNoAuthFunctionalTestModule{

		private final String API = "customers";
		@Override
		protected void runTests() {

			call(new ValidateOpinWellKnownUriSteps());

			env.putString("api_base", API);
			callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrl.class);

			runInBlock("Validate Personal - Identifications response", () -> {
				callAndStopOnFailure(PrepareToGetPersonalIdentifications.class);
				callAndStopOnFailure(CallNoCacheResource.class);
				callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(GetCustomersPersonalIdentificationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
			});
			runInBlock("Validate Personal - Qualifications response", () -> {
				callAndStopOnFailure(PrepareToGetPersonalQualifications.class);
				callAndStopOnFailure(CallNoCacheResource.class);
				callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(GetCustomersPersonalQualificationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
			});
			runInBlock("Validate Personal - Complimentary-Information response", () -> {
				callAndStopOnFailure(PrepareToGetPersonalComplimentaryInformation.class);
				callAndStopOnFailure(CallNoCacheResource.class);
				callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(GetCustomersPersonalComplimentaryInformationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
			});
		}
	}

