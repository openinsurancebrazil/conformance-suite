package net.openid.conformance.opin.testmodule.support.oasValidators.person.v1n6;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Person 1.6.0")
public class GetPersonOASValidatorV1n6 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/person/v1/v1n6/swagger-person-1.6.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/person";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}