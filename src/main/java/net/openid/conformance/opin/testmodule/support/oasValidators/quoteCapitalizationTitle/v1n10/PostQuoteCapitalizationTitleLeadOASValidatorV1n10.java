package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.AbstractPostQuoteCapitalizationTitleLeadOASValidator;

@ApiName("Quote Capitalization Title 1.10.0")
public class PostQuoteCapitalizationTitleLeadOASValidatorV1n10 extends AbstractPostQuoteCapitalizationTitleLeadOASValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteCapitalizationTitle/swagger-quote-capitalization-title-1.10.0.yaml";
    }
}
