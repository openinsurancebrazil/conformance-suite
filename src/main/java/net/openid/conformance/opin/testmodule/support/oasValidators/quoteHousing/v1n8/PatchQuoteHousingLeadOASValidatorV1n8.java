package net.openid.conformance.opin.testmodule.support.oasValidators.quoteHousing.v1n8;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class PatchQuoteHousingLeadOASValidatorV1n8 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteHousing/quote-housing-v1.8.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lead/request/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.PATCH;
    }
}
