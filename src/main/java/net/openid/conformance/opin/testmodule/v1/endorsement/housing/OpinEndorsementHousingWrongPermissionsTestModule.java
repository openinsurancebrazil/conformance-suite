package net.openid.conformance.opin.testmodule.v1.endorsement.housing;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.AbstractOpinEndorsementWrongPermissionsTest;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-endorsement-housing-api-wrong-permissions-test",
        displayName = "Ensure the endorsement API cannot be accessed without needed permissions",
        summary = "Ensure the endorsement API cannot be accessed without needed permissions\n" +
        "• Call the POST Consents API with HOUSING Phase 2 permissions\n" +
        "• Expect 201 a with status on \"AWAITING_AUTHORISATION\"  - Validate Response\n" +
        "• Redirect the user to authorize consent\n" +
        "• Call the GET Consents Endpoint\n" +
        "• Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "• Call the resources API\n" +
        "• Extract a shared policy ID on the resources APIs\n" +
        "• Call the POST Endorsement Endpoint with pre-defined Payload - " +
        "• Change endorsementType for EXCLUSAO, set policy ID for the Extracted policyID. The token obtained after the redirect is to be used here\n" +
        "• Expects 403 - Validate Error response\n" +
        "• Call the POST Consents Endpoint with Claim Notification permissions, set policy ID for the Extracted policyID\n" +
        "• Expects 201 - Validate Response\n" +
        "• Redirect the user to authorize consent\n" +
        "• Call the GET Consents Endpoint\n" +
        "• Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "• Call the POST Endorsement Endpoint\n" +
        "• Expects 403 - Validate Error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf"
        }
)
public class OpinEndorsementHousingWrongPermissionsTestModule extends AbstractOpinEndorsementWrongPermissionsTest {

        @Override
        protected void setupPermissions() {
                OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
                permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_HOUSING).build();
        }
}
