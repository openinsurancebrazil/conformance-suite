package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.AbstractGetInsurancePensionPlanMovementsOASValidator;

@ApiName("Insurance Pension Plan 1.4.0")
public class GetInsurancePensionPlanMovementsOASValidatorV1n4 extends AbstractGetInsurancePensionPlanMovementsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insurancePensionPlan/v1/swagger-insurance-pension-plan.yaml";
	}
}