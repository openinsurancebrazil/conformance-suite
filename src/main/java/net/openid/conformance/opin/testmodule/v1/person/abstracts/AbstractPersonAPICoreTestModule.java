package net.openid.conformance.opin.testmodule.v1.person.abstracts;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;

public abstract class AbstractPersonAPICoreTestModule extends AbstractOpinApiTestModuleV2 {
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddPersonScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsurancePersonUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_PERSON;
    }

    @Override
    protected String getApi() {
        return "insurance-person";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].policies[0].policyId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", getPolicyInfoValidator(),
                "premium", getPremiumValidator(),
                "claim", getClaimValidator()
        );
    }

    protected abstract Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getPremiumValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getClaimValidator();
}
