package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks.AbstractOpinQuotePatrimonialDiverseRisksDynamicFieldsTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.damageAndPerson.v1n4.GetDynamicFieldsDamageAndPersonOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.PatchPatrimonialDiverseRisksOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.PostPatrimonialDiverseRisksOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialDiverseRisksPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-diverse-risks_api_conditional-dynamic-fields_test-module_v1n10",
        displayName = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and accepted afterwards using dynamic field.",
        summary = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and accepted afterwards using dynamic field. \n This test is conditional and will only be executed if the fields \"Conditional - CPF for Dynamic fields\" or \"Conditional - CNPJ for Dynamic Fields\" are filled. This test applies to both Personal and diverse-risks products, depending on the config.\n" +
                "\u2022 Call GET /damage-and-person Dynamic Fields endpoint \n" +
                "\u2022 Expect 200 - Validate response and validade if api is QUOTE_PATRIMONIAL\n" +
                "\u2022 Call POST diverse-risks/request endpoint sending personal or diverse-risks information, following what is defined at the config, and the dynamicFields at quoteCustomData.generalQuoteInfo\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Poll the GET diverse-risks/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL for 1 minute, and fai the test if the quote status is not conclusive\n" +
                "\u2022 Call GET diverse-risks/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respones and ensure status is ACPT and that quoteCustomData is sent back\n" +
                "\u2022 Call PATCH diverse-risks/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN\n" +
                "\u2022 Expect 200 - Validate Response and ensure status is ACKN\n" +
                "\u2022 Call GET links.redirect endpoint\n" +
                "\u2022 Expect 200\n",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "resource.brazilCpfDynamicFields",
                "resource.brazilCnpjDynamicFields",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialDiverseRisksDynamicFieldsTestModuleV1n10 extends AbstractOpinQuotePatrimonialDiverseRisksDynamicFieldsTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialDiverseRisksOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialDiverseRisksOASValidatorV1n10.class;
    }

    @Override
    protected void getDynamicFieldsValidator() {
        callAndContinueOnFailure(GetDynamicFieldsDamageAndPersonOASValidatorV1n4.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialDiverseRisksPostRequestBodyWithCoverage.class;
    }
}
