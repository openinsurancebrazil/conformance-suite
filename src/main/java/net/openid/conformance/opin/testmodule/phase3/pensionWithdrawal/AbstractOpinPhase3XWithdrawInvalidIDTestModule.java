package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckIfAuthorizationEndpointResponseHasErrorOrNot;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.support.ApiTypes;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinPhase3XWithdrawInvalidIDTestModule extends AbstractOpinPhase3XWithdrawWrongPermissionsTestModule {

    public static String API_TYPE = ApiTypes.DATA_API_PHASE3.toString();

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        env.putString("api", getApi());
        env.putString("api_type", API_TYPE);
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(getPermissionsGroup()).build();
    }
    protected abstract PermissionsGroup getPermissionsGroup();

    protected abstract AbstractCondition createWithdrawalConsentBody();


    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                        sequenceOf(
                                condition(createWithdrawalConsentBody().getClass()))
                );
    }

    @Override
    protected void onAuthorizationCallbackResponse() {
        callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");
        callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");
        callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");
        callAndStopOnFailure(CheckIfAuthorizationEndpointResponseHasErrorOrNot.class);

        if (env.getBoolean("token_endpoint_response_was_error")) {
            callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);
            callAndStopOnFailure(RejectAuthCodeInAuthorizationEndpointResponse.class);
            onPostAuthorizationFlowComplete();
        } else {
            callAndStopOnFailure(ExtractAuthorizationCodeFromAuthorizationResponse.class);
            callAndContinueOnFailure(EnsureMinimumAuthorizationCodeLength.class, Condition.ConditionResult.FAILURE, "RFC6749-10.10", "RFC6819-5.1.4.2-2");
            callAndContinueOnFailure(EnsureMinimumAuthorizationCodeEntropy.class, Condition.ConditionResult.FAILURE, "RFC6749-10.10", "RFC6819-5.1.4.2-2");
            handleSuccessfulAuthorizationEndpointResponse();
        }
    }

    protected void executeTest() {
        postWithdrawal("422");
        validateWithdrawalResponse();
        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
    }

    protected void validateWithdrawalResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

}
