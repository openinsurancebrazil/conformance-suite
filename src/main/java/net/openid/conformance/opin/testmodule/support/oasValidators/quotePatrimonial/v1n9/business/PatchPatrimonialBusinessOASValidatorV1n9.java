package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.business;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Quote Patrimonial 1.9.0")
public class PatchPatrimonialBusinessOASValidatorV1n9 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePatrimonial/quote-patrimonial-1.9.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/business/request/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.PATCH;
    }
}
