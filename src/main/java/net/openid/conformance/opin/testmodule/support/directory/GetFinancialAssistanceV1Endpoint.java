package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetFinancialAssistanceV1Endpoint extends AbstractGetXFromAuthServer {

    @Override
    protected String getEndpointRegex() {
        return "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-financial-assistance\\/v\\d+/insurance-financial-assistance/contracts)$";
    }

    @Override
    protected String getApiFamilyType() {
        return "insurance-financial-assistance";
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(1.[0-9].[0-9])$";
    }

    @Override
    protected boolean isResource() {
        return true;
    }
}
