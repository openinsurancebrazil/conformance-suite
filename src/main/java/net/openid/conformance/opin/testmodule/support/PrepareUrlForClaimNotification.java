package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForClaimNotification extends AbstractPrepareUrlForApi {

    private String consentId;
    private String endpointType;

    @Override
    protected String getApiReplacement() {
        return "claim-notification";
    }

    @Override
    protected String getEndpointReplacement() {
        return String.format("request/%s/%s", endpointType, consentId);
    }

    @Override
    @PreEnvironment(strings = {"consent_id", "claim_notification_endpoint_type"})
    public Environment evaluate(Environment env) {
        consentId = env.getString("consent_id");
        endpointType = env.getString("claim_notification_endpoint_type");
        return super.evaluate(env);
    }
}
