package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuotePatrimonialLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "RESIDENCIAL_IMOVEL_BASICA";
    }
}