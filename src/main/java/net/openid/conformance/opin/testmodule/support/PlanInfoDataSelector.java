package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class PlanInfoDataSelector extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    public Environment evaluate(Environment env) {
        {
            JsonObject body = bodyFrom(env).getAsJsonObject();
            JsonObject data = Optional.ofNullable(body.getAsJsonObject("data"))
                    .orElseThrow(() -> error("Could not find data in the body", args("body", body)));

            if (data.isEmpty()) {
                throw error("Data array cannot be empty to extract title ID");
            }

            JsonArray series = Optional.ofNullable(data.getAsJsonArray("series"))
                    .orElseThrow(() -> error("Could not extract series array from the data", args("data", data)));

            if (series.isEmpty()) {
                throw error("Series array cannot be empty to extract title ID");
            }

            JsonObject seriesObject = series.get(0).getAsJsonObject();

            JsonArray titles = Optional.ofNullable(seriesObject.getAsJsonArray("titles"))
                    .orElseThrow(() -> error("Could not extract titles array from the data", args("data", data)));

            if (titles.isEmpty()) {
                throw error("Titles array cannot be empty to extract title ID");
            }

            JsonObject titleObject = titles.get(0).getAsJsonObject();

            JsonArray technicalProvisions = Optional.ofNullable(titleObject.getAsJsonArray("technicalProvisions"))
                    .orElseThrow(() -> error("Could not extract technicalProvisions array from the data", args("data", data)));

            if (technicalProvisions.isEmpty()) {
                throw error("TechnicalProvisions array cannot be empty to extract prAmount");
            }

            JsonObject technicalProvisionsObject = technicalProvisions.get(0).getAsJsonObject();

            JsonObject prAmount = Optional.ofNullable(technicalProvisionsObject.getAsJsonObject("prAmount"))
                    .orElseThrow(() -> error("Could not extract prAmount object from the data", args("data", data)));

            if (prAmount.isEmpty()) {
                throw error("PrAmount object cannot be empty");
            }

            if(env.containsObject("series_1")){
                env.putObject("series_2",seriesObject);
                env.putString("selected_id",Optional.ofNullable(seriesObject.get("seriesId"))
                        .map(OIDFJSON::getString)
                        .orElseThrow(() -> error("could not find series ID in the series object", args("series", seriesObject))));
            } else {
                env.putObject("series_1",seriesObject);
                env.putString("selected_id",Optional.ofNullable(seriesObject.get("seriesId"))
                        .map(OIDFJSON::getString)
                        .orElseThrow(() -> error("could not find series ID in the series object", args("series", seriesObject))));
            }
            return env;
        }
    }

        private JsonElement bodyFrom(Environment env) {
            try {
                return BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
                        .orElseThrow(() -> error("Could not find response body in the environment"));
            } catch (ParseException e) {
                throw error("Could not parse response body");
            }
        }

    }
