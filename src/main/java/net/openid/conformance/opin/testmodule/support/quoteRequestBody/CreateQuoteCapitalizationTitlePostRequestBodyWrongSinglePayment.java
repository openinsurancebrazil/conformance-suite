package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteCapitalizationTitlePostRequestBodyWrongSinglePayment extends AbstractCreateQuoteCapitalizationTitlePostRequestBody {

    @Override
    protected String getPaymentType() {
        return "UNICO";
    }

    @Override
    protected String getAmount() {
        return "1000.00";
    }

    @Override
    protected boolean isMonthlyPayment() {
        return true;
    }

    @Override
    protected boolean isSinglePayment() {
        return false;
    }

    @Override
    protected boolean isUnitTypePresent() {
        return false;
    }
}
