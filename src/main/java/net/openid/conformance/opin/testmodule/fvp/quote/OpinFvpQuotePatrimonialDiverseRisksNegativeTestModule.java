package net.openid.conformance.opin.testmodule.fvp.quote;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.fvp.quote.abstracts.AbstractOpinFvpQuoteNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialDiverseRisksPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-fvp-quote-patrimonial-diverse-risks_api_negative-quote_test-module",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = "Ensure a consent cannot be created in unhappy requests.\n" +
                "\u2022 Call POST diverse-risks/request endpoint sending personal or diverse-risks information, following what is defined at the config, and not send the consentId on request payload\n" +
                "\u2022 Expect 400 or 422 - Validate Error Response\n" +
                "\u2022 Call POST diverse-risks/request endpoint sending personal or diverse-risks information, following what is defined at the config, using a client credentials token with \"quote-patrimonial-lead\" scope\n" +
                "\u2022 Expect 403 - Validate Error Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinFvpQuotePatrimonialDiverseRisksNegativeTestModule extends AbstractOpinFvpQuoteNegativeTestModule {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected OPINScopesEnum getLeadScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_LEAD;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_DIVERSE_RISKS;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-patrimonial-diverse-risks";
    }

    @Override
    protected String getApiName() {
        return "quote-patrimonial";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "diverse-risks/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialDiverseRisksPostRequestBody.class;
    }
}
