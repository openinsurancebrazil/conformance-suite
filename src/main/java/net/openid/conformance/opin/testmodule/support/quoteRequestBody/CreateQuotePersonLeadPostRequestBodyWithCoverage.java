package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuotePersonLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "MORTE";
    }
}
