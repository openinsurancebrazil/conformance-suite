package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractPostClaimNotificationDamageOASValidator extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getEndpointPath() {
        return "/request/damage/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }
}
