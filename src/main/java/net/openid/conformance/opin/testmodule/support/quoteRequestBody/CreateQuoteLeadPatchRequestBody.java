package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateQuoteLeadPatchRequestBody extends AbstractCreateQuotePatchRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {}
}
