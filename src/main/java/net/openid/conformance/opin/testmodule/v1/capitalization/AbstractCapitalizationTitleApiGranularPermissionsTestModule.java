package net.openid.conformance.opin.testmodule.v1.capitalization;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;

public abstract class AbstractCapitalizationTitleApiGranularPermissionsTestModule extends AbstractOpinApiTestModuleV2 {

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddCapitalizationTitleScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceCapitalizationTitleUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.CAPITALIZATION_TITLE;

    }

    @Override
    protected String[] getIndividualPermissionsToBeRemoved() {
        return new String[]{"CAPITALIZATION_TITLE_EVENTS_READ", "CAPITALIZATION_TITLE_SETTLEMENTS_READ"};
    }

    @Override
    protected String getApi() {
        return "insurance-capitalization-title";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        return PlanIdSelector.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "plan-info", getPlanInfoValidator(),
                "events", getEventsValidator(),
                "settlements", getSettlementsValidator()

        );
    }

    @Override
    protected Class<? extends Condition> getExpectedResponseCode(String endpoint) {
        Map<String, Class<? extends Condition>> endpointToStatusCodeMap = Map.of(
                "plan-info", EnsureResourceResponseCodeWas200.class,
                "events", EnsureResourceResponseCodeWas403.class,
                "settlements", EnsureResourceResponseCodeWas403.class
        );
        return endpointToStatusCodeMap.get(endpoint);
    }

    @Override
    protected boolean checkConsentAuthorizedStatusAfterRedirect() {
        return true;
    }

    protected abstract Class<? extends AbstractJsonAssertingCondition> getPlanInfoValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getEventsValidator();
    protected abstract Class<? extends AbstractJsonAssertingCondition> getSettlementsValidator();
}
