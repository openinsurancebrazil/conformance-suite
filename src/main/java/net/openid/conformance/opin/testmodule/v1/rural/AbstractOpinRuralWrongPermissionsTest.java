package net.openid.conformance.opin.testmodule.v1.rural;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModule;
import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceRural.v1n3.GetInsuranceRuralPremiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Map;

public abstract class AbstractOpinRuralWrongPermissionsTest extends AbstractOpinWrongPermissionsTestModuleV2 {

    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator();

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_RURAL;
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddRuralScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildRuralConfigResourceUrlFromConsentUrl.class;
    }

    @Override
    protected String getApi() {
        return "insurance-rural";
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", getPolicyInfoValidator(),
                "premium", getPremiumValidator(),
                "claim", getClaimValidator()
        );
    }
}
