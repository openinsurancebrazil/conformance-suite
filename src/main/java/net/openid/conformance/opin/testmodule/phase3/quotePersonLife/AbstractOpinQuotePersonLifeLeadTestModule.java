package net.openid.conformance.opin.testmodule.phase3.quotePersonLife;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;


public abstract class AbstractOpinQuotePersonLifeLeadTestModule extends AbstractOpinPhase3QuoteLeadTestModule {
    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PERSON_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-person-life";
    }

    @Override
    protected String getApiName() {
        return "quote-person";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}
