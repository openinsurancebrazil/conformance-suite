package net.openid.conformance.opin.testmodule.v1.structural.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.AddConsentIdFromConfigToEnvironment;
import net.openid.conformance.opin.testmodule.support.BuildResourceUrlFromStructuralResourceUrlPhase3;
import net.openid.conformance.opin.testmodule.support.sequences.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;

public abstract class AbstractOpinDataStructuralPhase3TestModule extends AbstractNoAuthFunctionalTestModule {

    protected String api;
    protected String endpointType;
    protected Class<? extends Condition> validator;

    @Override
    protected void runTests() {
        call(new ValidateOpinWellKnownUriSteps());

        callAndStopOnFailure(AddConsentIdFromConfigToEnvironment.class);
        env.putString("claim_notification_endpoint_type", endpointType);
        env.putString("api_base", api);
        callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrlPhase3.class);

        runInBlock(String.format("Validate %s api with the given consentID", api), () -> {
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
        });
    }

    protected void setApi(String api) {
        this.api = api;
    }

    protected void setEndpointType(String endpointType) {
        this.endpointType = endpointType;
    }

    protected void setValidator(Class<? extends Condition> validator) {
        this.validator = validator;
    }
}
