package net.openid.conformance.opin.testmodule.v1.consents.v2n6;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentsApiPermissionGroupsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-consent-api-test-permission-groups-v2n6",
	displayName = "Validate that consent API accepts the consent groups",
	summary = "Validates that consent API accepts the consent groups\n" +
		"• Creates a series of consent requests with valid permissions group and expect for each of them a 201 to be returned by the server" +
			" or a 422 in case the product is not supported\n" +
		"• Validates consent API request for 'Personal Registration Data' permission group(s)\n" +
		"• Validates consent API request for 'Personal Additional Information' permission group(s)\n" +
		"• Validates consent API request for 'Personal Qualification' permission group(s)\n" +
		"• Validates consent API request for 'Business Registration Data' permission group(s)\n" +
		"• Validates consent API request for 'Business Additional Information' permission group(s)\n" +
		"• Validates consent API request for 'Business Qualification' permission group(s)\n" +
		"• Validates consent API request for 'Capitalization Titles' permission group(s)\n" +
		"• Validates consent API request for 'Pension Risk' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Patrimonial' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Responsability' permission group(s)\n"+
		"• Validates consent API request for 'Damages and People Transport' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Financial Risks' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Rural' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Auto' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Housing' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Acceptance and Branches Abroad' permission group(s)\n" +
		"• Validates consent API request for 'Damages and People Person' permission group(s)\n" +
		"• Expect a 201 or 422 - Unprocessable Entity in case the server does not support the permission group mentioned\n" +
		"• Validate Response\n" +
		"• For 201 responses, ensure that the permissions are not widened\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinConsentsApiPermissionGroupsTestModuleV2n6 extends AbstractOpinConsentsApiPermissionGroupsTestModule {

	@Override
	protected AbstractJsonAssertingCondition postValidator() {
		return new PostConsentsOASValidatorV2n6();
	}
}