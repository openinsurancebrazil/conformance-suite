package net.openid.conformance.opin.testmodule.phase1.rentGuarantee;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.rentGuarantee.v1n3.GetRentGuaranteeOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - Rent Guarantee API test",
        displayName = "Validate structure of Rent Guarantee response",
        summary = "Validate structure of Rent Guarantee response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class RentGuaranteeApiTestModule extends AbstractNoAuthFunctionalTestModule {
    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices Rent Guarantee response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetRentGuaranteeOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
        });
    }
}