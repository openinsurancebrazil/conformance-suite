package net.openid.conformance.opin.testmodule.support;

import java.util.List;

public class EnsureStatusWasRcvdOrEval extends AbstractEnsureStatusWasX {
    public final static String STATUS_KEY = "status_is_rcvd_or_eval";

    @Override
    protected List<String> getStatuses() {
        return List.of("RCVD", "EVAL");
    }

    @Override
    protected String getKeyToInformResult() {
        return STATUS_KEY;
    }
}
