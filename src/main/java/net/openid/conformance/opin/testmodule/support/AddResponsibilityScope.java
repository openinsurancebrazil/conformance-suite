package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;

public class AddResponsibilityScope extends AbstractScopeAddingCondition {

	@Override
	protected String newScope() {
		return "insurance-responsibility";
	}
}
