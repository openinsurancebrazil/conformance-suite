package net.openid.conformance.opin.testmodule.phase1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.frontchannel.BrowserControl;
import net.openid.conformance.info.ImageService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.logging.TestInstanceEventLogIgnoreSuccess;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.runner.TestExecutionManager;

import java.util.Map;

public abstract class AbstractOpenDataDynamicVersionTestModule extends AbstractNoAuthFunctionalTestModule {

    protected String headerVersion;
    protected String minVersion;
    protected String apiVersion;
    private final String greatMinVersion = "5.0.0";
    private final String unhappyVersion = "5.0.0";
    private TestInstanceEventLog originalLogger;
    private TestInstanceEventLog ignoreSuccessLogger;

    protected abstract Class<? extends Condition> getValidator();



    @Override
    public void setProperties(String id, Map<String, String> owner, TestInstanceEventLog eventLog, BrowserControl browser, TestInfoService testInfo, TestExecutionManager executionManager, ImageService imageService) {
        this.ignoreSuccessLogger = new TestInstanceEventLogIgnoreSuccess(eventLog);
        this.originalLogger = eventLog;
        super.setProperties(id, owner, eventLog, browser, testInfo, executionManager, imageService);
    }

    protected void disableLogging() {
        if (!eventLog.equals(ignoreSuccessLogger)) {
            eventLog.log(getName(), "Logging is reduced. Only errors and warnings will be displayed");
            eventLog = ignoreSuccessLogger;
        }
    }

    protected void enableLogging() {
        if (!eventLog.equals(originalLogger)) {
            eventLog = originalLogger;
            eventLog.log(getName(), "Full logging is enabled");
        }
    }

    @Override
    protected void runTests() {
        runInBlock("Validating if response matches version requested", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallResourceWithVersionHeaders.class);
            callAndContinueOnFailure(EnsureResponseVersionHeaderMatchesRequested.class, Condition.ConditionResult.FAILURE);
        });

        validateResponse();

        runInBlock("Validating if response matches the versions requested ", () -> {
            env.putString("x-min-v",minVersion);
            callAndStopOnFailure(CallResourceWithVersionHeaders.class);
            callAndContinueOnFailure(EnsureResponseVersionHeaderMatchesRequested.class, Condition.ConditionResult.FAILURE);
        });

        validateResponse();

        runInBlock("Validating if response ignores x-min-v when it's >= than x-v", () -> {
            env.putString("x-min-v",greatMinVersion);
            callAndStopOnFailure(CallResourceWithVersionHeaders.class);
            callAndContinueOnFailure(EnsureResponseVersionHeaderMatchesRequested.class, Condition.ConditionResult.FAILURE);
        });

        validateResponse();

        runInBlock("Calling resource with no version headers, expecting x-v header >= version of this test", () -> {
            callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(EnsureResponseVersionHeaderMatchesApiVersion.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("Calling GET endpoint with the x-v header as “5.0.0”", () -> {
            env.putString("x-v",unhappyVersion);
            env.removeNativeValue("x-min-v");
            callAndStopOnFailure(CallResourceWithVersionHeaders.class);
            callAndContinueOnFailure(getValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResponseCodeWas406.class);
        });
    }

    protected void validateResponse(){
        disableLogging();
        runInBlock("Validate Response", () -> callAndContinueOnFailure(getValidator(), Condition.ConditionResult.FAILURE));
        enableLogging();
    }

    protected void setVersionsInEnv(String headerVersion, String minVersion, String apiVersion){
        this.headerVersion = headerVersion;
        this.minVersion = minVersion;
        this.apiVersion = apiVersion;

        env.putString("x-v",headerVersion);
        env.putString("apiVersion", apiVersion);
    }

}
