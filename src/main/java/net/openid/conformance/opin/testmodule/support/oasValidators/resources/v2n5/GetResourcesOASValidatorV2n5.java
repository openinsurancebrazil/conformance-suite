package net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n5;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetResourcesOASValidatorV2n5 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/resources/v2n5/swagger-resources-api-2.5.0.yml";
    }

    @Override
    protected String getEndpointPath() {
        return "/resources";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
