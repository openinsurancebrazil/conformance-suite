package net.openid.conformance.opin.testmodule.phase1.stopLoss;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.stopLoss.v1n3.GetStopLossOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - Stop Loss API test",
        displayName = "Validate structure of Stop Loss response",
        summary = "Validate structure of Stop Loss response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class StopLossApiTestModule extends AbstractNoAuthFunctionalTestModule {
    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices Stop Loss response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetStopLossOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
        });
    }
}