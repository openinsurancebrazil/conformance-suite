package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import static net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.CreatePaymentErrorEnumV2.ERRO_IDEMPOTENCIA;

public class EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject extends AbstractJsonAssertingCondition {

    public static final String RESPONSE_ENV_KEY = "endpoint_response";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
        public Environment evaluate(Environment environment) {
        String expectedError = ERRO_IDEMPOTENCIA.toString();

        JsonElement body = bodyFrom(environment,RESPONSE_ENV_KEY);
        JsonElement errorsEl = findByPath(body, "errors");
        if (!errorsEl.isJsonObject()) {
            throw error("Errors is not JSON object", args("Errors", errorsEl));
        }

        String code = OIDFJSON.getString(findByPath(errorsEl, "code"));
        if (expectedError.equals(code)) {
            logSuccess("Found error with the expected code", args("Expected", expectedError, "Found error", errorsEl));
            return environment;
        }

        throw error("Could not find error with expected code in the errors JSON object", args("Expected", expectedError, "Errors", errorsEl));
}
}
