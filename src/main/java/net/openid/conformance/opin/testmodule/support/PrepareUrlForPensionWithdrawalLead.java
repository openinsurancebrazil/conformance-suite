package net.openid.conformance.opin.testmodule.support;

public class PrepareUrlForPensionWithdrawalLead extends AbstractPrepareUrlForApi {

    @Override
    protected String getApiReplacement() {
        return "withdrawal";
    }

    @Override
    protected String getEndpointReplacement() {
        return "lead/request";
    }
}
