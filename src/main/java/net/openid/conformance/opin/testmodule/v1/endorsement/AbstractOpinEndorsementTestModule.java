package net.openid.conformance.opin.testmodule.v1.endorsement;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScope;
import net.openid.conformance.opin.testmodule.AbstractOpinPhase3TestModule;
import net.openid.conformance.opin.testmodule.support.AddEndorsementScope;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.SelectPolicyIdFromResourcesResponse;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinEndorsementTestModule extends AbstractOpinPhase3TestModule {

    @Override
    protected void validateGetResourcesResponse() {
        runInBlock("Validating resources response", () -> {
            callAndContinueOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(SelectPolicyIdFromResourcesResponse.class);
        });
    }

    @Override
    protected void setupSecondFlow() {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.ENDORSEMENT_REQUEST_CREATE).build();
        callAndStopOnFailure(SetScope.class);
        callAndStopOnFailure(AddEndorsementScope.class);
    }

    protected abstract Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody();

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertBefore(
                        CallConsentEndpointWithBearerTokenAnyHttpMethod.class,
                        condition(createEndorsementConsentBody()).skipIfStringMissing("policyId")
                );
    }
}
