package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class ChangeDateToNextDay extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        String requestDateString = OIDFJSON.getString(Optional.ofNullable(data.get("requestDate"))
            .orElseThrow(() -> error("No requestDate field in request object.")));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String newRequestDateString = LocalDate.parse(requestDateString, formatter)
            .plusDays(1)
            .format(formatter);

        data.addProperty("requestDate", newRequestDateString);
        logSuccess("requestDate field changed to next day.", args("data", data));
        return env;
    }
}
