package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateCapitalizationTitleWithdrawalCompromissosPessoaisEmergenciaisPostRequestBody extends AbstractCreateCapitalizationTitleWithdrawalRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject withdrawalInformation = data.get("withdrawalInformation").getAsJsonObject();
        withdrawalInformation.addProperty("withdrawalReason","COMPROMISSOS_PESSOAIS_EMERGENCIAIS");
    }
}
