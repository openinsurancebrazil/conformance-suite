package net.openid.conformance.opin.testmodule.fvp;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureBrazilCpf;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.EnsureDcrClientExists;
import net.openid.conformance.opin.testmodule.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinResourcesV2Endpoint;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-fvp-preflight-check-test-v2",
		displayName = "Pre-Flights test that ensures that tested server accepted a DCR request and all the required tested URIs are registered within the Directory",
		summary = "Make sure the brazilCpf has been provided\n" +
				"\n" +
				"Make sure a client_id has been generated on the DCR\n" +
				"\n" +
				"Call the Tested Server Token endpoint with client_credentials grant - Make sure that server returns a 200\n" +
				"\n" +
				"Call the Directory participants endpoint for either Sandbox or Production, depending on the used platform\n" +
				"\n" +
				"Make sure The Provided Well-Known is registered within the Directory\n" +
				"\n" +
				"Make sure that the server has registered a ApiFamilyType of value consents with ApiVersion of value “2.0.1\". Return the ApiEndpointthat ends with /consents/v2/consents\n" +
				"\n" +
				"Make sure that the server has registered a ApiFamilyType of value resources with ApiVersion of value “2.0.1\". Return the ApiEndpointthat ends with /resources/v2/resources",
		profile = OBBProfile.OPIN_PROFILE_PROD_FVP,
		configurationFields = {
				"server.discoveryUrl",
				"resource.brazilCpf",
				"resource.brazilCnpj"
		}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
		"resource.consentUrl","resource.brazilOrganizationId"
})
public class OpinFvpPreFlightCheckModule extends OpinAbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(EnsureBrazilCpf.class);
		callAndStopOnFailure(EnsureDcrClientExists.class);
		call(new ValidateWellKnownUriSteps());
	}

	@Override
	protected void runTests() {
		call(new ValidateRegisteredEndpoints(sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetOpinResourcesV2Endpoint.class))));
	}


	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
