package net.openid.conformance.opin.testmodule.phase3.quotePersonLife.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonLife.AbstractOpinQuotePersonLifeRjctTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life.GetPersonLifeQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life.PostPersonLifeOASValidatorV1n10;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-life_api_rejected_test-module_v1",
        displayName = "Ensure that a person life quotation request can be rejected.",
        summary = """
            Ensure that a Patrimonial life quotation request can be rejected. This test module will send the quoteData with minimal required fields, and expect the quotation to be rejected. This test applies to both Personal and Business products
            • Call POST life/request endpoint sending personal or business information, following what is defined at the config, and sending the quoteData with only the minimum required fields, and the termStartDate after the termEndDate
            • Expect 201 - Validate Response and ensure status is RCVD or RJCT
            • Poll the GET life/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET life/request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Respone and ensure status is RJCT
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonLifeRjctTestModule extends AbstractOpinQuotePersonLifeRjctTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPersonLifeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPersonLifeQuoteStatusOASValidatorV1n10.class;
    }
}