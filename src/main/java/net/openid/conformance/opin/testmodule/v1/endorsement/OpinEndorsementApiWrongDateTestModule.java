package net.openid.conformance.opin.testmodule.v1.endorsement;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.AbstractEndorsementApiWrongDateTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "opin-endorsement-api-wrong-date-test",
    displayName = "Ensure that endorsement cannot be created with a future request Date",
    summary = "Ensure that endorsement cannot be created with a future request Date\n" +
        "\u2022 Call the POST Consents API with all the existing Phase 2 permissions\n" +
        "\u2022 Expect 201 a with status on \"AWAITING_AUTHORISATION\" - Validate Response \n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the resources API\n" +
        "\u2022 Extract one policy ID on the resources APIs\n" +
        "\u2022 Call the DELETE Consents Endpoint for tha data Shared\n" +
        "\u2022 Expect a 204 NO Content\n" +
        "\u2022 Call the POST Consents Endpoint with the Endorsement permissions, sending the policyNumber as the extracted policyID\n" +
        "\u2022 Expects 201 - Validate Response is \"AWAITING_AUTHORISATION\"\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AWAITING_AUTHORISATION\"\n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"-  \n" +
        "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for INCLUSAO, set policy ID as the extracted policyID, set requestDate as D+1\n" +
        "\u2022 Expects 422 - Validate Error response\n" +
        "\u2022 Call thE GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"CONSUMED\"",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "resource.consentUrl",
        "resource.brazilCpf",
        "consent.productType"
    }
)
public class OpinEndorsementApiWrongDateTestModule extends AbstractEndorsementApiWrongDateTestModule {

}
