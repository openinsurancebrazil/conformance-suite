package net.openid.conformance.opin.testmodule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.opin.testmodule.support.*;

public class AbstractOpinCustomerGranularPermissionsTestModule extends AbstractOpinFunctionalTestModule{

    protected String productType;

    protected Class<? extends Condition> identificationsValidator;
    protected Class<? extends Condition> qualificationValidator;
    protected Class<? extends Condition> complimentaryInformationValidator;



    public static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

    @Override
    protected void requestProtectedResource() {
        validateResponse();
    }

    @Override
    protected void validateResponse() {
        validate(PrepareUrlForCustomerIdentifications.class, identificationsValidator, String.format("Fetch %s identifications", productType));
        validate(PrepareUrlForCustomerQualification.class, qualificationValidator, String.format("Fetch %s qualifications", productType));
        validateError(PrepareUrlForCustomerAdditionalInfo.class, String.format("Fetch %s Additional Info, expecting error", productType));
        callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);

    }

    protected void validate(Class<? extends Condition>prepareUrlCondition, Class<? extends Condition> validator, String logMsg) {
        callAndStopOnFailure(prepareUrlCondition);
        preCallProtectedResource(logMsg);
        callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
    }

    protected void validateError(Class<? extends Condition>prepareUrlCondition, String logMsg) {
        callAndStopOnFailure(prepareUrlCondition);
        eventLog.startBlock(logMsg);
        call(sequence(CallProtectedResourceExpectingFailureSequence.class));
        eventLog.endBlock();
        callAndContinueOnFailure(complimentaryInformationValidator, Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
    }

    protected void setProductType(String productType) {
        this.productType = productType;
        env.putString("product_type", productType);
    }

    public void setIdentificationsValidator(Class<? extends Condition> identificationsValidator) {
        this.identificationsValidator = identificationsValidator;
    }

    public void setQualificationsValidator(Class<? extends Condition> qualificationsValidator) {
        this.qualificationValidator = qualificationsValidator;
    }

    public void setComplimentaryInformationValidator(Class<? extends Condition> complimentaryInformationValidator) {
        this.complimentaryInformationValidator = complimentaryInformationValidator;
    }

}
