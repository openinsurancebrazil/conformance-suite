package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddPatrimonialScope;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetPatrimonialV1Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialPremiumOASValidatorV1n4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.util.Map;

@PublishTestModule(
        testName = "opin-fvp-patrimonial-api-test-module",
        displayName = "Validates the structure of all Patrimonial API resources",
        summary = "Validates the structure of all Patrimonial API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the Patrimonial API (“DAMAGES_AND_PEOPLE_PATRIMONIAL_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
                "\u2022 Calls GET Patrimonial “/” API\n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
                "\u2022 Calls GET Patrimonial policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields\n" +
                "\u2022 Calls GET patrimonial premium API \n" +
                "\u2022 Expects 200- Validate all the fields\n" +
                "\u2022 Calls GET patrimonial claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.consentSyncTime",
                "consent.productType"
        }

)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})

public class OpinFvpPatrimonialApiTestModule extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetPatrimonialV1Endpoint.class));
    }
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddPatrimonialScope.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_PATRIMONIAL;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePatrimonialListOASValidatorV1n4.class;
    }

    @Override
    protected String getApi() {
        return "insurance-patrimonial";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", GetInsurancePatrimonialPolicyInfoOASValidatorV1n4.class,
                "premium", GetInsurancePatrimonialPremiumOASValidatorV1n4.class,
                "claim", GetInsurancePatrimonialClaimOASValidatorV1n4.class
        );
    }
}