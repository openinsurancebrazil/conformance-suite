package net.openid.conformance.opin.testmodule.v1.auto;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;


public abstract class AbstractOpinAutoResourcesApiTestModule extends AbstractOpinApiResourcesTestModuleV2 {

	@Override
	protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
		return AddAutoScope.class;
	}

	@Override
	protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
		return BuildAutoConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected PermissionsGroup getPermissionsGroup() {
		return PermissionsGroup.DAMAGES_AND_PEOPLE_AUTO;

	}

	@Override
	protected String getApi() {
		return "insurance-auto";
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*]");
		return AllIdsSelectorFromJsonPath.class;
	}

	@Override
	protected String getResourceType() {
		return EnumOpinResourcesType.DAMAGES_AND_PEOPLE_AUTO.name();
	}

	@Override
	protected String getResourceStatus() {
		return EnumResourcesStatus.AVAILABLE.name();
	}

}
