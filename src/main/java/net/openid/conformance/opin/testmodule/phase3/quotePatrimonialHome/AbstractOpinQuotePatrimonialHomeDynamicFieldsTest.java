package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteWithDynamicFieldsTestModule;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAckn;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAcpt;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchAcknRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialHomePostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePatrimonialHomeDynamicFieldsTest extends AbstractOpinPhase3QuoteWithDynamicFieldsTestModule {

    @Override
    protected String getDynamicFieldApiType() {
        return "QUOTE_PATRIMONIAL";
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_HOME;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-patrimonial-home";
    }

    @Override
    protected String getApiName() {
        return "quote-patrimonial";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "home/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialHomePostRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchAcknRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasAckn.class;
    }
}
