package net.openid.conformance.opin.testmodule.phase3.quotePersonLife.v1n11;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonLife.AbstractOpinQuotePersonLifeCoreAcknTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.life.GetPersonLifeQuoteStatusOASValidatorV1n11;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.life.PatchPersonLifeOASValidatorV1n11;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.life.PostPersonLifeOASValidatorV1n11;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePersonLifePostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-life_api_core-ackn_test-module_v1.11.0",
        displayName = "Ensure that a person life quotation request can be successfully created and accepted afterwards.",
        summary = """
            Ensure that a person life quotation request can be successfully created and accepted afterwards. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            • Call POST life/request endpoint sending personal or business information, following what is defined at the config
            • Expect 201 - Validate Response and ensure status is RCVD
            • Poll the GET life/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET life/request/{consentId}/quote-status endpoint
            • Expect 200 - Validate response and ensure status is ACPT
            • Call PATCH life/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
            • Expect 200 - Validate response and ensure status is ACKN
            • Call GET links.redirect endpoint
            • Expect 200
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonLifeCoreAcknTestModuleV1n11 extends AbstractOpinQuotePersonLifeCoreAcknTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPersonLifeOASValidatorV1n11.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPersonLifeQuoteStatusOASValidatorV1n11.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPersonLifeOASValidatorV1n11.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePersonLifePostRequestBodyWithCoverage.class;
    }
}
