package net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n5.GetInsurancePatrimonialListOASValidatorV1n5;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantNotApplicable;

@VariantNotApplicable(parameter = FAPI1FinalOPProfile.class, values = {"openbanking_uk", "plain_fapi", "consumerdataright_au"})
public abstract class AbstractOpinPatrimonialBranchTestModuleV1n5 extends AbstractOpinPatrimonialBranchTestModule {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsurancePatrimonialListOASValidatorV1n5.class;
	}

	@Override
	protected void getIdSelector() {
		env.putString("selected_id_json_path", "$.data[*].companies[*].policies[*].policyId");
		callAndStopOnFailure(AllIdsSelectorFromJsonPath.class);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}
