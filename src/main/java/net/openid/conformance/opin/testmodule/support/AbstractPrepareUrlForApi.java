package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractPrepareUrlForApi extends AbstractCondition {

    private final String validatorRegex = "^(https://)(.*?)(/open-insurance/consents/v\\d+/consents)$";

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {
        String url = env.getString("config","resource.consentUrl");
        String apiVersion = new OPINApiVersions().getApiVersion(getApiReplacement());
        String apiReplacement = getApiReplacement();
        String endpointReplacement = getEndpointReplacement();

        if(!url.matches(validatorRegex)) {
            throw error("consentUrl is not valid, please ensure that url matches " + validatorRegex, args("consentUrl", url));
        }

        String protectedUrl = !apiReplacement.equals("consents")? url.replaceFirst("consents", apiReplacement).replaceFirst("consents", endpointReplacement) :
                url.substring(0, url.lastIndexOf("consents")).concat(endpointReplacement);
        protectedUrl = protectedUrl.replaceFirst("v\\d+", apiVersion);
        env.putString("protected_resource_url", protectedUrl);
        logSuccess(String.format("protected_resource_url for %s set up", apiReplacement), args("protected_resource_url", protectedUrl));
        return env;
    }

    protected abstract String getApiReplacement();

    protected abstract String getEndpointReplacement();
}
