package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class ClaimNotificationSelectResourcePolicyId extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_endpoint_response_full", strings = "claim_notification_endpoint_type")
    public Environment evaluate(Environment env) {
        JsonObject response = env.getObject("resource_endpoint_response_full");
        String bodyString = OIDFJSON.getString(Optional.ofNullable(response.get("body"))
            .orElseThrow(() -> error("body not present in the api response.")));
        JsonObject body = JsonParser.parseString(bodyString).getAsJsonObject();
        JsonElement data = body.get("data");
        JsonArray dataArray = data.getAsJsonArray();

        Set<String> possibleResourceTypes;
        switch (env.getString("claim_notification_endpoint_type")) {
            case "person":
                possibleResourceTypes = SetUtils.createSet("DAMAGES_AND_PEOPLE_PERSON");
                break;
            case "damage":
                possibleResourceTypes = SetUtils.createSet(
                    "DAMAGES_AND_PEOPLE_PATRIMONIAL, DAMAGES_AND_PEOPLE_RESPONSIBILITY, " +
                    "DAMAGES_AND_PEOPLE_TRANSPORT, DAMAGES_AND_PEOPLE_FINANCIAL_RISKS, " +
                    "DAMAGES_AND_PEOPLE_RURAL, DAMAGES_AND_PEOPLE_AUTO, " +
                    "DAMAGES_AND_PEOPLE_HOUSING, DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD"
                );
                break;
            default:
                throw error("Invalid claim_notification_endpoint_type.");
        }

        String policyId = "";
        String policyType = "";
        for (JsonElement element : dataArray) {
            JsonObject elementObject = element.getAsJsonObject();
            String type = OIDFJSON.getString(elementObject.get("type"));
            String status = OIDFJSON.getString(elementObject.get("status"));
            if (possibleResourceTypes.contains(type) && "AVAILABLE".equals(status)) {
                policyId = OIDFJSON.getString(elementObject.get("resourceId"));
                policyType = type;
                break;
            }
        }

        if (!Objects.equals(policyId, "")) {
            env.putString("policyId", policyId);
            env.putString("policy_type", policyType);
            logSuccess("policyId extracted from resources API response.", args("policyId", policyId));
        } else {
            throw error("There is no resource of the correct type.");
        }

        return env;
    }
}
