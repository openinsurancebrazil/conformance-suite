package net.openid.conformance.opin.testmodule.v1.rural;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractOpinRuralResourcesApiTest extends AbstractOpinApiResourcesTestModuleV2 {

    @Override
    protected String getResourceType() {
        return EnumOpinResourcesType.DAMAGES_AND_PEOPLE_RURAL.name();
    }

    @Override
    protected String getResourceStatus() {
        return EnumResourcesStatus.AVAILABLE.name();
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_RURAL;
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddRuralScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildRuralConfigResourceUrlFromConsentUrl.class;
    }

    @Override
    protected String getApi() {
        return "insurance-rural";
    }
}
