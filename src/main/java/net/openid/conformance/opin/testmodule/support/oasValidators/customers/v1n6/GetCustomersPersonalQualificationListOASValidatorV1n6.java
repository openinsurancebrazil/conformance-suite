package net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n6;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Customers 1.6.0")
public class GetCustomersPersonalQualificationListOASValidatorV1n6 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/customers/v1/swagger-customers-api-1.6.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/personal/qualifications";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
