package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.AbstractGetQuoteCapitalizationTitleOASValidator;

@ApiName("Quote Capitalization Title 1.10.0")
public class GetQuoteCapitalizationTitleOASValidatorV1n10 extends AbstractGetQuoteCapitalizationTitleOASValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteCapitalizationTitle/swagger-quote-capitalization-title-1.10.0.yaml";
    }
}