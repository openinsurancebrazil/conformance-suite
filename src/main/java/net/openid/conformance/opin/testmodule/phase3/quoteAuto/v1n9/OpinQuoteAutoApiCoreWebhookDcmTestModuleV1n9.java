package net.openid.conformance.opin.testmodule.phase3.quoteAuto.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteAuto.AbstractOpinQuoteAutoApiCoreWebhookDcrDcmTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.GetQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.PatchQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.PostQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteAutoPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-auto_api_core-webhook-dcm_test-module_v1.9.0",
        displayName = "Ensure that a Auto quotation request can be successfully created and accepted afterwards, and webhook is sent as required if actioned through a DCM.",
        summary = """
                    Ensure that a Auto quotation request can be successfully created and accepted afterwards, and webhook is sent as required if actioned through a DCM. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
                    For this test the institution will need to register on it’s software statement a webhook under the following format - <https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/><alias>
                    · Obtain a SSA from the Directory
                    · Ensure that on the SSA the attribute software_api_webhook_uris contains the URI <https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/><alias>, where the alias is to be obtained from the field alias on the test configuration
                    · Call the Registration Endpoint without sending the “webhook_uris” field.
                    · Expect a 201 - Ensure that the “webhook_uris” is not returned on the response body
                    · Call the PUT Registration Endpoint sending the field "webhook_uris":[“<https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/><alias>”]
                    · Expect a 201 - Validate Response
                    · Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds
                    · Set the consents webhook notification endpoint to be equal to <https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/><alias>/open-insurance/webhook/v1/quote/v1/request/{consentId}/quote-status, where the alias is to be obtained from the field alias on the test configuration
                    · Call POST auto request endpoint sending personal or business information, following what is defined at the config, and the consentId as the webhook defined on the test
                    · Expect 201 - Validate Response and ensure status is RCVD
                    · Wait for webhook incoming call, validate timestamp
                    · Respond 202 with na empty body
                    · Call GET quote-status endpoint
                    · Expect 200 - Validate Respones and ensure status is ACPT
                    · Call PATCH auto request endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
                    · Expect 200 - Validate Response and ensure status is ACKN
                    · Call GET links.redirect endpoint
                    · Expect 200
                    · Call the Delete Registration Endpoint
                    · Expect a 204 - No Content
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteAutoApiCoreWebhookDcmTestModuleV1n9 extends AbstractOpinQuoteAutoApiCoreWebhookDcrDcmTestModule {

    @Override
    protected boolean shouldAlsoDoDCM() {
        return true;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostQuoteAutoOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetQuoteAutoOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchQuoteAutoOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteAutoPostRequestBodyWithCoverage.class;
    }
}
