package net.openid.conformance.opin.testmodule.v1.transport.v1n3;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n3.GetInsuranceTransportClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n3.GetInsuranceTransportListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n3.GetInsuranceTransportPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n3.GetInsuranceTransportPremiumOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.transport.AbstractOpinTransportApiTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-transport-api-test-v1n3",
        displayName = "Validates the structure of all Transport API resources",
        summary ="Validates the structure of all Transport API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_TRANSPORT_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "\u2022 Calls GET transport “/” API \n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
                "\u2022 Calls GET transport policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields \n" +
                "\u2022 Calls GET transport premium API \n" +
                "\u2022 Expects 200- Validate all the fields \n" +
                "\u2022 Calls GET transport claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }

)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})

public class OpinTransportApiTestModuleV1n3 extends AbstractOpinTransportApiTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceTransportListOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator() {
        return GetInsuranceTransportPolicyInfoOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator() {
        return GetInsuranceTransportPremiumOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator() {
        return GetInsuranceTransportClaimOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }
}
