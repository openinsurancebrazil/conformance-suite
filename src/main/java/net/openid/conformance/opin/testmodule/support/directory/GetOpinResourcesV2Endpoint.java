package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetOpinResourcesV2Endpoint extends AbstractGetXFromAuthServer {


    @Override
    protected String getEndpointRegex() {
        return "^(https:\\/\\/)(.*?)(\\/open-insurance\\/resources\\/v\\d+/resources)$";
    }

    @Override
    protected String getApiFamilyType() {
        return "resources";
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(2.[0-9].[0-9])$";
    }

    @Override
    protected boolean isResource() {
        return true;
    }


}
