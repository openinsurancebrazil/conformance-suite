package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractPatchPersonTravelOASValidator;

@ApiName("Quote Person 1.10.2")
public class PatchPersonTravelOASValidatorV1n10 extends AbstractPatchPersonTravelOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.10.2.yaml";
    }

}
