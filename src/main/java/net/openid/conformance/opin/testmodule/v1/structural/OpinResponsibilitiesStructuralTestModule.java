package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n3.GetInsuranceResponsibilityClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n3.GetInsuranceResponsibilityListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n3.GetInsuranceResponsibilityPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n3.GetInsuranceResponsibilityPremiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-responsibilities-api-structural-test",
	displayName = "Validate structure of Responsibility API Endpoints 200 response",
	summary = "Validate structure of Responsibility API Endpoints 200 response \n"+
	"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"resource.consentUrl"
		}
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinResponsibilitiesStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-responsibility";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);
		setRootValidator(GetInsuranceResponsibilityListOASValidatorV1n3.class);
		setPolicyInfoValidator(GetInsuranceResponsibilityPolicyInfoOASValidatorV1n3.class);
		setPremiumValidator(GetInsuranceResponsibilityPremiumOASValidatorV1n3.class);
		setClaimValidator(GetInsuranceResponsibilityClaimOASValidatorV1n3.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

