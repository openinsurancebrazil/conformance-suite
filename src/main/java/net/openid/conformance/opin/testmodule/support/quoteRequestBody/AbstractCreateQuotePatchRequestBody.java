package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateQuotePatchRequestBody extends AbstractCondition {
    @Override
    @PreEnvironment(required = "config")
    @PostEnvironment(strings = "resource_request_entity")
    public Environment evaluate(Environment env) {

        String productType = env.getString("config", "consent.productType");
        JsonObject author = new JsonObject();
        if ("business".equals(productType)) {
            author.addProperty("identificationType", "CNPJ");
            author.addProperty("identificationNumber", env.getString("config", "resource.brazilCnpj"));
        } else {
            author.addProperty("identificationType", "CPF");
            author.addProperty("identificationNumber", env.getString("config", "resource.brazilCpf"));
        }

        JsonObject data = new JsonObject();
        data.add("author", author);
        editData(env, data);

        JsonObject requestData = new JsonObject();
        requestData.add("data", data);
        env.putString("resource_request_entity", requestData.toString());
        return env;
    }

    protected abstract void editData(Environment env, JsonObject data);

}