package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;

public class AddAllScopes extends AbstractScopeAddingCondition {
    @Override
    protected String newScope() {
        return "consents resources customers insurance-acceptance-and-branches-abroad insurance-auto " +
                "insurance-financial-risk insurance-patrimonial insurance-responsibility";
    }
}
