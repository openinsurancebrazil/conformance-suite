package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public abstract class AbstractAddClaimNotificationToConsentData extends AbstractCondition {

    @Override
    @PreEnvironment(required = "consent_endpoint_request", strings = "policyId")
    public Environment evaluate(Environment env) {
        String policyId = env.getString("policyId");
        String endpointType = env.getString("claim_notification_endpoint_type");
        String documentType = env.getString("documentType");
        String occurrenceDate = LocalDateTime.now(ZoneOffset.UTC)
                .minusDays(1)
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        JsonObject claimNotificationInformation = getJsonObject(documentType, policyId, occurrenceDate, "12:00:00");

        String proposalId = getProposalId();
        if(proposalId != null && (Objects.equals(endpointType, "person"))){
            claimNotificationInformation.addProperty("proposalId", proposalId);
        }

        JsonObject data = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();
        data.add("claimNotificationInformation", claimNotificationInformation);
        logSuccess("claimNotificationInformation added to consent request body", args("consent data", data));
        return env;
    }

    private JsonObject getJsonObject(String documentType, String policyId, String occurrenceDate, String occurrenceTime) {
        JsonObject claimNotificationInformation = new JsonObject();
        claimNotificationInformation.addProperty("documentType", documentType);
        claimNotificationInformation.addProperty(getPolicyFieldName(), policyId);
        if (Objects.equals(documentType, "CERTIFICADO") || Objects.equals(documentType, "CERTIFICADO_AUTOMOVEL")) {
            claimNotificationInformation.addProperty("groupCertificateId", policyId);
        }

        JsonArray insuredObjectIds = getInsuredObjectId();
        if (insuredObjectIds != null && !insuredObjectIds.isEmpty()) {
            claimNotificationInformation.add("insuredObjectId", insuredObjectIds);
        }

        claimNotificationInformation.addProperty("occurrenceDate", occurrenceDate);
        claimNotificationInformation.addProperty("occurrenceTime", occurrenceTime);
        claimNotificationInformation.addProperty("occurrenceDescription", OPINPhase3Keys.Keys_occurrenceDescription);
        return claimNotificationInformation;
    }

    protected abstract String getPolicyFieldName();

    protected JsonArray getInsuredObjectId() {
        return null;
    }

    protected String getProposalId() {
        return null;
    }
}