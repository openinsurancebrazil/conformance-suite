package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeConsentTestModule;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-diverse-risks_api_negative-consent_test-module_v1",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = "Ensure a consent cannot be created in unhappy requests.\n" +
                "\u2022 Call POST Consents with permission for Customer Data, PF or PJ, and Patrimonial Lead.\n" +
                "\u2022 Expect 422 - Validate Error Response\n" +
                "\u2022 Call POST Consents with permission for Patrimonial Lead and the permission for this specified Patrimonial Branch.\n" +
                "\u2022 Expect 422 - Validate Error Response.\n" +
                "\u2022 Call POST Consents with permission for Patrimonial Lead.\n" +
                "\u2022 Expect 422 - Validate Error Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialDiverseRisksNegativeConsentTestModule extends AbstractOpinPhase3QuoteNegativeConsentTestModule {
    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.QUOTE_PATRIMONIAL_DIVERSE_RISKS;
    }
}
