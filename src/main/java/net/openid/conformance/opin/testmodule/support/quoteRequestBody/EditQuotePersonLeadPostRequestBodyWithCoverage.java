package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuotePersonLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "MORTE";
    }
}