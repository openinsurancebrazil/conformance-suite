package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.AbstractGetInsuranceAutoClaimOASValidator;

@ApiName("Insurance Auto 1.3.0")
public class GetInsuranceAutoClaimOASValidatorV1n3 extends AbstractGetInsuranceAutoClaimOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceAuto/v1/swagger-insurance-auto-api.yaml";
	}
}
