package net.openid.conformance.opin.testmodule.support;

public class PrepareToGetPersonalIdentifications extends AbstractPrepareUrlForApi {

	@Override
	protected String getApiReplacement() {
		return "customers";
	}

	@Override
	protected String getEndpointReplacement() {
		return "personal/identifications";
	}
}
