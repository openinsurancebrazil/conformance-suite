package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractAddClaimNotificationToConsentData;
import net.openid.conformance.opin.testmodule.support.AddClaimNotificationToConsentDataDiff;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyExclusaoDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyExclusaoDiff;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1n3.PostEndorsementOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.endorsement.AbstractEndorsementWrongPermissionsTestModule;

public class AbstractOpinEndorsementWrongPermissionsTestV1n extends AbstractEndorsementWrongPermissionsTestModule {

        @Override
        protected AbstractJsonAssertingCondition validator() {
                return new PostEndorsementOASValidatorV1n3();
        }

        @Override
        protected AbstractJsonAssertingCondition postConsentValidator() {
                return new PostConsentsOASValidatorV2n7();
        }

        @Override
        protected void validateGetConsentResponse() {
                callAndContinueOnFailure(GetConsentsOASValidatorV2n7.class, Condition.ConditionResult.FAILURE);
        }

        @Override
        protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
                return new CreateEndorsementRequestBodyExclusaoDiff();
        }

        @Override
        protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
                return CreateEndorsementConsentBodyExclusaoDiff.class;
        }

        @Override
        protected AbstractAddClaimNotificationToConsentData setClaimNotificationData() {
                return new AddClaimNotificationToConsentDataDiff();
        }
}
