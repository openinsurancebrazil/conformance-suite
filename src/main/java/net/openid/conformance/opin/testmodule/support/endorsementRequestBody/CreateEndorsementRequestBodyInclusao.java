package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyInclusao extends AbstractCreateEndorsementRequestBody {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.INCLUSAO;
    }
}
