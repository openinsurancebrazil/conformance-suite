package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddConsentIdFromConfigToEnvironment extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    @PostEnvironment(strings = "consent_id")
    protected Environment evaluate(Environment env) {
        String consentId = env.getString("config","resource.consentId");
        if (consentId == null || consentId.equals("")) {
            throw error("consentId value not present on configuration");
        }
        env.putString("consent_id", consentId);
        logSuccess("consentId added to Environment");
        return env;
    }
}
