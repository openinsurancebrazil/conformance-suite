package net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetResourcesOASValidatorV2n4 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/resources/v2/swagger-resources-api-v2.yml";
    }

    @Override
    protected String getEndpointPath() {
        return "/resources";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
