package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetEndorsementTypeToInvalid extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        data.addProperty("endorsementType", "INVALID");
        logSuccess("endorsementType changed to INVALID", args("data", data));
        return env;
    }
}
