package net.openid.conformance.opin.testmodule.v1.consents.abstracts;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRevokedByUser;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.v2.GenerateRefreshAccessTokenSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersPersonalIdentificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.ClientAuthType;


public abstract class AbstractOpinConsentsApiDeleteTestModule extends AbstractOpinFunctionalTestModule {

	protected abstract AbstractJsonAssertingCondition getValidator();
	protected ClientAuthType clientAuthType;
	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		env.putString("api_type", API_TYPE);
		env.putString("proceed_with_test", "true");

		return new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication)
				.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
						sequenceOf(
								condition(CreateRandomFAPIInteractionId.class),
								condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
						));
	}

	@Override
	protected void configureClient() {
		super.configureClient();
		callAndStopOnFailure(BuildOpinCustomCustomersConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void validateClientConfiguration() {
		super.validateClientConfiguration();
		new ScopesAndPermissionsBuilder(env, eventLog).addScopes(OPINScopesEnum.OPEN_ID, OPINScopesEnum.CONSENTS).build();
		callAndStopOnFailure(AddScopesForCustomerApi.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
		permissionsBuilder.buildFromEnv();
		clientAuthType = getVariant(ClientAuthType.class);
	}

	@Override
	protected void requestProtectedResource(){

		eventLog.startBlock("Try calling protected resource after user authentication");
		callAndStopOnFailure(PrepareToGetCustomCustomerIdentificationsOpin.class);
		callAndStopOnFailure(SaveProtectedResourceAccessToken.class);
		callAndStopOnFailure(CallProtectedResource.class);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(LoadConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ExtractConsentIdFromConsentEndpointResponse.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");

		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(LoadConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(getValidator().getClass(), Condition.ConditionResult.FAILURE);
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");
		callAndStopOnFailure(CloneObjectConsentResponseToResourceResponse.class);
		callAndContinueOnFailure(EnsureConsentStatusWasRejected.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentRevokedByUser.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(LoadProtectedResourceAccessToken.class);

		eventLog.startBlock("Try calling protected resource after consent is deleted");
		callAndStopOnFailure(PrepareToGetCustomCustomerIdentificationsOpin.class);
		callAndStopOnFailure(CallProtectedResource.class);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(GetCustomersPersonalIdentificationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);

		call(new GenerateRefreshAccessTokenSteps(clientAuthType)
			.replace(
				CallTokenEndpoint.class,
				condition(CallTokenEndpointAndReturnFullResponse.class))
			.insertAfter(
				CallTokenEndpoint.class,
				sequenceOf(condition(EnsureTokenResponseWasAFailure.class).dontStopOnFailure(),
					       condition(EnsureTokenResponseWas400.class)))
						.skip(CheckIfTokenEndpointResponseError.class, "The response code was 400, as expected")
						.skip(CheckForAccessTokenValue.class, "The response code was 400, as expected")
						.skip(ExtractAccessTokenFromTokenResponse.class, "The response code was 400, as expected")
				);
	}

	@Override
	protected void validateResponse() {}
}
