package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.AbstractPostClaimNotificationPersonOASValidator;

@ApiName("Post Claim Notification Person V1.4.0")
public class PostClaimNotificationPersonOASValidatorV1n4 extends AbstractPostClaimNotificationPersonOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/claimNotification/swagger-claim-notification-1.4.0.yaml";
    }
}
