package net.openid.conformance.opin.testmodule.v1.consents.v2n6;


import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.GetConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentsApiDeleteTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
	testName = "opin-consents-api-delete-test-v2n6",
	displayName = "Makes sure that after consent has been deleted no more tokens can be issued with the related refresh token ",
	summary = "Makes sure that after consent has been deleted no more tokens can be issued with the related refresh token \n" +
		"• Call the POST Consents\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user\n" +
		"• Calls the Token endpoint using the authorization code flow\n" +
		"• Makes sure a valid access token has been created\n" +
		"• Calls the Protected Resource endpoint to make sure a valid access token has been created\n" +
		"• Calls the DELETE Consents API\n" +
		"• Expects  204 - NO_CONTENT\n" +
		"• Calls the GET Consents API\n" +
		"• Expects 200 - Validate Response and Status is \"REJECTED\", rejectedBy is \"USER\" and reason is \"CUSTOMER_MANUALLY_REVOKED\"\n" +
		"• Calls the Protected Resource endpoint\n" +
		"• Expects 401 - Validate response\n" +
		"• Calls the token endpoint to issue a new access token using the refresh token\n" +
		"• Expects a 400",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinConsentsApiDeleteTestModuleV2n6 extends AbstractOpinConsentsApiDeleteTestModule {

	@Override
	protected AbstractJsonAssertingCondition getValidator() {
		return new GetConsentsOASValidatorV2n6();
	}
}