package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts;

import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.AbstractCreateEndorsementConsentBody;
import net.openid.conformance.opin.testmodule.support.endorsementConsentBody.CreateEndorsementConsentBodyInclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.AbstractCreateEndorsementRequestBody;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBody.CreateEndorsementRequestBodyInclusao;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.ChangeDateToNextDay;

public abstract class AbstractEndorsementApiWrongDateTestModule extends AbstractEndorsementApiNegativeTestModule {

    @Override
    protected AbstractCreateEndorsementRequestBody createEndorsementRequestBody() {
        return new CreateEndorsementRequestBodyInclusao();
    }

    @Override
    protected void editEndorsementRequestBody() {
        callAndStopOnFailure(ChangeDateToNextDay.class);
    }

    @Override
    protected Class<? extends AbstractCreateEndorsementConsentBody> createEndorsementConsentBody() {
        return CreateEndorsementConsentBodyInclusao.class;
    }
}
