package net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.AbstractPatchQuoteAutoOASValidator;

@ApiName("Quote Auto 1.9.0")
public class PatchQuoteAutoOASValidatorV1n9 extends AbstractPatchQuoteAutoOASValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteAuto/quote-auto-v1.9.0.yaml";
    }

}
