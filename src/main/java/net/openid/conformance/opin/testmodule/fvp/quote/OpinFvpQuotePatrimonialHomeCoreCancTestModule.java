package net.openid.conformance.opin.testmodule.fvp.quote;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.fvp.quote.abstracts.AbstractFvpOpinQuoteApiTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.GetPatrimonialHomeQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PatchPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.home.PostPatrimonialHomeOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchCancRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialHomePostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-fvp-quote-patrimonial-home_api_core-canc_test-module",
        displayName = "Ensure that a Patrimonial Home quotation request can be successfully created and rejected afterwards.",
        summary = "Ensure that a Patrimonial Home quotation request can be successfully created and rejected afterwards. This test applies to both Personal and Business products. If the \"brazilCnpj\" field is filled out, it will transmit business information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST home/request endpoint sending personal or business information, following what is defined at the config\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Poll the GET home/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET home/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respones and ensure status is ACPT\n" +
                "\u2022 Call PATCH home/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC\n" +
                "\u2022 Expect 200 - Validate Response and ensure status is CANC",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinFvpQuotePatrimonialHomeCoreCancTestModule extends AbstractFvpOpinQuoteApiTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialHomeQuoteStatusOASValidatorV1n10.class;
    }


    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialHomeOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchCancRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasCanc.class;
    }

   @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_HOME;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-patrimonial-home";
    }

    @Override
    protected String getApiName() {
        return "quote-patrimonial";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "home/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialHomePostRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }
}
