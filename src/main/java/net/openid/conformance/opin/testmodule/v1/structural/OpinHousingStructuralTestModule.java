package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingPremiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
    testName = "opin-housing-api-structural-test",
    displayName = "Validate structure of Housing API Endpoint 200 response",
    summary = "Validate structure of Housing API Endpoint 200 response \n"+
            "• Call the “/\" endpoint - Expect 200 and validate response\n" +
            "• Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
            "• Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
            "• Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "resource.consentUrl"
        }
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinHousingStructuralTestModule extends AbstractOpinDataStructuralTestModule {
    private static final String API = "insurance-housing";

    @Override
    public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
        setApi(API);
        setRootValidator(GetInsuranceHousingListOASValidatorV1n3.class);
        setPolicyInfoValidator(GetInsuranceHousingPolicyInfoOASValidatorV1n3.class);
        setPremiumValidator(GetInsuranceHousingPremiumOASValidatorV1n3.class);
        setClaimValidator(GetInsuranceHousingClaimOASValidatorV1n3.class);

        super.configure(config, baseUrl, externalUrlOverride);
    }
}
