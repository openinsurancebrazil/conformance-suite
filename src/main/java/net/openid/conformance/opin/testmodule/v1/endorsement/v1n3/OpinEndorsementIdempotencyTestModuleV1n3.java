package net.openid.conformance.opin.testmodule.v1.endorsement.v1n3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n.AbstractOpinEndorsementIdempotencyTestModuleV1n;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-endorsement-api_idempotency_test-v1n3",
        displayName = "Ensure that after an initial endorsement request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idempotency key the request fails",
        summary = """
                Ensure that after an initial endorsement request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idempotency key the request fails
                - Call the POST Consents API with all the existing Phase 2 permissions
                - Expects 201 - Validate Response
                - Redirect the user to authorize consent
                - Call the GET Consents
                - Expects 200 - Validate Response is "AUTHORISED"
                - Call the resources API
                - Extract one policy ID on the resources APIs
                - Call the POST Consents Endpoint with the Endorsement permissions, sending the policyID as one the extracted policyID
                - Expects 201 - Validate Response
                - Redirect the user to authorize consent
                - Call the GET Consents Endpoint
                - Expects 200 - Validate Response is "AUTHORISED"
                - Call the POST Endorsement Endpoint with pre-defined Payload - set endorsementType as EXCLUSAO, set policyID as the one extracted
                - Expects 201 - Validate Response
                - Call the POST Endorsement Endpoint with the same payload and idempotency id
                - Expects 201 - Validate Response
                - Call the POST Endorsement, with a different payload as the previous request but the same idempotency id
                - Expects 422 ERRO_IDEMPOTENCIA - Validate Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinEndorsementIdempotencyTestModuleV1n3 extends AbstractOpinEndorsementIdempotencyTestModuleV1n {

}
