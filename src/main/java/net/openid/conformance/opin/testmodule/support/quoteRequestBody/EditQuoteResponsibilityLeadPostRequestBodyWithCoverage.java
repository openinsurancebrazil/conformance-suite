package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuoteResponsibilityLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "DANOS_CAUSADOS_A_TERCEIROS";
    }
}