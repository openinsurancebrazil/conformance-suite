package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractGetPersonTravelQuoteStatusOASValidator;

@ApiName("Quote Person 1.11.0")
public class GetPersonTravelQuoteStatusOASValidatorV1n11 extends AbstractGetPersonTravelQuoteStatusOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.11.0.yaml";
    }

}