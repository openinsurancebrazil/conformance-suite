package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.opin.testmodule.v1.patrimonial.PatrimonialBranches;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class VerifyBranch extends AbstractJsonAssertingCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full",strings = "branch")
	public Environment evaluate(Environment env) {
		Boolean branchFound = false;
		JsonElement body = bodyFrom(env,"resource_endpoint_response_full");

		String branchCode = env.getString("branch");
		PatrimonialBranches branch = findBranch(branchCode);
		if (branch == null) {
			throw error("Non-existent branch code saved in the environment.", args("branchCode", branchCode));
		}
		JsonElement insuredObjectsElement = findByPath(body,"$.data.insuredObjects");

		for(int i=0; i<insuredObjectsElement.getAsJsonArray().size() && !branchFound; i++)
		{
			JsonElement coveragesElement = findByPath(body,"$.data.insuredObjects["+i+"].coverages");
			for(int j=0; j<coveragesElement.getAsJsonArray().size() && !branchFound; j++){
				String path = "$.data.insuredObjects["+ i +"].coverages["+ j +"].branch";

				JsonElement branchElement = findByPath(body, path);

				if (branchElement.isJsonPrimitive() && OIDFJSON.getString(branchElement).equals(branch.getBranchCode())) {
					logSuccess(String.format("Successfully found a policyID of type %s", branch.name()));
					branchFound = true;
				}
			}
		}
		env.putBoolean("branch_found", branchFound);

 		return env;
	}

	private PatrimonialBranches findBranch(String branchCode) {

		for (PatrimonialBranches b : PatrimonialBranches.values()) {
			if (b.getBranchCode().equals(branchCode)) {
				return b;
			}
		}
		return null;
	}
}
