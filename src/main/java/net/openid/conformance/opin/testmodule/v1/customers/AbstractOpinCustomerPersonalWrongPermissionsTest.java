package net.openid.conformance.opin.testmodule.v1.customers;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.opin.testmodule.AbstractOpinPermissionsCheckingFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinCustomerPersonalWrongPermissionsTest extends AbstractOpinPermissionsCheckingFunctionalTestModule {

	private static final String API = "insurance-customer-personal";
	OpinConsentPermissionsBuilder permissionsBuilder;

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalIdentificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalQualificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalComplimentaryInfoValidator();

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpinPreAuthorizationConsentApi steps = new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
		return steps;
	}

	@Override
	protected void configureClient(){
		callAndStopOnFailure(PrepareConfigForCustomerPersonalTest.class);
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		callAndStopOnFailure(BuildOpinPersonalCustomersConfigResourceUrlFromConsentUrl.class);
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		//Simple UI fix
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		callAndStopOnFailure(AddScopesForCustomerApi.class);
		setApi(API);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void preFetchResources() {
		//Not needed for this test
	}

	@Override
	protected void prepareCorrectConsents() {
		permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL).build();
		callAndStopOnFailure(PrepareToGetPersonalQualifications.class);
	}

	@Override
	protected void prepareIncorrectPermissions() {
		permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.ALL_PHASE2)
			.removePermissionsGroups(PermissionsGroup.CUSTOMERS_PERSONAL)
			.removePermissionsGroups(PermissionsGroup.CUSTOMERS_BUSINESS).build();
	}

	@Override
	protected void requestResourcesWithIncorrectPermissions() {
		runInBlock("Ensure we cannot call the  Customer Personal Qualification", () -> {
			callAndStopOnFailure(PrepareToGetPersonalQualifications.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getCustomerPersonalQualificationValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the Customer Personal Identifications", () -> {
			callAndStopOnFailure(PrepareToGetPersonalIdentifications.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getCustomerPersonalIdentificationValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the Customer Personal Complimentary Information", () -> {
			callAndStopOnFailure(PrepareToGetPersonalComplimentaryInformation.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getCustomerPersonalComplimentaryInfoValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});
	}


}
