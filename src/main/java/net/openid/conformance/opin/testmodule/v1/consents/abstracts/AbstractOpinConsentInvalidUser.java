package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;


public abstract class AbstractOpinConsentInvalidUser extends AbstractOpinFunctionalTestModule {

	OpinConsentPermissionsBuilder permissionsBuilder;
	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	@Override
	protected void configureClient(){
		super.configureClient();
		env.putString("api_type", API_TYPE);
		//Arbitrary resource
		callAndStopOnFailure(BuildFinancialRiskConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		new ScopesAndPermissionsBuilder(env, eventLog).addScopes(OPINScopesEnum.OPEN_ID, OPINScopesEnum.CONSENTS).build();
		callAndStopOnFailure(DeleteCnpjFromConfig.class);
		callAndStopOnFailure(OpinAddDummyCpfToConfig.class);
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.resetPermissions()
			.addPermissionsGroup(PermissionsGroup.ALL_PERSONAL_PHASE2)
			.removePermissionsGroups(PermissionsGroup.CUSTOMERS_BUSINESS)
			.build();
	}

	@Override
	protected void onAuthorizationCallbackResponse() {

		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");
		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);
		callAndContinueOnFailure(CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint.class, Condition.ConditionResult.WARNING, "OIDCC-3.1.2.6");
		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");
		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");
		fireTestFinished();
	}

	@Override
	protected void validateResponse() {
	}
}
