package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

import com.google.gson.JsonArray;

public abstract class AbstractEndorsementRequestBodyDiff extends AbstractCreateEndorsementRequestBody {

    @Override
    protected String getPolicyFieldName() {
        return "policyId";
    }

    @Override
    protected JsonArray getInsuredObjectId() {
        JsonArray insuredObjectIds = new JsonArray();
        insuredObjectIds.add("216731531723");
        return insuredObjectIds;
    }
}
