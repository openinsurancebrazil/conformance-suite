package net.openid.conformance.opin.testmodule.phase3.quotePersonTravel;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePersonTravelPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePersonTravelNegativeTestModule extends AbstractOpinPhase3QuoteNegativeTestModule {

    @Override
    protected OPINScopesEnum getLeadScope() {
        return OPINScopesEnum.QUOTE_PERSON_LEAD;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PERSON_TRAVEL;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-person-travel";
    }

    @Override
    protected String getApiName() {
        return "quote-person";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "travel/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePersonTravelPostRequestBody.class;
    }
}
