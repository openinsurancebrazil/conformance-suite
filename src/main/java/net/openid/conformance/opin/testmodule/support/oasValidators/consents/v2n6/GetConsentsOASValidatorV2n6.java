package net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Consents 2.6.0")
public class GetConsentsOASValidatorV2n6 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/consents/v2n6/swagger-consents-api-2.0.6.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/consents/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

    @Override
    protected ResponseEnvKey getResponseEnvKey(){
        return ResponseEnvKey.FullConsentResponseEnvKey;
    }
}
