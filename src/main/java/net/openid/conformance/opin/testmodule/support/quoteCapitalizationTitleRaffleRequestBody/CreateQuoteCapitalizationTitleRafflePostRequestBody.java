package net.openid.conformance.opin.testmodule.support.quoteCapitalizationTitleRaffleRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.AbstractCreateCapitalizationTitleXRequestBody;
import net.openid.conformance.testmodule.Environment;

public class CreateQuoteCapitalizationTitleRafflePostRequestBody extends AbstractCreateCapitalizationTitleXRequestBody {
    @Override
    @PreEnvironment(required = {"config", "series_1"})
    @PostEnvironment(required = "resource_request_entity", strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject requestData = new JsonObject();
        JsonObject data = new JsonObject();

        JsonObject series = env.getObject("series_1");

        data.addProperty("modality", series.get("modality").getAsString());
        data.addProperty("susepProcessNumber", series.get("susepProcessNumber").getAsString());
        data.addProperty("contactType", "EMAIL");
        data.addProperty("email", "contact@email.com");


        JsonObject phone = new JsonObject();
        data.add("phone", phone);

        data.addProperty("cpfNumber", env.getString("config", "resource.brazilCpf"));

        editData(env, data);

        requestData.add("data", data);
        logSuccess("The request body was created", args("body", requestData));
        env.putObject("resource_request_entity", requestData);
        env.putString("resource_request_entity", requestData.toString());
        return env;
    }

    @Override
    protected void editData(Environment env, JsonObject data) {

    }
}

