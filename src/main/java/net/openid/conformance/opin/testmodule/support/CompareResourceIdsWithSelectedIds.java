package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Map;

public class CompareResourceIdsWithSelectedIds extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"extracted_resource_id"}, strings = {"selected_id"})
	public Environment evaluate(Environment env) {

		String selectedIdsString = env.getString("selected_id");

		JsonArray extractedSelectedIds = JsonParser.parseString(selectedIdsString).getAsJsonArray();
		JsonArray extractedResourcesIds = env.getElementFromObject("extracted_resource_id", "extractedResourceIds").getAsJsonArray();

		if (extractedSelectedIds.isEmpty()) {
			throw error("Extracted API IDs array is empty");
		}

		if (extractedResourcesIds.isEmpty()) {
			throw error("Extracted Resources IDs array is empty");
		}

		if (extractedSelectedIds.size() == extractedResourcesIds.size()) {
			log("Sizes are equal", Map.of("PolicyIdsSize", extractedSelectedIds.size(), "ResourceIdsSize", extractedResourcesIds.size()));
			for (JsonElement extractedResourcesId : extractedResourcesIds) {
				if (!extractedSelectedIds.contains(extractedResourcesId)) {
					throw error("API resources do not have a resource fetched from the resource endpoint response",
						Map.of("Missing ID", extractedResourcesId,
							"extractedSelectedIds", extractedSelectedIds,
							"extractedResourceIds", extractedResourcesIds)
					);
				}
			}
		} else {
			throw error("Sizes of two resource lists are not equal", Map.of(
				"SelectedIdsSize", extractedSelectedIds.size(),
				"ResourceIdsSize", extractedResourcesIds.size(),
				"extractedSelectedIds", extractedSelectedIds,
				"extractedResourceIds", extractedResourcesIds)
			);
		}

		logSuccess("resourceId values are identical");
		return env;
	}
}
