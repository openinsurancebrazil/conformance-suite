package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class TranslatePolicyTypeToApi extends AbstractCondition {

    private String policyTypeToApi(String policyType) {
        String api = policyType.replace("DAMAGES_AND_PEOPLE_", "")
            .replaceAll("_", "-").toLowerCase();
        if (api.equals("financial-risks")) {
            api = "financial-risk";
        }
        return "insurance-" + api;
    }

    @Override
    @PreEnvironment(strings = "policy_type")
    @PostEnvironment(strings = "api")
    public Environment evaluate(Environment env) {
        String policyType = env.getString("policy_type");
        String api = policyTypeToApi(policyType);
        env.putString("api", api);
        logSuccess("Successfully extracted api from resource type.", args("api", api));
        return env;
    }
}
