package net.openid.conformance.opin.testmodule.v1.lifePension;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractLifePensionApiResourcesTest extends AbstractOpinApiResourcesTestModuleV2 {

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddLifePensionScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceLifePensionUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.LIFE_PENSION;
    }

    @Override
    protected String getApi() {
        return "insurance-life-pension";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[*].brand.companies[*].contracts[*].certificateId");
        return AllIdsSelectorFromJsonPath.class;
    }

    @Override
    protected String getResourceType() {
        return EnumOpinResourcesType.LIFE_PENSION.name();
    }

    @Override
    protected String getResourceStatus() {
        return EnumResourcesStatus.AVAILABLE.name();
    }
}
