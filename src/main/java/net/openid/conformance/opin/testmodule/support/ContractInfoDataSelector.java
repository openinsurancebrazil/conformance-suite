package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ContractInfoDataSelector extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    public Environment evaluate(Environment env) {
        {
            JsonObject body = bodyFrom(env).getAsJsonObject();
            JsonObject data = Optional.ofNullable(body.getAsJsonObject("data"))
                    .orElseThrow(() -> error("Could not find data in the body", args("body", body)));

            if (data.isEmpty()) {
                throw error("Data array cannot be empty to extract pmbacAmount");
            }

            JsonArray suseps = Optional.ofNullable(data.getAsJsonArray("suseps"))
                    .orElseThrow(() -> error("Could not extract suseps array from the data", args("data", data)));

            if (suseps.isEmpty()) {
                throw error("Suseps array cannot be empty to extract pmbacAmount");
            }

            JsonObject susepsObject = suseps.get(0).getAsJsonObject();

            JsonArray fieArray = Optional.ofNullable(susepsObject.getAsJsonArray("FIE"))
                    .orElseThrow(() -> error("Could not extract FIE array from the data", args("data", data)));

            if (fieArray.isEmpty()) {
                throw error("FIE array cannot be empty to extract pmbacAmount");
            }

            JsonObject fieObject = fieArray.get(0).getAsJsonObject();

            JsonObject pmbacAmount = Optional.ofNullable(fieObject.getAsJsonObject("pmbacAmount"))
                    .orElseThrow(() -> error("Could not extract pmbacAmount object from the data", args("data", data)));

            if (pmbacAmount.isEmpty()) {
                throw error("PmbacAmount object cannot be empty");
            }

            if(env.containsObject("pmbacAmount_1")){
                env.putObject("pmbacAmount_2",pmbacAmount);
            } else {
                env.putObject("pmbacAmount_1",pmbacAmount);
            }
            return env;
        }
    }

        private JsonElement bodyFrom(Environment env) {
            try {
                return BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
                        .orElseThrow(() -> error("Could not find response body in the environment"));
            } catch (ParseException e) {
                throw error("Could not parse response body");
            }
        }

    }
