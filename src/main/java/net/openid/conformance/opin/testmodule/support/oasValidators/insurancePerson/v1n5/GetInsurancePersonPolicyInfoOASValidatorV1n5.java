package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.AbstractGetInsurancePersonPolicyInfoOASValidator;

@ApiName("Insurance Person 1.5.0")
public class GetInsurancePersonPolicyInfoOASValidatorV1n5 extends AbstractGetInsurancePersonPolicyInfoOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insurancePerson/v1/swagger-insurance-person.yaml";
	}
}
