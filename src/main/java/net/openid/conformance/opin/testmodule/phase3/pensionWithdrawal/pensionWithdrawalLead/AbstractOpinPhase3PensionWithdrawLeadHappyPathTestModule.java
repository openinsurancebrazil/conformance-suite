package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.pensionWithdrawalLead;

import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.AbstractOpinPhase3PensionWithdrawHappyPathTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawalLead.v1n3.PostPensionWithdrawalLeadOASValidatorV1n3;

public abstract class AbstractOpinPhase3PensionWithdrawLeadHappyPathTestModule extends AbstractOpinPhase3PensionWithdrawHappyPathTestModule {

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostPensionWithdrawalLeadOASValidatorV1n3();
    }

    @Override
    protected PermissionsGroup getPermissionsGroup2() {
        return PermissionsGroup.PENSION_WITHDRAWAL_LEAD;
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }

    @Override
    protected void executeTest() {
        postPensionWithdrawal();
        validatePensionWithdrawalResponse();
        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
    }

    @Override
    protected void setupSecondFlow() {
        runInBlock("Creating consent with Pension Withdrawal permissions", () -> {
            callAndStopOnFailure(OpinOverrideScopeWithOpenId.class);
            OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
            permissionsBuilder.resetPermissions().addPermissionsGroup(getPermissionsGroup2()).build();
            callAndStopOnFailure(AddWithdrawalPensionLeadScope.class);
        });
        callAndStopOnFailure(createPensionWithdrawalConsentBody().getClass());
    }

    protected void postPensionWithdrawal() {
        eventLog.startBlock("POST Pension Withdrawal - Expecting 201");
        callAndStopOnFailure(PrepareUrlForPensionWithdrawalLead.class);
        callAndStopOnFailure(createPensionWithdrawalRequestBody().getClass());
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        setIdempotencyKey();
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(AddJsonAcceptHeaderRequest.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
    }
}
