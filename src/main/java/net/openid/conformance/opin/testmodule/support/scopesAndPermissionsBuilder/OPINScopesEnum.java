package net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder;

import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.ScopesEnum;

public enum OPINScopesEnum implements ScopesEnum {
    QUOTE_PATRIMONIAL_LEAD("quote-patrimonial-lead"),
    QUOTE_RESPONSIBILITY_LEAD("quote-responsibility-lead"),
    QUOTE_RURAL_LEAD("quote-rural-lead"),
    QUOTE_AUTO_LEAD("quote-auto-lead"),
    QUOTE_TRANSPORT_LEAD("quote-transport-lead"),
    QUOTE_AUTO("quote-auto"),
    QUOTE_ACCEPTANCE_AND_BRANCHES_ABROAD_LEAD("quote-acceptance-and-branches-abroad-lead"),
    QUOTE_FINANCIAL_RISK_LEAD("quote-financial-risk-lead"),
    QUOTE_HOUSING_LEAD("quote-housing-lead"),
    QUOTE_PATRIMONIAL_HOME("quote-patrimonial-home"),
    QUOTE_PATRIMONIAL_BUSINESS("quote-patrimonial-business"),
    QUOTE_PATRIMONIAL_CONDOMINIUM("quote-patrimonial-condominium"),
    QUOTE_PATRIMONIAL_DIVERSE_RISKS("quote-patrimonial-diverse-risks"),
    DYNAMIC_FIELDS("dynamic-fields"),
    OPEN_ID("openid"),
    CONSENTS("consents"),
    QUOTE_PERSON_LEAD("quote-person-lead"),
    QUOTE_PERSON_LIFE("quote-person-life"),
    QUOTE_PERSON_TRAVEL("quote-person-travel"),
    CONTRACT_LIFE_PENSION_LEAD("contract-life-pension-lead"),
    CONTRACT_LIFE_PENSION("contract-life-pension"),
    CAPITALIZATION_TITLE("capitalization-title"),
    QUOTE_CAPITALIZATION_TITLE_LEAD("quote-capitalization-title-lead"),
    QUOTE_CAPITALIZATION_TITLE("quote-capitalization-title");

    private final String scope;

    OPINScopesEnum(String scope) {
        this.scope = scope;
    }

    @Override
    public String getScope() {
        return scope;
    }
}
