package net.openid.conformance.opin.testmodule.phase3;

import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePatrimonialLeadTest extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_LEAD;
    }

    @Override
    protected String getApiName() {
        return "quote-patrimonial";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}

