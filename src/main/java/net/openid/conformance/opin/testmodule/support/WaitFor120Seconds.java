package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.common.AbstractWaitForSpecifiedSeconds;
import net.openid.conformance.testmodule.Environment;

public class WaitFor120Seconds extends AbstractWaitForSpecifiedSeconds {
    public WaitFor120Seconds() {
    }

    protected long getExpectedWaitSeconds(Environment env) {
        return 120L;
    }
}
