package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractSetEndorsementPolicyIDtoSecondOne extends AbstractCondition {
    @Override
    @PreEnvironment(required = {"resource_request_entity"}, strings = {"secondPolicyId", "policyId"})
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        data.addProperty(getPolicyFieldName(), env.getString("secondPolicyId"));
        logSuccess(getPolicyFieldName() + " changed to second one available", args("data", data));
        return env;
    }

    protected abstract String getPolicyFieldName();
}
