package net.openid.conformance.opin.testmodule;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;

@Deprecated
/**
 * @deprecated
 * This class is deprecated, use {@link AbstractOpinWrongPermissionsTestModuleV2} instead
 */
public abstract class AbstractOpinWrongPermissionsTestModule extends AbstractOpinFunctionalTestModule {

	private boolean isFirstConsent = true;
	protected boolean preFetchResources = false;
	boolean preFetched = false;
	protected String api;
	protected OpinConsentPermissionsBuilder permissionsBuilder;
	protected Class<? extends Condition> rootValidator;
	protected Class<? extends Condition> policyInfoValidator;
	protected Class<? extends Condition> premiumValidator;
	protected Class<? extends Condition> claimValidator;

	public static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();

	protected void preFetchResources() {
		callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PolicyIDSelector.class);

		validate(PrepareUrlForFetchingPolicyInfo.class, policyInfoValidator, String.format("Fetch %s policy info", api));
		validate(PrepareUrlForFetchingPremium.class, premiumValidator, String.format("Fetch %s premium", api));
		validate(PrepareUrlForFetchingClaim.class, claimValidator, String.format("Fetch %s claim", api));
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
	}

	protected void prepareIncorrectPermissions() {

		String productType = env.getString("config", "consent.productType");

		if(!Strings.isNullOrEmpty(productType) && productType.equals("business")) {
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS).build();
		}else if(!Strings.isNullOrEmpty(productType) && productType.equals("personal")){
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL).build();
		}

	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		this.callAndStopOnFailure(RememberOriginalScopes.class);
		this.prepareCorrectConsents();
	}

	@Override
	protected void requestProtectedResource() {
		if (!this.preFetchResources) {
			this.preFetchResources = true;
			super.requestProtectedResource();
			this.eventLog.startBlock(this.currentClientString() + "Validate response");
			this.preFetchResources();
			this.callAndStopOnFailure(ResetScopesToConfigured.class);
			this.prepareIncorrectPermissions();
			this.performAuthorizationFlow();
			this.eventLog.endBlock();
		}

	}
	@Override
	protected void onPostAuthorizationFlowComplete() {
		if (!this.preFetched) {
			this.preFetched = true;
		} else {
			this.requestResourcesWithIncorrectPermissions();
			this.fireTestFinished();
		}
	}

	protected void requestResourcesWithIncorrectPermissions() {

		runInBlock("Ensure we cannot call the root API", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingRoot.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the policy-Info API specifying a Policy ID", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingPolicyInfo.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(policyInfoValidator, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the premium API specifying an Policy ID", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingPremium.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(premiumValidator, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the claim API specifying an Policy ID", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingClaim.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(claimValidator, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});
	}

	protected void setApi(String api) {
		this.api = api;
		env.putString("api", api);
		env.putString("api_type", API_TYPE);
	}

	protected void validate(Class<? extends Condition> prepareUrlCondition, Class<? extends Condition> validator, String logMsg) {
		callAndStopOnFailure(prepareUrlCondition);
		preCallProtectedResource(logMsg);
		callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
	}

	public void setRootValidator(Class<? extends Condition> rootValidator) {
		this.rootValidator = rootValidator;
	}
	public void setPolicyInfoValidator(Class<? extends Condition> policyInfoValidator) {
		this.policyInfoValidator = policyInfoValidator;
	}
	public void setPremiumValidator(Class<? extends Condition> premiumValidator) {
		this.premiumValidator = premiumValidator;
	}
	public void setClaimValidator(Class<? extends Condition> claimValidator) {
		this.claimValidator = claimValidator;
	}


	protected abstract void prepareCorrectConsents();

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndContinueOnFailure(SaveConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void validateResponse() {

	}

}
