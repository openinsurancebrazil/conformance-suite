package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import com.google.common.base.Strings;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateRequestedPermissionsAreNotWidened;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.opin.testmodule.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;


public abstract class AbstractOpinConsentsApiPermissionGroupsTestModule extends OpinAbstractClientCredentialsGrantFunctionalTestModule {

	protected abstract AbstractJsonAssertingCondition postValidator();
	private OpinConsentPermissionsBuilder permissionsBuilder;
	private boolean passed = false;

	@Override
	protected void runTests() {

		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		passed = false;

		String productType = env.getString("config", "consent.productType");
		if (!Strings.isNullOrEmpty(productType) && productType.equals("business")) {
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS).build();
			validatePermissions(PermissionsGroup.CUSTOMERS_BUSINESS);
		}
		if (!Strings.isNullOrEmpty(productType) && productType.equals("personal")) {
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL).build();
			validatePermissions(PermissionsGroup.CUSTOMERS_PERSONAL);
		}

		for (PermissionsGroup permissionsGroup : PermissionsGroup.values()) {
			if (shouldSkipPermissionsGroup(permissionsGroup)) {
				continue;
			}
			permissionsBuilder.resetPermissions().addPermissionsGroup(permissionsGroup).build();
			validatePermissions(permissionsGroup);
		}

		//If all validates returned a 422
		if (!passed) {
			throw new TestFailureException(getId(), "All resources returned a 422 when at least one set of permissions should have passed");
		}
	}

	protected boolean shouldSkipPermissionsGroup(PermissionsGroup permissionsGroup) {
		return permissionsGroup.equals(PermissionsGroup.CUSTOMERS_BUSINESS) ||
				permissionsGroup.equals(PermissionsGroup.CUSTOMERS_PERSONAL) ||
				permissionsGroup.equals(PermissionsGroup.RESOURCES) ||
				permissionsGroup.equals(PermissionsGroup.CLAIM_NOTIFICATION_REQUEST) ||
				permissionsGroup.equals(PermissionsGroup.ENDORSEMENT_REQUEST_CREATE) ||
				permissionsGroup.equals(PermissionsGroup.CAPITALIZATION_TITLE_WITHDRAWAL) ||
				permissionsGroup.equals(PermissionsGroup.PENSION_WITHDRAWAL) ||
				permissionsGroup.equals(PermissionsGroup.CONTRACT_LIFE_PENSION) ||
				permissionsGroup.equals(PermissionsGroup.PENSION_WITHDRAWAL_LEAD) ||
				permissionsGroup.name().startsWith("QUOTE_") ||
				permissionsGroup.name().startsWith(PermissionsGroup.ALL.name());
	}

	private void validatePermissions(PermissionsGroup permissionsGroup) {
		String logMessage = String.format("Validate consent api request for '%s' permission group(s)", permissionsGroup.name());
		runInBlock(logMessage, () -> {

			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(SetContentTypeApplicationJson.class);
			callAndStopOnFailure(FAPIBrazilOpenInsuranceCreateConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
			callAndStopOnFailure(CreateIdempotencyKey.class);
			callAndStopOnFailure(AddIdempotencyKeyHeader.class);

			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.SUCCESS);
			call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));

			String body = env.getString("resource_endpoint_response_full", "body");
			if(Strings.isNullOrEmpty(body)) {
				throw new TestFailureException(getId(),"Empty body from endpoint");
			}

			//if resource_endpoint_response isn't empty we also need to check if error 422 wasn't given for other reason
			if (!body.equals("{}") && env.getInteger("resource_endpoint_response_full", "status") != HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				passed = true;
				callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(postValidator().getClass(), Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(ValidateRequestedPermissionsAreNotWidened.class, Condition.ConditionResult.FAILURE);
			} else {
				callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(postValidator().getClass(), Condition.ConditionResult.FAILURE);
			}
		});
	}
}
