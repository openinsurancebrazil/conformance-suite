package net.openid.conformance.opin.testmodule.v1.endorsement.housing.v1n3;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoFirstOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.AbstractSetEndorsementPolicyIDtoSecondOne;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoFirstOneDiff;
import net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions.SetEndorsementPolicyIDtoSecondOneDiff;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1n3.PostEndorsementOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.endorsement.AbstractEndorsementApiWrongPolicyIDTestModule;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n.AbstractOpinEndorsementApiWrongPolicyIDTestV1n;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-endorsement-housing-api-wrong-policyID-test-v1n3",
        displayName = "For this test, we want to ensure that a endorsement can only be created if the specific policy id was endorsed.",
        summary = "\u2022 Call the POST Consents API with HOUSING Phase 2 permissions\n" +
                "\u2022 Expects 201 - Validate Response \n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "\u2022 Call the resources API\n" +
                "\u2022 Validate there are at least two policy IDS available - (1) and (2)\n" +
                "\u2022 Extract a shared policy ID on the resources APIs\n" +
                "\u2022 Call the POST Consents Endpoint with the first policy ID available (1) \n" +
                "\u2022 Expects 201 - Validate Response\n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for EXCLUSAO, set policy ID for the second extracted policyID (2) \n" +
                "\u2022 Expects 422 - Validate Error response\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"CONSUMED”\n" +
                "\u2022 Call the POST Consents Endpoint with the first policy ID available (1) \n" +
                "\u2022 Expects 201 - Validate Response\n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for EXCLUSAO, set policy ID for the first  extracted policyID (1) \n" +
                "\u2022 Expects 201 - Validate Response\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"CONSUMED\"",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinEndorsementHousingApiWrongPolicyIDTestModuleV1n3 extends AbstractOpinEndorsementApiWrongPolicyIDTestV1n {

        @Override
        protected void setupPermissions() {
                OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
                permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_HOUSING).build();
        }
}






