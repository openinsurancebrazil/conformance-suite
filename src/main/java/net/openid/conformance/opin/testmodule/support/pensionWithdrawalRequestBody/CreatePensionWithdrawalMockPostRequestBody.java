package net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.AbstractOpinPhase3PensionWithdrawTestModule;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.AbstractCreateCapitalizationTitleWithdrawalRequestBody;
import net.openid.conformance.testmodule.Environment;

public class CreatePensionWithdrawalMockPostRequestBody extends AbstractCreatePensionWithdrawalRequestBody {

    @Override
    @PostEnvironment(required = "resource_request_entity", strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject requestData = new JsonObject();
        JsonObject data = new JsonObject();

        JsonObject pmbacAmount = new JsonObject();
        pmbacAmount.addProperty("amount","9999.00");
        JsonObject unit = new JsonObject();
        unit.addProperty("code","R$");
        unit.addProperty("description","BRL");
        pmbacAmount.add("unit",unit);

        JsonObject generalInformation = new JsonObject();
        generalInformation.addProperty("productName", "string");
        generalInformation.addProperty("certificateId","string2");

        JsonObject withdrawalInformation = new JsonObject();
        withdrawalInformation.addProperty("withdrawalReason","5_INSATISFACAO_COM_O_PRODUTO");
        withdrawalInformation.addProperty("withdrawalType", "1_TOTAL");
        withdrawalInformation.add("pmbacAmount",pmbacAmount);

        data.add("generalInfo", generalInformation);
        data.add("withdrawalInfo", withdrawalInformation);

        editData(env, data);

        requestData.add("data", data);
        logSuccess("The mock request body was created", args("body", requestData));
        env.putObject("resource_request_entity", requestData);
        env.putString("resource_request_entity", requestData.toString());
        return env;
    }

    @Override
    protected void editData(Environment env, JsonObject data) {}
}
