package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;

public class ExtractDynamicFields extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(strings = "dynamic_field_api_type")
    @PostEnvironment(required = "dynamic_field")
    public Environment evaluate(Environment env) {
        JsonObject responseBody;
        try {
            responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }

        String dynamicFieldApiType = env.getString("dynamic_field_api_type");
        JsonArray dynamicFields = new JsonArray();
        JsonArray dataArray = responseBody.get("data").getAsJsonArray();
        for(JsonElement dataElement : dataArray) {
            JsonObject data = dataElement.getAsJsonObject();
            if(dynamicFieldApiType.equals(data.get("api").getAsString())) {
                dynamicFields.addAll(getElementsUsingJsonPath(data, "$.branch[*].fields[*]"));
            }
        }

        if(dynamicFields.isEmpty()) {
            logFailure("The response doesn't contain any dynamic field matching the required type", args(
                    "api", dynamicFieldApiType,
                    "data", dataArray
            ));
            return env;
        }

        logSuccess("Found dynamic fields", args(
                "dynamic_fields", dynamicFields
        ));
        JsonObject dynamicField = new JsonObject();
        dynamicField.add("dynamic_fields", dynamicFields);
        env.putObject("dynamic_field", dynamicField);

        return env;
    }

    private JsonArray getElementsUsingJsonPath(JsonElement base, String jsonPath) {
        try {
            return JsonPath.read(base, jsonPath);
        } catch (PathNotFoundException e) {
            throw error("The json object does not correspond to the json path", args(
                    "jsonObject", base,
                    "jsonPath", jsonPath
            ));
        }
    }
}
