package net.openid.conformance.opin.testmodule.pcm.consent.funnel;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.pcm.AbstractPcmTestModule;
import net.openid.conformance.opin.validator.PCM.consent.funnel.PcmConsentFunnelPostClientEventPayloadValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "pcm-consent-funnel_api_client-event_test-module",
        displayName = "Validate that the institution correctly sends a Single request for a Server report",
        summary = "For the Consent Funnel PCM API, Validate that the institution correctly sends a Single request for a Client report. The Endpoint used on the test depends on the <alias> provided on the test configuration\n" +
                "\u2022 The test will wait at most 100 seconds for a request to be sent against the following endpoint: https://web.conformance.directory.opinbrasil.com.br/test/a/{alias}/report-api/v1/server-event\n" +
                "\u2022 Expect an incoming request - Validate (1) That the JWT was signed by the organisation provided on the clientOrgId field, (2) That the fields clientSSId and serverASId are registered at the Sandbox Directory, (3) That all received fields are valid, according to the yaml rules. \n" +
                "\u2022 Return 200 - Response will be on ACCEPTED status and contain a random dummy value of reportId and correlationId",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class PcmConsentFunnelApiClientEventTestModule extends AbstractPcmTestModule {


    @Override
    protected boolean isClientEndpoint() {
        return true;
    }

    @Override
    protected Class<? extends Condition> getRequestValidator() {
        return PcmConsentFunnelPostClientEventPayloadValidatorV1.class;
    }

    @Override
    protected String getExpectedPath() {
        return "report-api/v1/client-event";
    }





}
