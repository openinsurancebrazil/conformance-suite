package net.openid.conformance.opin.testmodule.support.quotePatrimonialStructuralBuildResourceUrl;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddQuoteStatusToEndpoint extends AbstractCondition {

    @Override
    @PreEnvironment(strings = {"protected_resource_url", "consent_id"})
    public Environment evaluate(Environment env) {
        String consentId = env.getString("consent_id");
        String oldUrl = env.getString("protected_resource_url");
        String regex = "^(https://)(.*?)(/open-insurance)/(.+)/request$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(oldUrl);
        if (!matcher.find()) {
            throw error("The old url does not have the correct format.",
                args("old url", oldUrl, "expected format", regex));
        }

        String newUrl = String.format("%s/%s/quote-status", oldUrl, consentId);
        env.putString("protected_resource_url", newUrl);
        env.putString("config", "resource.resourceUrl", newUrl);

        logSuccess(String.format("protected_resource_url and resource.resourceUrl set to %s", newUrl), args("endpoint", newUrl));
        return env;
    }
}
