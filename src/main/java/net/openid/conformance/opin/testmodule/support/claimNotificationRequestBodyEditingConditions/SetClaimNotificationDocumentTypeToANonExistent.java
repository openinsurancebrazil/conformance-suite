package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class SetClaimNotificationDocumentTypeToANonExistent extends AbstractCondition {
    @Override
    @PreEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();

        data.addProperty("documentType", "INEXISTENTE");

        logSuccess("documentType changed to INEXISTENTE", args("data", data));
        return env;
    }
}
