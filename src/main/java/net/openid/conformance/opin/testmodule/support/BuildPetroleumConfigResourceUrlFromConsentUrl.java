package net.openid.conformance.opin.testmodule.support;

public class BuildPetroleumConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "insurance-petroleum";
	}

	@Override
	protected String getEndpointReplacement() {
		return "insurance-petroleum";
	}
}
