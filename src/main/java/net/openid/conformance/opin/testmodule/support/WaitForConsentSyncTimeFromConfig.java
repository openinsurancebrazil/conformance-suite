package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.concurrent.TimeUnit;

public class WaitForConsentSyncTimeFromConfig extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {

        String expectedWaitSeconds= env.getString("config", "resource.consentSyncTime");
        long waitTime = parseWaitTime(expectedWaitSeconds);

        env.putString("waitTime", String.valueOf(waitTime));
        pauseExecution(waitTime);
        return env;
    }

    private long parseWaitTime(String expectedWaitSeconds) {
        if(expectedWaitSeconds == null) {
            log("No wait time specified in config, defaulting to 0 seconds");
            return 0;
        }

        try {
            long waitTime = Long.parseLong(expectedWaitSeconds);
            if (waitTime < 0 || waitTime > 300) {
                logSuccess("Invalid wait time specified in config, defaulting to 300 seconds");
                return 300;
            }
            return waitTime;
        } catch (NumberFormatException e) {
            throw error("Invalid wait time specified in config, it must be a number");
        }
    }

    protected void pauseExecution(long waitTime) {
        try {
            logSuccess("Pausing for " + waitTime + " seconds");
            TimeUnit.SECONDS.sleep(waitTime);
            logSuccess("Woke up after " + waitTime + " seconds sleep");
        } catch (InterruptedException e) {
            throw error("Interrupted while sleeping", e);
        }
    }
}
