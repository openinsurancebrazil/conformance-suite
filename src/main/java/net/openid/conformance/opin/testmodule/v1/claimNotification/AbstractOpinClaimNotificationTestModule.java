package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.opin.testmodule.AbstractOpinPhase3TestModule;
import net.openid.conformance.opin.testmodule.phase3.ClearIdempotencyKeyHeader;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.oasValidators.resources.v2n4.GetResourcesOASValidatorV2n4;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinClaimNotificationTestModule extends AbstractOpinPhase3TestModule {

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                        condition(AddClaimNotificationToConsentData.class).skipIfStringMissing("policyId"));
    }

    protected abstract AbstractAddEndpointTypeToEnvironment addEndpointType();

    @Override
    protected void validateGetResourcesResponse() {
        runInBlock("Validating resources response", () -> {
            callAndContinueOnFailure(GetResourcesOASValidatorV2n4.class, Condition.ConditionResult.FAILURE);

            callAndStopOnFailure(addEndpointType().getClass());
            callAndStopOnFailure(ClaimNotificationSelectResourcePolicyId.class);

            callAndContinueOnFailure(TranslatePolicyTypeToApi.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(PrepareUrlForFetchingPolicyInfo.class);
        });

        preCallProtectedResource("Calling the correspondent API policy-info endpoint");

        callAndStopOnFailure(ExtractDocumentTypeFromPolicyInfoResponse.class);
    }

    @Override
    protected void setupSecondFlow() {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.CLAIM_NOTIFICATION_REQUEST);
        if("damage".equals(env.getString("claim_notification_endpoint_type"))) {
            permissionsBuilder.removePermission("CLAIM_NOTIFICATION_REQUEST_PERSON_CREATE");
        } else {
            permissionsBuilder.removePermission("CLAIM_NOTIFICATION_REQUEST_DAMAGE_CREATE");
        }

        permissionsBuilder.build();
        callAndStopOnFailure(SetScope.class);
        callAndStopOnFailure(AddClaimNotificationScope.class);
    }

    @Override
    protected void validateLinks(String responseFull) {
        call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
        env.mapKey("resource_endpoint_response_full", responseFull);

        ConditionSequence sequence = new ValidateSelfEndpoint();

        if (responseFull.contains("consent")) {
            sequence.insertAfter(ClearContentTypeHeaderForResourceEndpointRequest.class , condition(ClearIdempotencyKeyHeader.class));
            sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
        }

        call(sequence);
    }
}
