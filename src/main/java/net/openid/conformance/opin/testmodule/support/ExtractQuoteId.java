package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class ExtractQuoteId extends AbstractCondition {
    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";
    @Override
    @PostEnvironment(strings = "quote_id")
    public Environment evaluate(Environment env) {
        JsonObject responseBody = null;
        try {
            responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }

        JsonElement quoteId = JsonPath.read(responseBody, "$.data.quoteInfo.quotes[0].insurerQuoteId");
        env.putString("quote_id", OIDFJSON.getString(quoteId));
        logSuccess("A quote ID was extracted", args(
                "quote_id",
                env.getString("quote_id")
        ));
        return env;
    }
}
