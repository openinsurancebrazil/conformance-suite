package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public class CreateEndorsementRequestBodyInclusaoDiff extends AbstractEndorsementRequestBodyDiff {

    @Override
    protected EndorsementType endorsementType() {
        return EndorsementType.INCLUSAO;
    }
}
