package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class IdSelectorFromJsonPath extends AbstractIdSelector {

    @Override
    @PreEnvironment(strings = "selected_id_json_path")
    protected String getId(Environment env) {
        String jsonPath = env.getString("selected_id_json_path");
        JsonObject body = bodyFrom(env, "resource_endpoint_response_full").getAsJsonObject();

        JsonElement selectedId = getElementUsingJsonPath(body, jsonPath);

        return OIDFJSON.getString(selectedId);
    }

    private JsonElement getElementUsingJsonPath(JsonElement base, String jsonPath) {
        try {
            return JsonPath.read(base, jsonPath);
        } catch (PathNotFoundException e) {
            throw error("The json object does not correspond to the json path", args(
                    "jsonObject", base,
                    "jsonPath", jsonPath
            ));
        }
    }
}
