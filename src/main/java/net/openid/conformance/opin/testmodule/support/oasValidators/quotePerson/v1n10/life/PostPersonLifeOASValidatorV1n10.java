package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.life;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractPostPersonLifeOASValidator;

@ApiName("Quote Person 1.10.2")
public class PostPersonLifeOASValidatorV1n10 extends AbstractPostPersonLifeOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.10.2.yaml";
    }

}