package net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.damageAndPerson.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Get DynamicFields DamageAndPerson V1.4.0")
public class GetDynamicFieldsDamageAndPersonOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/dynamicFields/v1n4/swagger-dynamic-fields-1.4.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/damage-and-person";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
