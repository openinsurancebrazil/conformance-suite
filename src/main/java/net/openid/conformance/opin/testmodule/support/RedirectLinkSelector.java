package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class RedirectLinkSelector extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		{
			JsonObject body = bodyFrom(env).getAsJsonObject();
			JsonObject data = Optional.ofNullable(body.getAsJsonObject("data"))
					.orElseThrow(() -> error("Could not find data in the body", args("body", body)));
			if (data.isEmpty()) {
				throw error("Data object cannot be empty to extract redirectLink");
			}

			String protectedUrl = Optional.ofNullable(data.get("redirectLink"))
					.map(OIDFJSON::getString)
					.orElseThrow(() -> error("Could not extract redirectLink from the data", args("data", data)));
			if(protectedUrl.equals("")){
				throw error("RedirectLink cannot be empty");
			}

			env.putString("protected_resource_url", protectedUrl);
			logSuccess("protected_resource_url for redirectLink set up", args("protected_resource_url", protectedUrl));
			return env;
		}
	}

	private JsonElement bodyFrom(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not find response body in the environment"));
		} catch (ParseException e) {
			throw error("Could not parse response body");
		}
	}

}

