package net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractPostClaimNotificationPersonOASValidator extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getEndpointPath() {
        return "/request/person/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }

    @Override
    protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
        JsonObject data = body.getAsJsonObject("data");
        assertRequestorInformation(data);
    }
    protected void assertRequestorInformation(JsonObject data) {
        assertField1IsRequiredWhenField2HasValue2(
                data,
                "requestor",
                "requestorIsInsured",
                false
        );
    }
}
