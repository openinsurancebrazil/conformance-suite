package net.openid.conformance.opin.testmodule.v1.pensionPlan.abstracts;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;

import java.util.Map;

public abstract class AbstractPensionPlanApiGranularPermissionsTestModule extends AbstractPensionPlanAPICoreTestModule {

    @Override
    protected String[] getIndividualPermissionsToBeRemoved() {
        return new String[]{"PENSION_PLAN_MOVEMENTS_READ", "PENSION_PLAN_PORTABILITIES_READ", "PENSION_PLAN_WITHDRAWALS_READ", "PENSION_PLAN_CLAIM"};
    }

    @Override
    protected Class<? extends Condition> getExpectedResponseCode(String endpoint) {
        Map<String, Class<? extends Condition>> endpointToStatusCodeMap = Map.of(
                "contract-info", EnsureResourceResponseCodeWas200.class,
                "movements", EnsureResourceResponseCodeWas403.class,
                "portabilities", EnsureResourceResponseCodeWas403.class,
                "withdrawals", EnsureResourceResponseCodeWas403.class,
                "claim", EnsureResourceResponseCodeWas403.class
        );
        return endpointToStatusCodeMap.get(endpoint);
    }

    @Override
    protected boolean checkConsentAuthorizedStatusAfterRedirect() {
        return true;
    }
}
