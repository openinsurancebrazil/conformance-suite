package net.openid.conformance.opin.testmodule.support.oasValidators.rural.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Rural 1.3.0")
public class GetRuralOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/rural/v1n3/swagger-rural-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/rural";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}