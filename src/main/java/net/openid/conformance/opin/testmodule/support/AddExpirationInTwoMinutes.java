package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class AddExpirationInTwoMinutes extends AbstractCondition {
    public AddExpirationInTwoMinutes() {
    }

    @PreEnvironment(
            required = {"consent_endpoint_request"}
    )
    public Environment evaluate(Environment env) {
        Instant expiryTime = Instant.now().plus(2L, ChronoUnit.MINUTES);
        Instant expiryTimeNoFractionalSeconds = expiryTime.truncatedTo(ChronoUnit.SECONDS);
        String rfc3339ExpiryTime = DateTimeFormatter.ISO_INSTANT.format(expiryTimeNoFractionalSeconds);
        JsonObject consentRequest = env.getObject("consent_endpoint_request");
        JsonObject data = consentRequest.getAsJsonObject("data");
        data.addProperty("expirationDateTime", rfc3339ExpiryTime);
        this.logSuccess("Set expiry date to be in two minutes", this.args(new Object[]{"expiry", rfc3339ExpiryTime}));
        return env;
    }
}