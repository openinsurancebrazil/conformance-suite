package net.openid.conformance.opin.testmodule.support;

public class BuildOpinBusinessCustomersConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "customers";
	}

	@Override
	protected String getEndpointReplacement() {
		return "business/identifications";
	}
}
