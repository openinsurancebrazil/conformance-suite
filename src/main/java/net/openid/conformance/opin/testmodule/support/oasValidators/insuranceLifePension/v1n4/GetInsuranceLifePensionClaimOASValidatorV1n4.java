package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Life Pensions 1.4.0")
public class GetInsuranceLifePensionClaimOASValidatorV1n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceLifePension/v1/swagger-insurance-life-pension.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-life-pension/{certificateId}/claim";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}