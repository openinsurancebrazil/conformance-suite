package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.business;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Quote Patrimonial 1.10.0")
public class GetPatrimonialBusinessQuoteStatusOASValidatorV1n10 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePatrimonial/v1n10/quote-patrimonial-1.10.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/business/request/{consentId}/quote-status";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
