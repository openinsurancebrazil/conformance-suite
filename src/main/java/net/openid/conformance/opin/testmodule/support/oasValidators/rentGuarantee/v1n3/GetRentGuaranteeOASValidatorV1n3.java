package net.openid.conformance.opin.testmodule.support.oasValidators.rentGuarantee.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Rent Guarantee 1.3.0")
public class GetRentGuaranteeOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/rentGuarantee/v1n3/swagger-rent-guarantee-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/rent-guarantee";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}