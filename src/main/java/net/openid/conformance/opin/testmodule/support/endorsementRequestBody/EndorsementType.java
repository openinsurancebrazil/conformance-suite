package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

public enum EndorsementType {

    ALTERACAO,
    CANCELAMENTO,
    INCLUSAO,
    EXCLUSAO

}
