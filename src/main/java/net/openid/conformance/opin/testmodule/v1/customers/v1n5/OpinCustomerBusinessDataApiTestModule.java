package net.openid.conformance.opin.testmodule.v1.customers.v1n5;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessIdentificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessQualificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.v1.customers.AbstractOpinCustomerBusinessDataApiTest;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-customer-business-data-api-test",
	displayName = "Validate structure of all business customer data API resources V1",
	summary = "Validates the structure of all business customer data API resources V1\n" +
		"\u2022 Creates a Consent with the customer business permissions (\"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\",\"CUSTOMERS_BUSINESS_ADDITIONALINFO_READ\",\"RESOURCES_READ\")\n" +
		"\u2022 Expects a success 201 - Validate Response\n" +
		"\u2022 Calls GET Business Identifications Endpoint\n" +
		"\u2022 Expects a success 200 - Validate Response\n" +
		"\u2022 Calls GET Business Qualifications Endpoint\n" +
		"\u2022 Expects a success 200 - Validate Response\n" +
		"\u2022 Calls GET Business Complimentary-Information Endpoint\n" +
		"\u2022 Expects a success 200 - Validate Response\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks","consent.productType"
})
public class OpinCustomerBusinessDataApiTestModule extends AbstractOpinCustomerBusinessDataApiTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessIdentificationValidator(){
		return GetCustomersBusinessIdentificationListOASValidatorV1n5.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessQualificationValidator(){
		return GetCustomersBusinessQualificationListOASValidatorV1n5.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessComplimentaryInfoValidator(){
		return GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5.class;
	}
}
