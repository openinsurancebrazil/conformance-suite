package net.openid.conformance.opin.testmodule.phase3.quoteAuto;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteAutoLeadApiCoreTestModule extends AbstractOpinPhase3QuoteLeadTestModule{

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_AUTO_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-auto";
    }

    @Override
    protected String getApiName() {
        return "quote-auto";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}