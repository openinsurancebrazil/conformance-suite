package net.openid.conformance.opin.testmodule.v1.financialAssistance;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractFinancialAssistanceApiResourcesTest extends AbstractOpinApiResourcesTestModuleV2 {
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddFinancialAssistanceScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceFinancialAssistanceUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.FINANCIAL_ASSISTANCE;
    }

    @Override
    protected String getApi() {
        return "insurance-financial-assistance";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[*].brand.companies[*].contracts[*].contractId");
        return AllIdsSelectorFromJsonPath.class;
    }

    @Override
    protected String getResourceType() {
        return EnumOpinResourcesType.FINANCIAL_ASSISTANCE.name();
    }

    @Override
    protected String getResourceStatus() {
        return EnumResourcesStatus.AVAILABLE.name();
    }
}
