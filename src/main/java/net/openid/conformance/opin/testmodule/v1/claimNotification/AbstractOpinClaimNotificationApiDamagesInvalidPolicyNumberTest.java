package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AddDamageEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions.SetClaimNotificationIdFieldToInvalidValue;

public abstract class AbstractOpinClaimNotificationApiDamagesInvalidPolicyNumberTest extends AbstractClaimNotificationsNegativeTest {

    @Override
    protected void editClaimNotificationRequestBody() {
        callAndStopOnFailure(SetClaimNotificationIdFieldToInvalidValue.class);
    }

    @Override
    protected AbstractAddEndpointTypeToEnvironment addEndpointType() {
        return new AddDamageEndpointTypeToEnvironment();
    }
}
