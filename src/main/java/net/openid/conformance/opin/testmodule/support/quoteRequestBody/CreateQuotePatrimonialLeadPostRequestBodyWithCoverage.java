package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuotePatrimonialLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "RESIDENCIAL_IMOVEL_BASICA";
    }
}
