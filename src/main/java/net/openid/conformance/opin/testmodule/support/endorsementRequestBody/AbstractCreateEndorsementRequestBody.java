package net.openid.conformance.opin.testmodule.support.endorsementRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.OPINPhase3Keys;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public abstract class AbstractCreateEndorsementRequestBody extends AbstractCondition {

    @Override
    @PreEnvironment(strings = "policyId")
    @PostEnvironment(required = "resource_request_entity", strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        String policyId = env.getString("policyId");
        String date = LocalDateTime.now(ZoneOffset.UTC)
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        JsonObject requestData = new JsonObject();
        JsonObject data = new JsonObject();
        data.addProperty(getPolicyFieldName(), policyId);
        data.addProperty("endorsementType", endorsementType().name());
        data.addProperty("requestDescription", OPINPhase3Keys.Keys_requestDescription);
        data.addProperty("requestDate", date);

        JsonArray insuredObjectIds = getInsuredObjectId();
        if (insuredObjectIds != null) {
            data.add("insuredObjectId", insuredObjectIds);
        }

        requestData.add("data", data);

        env.putObject("resource_request_entity", requestData);
        env.putString("resource_request_entity", requestData.toString());

        return env;
    }

    protected abstract EndorsementType endorsementType();

    protected String getPolicyFieldName(){
        return "policyNumber";
    }

    protected JsonArray getInsuredObjectId() {
        return null;
    }
}
