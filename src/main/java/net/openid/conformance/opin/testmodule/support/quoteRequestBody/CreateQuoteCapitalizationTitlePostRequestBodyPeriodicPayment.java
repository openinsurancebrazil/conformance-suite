package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteCapitalizationTitlePostRequestBodyPeriodicPayment extends AbstractCreateQuoteCapitalizationTitlePostRequestBody {

    @Override
    protected String getPaymentType() {
        return "PERIODICO";
    }

    @Override
    protected String getAmount() {
        return "1000.00";
    }

    @Override
    protected boolean isMonthlyPayment() {
        return false;
    }

    @Override
    protected boolean isSinglePayment() {
        return false;
    }

    @Override
    protected boolean isUnitTypePresent() {
        return false;
    }
}
