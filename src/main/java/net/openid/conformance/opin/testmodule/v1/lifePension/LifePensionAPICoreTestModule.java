package net.openid.conformance.opin.testmodule.v1.lifePension;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceLifePension.v1n4.*;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "life-pension_api_core_test-module_v1",
        displayName = "Validates the structure of all Life Pension API",
        summary ="Validates the structure of all Life Pension API \n" +
                "\u2022 Call the POST consents with all the permissions needed to access the Capitalization Title API (“LIFE_PENSION_READ”, “LIFE_PENSION_CONTRACTINFO_READ”, “LIFE_PENSION_MOVEMENTS_READ”, “LIFE_PENSION_PORTABILITIES_READ”,“LIFE_PENSION_WITHDRAWALS_READ”,“LIFE_PENSION_CLAIM”,“RESOURCES_READ”)\n" +
                "\u2022 Expects 201 -  Validate Response \n" +
                "\u2022 Redirect the user to authorize consent \n" +
                "\u2022 Calls GET Life Pension contracts Endpoint \n" +
                "\u2022 Expects 200 - Fetches one of the certificate IDs returned \n" +
                "\u2022 Calls GET Life Pension {certificateId} contract-info Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} movements Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} portabilities Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} withdrawals Endpoint \n" +
                "\u2022 Expects 200 - Validate Response \n" +
                "\u2022 Calls GET Life Pension {certificateId} claim Endpoint \n" +
                "\u2022 Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class LifePensionAPICoreTestModule extends AbstractLifePensionAPICoreTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceLifePensionContractsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator() {
        return GetInsuranceLifePensionContractInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getMovementsValidator() {
        return GetInsuranceLifePensionMovementsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator() {
        return GetInsuranceLifePensionPortabilitiesOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator() {
        return GetInsuranceLifePensionWithdrawalsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsuranceLifePensionClaimOASValidatorV1n4.class;
    }
}
