package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.lead;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractPatchPersonLeadOASValidator;

@ApiName("Quote Person 1.11.0")
public class PatchPersonLeadOASValidatorV1n11 extends AbstractPatchPersonLeadOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.11.0.yaml";
    }
}
