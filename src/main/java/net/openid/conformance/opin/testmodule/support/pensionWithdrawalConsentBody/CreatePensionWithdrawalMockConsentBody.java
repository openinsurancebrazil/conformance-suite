package net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.AbstractOpinPhase3PensionWithdrawTestModule;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.AbstractCreateCapitalizationTitleWithdrawalConsentBody;
import net.openid.conformance.testmodule.Environment;

public class CreatePensionWithdrawalMockConsentBody extends AbstractCreatePensionWithdrawalConsentBody {

    @Override
    public Environment evaluate(Environment env) {
        JsonObject pensionWithdrawData = new JsonObject();
        JsonObject consentData = env.getElementFromObject("consent_endpoint_request", "data").getAsJsonObject();
        JsonObject pmbacAmount = new JsonObject();
        pmbacAmount.addProperty("amount","9999.00");
        JsonObject unit = new JsonObject();
        unit.addProperty("code","R$");
        unit.addProperty("description","BRL");
        pmbacAmount.add("unit",unit);

        pensionWithdrawData.addProperty("productName", "string");
        pensionWithdrawData.addProperty("certificateId", "string2");
        pensionWithdrawData.addProperty("withdrawalType", "1_TOTAL");
        pensionWithdrawData.addProperty("withdrawalReason","5_INSATISFACAO_COM_O_PRODUTO");
        pensionWithdrawData.add("pmbacAmount",pmbacAmount);
        env.putObject("pmbacAmount_1", pmbacAmount);

        editData(env, pensionWithdrawData);

        consentData.add("withdrawalLifePensionInformation", pensionWithdrawData);
        logSuccess("CapitalizationTitleWithdrawal mock consent body created and added",args("consent_endpoint_request", consentData));
        return env;
    }
    @Override
    protected void editData(Environment env, JsonObject data) {}
}
