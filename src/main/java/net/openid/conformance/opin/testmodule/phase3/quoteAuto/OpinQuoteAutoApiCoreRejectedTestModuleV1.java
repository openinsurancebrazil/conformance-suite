package net.openid.conformance.opin.testmodule.phase3.quoteAuto;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n8.GetQuoteAutoOASValidatorV1n8;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n8.PostQuoteAutoOASValidatorV1n8;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-auto_api_rejected_test-module_v1",
        displayName = "Ensure that a Auto quotation request can be rejected.",
        summary = """
                Ensure that a Auto quotation request can be rejected. This test module will send the quoteData with only the required fields, and expect the quotation to be rejected. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
                · Call POST auto request endpoint sending personal or business information, following what is defined at the config, and sending the quoteData with only the required fields, and setting termStartDate to date after termEndDate
                · Expect 201 - Validate Response and ensure status is RCVD or RJCT
                · Poll the GET quote-status endpoint while status is RCVD or EVAL
                · Call GET quote-status endpoint
                · Expect 200 - Validate Respone and ensure status is RJCT
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteAutoApiCoreRejectedTestModuleV1 extends AbstractOpinQuoteAutoApiCoreRejectedTestModule {

        @Override
        protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
                return PostQuoteAutoOASValidatorV1n8.class;
        }

        @Override
        protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
                return GetQuoteAutoOASValidatorV1n8.class;
        }
}
