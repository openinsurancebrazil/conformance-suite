package net.openid.conformance.opin.testmodule.support;

public class BuildRuralConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

    @Override
    protected String getApiReplacement() {
        return "insurance-rural";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-rural";
    }
}
