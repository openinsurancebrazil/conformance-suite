package net.openid.conformance.opin.testmodule.v1.pensionPlan.v1n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n5.*;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.abstracts.AbstractPensionPlanApiGranularPermissionsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "pension-plan_api_granular-permissions_test-module_v1.5.0",
        displayName = "Ensure specific methods can only be called if the respective permissions is granted",
        summary ="Ensure specific methods can only be called if the respective permissions is granted\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“PENSION_PLAN_READ”, “PENSION_PLAN_CONTRACTINFO_READ”, “PENSION_PLAN_MOVEMENTS_READ”, “PENSION_PLAN_PORTABILITIES_READ”,“PENSION_PLAN_WITHDRAWALS_READ”,“PENSION_PLAN_CLAIM”, “RESOURCES_READ”)\n" +
                "• Expects 201 -  Validate Response \n" +
                "• Redirect the user to authorize consent \n" +
                "• Calls the GET Consents\n" +
                "• Expects 200 - Validate Response and Status is Authorized\n" +
                "• Calls GET Pension Plan contracts Endpoint \n" +
                "• Expects 200 - Fetches one of the pension Identifications returned \n" +
                "• Calls GET Pension Plan {pensionIdentification} contract-info Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Pension Plan {pensionIdentification} movements Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} portabilities Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} withdrawals Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} claim Endpoint \n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class PensionPlanApiGranularPermissionsTestModuleV1n5 extends AbstractPensionPlanApiGranularPermissionsTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePensionPlanContractsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator() {
        return GetInsurancePensionPlanContractInfoOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getMovementsValidator() {
        return GetInsurancePensionPlanMovementsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator() {
        return GetInsurancePensionPlanPortabilitiesOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator() {
        return GetInsurancePensionPlanWithdrawalsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsurancePensionPlanClaimOASValidatorV1n5.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }
}
