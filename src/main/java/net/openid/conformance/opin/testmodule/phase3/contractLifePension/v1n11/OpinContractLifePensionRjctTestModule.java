package net.openid.conformance.opin.testmodule.phase3.contractLifePension.v1n11;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.contractLifePension.AbstractOpinContractLifePensionRjctTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.GetContractLifePensionQuoteStatusOASValidatorV1n12;
import net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.PostContractLifePensionOASValidatorV1n12;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-contract-life-pension_api_rejected_test-module_v1",
        displayName = "Ensure that a  Life Pension contract request can be rejected.",
        summary = """
            Ensure that a  Life Pension contract request can be rejected. This test module will send the quoteData with only the minimum required fields, initialContribution amount set to 1000 and unitType set to PORCENTAGEM, and expect the quotation to be rejected. This test applies to both travelal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, travelal information will be used.
            • Call POST request endpoint sending travelal or business information, following what is defined at the config, initialContribution amount set to 1000 and unitType set to PORCENTAGEM
            • Expect 201 - Validate Response and ensure status is RCVD or RJCT
            • Poll the GET request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Respone and ensure status is RJCT
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinContractLifePensionRjctTestModule extends AbstractOpinContractLifePensionRjctTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostContractLifePensionOASValidatorV1n12.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetContractLifePensionQuoteStatusOASValidatorV1n12.class;
    }
}