package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateCapitalizationTitleWithdrawalConsentBody extends AbstractCreateCapitalizationTitleWithdrawalConsentBody{
    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject pmbacAmount =  data.getAsJsonObject("withdrawalTotalAmount");
        String amount = pmbacAmount.get("amount").getAsString();
        Float floatAmount = Float.parseFloat(amount);
        pmbacAmount.addProperty("amount", String.format("%.2f",floatAmount).replaceAll(",","."));
    }
}
