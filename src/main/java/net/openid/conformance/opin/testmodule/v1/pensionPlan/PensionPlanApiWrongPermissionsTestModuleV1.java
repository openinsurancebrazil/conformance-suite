package net.openid.conformance.opin.testmodule.v1.pensionPlan;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n4.*;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.abstracts.AbstractPensionPlanApiWrongPermissionsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "pension-plan_api_wrong-permissions_test-module_v1",
        displayName = "Ensures API cannot be called with wrong permissions",
        summary = "Ensures API  cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“PENSION_PLAN_READ”, “PENSION_PLAN_CONTRACTINFO_READ”, “PENSION_PLAN_MOVEMENTS_READ”, “PENSION_PLAN_PORTABILITIES_READ”,“PENSION_PLAN_WITHDRAWALS_READ”,“PENSION_PLAN_CLAIM”, “RESOURCES_READ”)\n" +
                "• Expects 201 -  Validate Response \n" +
                "• Redirect the user to authorize consent \n" +
                "• Calls GET Pension Plan contracts Endpoint \n" +
                "• Expects 200 - Fetches one of the certificate IDs returned \n" +
                "• Calls GET Pension Plan {pensionIdentification} contract-info Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Pension Plan {pensionIdentification} movements Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Pension Plan {pensionIdentification} portabilities Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Pension Plan {pensionIdentification} withdrawals Endpoint \n" +
                "• Expects 200 - Validate Response \n" +
                "• Calls GET Pension Plan {pensionIdentification} claim Endpoint \n" +
                "• Expects 200 - Validate Response\n" +
                "• Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field \n" +
                "• Expects a success 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Pension Plan contracts Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} contract-info Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} movements Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} portabilities Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} withdrawals Endpoint \n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Pension Plan {pensionIdentification} claim Endpoint \n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class PensionPlanApiWrongPermissionsTestModuleV1 extends AbstractPensionPlanApiWrongPermissionsTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePensionPlanContractsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getContractInfoValidator() {
        return GetInsurancePensionPlanContractInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getMovementsValidator() {
        return GetInsurancePensionPlanMovementsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPortabilitiesValidator() {
        return GetInsurancePensionPlanPortabilitiesOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getWithdrawalsValidator() {
        return GetInsurancePensionPlanWithdrawalsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsurancePensionPlanClaimOASValidatorV1n4.class;
    }
}
