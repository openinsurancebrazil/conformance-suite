package net.openid.conformance.opin.testmodule.support.pensionWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreatePensionWithdrawalTotalHalvedConsentBody extends AbstractCreatePensionWithdrawalConsentBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject desiredTotalAmount =  env.getObject("pmbacAmount_1").deepCopy();
        String amount = desiredTotalAmount.get("amount").getAsString();
        Float partialAmount = Float.parseFloat(amount);
        partialAmount = partialAmount/2;
        desiredTotalAmount.addProperty("amount", String.format("%.2f",partialAmount).replaceAll(",","."));
        data.add("desiredTotalAmount", desiredTotalAmount);
    }
}
