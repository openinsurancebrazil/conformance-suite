package net.openid.conformance.opin.testmodule.support;

public class PrepareUrlForQuoteCapitalizationTitleRaffle extends AbstractPrepareUrlForApi {

    @Override
    protected String getApiReplacement() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getEndpointReplacement() {
        return "raffle/request";
    }
}
