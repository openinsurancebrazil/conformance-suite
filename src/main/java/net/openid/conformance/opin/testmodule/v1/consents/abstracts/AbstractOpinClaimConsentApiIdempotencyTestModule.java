package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.opin.testmodule.support.EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject;
import net.openid.conformance.opin.testmodule.support.LoadConsentBodyAsString;
import net.openid.conformance.opin.testmodule.support.OpinConsentEndpointResponseValidatePermissions;
import net.openid.conformance.opin.testmodule.support.consents.EditPostConsentBody;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractOpinClaimConsentApiIdempotencyTestModule extends AbstractOpinConsentsApiConsentStatusTestModule {


    @Override
    protected void performPostAuthorizationFlow() {
        this.requestProtectedResource();
        this.onPostAuthorizationFlowComplete();
    }

    @Override
    protected void stepsAfterPreAuthorizationSteps() {
        // no additional steps
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        OpenBankingBrazilPreAuthorizationSteps steps = new OpenBankingBrazilPreAuthorizationSteps(false,
                false, addTokenEndpointClientAuthentication, false, true,false);
        steps
                .insertAfter(CreateEmptyResourceEndpointRequestHeaders.class, setIdempotencyKey())
                .replace(FAPIBrazilConsentEndpointResponseValidatePermissions.class,condition(OpinConsentEndpointResponseValidatePermissions.class))
                .then(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"),
                        condition(postValidator().getClass()).dontStopOnFailure());
        return steps;
    }

    @Override
    protected void requestProtectedResource() {
        eventLog.startBlock("Call the POST Consents API with the same payload and idempotency id - Expects 201");
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
        callAndStopOnFailure(CallConsentEndpointWithBearerToken.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        callAndStopOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(postValidator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();

        eventLog.startBlock("Call the POST Consents API with a different payload as the previous request but the same idempotency id - Expects 422");
        callAndStopOnFailure(EditPostConsentBody.class);
        callAndStopOnFailure(LoadConsentBodyAsString.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
        callAndStopOnFailure(CallConsentEndpointWithBearerToken.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        env.mapKey("endpoint_response", "consent_endpoint_response_full");
        callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasErroIdempotenciaUsingObject.class, Condition.ConditionResult.FAILURE);
        env.unmapKey("endpoint_response");
        callAndStopOnFailure(postValidator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }

    protected ConditionSequence setIdempotencyKey() {
        return sequenceOf(
                condition(CreateIdempotencyKey.class),
                condition(AddIdempotencyKeyHeader.class)
        );
    }
}
