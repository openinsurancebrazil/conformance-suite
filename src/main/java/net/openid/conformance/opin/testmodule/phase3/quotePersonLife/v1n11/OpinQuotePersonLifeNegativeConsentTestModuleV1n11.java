package net.openid.conformance.opin.testmodule.phase3.quotePersonLife.v1n11;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonLife.AbstractOpinQuotePersonLifeNegativeConsentTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-life_api_negative-consent_test-module_v1.11.0",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = """
            Ensure a consent cannot be created in unhappy requests.
            • Call POST Consents with permission for Customer Data, PF or PJ, and Person Lead.
            • Expect 422 - Validate Error Response
            • Call POST Consents with permission for Person Lead and the permission for this specified Person Branch.
            • Expect 422 - Validate Error Response.
            • Call POST Consents with permission for Person Lead.
            • Expect 422 - Validate Error Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonLifeNegativeConsentTestModuleV1n11 extends AbstractOpinQuotePersonLifeNegativeConsentTestModule {

    @Override
    protected void postConsentValidator() {
        callAndStopOnFailure(PostConsentsOASValidatorV2n7.class);
    }
}