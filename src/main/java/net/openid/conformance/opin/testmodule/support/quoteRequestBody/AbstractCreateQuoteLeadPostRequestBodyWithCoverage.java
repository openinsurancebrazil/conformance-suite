package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateQuoteLeadPostRequestBodyWithCoverage extends AbstractCreateQuotePostRequestBody {

    protected abstract String getCode();

    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject quoteData = new JsonObject();

        JsonArray coverages = new JsonArray();
        JsonObject coverage = new JsonObject();

        coverage.addProperty("branch", "0111");
        coverage.addProperty("code", getCode());
        coverage.addProperty("description", "string");

        coverages.add(coverage);

        quoteData.add("coverages", coverages);

        data.add("quoteData", quoteData);
    }
}
