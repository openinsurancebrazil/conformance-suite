package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingRoot extends AbstractPrepareUrlForApi {

	private String api;

	@Override
	@PreEnvironment(strings = "api")
	public Environment evaluate(Environment env) {
		api = env.getString("api");
		return super.evaluate(env);
	}

	@Override
	protected String getApiReplacement() {
		return api;
	}

	@Override
	protected String getEndpointReplacement() {
		return api;
	}
}
