package net.openid.conformance.opin.testmodule.v1.endorsement.v1n3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.endorsement.abstracts.v1n.AbstractOpinEndorsementApiWrongPolicyIDTestV1n;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-endorsement-api-wrong-policyID-test-v1n3",
        displayName = "For this test, we want to ensure that a endorsement can only be created if the specific policy id was endorsed.",
        summary = "\u2022 Call the POST Consents API with all the existing Phase 2 permissions\n" +
                "\u2022 Expects 201 - Validate Response \n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "\u2022 Call the resources API\n" +
                "\u2022 Validate there are at least two policy IDS available - (1) and (2)\n" +
                "\u2022 Extract a shared policy ID on the resources APIs\n" +
                "\u2022 Call the POST Consents Endpoint with the first policy ID available (1) \n" +
                "\u2022 Expects 201 - Validate Response\n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for EXCLUSAO, set policy ID for the second extracted policyID (2) \n" +
                "\u2022 Expects 422 - Validate Error response\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"CONSUMED”\n" +
                "\u2022 Call the POST Consents Endpoint with the first policy ID available (1) \n" +
                "\u2022 Expects 201 - Validate Response\n" +
                "\u2022 Redirect the user to authorize consent\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "\u2022 Call the POST Endorsement Endpoint with pre-defined Payload - Change endorsementType for EXCLUSAO, set policy ID for the first  extracted policyID (1) \n" +
                "\u2022 Expects 201 - Validate Response\n" +
                "\u2022 Call the GET Consents Endpoint\n" +
                "\u2022 Expects 200 - Validate Response is \"CONSUMED\"",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinEndorsementApiWrongPolicyIDTestModuleV1n3 extends AbstractOpinEndorsementApiWrongPolicyIDTestV1n {

}






