package net.openid.conformance.opin.testmodule.v1.consents.v2n7;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentsApiConsentStatusWithIdempotencyKeyTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-consent-api-status-test-v2n7",
	displayName = "Validate that consents are actually authorised on redirect",
	summary = "Validates that consents are actually authorised on redirect\n" +
		"• Call the POST Consents API with either the customer business or the customer personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
		"• Expect a 201 - Validate all of the fields of response_body of the POST Consents API response\n" +
		"• Call the GET Consent API with Bearer Token\n" +
		"• Expect a 200 response - Validate all the fields\n" +
		"• Validate if status is \"AWAITING_AUTHORIZATION\"\n" +
		"• Redirect the user to authorize the created ConsentID - Request all the OPIN Phase 2 scopes\n" +
		"• Calls the GET Consents endpoint with the authorized consentID\n" +
		"• Expects a 200 - Validate that the Consent is on an Authorised state\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"resource.customerUrl",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinConsentsApiConsentStatusTestModuleV2n7 extends AbstractOpinConsentsApiConsentStatusWithIdempotencyKeyTestModule {

	@Override
	protected AbstractJsonAssertingCondition postValidator() {
		return new PostConsentsOASValidatorV2n7();
	}

	@Override
	protected AbstractJsonAssertingCondition getValidator() {
		return new GetConsentsOASValidatorV2n7();
	}
}