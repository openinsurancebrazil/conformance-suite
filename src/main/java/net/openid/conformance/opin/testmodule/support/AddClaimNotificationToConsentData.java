package net.openid.conformance.opin.testmodule.support;

public class AddClaimNotificationToConsentData extends AbstractAddClaimNotificationToConsentData {

    protected String getPolicyFieldName() {
        return "policyNumber";
    }
}