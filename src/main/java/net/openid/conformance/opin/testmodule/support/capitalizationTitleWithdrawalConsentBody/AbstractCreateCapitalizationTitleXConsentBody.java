package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateCapitalizationTitleXConsentBody extends AbstractCondition {

    @Override
    @PreEnvironment(required = "consent_endpoint_request")
    protected Environment evaluate(Environment env) {
        JsonObject data = new JsonObject();

        editData(env, data);

        return env;
    }

    protected abstract void editData(Environment env, JsonObject data);
}
