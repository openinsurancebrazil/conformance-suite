package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class PlansDataSelector extends AbstractCondition {

    @Override
    public Environment evaluate(Environment env) {
        JsonObject body = bodyFrom(env).getAsJsonObject();
        JsonArray data = Optional.ofNullable(body.getAsJsonArray("data"))
                .orElseThrow(() -> error("Could not find data in the body", args("body", body)));

        if (data.isEmpty()) {
            throw error("Data array cannot be empty to extract plan ID");
        }

        JsonArray companies = Optional.ofNullable(data.get(0).getAsJsonObject().getAsJsonObject("brand"))
                .map(brand ->  brand.getAsJsonArray("companies"))
                .orElseThrow(() -> error("Could not extract brand.companies array from the data", args("data", data)));

        if (companies.isEmpty()) {
            throw error("Companies array cannot be empty to extract plan ID");
        }

        JsonArray products = Optional.ofNullable(companies.get(0).getAsJsonObject().getAsJsonArray("products"))
                .orElseThrow(() -> error("Could not extract products array from the data", args("data", data)));

        if (products.isEmpty()) {
            throw error("Products array cannot be empty to extract plan ID");
        }

        JsonObject product1 = products.get(0).getAsJsonObject();
        env.putObject("product_1",product1);

        env.putString("planId",Optional.ofNullable(product1.get("planId"))
                .map(OIDFJSON::getString)
                .orElseThrow(() -> error("could not find plan ID in the product object", args("product", product1))));
        env.putString("selected_id",env.getString("planId"));

        if(products.size()>1){
            JsonObject product2 = products.get(1).getAsJsonObject();
            env.putObject("product_2",product2);

            env.putString("planId2",Optional.ofNullable(product2.get("planId"))
                    .map(OIDFJSON::getString)
                    .orElseThrow(() -> error("could not find plan ID in the 2nd product object", args("product", product2))));
            log("Successfully found both elements.", args("plan ID 1", env.getString("planId"),"plan ID 2", env.getString("planId2")));

            return env;
        }
        log("Product array second element was not found. Proceeding with 1st element only", args("plan id", env.getString("planId")));

        return env;
    }

        private JsonElement bodyFrom(Environment env) {
            try {
                return BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
                        .orElseThrow(() -> error("Could not find response body in the environment"));
            } catch (ParseException e) {
                throw error("Could not parse response body");
            }
        }

    }
