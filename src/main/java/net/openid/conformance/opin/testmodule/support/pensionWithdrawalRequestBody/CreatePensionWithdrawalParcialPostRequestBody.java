package net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.testmodule.Environment;

public class CreatePensionWithdrawalParcialPostRequestBody extends AbstractCreatePensionWithdrawalRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject withdrawalInformation = data.get("withdrawalInfo").getAsJsonObject();
        withdrawalInformation.addProperty("withdrawalType", EnumWithdrawalType.PARCIAL.toString());
        JsonObject desiredTotalAmount =  withdrawalInformation.get("pmbacAmount").getAsJsonObject().deepCopy();
        String amount = desiredTotalAmount.get("amount").getAsString();
        Float partialAmount = Float.parseFloat(amount);
        partialAmount = partialAmount/2;
        desiredTotalAmount.addProperty("amount", String.format("%.2f",partialAmount).replaceAll(",","."));
        withdrawalInformation.add("desiredTotalAmount", desiredTotalAmount);
    }
}
