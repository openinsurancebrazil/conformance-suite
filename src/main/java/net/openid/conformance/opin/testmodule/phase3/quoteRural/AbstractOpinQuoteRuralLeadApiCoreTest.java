package net.openid.conformance.opin.testmodule.phase3.quoteRural;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteRuralLeadApiCoreTest extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_RURAL_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-rural";
    }

    @Override
    protected String getApiName() {
        return "quote-rural";
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }
}
