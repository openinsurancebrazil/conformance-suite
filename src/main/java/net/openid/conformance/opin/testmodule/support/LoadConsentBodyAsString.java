package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadConsentBodyAsString extends AbstractCondition {
    @Override
    @PreEnvironment(required = "consent_endpoint_request")
    @PostEnvironment(strings = "consent_endpoint_request")
    public Environment evaluate(Environment env) {
        env.putString("consent_endpoint_request", env.getObject("consent_endpoint_request").toString());
        return env;
    }
}
