package net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.damageAndPerson.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Get DynamicFields DamageAndPerson V1.3.0")
public class GetDynamicFieldsDamageAndPersonOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/dynamicFields/v1n3/swagger-dynamic-fields-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/damage-and-person";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
