package net.openid.conformance.opin.testmodule.phase1.domesticCredit;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.testmodule.support.oasValidators.domesticCredit.v1n3.GetDomesticCreditOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - Domestic Credit API test",
        displayName = "Validate structure of Domestic Credit response",
        summary = "Validate structure of Domestic Credit response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class DomesticCreditApiTestModule extends AbstractNoAuthFunctionalTestModule {
    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices Domestic Credit response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetDomesticCreditOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
        });
    }
}