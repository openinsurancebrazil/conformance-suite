package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class ExtractDocumentTypeFromPolicyInfoResponse extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_endpoint_response_full")
    @PostEnvironment(strings = "documentType")
    public Environment evaluate(Environment env) {
        JsonObject response = env.getObject("resource_endpoint_response_full");
        String bodyString = OIDFJSON.getString(Optional.ofNullable(response.get("body"))
            .orElseThrow(() -> error("body not present in the api response.")));
        JsonObject body = JsonParser.parseString(bodyString).getAsJsonObject();
        JsonObject data = body.getAsJsonObject("data");
        String documentType = OIDFJSON.getString(Optional.ofNullable(data.get("documentType"))
            .orElseThrow(() -> error("documentType field not present in the api response")));
        env.putString("documentType", documentType);
        logSuccess("documentType field extracted from policy-info response.", args("documentType", documentType));
        return env;
    }
}
