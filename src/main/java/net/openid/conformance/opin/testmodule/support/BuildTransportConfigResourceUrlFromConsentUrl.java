package net.openid.conformance.opin.testmodule.support;

public class BuildTransportConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

    @Override
    protected String getApiReplacement() {
        return "insurance-transport";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-transport";
    }
}
