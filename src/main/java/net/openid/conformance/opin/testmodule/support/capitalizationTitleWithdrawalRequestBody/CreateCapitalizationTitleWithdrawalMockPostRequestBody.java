package net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateCapitalizationTitleWithdrawalMockPostRequestBody extends AbstractCreateCapitalizationTitleWithdrawalRequestBody {

    @Override
    public Environment evaluate(Environment env) {
        JsonObject requestData = new JsonObject();
        JsonObject data = new JsonObject();

        JsonObject productInformation = new JsonObject();
        productInformation.addProperty("capitalizationTitleName", "string");
        productInformation.addProperty("planId","string2");
        productInformation.addProperty("titleId","string3");
        productInformation.addProperty("seriesId","string4");
        productInformation.addProperty("termEndDate","2025-04-01");

        JsonObject withdrawalInformation = new JsonObject();
        withdrawalInformation.addProperty("withdrawalReason","IMPOSSIBILIDADE_DE_PAGAMENTO_DAS_PARCELAS");

        JsonObject withdrawalTotalAmount = new JsonObject();
        withdrawalTotalAmount.addProperty("amount", "2000.05");
        JsonObject unit = new JsonObject();
        unit.addProperty("code","R$");
        unit.addProperty("description","BRL");
        withdrawalTotalAmount.add("unit", unit);

        withdrawalInformation.add("withdrawalTotalAmount", withdrawalTotalAmount);

        data.addProperty("modality", "TRADICIONAL");
        data.addProperty("susepProcessNumber", "54321");
        data.add("productInformation", productInformation);
        data.add("withdrawalInformation", withdrawalInformation);

        editData(env, data);

        requestData.add("data", data);
        logSuccess("The request body was created", args("body", requestData));
        env.putObject("resource_request_entity", requestData);
        env.putString("resource_request_entity", requestData.toString());
        return env;
    }

    @Override
    protected void editData(Environment env, JsonObject data) {}
}
