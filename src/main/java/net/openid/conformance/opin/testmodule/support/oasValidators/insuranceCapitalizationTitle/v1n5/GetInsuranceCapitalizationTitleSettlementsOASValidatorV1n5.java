package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n5;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.AbstractGetInsuranceCapitalizationTitleSettlementsOASValidator;

@ApiName("Insurance Capitalization Title 1.5.0")
public class GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n5 extends AbstractGetInsuranceCapitalizationTitleSettlementsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceCapitalizationTitle/v1/swagger-insurance-capitalization-title-v1.5.0.yaml";
	}
}