package net.openid.conformance.opin.testmodule.support;

public class OPINPhase3Keys {
    public static final String Keys_requestDescription = "Atualização de apolice para testes funcionais do Open Insurance";
	public static final String Keys_diffRequestDescription = "Diferente requestDescription";
	public static final String Keys_Bad_customDatafieldId = "578-psd-71md6971kjh-2d414";
	public static final String Keys_Bad_customDataValue = "This is a random string of data";
	public static final String Keys_occurrenceDescription = "This is a random string of data";
	public static final String Keys_diffOccurrenceDescription = "This is a different random string of data";
	public static final String KEYS_INVALID_POLICY_ID = "00000000";
}
