package net.openid.conformance.opin.testmodule.support.oasValidators.quoteAcceptanceAndBranchesAbroad.v1n8;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Quote Acceptance and Branches Abroad V1.8.0")
public class PostQuoteAcceptanceAndBranchesAbroadLeadOASValidatorV1n8 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteAcceptanceAndBranchesAbroad/swagger-quote-acceptance-and-branches-abroad-1.8.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lead/request";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }
}
