package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleCore;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAckn;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.GetQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PatchQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PostQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodySinglePayment;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchAcknRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title_api_core-ackn_test-module_v1",
        displayName = "opin-quote-capitalization-title_api_core-ackn_test-module_v1",
        summary = """
            Ensure that a Capitalization Title quotation request can be successfully created and accepted afterwards. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            • Call POST request endpoint sending personal or business information, following what is defined at the config, and sending paymentType as UNICO, and singlePayment field at the request
            • Expect 201 - Validate Response and ensure status is RCVD
            • Poll the GET request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET request/{consentId}/quote-status endpoint
            • Expect 200 - Validate response and ensure status is ACPT
            • Call PATCH request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
            • Expect 200 - Validate response and ensure status is ACKN
            • Call GET links.redirect endpoint
            • Expect 200
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteCapitalizationTitleCoreAcknTestModuleV1 extends AbstractOpinQuoteCapitalizationTitleCore {

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteCapitalizationTitlePostRequestBodySinglePayment.class;
    }


    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostQuoteCapitalizationTitleOASValidator.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetQuoteCapitalizationTitleOASValidator.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchAcknRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasAckn.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchQuoteCapitalizationTitleOASValidator.class;
    }
}
