package net.openid.conformance.opin.testmodule.support;

public class BuildInsurancePensionPlanUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {
    @Override
    protected String getApiReplacement() {
        return "insurance-pension-plan";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-pension-plan/contracts";
    }
}
