package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public abstract class AbstractPatchPersonTravelOASValidator extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getEndpointPath() {
        return "/travel/request/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.PATCH;
    }
}
