package net.openid.conformance.opin.testmodule.v1.endorsement.abstracts;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1.PostEndorsementOASValidatorV1;
import net.openid.conformance.opin.testmodule.v1.endorsement.AbstractEndorsementNegativeTest;

public abstract class AbstractEndorsementApiNegativeTestModule extends AbstractEndorsementNegativeTest {

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostEndorsementOASValidatorV1();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }
}
