package net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment;

public class AddPersonEndpointTypeToEnvironment extends AbstractAddEndpointTypeToEnvironment {

    @Override
    protected String endpointType() {
        return "person";
    }
}
