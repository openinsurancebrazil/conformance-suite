package net.openid.conformance.opin.testmodule.support.pcm;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;


public class ExtractClientSsIdFromPcmRequestEvent extends AbstractCondition {


    @Override
    @PreEnvironment(required = "pcm_event")
    public Environment evaluate(Environment env) {
        String clientSsId = Optional.ofNullable(env.getString("pcm_event", "clientSSId"))
                .orElseThrow(() -> error("Could not find clientSsId in the request"));


        env.putString("config", "directory.client_id", clientSsId);
        logSuccess("Extracted clientSSId from from the request", args("clientSSId", clientSsId));
        return env;
    }
}
