package net.openid.conformance.opin.testmodule.v1.structural;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.BuildResourceUrlFromStructuralResourceUrl;
import net.openid.conformance.opin.testmodule.support.PrepareToGetBusinessComplimentaryInformation;
import net.openid.conformance.opin.testmodule.support.PrepareToGetBusinessIdentifications;
import net.openid.conformance.opin.testmodule.support.PrepareToGetBusinessQualifications;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessIdentificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessQualificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.sequences.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-customer-business-api-structural-test",
	displayName = "Validate structure of Customer - Business API resources",
	summary = "Call the “/business/identifications\" endpoint - Expect 200 and validate response\n" +
		"Call the “/business/qualifications\" endpoint - Expect 200 and validate response\n" +
		"Call the “/business/complimentary-information\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"resource.consentUrl"
		}
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinCustomerBusinessStructuralTestModule extends AbstractNoAuthFunctionalTestModule{

	private final String API = "customers";
	@Override
	protected void runTests() {

		call(new ValidateOpinWellKnownUriSteps());

		env.putString("api_base", API);
		callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrl.class);

		runInBlock("Validate Business - Identifications response", () -> {
			callAndStopOnFailure(PrepareToGetBusinessIdentifications.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetCustomersBusinessIdentificationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
		});
		runInBlock("Validate Business - Qualifications response", () -> {
			callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetCustomersBusinessQualificationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
		});
		runInBlock("Validate Business - Complimentary-Information response", () -> {
			callAndStopOnFailure(PrepareToGetBusinessComplimentaryInformation.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5.class, Condition.ConditionResult.FAILURE);
		});
	}
}
