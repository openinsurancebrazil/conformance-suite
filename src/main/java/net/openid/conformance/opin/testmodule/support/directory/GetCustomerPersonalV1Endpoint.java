package net.openid.conformance.opin.testmodule.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetCustomerPersonalV1Endpoint extends AbstractGetXFromAuthServer {

    @Override
    protected String getEndpointRegex() {
        return "^(https:\\/\\/)(.*?)(\\/open-insurance\\/customers\\/v\\d+/personal/identifications)$";
    }

    @Override
    protected String getApiFamilyType() {
        return "customers-personal";
    }

    @Override
    protected String getApiVersionRegex() {
        return "^(1.[0-9].[0-9])$";
    }

    @Override
    protected boolean isResource() {
        return true;
    }
}