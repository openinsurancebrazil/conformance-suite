package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.UUID;

public class CreateRandomConsentID extends AbstractCondition {


    @Override
    @PostEnvironment(strings = "consent_id")
    public Environment evaluate(Environment env) {
        String consentId = String.format("urn:raidiam:%s", UUID.randomUUID());
        env.putString("consent_id", consentId);
        logSuccess("New consent ID created", args(
                "consent_id", consentId
        ));
        return env;
    }
}
