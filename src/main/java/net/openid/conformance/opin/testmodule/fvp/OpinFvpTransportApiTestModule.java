package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.AddTransportScope;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetTransportV1Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportClaimOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportListOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportPolicyInfoOASValidatorV1n2;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceTransport.v1n2.GetInsuranceTransportPremiumOASValidatorV1n2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.util.Map;

@PublishTestModule(
        testName = "opin-fvp-transport-api-test-module",
        displayName = "Validates the structure of all Transport API resources",
        summary ="Validates the structure of all Transport API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_TRANSPORT_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "\u2022 Calls GET transport “/” API \n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
                "\u2022 Calls GET transport policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields \n" +
                "\u2022 Calls GET transport premium API \n" +
                "\u2022 Expects 200- Validate all the fields \n" +
                "\u2022 Calls GET transport claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.consentSyncTime",
                "consent.productType"
        }

)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})

public class OpinFvpTransportApiTestModule extends AbstractOpinFvpApiTestModule {

    @Override
    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetTransportV1Endpoint.class));
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddTransportScope.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_TRANSPORT;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceTransportListOASValidatorV1n2.class;
    }

    @Override
    protected String getApi() {
        return "insurance-transport";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", GetInsuranceTransportPolicyInfoOASValidatorV1n2.class,
                "premium", GetInsuranceTransportPremiumOASValidatorV1n2.class,
                "claim", GetInsuranceTransportClaimOASValidatorV1n2.class
        );
    }
}