package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.lead;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.AbstractPatchPersonLeadOASValidator;

@ApiName("Quote Person 1.10.2")
public class PatchPersonLeadOASValidatorV1n10 extends AbstractPatchPersonLeadOASValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePerson/quote-person-1.10.2.yaml";
    }
}
