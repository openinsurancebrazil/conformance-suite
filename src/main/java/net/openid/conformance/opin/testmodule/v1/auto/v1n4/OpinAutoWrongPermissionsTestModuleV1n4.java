package net.openid.conformance.opin.testmodule.v1.auto.v1n4;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAuto.v1n4.GetInsuranceAutoPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.auto.AbstractOpinAutoWrongPermissionsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-auto-api-wrong-permissions-test-v1.4.0",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
			"Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_AUTO_READ”, “DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET auto “/” API \n" +
		"\u2022 Expects 201 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET auto policy-Info API specifying an Policy ID \n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET auto premium API specifying an Policy ID \n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Calls GET auto claim API specifying an Policy ID \n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field " +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET auto “/” API \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET auto policy-Info API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET auto premium API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET auto claim API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"client.client_id",
			"client.jwks",
			"mtls.key",
			"mtls.cert",
			"resource.consentUrl",
			"resource.brazilCpf",
			"consent.productType"
	}
)
public class OpinAutoWrongPermissionsTestModuleV1n4 extends AbstractOpinAutoWrongPermissionsTestModule {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
		return GetInsuranceAutoPolicyInfoOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
		return GetInsuranceAutoPremiumOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
		return GetInsuranceAutoClaimOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsuranceAutoListOASValidatorV1n4.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
		return IdSelectorFromJsonPath.class;
	}
}