package net.openid.conformance.opin.testmodule.v1.capitalization.v1n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n5.GetInsuranceCapitalizationTitleEventsOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n5.GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n5.GetInsuranceCapitalizationTitlePlansOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n5.GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.v1.capitalization.AbstractCapitalizationTitleApiCoreTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "capitalization-title_api_core_test-module_v1.5.0",
        displayName = "Validates the structure of all Capitalization Tile API",
        summary = "Validates the structure of all Capitalization Tile API\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“CAPITALIZATION_TITLE_READ”, “CAPITALIZATION_TITLE_PLANINFO_READ”, “CAPITALIZATION_TITLE_EVENTS_READ”, “CAPITALIZATION_TITLE_SETTLEMENTS_READ”, “RESOURCES_READ”)\n" +
                "• Expects 201 -  Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Capitalization Title Plans Endpoint\n" +
                "• Expects 200 - Fetches one of the Plan IDs returned\n" +
                "• Calls GET Capitalization Title {planId} plan-info Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} events Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} settlements Endpoint\n" +
                "• Expects 200 - Validate Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class CapitalizationTitleApiCoreTestModuleV1n5 extends AbstractCapitalizationTitleApiCoreTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceCapitalizationTitlePlansOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPlanInfoValidator() {
        return GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getEventsValidator() {
        return GetInsuranceCapitalizationTitleEventsOASValidatorV1n5.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getSettlementsValidator() {
        return GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n5.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }

}
