package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

public class SetEndorsementPolicyIDtoSecondOneDiff extends AbstractSetEndorsementPolicyIDtoSecondOne {

    @Override
    protected String getPolicyFieldName() {
        return "policyId";
    }
}
