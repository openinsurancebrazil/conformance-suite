package net.openid.conformance.opin.testmodule.support;

public class BuildFinancialRiskConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "insurance-financial-risk";
	}

	@Override
	protected String getEndpointReplacement() {
		return "insurance-financial-risk";
	}
}
