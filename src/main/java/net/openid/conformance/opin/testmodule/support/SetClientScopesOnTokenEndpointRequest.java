package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetClientScopesOnTokenEndpointRequest extends AbstractCondition {
    @PreEnvironment(
            required = {"token_endpoint_request_form_parameters", "client"}
    )
    @PostEnvironment(
            required = {"token_endpoint_request_form_parameters"}
    )
    public Environment evaluate(Environment env) {
        JsonObject tokenEndpointRequest = env.getObject("token_endpoint_request_form_parameters");
        tokenEndpointRequest.addProperty("scope", env.getString("client", "scope"));
        this.logSuccess("Set scope parameter to the client scopes", tokenEndpointRequest);
        env.putObject("token_endpoint_request_form_parameters", tokenEndpointRequest);
        return env;
    }
}
