package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.pensionWithdrawalLead;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.AbstractOpinPhase3PensionWithdrawNegativeTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.pensionWithdrawal.v1n3.PostPensionWithdrawalOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-pension-withdraw-lead_api_wrong-certificateId_test-module_v1",
        displayName = "Validates the structure of Pension Withdrawal API",
        summary = """
                Ensure a life pension withdrawal request cannot be successfully executed if the consent was not granted to the specified certificateId. This test module will require at least two valid certificate IDs at the insurance-life-pension/contracts Endpoint.
                
                · Execute a Customer Data Sharing Journey for the Life Pension Product, obtaining the first and second productName and certificateId from the insurance-life-pension/contracts endpoint, and the corresponding data.suseps.FIE.pmbacAmount from the contract-info endpoint
                · Call the POST Consents with PENSION_WITHDRAWAL_LEAD_CREATE permission, sending the withdrawalLifePensionInformation with the pre-saved information from the first certificateID,  withdrawalType as 1_TOTAL, and not sending the desiredTotalAmount field
                · Expect a 201 - Validate the response
                · Redirect the User to Authorize the Consent
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response and ensure the status is "AUTHORIZED"
                · Call the POST lead/request endpoint, sending the same information from the second certificateId
                · Expect a 422- Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3PensionWithdrawLeadApiWrongCertificateIdTestModule extends AbstractOpinPhase3PensionWithdrawLeadNegativeTestModule {

    @Override
    protected void configureClient() {
        super.configureClient();
        env.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());
    }
    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        callSecondResource=true;
        super.onConfigure(config, baseUrl);
    }

    @Override
    protected void editPensionWithdrawalRequestBody() {
        //not needed in this test
    }

}
