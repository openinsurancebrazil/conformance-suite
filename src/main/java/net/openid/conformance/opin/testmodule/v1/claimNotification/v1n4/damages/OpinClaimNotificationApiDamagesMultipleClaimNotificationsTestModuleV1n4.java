package net.openid.conformance.opin.testmodule.v1.claimNotification.v1n4.damages;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.AddClaimNotificationToConsentDataDiff;
import net.openid.conformance.opin.testmodule.support.CreateClaimNotificationRequestBodyDiff;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AddDamageEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1n4.PostClaimNotificationDamageOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.v1.claimNotification.AbstractClaimNotificationMultipleClaimNotificationsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "opin-claim-notification-api-damages-multiple-claim-notifications-test-v1.4.0",
    displayName = "Ensure that Claim notification damages cannot be created multiple times with a unique consent.",
    summary = "Ensure that Claim notification damages cannot be created multiple times with a unique consent\n" +
        "\u2022 Call the POST Consents API with all the existing Phase 2 permissions\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET resources API\n" +
        "\u2022 Expects 200 - Validate Response\n" +
        "\u2022 Call the GET Shared API policy-info endpoint with the extracted resourceId, where the API to be called is to be determined by the Type of the extracted policyID\n" +
        "\u2022 Expect a 200 - Extract the field data.documentType from the response_body\n" +
        "\u2022 Call the POST Consents Endpoint with Claim Notification permissions\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AUTHORISED\"\n" +
        "\u2022 Call the POST Claim Notification at the damages endpoint\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"CONSUMED\"\n" +
        "\u2022 Call the POST Claim Notification with a different payload and a different idempotency key\n" +
        "\u2022 Expects 401 - Validate Error Message",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "resource.consentUrl",
        "resource.brazilCpf",
        "consent.productType"
    }
)
public class OpinClaimNotificationApiDamagesMultipleClaimNotificationsTestModuleV1n4 extends AbstractClaimNotificationMultipleClaimNotificationsTest {
    @Override
    protected AbstractAddEndpointTypeToEnvironment addEndpointType() {
        return new AddDamageEndpointTypeToEnvironment();
    }

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostClaimNotificationDamageOASValidatorV1n4();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n7();
    }

    protected void createClaimNotificationRequestBody() {
        callAndStopOnFailure(CreateClaimNotificationRequestBodyDiff.class);
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps()
                .insertAfter(FAPIBrazilOpenInsuranceCreateConsentRequest.class,
                        condition(AddClaimNotificationToConsentDataDiff.class).skipIfStringMissing("policyId"));
    }

    @Override
    protected void validateGetConsentResponse() {
        callAndContinueOnFailure(GetConsentsOASValidatorV2n7.class, Condition.ConditionResult.FAILURE);
    }

}
