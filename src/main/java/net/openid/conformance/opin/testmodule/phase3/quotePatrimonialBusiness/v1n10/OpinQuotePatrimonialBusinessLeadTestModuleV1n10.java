package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialBusiness.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinQuotePatrimonialLeadTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.lead.PatchPatrimonialLeadOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.lead.PostPatrimonialLeadOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialLeadPostRequestBodyWithCoverage;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.EditQuotePatrimonialLeadPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-patrimonial-business-lead_api_core_test-module_v1n10",
        displayName = "Ensure that a Patrimonial Lead request can be successfully created and deleted afterwards.",
        summary = """
            Ensure that after an initial Lead request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idempotency key the request fails
            · Call POST lead/request endpoint sending personal or business information, following what is defined at the config
            · Expect 201 - Validate Response and ensure status is RCVD
            · Call the POST lead/request Endpoint with the same payload and idempotency id
            · Expects 201 - Validate Response
            · Call the POST lead/request, with a different payload as the previous request but the same idempotency id
            · Expects 422 ERRO_IDEMPOTENCIA - Validate Response
            · Call PATCH lead/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF
            · Expect 200 - Validate Response and ensure status is CANC
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialBusinessLeadTestModuleV1n10 extends AbstractOpinQuotePatrimonialLeadTest {

    @Override
    protected String getApiFamilyType() {
        return "quote-patrimonial-business";
    }

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteLeadResponse() {
        return PostPatrimonialLeadOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteLeadResponse() {
        return PatchPatrimonialLeadOASValidatorV1n10.class;
    }

    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialLeadPostRequestBodyWithCoverage.class;
    }

    @Override
    protected Class<? extends AbstractCondition> editQuoteLeadPostRequestBody() {
        return EditQuotePatrimonialLeadPostRequestBodyWithCoverage.class;
    }
}