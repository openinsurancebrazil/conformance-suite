package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AbstractAddEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.claimNotificationAddEndpointTypeToEnvironment.AddDamageEndpointTypeToEnvironment;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1.PostClaimNotificationDamageOASValidatorV1;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-claim-notification-api-damages-wrong-policyID-test",
        displayName = "Ensure that Claim Notification for Damages cannot be created for a policy that was not consented",
        summary = "Ensure that Claim Notification Damages cannot be created for a policy that was not consented. For this test, at least two insurance policies for the same type  should be available at the resources API\n" +
                "• Call the POST Consents API with all the existing Phase 2 permissions\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Call the GET resources API\n" +
                "• Expects 200 - Validate Response and if there are at least two resources from the same type\n" +
                "• Call the GET Shared API policy-info endpoint with the extracted resourceId, where the API to be called is to be determined by the Type of the extracted policyID\n" +
                "• Expect a 200 - Extract the field data.documentType from the response_body\n" +
                "• Call the POST Consents Endpoint with the first policy ID available\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Call the GET Consents Endpoint\n" +
                "• Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "• Call the POST Claim Notification Endpoint with a different policy ID, in the Damages endpoint\n" +
                "• Expects 422 - Validate Error response\n" +
                "• Call thE GET Consents Endpoint\n" +
                "• Expects 200 - Validate Response is CONSUMED\n" +
                "• Call the POST Consents Endpoint with the first policy ID available\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Call the GET Consents Endpoint\n" +
                "• Expects 200 - Validate Response is \"AUTHORISED\"\n" +
                "• Call the POST Claim Notification Endpoint with the first policyID\n" +
                "• Expects 201 - Validate Response\n" +
                "• Call thE GET Consents Endpoint\n" +
                "• Expects 200 - Validate Response is \"CONSUMED\"",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinClaimNotificationDamagesApiWrongPolicyIDTestModule extends AbstractOpinClaimNotificationApiWrongPolicyIDTestModule {

    @Override
    protected AbstractAddEndpointTypeToEnvironment addEndpointType() {
        return new AddDamageEndpointTypeToEnvironment();
    }

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostClaimNotificationDamageOASValidatorV1();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }
}
