package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractOBBrasilFunctionalTestModuleOptionalErrors;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.opin.testmodule.phase3.ClearIdempotencyKeyHeader;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;

import java.util.Optional;

public abstract class AbstractOpinConsentsApiConsentStatusTestModule extends AbstractOBBrasilFunctionalTestModuleOptionalErrors {

	private OpinConsentPermissionsBuilder permissionsBuilder;
	protected abstract AbstractJsonAssertingCondition postValidator();
	protected abstract AbstractJsonAssertingCondition getValidator();

	private static final String API_TYPE =  ApiTypes.DATA_API_PHASE2.toString();

	@Override
	protected void configureClient() {
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		callAndStopOnFailure(BuildOpinCustomCustomersConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(AddAllScopes.class);

		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS).build();
		} else {
			permissionsBuilder.addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL).build();
		}
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		stepsAfterPreAuthorizationSteps();
	}

	protected void stepsAfterPreAuthorizationSteps() {
		callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
		fetchConsentToCheckAwaitingAuthorizationStatus();
	}

	protected void fetchConsentToCheckAwaitingAuthorizationStatus() {
		runInBlock("Checking the created consent - Expecting AWAITING_AUTHORISATION status", () -> {
			callAndStopOnFailure(PaymentConsentIdExtractor.class);
			callAndStopOnFailure(AddJWTAcceptHeader.class);
			callAndStopOnFailure(ExpectJWTResponse.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		});
		runInBlock("Validate GET consents response", () -> {
			callAndContinueOnFailure(EnsureConsentWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
			env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			env.unmapKey("resource_endpoint_response_full");
		});
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpenBankingBrazilPreAuthorizationSteps steps = new OpenBankingBrazilPreAuthorizationSteps(false,
				false, addTokenEndpointClientAuthentication, false, true,false);
		steps
				.replace(FAPIBrazilConsentEndpointResponseValidatePermissions.class,condition(OpinConsentEndpointResponseValidatePermissions.class))
				.then(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"),
						condition(postValidator().getClass()).dontStopOnFailure());
		return steps;
	}

	@Override
	protected void requestProtectedResource() {
		runInBlock("Calling GET consents - Expecting AUTHORISED status", () -> {
			callAndStopOnFailure(ConsentIdExtractor.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		});
		runInBlock("Validate GET consents response", () -> {
			exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentWasAuthorised.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getValidator().getClass(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void validateResponse() {
		//Not needed for this test
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		String resourceResponse = "resource_endpoint_response_full";
		String consentResponse = "consent_endpoint_response_full";
		String methodString = env.getString("http_method");
		HttpMethod method;
		if(methodString != null) {
			method = HttpMethod.valueOf(methodString);
			if (method.equals(HttpMethod.DELETE)) {
				return;
			}
		}

		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
				validateLinksAndMeta(resourceResponse);
			} else {
				validateLinksAndMeta(consentResponse);
			}
		}
		if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
				CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass())) {
			validateLinksAndMeta("consent_endpoint_response_full");
		}
	}

	private boolean isEndpointCallSuccessful(int status) {
		return status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK;
	}

	private void validateLinksAndMeta(String responseFull) {

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
				.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
		if (!isEndpointCallSuccessful(status)) {
			return;
		}

		if (responseFull.contains("resource") || env.getElementFromObject(responseFull, "body_json.meta") != null) {
			if (responseFull.contains("resource")) {
				env.mapKey(OpinPaginationValidator.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			}

			call(condition(OpinPaginationValidator.class)
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure());

			if (responseFull.contains("resource")) {
				env.unmapKey(OpinPaginationValidator.RESPONSE_ENV_KEY);
			}
		}

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
		if (recursion.equals("false")) {
			callAndStopOnFailure(SaveOldValues.class);
			env.putString("recursion", "true");
			env.mapKey("resource_endpoint_response_full", responseFull);

			if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
				validateLinks(responseFull);
			}

			env.unmapKey("resource_endpoint_response_full");
			callAndStopOnFailure(LoadOldValues.class);
			env.putString("recursion", "false");
		}
	}

	private void validateLinks(String responseFull) {
		call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
		env.mapKey("resource_endpoint_response_full", responseFull);

		ConditionSequence sequence = new ValidateSelfEndpoint();

		if (responseFull.contains("consent")) {
			sequence.insertAfter(ClearContentTypeHeaderForResourceEndpointRequest.class , condition(ClearIdempotencyKeyHeader.class));
			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
		}

		call(sequence);
	}
}
