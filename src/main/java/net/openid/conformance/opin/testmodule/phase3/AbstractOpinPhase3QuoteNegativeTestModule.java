package net.openid.conformance.opin.testmodule.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.opin.testmodule.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.directory.GetQuoteResourceV1Endpoint;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinPhase3QuoteNegativeTestModule extends OpinAbstractClientCredentialsGrantFunctionalTestModule {

    private static final String API_TYPE =  ApiTypes.DATA_API_PHASE3.toString();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> quoteValidator();

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        env.putObject("client", config.getAsJsonObject("client"));
        env.putString("api_type", API_TYPE);

        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
        call(new ValidateWellKnownUriSteps());
        callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
        callAndStopOnFailure(CreateRandomConsentID.class);

        ScopesAndPermissionsBuilder builder = new ScopesAndPermissionsBuilder(env,eventLog);
        builder.addScopes(getScope());
        builder.build();
    }

    @Override
    protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
        return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence).replace(
                SetConsentsScopeOnTokenEndpointRequest.class,
                condition(SetClientScopesOnTokenEndpointRequest.class)
        );
    }

    @Override
    protected void runTests() {
        env.putString("api_family_type", getApiFamilyType());
        env.putString("api_name", getApiName());
        env.putString("api_endpoint", getApiBaseEndpoint());
        callQuoteResourceEndpoint();
        postQuoteCalls();
    }

    protected void callQuoteResourceEndpoint() {
        callAndStopOnFailure(GetQuoteResourceV1Endpoint.class);
    }

    protected void postQuoteCalls() {
        this.postQuoteWithoutConsentId();
        this.postQuoteWithWrongScope();
    }

    protected void postQuoteWithoutConsentId() {
        runInBlock("Create a quote without the consent ID", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            call(setHeadersSequence());
            callAndStopOnFailure(createPostQuoteRequestBody());
            callAndStopOnFailure(RemoveConsentIdFromQuoteRequestBody.class);
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(SetResourceMethodToPost.class);
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the quote creation error response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas400or422.class);
            callAndStopOnFailure(quoteValidator());
        });
    }

    protected void postQuoteWithWrongScope() {

        ScopesAndPermissionsBuilder builder = new ScopesAndPermissionsBuilder(env,eventLog);
        builder.addScopes(getLeadScope());
        builder.build();
        call(createGetAccessTokenWithClientCredentialsSequence(this.clientAuthSequence));

        runInBlock("Create a quote with a wrong scope", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            call(setHeadersSequence());
            callAndStopOnFailure(createPostQuoteRequestBody());
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(SetResourceMethodToPost.class);
            skipSelfLinkValidation();
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the quote creation error response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
            callAndStopOnFailure(quoteValidator());
        });
    }

    protected abstract OPINScopesEnum getLeadScope();

    protected ConditionSequence setHeadersSequence() {
        return sequenceOf(
                condition(CreateEmptyResourceEndpointRequestHeaders.class),
                condition(AddJsonAcceptHeaderRequest.class),
                condition(AddFAPIAuthDateToResourceEndpointRequest.class),
                condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
                condition(CreateRandomFAPIInteractionId.class),
                condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
                condition(CreateIdempotencyKey.class),
                condition(AddIdempotencyKeyHeader.class)
        );
    }

    protected abstract OPINScopesEnum getScope();
    protected abstract String getApiFamilyType();
    protected abstract String getApiName();
    protected abstract String getApiBaseEndpoint();
    protected abstract Class<? extends AbstractCondition> createPostQuoteRequestBody();
}
