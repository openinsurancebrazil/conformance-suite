package net.openid.conformance.opin.testmodule.v1.financialRisk;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPremiumOASValidatorV1n3;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.util.Map;

@PublishTestModule(
	testName = "opin-financial-risk-api-test",
	displayName = "Validates the structure of all financial risk API resources",
	summary = "Validates the structure of all financial risk API resources\n" +
		"\u2022 Creates a consent with all the permissions needed to access the financial risk API (“DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Financial Risk “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET Financial Risk policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET Financial Risk premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Financial Risk claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinFinancialRiskApiTestModule extends AbstractOpinFinancialRiskApiTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getRootValidator() {
		return GetInsuranceFinancialRiskListOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator() {
		return GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator() {
		return GetInsuranceFinancialRiskPremiumOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator() {
		return GetInsuranceFinancialRiskClaimOASValidatorV1n3.class;
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
		return IdSelectorFromJsonPath.class;
	}

}
