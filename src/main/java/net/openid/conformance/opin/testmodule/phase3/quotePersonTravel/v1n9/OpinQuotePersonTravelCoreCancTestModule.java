package net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.AbstractOpinQuotePersonTravelCore;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasCanc;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel.GetPersonTravelQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel.PatchPersonTravelOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n10.travel.PostPersonTravelOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchCancRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-travel_api_core-canc_test-module_v1",
        displayName = "Ensure that a person travel quotation request can be successfully created and rejected afterwards.",
        summary = """
            Ensure that a person travel quotation request can be successfully created and rejected afterwards. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            • Call POST travel/request endpoint sending personal or business information, following what is defined at the config
            • Expect 201 - Validate Response and ensure status is RCVD
            • Poll the GET travel/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET travel/request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Respones and ensure status is ACPT
            • Call PATCH travel/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC
            • Expect 200 - Validate Response and ensure status is CANC
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonTravelCoreCancTestModule extends AbstractOpinQuotePersonTravelCore {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPersonTravelOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPersonTravelQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchCancRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasCanc.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPersonTravelOASValidatorV1n10.class;
    }
}