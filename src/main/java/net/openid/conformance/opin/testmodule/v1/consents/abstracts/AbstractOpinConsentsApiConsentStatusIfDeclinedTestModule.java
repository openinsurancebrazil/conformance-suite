package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.TransformConsentRequestForProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRejectUserManuallyRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;


public abstract class AbstractOpinConsentsApiConsentStatusIfDeclinedTestModule extends AbstractOpinFunctionalTestModule {

	OpinConsentPermissionsBuilder permissionsBuilder;
	protected abstract AbstractJsonAssertingCondition postValidator();
	protected abstract AbstractJsonAssertingCondition getValidator();

	private static final String API_TYPE = ApiTypes.DATA_API_PHASE2.toString();
	@Override
	protected void configureClient(){
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(OpinInsertMtlsCa.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		this.callAndStopOnFailure(GetResourceEndpointConfiguration.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
		permissionsBuilder.buildFromEnv();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpenBankingBrazilPreAuthorizationSteps steps = new OpenBankingBrazilPreAuthorizationSteps(false,
			false, addTokenEndpointClientAuthentication, false, true,false);
		steps
			.replace(FAPIBrazilConsentEndpointResponseValidatePermissions.class,condition(OpinConsentEndpointResponseValidatePermissions.class))
				.insertAfter(CreateEmptyResourceEndpointRequestHeaders.class,
						sequenceOf(
								condition(CreateIdempotencyKey.class),
								condition(AddIdempotencyKeyHeader.class),
								condition(CreateRandomFAPIInteractionId.class),
								condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
						))
			.then(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"),
			condition(postValidator().getClass()).dontStopOnFailure());
		return steps;
	}

	@Override
	protected void validateResponse() {
		runInBlock("Validating get consent response", () -> {
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndStopOnFailure(TransformConsentRequestForProtectedResource.class);
			call(createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication));
			preCallProtectedResource("Fetch consent");
			callAndContinueOnFailure(EnsureConsentStatusWasRejected.class, Condition.ConditionResult.WARNING);
			callAndContinueOnFailure(getValidator().getClass(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentRejectUserManuallyRejected.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void onAuthorizationCallbackResponse() {

		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");
		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);
		callAndContinueOnFailure(CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint.class, Condition.ConditionResult.WARNING, "OIDCC-3.1.2.6");
		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");
		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");

		eventLog.startBlock(currentClientString() + "Validate response");
		validateResponse();
		eventLog.endBlock();

		fireTestFinished();
	}

	@Override
	protected void preCallProtectedResource() {
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
		callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
		callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CheckForDateHeaderInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
		callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
		callAndContinueOnFailure(EnsureResourceResponseReturnedJsonContentType.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-9", "FAPI1-BASE-6.2.1-10");
	}
}
