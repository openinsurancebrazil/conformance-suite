package net.openid.conformance.opin.testmodule.phase3.quoteAuto;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvdOrRjct;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRjct;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteAutoPostRequestBodyWithDiffTermDate;


public abstract class AbstractOpinQuoteAutoApiCoreRejectedTestModule extends AbstractOpinQuoteAutoApiCore {

        @Override
        protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
                return CreateQuoteAutoPostRequestBodyWithDiffTermDate.class;
        }

        @Override
        protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
                return EnsureStatusWasRcvdOrRjct.class;
        }


        @Override
        protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
                return EnsureStatusWasRjct.class;
        }

        // Quote is rejected so it will not be patched

        @Override
        protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
                return null;
        }

        @Override
        protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
                return null;
        }

        @Override
        protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
                return null;
        }
}
