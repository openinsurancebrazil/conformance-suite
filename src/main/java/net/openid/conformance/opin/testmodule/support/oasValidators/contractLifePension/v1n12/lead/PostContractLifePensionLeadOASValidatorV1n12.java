package net.openid.conformance.opin.testmodule.support.oasValidators.contractLifePension.v1n12.lead;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Contract Life Pension 1.12.1")
public class PostContractLifePensionLeadOASValidatorV1n12 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/contractLifePension/contract-life-pension-1.12.1.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lead/request";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }
}