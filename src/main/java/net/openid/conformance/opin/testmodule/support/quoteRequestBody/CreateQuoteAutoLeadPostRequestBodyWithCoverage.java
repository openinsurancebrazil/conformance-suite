package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteAutoLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "CASCO_COMPREENSIVA";
    }
}
