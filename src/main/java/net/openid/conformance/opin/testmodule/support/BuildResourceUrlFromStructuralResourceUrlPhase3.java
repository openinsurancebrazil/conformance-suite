package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Objects;

public abstract class BuildResourceUrlFromStructuralResourceUrlPhase3 extends OpinStructuralResourceBuilder {

    private String consentId;
    private String endpointType;

    @Override
    @PreEnvironment(required = "config", strings = {"api_base", "claim_notification_endpoint_type", "consent_id"})
    public Environment evaluate(Environment env) {
        setApi(env.getString("api_base"));
        consentId = env.getString("consent_id");
        endpointType = env.getString("claim_notification_endpoint_type");
        if (!Objects.equals(endpointType, "")) {
            endpointType = "/" + endpointType;
        }
        return super.evaluate(env);
    }

    @Override
    protected String getEndpointReplacement() {
        return String.format("request%s/%s", endpointType, consentId);
    }
}
