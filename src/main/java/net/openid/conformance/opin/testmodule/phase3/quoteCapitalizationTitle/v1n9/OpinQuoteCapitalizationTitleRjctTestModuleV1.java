package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleRjctTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.GetQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n9.PostQuoteCapitalizationTitleOASValidator;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentForRjct;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title_api_rejected_test-module_v1",
        displayName = "opin-quote-capitalization-title_api_rejected_test-module_v1",
        summary = """
            Ensure that a Capitalization Title quotation request can be rejected. This test module will send the quoteData with only the minimum required fields, paymentType set to Único, singlePayment amount set to 0.00, and expect the quotation to be rejected. This test applies to both travel and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            • Call POST request endpoint sending travelal or business information, following what is defined at the config, and sending the quoteData with only the minimum required fields, paymentType set to Único, singlePayment amount set to 0.00
            • Expect 201 - Validate Response and ensure status is RCVD or RJCT
            • Poll the GET request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Response and ensure status is RJCT
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteCapitalizationTitleRjctTestModuleV1 extends AbstractOpinQuoteCapitalizationTitleRjctTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostQuoteCapitalizationTitleOASValidator.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetQuoteCapitalizationTitleOASValidator.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentForRjct.class;
    }
}