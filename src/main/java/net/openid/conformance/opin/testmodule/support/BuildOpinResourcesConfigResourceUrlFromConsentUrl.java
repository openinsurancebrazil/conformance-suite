package net.openid.conformance.opin.testmodule.support;

public class BuildOpinResourcesConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "resources";
	}

	@Override
	protected String getEndpointReplacement() {
		return "resources";
	}
}
