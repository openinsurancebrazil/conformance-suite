package net.openid.conformance.opin.testmodule.support;

public enum ApiTypes {
    DATA_API_PHASE2,
    DATA_API_PHASE3
}
