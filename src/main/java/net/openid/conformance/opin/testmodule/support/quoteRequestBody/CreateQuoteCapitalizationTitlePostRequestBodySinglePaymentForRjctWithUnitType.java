package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateQuoteCapitalizationTitlePostRequestBodySinglePaymentForRjctWithUnitType extends AbstractCreateQuotePostRequestBody {

    @Override
    protected void editData(Environment env, JsonObject data) {
        this.addQuoteData(env, data);
    }

    private void addQuoteData(Environment env, JsonObject data) {

        JsonObject singlePayment = new JsonObject();
        singlePayment.addProperty("amount", "1000.00");
        singlePayment.addProperty("unitType", "PORCENTAGEM");

        JsonObject quoteData = new JsonObject();

        quoteData.addProperty("modality", "TRADICIONAL");
        quoteData.addProperty("paymentType", "UNICO");
        quoteData.add("singlePayment", singlePayment);

        data.add("quoteData", quoteData);
    }

}
