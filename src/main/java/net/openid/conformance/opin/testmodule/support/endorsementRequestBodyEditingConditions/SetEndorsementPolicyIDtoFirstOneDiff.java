package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

public class SetEndorsementPolicyIDtoFirstOneDiff extends AbstractSetEndorsementPolicyIDtoFirstOne {

    protected String getPolicyFieldName(){
        return "policyId";
    }
}
