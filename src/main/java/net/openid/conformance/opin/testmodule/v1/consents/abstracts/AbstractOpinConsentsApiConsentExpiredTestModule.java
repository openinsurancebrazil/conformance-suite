package net.openid.conformance.opin.testmodule.v1.consents.abstracts;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRejectAspspMaxDateReached;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;


public abstract class AbstractOpinConsentsApiConsentExpiredTestModule extends AbstractOpinFunctionalTestModule {

	OpinConsentPermissionsBuilder permissionsBuilder;

	protected abstract AbstractJsonAssertingCondition postValidator();
	protected abstract AbstractJsonAssertingCondition getValidator();

	private static final String API_TYPE =  ApiTypes.DATA_API_PHASE2.toString();

	@Override
	protected void configureClient(){
		super.configureClient();
		env.putString("api_type", API_TYPE);
		callAndStopOnFailure(OpinInsertMtlsCa.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		this.callAndStopOnFailure(GetResourceEndpointConfiguration.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
		permissionsBuilder.buildFromEnv();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpenBankingBrazilPreAuthorizationSteps steps = new OpenBankingBrazilPreAuthorizationSteps(false,
				false, addTokenEndpointClientAuthentication, false, true,false);
		steps.replace(FAPIBrazilAddExpirationToConsentRequest.class, condition(AddExpirationInTwoMinutes.class));
		steps.replace(FAPIBrazilConsentEndpointResponseValidatePermissions.class,condition(OpinConsentEndpointResponseValidatePermissions.class))
				.insertAfter(CreateEmptyResourceEndpointRequestHeaders.class,
				sequenceOf(
						condition(CreateIdempotencyKey.class),
						condition(AddIdempotencyKeyHeader.class),
						condition(CreateRandomFAPIInteractionId.class),
						condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
				))
				.then(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"),
						condition(postValidator().getClass()).dontStopOnFailure());
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		return steps;
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndContinueOnFailure(EnsureResourceResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void requestProtectedResource() {
		runInBlock("Validating get consent response", () -> {
			callAndStopOnFailure(ConsentIdExtractor.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndStopOnFailure(TransformConsentRequestForProtectedResource.class);
			call(createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication));
			preCallProtectedResource("Fetch consent");
			callAndContinueOnFailure(EnsureConsentWasAuthorised.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getValidator().getClass(), Condition.ConditionResult.FAILURE);
		});
		callAndContinueOnFailure(WaitFor120Seconds.class, Condition.ConditionResult.FAILURE);
		runInBlock("Validating get consent Response", () -> {
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndStopOnFailure(TransformConsentRequestForProtectedResource.class);
			call(createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication));
			preCallProtectedResource("Fetch consent");
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentStatusWasRejected.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentRejectAspspMaxDateReached.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getValidator().getClass(), Condition.ConditionResult.FAILURE);
		});

		fireTestFinished();
	}

	@Override
	protected void preCallProtectedResource() {
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
		callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
		callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CheckForDateHeaderInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
		callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
		callAndContinueOnFailure(EnsureResourceResponseReturnedJsonContentType.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-9", "FAPI1-BASE-6.2.1-10");
	}

	@Override
	protected void validateResponse() {
	}

	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainAccessTokenWithClientCredentials(clientAuthSequence);
	}
}
