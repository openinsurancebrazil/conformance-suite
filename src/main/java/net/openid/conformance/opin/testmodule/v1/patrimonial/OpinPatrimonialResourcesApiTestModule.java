package net.openid.conformance.opin.testmodule.v1.patrimonial;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts.AbstractOpinPatrimonialResourcesApiTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-patrimonial-resources-api-test",
	displayName = "Validate structure of all patrimonial API resources",
	summary = "Makes sure that the Resource API  and the API that is the scope of this test plan are returning the same available IDs\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Patrimonial API (“DAMAGES_AND_PEOPLE_PATRIMONIAL_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects server to create the consent with 201\n" +
		"\u2022 Redirect the user to authorize at the financial institution\n" +
		"\u2022 Call the Patrimonial “/” API\n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that a policy id is returned - Fetch the policy id provided by this API\n" +
		"\u2022 Call the resources API \n" +
		"\u2022 Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
public class OpinPatrimonialResourcesApiTestModule extends AbstractOpinPatrimonialResourcesApiTestModule {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
		return GetInsurancePatrimonialListOASValidatorV1n4.class;
	}
}
