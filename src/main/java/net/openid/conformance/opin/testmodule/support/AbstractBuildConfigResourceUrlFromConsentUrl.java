package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractBuildConfigResourceUrlFromConsentUrl extends AbstractCondition {

	private final String validatorRegex = "^(https://)(.*?)(/open-insurance/consents/v\\d+/consents)$";

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String consentUrl = env.getString("config","resource.consentUrl");
		String apiVersion = new OPINApiVersions().getApiVersion(getApiReplacement());
		String apiReplacement = getApiReplacement();
		String endpointReplacement = getEndpointReplacement();

		if(!consentUrl.matches(validatorRegex)) {
			throw error("consentUrl is not valid, please ensure that url matches " + validatorRegex, args("consentUrl", consentUrl));
		}

		String resourceUrl = consentUrl.replaceFirst("consents", apiReplacement).replaceFirst("consents", endpointReplacement);
		resourceUrl = resourceUrl.replaceFirst("v\\d+", apiVersion);
		env.putString("protected_resource_url", resourceUrl);
		env.putString("config", "resource.resourceUrl", resourceUrl);

		logSuccess(String.format("resourceUrl and protected_resource_url for %s set up", apiReplacement), args("resource_url", resourceUrl, "protected_resource_url", resourceUrl));
		return env;
	}

	protected abstract String getApiReplacement();
	protected abstract String getEndpointReplacement();
}
