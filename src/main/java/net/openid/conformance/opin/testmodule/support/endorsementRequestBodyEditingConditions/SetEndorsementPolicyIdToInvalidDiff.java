package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

public class SetEndorsementPolicyIdToInvalidDiff extends AbstractSetEndorsementPolicyIdToInvalid {

    protected String getPolicyFieldName(){
        return "policyId";
    }
}
