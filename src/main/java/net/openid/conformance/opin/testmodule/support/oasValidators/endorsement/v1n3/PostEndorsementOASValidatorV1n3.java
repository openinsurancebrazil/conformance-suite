package net.openid.conformance.opin.testmodule.support.oasValidators.endorsement.v1n3;

import com.google.gson.JsonObject;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Post Endorsement V1.3.0")
public class PostEndorsementOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/endorsement/v1n3/swagger-endorsement-1.3.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/request/{consentId}";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }

    @Override
    protected void assertSchemaAdditionalConstraints(JsonObject body, Integer statusCode) {

    }
}
