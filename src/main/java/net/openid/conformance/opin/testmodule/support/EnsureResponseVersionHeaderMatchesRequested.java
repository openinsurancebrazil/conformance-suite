package net.openid.conformance.opin.testmodule.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.regex.Pattern;

public class EnsureResponseVersionHeaderMatchesRequested extends AbstractCondition {
    @Override
    @PreEnvironment(required = "resource_endpoint_response_headers",strings = "x-v")
    public Environment evaluate(Environment env) {

        if(Strings.isNullOrEmpty(env.getString("resource_endpoint_response_headers","x-v"))){
            throw error("x-v header is missing from response");
        }

        String highestExpected = env.getString("x-v");
        String minimumExpected = env.getString("x-v-min");
        String actual = env.getString("resource_endpoint_response_headers", "x-v");

        if(!Pattern.matches("^\\d+\\.\\d+\\.\\d+$",actual)){
            throw error("x-v must be in the format: ^\\\\d+\\\\.\\\\d+\\\\.\\\\d+$", args("x-v returned:", Strings.nullToEmpty(actual)));
        }

        if (Strings.isNullOrEmpty(minimumExpected) || isVersionGreater(highestExpected,minimumExpected)) {
            if (highestExpected.equals(actual)) {
                logSuccess("Response's x-v matched with request's x-v", args("Response's x-v", Strings.nullToEmpty(actual)));
                return env;
            } else {
                throw error("x-v returned different from requested", args("Expected", Strings.nullToEmpty(highestExpected), "Actual", Strings.nullToEmpty(actual)));
            }
        } else {
            if(versionIsCompatible(highestExpected, minimumExpected, actual)){
                logSuccess("Returned version is compatible.",
                        args("Requested", Strings.nullToEmpty(highestExpected),
                                "Actual", Strings.nullToEmpty(actual),
                                "Minimum required",Strings.nullToEmpty(minimumExpected)));
                return env;
            } else {
                throw error("x-v returned is not compatible",
                        args("Expected", Strings.nullToEmpty(highestExpected),
                                "Actual", Strings.nullToEmpty(actual),
                                "Minimum required",Strings.nullToEmpty(minimumExpected)));
            }
        }
}

    private boolean versionIsCompatible(String expected, String minimumExpected, String actual) {
        return !isVersionLesser(minimumExpected, actual) && !isVersionGreater(expected, actual);
    }

    private boolean isVersionLesser(String minVersion, String actualVersion) {
        String[] minParts = minVersion.split("\\.");
        String[] actualParts = actualVersion.split("\\.");

        for (int i = 0; i < 3; i++) {
            int minPart = Integer.parseInt(minParts[i]);
            int actualPart = Integer.parseInt(actualParts[i]);

            if (actualPart < minPart) {
                return true;
            } else if (actualPart > minPart) {
                return false;
            }
        }
        return false;
    }

    private boolean isVersionGreater(String requestedVersion, String receivedVersion) {
        String[] maxParts = requestedVersion.split("\\.");
        String[] actualParts = receivedVersion.split("\\.");

        for (int i = 0; i < 3; i++) {
            int maxPart = Integer.parseInt(maxParts[i]);
            int actualPart = Integer.parseInt(actualParts[i]);

            if (actualPart > maxPart) {
                return true;

            } else if (actualPart < maxPart) {
                return false;
            }
        }
        return false;
    }

}
