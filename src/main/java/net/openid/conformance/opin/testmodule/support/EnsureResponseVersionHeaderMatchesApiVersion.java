package net.openid.conformance.opin.testmodule.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.regex.Pattern;

public class EnsureResponseVersionHeaderMatchesApiVersion extends AbstractCondition {
    @Override
    @PreEnvironment(required = {"resource_endpoint_response_headers"}, strings = "apiVersion")
    public Environment evaluate(Environment env) {

        String responseVersion = env.getString("resource_endpoint_response_headers", "x-v");
        String apiVersion = env.getString("apiVersion");


        if (Strings.isNullOrEmpty(responseVersion)) {
            throw error("x-v header is missing from response");
        }else if (!Pattern.matches("^\\d+\\.\\d+\\.\\d+$",responseVersion)){
            throw error("x-v must be in the format: ^\\\\d+\\\\.\\\\d+\\\\.\\\\d+$", args("x-v returned:", Strings.nullToEmpty(responseVersion)));
        }else if (!isEqualOrGreater(apiVersion,responseVersion)){
            throw error("Response version is lesser than API version", args("test version", Strings.nullToEmpty(apiVersion), "response version", Strings.nullToEmpty(responseVersion)));
        } else {
            logSuccess("Response version is equal or greater than test version", args("x-v", Strings.nullToEmpty(responseVersion)));
            return env;
        }

    }

    private boolean isEqualOrGreater(String testVersion, String responseVersion) {
        String[] testParts = testVersion.split("\\.");
        String[] responseParts = responseVersion.split("\\.");

        for (int i = 0; i < 3; i++) {
            int testPart = Integer.parseInt(testParts[i]);
            int responsePart = Integer.parseInt(responseParts[i]);

            if (responsePart > testPart) {
                return true;
            } else if (responsePart < testPart) {
                return false;
            }
        }
        return true;
    }

}





