package net.openid.conformance.opin.testmodule.support.oasValidators.engineering.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Engineering 1.3.0")
public class GetEngineeringOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/engineering/v1n3/swagger-engineering-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/engineering";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}