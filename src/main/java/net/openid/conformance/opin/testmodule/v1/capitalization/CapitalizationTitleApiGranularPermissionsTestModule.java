package net.openid.conformance.opin.testmodule.v1.capitalization;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitleEventsOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitlePlansOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceCapitalizationTitle.v1n4.GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "capitalization-title_api_granular-permissions_test-module_v1",
        displayName = "Ensure specific methods can only be called if the respective permissions is granted.",
        summary = "Ensure specific methods can only be called if the respective permissions is granted.\n" +
                "• Call the POST Consents with the RESOURCES_READ, CAPITALIZATION_TITLE_READ and CAPITALIZATION_TITLE_PLANINFO_READ permissions\n" +
                "• Expects 201 -  Validate Response\n" +
                "• Redirects the user to Authorize consent\n" +
                "• Calls the GET Consents\n" +
                "• Expects 200 - Validate Response and Status is Authorized\n" +
                "• Calls GET Capitalization Title Plans Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} plan-info Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Calls GET Capitalization Title {planId} events Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Calls GET Capitalization Title {planId} settlements Endpoint\n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class CapitalizationTitleApiGranularPermissionsTestModule extends AbstractCapitalizationTitleApiGranularPermissionsTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceCapitalizationTitlePlansOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPlanInfoValidator() {
        return GetInsuranceCapitalizationTitlePlanInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getEventsValidator() {
        return GetInsuranceCapitalizationTitleEventsOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getSettlementsValidator() {
        return GetInsuranceCapitalizationTitleSettlementsOASValidatorV1n4.class;
    }
}
