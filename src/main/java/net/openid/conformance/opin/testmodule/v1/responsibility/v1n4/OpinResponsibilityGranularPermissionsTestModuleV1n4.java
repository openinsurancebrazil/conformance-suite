package net.openid.conformance.opin.testmodule.v1.responsibility.v1n4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.responsibility.AbstractOpinResponsibilityGranularPermissionsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-responsibility-api-granular-permissions-test-v1n4",
        displayName = "Ensure specific methods can only be called if the respective permissions is granted.",
        summary ="Ensure specific methods can only be called if the respective permissions is granted.\n" +
                "\u2022 Call the POST Consents with the RESOURCES_READ, READ and POLICY_INFO permissions\n" +
                "\u2022 Expects 201 - Validate response\n" +
                "\u2022 Redirects the user to Authorize consent\n" +
                "\u2022 Calls the GET Consents\n" +
                "\u2022 Expects 200 - Validate Response and Status is Authorized\n" +
                "\u2022 Call the GET Root Endpoint\n" +
                "\u2022 Expects 200 - Validate Response\n" +
                "\u2022 Call the GET Policy Info Endpoint\n" +
                "\u2022 Expects 200 - Validate Response\n" +
                "\u2022 Call the GET Premium Endpoint\n" +
                "\u2022 Expects a 403 response  - Validate error response \n" +
                "\u2022 Call the GET Claim Endpoint\n" +
                "\u2022 Expects a 403 response  - Validate error response ",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})
public class OpinResponsibilityGranularPermissionsTestModuleV1n4 extends AbstractOpinResponsibilityGranularPermissionsTest {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getRootValidator(){
        return GetInsuranceResponsibilityListOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator(){
        return GetInsuranceResponsibilityPolicyInfoOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator(){
        return GetInsuranceResponsibilityPremiumOASValidatorV1n4.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator(){
        return GetInsuranceResponsibilityClaimOASValidatorV1n4.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }
}

