package net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.condominium;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Quote Patrimonial 1.9.0")
public class GetPatrimonialCondominiumQuoteStatusOASValidatorV1n9 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quotePatrimonial/quote-patrimonial-1.9.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/condominium/request/{consentId}/quote-status";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}
