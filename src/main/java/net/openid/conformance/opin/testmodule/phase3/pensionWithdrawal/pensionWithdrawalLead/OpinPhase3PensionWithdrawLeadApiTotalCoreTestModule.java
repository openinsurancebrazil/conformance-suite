package net.openid.conformance.opin.testmodule.phase3.pensionWithdrawal.pensionWithdrawalLead;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.EnumWithdrawalType;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-pension-withdraw-lead_api_total-core_test-module_v1",
        displayName = "Validates the structure of Pension Withdrawal Lead API",
        summary = """
                Ensure a life pension withdrawal Lead request can be successfully executed with withdrawalType as 1_TOTAL
                
                · Execute a Customer Data Sharing Journey for the Life Pension Product, obtaining the first productName and certificateId from the insurance-life-pension/contracts endpoint, and the corresponding data.suseps.FIE.pmbacAmount from the contract-info endpoint
                · Call the POST Consents with PENSION_WITHDRAWAL_LEAD_CREATE permission, sending the withdrawalLifePensionInformation with the pre-saved information, withdrawalType as 1_TOTAL, and not sending the desiredTotalAmount field
                · Expect a 201 - Validate the response
                · Redirect the User to Authorize theConsent
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response and ensure the status is "AUTHORIZED"
                · Call the POST lead/request endpoint, sending the same information as the consent request
                · Expect a 201 - Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3PensionWithdrawLeadApiTotalCoreTestModule extends AbstractOpinPhase3PensionWithdrawLeadHappyPathTestModule {

    @Override
    protected void configureClient() {
        super.configureClient();
        env.putString("withdrawalType", EnumWithdrawalType.TOTAL.toString());
    }

}
