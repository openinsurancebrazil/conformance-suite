package net.openid.conformance.opin.testmodule.v1.financialAssistance;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;

public abstract class AbstractFinancialAssistanceApiWrongPermissionsTest extends AbstractOpinWrongPermissionsTestModuleV2 {

    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getContractInfoValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getMovementsValidator();

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddFinancialAssistanceScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceFinancialAssistanceUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.FINANCIAL_ASSISTANCE;
    }

    @Override
    protected String getApi() {
        return "insurance-financial-assistance";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].contracts[0].contractId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "contract-info", getContractInfoValidator(),
                "movements", getMovementsValidator()
        );
    }
}
