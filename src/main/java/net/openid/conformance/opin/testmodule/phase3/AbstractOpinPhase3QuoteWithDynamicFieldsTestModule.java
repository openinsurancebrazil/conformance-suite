package net.openid.conformance.opin.testmodule.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.condition.client.SetResourceMethodToPost;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.AddDynamicFieldsToQuoteRequest;
import net.openid.conformance.opin.testmodule.support.AddDynamicFieldsUserToConfig;
import net.openid.conformance.opin.testmodule.support.ExtractDynamicFields;
import net.openid.conformance.opin.testmodule.support.directory.GetQuoteResourceV1Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.capitalizationTitle.v1n3.GetDynamicFieldsCapitalizationTitleOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.opin.testmodule.support.oasValidators.dynamicFields.damageAndPerson.v1n3.GetDynamicFieldsDamageAndPersonOASValidatorV1n3;
import org.apache.commons.lang3.StringUtils;

public abstract class AbstractOpinPhase3QuoteWithDynamicFieldsTestModule extends AbstractOpinPhase3QuoteTestModule {

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        if (
                StringUtils.isBlank(env.getString("config", "resource.brazilCpfDynamicFields"))
                && StringUtils.isBlank(env.getString("config", "resource.brazilCnpjDynamicFields"))
        ) {
            fireTestSkipped("Test skipped since no 'Conditional - CPF for Dynamic Fields' or 'Conditional - CNPJ for Dynamic Fields' was informed.");
        }
        callAndStopOnFailure(AddDynamicFieldsUserToConfig.class);
        super.preConfigure(config, baseUrl, externalUrlOverride);
        ScopesAndPermissionsBuilder builder = new ScopesAndPermissionsBuilder(env,eventLog);
        builder.addScopes(getScope(), OPINScopesEnum.DYNAMIC_FIELDS);
        builder.build();
    }

    @Override
    protected void runTests() {
        env.putString("api_family_type", "dynamic-fields");
        env.putString("api_name", "dynamic-fields");
        env.putString("api_endpoint", "damage-and-person");
        callAndStopOnFailure(GetQuoteResourceV1Endpoint.class);

        this.getDynamicFields();

        super.runTests();
    }

    protected void getDynamicFields() {
        runInBlock("Get dynamic fields", () -> {

            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            call(setHeadersSequence());
            callAndStopOnFailure(SetResourceMethodToGet.class);
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
            callAndStopOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            getDynamicFieldsValidator();
            env.putString("dynamic_field_api_type", getDynamicFieldApiType());
            callAndContinueOnFailure(ExtractDynamicFields.class, Condition.ConditionResult.FAILURE);
        });
    }

    protected void getDynamicFieldsValidator() {
        callAndContinueOnFailure(GetDynamicFieldsDamageAndPersonOASValidatorV1n3.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void postQuote() {
        runInBlock("Create a quote", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
            call(setHeadersSequence());
            call(setIdempotencyKey());
            callAndStopOnFailure(createPostQuoteRequestBody());
            callAndStopOnFailure(AddDynamicFieldsToQuoteRequest.class);
            callAndStopOnFailure(SetContentTypeApplicationJson.class);
            callAndStopOnFailure(SetResourceMethodToPost.class);
            callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        });
        runInBlock("Validate the quote creation response", () -> {
            callAndStopOnFailure(EnsureResourceResponseCodeWas201.class);
            callAndContinueOnFailure(ensurePostQuoteStatus(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(validatePostQuoteResponse(), Condition.ConditionResult.FAILURE);
        });
    }

    protected abstract String getDynamicFieldApiType();
}
