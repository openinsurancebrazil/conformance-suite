package net.openid.conformance.opin.testmodule.v1.customers.v1n5;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersPersonalComplimentaryInformationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersPersonalIdentificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersPersonalQualificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.v1.customers.AbstractOpinCustomerPersonalWrongPermissionsTest;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-customer-personal-api-wrong-permissions-test",
	displayName = "Ensures API resource cannot be called with wrong permissions",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a Consent with the customer personal permissions ( \"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\",”CUSTOMERS_PERSONAL_QUALIFICATION_READ”\"CUSTOMERS_PERSONAL_ADDITIONALINFO_READ\",\"RESOURCES_READ”)\n" +
		"\u2022 Expects a success 201 - Checks all of the fields sent on the consent API are specification compliant\n" +
		"\u2022 Calls GET Personal Qualifications resources V2\n" +
		"\u2022 Expects a success 200 - Validate Response\n" +
		"\u2022 Creates a POST Consent with all the permissions but the Customer\n" +
		"\u2022 Expects a success 201 - Validate Response\n" +
		"\u2022 Calls GET Personal Identifications Endpoint\n" +
		"\u2022 Expects a 403 - Validate Error\n" +
		"\u2022 Calls GET Personal Qualifications Endpoint\n" +
		"\u2022 Expects a 403 - Validate Error\n" +
		"\u2022 Calls GET Personal Complimentary-Information Endpoint\n" +
		"\u2022 Expects a 403 - Validate Error\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks","consent.productType","resource.brazilCnpj"
})
public class OpinCustomerPersonalWrongPermissionsTestModule extends AbstractOpinCustomerPersonalWrongPermissionsTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalIdentificationValidator(){
		return GetCustomersPersonalIdentificationListOASValidatorV1n5.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalQualificationValidator(){
		return GetCustomersPersonalQualificationListOASValidatorV1n5.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerPersonalComplimentaryInfoValidator(){
		return GetCustomersPersonalComplimentaryInformationListOASValidatorV1n5.class;
	}
}
