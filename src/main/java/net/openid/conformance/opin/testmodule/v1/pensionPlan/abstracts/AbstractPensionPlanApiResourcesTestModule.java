package net.openid.conformance.opin.testmodule.v1.pensionPlan.abstracts;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;


public abstract class AbstractPensionPlanApiResourcesTestModule extends AbstractOpinApiResourcesTestModuleV2 {
    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddPensionPlanScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsurancePensionPlanUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.PENSION_PLAN;
    }

    @Override
    protected String getApi() {
        return "insurance-pension-plan";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[*].brand.companies[*].contracts[*].pensionIdentification");
        return AllIdsSelectorFromJsonPath.class;
    }

    @Override
    protected String getResourceType() {
        return EnumOpinResourcesType.PENSION_PLAN.name();
    }

    @Override
    protected String getResourceStatus() {
        return EnumResourcesStatus.AVAILABLE.name();
    }
}
