package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialAssistance.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Financial Assistance 1.3.0")
public class GetInsuranceFinancialAssistanceMovementsOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceFinancialAssistance/v1/swagger-insurance-financial-assistance-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-financial-assistance/{contractId}/movements";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
