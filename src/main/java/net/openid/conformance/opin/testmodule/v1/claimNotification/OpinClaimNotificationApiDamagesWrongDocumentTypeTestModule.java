package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1.PostClaimNotificationDamageOASValidatorV1;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "opin-claim-notification-api-damages-wrong-documentType-test",
    displayName = "Ensure that Claim notification for damages cannot be created with a valid documentType, but different from the one specified at Phase 2 APIs",
    summary = "Ensure that Claim notification for damages cannot be created with a valid documentType, but different from the one specified at Phase 2 APIs\n" +
        "\u2022 Call the POST Consents API with all the existing Phase 2 permissions\n" +
        "\u2022 Expects 201 - Validate Response \n" +
        "\u2022 Redirect the user to authorize consent\n" +
        "\u2022 Call the GET resources API\n" +
        "\u2022 Expects 200 - Validate Response - For damages expect one resource shared with one of the following types [DAMAGES_AND_PEOPLE_PATRIMONIAL, DAMAGES_AND_PEOPLE_RESPONSIBILITY, DAMAGES_AND_PEOPLE_TRANSPORT, DAMAGES_AND_PEOPLE_FINANCIAL_RISKS, DAMAGES_AND_PEOPLE_RURAL, DAMAGES_AND_PEOPLE_AUTO, DAMAGES_AND_PEOPLE_HOUSING, DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD]\n" +
        "\u2022 Select one of the returned resources and save the policyID\n" +
        "\u2022 Call the GET Shared API policy-info endpoint with the extracted resourceId, where the API to be called is to be determined by the Type of the extracted policyID\n" +
        "\u2022 Expect a 200 - Extract the field data.documentType from the response_body\n" +
        "\u2022 Call the DELETE Consents Endpoint for tha created Consent\n" +
        "\u2022 Call the POST Consents Endpoint with Claim Notification permissions and the extracted policyID\n" +
        "\u2022 Expects 201 - Validate Response\n" +
        "\u2022 Redirect the user to authorize consent - Expect successful authorization and obtain a token via authorization_code grant\n" +
        "\u2022 Call the GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"AUTHORIZED\"\n" +
        "\u2022 Prepare the POST Claim request_body with the following payload\n" +
        "— If the extracted documentType is equal to \"CERTIFICADO\" send groupCertificateId as the extracted policyID and don’t send policyNumber field. Else, send only the policyNumber which should be equal to extracted policyID\n" +
        "— Set documentType as the extracted documentType\n" +
        "— Set occurrenceDate as CurrentDateTime-1 and the occurrenceTime as the CurrentTime\n" +
        "\u2022 Call the POST Claim damages endpoint with the the pre-set request_body replacing one of the potential fields occurrenceDate, documentType, policyNumber with a valid documentType, but different from the one specified at Phase 2 APIs\n" +
        "\u2022 Expects 422 - Validate Error response\n" +
        "\u2022 Call The GET Consents Endpoint\n" +
        "\u2022 Expects 200 - Validate Response is \"CONSUMED\"",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "resource.consentUrl",
        "resource.brazilCpf",
        "consent.productType"
    }
)
public class OpinClaimNotificationApiDamagesWrongDocumentTypeTestModule extends AbstractOpinClaimNotificationApiDamagesWrongDocumentTypeTestModule {

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostClaimNotificationDamageOASValidatorV1();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }
}
