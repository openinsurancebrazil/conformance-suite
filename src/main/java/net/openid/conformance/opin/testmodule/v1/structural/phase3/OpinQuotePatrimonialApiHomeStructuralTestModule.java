package net.openid.conformance.opin.testmodule.v1.structural.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.QuotePatrimonialTypes;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.home.GetPatrimonialHomeQuoteStatusOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.home.PostPatrimonialHomeOASValidatorV1n9;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-quote-patrimonial-api-home-structural-test",
        displayName = "Validate structure of Quote Patrimonial API Home Endpoint successful response",
        summary = "Validate structure of Quote Patrimonial API Home Endpoint successful response\n" +
                "\u2022 Call the \"/home/request\" endpoint using GET method\n" +
                "\u2022 Expect 200 - validate response\n" +
                "\u2022 Call the \"/home/request/{consentId}/quote-status\" endpoint using GET method\n" +
                "\u2022 Expect 200 - validate response\n",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "resource.consentUrl",
                "resource.consentId"
        }
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})
public class OpinQuotePatrimonialApiHomeStructuralTestModule extends QuotePatrimonialStructuralTestAbstractClass {

    @Override
    protected QuotePatrimonialTypes quotePatrimonialType() {
        return QuotePatrimonialTypes.HOME;
    }

    @Override
    protected Class<? extends Condition> validatorRoot() {
        return PostPatrimonialHomeOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends Condition> validatorQuoteStatus() {
        return GetPatrimonialHomeQuoteStatusOASValidatorV1n9.class;
    }
}
