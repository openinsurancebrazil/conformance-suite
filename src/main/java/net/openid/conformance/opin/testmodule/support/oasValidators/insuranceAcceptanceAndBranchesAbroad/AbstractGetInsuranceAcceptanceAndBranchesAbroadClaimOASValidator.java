package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetInsuranceAcceptanceAndBranchesAbroadClaimOASValidator extends OpenAPIJsonSchemaValidator{

	@Override
	protected String getEndpointPath() {
		return "/insurance-acceptance-and-branches-abroad/{policyId}/claim";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
