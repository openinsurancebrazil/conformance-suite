package net.openid.conformance.opin.testmodule.v1.customers.v1n5;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessIdentificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.support.oasValidators.customers.v1n5.GetCustomersBusinessQualificationListOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.v1.customers.AbstractOpinCustomerBusinessWrongPermissionsTest;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
	testName = "opin-customer-business-api-wrong-permissions-test",
	displayName = "Ensures API resource cannot be called with wrong permissions",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a Consent with the customer business permissions (\"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\",”CUSTOMERS_BUSINESS_QUALIFICATION_READ”, \"CUSTOMERS_BUSINESS_ADDITIONALINFO_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects a success 201 - Check all of the fields sent on the consent API is spec compliant \n" +
		"\u2022 Calls GET Business Qualifications resource\n" +
		"\u2022 Expects a success 200 - Validate Response\n" +
		"\u2022 Creates a POST Consent with all the permissions but the Customer ones \n" +
		"\u2022 Expects a success 201 - Validate Response\n" +
		"\u2022 Calls GET Business Identifications Endpoint\n" +
		"\u2022 Expects a 403 - Validate Error\n" +
		"\u2022 Calls GET Business Qualifications Endpoint\n" +
		"\u2022 Expects a 403 - Validate Error\n" +
		"\u2022 Calls GET Business Complimentary-Information Endpoint\n" +
		"\u2022 Expects a 403 - Validate Error\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinCustomerBusinessWrongPermissionsTestModule extends AbstractOpinCustomerBusinessWrongPermissionsTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessIdentificationValidator(){
		return GetCustomersBusinessIdentificationListOASValidatorV1n5.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessQualificationValidator(){
		return GetCustomersBusinessQualificationListOASValidatorV1n5.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessComplimentaryInfoValidator(){
		return GetCustomersBusinessComplimentaryInformationListOASValidatorV1n5.class;
	}
}
