package net.openid.conformance.opin.testmodule.phase3.contractLifePension;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteWithDynamicFieldsTestModule;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAckn;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAcpt;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateContractLifePensionPostRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchAcknRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;


public abstract class AbstractOpinContractLifePensionDynamicFieldsTestModule extends AbstractOpinPhase3QuoteWithDynamicFieldsTestModule {
    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.CONTRACT_LIFE_PENSION;
    }

    @Override
    protected String getApiFamilyType() {
        return "contract-life-pension";
    }

    @Override
    protected String getApiName() {
        return "contract-life-pension";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateContractLifePensionPostRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }


    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchAcknRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasAckn.class;
    }

    @Override
    protected String getDynamicFieldApiType() {
        return "CONTRACT_LIFE_PENSION";
    }
}