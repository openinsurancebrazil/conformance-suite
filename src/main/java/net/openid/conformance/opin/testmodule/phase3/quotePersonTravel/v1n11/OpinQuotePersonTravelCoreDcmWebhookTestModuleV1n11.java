package net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.v1n11;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonTravel.AbstractOpinQuotePersonTravelCoreDcmDcrWebhookTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel.GetPersonTravelQuoteStatusOASValidatorV1n11;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel.PatchPersonTravelOASValidatorV1n11;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson.v1n11.travel.PostPersonTravelOASValidatorV1n11;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePersonTravelPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-person-travel_api_core-webhook-dcm_test-module_v1.11.0",
        displayName = "Ensure that a Person quotation request can be successfully created and accepted afterwards, and webhook is sent as required if actioned through a DCM.",
        summary = """
            Ensure that a Person quotation request can be successfully created and accepted afterwards, and webhook is sent as required if actioned through a DCM. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>.
            • Obtain a SSA from the Directory
            • Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration
            • Call the Registration Endpoint without sending the "webhook_uris" field.
            • Expect a 201 - Ensure that the "webhook_uris" is not returned on the response body
            • Call the PUT Registration Endpoint sending the field "webhook_uris":["https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>"]
            • Expect a 201 - Validate Response
            • Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds
            • Set the consents webhook notification endpoint to be equal to https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>/open-insurance/webhook/v1/quote/v1/request/{consentId}/quote-status, where the alias is to be obtained from the field alias on the test configuration
            • Call POST travel/request endpoint sending personal or business information, following what is defined at the config, and the consentId as the webhook defined on the test
            • Expect 201 - Validate Response and ensure status is RCVD
            • Wait for webhook incoming call until 1 minute and fail the test if not received
            • Respond 202 with an empty body, validate if timestamp is after the POST travel/request call
            • Call GET travel/request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Respones and ensure status is ACPT
            • Call PATCH travel/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
            • Expect 200 - Validate Response and ensure status is ACKN
            • Call GET links.redirect endpoint
            • Expect 200
            • Call the Delete Registration Endpoint
            • Expect a 204 - No Content
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "mtls.ca",
                "directory.client_id",
                "directory.discoveryUrl",
                "directory.apibase",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePersonTravelCoreDcmWebhookTestModuleV1n11 extends AbstractOpinQuotePersonTravelCoreDcmDcrWebhookTestModule {
    @Override
    protected boolean shouldAlsoDoDCM() {
        return true;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPersonTravelOASValidatorV1n11.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPersonTravelQuoteStatusOASValidatorV1n11.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPersonTravelOASValidatorV1n11.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePersonTravelPostRequestBodyWithCoverage.class;
    }
}