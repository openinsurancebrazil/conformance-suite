package net.openid.conformance.opin.testmodule.v1.claimNotification;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.claimNotification.v1.PostClaimNotificationPersonOASValidatorV1;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-claim-notification-person-api_idempotency_test",
        displayName = "Ensure that after an initial claim notification request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idempotency key the request fails",
        summary = """
                Ensure that after an initial claim notification request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idempotency key the request fails
                - Call the POST Consents API with damages and people person permissions
                - Expects 201 - Validate Response
                - Redirect the user to authorize consent
                - Call the GET resources API
                - Expects 200 - Validate Response
                - Call the GET Shared API policy-info endpoint with the extracted resourceId, where the API to be called is to be determined by the Type of the extracted policyID
                - Expect a 200 - Extract the field data.documentType from the response_body
                - Call the POST Consents Endpoint with Claim Notification permissions
                - Expects 201 - Validate Response
                - Redirect the user to authorize consent
                - Call the GET Consents Endpoint
                - Expects 200 - Validate Response is "AUTHORISED"
                - Call the POST Claim Notification at the person endpoint
                - Expects 201 - Validate Response
                - Call the  POST Claim Notification Endpoint with the same payload and idempotency id
                - Expects 201 - Validate Response
                - Call the POST Claim Notification, with a different payload as the previous request but the same idempotency id
                - Expects 422 ERRO_IDEMPOTENCIA - Validate Response
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinClaimNotificationApiPersonIdempotencyTestModule extends AbstractOpinClaimNotificationApiPersonIdempotencyTestModule {
    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostClaimNotificationPersonOASValidatorV1();
    }

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }
}
