package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractEditQuoteLeadPostRequestBodyWithCoverage extends AbstractCreateQuotePostRequestBody {

    protected abstract String getCode();
    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject quoteCustomer = data.getAsJsonObject("quoteCustomer");
        JsonObject identificationData = quoteCustomer.getAsJsonObject("identificationData");
        identificationData.addProperty("brandName", "Different Brand Name");

        JsonObject quoteData = new JsonObject();

        JsonArray coverages = new JsonArray();
        JsonObject coverage = new JsonObject();

        coverage.addProperty("branch", "0111");
        coverage.addProperty("code", getCode());
        coverage.addProperty("description", "string");

        coverages.add(coverage);

        quoteData.add("coverages", coverages);

        data.add("quoteData", quoteData);
    }
}