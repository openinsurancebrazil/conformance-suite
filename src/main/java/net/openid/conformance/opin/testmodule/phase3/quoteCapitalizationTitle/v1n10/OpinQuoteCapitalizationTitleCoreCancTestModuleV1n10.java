package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle.AbstractOpinQuoteCapitalizationTitleCore;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasCanc;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10.GetQuoteCapitalizationTitleOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10.PatchQuoteCapitalizationTitleOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle.v1n10.PostQuoteCapitalizationTitleOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPaymentWithUnitType;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchCancRequestBody;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-capitalization-title_api_core-canc_test-module_v1.10.0",
        displayName = "opin-quote-capitalization-title_api_core-canc_test-module_v1",
        summary = """
            Ensure that a  Capitalization Title quotation request can be successfully created and rejected afterwards. This test applies to both personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            • Call POST /request endpoint sending personal or business information, following what is defined at the config, and sending paymentType as MENSAL, and monthlyPayment field at the request
            • Expect 201 - Validate Response and ensure status is RCVD
            • Poll the GET request/{consentId}/quote-status  endpoint while status is RCVD or EVAL
            • Call GET request/{consentId}/quote-status endpoint
            • Expect 200 - Validate Respones and ensure status is ACPT
            • Call PATCH request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC
            • Expect 200 - Validate Response and ensure status is CANC
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteCapitalizationTitleCoreCancTestModuleV1n10 extends AbstractOpinQuoteCapitalizationTitleCore {

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuoteCapitalizationTitlePostRequestBodyMonthlyPaymentWithUnitType.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostQuoteCapitalizationTitleOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetQuoteCapitalizationTitleOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchCancRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasCanc.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchQuoteCapitalizationTitleOASValidatorV1n10.class;
    }
}