package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreateQuoteAutoPostRequestBodyWithCoverage extends AbstractCreateQuotePostRequestBody {

    @Override
    protected String getCode(){
        return "CASCO_COMPREENSIVA";
    }

    @Override
    protected void editData(Environment env, JsonObject data) {
        JsonObject quoteData = data.getAsJsonObject("quoteData");
        quoteData.remove("maxLMG");
        quoteData.remove("policyId");
        quoteData.remove("insurerId");
        quoteData.addProperty("termType", "ANUAL");

        JsonArray coverages = getCoverages();

        quoteData.add("coverages", coverages);
    }
}
