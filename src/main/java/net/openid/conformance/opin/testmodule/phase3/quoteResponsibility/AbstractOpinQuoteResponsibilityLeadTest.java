package net.openid.conformance.opin.testmodule.phase3.quoteResponsibility;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteResponsibilityLeadTest extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_RESPONSIBILITY_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-responsibility";
    }

    @Override
    protected String getApiName() {
        return "quote-responsibility";
    }
}
