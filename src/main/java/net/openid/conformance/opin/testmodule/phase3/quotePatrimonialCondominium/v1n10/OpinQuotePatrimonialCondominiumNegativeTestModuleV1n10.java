package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium.AbstractOpinQuotePatrimonialCondominiumNegativeTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.condominium.GetPatrimonialCondominiumQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-condominium_api_negative-quote_test-module_v1n10",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = "Ensure a consent cannot be created in unhappy requests.\n" +
                "\u2022 Call POST condominium/request endpoint sending personal or business information, following what is defined at the config, and not send the consentId on request payload\n" +
                "\u2022 Expect 400 or 422 - Validate Error Response\n" +
                "\u2022 Call POST condominium/request endpoint sending personal or business information, following what is defined at the config, using a client credentials token with \"quote-patrimonial-lead\" scope\n" +
                "\u2022 Expect 403 - Validate Error Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialCondominiumNegativeTestModuleV1n10 extends AbstractOpinQuotePatrimonialCondominiumNegativeTest {

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> quoteValidator() {
        return GetPatrimonialCondominiumQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage.class;
    }
}
