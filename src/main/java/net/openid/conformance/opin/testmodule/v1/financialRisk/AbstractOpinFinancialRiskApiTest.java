package net.openid.conformance.opin.testmodule.v1.financialRisk;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceFinancialRisk.v1n3.GetInsuranceFinancialRiskPremiumOASValidatorV1n3;

import java.util.Map;

public abstract class AbstractOpinFinancialRiskApiTest extends AbstractOpinApiTestModuleV2 {

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator();

	@Override
	protected PermissionsGroup getPermissionsGroup() {
		return PermissionsGroup.DAMAGES_AND_PEOPLE_FINANCIAL_RISKS;
	}

	@Override
	protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
		return AddFinancialRiskScope.class;
	}

	@Override
	protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
		return BuildFinancialRiskConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected String getApi() {
		return "insurance-financial-risk";
	}

	@Override
	protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
		return Map.of(
				"policy-info", getPolicyInfoValidator(),
				"premium", getPremiumValidator(),
				"claim", getClaimValidator()
		);
	}

}
