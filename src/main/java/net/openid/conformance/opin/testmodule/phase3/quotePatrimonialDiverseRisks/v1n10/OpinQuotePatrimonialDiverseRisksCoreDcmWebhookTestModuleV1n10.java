package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks.AbstractOpinQuotePatrimonialDiverseRisksCoreWebhookTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.PatchPatrimonialDiverseRisksOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.PostPatrimonialDiverseRisksOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialDiverseRisksPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-diverse-risks_api_core-webhook-dcm_test-module_v1n10",
        displayName = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and accepted afterwards, and webhook is sent as required if actioned through a DCM.",
        summary = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and accepted afterwards, and webhook is sent as required if actioned through a DCM. This test applies to both Personal and diverse-risks products. If the \"brazilCnpj\" field is filled out, it will transmit diverse-risks information; otherwise, personal information will be used.\n" +
                "For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>.\n" +
                "\u2022 Obtain a SSA from the Directory\n" +
                "\u2022 Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration\n" +
                "\u2022 Call the Registration Endpoint without sending the \"webhook_uris\" field.\n" +
                "\u2022 Expect a 201 - Ensure that the \"webhook_uris\" is not returned on the response body\n" +
                "\u2022 Call the PUT Registration Endpoint sending the field \"webhook_uris\":[\"https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>\"]\n" +
                "\u2022 Expect a 201 - Validate Response\n" +
                "\u2022 Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
                "\u2022 Set the consents webhook notification endpoint to be equal to https://web.conformance.directory.opinbrasil.com.br/test-mtls/a/<alias>/open-insurance/webhook/v1/quote/v1/request/{consentId}/quote-status, where the alias is to be obtained from the field alias on the test configuration\n" +
                "\u2022 Call POST diverse-risks/request endpoint sending personal or diverse-risks information, following what is defined at the config, and the consentId as the webhook defined on the test\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Wait for webhook incoming call until 1 minute and fail the test if not received\n" +
                "\u2022 Respond 202 with an empty body, validate if timestamp is after the POST diverse-risks/request call\n" +
                "\u2022 Call GET diverse-risks/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respones and ensure status is ACPT\n" +
                "\u2022 Call PATCH diverse-risks/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN\n" +
                "\u2022 Expect 200 - Validate Response and ensure status is ACKN\n" +
                "\u2022 Call GET links.redirect endpoint\n" +
                "\u2022 Expect 200\n" +
                "\u2022 Call the Delete Registration Endpoint\n" +
                "\u2022 Expect a 204 - No Content\n",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "mtls.ca",
                "directory.client_id",
                "directory.discoveryUrl",
                "directory.apibase",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialDiverseRisksCoreDcmWebhookTestModuleV1n10 extends AbstractOpinQuotePatrimonialDiverseRisksCoreWebhookTest {

    @Override
    protected boolean shouldAlsoDoDCM() {
        return true;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialDiverseRisksOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialDiverseRisksOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialDiverseRisksPostRequestBodyWithCoverage.class;
    }
}
