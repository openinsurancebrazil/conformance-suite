package net.openid.conformance.opin.testmodule.support;

public class BuildResponsibilityConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

	@Override
	protected String getApiReplacement() {
		return "insurance-responsibility";
	}

	@Override
	protected String getEndpointReplacement() {
		return "insurance-responsibility";
	}
}
