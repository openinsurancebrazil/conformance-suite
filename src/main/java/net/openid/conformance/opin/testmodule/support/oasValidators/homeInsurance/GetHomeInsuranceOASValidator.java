package net.openid.conformance.opin.testmodule.support.oasValidators.homeInsurance;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("ProductsNServices Home Insurance 1.4.0")
public class GetHomeInsuranceOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/productsServices/swagger-home-insurance-1.4.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/home-insurance/commercializationArea/{commercializationArea}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}