package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceAcceptanceAndBranchesAbroad.AbstractGetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidator;

@ApiName("Insurance Acceptance and Branches Abroad 1.4.0")
public class GetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidatorV1n4 extends AbstractGetInsuranceAcceptanceAndBranchesAbroadPolicyInfoOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceAcceptanceAndBranchesAbroad/v1/v1n4/swagger-insurance-acceptance-and-branches-abroad-1.4.0.yaml";
	}
}
