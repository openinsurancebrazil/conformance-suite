package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.abstracts;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;


public abstract class AbstractOpinAcceptanceAndBranchesAbroadApiTestModule extends AbstractOpinApiTestModuleV2 {


	@Override
	protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
		return AddAcceptanceBranchesAbroadScope.class;
	}

	@Override
	protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
		return BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected PermissionsGroup getPermissionsGroup() {
		return PermissionsGroup.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD;
	}

	@Override
	protected String getApi() {
		return "insurance-acceptance-and-branches-abroad";
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
		return IdSelectorFromJsonPath.class;
	}

	@Override
	protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
		return Map.of(
				"policy-info", getPolicyInfoValidator(),
				"premium", getPremiumValidator(),
				"claim", getClaimValidator()

		);
	}
	protected abstract Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getPremiumValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getClaimValidator();
}
