package net.openid.conformance.opin.testmodule.support.oasValidators.errorsOmissionsLiability.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Errors Omissions Liability 1.3.0")
public class GetErrorsOmissionsLiabilityOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/errorsOmissionsLiability/v1n3/swagger-errors-omissions-liability-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/errors-omissions-liability";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}