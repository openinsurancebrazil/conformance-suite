package net.openid.conformance.opin.testmodule.support;

import java.util.HashMap;
import java.util.Map;

public class OPINApiVersions {

    private final Map<String, String> apiVersions;

    public OPINApiVersions() {
        apiVersions = new HashMap<>();
        // phase 1 apis
        apiVersions.put("products-services", "v1");
        // phase 2 apis
        apiVersions.put("insurance-acceptance-and-branches-abroad", "v1");
        apiVersions.put("insurance-auto", "v1");
        apiVersions.put("insurance-financial-risk", "v1");
        apiVersions.put("insurance-housing", "v1");
        apiVersions.put("insurance-patrimonial", "v1");
        apiVersions.put("insurance-responsibility", "v1");
        apiVersions.put("insurance-rural", "v1");
        apiVersions.put("insurance-transport", "v1");
        apiVersions.put("resources", "v2");
        apiVersions.put("consents", "v2");
        apiVersions.put("customers", "v1");
        apiVersions.put("insurance-customer-business", "v1");
        apiVersions.put("insurance-customer-personal", "v1");

        apiVersions.put("insurance-capitalization-title", "v1");
        apiVersions.put("insurance-person", "v1");
        apiVersions.put("insurance-financial-assistance", "v1");
        apiVersions.put("insurance-life-pension", "v1");
        apiVersions.put("insurance-pension-plan", "v1");
        // phase 3 apis
        apiVersions.put("claim-notification", "v1");
        apiVersions.put("endorsement", "v1");
        apiVersions.put("quote-patrimonial", "v1");
        apiVersions.put("withdrawal", "v1");
        apiVersions.put("quote-capitalization-title", "v1");

    }

    public String getApiVersion(String api) {
        return apiVersions.get(api);
    }
}
