package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

public class SetEndorsementPolicyIdToInvalid extends AbstractSetEndorsementPolicyIdToInvalid {

    protected String getPolicyFieldName(){
        return "policyNumber";
    }
}
