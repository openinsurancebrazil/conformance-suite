package net.openid.conformance.opin.testmodule.support.oasValidators.quoteRural.v1n8;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Quote Rural V1.8.0")
public class PostQuoteRuralLeadOASValidatorV1n8 extends OpenAPIJsonSchemaValidator {
    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/quoteRural/quote-rural-v1.8.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/lead/request";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.POST;
    }
}
