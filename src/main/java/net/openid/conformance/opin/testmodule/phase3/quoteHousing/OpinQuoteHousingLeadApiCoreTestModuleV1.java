package net.openid.conformance.opin.testmodule.phase3.quoteHousing;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteHousing.v1n8.PatchQuoteHousingLeadOASValidatorV1n8;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteHousing.v1n8.PostQuoteHousingLeadOASValidatorV1n8;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-housing-lead_api_core_test-module_v1",
        displayName = "Ensure that a Housing Lead request can be successfully created and deleted afterwards.",
        summary = """
            Ensure that a Housing Lead request can be successfully created and deleted afterwards. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
            · Call POST lead/request endpoint sending personal or business information, following what is defined at the config
            · Expect 201 - Validate Response and ensure status is RCVD
            · Call the POST lead/request Endpoint with the same payload and idempotency id
            · Expects 201 - Validate Response
            · Call the POST lead/request, with a different payload as the previous request but the same idempotency id
            · Expects 422 ERRO_IDEMPOTENCIA - Validate Response
            · Call PATCH lead/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteHousingLeadApiCoreTestModuleV1 extends AbstractOpinQuoteHousingLeadApiCoreTest{

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteLeadResponse() {
        return PostQuoteHousingLeadOASValidatorV1n8.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteLeadResponse() {
        return PatchQuoteHousingLeadOASValidatorV1n8.class;
    }
}