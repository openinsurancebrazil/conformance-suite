package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class EditQuoteHousingLeadPostRequestBodyWithCoverage extends AbstractEditQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "DANOS_ELETRICOS";
    }
}