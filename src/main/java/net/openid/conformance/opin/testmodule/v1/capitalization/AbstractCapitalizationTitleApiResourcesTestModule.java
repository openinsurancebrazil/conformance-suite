package net.openid.conformance.opin.testmodule.v1.capitalization;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.opin.testmodule.AbstractOpinApiResourcesTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;


public abstract class AbstractCapitalizationTitleApiResourcesTestModule extends AbstractOpinApiResourcesTestModuleV2 {

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddCapitalizationTitleScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceCapitalizationTitleUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.CAPITALIZATION_TITLE;

    }

    @Override
    protected String getApi() {
        return "insurance-capitalization-title";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        return AllPlanIdsSelector.class;
    }

    @Override
    protected String getResourceType() {
        return EnumOpinResourcesType.CAPITALIZATION_TITLES.name();
    }

    @Override
    protected String getResourceStatus() {
        return EnumResourcesStatus.AVAILABLE.name();
    }
}
