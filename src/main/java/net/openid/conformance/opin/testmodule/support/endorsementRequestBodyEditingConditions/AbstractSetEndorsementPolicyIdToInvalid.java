package net.openid.conformance.opin.testmodule.support.endorsementRequestBodyEditingConditions;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.OPINPhase3Keys;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractSetEndorsementPolicyIdToInvalid extends AbstractCondition {

    @Override
    @PreEnvironment(required = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject data = env.getElementFromObject("resource_request_entity", "data").getAsJsonObject();
        data.addProperty(getPolicyFieldName(), OPINPhase3Keys.KEYS_INVALID_POLICY_ID);
        logSuccess(getPolicyFieldName() + " changed to an invalid value", args("data", data));
        return env;
    }

    protected abstract String getPolicyFieldName();
}
