package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;

public class SetProtectedResourceUrlAsLinksRedirect extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    public Environment evaluate(Environment env) {
        JsonObject responseBody = null;
        try {
            responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }

        JsonElement data = responseBody.get("data");
        if(data == null) {
            throw error("The response body doesn't contain data");
        }
        if(data.getAsJsonObject().has("redirectLink")){
            env.putString("protected_resource_url", data.getAsJsonObject().get("redirectLink").getAsString());
            return env;
        }else{
            env.putString("protected_resource_url", data.getAsJsonObject().getAsJsonObject("links").get("redirect").getAsString());
            return env;
        }
    }
}
