package net.openid.conformance.opin.testmodule.phase3.quoteAuto.v1n9;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteAuto.AbstractOpinQuoteAutoApiCore;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.GetQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.PatchQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteAuto.v1n9.PostQuoteAutoOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuoteAutoPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-auto_api_core-ackn_test-module_v1.9.0",
        displayName = "Ensure that a Auto quotation request can be successfully created and accepted afterwards.",
        summary = """
                Ensure that a Auto quotation request can be successfully created and accepted afterwards. This test applies to both Personal and Business products. If the "brazilCnpj" field is filled out, it will transmit business information; otherwise, personal information will be used.
                · Call POST auto request endpoint sending personal or business information, following what is defined at the config
                · Expect 201 - Validate Response and ensure status is RCVD
                · Poll the GET  quote-status endpoint while status is RCVD or EVAL
                · Call GET  quote-status endpoint
                · Expect 200 - Validate Respones and ensure status is ACPT
                · Call PATCH auto request endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN
                · Expect 200 - Validate Response and ensure status is ACKN
                · Call GET links.redirect endpoint
                · Expect 200
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuoteAutoApiCoreAcknTestModuleV1n9 extends AbstractOpinQuoteAutoApiCore {

        @Override
        protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
                return PostQuoteAutoOASValidatorV1n9.class;
        }

        @Override
        protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
                return GetQuoteAutoOASValidatorV1n9.class;
        }

        @Override
        protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
                return PatchQuoteAutoOASValidatorV1n9.class;
        }

        @Override
        protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
                return CreateQuoteAutoPostRequestBodyWithCoverage.class;
        }
}
