package net.openid.conformance.opin.testmodule.support;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.*;

import static net.openid.conformance.opin.testmodule.support.PermissionsGroup.*;

public class OpinConsentEndpointResponseValidatePermissions extends AbstractCondition {

	private static final String[] allPersonal = ALL_PERSONAL_PHASE2.getPermissions();
	private static final String[] allBusiness = ALL_BUSINESS_PHASE2.getPermissions();
	private static final String[] customerBusiness = CUSTOMERS_BUSINESS.getPermissions();
	private static final String[] customerPersonal = CUSTOMERS_PERSONAL.getPermissions();
	private static final String[] damagesAndPeoplePatrimonial = DAMAGES_AND_PEOPLE_PATRIMONIAL.getPermissions();
	private static final String[] damagesAndPeopleResponsibility = DAMAGES_AND_PEOPLE_RESPONSIBILITY.getPermissions();
	private static final String[] damagesAndPeopleTransport = DAMAGES_AND_PEOPLE_TRANSPORT.getPermissions();;
	private static final String[] damagesAndPeopleFinancialRisks = DAMAGES_AND_PEOPLE_FINANCIAL_RISKS.getPermissions();
	private static final String[] damagesAndPeopleRural = DAMAGES_AND_PEOPLE_RURAL.getPermissions();
	private static final String[] damagesAndPeopleAuto = DAMAGES_AND_PEOPLE_AUTO.getPermissions();
	private static final String[] damagesAndPeopleHousing = DAMAGES_AND_PEOPLE_HOUSING.getPermissions();
	private static final String[] damagesAndPeopleAcceptanceAndBranchesAbroad = DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD.getPermissions();
	private static final String[] damagesAndPeoplePerson = DAMAGES_AND_PEOPLE_PERSON.getPermissions();
	private static final String[] capitalizationTitle = CAPITALIZATION_TITLE.getPermissions();
	private static final String[] claimNotificationRequest = CLAIM_NOTIFICATION_REQUEST.getPermissions();
	private static final String[] pensionPlan = PENSION_PLAN.getPermissions();
	private static final String[] lifePension = LIFE_PENSION.getPermissions();
	private static final String[] financialAssistance = FINANCIAL_ASSISTANCE.getPermissions();


	private static final String[][] permissionGroups = {
		allPersonal,allBusiness,
		customerPersonal, customerBusiness,
		damagesAndPeoplePatrimonial, damagesAndPeopleResponsibility, damagesAndPeopleTransport, damagesAndPeopleFinancialRisks,
		damagesAndPeopleRural, damagesAndPeopleAuto, damagesAndPeopleHousing, damagesAndPeopleAcceptanceAndBranchesAbroad, damagesAndPeoplePerson,
		capitalizationTitle, claimNotificationRequest, pensionPlan, lifePension, financialAssistance};

	@Override
	@PreEnvironment(required = { "consent_endpoint_response", "brazil_consent" })
	public Environment evaluate(Environment env) {
		String path = "data.permissions";

		JsonElement grantedPermissionsEl = env.getElementFromObject("consent_endpoint_response", path);
		if (grantedPermissionsEl == null) {
			throw error("Couldn't find "+path+" in the consent response");
		}
		if (!grantedPermissionsEl.isJsonArray()) {
			throw error(path+" in the consent response is not a JSON array", args("permissions", grantedPermissionsEl));
		}
		JsonArray grantedPermissions = (JsonArray) grantedPermissionsEl;
		if (grantedPermissions.size() <= 0) {
			throw error(path+" in the consent response is an empty array", args("permissions", grantedPermissionsEl));
		}

		JsonArray requestedPermissions = (JsonArray) env.getElementFromObject("brazil_consent", "requested_permissions");

		if (!jsonArraysIsSubset(requestedPermissions,grantedPermissions)) {
			throw error("Consent endpoint response contains different permissions than requested", args("granted", grantedPermissionsEl, "requested", requestedPermissions));
		}

		if (!isValidResourceGroup(grantedPermissions)) {
			log("Consent endpoint response is not a complete grouping.", args("granted", grantedPermissionsEl, "requested", requestedPermissions));
			throw error("Consent endpoint response is not a complete grouping");
		}

		logSuccess("Consent endpoint response contains expected permissions", args("granted", grantedPermissionsEl, "requested", requestedPermissions));

		return env;
	}

	boolean jsonArraysIsSubset(JsonArray supersetJson, JsonArray subsetJson) {

		Set<String> superset = new HashSet<>();
		supersetJson.forEach(e -> superset.add(OIDFJSON.getString(e)));

		Set<String> subset = new HashSet<>();
		subsetJson.forEach(e -> subset.add(OIDFJSON.getString(e)));

		return superset.containsAll(subset);
	}

	private boolean isValidResourceGroup(JsonArray grantedPermissions) {
		Gson gson = new Gson();

		String[] grantedPermissionsArray = gson.fromJson(grantedPermissions, String[].class);
		List<String> grantedPermissionsList = Arrays.asList(grantedPermissionsArray);
		TreeSet<String> grantedPermissionsSet = new TreeSet<>();
		grantedPermissionsSet.addAll(grantedPermissionsList);

		TreeSet<String> permissionsInCompleteGroup = new TreeSet<>();
		for (String[] permissionGroup : permissionGroups){
			TreeSet<String> permGroup = new TreeSet<>();
			permGroup.addAll(Arrays.asList(permissionGroup));

			if(grantedPermissionsSet.equals(permGroup)){
				return true;
			}
			if(grantedPermissionsSet.containsAll(permGroup)){
				permissionsInCompleteGroup.addAll(permGroup);
			}
			if(permissionsInCompleteGroup.equals(grantedPermissionsSet)){
				return true;
			}
		}
		return false;
	}

}
