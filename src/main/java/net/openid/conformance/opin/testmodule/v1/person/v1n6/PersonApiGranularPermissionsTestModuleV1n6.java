package net.openid.conformance.opin.testmodule.v1.person.v1n6;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonClaimOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonListOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonPolicyInfoOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePerson.v1n6.GetInsurancePersonPremiumOASValidatorV1n6;
import net.openid.conformance.opin.testmodule.v1.person.abstracts.AbstractPersonApiGranularPermissionsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "person_api_granular-permissions_test-module_v1.6.0",
        displayName = "Ensure specific methods can only be called if the respective permissions is granted.",
        summary = "Ensure specific methods can only be called if the respective permissions is granted.\n" +
                "• Call the POST Consents with the RESOURCES_READ, DAMAGES_AND_PEOPLE_PERSON_READ and DAMAGES_AND_PEOPLE_PERSON_POLICYINFO_READ permissions\n" +
                "• Expects 201 -  Validate Response\n" +
                "• Redirects the user to Authorize consent\n" +
                "• Calls the GET Consents\n" +
                "• Expects 200 - Validate Response and Status is Authorized\n" +
                "• Calls the GET Root Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Call the GET Policy Info Endpoint\n" +
                "• Expects 200 - Validate Response\n" +
                "• Call the GET Premium Endpoint\n" +
                "• Expects a 403 response - Validate error response\n" +
                "• Call the GET Claim Endpoint\n" +
                "• Expects a 403 response - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class PersonApiGranularPermissionsTestModuleV1n6 extends AbstractPersonApiGranularPermissionsTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePersonListOASValidatorV1n6.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPolicyInfoValidator() {
        return GetInsurancePersonPolicyInfoOASValidatorV1n6.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getPremiumValidator() {
        return GetInsurancePersonPremiumOASValidatorV1n6.class;
    }

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getClaimValidator() {
        return GetInsurancePersonClaimOASValidatorV1n6.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }
}
