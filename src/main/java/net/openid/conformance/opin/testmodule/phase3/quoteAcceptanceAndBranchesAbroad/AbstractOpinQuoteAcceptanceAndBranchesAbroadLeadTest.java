package net.openid.conformance.opin.testmodule.phase3.quoteAcceptanceAndBranchesAbroad;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteAcceptanceAndBranchesAbroadLeadTest extends AbstractOpinPhase3QuoteLeadTestModule {

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_ACCEPTANCE_AND_BRANCHES_ABROAD_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-acceptance-and-branches-abroad";
    }

    @Override
    protected String getApiName() {
        return "quote-acceptance-and-branches-abroad";
    }
}
