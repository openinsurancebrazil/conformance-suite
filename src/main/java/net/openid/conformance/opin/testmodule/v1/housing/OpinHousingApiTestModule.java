package net.openid.conformance.opin.testmodule.v1.housing;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingClaimOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingListOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingPolicyInfoOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3.GetInsuranceHousingPremiumOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.v1.housing.abstracts.AbstractOpinHousingApiTest;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-housing-api-test",
        displayName = "Validates the structure of all housing API resources",
        summary = "Validates the structure of all housing API resources\n" +
                "• Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_HOUSING_READ”, “DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "• Calls GET housing “/” API\n" +
                "• Expects 200 - Fetches one of the Policy IDs returned\n" +
                "• Calls GET housing policy-Info API \n" +
                "• Expects 200 - Validate all the fields\n" +
                "• Calls GET housing premium API \n" +
                "• Expects 200- Validate all the fields\n" +
                "• Calls GET housing claim API \n" +
                "• Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
        "client.org_jwks"
})

public class OpinHousingApiTestModule extends AbstractOpinHousingApiTest {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsuranceHousingListOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0]");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator() {
        return GetInsuranceHousingPolicyInfoOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator() {
        return GetInsuranceHousingPremiumOASValidatorV1n3.class;
    }

    @Override
    protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator() {
        return GetInsuranceHousingClaimOASValidatorV1n3.class;
    }
}