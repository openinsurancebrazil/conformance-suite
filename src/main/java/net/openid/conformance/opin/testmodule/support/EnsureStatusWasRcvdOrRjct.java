package net.openid.conformance.opin.testmodule.support;

import java.util.List;

public class EnsureStatusWasRcvdOrRjct extends AbstractEnsureStatusWasX {

    @Override
    protected List<String> getStatuses() {
        return List.of("RCVD", "RJCT");
    }
}
