package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public abstract class AbstractCreateQuotePostRequestBody extends AbstractCondition {
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    @Override
    @PreEnvironment(required = "config", strings = "consent_id")
    @PostEnvironment(required = "resource_request_entity", strings = "resource_request_entity")
    public Environment evaluate(Environment env) {
        JsonObject requestData = new JsonObject();
        JsonObject data = new JsonObject();
        LocalDateTime now = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
        String expirationDateTime = now.plusDays(7).format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
        String productType = env.getString("config", "consent.productType");

        // Fill the data.
        data.addProperty("consentId", env.getString("consent_id"));
        data.addProperty("expirationDateTime", expirationDateTime);
        if("business".equals(productType)) {
            addBusinessInfo(env, data);
        } else {
            addPersonalInfo(env, data);
        }
        addCommonQuoteData(env, data);
        editData(env, data);

        requestData.add("data", data);
        logSuccess("The request body was created", args(
                "body",
                requestData
        ));
        env.putObject("resource_request_entity", requestData);
        env.putString("resource_request_entity", requestData.toString());
        return env;
    }

    private void addPersonalInfo(Environment env, JsonObject data) {
        JsonObject quoteCustomer = new JsonObject();
        addPersonalIdentificationData(env, quoteCustomer);
        addPersonalQualificationData(env, quoteCustomer);
        addComplimentaryInformationData(env, quoteCustomer);
        data.add("quoteCustomer", quoteCustomer);
    }

    private void addBusinessInfo(Environment env, JsonObject data) {
        JsonObject quoteCustomer = new JsonObject();
        addBusinessIdentificationData(env, quoteCustomer);
        addBusinessQualificationData(env, quoteCustomer);
        addComplimentaryInformationData(env, quoteCustomer);
        data.add("quoteCustomer", quoteCustomer);
    }

    private void addPersonalIdentificationData(Environment env, JsonObject quoteCustomer) {
        JsonObject companyInfo = new JsonObject();
        companyInfo.addProperty("cnpjNumber", "01773247000563");
        companyInfo.addProperty("name", "Empresa da Organização A");

        JsonObject contact = new JsonObject();
        JsonArray postalAddresses = new JsonArray();
        JsonObject postalAddress = new JsonObject();
        postalAddress.addProperty("address", "Av Naburo Ykesaki, 1270");
        postalAddress.addProperty("townName", "Marília");
        postalAddress.addProperty("countrySubDivision", "SP");
        postalAddress.addProperty("postCode", "10000000");
        postalAddress.addProperty("country", "BRA");
        postalAddresses.add(postalAddress);
        contact.add("postalAddresses", postalAddresses);

        JsonObject identificationData = new JsonObject();
        identificationData.addProperty(
                "updateDateTime",
                ZonedDateTime
                        .now(ZoneOffset.UTC)
                        .toLocalDateTime()
                        .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT))
        );
        identificationData.addProperty("brandName", "Organização A");
        identificationData.addProperty("civilName", "Juan Kaique Cláudio Fernandes");
        identificationData.addProperty("cpfNumber", env.getString("config", "resource.brazilCpf"));
        identificationData.add("companyInfo", companyInfo);
        identificationData.addProperty("hasBrazilianNationality", true);
        identificationData.add("contact", contact);

        quoteCustomer.add("identificationData", identificationData);
    }

    private void addPersonalQualificationData(Environment env, JsonObject quoteCustomer) {
        JsonObject qualificationData = new JsonObject();
        qualificationData.addProperty(
                "updateDateTime",
                ZonedDateTime
                        .now(ZoneOffset.UTC)
                        .toLocalDateTime()
                        .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT))
        );
        qualificationData.addProperty("pepIdentification", "NAO_EXPOSTO");
        qualificationData.addProperty("lifePensionPlans", "NAO_SE_APLICA");

        quoteCustomer.add("qualificationData", qualificationData);
    }

    private void addBusinessIdentificationData(Environment env, JsonObject quoteCustomer) {
        JsonObject companyInfo = new JsonObject();
        companyInfo.addProperty("cnpjNumber", "01773247000563");
        companyInfo.addProperty("name", "Empresa da Organização A");

        JsonObject document = new JsonObject();
        document.addProperty("businesscnpjNumber", env.getString("config", "resource.brazilCnpj"));

        JsonObject contact = new JsonObject();
        JsonArray postalAddresses = new JsonArray();
        JsonObject postalAddress = new JsonObject();
        postalAddress.addProperty("address", "Av Naburo Ykesaki, 1270");
        postalAddress.addProperty("townName", "Marília");
        postalAddress.addProperty("countrySubDivision", "SP");
        postalAddress.addProperty("postCode", "10000000");
        postalAddress.addProperty("country", "BRA");
        postalAddresses.add(postalAddress);
        contact.add("postalAddresses", postalAddresses);

        JsonObject identificationData = new JsonObject();
        identificationData.addProperty(
                "updateDateTime",
                ZonedDateTime
                        .now(ZoneOffset.UTC)
                        .toLocalDateTime()
                        .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT))
        );
        identificationData.addProperty("brandName", "Organização A");
        identificationData.add("companyInfo", companyInfo);
        identificationData.addProperty("businessName", "Luiza e Benjamin Assessoria Jurídica Ltda");
        identificationData.add("document", document);
        identificationData.add("contact", contact);

        quoteCustomer.add("identificationData", identificationData);
    }

    private void addBusinessQualificationData(Environment env, JsonObject quoteCustomer) {
        JsonObject qualificationData = new JsonObject();
        qualificationData.addProperty(
                "updateDateTime",
                ZonedDateTime
                        .now(ZoneOffset.UTC)
                        .toLocalDateTime()
                        .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT))
        );

        quoteCustomer.add("qualificationData", qualificationData);
    }

    private void addComplimentaryInformationData(Environment env, JsonObject quoteCustomer) {

        JsonArray productsServices = new JsonArray();
        JsonObject productsService = new JsonObject();
        productsService.addProperty("contract", "string");
        productsService.addProperty("type", "MICROSSEGUROS");
        productsServices.add(productsService);

        JsonObject complimentaryInformationData = new JsonObject();
        complimentaryInformationData.addProperty(
                "updateDateTime",
                ZonedDateTime
                        .now(ZoneOffset.UTC)
                        .toLocalDateTime()
                        .format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT))
        );
        complimentaryInformationData.addProperty(
                "startDate",
                ZonedDateTime
                        .now(ZoneOffset.UTC)
                        .toLocalDateTime()
                        .format(DateTimeFormatter.ofPattern(DATE_FORMAT))
        );
        complimentaryInformationData.add("productsServices", productsServices);

        quoteCustomer.add("complimentaryInformationData", complimentaryInformationData);
    }

    private void addCommonQuoteData(Environment env, JsonObject data) {
        JsonObject unit = new JsonObject();
        unit.addProperty("code", "R$");
        unit.addProperty("description", "BRL");

        JsonObject maxLmg = new JsonObject();
        maxLmg.addProperty("amount", "2000.00");
        maxLmg.add("unit", unit);

        JsonObject quoteData = new JsonObject();
        String today = ZonedDateTime
                .now(ZoneOffset.UTC)
                .toLocalDateTime()
                .format(DateTimeFormatter.ofPattern(AbstractCreateQuotePostRequestBody.DATE_FORMAT));
        quoteData.addProperty("termStartDate", today);
        quoteData.addProperty("termEndDate", today);
        quoteData.addProperty("insuranceType", "RENOVACAO");
        quoteData.addProperty("policyId", "111111");
        quoteData.addProperty("insurerId", "insurer_id");
        quoteData.addProperty("currency", "BRL");
        quoteData.add("maxLMG", maxLmg);
        quoteData.addProperty("includesAssistanceServices", false);

        data.add("quoteData", quoteData);
    }

    protected JsonArray getBeneficiaries(Environment env) {
        String productType = env.getString("config", "consent.productType");
        JsonObject beneficiary = new JsonObject();
        if("business".equals(productType)) {
            beneficiary.addProperty("identification", env.getString("config", "resource.brazilCnpj"));
            beneficiary.addProperty("identificationType", "CNPJ");
        } else {
            beneficiary.addProperty("identification", env.getString("config", "resource.brazilCpf"));
            beneficiary.addProperty("identificationType", "CPF");
        }

        JsonArray beneficiaries = new JsonArray();
        beneficiaries.add(beneficiary);

        return beneficiaries;
    }

    protected JsonArray getCoverages() {
        JsonArray coverages = new JsonArray();
        JsonObject coverage = new JsonObject();
        coverage.addProperty("branch", "0111");
        coverage.addProperty("code", getCode());
        coverage.addProperty("isSeparateContractingAllowed", false);
        JsonObject maxLMI = new JsonObject();
        maxLMI.addProperty("amount", "90.85");
        maxLMI.addProperty("unitType", "PORCENTAGEM");
        coverage.add("maxLMI", maxLMI);
        coverages.add(coverage);
        return coverages;
    }

    protected abstract void editData(Environment env, JsonObject data);

    protected String getCode(){
        return null;
    }
}
