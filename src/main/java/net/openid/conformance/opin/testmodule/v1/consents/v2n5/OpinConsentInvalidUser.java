package net.openid.conformance.opin.testmodule.v1.consents.v2n5;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentInvalidUser;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-consent-invalid-user-test",
	displayName = "This test will use a dummy, but well-formated payload to make sure that the server will accept the POST Consents request, as mandated by the security guidelines, however, will not be able to complete the authorization code flow as no user with this CPF exists on the financial institution",
	summary = "This test will use a dummy, but well-formated payload to make sure that the server will accept the POST Consents request, as mandated by the security guidelines, however, will not be able to complete the authorization code flow as no user with this CPF exists on the financial institution\n" +
		"• Call the POST Consents API with the dummy payload\n" +
		"• Expect the server to accept the message and return the 201 as the financial institution should not validate the CPF at that stage of the process.\n" +
		"• Redirect the user to authorize the consent\n" +
		"• Expect a failure an error on the authorization redirect",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinConsentInvalidUser extends AbstractOpinConsentInvalidUser {

}