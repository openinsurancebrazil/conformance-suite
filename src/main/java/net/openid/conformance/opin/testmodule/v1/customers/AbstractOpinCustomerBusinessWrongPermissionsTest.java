package net.openid.conformance.opin.testmodule.v1.customers;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessIdentifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBusinessProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.opin.testmodule.AbstractOpinPermissionsCheckingFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinCustomerBusinessWrongPermissionsTest extends AbstractOpinPermissionsCheckingFunctionalTestModule {

	private static final String API = "insurance-customer-business";
	OpinConsentPermissionsBuilder permissionsBuilder;

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessIdentificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessQualificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getCustomerBusinessComplimentaryInfoValidator();

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpinPreAuthorizationConsentApi steps = new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
		return steps;
	}

	@Override
	protected void configureClient(){
		callAndStopOnFailure(PrepareConfigForCustomerBusinessTest.class);
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		callAndStopOnFailure(BuildOpinBusinessCustomersConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		//Simple UI fix
		callAndStopOnFailure(AddDummyBusinessProductTypeToConfig.class);
		callAndStopOnFailure(AddScopesForCustomerApi.class);
		setApi(API);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void preFetchResources() {
		//Not needed for this test
	}

	@Override
	protected void prepareCorrectConsents() {
		permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS).build();
		callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
	}

	@Override
	protected void prepareIncorrectPermissions() {
		permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.ALL_PHASE2)
			.removePermissionsGroups(PermissionsGroup.CUSTOMERS_PERSONAL)
			.removePermissionsGroups(PermissionsGroup.CUSTOMERS_BUSINESS).build();
	}

	@Override
	protected void requestResourcesWithIncorrectPermissions() {
		runInBlock("Ensure we cannot call the  Customer Business Qualification", () -> {
			callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getCustomerBusinessQualificationValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the Customer Business Identifications", () -> {
			callAndStopOnFailure(PrepareToGetBusinessIdentifications.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getCustomerBusinessIdentificationValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the Customer Business Complimentary-Information", () -> {
			callAndStopOnFailure(PrepareToGetBusinessComplimentaryInformation.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getCustomerBusinessComplimentaryInfoValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});
	}
}
