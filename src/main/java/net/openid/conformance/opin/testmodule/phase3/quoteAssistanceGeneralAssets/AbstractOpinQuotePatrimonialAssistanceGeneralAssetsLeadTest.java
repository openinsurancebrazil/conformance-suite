package net.openid.conformance.opin.testmodule.phase3.quoteAssistanceGeneralAssets;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuotePatrimonialAssistanceGeneralAssetsLeadTest extends AbstractOpinPhase3QuoteLeadTestModule {

        @Override
        protected OPINScopesEnum getScope() {
                return OPINScopesEnum.QUOTE_PATRIMONIAL_LEAD;
        }

        @Override
        protected String getApiFamilyType() {
                return "quote-patrimonial-assistance-general-assets";
        }

        @Override
        protected String getApiName() {
                return "quote-patrimonial";
        }

        @Override
        protected boolean testIdempotencyLead() {
                return true;
        }
}
