package net.openid.conformance.opin.testmodule.support;

public class BuildInsurancePersonUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {
    @Override
    protected String getApiReplacement() {
        return "insurance-person";
    }

    @Override
    protected String getEndpointReplacement() {
        return "insurance-person";
    }
}
