package net.openid.conformance.opin.testmodule.support.oasValidators.domesticCredit.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Domestic Credit 1.3.0")
public class GetDomesticCreditOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/domesticCredit/v1n3/swagger-domestic-credit-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/domestic-credit";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
