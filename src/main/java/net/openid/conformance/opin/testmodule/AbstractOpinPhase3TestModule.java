package net.openid.conformance.opin.testmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinConsentV2Endpoint;
import net.openid.conformance.opin.testmodule.support.directory.GetOpinResourcesV2Endpoint;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.GetConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.sequences.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinPhase3TestModule extends AbstractOpinFunctionalTestModule {

    protected boolean isPolicyIdExtractionFlow = true;
    protected static final String API_TYPE = ApiTypes.DATA_API_PHASE3.toString();
    protected abstract AbstractJsonAssertingCondition postConsentValidator();

    @Override
    protected void configureClient() {
        call(new ValidateWellKnownUriSteps());
        call(new ValidateRegisteredEndpoints(getEndpointCondition()));
        super.configureClient();
        env.putString("api_type", API_TYPE);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        setupPermissions();
        callAndStopOnFailure(StoreScope.class);
        callAndStopOnFailure(AddResourcesScope.class);
    }

    protected void setupPermissions() {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
        callAndStopOnFailure(OpinSetPermissionsBuilderForAllPhase2Permissions.class);
        permissionsBuilder.buildFromEnv();
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication)
                .skip(PostConsentsOASValidatorV2n6.class, "Already using a validator")
                .insertAfter(OpinConsentEndpointResponseValidatePermissions.class,
                        sequenceOf(
                                condition(postConsentValidator().getClass()).dontStopOnFailure(),
                                condition(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class),
                                condition(SaveConsentsAccessToken.class)
                        )
                )
                .insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
                        sequenceOf(
                                condition(CreateRandomFAPIInteractionId.class),
                                condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
                        ));
    }

    @Override
    protected void performPreAuthorizationSteps() {
        super.performPreAuthorizationSteps();
        if (!isPolicyIdExtractionFlow) {
            fetchConsent("AWAITING_AUTHORISATION", EnsurePaymentConsentStatusWasAwaitingAuthorisation.class);
            validateGetConsentResponse();
        }
    }

    @Override
    protected void requestProtectedResource() {
        if (isPolicyIdExtractionFlow) {
            preCallProtectedResource("GET resources API to extract a policy ID");
            validateGetResourcesResponse();

            eventLog.startBlock("Deleting consent");
            call(deleteConsent());
            eventLog.endBlock();
        } else {
            executeTest();
        }
    }

    protected abstract void validateGetResourcesResponse();

    @Override
    protected void performPostAuthorizationFlow() {
        if (!isPolicyIdExtractionFlow) {
            fetchConsent("AUTHORISED", EnsurePaymentConsentStatusWasAuthorised.class);
            validateGetConsentResponse();
        }

        super.performPostAuthorizationFlow();
    }

    @Override
    protected void validateGetConsentResponse() {
        callAndContinueOnFailure(GetConsentsOASValidatorV2n6.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
        if (isPolicyIdExtractionFlow) {
            isPolicyIdExtractionFlow = false;
            setupSecondFlow();
            performAuthorizationFlow();
        } else {
            super.onPostAuthorizationFlowComplete();
        }
    }

    protected ConditionSequence deleteConsent() {
        return sequenceOf(
            condition(LoadConsentsAccessToken.class),
            condition(PrepareToFetchConsentRequest.class),
            condition(PrepareToDeleteConsent.class),
            condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
        );
    }

    protected abstract void setupSecondFlow();

    protected void fetchConsent(String status, Class<? extends Condition> statusCheckingCondition) {
        runInBlock(String.format("Checking the created consent - Expecting %s status", status), () -> {
            useClientCredentialsToken();
            callAndStopOnFailure(PaymentConsentIdExtractor.class);
            callAndStopOnFailure(AddJWTAcceptHeader.class);
            callAndStopOnFailure(ExpectJWTResponse.class);
            callAndStopOnFailure(PrepareToFetchConsentRequest.class);
            callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(statusCheckingCondition, Condition.ConditionResult.FAILURE);
            unmapClientCredentialsToken();
        });
    }

    protected abstract void executeTest();

    @Override
    protected void validateResponse() {}

    protected ConditionSequence getEndpointCondition(){
        return sequenceOf(condition(GetOpinConsentV2Endpoint.class),condition(GetOpinResourcesV2Endpoint.class));
    }

    protected void callProtectedResource() {
        callAndStopOnFailure(SetResourceMethodToGet.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class);
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
        callAndStopOnFailure(CallProtectedResource.class);
        callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
    }
}
