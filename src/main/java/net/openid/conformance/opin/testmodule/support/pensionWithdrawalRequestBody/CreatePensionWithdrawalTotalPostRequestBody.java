package net.openid.conformance.opin.testmodule.support.pensionWithdrawalRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;

public class CreatePensionWithdrawalTotalPostRequestBody extends AbstractCreatePensionWithdrawalRequestBody {
    @Override
    protected void editData(Environment env, JsonObject data) {}
}
