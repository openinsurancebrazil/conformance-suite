package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium.v1n10;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium.AbstractOpinQuotePatrimonialCondominiumCoreAcknTest;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.condominium.GetPatrimonialCondominiumQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.condominium.PatchPatrimonialCondominiumOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.condominium.PostPatrimonialCondominiumOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-condominium_api_core-ackn_test-module_v1n10",
        displayName = "Ensure that a Patrimonial Condominium quotation request can be successfully created and accepted afterwards.",
        summary = "Ensure that a Patrimonial Condominium quotation request can be successfully created and accepted afterwards. This test applies to both Personal and Business products. If the \"brazilCnpj\" field is filled out, it will transmit business information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST condominium/request endpoint sending personal or business information, following what is defined at the config\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Poll the GET condominium/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET condominium/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate response and ensure status is ACPT\n" +
                "\u2022 Call PATCH condominium/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as ACKN\n" +
                "\u2022 Expect 200 - Validate response and ensure status is ACKN\n" +
                "\u2022 Call GET links.redirect endpoint\n" +
                "\u2022 Expect 200",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialCondominiumCoreAcknTestModuleV1n10 extends AbstractOpinQuotePatrimonialCondominiumCoreAcknTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialCondominiumOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialCondominiumQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialCondominiumOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialCondominiumPostRequestBodyWithCoverage.class;
    }
}
