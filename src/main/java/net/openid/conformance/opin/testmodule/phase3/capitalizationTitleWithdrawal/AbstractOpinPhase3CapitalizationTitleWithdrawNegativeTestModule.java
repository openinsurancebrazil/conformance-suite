package net.openid.conformance.opin.testmodule.phase3.capitalizationTitleWithdrawal;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.opin.testmodule.support.LoadRequestBodyAsString;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForCapitalizationTitleWithdrawal;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.AbstractCreateCapitalizationTitleXRequestBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.CreateCapitalizationTitleWithdrawalPostRequestBody;

public abstract class AbstractOpinPhase3CapitalizationTitleWithdrawNegativeTestModule extends AbstractOpinPhase3CapitalizationTitleWithdrawTestModule{

    @Override
    protected void validateGetResourcesResponse() {
        //endpoint validated in other step
    }

    @Override
    protected void executeTest() {
        postCapitalizationTitleWithdrawal("422");
        validateCapitalizationTitleWithdrawalResponse();
        fetchConsent("CONSUMED", EnsurePaymentConsentStatusWasConsumed.class);
    }

    @Override
    protected AbstractCreateCapitalizationTitleXRequestBody createRequestBody() {
        return new CreateCapitalizationTitleWithdrawalPostRequestBody();
    }

    protected void postCapitalizationTitleWithdrawal(String status) {
        eventLog.startBlock(String.format("POST Capitalization Title Withdrawal - Expecting %s", status));
        callAndStopOnFailure(PrepareUrlForCapitalizationTitleWithdrawal.class);
        callAndStopOnFailure(createRequestBody().getClass());
        editCapitalizationTitleWithdrawalRequestBody();
        callAndStopOnFailure(LoadRequestBodyAsString.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        setIdempotencyKey();
        callAndStopOnFailure(SetContentTypeApplicationJson.class);
        callAndStopOnFailure(AddJsonAcceptHeaderRequest.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(SetResourceMethodToPost.class);
        skipSelfLinkValidation();
        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
        eventLog.endBlock();
    }

    protected abstract void editCapitalizationTitleWithdrawalRequestBody();


    protected void validateCapitalizationTitleWithdrawalResponse() {
        eventLog.startBlock("Validate response");
        callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(validator().getClass(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }
    protected void setIdempotencyKey() {
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
    }

    protected abstract AbstractJsonAssertingCondition validator();

}
