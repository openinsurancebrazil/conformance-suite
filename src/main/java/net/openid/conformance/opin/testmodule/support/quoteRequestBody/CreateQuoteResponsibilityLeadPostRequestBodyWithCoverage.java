package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

public class CreateQuoteResponsibilityLeadPostRequestBodyWithCoverage extends AbstractCreateQuoteLeadPostRequestBodyWithCoverage {

    @Override
    protected String getCode() {
        return "DANOS_CAUSADOS_A_TERCEIROS";
    }
}
