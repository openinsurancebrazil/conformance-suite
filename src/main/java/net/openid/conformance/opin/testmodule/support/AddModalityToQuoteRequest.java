package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddModalityToQuoteRequest extends AbstractCondition {

    @PreEnvironment(strings = "modality", required = "resource_request_entity")
    @Override
    public Environment evaluate(Environment env) {
        JsonObject requestData = env.getObject("resource_request_entity");
        String modality = env.getString("modality");

        JsonObject data = requestData.getAsJsonObject("data");
        data.addProperty("modality", modality);

        logSuccess("Added modality to request body", args(
                "modality", modality,
                "data", data
        ));

        env.putObject("resource_request_entity", requestData);

        return env;
    }
}


