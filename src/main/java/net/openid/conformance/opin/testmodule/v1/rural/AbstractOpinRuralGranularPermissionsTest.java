package net.openid.conformance.opin.testmodule.v1.rural;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.AbstractBuildConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.AddRuralScope;
import net.openid.conformance.opin.testmodule.support.BuildRuralConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import java.util.Map;

public abstract class AbstractOpinRuralGranularPermissionsTest extends AbstractOpinApiTestModuleV2 {

    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator();

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.DAMAGES_AND_PEOPLE_RURAL;
    }

    @Override
    protected String[] getIndividualPermissionsToBeRemoved() {
        return new String[]{"DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ","DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ"};
    }

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddRuralScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildRuralConfigResourceUrlFromConsentUrl.class;
    }

    @Override
    protected String getApi() {
        return "insurance-rural";
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "policy-info", getPolicyInfoValidator(),
                "premium", getPremiumValidator(),
                "claim", getClaimValidator()
        );
    }

    @Override
    protected Class<? extends Condition> getExpectedResponseCode(String endpoint) {
        Map<String, Class<? extends Condition>> endpointToStatusCodeMap = Map.of(
                "policy-info", EnsureResourceResponseCodeWas200.class,
                "premium", EnsureResourceResponseCodeWas403.class,
                "claim", EnsureResourceResponseCodeWas403.class
        );
        return endpointToStatusCodeMap.get(endpoint);
    }

    @Override
    protected boolean checkConsentAuthorizedStatusAfterRedirect() {
        return true;
    }


}
