package net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n4;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.AbstractGetInsurancePensionPlanContractsOASValidator;

@ApiName("Insurance Pension Plan 1.4.0")
public class GetInsurancePensionPlanContractsOASValidatorV1n4 extends AbstractGetInsurancePensionPlanContractsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insurancePensionPlan/v1/swagger-insurance-pension-plan.yaml";
	}
}
