package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePatrimonial.v1n4.GetInsurancePatrimonialPremiumOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-patrimonial-api-structural-test",
	displayName = "Validate structure of Patrimonial API Endpoints 200 response",
	summary = "Validate structure of Patrimonial API Endpoints 200 response \n"+
		"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"resource.consentUrl"
		}
)
@VariantHidesConfigurationFields(parameter = ClientAuthType.class, value = "none", configurationFields = {
		"resource.resourceUrl"
})

public class OpinPatrimonialStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-patrimonial";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);
		setRootValidator(GetInsurancePatrimonialListOASValidatorV1n4.class);
		setPolicyInfoValidator(GetInsurancePatrimonialPolicyInfoOASValidatorV1n4.class);
		setPremiumValidator(GetInsurancePatrimonialPremiumOASValidatorV1n4.class);
		setClaimValidator(GetInsurancePatrimonialClaimOASValidatorV1n4.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

