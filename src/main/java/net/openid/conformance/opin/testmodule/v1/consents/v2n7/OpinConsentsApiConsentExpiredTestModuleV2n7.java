package net.openid.conformance.opin.testmodule.v1.consents.v2n7;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.GetConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.v1.consents.abstracts.AbstractOpinConsentsApiConsentExpiredTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
		testName = "opin-consent-api-expired-consent-test-v2n7",
		displayName = "Ensure a consent expires if the expiration time is reached",
		summary = "Ensure a consent expires if the expiration time is reached\n" +
				"• Call the POST Consents with all of the existing permissions and set expiration to be of 2 minute\n" +
				"• Expect a 201 - Validate Response\n" +
				"• Redirect the user to authorize the created ConsentID\n" +
				"• Calls the GET Consents endpoint with the authorized consentID\n" +
				"• Expects a 200 - Validate that the Consent is on an Authorised state\n" +
				"• Conformance Suite Will be set to sleep for 2 minute\n" +
				"• Expect a success 200 - Make sure Status is set to REJECTED, rejectedBy is ASPSP and reason is CONSENT_MAX_DATE_REACHED",
		profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
		configurationFields = {
				"server.discoveryUrl",
				"client.client_id",
				"client.jwks",
				"mtls.key",
				"mtls.cert",
				"resource.consentUrl",
				"resource.brazilCpf",
		}
)
public class OpinConsentsApiConsentExpiredTestModuleV2n7 extends AbstractOpinConsentsApiConsentExpiredTestModule {

	@Override
	protected AbstractJsonAssertingCondition postValidator() {
		return new PostConsentsOASValidatorV2n7();
	}

	@Override
	protected AbstractJsonAssertingCondition getValidator() {
		return new GetConsentsOASValidatorV2n7();
	}
}