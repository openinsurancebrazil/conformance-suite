package net.openid.conformance.opin.testmodule.v1.financialAssistance;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.*;

import java.util.Map;

public abstract class AbstractFinancialAssistanceApiGranularPermissionsTest extends AbstractOpinApiTestModuleV2 {

    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getContractInfoValidator();
    protected abstract Class<? extends OpenAPIJsonSchemaValidator> getMovementsValidator();

    @Override
    protected Class<? extends AbstractScopeAddingCondition> getScopeCondition() {
        return AddFinancialAssistanceScope.class;
    }

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return BuildInsuranceFinancialAssistanceUrlFromConsentUrl.class;
    }

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.FINANCIAL_ASSISTANCE;
    }

    @Override
    protected String[] getIndividualPermissionsToBeRemoved() {
        return new String[]{"FINANCIAL_ASSISTANCE_MOVEMENTS_READ"};
    }

    @Override
    protected String getApi() {
        return "insurance-financial-assistance";
    }

    @Override
    protected Class<? extends AbstractIdSelector> getIdSelector() {
        env.putString("selected_id_json_path", "$.data[0].brand.companies[0].contracts[0].contractId");
        return IdSelectorFromJsonPath.class;
    }

    @Override
    protected Class<? extends Condition> getExpectedResponseCode(String endpoint) {
        Map<String, Class<? extends Condition>> endpointToStatusCodeMap = Map.of(
                "contract-info", EnsureResourceResponseCodeWas200.class,
                "movements", EnsureResourceResponseCodeWas403.class
        );
        return endpointToStatusCodeMap.get(endpoint);
    }

    @Override
    protected Map<String, Class<? extends AbstractJsonAssertingCondition>> getEndpoints() {
        return Map.of(
                "contract-info", getContractInfoValidator(),
                "movements", getMovementsValidator()
        );
    }

    @Override
    protected boolean checkConsentAuthorizedStatusAfterRedirect() {
        return true;
    }
}
