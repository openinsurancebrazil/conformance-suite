package net.openid.conformance.opin.testmodule.phase3.capitalizationTitleWithdrawal;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.AbstractCreateCapitalizationTitleXConsentBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalConsentBody.CreateCapitalizationTitleWithdrawalPerdaDeInteresseConsentBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.AbstractCreateCapitalizationTitleWithdrawalRequestBody;
import net.openid.conformance.opin.testmodule.support.capitalizationTitleWithdrawalRequestBody.CreateCapitalizationTitleWithdrawalCompromissosPessoaisEmergenciaisPostRequestBody;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitleWithdrawal.v1n3.PostCapitalizationTitleWithdrawalOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-capitalization-title-withdraw_api_wrong-withdrawalReason_test-module_v1",
        displayName = "Validates the structure of Capitalization Title Withdrawal API",
        summary = """
                Ensure a Capitalization Title withdrawal request is not successful with a mismatching withdrawalReason
                                
                · Execute a Customer Data Sharing Journey for the Capitalization Title Product, obtaining the first productName and planId from the insurance-capitalization-title/plans endpoint, and the corresponding modality, susepProcessNumber, titleId, seriesId, termEndDate and prAmount from the plan-info endpoint
                · Call the POST Consents with CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permission, sending the withdrawalCaptalizationInformation with the pre-saved information, withdrawalTotalAmount as the value of prAmount, and withdrawalReason as PERDA_DE_INTERESSE
                · Expect a 201 - Validate the response
                · Redirect the User to Authorize the Consent
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response and ensure the status is "AUTHORIZED"
                · Call the POST capitalization-title/request endpoint, sending the same information as the consent request, but withwithdrawalReason as COMPROMISSOS_PESSOAIS_EMERGENCIAIS
                · Expect a 422 - Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3CapitalizationTitleWithdrawApiWrongWithdrawalReasonTestModule extends AbstractOpinPhase3CapitalizationTitleWithdrawNegativeTestModule{

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }

    @Override
    protected void editCapitalizationTitleWithdrawalRequestBody() {
    }

    @Override
    protected AbstractCreateCapitalizationTitleWithdrawalRequestBody createRequestBody() {
        return new CreateCapitalizationTitleWithdrawalCompromissosPessoaisEmergenciaisPostRequestBody();
    }

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostCapitalizationTitleWithdrawalOASValidatorV1n3();
    }

    @Override
    protected AbstractCreateCapitalizationTitleXConsentBody createConsentBody() {
        return new CreateCapitalizationTitleWithdrawalPerdaDeInteresseConsentBody();
    }
}
