package net.openid.conformance.opin.testmodule.support;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public enum PermissionsGroup {
	ALL,
	ALL_PERSONAL,
	ALL_BUSINESS,
	ALL_PHASE2,
	ALL_PHASE3,
	ALL_PERSONAL_PHASE2,
	ALL_BUSINESS_PHASE2,
	RESOURCES,
	CUSTOMERS_PERSONAL,
	CUSTOMERS_BUSINESS,
	CAPITALIZATION_TITLE
	,

	DAMAGES_AND_PEOPLE_PATRIMONIAL,
	DAMAGES_AND_PEOPLE_RESPONSIBILITY,
	DAMAGES_AND_PEOPLE_TRANSPORT,
	DAMAGES_AND_PEOPLE_FINANCIAL_RISKS,
	DAMAGES_AND_PEOPLE_RURAL,
	DAMAGES_AND_PEOPLE_AUTO,
	DAMAGES_AND_PEOPLE_HOUSING,
	DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD,
	DAMAGES_AND_PEOPLE_PERSON,
	PENSION_PLAN,
	LIFE_PENSION,
	FINANCIAL_ASSISTANCE,

	// Phase 3.
	ENDORSEMENT_REQUEST_CREATE,
	CLAIM_NOTIFICATION_REQUEST,
	QUOTE_AUTO_LEAD,
	QUOTE_PATRIMONIAL_LEAD,
	QUOTE_PATRIMONIAL_HOME,
	QUOTE_PATRIMONIAL_BUSINESS,
	QUOTE_PATRIMONIAL_DIVERSE_RISKS,
	QUOTE_PATRIMONIAL_CONDOMINIUM,
	QUOTE_PERSON_LIFE,
	QUOTE_PERSON_TRAVEL,
	QUOTE_PERSON_LEAD,
	CONTRACT_LIFE_PENSION,
	QUOTE_CAPITALIZATION_TITLE,
	QUOTE_CAPITALIZATION_TITLE_LEAD,
	QUOTE_CAPITALIZATION_TITLE_RAFFLE,
	CAPITALIZATION_TITLE_WITHDRAWAL,
	PENSION_WITHDRAWAL_LEAD,
	PENSION_WITHDRAWAL
	;


	private static Set<PermissionsGroup> phase2 = new HashSet<>();
	private static Set<PermissionsGroup> phase3 = new HashSet<>();

	static {
		phase2.add(RESOURCES);
		phase3.add(RESOURCES);
		for (PermissionsGroup permission : PermissionsGroup.values()) {
			if (permission.name().startsWith(PermissionsGroup.ALL.name())) {
				continue;
			}

			//PHASE 3
			if (permission.equals(ENDORSEMENT_REQUEST_CREATE) ||
					permission.equals(CLAIM_NOTIFICATION_REQUEST) ||
					permission.name().startsWith("QUOTE_") ||
					permission.name().startsWith("CONTRACT_") ||
					permission.name().startsWith("CAPITALIZATION_TITLE_WITHDRAWAL") ||
					permission.name().startsWith("PENSION_WITHDRAWAL"))
			{
				phase3.add(permission);
			} else {
				//PHASE 2
				phase2.add(permission);
			}
		}
	}
	public String[] getPermissions() {
		String[] permissions = {};

		if (this.name().startsWith(PermissionsGroup.ALL.name())) {
			permissions = getMultiplePermissions(this);
		} else {
			permissions = getIndividualPermissions(this);
		}

		return permissions;
	}

	private String[] getMultiplePermissions(PermissionsGroup permissionsGroup) {
		Set<String> permissions = new HashSet<>();
		switch (this) {
			case ALL:
				phase2.stream().forEach(elem -> {
					permissions.addAll(List.of(getIndividualPermissions(elem)));
				});
				phase3.stream().forEach(elem -> {
					permissions.addAll(List.of(getIndividualPermissions(elem)));
				});
				break;
			case ALL_PERSONAL:
				phase2.stream()
						.filter(elem -> !elem.equals(CUSTOMERS_BUSINESS))
						.forEach(elem -> {
					permissions.addAll(List.of(getIndividualPermissions(elem)));
				});
				phase3.stream()
						.filter(elem -> !elem.equals(CUSTOMERS_BUSINESS))
						.forEach(elem -> {
					permissions.addAll(List.of(getIndividualPermissions(elem)));
				});
				break;
			case ALL_BUSINESS:
				phase2.stream()
						.filter(elem -> !elem.equals(CUSTOMERS_PERSONAL))
						.forEach(elem -> {
							permissions.addAll(List.of(getIndividualPermissions(elem)));
						});
				phase3.stream()
						.filter(elem -> !elem.equals(CUSTOMERS_PERSONAL))
						.forEach(elem -> {
							permissions.addAll(List.of(getIndividualPermissions(elem)));
						});
				break;
			case ALL_PERSONAL_PHASE2:
				phase2.stream()
						.filter(elem -> !elem.equals(CUSTOMERS_BUSINESS))
						.forEach(elem -> {
							permissions.addAll(List.of(getIndividualPermissions(elem)));
						});
				break;
			case ALL_BUSINESS_PHASE2:
				phase2.stream()
						.filter(elem -> !elem.equals(CUSTOMERS_PERSONAL))
						.forEach(elem -> {
							permissions.addAll(List.of(getIndividualPermissions(elem)));
						});
				break;
			case ALL_PHASE3:
				phase3.stream().forEach(elem -> {
					permissions.addAll(List.of(getIndividualPermissions(elem)));
				});
				break;
			default:
				//Base case for ALL_PHASE2
				phase2.stream().forEach(elem -> {
					permissions.addAll(List.of(getIndividualPermissions(elem)));
				});
				break;
		}
		return permissions.toArray(new String[0]);
	}
	private String[] getIndividualPermissions(PermissionsGroup permissionsGroup) {
		String[] permissions;
		switch (permissionsGroup) {
			case CUSTOMERS_PERSONAL:

			case CUSTOMERS_BUSINESS:
				permissions = new String[]{permissionsGroup.name()+"_IDENTIFICATIONS_READ", permissionsGroup.name()+"_QUALIFICATION_READ",
					permissionsGroup.name()+"_ADDITIONALINFO_READ", "RESOURCES_READ"};
				break;

			case LIFE_PENSION, PENSION_PLAN:
				permissions = new String[]{permissionsGroup.name() + "_READ",
						permissionsGroup.name()+"_CONTRACTINFO_READ",
						permissionsGroup.name()+"_MOVEMENTS_READ",
						permissionsGroup.name()+"_PORTABILITIES_READ",
						permissionsGroup.name()+"_WITHDRAWALS_READ",
						permissionsGroup.name()+"_CLAIM",
						"RESOURCES_READ"};
				break;

			case FINANCIAL_ASSISTANCE:
				permissions = new String[]{permissionsGroup.name() + "_READ",
						permissionsGroup.name()+"_CONTRACTINFO_READ",
						permissionsGroup.name()+"_MOVEMENTS_READ",
						"RESOURCES_READ"};
				break;

			case DAMAGES_AND_PEOPLE_PERSON:
				permissions = new String[]{permissionsGroup.name() + "_READ",
						permissionsGroup.name()+"_POLICYINFO_READ",
						permissionsGroup.name()+"_CLAIM_READ",
						permissionsGroup.name()+"_PREMIUM_READ",
						"RESOURCES_READ"};
				break;


			case RESOURCES:
				permissions = new String[]{"RESOURCES_READ"};
				break;

			case ENDORSEMENT_REQUEST_CREATE:
				permissions = new String[]{permissionsGroup.name()};
				break;

			case CLAIM_NOTIFICATION_REQUEST:
				permissions = new String[]{permissionsGroup.name() + "_DAMAGE_CREATE",
					permissionsGroup.name() + "_PERSON_CREATE"};
				break;

			case CAPITALIZATION_TITLE:
				permissions = new String[]{
						permissionsGroup.name() + "_READ",
						permissionsGroup.name() + "_PLANINFO_READ",
						permissionsGroup.name() + "_EVENTS_READ",
						permissionsGroup.name() + "_SETTLEMENTS_READ",
						"RESOURCES_READ"
					};
				break;

			case PENSION_WITHDRAWAL_LEAD, PENSION_WITHDRAWAL, CAPITALIZATION_TITLE_WITHDRAWAL, QUOTE_CAPITALIZATION_TITLE_RAFFLE:
				permissions = new String[]{
						permissionsGroup.name() + "_CREATE"
				};
				break;


			case QUOTE_PATRIMONIAL_LEAD, QUOTE_AUTO_LEAD, QUOTE_CAPITALIZATION_TITLE_LEAD, QUOTE_PERSON_LEAD:
				permissions = new String[]{
						permissionsGroup.name() + "_CREATE",
						permissionsGroup.name() + "_UPDATE"
				};
				break;

			case QUOTE_PATRIMONIAL_CONDOMINIUM, QUOTE_PATRIMONIAL_HOME, QUOTE_PATRIMONIAL_BUSINESS,
					QUOTE_PATRIMONIAL_DIVERSE_RISKS, QUOTE_PERSON_LIFE, QUOTE_PERSON_TRAVEL,
				 CONTRACT_LIFE_PENSION, QUOTE_CAPITALIZATION_TITLE:
				permissions = new String[]{
						permissionsGroup.name() + "_READ",
						permissionsGroup.name() + "_CREATE",
						permissionsGroup.name() + "_UPDATE"
				};
				break;

			default:
				permissions = new String[]{permissionsGroup.name() + "_READ", permissionsGroup.name() + "_POLICYINFO_READ",
					permissionsGroup.name() + "_PREMIUM_READ", permissionsGroup.name() + "_CLAIM_READ", "RESOURCES_READ"};
				break;
		}

		return permissions;
	}
}
