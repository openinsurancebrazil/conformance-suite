package net.openid.conformance.opin.testmodule.v1.pensionPlan.v1n5;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insurancePensionPlan.v1n5.GetInsurancePensionPlanContractsOASValidatorV1n5;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.abstracts.AbstractPensionPlanApiResourcesTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "pension-plan_api_resources_test-module_v1.5.0",
        displayName = "Makes sure that the Resource API and the Pension Plan API are returning the same available IDs",
        summary = "Makes sure that the Resource API and the Pension Plan API are returning the same available IDs\n" +
                "• Call the POST consents with all the permissions needed to access the Capitalization Title API (“PENSION_PLAN_READ”, “PENSION_PLAN_CONTRACTINFO_READ”, “PENSION_PLAN_MOVEMENTS_READ”, “PENSION_PLAN_PORTABILITIES_READ”,“PENSION_PLAN_WITHDRAWALS_READ”,“PENSION_PLAN_CLAIM”, “RESOURCES_READ”)\n" +
                "• Expects 201 - Validate Response\n" +
                "• Redirect the user to authorize consent\n" +
                "• Calls GET Pension Plan contracts Endpoint\n" +
                "• Expect 200  - Validate response and Make sure that an id is returned - Fetch the pension Identification provided by this API\n" +
                "• Call the resources API\n" +
                "• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2
)
public class PensionPlanApiResourcesTestModuleV1n5 extends AbstractPensionPlanApiResourcesTestModule {

    @Override
    protected Class<? extends AbstractJsonAssertingCondition> getRootValidator() {
        return GetInsurancePensionPlanContractsOASValidatorV1n5.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
    }
}
