package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialBusiness.v1n10;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteNegativeConsentTestModule;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-business_api_negative-consent_test-module_v1n10",
        displayName = "Ensure a consent cannot be created in unhappy requests.",
        summary = "Ensure a consent cannot be created in unhappy requests.\n" +
                "\u2022 Call POST Consents with permission for Customer Data, PF or PJ, and Patrimonial Lead.\n" +
                "\u2022 Expect 422 - Validate Error Response\n" +
                "\u2022 Call POST Consents with permission for Patrimonial Lead and the permission for this specified Patrimonial Branch.\n" +
                "\u2022 Expect 422 - Validate Error Response.\n" +
                "\u2022 Call POST Consents with permission for Patrimonial Lead.\n" +
                "\u2022 Expect 422 - Validate Error Response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialBusinessNegativeConsentTestModuleV1n10 extends AbstractOpinPhase3QuoteNegativeConsentTestModule {

    @Override
    protected PermissionsGroup getPermissionsGroup() {
        return PermissionsGroup.QUOTE_PATRIMONIAL_BUSINESS;
    }

    @Override
    protected void postConsentExpectingFailure(String description) {
        runInBlock(description, () -> {
            call(setHeadersSequence());
            setIdempotencyKey();
            callAndStopOnFailure(PrepareToPostConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
            callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
            env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
            callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(PostConsentsOASValidatorV2n7.class, Condition.ConditionResult.FAILURE);
            env.unmapKey("resource_endpoint_response_full");
        });
    }
}
