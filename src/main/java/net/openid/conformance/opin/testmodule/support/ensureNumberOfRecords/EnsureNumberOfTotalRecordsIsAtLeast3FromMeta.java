package net.openid.conformance.opin.testmodule.support.ensureNumberOfRecords;

import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.AbstractEnsureNumberOfTotalRecordsFromMeta;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.TotalRecordsComparisonOperator;

public class EnsureNumberOfTotalRecordsIsAtLeast3FromMeta extends AbstractEnsureNumberOfTotalRecordsFromMeta {

    @Override
    protected int getTotalRecordsComparisonAmount() {
        return 3;
    }

    @Override
    protected TotalRecordsComparisonOperator getComparisonMethod() {
        return TotalRecordsComparisonOperator.AT_LEAST;
    }
}
