package net.openid.conformance.opin.testmodule.fvp.quote;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.fvp.quote.abstracts.AbstractFvpOpinQuoteApiTestModule;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAcpt;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasCanc;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.PatchPatrimonialDiverseRisksOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n10.diverseRisks.PostPatrimonialDiverseRisksOASValidatorV1n10;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatchCancRequestBody;
import net.openid.conformance.opin.testmodule.support.quoteRequestBody.CreateQuotePatrimonialDiverseRisksPostRequestBody;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-fvp-quote-patrimonial-diverse-risks_api_core-canc_test-module",
        displayName = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and rejected afterwards.",
        summary = "Ensure that a Patrimonial diverse-risks quotation request can be successfully created and rejected afterwards. This test applies to both Personal and diverse-risks products. If the \"brazilCnpj\" field is filled out, it will transmit diverse-risks information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST diverse-risks/request endpoint sending personal or diverse-risks information, following what is defined at the config\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD\n" +
                "\u2022 Poll the GET diverse-risks/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET diverse-risks/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respones and ensure status is ACPT\n" +
                "\u2022 Call PATCH diverse-risks/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF, and status as CANC\n" +
                "\u2022 Expect 200 - Validate Response and ensure status is CANC",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinFvpQuotePatrimonialDiverseRisksCoreCancTestModule extends AbstractFvpOpinQuoteApiTestModule {

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_PATRIMONIAL_DIVERSE_RISKS;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-patrimonial-diverse-risks";
    }

    @Override
    protected String getApiName() {
        return "quote-patrimonial";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "diverse-risks/request";
    }

    @Override
    protected Class<? extends AbstractCondition> createPostQuoteRequestBody() {
        return CreateQuotePatrimonialDiverseRisksPostRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialDiverseRisksOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialDiverseRisksQuoteStatusOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteResponse() {
        return PatchPatrimonialDiverseRisksOASValidatorV1n10.class;
    }

    @Override
    protected Class<? extends AbstractCondition> createPatchQuoteRequestBody() {
        return CreateQuotePatchCancRequestBody.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePatchQuoteStatus() {
        return EnsureStatusWasCanc.class;
    }
}
