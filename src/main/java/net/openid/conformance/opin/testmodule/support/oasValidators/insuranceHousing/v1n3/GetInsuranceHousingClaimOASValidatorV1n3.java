package net.openid.conformance.opin.testmodule.support.oasValidators.insuranceHousing.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Insurance Housing 1.3.0")
public class GetInsuranceHousingClaimOASValidatorV1n3 extends OpenAPIJsonSchemaValidator{

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/insuranceHousing/v1/swagger-insurance-housing.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/insurance-housing/{policyId}/claim";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
