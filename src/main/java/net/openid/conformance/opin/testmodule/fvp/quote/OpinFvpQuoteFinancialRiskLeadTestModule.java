package net.openid.conformance.opin.testmodule.fvp.quote;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.fvp.quote.abstracts.AbstractOpinFvpPhase3QuoteLeadTestModule;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteFinancialRisk.v1n9.PatchQuoteFinancialRiskLeadOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quoteFinancialRisk.v1n9.PostQuoteFinancialRiskLeadOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-fvp-quote-financial-risk-lead_api_core_test-module",
        displayName = "Ensure that after an initial Lead request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idepotemcy key the request fails",
        summary = """
        Ensure that after an initial Lead request, a subsequent request using same idempotency key and payload can succeed, and when the payload is changed using the same idepotemcy key the request fails
        · Call POST lead/request endpoint sending personal or business information, following what is defined at the config
        · Expect 201 - Validate Response and ensure status is RCVD
        · Call the POST lead/request Endpoint with the same payload and idempotency id
        · Expects 201 - Validate Response
        · Call the POST lead/request, with a different payload as the previous request but the same idempotency id
        · Expects 422 ERRO_IDEMPOTENCIA - Validate Response
        · Call PATCH lead/request/{consentId} endpoint, sending all the required headers and using the identificationNumber as defined at the config, either CNPJ or CPF
        · Expect 200 - Validate Response and ensure status is CANC
        """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "server.authorisationServerId",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinFvpQuoteFinancialRiskLeadTestModule extends AbstractOpinFvpPhase3QuoteLeadTestModule {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteLeadResponse() {
        return PostQuoteFinancialRiskLeadOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validatePatchQuoteLeadResponse() {
        return PatchQuoteFinancialRiskLeadOASValidatorV1n9.class;
    }

    @Override
    protected boolean testIdempotencyLead() {
        return true;
    }

    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_FINANCIAL_RISK_LEAD;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-financial-risk";
    }

    @Override
    protected String getApiName() {
        return "quote-financial-risk";
    }
}
