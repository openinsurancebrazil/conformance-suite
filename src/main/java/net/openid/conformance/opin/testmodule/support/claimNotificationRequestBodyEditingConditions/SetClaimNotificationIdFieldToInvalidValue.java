package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

public class SetClaimNotificationIdFieldToInvalidValue extends AbstractSetClaimNotificationIdFieldToInvalidValue {

    @Override
    protected String getPolicyIdFieldName() {
        return "policyNumber";
    }
}

