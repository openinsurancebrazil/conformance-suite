package net.openid.conformance.opin.testmodule.support.oasValidators.equipmentBreakdown.v1n3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Equipment Breakdown 1.3.0")
public class GetEquipmentBreakdownOASValidatorV1n3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openinsurance/equipmentBreakdown/v1n3/swagger-equipment-breakdown-1.3.0.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/equipment-breakdown";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}