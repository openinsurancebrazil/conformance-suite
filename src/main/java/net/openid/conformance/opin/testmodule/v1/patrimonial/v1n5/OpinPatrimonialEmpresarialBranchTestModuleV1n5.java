package net.openid.conformance.opin.testmodule.v1.patrimonial.v1n5;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.patrimonial.PatrimonialBranches;
import net.openid.conformance.opin.testmodule.v1.patrimonial.abstracts.AbstractOpinPatrimonialBranchTestModuleV1n5;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "opin-patrimonial-empresarial-api-branch-test-v1.5.0",
	displayName = "Validates if at least one Policy Id returned is from “Compreensivo Empresarial” branch",
	summary = "Validates if at least one Policy Id returned is from “Compreensivo Empresarial” branch\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Patrimonial API (“DAMAGES_AND_PEOPLE_PATRIMONIAL_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ”,  “RESOURCES_READ”) \n" +
		"\u2022 Expects 201 - Expects Success on Redirect\n" +
		"\u2022 Calls GET Patrimonial “/” API\n" +
		"\u2022 Expects 201 - Loops through all of the Policy IDs returned\n" +
		"\u2022 Calls GET Patrimonial policy-Info API with each Policy ID\n" +
		"\u2022 Expects 200 - Validate if at least one of the branch object on the response body has the value of  “0118” (Compreensivo Empresarial)\n" +
		"\u2022 Return success if at least one of the policy IDs is of ”Compreensivo Empresarial”, return an warning otherwise",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"resource.brazilCnpj",
		"consent.productType"
	}
)

public class OpinPatrimonialEmpresarialBranchTestModuleV1n5 extends AbstractOpinPatrimonialBranchTestModuleV1n5 {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		setBranch(PatrimonialBranches.COMPREENSIVO_EMPRESARIAL);
		super.onConfigure(config, baseUrl);
	}
}
