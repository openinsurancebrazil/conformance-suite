package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class PrepareConfigForCustomerPersonalTest extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {
        env.putString("config","consent.productType","personal");
        env.getElementFromObject("config", "resource").getAsJsonObject().remove("brazilCnpj");
        logSuccess("productType has been set to personal and brazilCnpj deleted from resource",
            args("resource", env.getElementFromObject("config", "resource").getAsJsonObject(),
                "productType", OIDFJSON.getString(env.getElementFromObject("config", "consent.productType"))));
        return env;
    }
}
