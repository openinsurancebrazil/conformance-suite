package net.openid.conformance.opin.testmodule.support.quoteRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreateQuotePatchAcknRequestBody extends AbstractCreateQuotePatchRequestBody {
    @Override
    @PreEnvironment(strings = "quote_id")
    protected void editData(Environment env, JsonObject data) {
        data.addProperty("status", "ACKN");
        data.addProperty("insurerQuoteId", env.getString("quote_id"));
    }
}
