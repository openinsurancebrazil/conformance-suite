package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

public class SetClaimNotificationIdFieldToWrongType extends AbstractSetClaimNotificationIdFieldToWrongType {

    @Override
    protected String getPolicyIdFieldName() {
        return "policyNumber";
    }
}
