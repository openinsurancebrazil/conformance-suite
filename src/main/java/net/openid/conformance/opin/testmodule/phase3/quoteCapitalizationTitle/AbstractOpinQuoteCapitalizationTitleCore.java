package net.openid.conformance.opin.testmodule.phase3.quoteCapitalizationTitle;

import net.openid.conformance.opin.testmodule.phase3.AbstractOpinPhase3QuoteTestModule;
import net.openid.conformance.opin.testmodule.support.AbstractEnsureStatusWasX;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasAcpt;
import net.openid.conformance.opin.testmodule.support.EnsureStatusWasRcvd;
import net.openid.conformance.opin.testmodule.support.scopesAndPermissionsBuilder.OPINScopesEnum;

public abstract class AbstractOpinQuoteCapitalizationTitleCore extends AbstractOpinPhase3QuoteTestModule {
    @Override
    protected OPINScopesEnum getScope() {
        return OPINScopesEnum.QUOTE_CAPITALIZATION_TITLE;
    }

    @Override
    protected String getApiFamilyType() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getApiName() {
        return "quote-capitalization-title";
    }

    @Override
    protected String getApiBaseEndpoint() {
        return "/request";
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensurePostQuoteStatus() {
        return EnsureStatusWasRcvd.class;
    }

    @Override
    protected Class<? extends AbstractEnsureStatusWasX> ensureGetQuoteStatus() {
        return EnsureStatusWasAcpt.class;
    }

}
