package net.openid.conformance.opin.testmodule.support.oasValidators.quotePerson;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetPersonTravelQuoteStatusOASValidator extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getEndpointPath() {
        return "/travel/request/{consentId}/quote-status";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }
}