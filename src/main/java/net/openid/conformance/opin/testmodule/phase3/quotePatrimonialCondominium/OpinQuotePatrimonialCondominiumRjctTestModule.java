package net.openid.conformance.opin.testmodule.phase3.quotePatrimonialCondominium;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.condominium.GetPatrimonialCondominiumQuoteStatusOASValidatorV1n9;
import net.openid.conformance.opin.testmodule.support.oasValidators.quotePatrimonial.v1n9.condominium.PostPatrimonialCondominiumOASValidatorV1n9;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-quote-patrimonial-condominium_api_rejected_test-module_v1",
        displayName = "Ensure that a Patrimonial Condominium quotation request can be rejected.",
        summary = "Ensure that a Patrimonial Condominium quotation request can be rejected. This test module will send the quoteData as an empty field, and expect the quotation to be rejected. This test applies to both Personal and Business products. If the \"brazilCnpj\" field is filled out, it will transmit business information; otherwise, personal information will be used.\n" +
                "\u2022 Call POST condominium/request endpoint sending personal or business information, following what is defined at the config, and sending the quoteData as an empty field\n" +
                "\u2022 Expect 201 - Validate Response and ensure status is RCVD or RJCT\n" +
                "\u2022 Poll the GET condominium/request/{consentId}/quote-status  endpoint while status is RCVD or EVAL\n" +
                "\u2022 Call GET condominium/request/{consentId}/quote-status endpoint\n" +
                "\u2022 Expect 200 - Validate Respone and ensure status is RJCT",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "resource.brazilCnpj",
                "consent.productType"
        }
)
public class OpinQuotePatrimonialCondominiumRjctTestModule extends AbstractOpinQuotePatrimonialCondominiumRjctTest {

    @Override
    protected Class<? extends AbstractCondition> validatePostQuoteResponse() {
        return PostPatrimonialCondominiumOASValidatorV1n9.class;
    }

    @Override
    protected Class<? extends AbstractCondition> validateGetQuoteResponse() {
        return GetPatrimonialCondominiumQuoteStatusOASValidatorV1n9.class;
    }
}
