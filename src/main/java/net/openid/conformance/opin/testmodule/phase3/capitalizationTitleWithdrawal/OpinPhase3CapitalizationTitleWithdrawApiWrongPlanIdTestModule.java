package net.openid.conformance.opin.testmodule.phase3.capitalizationTitleWithdrawal;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.oasValidators.capitalizationTitleWithdrawal.v1n3.PostCapitalizationTitleWithdrawalOASValidatorV1n3;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-capitalization-title-withdraw_api_wrong-planId_test-module_v1",
        displayName = "Validates the structure of Capitalization Title Withdrawal API",
        summary = """
                Ensure a Capitalization Title withdrawal request is not successful with a mismatching planId
                                
                · Execute a Customer Data Sharing Journey for the Capitalization Title Product, obtaining the first and second productName and planId from the insurance-capitalization-title/plans endpoint, and the corresponding modality, susepProcessNumber, titleId, seriesId, termEndDate and prAmount from the plan-info endpoint
                · Call the POST Consents with CAPITALIZATION_TITLE_WITHDRAWAL_CREATE permission, sending the withdrawalCaptalizationInformation with the pre-saved information from the first planId, withdrawalTotalAmount as the value of prAmount\s
                · Expect a 201 - Validate the response
                · Redirect the User to Authorize the Consent
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response and ensure the status is "AUTHORIZED"
                · Call the POST capitalization-title/request endpoint, sending the information from the second PlanId
                · Expect a 422 - Validate the response
                · Call the GET Consents endpoint
                · Expect a 200 - Validate the response, ensuring the status is "CONSUMED"
                """,
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)

public class OpinPhase3CapitalizationTitleWithdrawApiWrongPlanIdTestModule extends AbstractOpinPhase3CapitalizationTitleWithdrawNegativeTestModule{

    @Override
    protected AbstractJsonAssertingCondition postConsentValidator() {
        return new PostConsentsOASValidatorV2n6();
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        callSecondResource=true;
        super.onConfigure(config, baseUrl);
    }

    @Override
    protected void editCapitalizationTitleWithdrawalRequestBody() {
        //not needed in this test
    }

    @Override
    protected AbstractJsonAssertingCondition validator() {
        return new PostCapitalizationTitleWithdrawalOASValidatorV1n3();
    }
}
