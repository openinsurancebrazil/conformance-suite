package net.openid.conformance.opin.testmodule.support.claimNotificationRequestBodyEditingConditions;

public class SetClaimNotificationIdFieldToWrongTypeDiff extends AbstractSetClaimNotificationIdFieldToWrongType {

    @Override
    protected String getPolicyIdFieldName() {
        return "policyId";
    }
}
