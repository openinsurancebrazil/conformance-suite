package net.openid.conformance.opin.testmodule.fvp;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractMTLSCertificatesFromConfiguration;
import net.openid.conformance.condition.client.GetStaticClientConfiguration;
import net.openid.conformance.condition.client.ValidateMTLSCertificatesHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.AddOpenIdScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModuleV2;
import net.openid.conformance.opin.testmodule.support.AbstractBuildConfigResourceUrlFromConsentUrl;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractOpinFvpApiTestModule extends AbstractOpinApiTestModuleV2 {

    protected abstract ConditionSequence getEndpointCondition();

    @Override
    protected Class<? extends AbstractBuildConfigResourceUrlFromConsentUrl> getBuildConfigResourceUrlCondition() {
        return null;
    }

    @Override
    protected void configureClient() {
        call(new ValidateRegisteredEndpoints(getEndpointCondition()));
        callAndStopOnFailure(GetStaticClientConfiguration.class);
        callAndStopOnFailure(AddOpenIdScope.class);
        exposeEnvString("client_id");
        // Test won't pass without MATLS, but we'll try anyway (for now)
        callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
        callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);
        validateClientConfiguration();
    }

    @Override
    protected void requestProtectedResource() {
        syncWaitTime();
        super.requestProtectedResource();
    }

}
