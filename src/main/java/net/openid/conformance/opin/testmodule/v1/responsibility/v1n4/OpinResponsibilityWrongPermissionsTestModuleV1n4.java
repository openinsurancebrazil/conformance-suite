package net.openid.conformance.opin.testmodule.v1.responsibility.v1n4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.opin.testmodule.support.AbstractIdSelector;
import net.openid.conformance.opin.testmodule.support.IdSelectorFromJsonPath;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n6.PostConsentsOASValidatorV2n6;
import net.openid.conformance.opin.testmodule.support.oasValidators.consents.v2n7.PostConsentsOASValidatorV2n7;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityClaimOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityListOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityPolicyInfoOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.support.oasValidators.insuranceResponsibility.v1n4.GetInsuranceResponsibilityPremiumOASValidatorV1n4;
import net.openid.conformance.opin.testmodule.v1.responsibility.AbstractOpinResponsibilityWrongPermissionsTest;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-responsibility-api-wrong-permissions-test-v1n4",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Responsibility API (“DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ”, “DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET Responsibility “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET Responsibility policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET Responsibility premium API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Responsibility claim API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with only either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well\n" +
		"\u2022 Calls GET Responsibility “/” API\n"+
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Responsibility policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Responsibility premium API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Responsibility claim API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
public class OpinResponsibilityWrongPermissionsTestModuleV1n4 extends AbstractOpinResponsibilityWrongPermissionsTest {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getRootValidator(){
		return GetInsuranceResponsibilityListOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPolicyInfoValidator(){
		return GetInsuranceResponsibilityPolicyInfoOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getPremiumValidator(){
		return GetInsuranceResponsibilityPremiumOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getClaimValidator(){
		return GetInsuranceResponsibilityClaimOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends AbstractIdSelector> getIdSelector() {
		env.putString("selected_id_json_path", "$.data[0].companies[0].policies[0].policyId");
		return IdSelectorFromJsonPath.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps().replace(PostConsentsOASValidatorV2n6.class, condition(PostConsentsOASValidatorV2n7.class));
	}
}
