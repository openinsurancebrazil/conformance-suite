package net.openid.conformance.opin.testmodule.support.oasValidators.quoteCapitalizationTitle;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public abstract class AbstractGetQuoteCapitalizationTitleOASValidator extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getEndpointPath() {
        return "/request/{consentId}/quote-status";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

    @Override
    protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
        JsonObject data = body.getAsJsonObject("data");
        String status = data.get("status").getAsString();
        if ("ACPT".equals(status) && !data.has("quoteInfo")) {
            throw error("quoteInfo is required when status is ACPT");
        }

        if (data.has("quoteInfo")) {
            assertData(data);
        }
    }

    protected void assertData(JsonObject data) {
        JsonObject quoteInfo = data.getAsJsonObject("quoteInfo");
        JsonObject quoteData = quoteInfo.getAsJsonObject("quoteData");

        assertField1IsRequiredWhenField2HasValue2(quoteData, "singlePayment", "paymentType", "UNICO");
        assertField1IsRequiredWhenField2HasValue2(quoteData, "monthlyPayment", "paymentType", "MENSAL");
    }
}