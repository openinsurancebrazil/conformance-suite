package net.openid.conformance.opin.testmodule.support.oasValidators.productsNServices.lifePension.v1n5;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

import java.util.List;

/**
 * Api Source: swagger/openinsurance/productsServices/swagger-life-pension-1.5.0.yaml
 * Api endpoint: /life-pension
 * Api version: 1.5.0
 */

@ApiName("ProductsNServices Life Pension")
public class GetProductsNServicesLifePensionOASValidatorV1n5 extends OpenAPIJsonSchemaValidator {

    @Override
    protected String getPathToOpenApiSpec() {
        return "swagger/openinsurance/productsServices/swagger-life-pension-1.5.0.yaml";
    }

    @Override
    protected String getEndpointPath() {
        return "/life-pension";
    }

    @Override
    protected HttpMethod getEndpointMethod() {
        return HttpMethod.GET;
    }

    @Override
    protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
        JsonObject data = body.getAsJsonObject("data");
        assertProductDetails(data);
    }

    public void assertProductDetails(JsonObject data){
        JsonArray productDetails = JsonPath.read(data, "brand.companies[*].products[*].productDetails[*]");
        productDetails.forEach(productDetail -> assertField1IsRequiredWhenField2HasNotValue2Array(
                productDetail.getAsJsonObject().get("grantPeriodBenefit").getAsJsonObject(),
                "biometricTable",
                "incomeModality",
                List.of("PAGAMENTO_UNICO","RENDA_PRAZO_CERTO")
        ));
    }

    protected void assertField1IsRequiredWhenField2HasNotValue2Array(JsonObject data, String field1, String field2, List<String> value2) {
        JsonArray actualValue2 = this.findByPath(data, field2).getAsJsonArray();
        if(!this.isPresent(data, field1)){
            boolean matchFound=false;
            for(int i=0; i<actualValue2.size() && !matchFound; i++) {
                if (value2.contains(OIDFJSON.getString(actualValue2.get(i)))) {
                    matchFound=true;
                }
            }
            if (!matchFound) {
                throw this.error(String.format("%s is required when %s is not %s", field1, field2, value2));
            }
        }
    }

}
