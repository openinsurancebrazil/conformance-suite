package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.housing.v1n4.OpinHousingApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.housing.v1n4.OpinHousingGranularPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.housing.v1n4.OpinHousingResourcesApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.housing.v1n4.OpinHousingWrongPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance Housing API test V1.4.0",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.HOUSING_API_TEST_PLAN_V1_4,
	summary = PlanNames.HOUSING_API_TEST_PLAN_V1_4
)

public class HousingApiTestPlanV1n4 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinHousingApiTestModuleV1n4.class,
					OpinHousingResourcesApiTestModuleV1n4.class,
					OpinHousingWrongPermissionsTestModuleV1n4.class,
					OpinHousingGranularPermissionsTestModuleV1n4.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
