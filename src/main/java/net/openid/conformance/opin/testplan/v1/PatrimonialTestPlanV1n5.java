package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.patrimonial.v1n5.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance patrimonial api test-v1.5.0",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.PATRIMONIAL_API_TEST_PLAN_v1_5,
	summary = PlanNames.PATRIMONIAL_API_TEST_PLAN_v1_5 + ". To obtain Certification for Patrimonial Branches, it is required to submit certification requests collectively. If a branch has already been certified before, the new certification request should encompass all previously certified branches along with the new additions."
)

public class PatrimonialTestPlanV1n5 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinPatrimonialApiTestModuleV1n5.class,
					OpinPatrimonialWrongPermissionsTestModuleV1n5.class,
					OpinPatrimonialResourcesApiTestModuleV1n5.class,
					OpinPatrimonialResidencialBranchTestModuleV1n5.class,
					OpinPatrimonialCondominioBranchTestModuleV1n5.class,
					OpinPatrimonialEmpresarialBranchTestModuleV1n5.class,
					OpinPatrimonialRnRoBranchTestModuleV1n5.class,
					OpinPatrimonialGlobalBancosBranchTestModuleV1n5.class,
					OpinPatrimonialLucrosCessantesBranchTestModuleV1n5.class,
					OpinPatrimonialRiscosDeEngenhariaBranchTestModuleV1n5.class,
					OpinPatrimonialAssistenciaBranchTestModuleV1n5.class,
					OpinPatrimonialRiscosDiversosBranchTestModuleV1n5.class,
					OpinPatrimonialGarantiaEstendidaBranchTestModuleV1n5.class,
					OpinPatrimonialGranularPermissionsTestModuleV1n5.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
