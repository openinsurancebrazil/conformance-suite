package net.openid.conformance.opin.testplan.v1;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.rural.v1n4.OpinRuralApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.rural.v1n4.OpinRuralGranularPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.rural.v1n4.OpinRuralResourcesApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.rural.v1n4.OpinRuralWrongPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "Insurance rural api test-v1n4",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        displayName = PlanNames.RURAL_API_TEST_PLAN_V1_4,
        summary = PlanNames.RURAL_API_TEST_PLAN_V1_4
)

public class RuralApiTestPlanV1n4 implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinRuralApiTestModuleV1n4.class,
                                OpinRuralResourcesApiTestModuleV1n4.class,
                                OpinRuralWrongPermissionsTestModuleV1n4.class,
                                OpinRuralGranularPermissionsTestModuleV1n4.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
