package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.financialAssistance.v1n3.FinancialAssistanceAPICoreTestModuleV1n3;
import net.openid.conformance.opin.testmodule.v1.financialAssistance.v1n3.FinancialAssistanceApiGranularPermissionsTestModuleV1n3;
import net.openid.conformance.opin.testmodule.v1.financialAssistance.v1n3.FinancialAssistanceApiResourcesTestModuleV1n3;
import net.openid.conformance.opin.testmodule.v1.financialAssistance.v1n3.FinancialAssistanceApiWrongPermissionsTestModuleV1n3;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "financial-assistance_test-plan_v1n3",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        displayName = PlanNames.FINANCIAL_ASSISTANCE_API_TEST_PLAN_V1_3_0,
        summary = PlanNames.FINANCIAL_ASSISTANCE_API_TEST_PLAN_V1_3_0
)
public class FinancialAssistanceApiTestPlanV1n3 implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                FinancialAssistanceAPICoreTestModuleV1n3.class,
                                FinancialAssistanceApiWrongPermissionsTestModuleV1n3.class,
                                FinancialAssistanceApiResourcesTestModuleV1n3.class,
                                FinancialAssistanceApiGranularPermissionsTestModuleV1n3.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
