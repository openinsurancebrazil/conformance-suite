package net.openid.conformance.opin.testplan.v1;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.financialRisk.OpinFinancialRiskGranularPermissionsTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.opin.testmodule.v1.financialRisk.OpinFinancialRiskApiTestModule;
import net.openid.conformance.opin.testmodule.v1.financialRisk.OpinFinancialRiskResourcesApiTestModule;
import net.openid.conformance.opin.testmodule.v1.financialRisk.OpinFinancialRiskWrongPermissionsTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance financial risks api test",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN,
	summary = PlanNames.FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN
)

public class FinancialRisksApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinFinancialRiskApiTestModule.class,
					OpinFinancialRiskResourcesApiTestModule.class,
					OpinFinancialRiskWrongPermissionsTestModule.class,
					OpinFinancialRiskGranularPermissionsTestModule.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
