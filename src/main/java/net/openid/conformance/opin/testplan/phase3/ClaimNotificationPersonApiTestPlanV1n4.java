package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.claimNotification.v1n4.person.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
    testPlanName = "Claim Notification person api test v1.4.0",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    displayName = PlanNames.CLAIM_NOTIFICATION_PERSON_API_TEST_PLAN_V1_4,
    summary = PlanNames.CLAIM_NOTIFICATION_PERSON_API_TEST_PLAN_V1_4
)
public class ClaimNotificationPersonApiTestPlanV1n4 implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
            new ModuleListEntry(
                List.of(
                    OpinPreFlightCheckModule.class,
                    OpinClaimNotificationPersonApiTestModuleV1n4.class,
                    OpinClaimNotificationApiPersonWrongDocumentTypeTestModuleV1n4.class,
                    OpinClaimNotificationApiPersonInvalidDocumentTypeTestModuleV1n4.class,
                    OpinClaimNotificationApiPersonInvalidPolicyNumberTestV1n4.class,
                    OpinClaimNotificationApiPersonInvalidOccurrenceDateTestModuleV1n4.class,
                    OpinClaimNotificationApiPersonWrongIdFieldTestModuleV1n4.class,
                    OpinClaimNotificationApiPersonMultipleClaimNotificationsTestModuleV1n4.class,
                    OpinClaimNotificationPersonApiWrongPolicyIDTestModuleV1n4.class,
                    OpinClaimNotificationApiPersonIdempotencyTestModuleV1n4.class
                ),
                    List.of(
                            new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                            new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                            new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                            new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
            )
        );
    }
}
