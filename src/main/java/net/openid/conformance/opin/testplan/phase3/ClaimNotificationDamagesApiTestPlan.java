package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.claimNotification.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
    testPlanName = "Claim Notification damages api test",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
    displayName = PlanNames.CLAIM_NOTIFICATION_DAMAGES_API_TEST_PLAN,
    summary = PlanNames.CLAIM_NOTIFICATION_DAMAGES_API_TEST_PLAN
)
public class ClaimNotificationDamagesApiTestPlan implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
            new ModuleListEntry(
                List.of(
                    OpinPreFlightCheckModule.class,
                    OpinClaimNotificationDamagesApiTestModule.class,
                    OpinClaimNotificationApiDamagesWrongDocumentTypeTestModule.class,
                    OpinClaimNotificationApiDamagesInvalidDocumentTypeTestModule.class,
                    OpinClaimNotificationApiDamagesInvalidPolicyNumberTest.class,
                    OpinClaimNotificationApiDamagesInvalidOccurrenceDateTestModule.class,
                    OpinClaimNotificationApiDamagesWrongIdFieldTestModule.class,
                    OpinClaimNotificationApiDamagesMultipleClaimNotificationsTestModule.class,
                    OpinClaimNotificationDamagesApiWrongPolicyIDTestModule.class,
                    OpinClaimNotificationApiDamagesIdempotencyTestModule.class
                ),
                    List.of(
                            new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                            new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                            new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                            new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
            )
        );
    }
}
