package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.responsibility.v1n4.OpinResponsibilityApiApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.responsibility.v1n4.OpinResponsibilityGranularPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.responsibility.v1n4.OpinResponsibilityResourcesApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.responsibility.v1n4.OpinResponsibilityWrongPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.*;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance responsibility api test-v1n4",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.RESPONSIBILITY_API_PHASE_2_TEST_PLAN_V1_4,
	summary = PlanNames.RESPONSIBILITY_API_PHASE_2_TEST_PLAN_V1_4
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
		"vp_fapi_response_mode.plain_response"
})

public class ResponsibilityApiTestPlanV1n4 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinResponsibilityApiApiTestModuleV1n4.class,
					OpinResponsibilityResourcesApiTestModuleV1n4.class,
					OpinResponsibilityWrongPermissionsTestModuleV1n4.class,
					OpinResponsibilityGranularPermissionsTestModuleV1n4.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
