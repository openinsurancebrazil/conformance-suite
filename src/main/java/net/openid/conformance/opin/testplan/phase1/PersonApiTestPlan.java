package net.openid.conformance.opin.testplan.phase1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase1.person.PersonApiTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "products-and-services-person_test-plan_v1-5",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1,
	displayName = PlanNames.PRODUCTS_N_SERVICES_PERSON_API_TEST_PLAN,
	summary = PlanNames.PRODUCTS_N_SERVICES_PERSON_API_TEST_PLAN
)
public class PersonApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PersonApiTestModule.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
