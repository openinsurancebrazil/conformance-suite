package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.transport.v1n3.OpinTransportApiTestModuleV1n3;
import net.openid.conformance.opin.testmodule.v1.transport.v1n3.OpinTransportGranularPermissionsTestModuleV1n3;
import net.openid.conformance.opin.testmodule.v1.transport.v1n3.OpinTransportResourcesApiTestModuleV1n3;
import net.openid.conformance.opin.testmodule.v1.transport.v1n3.OpinTransportWrongPermissionsTestModuleV1n3;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance transport api test-v1n3",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.TRANSPORT_API_TEST_PLAN_V1_3,
	summary = PlanNames.TRANSPORT_API_TEST_PLAN_V1_3
)

public class TransportApiTestPlanV1n3 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinTransportApiTestModuleV1n3.class,
					OpinTransportWrongPermissionsTestModuleV1n3.class,
					OpinTransportResourcesApiTestModuleV1n3.class,
					OpinTransportGranularPermissionsTestModuleV1n3.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
