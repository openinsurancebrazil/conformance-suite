package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteAuto.v1n9.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "quote-auto_test-plan-v1.9",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        displayName = PlanNames.QUOTE_AUTO_API_TEST_PLAN_V1_9_0,
        summary = PlanNames.QUOTE_AUTO_API_TEST_PLAN_V1_9_0
)
public class QuoteAutoApiTestPlanV1n9 implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinQuoteAutoLeadApiCoreTestModuleV1n9.class,
                                OpinQuoteAutoApiCoreAcknTestModuleV1n9.class,
                                OpinQuoteAutoApiCoreCancTestModuleV1n9.class,
                                OpinQuoteAutoApiCoreRejectedTestModuleV1n9.class,
                                OpinQuoteAutoApiConditionalDynamicFieldsTestModuleV1n9.class,
                                OpinQuoteApiNegativeConsentTestModuleV1n9.class,
                                OpinQuoteAutoApiNegativeQuoteTestModuleV1n9.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
