package net.openid.conformance.opin.testplan.utils;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallResource;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpHeaders;

public class CallNoCacheResource extends CallResource {

	@Override
	protected HttpHeaders getHeaders(Environment env) {
		HttpHeaders headers = super.getHeaders(env);
		headers.set("cache-control", "no-cache");
		return headers;
	}

	@Override
	protected Environment handleClientResponse(Environment env, JsonObject responseCode, String responseBody, JsonObject responseHeaders, JsonObject fullResponse) {
		env.putObject("resource_endpoint_response_headers", responseHeaders);
		env.putString("resource_endpoint_response", responseBody);
		env.putObject("resource_endpoint_response_full", fullResponse);
		this.logSuccess("Got a response from the resource endpoint: ", fullResponse);
		return env;
	}
}
