package net.openid.conformance.opin.testplan.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.*;
import net.openid.conformance.opin.testmodule.v1.consents.v2n6.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance consents api test V2.6.0",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.CONSENTS_API_TEST_PLAN_v2_6,
	summary = PlanNames.CONSENTS_API_TEST_PLAN_v2_6
)

public class ConsentsApiTestPlanV2n6 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
						OpinPreFlightCheckModule.class,
						OpinConsentsApiConsentStatusTestModuleV2n6.class,
						OpinConsentApiNegativeTestsV2n6.class,
						OpinConsentsApiPermissionGroupsTestModuleV2n6.class,
						OpinConsentsApiCrossClientTestModuleV2n6.class,
						OpinConsentsApiConsentStatusIfDeclinedTestModuleV2n6.class,
						OpinConsentsApiConsentExpiredTestModuleV2n6.class,
						OpinConsentsApiDeleteTestModuleV2n6.class,
						OpinConsentInvalidUserV2n6.class,
						OpinConsentsApiGranularPermissionTestModuleV2n6.class,
						OpinClaimConsentApiIdempotencyTestV2n6.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
