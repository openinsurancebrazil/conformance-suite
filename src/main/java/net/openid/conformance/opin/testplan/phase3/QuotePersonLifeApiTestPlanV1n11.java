package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePersonLife.v1n11.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "quote-person-life_test-plan-v1.11.0",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        displayName = PlanNames.QUOTE_PERSON_LIFE_API_TEST_PLAN_V1_11,
        summary = PlanNames.QUOTE_PERSON_LIFE_API_TEST_PLAN_V1_11
)
public class QuotePersonLifeApiTestPlanV1n11 implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinQuotePersonLifeLeadTestModuleV1n11.class,
                                OpinQuotePersonLifeCoreAcknTestModuleV1n11.class,
                                OpinQuotePersonLifeCoreCancTestModuleV1n11.class,
                                OpinQuotePersonLifeRjctTestModuleV1n11.class,
                                OpinQuotePersonLifeDynamicFieldsTestModuleV1n11.class,
                                OpinQuotePersonLifeNegativeConsentTestModuleV1n11.class,
                                OpinQuotePersonLifeNegativeTestModuleV1n11.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}