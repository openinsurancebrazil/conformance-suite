package net.openid.conformance.opin.testplan.fvp;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.fvp.*;
import net.openid.conformance.opin.testmodule.fvp.OpinFvpFinancialRiskApiTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;

import java.util.List;

@PublishTestPlan(
        testPlanName = "fvp-customer-data_test-plan",
        profile = OBBProfile.OPIN_PROFILE_PROD_FVP,
        displayName = PlanNames.FVP_CUSTOMER_DATA_TEST_PLAN,
        summary = PlanNames.FVP_CUSTOMER_DATA_TEST_PLAN
)
public class FVPTestPlan implements TestPlan{

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinFAPI1AdvancedFinalBrazilDCRHappyFlowVariantNoAuthFlow.class,
                                OpinFvpPreFlightCheckModule.class,
                                OpinFvpResourcesApiTestModule.class,
                                OpinFvpCustomerBusinessApiTestModule.class,
                                OpinFvpCustomerPersonalApiTestModule.class,
                                OpinFvpFinancialRiskApiTestModule.class,
                                OpinFvpPensionPlanApiTestModule.class,
                                OpinFvpPersonApiTestModule.class,
                                OpinFvpLifePensionApiTestModule.class,
                                OpinFvpFinancialAssistanceApiTestModule.class,
                                OpinFvpCapitalizationTitleApiTestModule.class,
                                OpinFvpTransportApiTestModule.class,
                                OpinFvpAcceptanceAndBranchesAbroadApiTestModule.class,
                                OpinFvpAutoApiTestModule.class,
                                OpinFvpHousingApiTestModule.class,
                                OpinFvpPatrimonialApiTestModule.class,
                                OpinFvpResponsibilityApiTestModule.class,
                                OpinFvpRuralApiTestModule.class,
                                OpinFvpConsentsApiTestModule.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"))));
    }

}
