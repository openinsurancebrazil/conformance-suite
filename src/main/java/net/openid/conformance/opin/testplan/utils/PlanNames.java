package net.openid.conformance.opin.testplan.utils;

public class PlanNames {
	/** VERSION 1 **/
	/* Phase 1 - Open Data */
	public static final String PRODUCTS_N_SERVICES_HOUSING_API_TEST_PLAN  = "Structural tests for ProductsNServices - Housing API - based on Swagger version: 1.3.0"; // OPIN Rural
	public static final String PRODUCTS_N_SERVICES_RURAL_API_TEST_PLAN = "Structural tests for ProductsNServices - Rural API - based on Swagger version: 1.3.0"; // OPIN Rural
	public static final String PRODUCTS_N_SERVICES_TRANSPORT_API_TEST_PLAN  = "Structural tests for ProductsNServices - Transport API - based on Swagger version: 1.3.0"; // OPIN Transport
	public static final String PRODUCTS_N_SERVICES_PERSON_API_TEST_PLAN  = "Structural tests for ProductsNServices - Person API - based on Swagger version: 1.5.1"; // OPIN Person
	public static final String PERSON_PENSION_PLAN_API_TEST_PLAN = "Structural tests for ProductsNServices - Pension Plan API - based on Swagger version: 1.3.0"; // OPIN Pension Plan
	public static final String AUTO_INSURANCE_PLAN_API_TEST_PLAN = "Structural tests for ProductsNServices - Auto Insurance API - based on Swagger version: 1.5.0"; // OPIN Auto Insurance
	public static final String HOME_INSURANCE_PLAN_API_TEST_PLAN = "Structural tests for ProductsNServices - Home Insurance API - based on Swagger version: 1.4.0"; // OPIN Home Insurance
	public static final String CAPITALIZATION_TITLE_PLAN_API_TEST_PLAN = "Structural tests for ProductsNServices - Capitalization Title API - based on Swagger version: 1.3.0"; // OPIN Capitalization Title
	public static final String LIFE_PENSION_PLAN_API_TEST_PLAN = "Structural tests for ProductsNServices - Life Pension API - based on Swagger version: 1.5.0"; // OPIN Life Pension
	public static final String OPIN_CHANNELS_BRANCHES_API_TEST_PLAN = "Structural tests for Channels - Branches API - based on Swagger version: 1.5.0"; // OPIN Channel Branches
	public static final String OPIN_ELECTRONIC_CHANNELS_API_TEST_PLAN = "Structural tests for Channels - Electronic Channels API - based on Swagger version: 1.5.0"; // OPIN Electronic Channel
	public static final String OPIN_PHONE_CHANNELS_API_TEST_PLAN = "Structural tests for Channels - Phone Channels API - based on Swagger version: 1.5.0"; // OPIN Phone Channels
	public static final String OPIN_ADMIN_API_TEST_PLAN = "Structural tests for Admin API - based on Swagger version: 1.3.0"; // OPIN Admin
	public static final String OPIN_DISCOVERY_STATUS_TEST_PLAN = "Structural tests for Discovery Status API - based on Swagger version: 1.1.0"; // OPIN Discovery
	public static final String OPIN_DISCOVERY_OUTAGES_TEST_PLAN = "Structural tests for Discovery  Outages API - based on Swagger version: 1.1.0"; // OPIN Discovery
	public static final String BUSINESS_API_TEST_PLAN = "Structural tests for ProductsNServices - Business API - based on Swagger version: 1.3.0";
	public static final String AUTO_EXTENDED_WARRANTY_API_TEST_PLAN = "Structural tests for ProductsNServices - Auto Extended Warranty API - based on Swagger version: 1.3.0";
	public static final String ASSISTANCE_GENERAL_ASSETS_API_TEST_PLAN = "Structural tests for ProductsNServices - Assistance General Assets API - based on Swagger version: 1.3.0";
	public static final String GENERAL_LIABILITY_API_TEST_PLAN = "Structural tests for ProductsNServices - General Liability API - based on Swagger version: 1.3.0";
	public static final String EQUIPMENT_BREAKDOWN_API_TEST_PLAN = "Structural tests for ProductsNServices - Equipment Breakdown API - based on Swagger version: 1.3.0";
	public static final String LOST_PROFIT_API_TEST_PLAN = "Structural tests for ProductsNServices - Lost Profit API - based on Swagger version: 1.3.0";
	public static final String ENGINEERING_API_TEST_PLAN = "Structural tests for ProductsNServices - Engineering API - based on Swagger version: 1.3.0";
	public static final String PRIVATE_GUARANTEE_API_TEST_PLAN = "Structural tests for ProductsNServices - Private Guarantee API - based on Swagger version: 1.3.0";
	public static final String DOMESTIC_CREDIT_API_TEST_PLAN = "Structural tests for ProductsNServices - Domestic Credit API - based on Swagger version: 1.3.0";
	public static final String EXTENDED_WARRANTY_API_TEST_PLAN = "Structural tests for ProductsNServices - Extended Warranty API - based on Swagger version: 1.3.0";
	public static final String CYBER_RISK_API_TEST_PLAN = "Structural tests for ProductsNServices - Cyber Risk Plan API - based on Swagger version: 1.3.0";
	public static final String EXPORT_CREDIT_API_TEST_PLAN = "Structural tests for ProductsNServices - Export Credit API - based on Swagger version: 1.3.0";
	public static final String PUBLIC_GUARANTEE_API_TEST_PLAN = "Structural tests for ProductsNServices - Public Guarantee API - based on Swagger version: 1.3.0";
	public static final String NAMED_OPERATIONAL_RISKS_API_TEST_PLAN = "Structural tests for ProductsNServices - Named Operational Risks API - based on Swagger version: 1.1.0";
	public static final String FINANCIAL_RISK_API_TEST_PLAN = "Structural tests for ProductsNServices - Financial Risk API - based on Swagger version: 1.3.0";
	public static final String GLOBAL_BANKING_API_TEST_PLAN = "Structural tests for ProductsNServices - Global Banking API - based on Swagger version: 1.1.0";
	public static final String STOP_LOSS_API_TEST_PLAN = "Structural tests for ProductsNServices - Stop Loss API - based on Swagger version: 1.3.0";
	public static final String RENT_GUARANTEE_API_TEST_PLAN = "Structural tests for ProductsNServices - Rent Guarantee API - based on Swagger version: 1.3.0";
	public static final String ENVIRONMENTAL_LIABILITY_API_TEST_PLAN = "Structural tests for ProductsNServices - Environmental Liability API - based on Swagger version: 1.3.0";
	public static final String CONDOMINIUM_API_TEST_PLAN =  "Structural tests for ProductsNServices - Condominium API - based on Swagger version: 1.3.0";
	public static final String ERRORS_OMISSIONS_LIABILITY_API_TEST_PLAN = "Structural tests for ProductsNServices - Errors Omissions Liability API - based on Swagger version: 1.3.0";
	public static final String DIRECTORS_OFFICERS_LIABILITY_API_TEST_PLAN = "Structural tests for ProductsNServices - Directors Officers Liability API - based on Swagger version: 1.3.0";
	public static final String OTHERS_SCOPES_API_TEST_PLAN = "Structural tests for ProductsNServices - Others Scopes API - based on Swagger version: 1.0.0";

	public static final String INTERMEDIARY_API_TEST_PLAN = "Structural tests for Channels - Intermediary API - based on Swagger version: 1.4.0";
	public static final String REFERENCED_NETWORK_API_TEST_PLAN = "Structural tests for Channels - Referenced Network API - based on Swagger version: 1.4.0";


	/* Phase 2 - Customer Data */
	public static final String CONSENTS_API_TEST_PLAN = "Functional Tests for Consents API - based on Swagger Version 2.5.0";
	public static final String CONSENTS_API_TEST_PLAN_v2_6 = "Functional Tests for Consents API - based on Swagger Version 2.6.0";
	public static final String CONSENTS_API_TEST_PLAN_v2_7 = "Functional Tests for Consents API - based on Swagger Version 2.7.0";
	public static final String RESOURCES_API_TEST_PLAN = "Functional Tests for Resources API - based on Swagger Version 2.4.0";
	public static final String RESOURCES_API_TEST_PLAN_V2_5 = "Functional Tests for Resources API - based on Swagger Version 2.5.0";
	public static final String CUSTOMERS_BUSINESS_API_TEST_PLAN = "Functional Tests for Customer Business API - based on swagger Version 1.5.0";
	public static final String CUSTOMERS_BUSINESS_API_TEST_PLAN_V1_6_0 = "Functional Tests for Customer Business API - based on swagger Version 1.6.0";
	public static final String CUSTOMERS_PERSONAL_API_TEST_PLAN = "Functional Tests for Customer Personal API - based on swagger Version 1.5.0";
	public static final String CUSTOMERS_PERSONAL_API_TEST_PLAN_V1_6_0 = "Functional Tests for Customer Personal API - based on swagger Version 1.6.0";
	public static final String PATRIMONIAL_API_TEST_PLAN = "Functional Tests for Patrimonial API - based on swagger Version 1.4.0";
	public static final String PATRIMONIAL_API_TEST_PLAN_v1_5 = "Functional Tests for Patrimonial API - based on swagger Version 1.5.0";

	public static final String RESPONSIBILITY_API_PHASE_2_TEST_PLAN = "Functional Tests for Responsibility API - based on Swagger Version 1.3.0";
	public static final String RESPONSIBILITY_API_PHASE_2_TEST_PLAN_V1_4 = "Functional Tests for Responsibility API - based on Swagger Version 1.4.0";
	public static final String FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN = "Functional Tests for Financial Risks API - based on Swagger Version 1.3.1";
	public static final String FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN_V1_4 = "Functional Tests for Financial Risks API - based on Swagger Version 1.4.0";
	public static final String ACCEPTANCE_AND_BRANCHES_ABROAD_PHASE_2_TEST_PLAN = "Functional Tests for Acceptance and Branches Abroad API - based on Swagger Version 1.3.0";
	public static final String ACCEPTANCE_AND_BRANCHES_ABROAD_PHASE_2_TEST_PLAN_v1_4 = "Functional Tests for Acceptance and Branches Abroad API - based on Swagger Version 1.4.0";

	public static final String RURAL_API_TEST_PLAN = "Functional tests for Rural API - based on Swagger version: 1.3.0";
	public static final String RURAL_API_TEST_PLAN_V1_4 = "Functional tests for Rural API - based on Swagger version: 1.4.0";
	public static final String AUTO_API_TEST_PLAN = "Functional Tests for Auto API - based on swagger Version 1.3.0";
	public static final String AUTO_API_TEST_PLAN_v1_4 = "Functional Tests for Auto API - based on swagger Version 1.4.0";
	public static final String TRANSPORT_API_TEST_PLAN = "Functional Tests for Transport API - based on Swagger Version 1.2.0";
	public static final String TRANSPORT_API_TEST_PLAN_V1_3 = "Functional Tests for Transport API - based on Swagger Version 1.3.0";

	public static final String HOUSING_API_TEST_PLAN = "Functional tests for Housing API - based on Swagger version: 1.3.0";
	public static final String HOUSING_API_TEST_PLAN_V1_4 = "Functional tests for Housing API - based on Swagger version: 1.4.0";

	public static final String STRUCTURAL_TEST_PLAN = "Open Insurance Brasil Structural Tests for Phase 2 - Version 1";
	public static final String STRUCTURAL_TEST_PLAN_PHASE3 = "Open Insurance Brasil Structural Tests for Phase 3 - Version 1 (WIP)";
	public static final String OPIN_DCR_TEST_PLAN = "Opin Brazil DCR Test";
	public static final String PENSION_PLAN_API_TEST_PLAN = "Functional tests for Pension Plan API - based on Swagger version: 1.4.0";
	public static final String PENSION_PLAN_API_TEST_PLAN_V1_5 = "Functional tests for Pension Plan API - based on Swagger version: 1.5.0";
	public static final String PERSON_API_TEST_PLAN = "Functional Tests for Person API - based on Swagger Version 1.5.0";
	public static final String PERSON_API_TEST_PLAN_V1_6 = "Functional Tests for Person API - based on Swagger Version 1.6.0";
	public static final String CAPITALIZATION_TITLE_API_TEST_PLAN = "Functional Tests for Capitalization Title API - based on Swagger Version 1.4.0";
	public static final String CAPITALIZATION_TITLE_API_TEST_PLAN_v1_5 = "Functional Tests for Capitalization Title API - based on Swagger Version 1.5.0";
	public static final String LIFE_PENSION_PLAN_API_TEST_PLAN_PHASE2 = "Functional Tests for Life Pension API - based on Swagger version: 1.4.0";
	public static final String LIFE_PENSION_PLAN_API_TEST_PLAN_PHASE2_V1_5 = "Functional Tests for Life Pension API - based on Swagger version: 1.5.0";
	public static final String FINANCIAL_ASSISTANCE_API_TEST_PLAN = "Functional Tests for Financial Assistance API - based on Swagger Version 1.2.0";
	public static final String FINANCIAL_ASSISTANCE_API_TEST_PLAN_V1_3_0 = "Functional Tests for Financial Assistance API - based on Swagger Version 1.3.0";

	/* Phase 3 - Services */
	public static final String ENDORSEMENT_API_TEST_PLAN = "Functional test for Endorsements API - based on Swagger version: 1.2.0";
	public static final String ENDORSEMENT_API_TEST_PLAN_V1_3 = "Functional test for Endorsements API - based on Swagger version: 1.3.0";
	public static final String ENDORSEMENT_HOUSING_API_TEST_PLAN = "Functional test for Endorsements Housing API - based on Swagger version: 1.2.0";
	public static final String ENDORSEMENT_HOUSING_API_TEST_PLAN_V1_3 = "Functional test for Endorsements Housing API - based on Swagger version: 1.3.0";
	public static final String CLAIM_NOTIFICATION_DAMAGES_API_TEST_PLAN = "Functional test for Claim Notification Damages API - based on Swagger version: 1.3.0";
	public static final String CLAIM_NOTIFICATION_PERSON_API_TEST_PLAN = "Functional test for Claim Notification Person API - based on Swagger version: 1.3.0";
	public static final String CLAIM_NOTIFICATION_DAMAGES_API_TEST_PLAN_V1_4 = "Functional test for Claim Notification Damages API - based on Swagger version: 1.4.0";
	public static final String CLAIM_NOTIFICATION_PERSON_API_TEST_PLAN_V1_4 = "Functional test for Claim Notification Person API - based on Swagger version: 1.4.0";
    public static final String QUOTE_PATRIMONIAL_HOME_API_TEST_PLAN = "Functional test for Quote Patrimonial Home API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_HOME_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Home API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_HOME_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Patrimonial Home Webhook API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_HOME_WEBHOOK_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Home Webhook API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_BUSINESS_API_TEST_PLAN = "Functional test for Quote Patrimonial Business API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_BUSINESS_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Business API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_BUSINESS_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Patrimonial Business Webhook API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_BUSINESS_WEBHOOK_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Business Webhook API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_DIVERSE_RISKS_API_TEST_PLAN = "Functional test for Quote Patrimonial Diverse Risks API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_DIVERSE_RISKS_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Diverse Risks API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_DIVERSE_RISKS_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Patrimonial Diverse Risks Webhook API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_DIVERSE_RISKS_WEBHOOK_API_TEST_PLAN_V1n10 = "Functional test for Quote Patrimonial Diverse Risks Webhook API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PERSON_LIFE_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Person Life Webhook API - based on Swagger version: 1.10.2";
	public static final String QUOTE_PERSON_LIFE_WEBHOOK_API_TEST_PLAN_V1_11 = "Functional test for Quote Person Life Webhook API - based on Swagger version: 1.11.0";
	public static final String QUOTE_CONTRACT_LIFE_PENSION_WEBHOOK_API_TEST_PLAN = "Functional test for Contract Life Pension Webhook API - based on Swagger version: 1.12.1";
	public static final String QUOTE_CONTRACT_LIFE_PENSION_WEBHOOK_API_TEST_PLAN_V1_13 = "Functional test for Contract Life Pension Webhook API - based on Swagger version: 1.13.0";
	public static final String QUOTE_PERSON_TRAVEL_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Person Travel Webhook API - based on Swagger version: 1.10.2";
	public static final String QUOTE_PERSON_TRAVEL_WEBHOOK_API_TEST_PLAN_V1_11 = "Functional test for Quote Person Travel Webhook API - based on Swagger version: 1.11.0";
	public static final String QUOTE_PATRIMONIAL_LOST_PROFIT_API_TEST_PLAN = "Functional test for Quote Patrimonial Lost Profit API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_LOST_PROFIT_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Lost Profit API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_ENGINEERING_API_TEST_PLAN = "Functional test for Quote Patrimonial Engineering API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_ENGINEERING_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Engineering API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_ASSISTANCE_GENERAL_ASSETS_API_TEST_PLAN = "Functional test for Quote Patrimonial Assistance General Assets API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_ASSISTANCE_GENERAL_ASSETS_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Assistance General Assets API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_EXTENDED_WARRANTY_API_TEST_PLAN = "Functional test for Quote Patrimonial Extended Warranty API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_EXTENDED_WARRANTY_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Extended Warranty API - based on Swagger version: 1.10.0";

	public static final String QUOTE_PATRIMONIAL_CONDOMINIUM_API_TEST_PLAN = "Functional test for Quote Patrimonial Condominium API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_CONDOMINIUM_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Condominium API - based on Swagger version: 1.10.0";
	public static final String QUOTE_PATRIMONIAL_CONDOMINIUM_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Patrimonial Condominium API Webhook - based on Swagger version: 1.9.0";
	public static final String QUOTE_PATRIMONIAL_CONDOMINIUM_WEBHOOK_API_TEST_PLAN_V1_10 = "Functional test for Quote Patrimonial Condominium API Webhook - based on Swagger version: 1.10.0";

	public static final String QUOTE_RESPONSIBILITY_API_TEST_PLAN_V1_8_0 = "Functional test for Quote Responsibility API - based on Swagger version: 1.8.0";
	public static final String QUOTE_RESPONSIBILITY_API_TEST_PLAN_V1_9_0 = "Functional test for Quote Responsibility API - based on Swagger version: 1.9.0";
	public static final String QUOTE_RURAL_API_TEST_PLAN = "Functional test for Quote Rural API - based on Swagger version: 1.8.0";
	public static final String QUOTE_RURAL_API_TEST_PLAN_V1_9 = "Functional test for Quote Rural API - based on Swagger version: 1.9.0";
	public static final String QUOTE_TRANSPORT_API_TEST_PLAN = "Functional test for Quote Transport API - based on Swagger version: 1.8.0";
	public static final String QUOTE_TRANSPORT_API_TEST_PLAN_V1_9 = "Functional test for Quote Transport API - based on Swagger version: 1.9.0";
	public static final String QUOTE_AUTO_API_TEST_PLAN = "Functional test for Quote Auto API - based on Swagger version: 1.8.0";
	public static final String QUOTE_AUTO_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Auto API Webhook - based on Swagger version: 1.8.0";
	public static final String QUOTE_AUTO_API_TEST_PLAN_V1_9_0 = "Functional test for Quote Auto API - based on Swagger version: 1.9.0";
	public static final String QUOTE_AUTO_WEBHOOK_API_TEST_PLAN_V1_9_0 = "Functional test for Quote Auto API Webhook - based on Swagger version: 1.9.0";
	public static final String QUOTE_FINANCIAL_RISK_API_TEST_PLAN_V1_8_0 = "Functional test for Quote Financial Risk API - based on Swagger version: 1.8.0";
	public static final String QUOTE_FINANCIAL_RISK_API_TEST_PLAN_V1_9_0 = "Functional test for Quote Financial Risk API - based on Swagger version: 1.9.0";
	public static final String QUOTE_HOUSING_API_TEST_PLAN = "Functional test for Quote Housing API - based on Swagger version: 1.8.0";
	public static final String QUOTE_HOUSING_API_TEST_PLAN_V1_9_0 = "Functional test for Quote Housing API - based on Swagger version: 1.9.0";
	public static final String QUOTE_ACCEPTANCE_AND_BRANCHES_ABROAD_API_TEST_PLAN_V1_8_0 = "Functional test for Quote Acceptance and Branches Abroad API - based on Swagger version: 1.8.0";
	public static final String QUOTE_ACCEPTANCE_AND_BRANCHES_ABROAD_API_TEST_PLAN_V1_9_0 = "Functional test for Quote Acceptance and Branches Abroad API - based on Swagger version: 1.9.0";
	public static final String QUOTE_PERSON_LIFE_API_TEST_PLAN = "Functional test for Quote Person Life API - based on Swagger version: 1.10.2";
	public static final String QUOTE_PERSON_TRAVEL_API_TEST_PLAN = "Functional test for Quote Person Travel API - based on Swagger version: 1.10.2";
	public static final String QUOTE_PERSON_LIFE_API_TEST_PLAN_V1_11 = "Functional test for Quote Person Life API - based on Swagger version: 1.11.0";
	public static final String QUOTE_PERSON_TRAVEL_API_TEST_PLAN_V1_11 = "Functional test for Quote Person Travel API - based on Swagger version: 1.11.0";
	public static final String CONTRACT_LIFE_PENSION_API_TEST_PLAN = "Functional test for Contract Life Pension API - based on Swagger version: 1.12.1";
	public static final String CONTRACT_LIFE_PENSION_API_TEST_PLAN_V1_13 = "Functional test for Contract Life Pension API - based on Swagger version: 1.13.0";
	public static final String QUOTE_CAPITALIZATION_TITLE_API_TEST_PLAN = "Functional test for Quote Capitalization Title API - based on Swagger version: 1.9.4";
	public static final String QUOTE_CAPITALIZATION_TITLE_WEBHOOK_API_TEST_PLAN = "Functional test for Quote Capitalization Title Webhook API - based on Swagger version: 1.9.4";
	public static final String QUOTE_CAPITALIZATION_TITLE_API_TEST_PLAN_V1_10 = "Functional test for Quote Capitalization Title API - based on Swagger version: 1.10.0";
	public static final String QUOTE_CAPITALIZATION_TITLE_WEBHOOK_API_TEST_PLAN_V1_10 = "Functional test for Quote Capitalization Title Webhook API - based on Swagger version: 1.10.0";

	public static final String CAPITALIZATION_TITLE_WITHDRAW_API_TEST_PLAN = "Functional test for Capitalization Title Withdraw API - based on Swagger version: 1.3.0";
	public static final String PENSION_WITHDRAW_API_TEST_PLAN = "Functional test for Pension Withdraw API - based on Swagger version: 1.3.0";
	public static final String PENSION_WITHDRAW_LEAD_API_TEST_PLAN = "Functional test for Pension Withdraw Lead API - based on Swagger version: 1.3.0";


	public static final String QUOTE_CAPITALIZATION_TITLE_RAFFLE_API_TEST_PLAN = "Functional test for Quote Capitalization Title Raffle API - based on Swagger version: 1.9.4";
	public static final String QUOTE_CAPITALIZATION_TITLE_RAFFLE_API_TEST_PLAN_V1_10 = "Functional test for Quote Capitalization Title Raffle API - based on Swagger version: 1.10.0";


	/** Dynamic Version**/

	public static final String STRUCTURAL_CAPITALIZATION_TITLE_API_TEST_PLAN_v1_4 = "Structural tests for ProductsNServices - Capitalization Title API - based on Swagger version: 1.4.0";
	public static final String STRUCTURAL_PENSION_PLAN_API_TEST_PLAN_v1_4 = "Structural tests for ProductsNServices - Pension Plan API - based on Swagger version: 1.4.0";
	public static final String STRUCTURAL_PERSON_API_TEST_PLAN_v1_6 = "Structural tests for ProductsNServices - Person API - based on Swagger version: 1.6.0";

	/* PCM test plans */

	public static final String PCM_METRICS_TEST_PLAN = "Functional test for PCM - Metrics API - based on Swagger version: 1.0.10";
	public static final String PCM_CONSENT_FUNNEL_TEST_PLAN = "Functional test for PCM - Consent Funnel API - based on Swagger version: 1.0.4";

	/** FVP Test Plans **/

	public static final String FVP_CUSTOMER_DATA_TEST_PLAN = "Production Functional Tests for Consents and Resources API";
	public static final String FVP_QUOTES_API_TEST_PLAN = "Production Functional Tests for Quotes API";

}
