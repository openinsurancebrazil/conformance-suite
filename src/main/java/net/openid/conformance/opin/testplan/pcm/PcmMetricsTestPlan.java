package net.openid.conformance.opin.testplan.pcm;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.pcm.metrics.PcmMetricsApiClientBatchTestModule;
import net.openid.conformance.opin.testmodule.pcm.metrics.PcmMetricsApiClientEventTestModule;
import net.openid.conformance.opin.testmodule.pcm.metrics.PcmMetricsApiServerBatchTestModule;
import net.openid.conformance.opin.testmodule.pcm.metrics.PcmMetricsApiServerEventTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
        testPlanName = "pcm-metrics_test-plan",
        profile = OBBProfile.OBB_PROFILE_OPIN_DCR,
        displayName = PlanNames.PCM_METRICS_TEST_PLAN,
        summary = PlanNames.PCM_METRICS_TEST_PLAN
)
public class PcmMetricsTestPlan implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                PcmMetricsApiServerEventTestModule.class,
                                PcmMetricsApiServerBatchTestModule.class,
                                PcmMetricsApiClientEventTestModule.class,
                                PcmMetricsApiClientBatchTestModule.class
                        ),
                        List.of(new Variant(ClientAuthType.class, "none"))
                )
        );
    }

}
