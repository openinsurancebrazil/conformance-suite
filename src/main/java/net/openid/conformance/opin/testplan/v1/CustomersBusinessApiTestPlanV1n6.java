package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.customers.v1n6.OpinCustomerBusinessApiGranularPermissionsTestModuleV1n6;
import net.openid.conformance.opin.testmodule.v1.customers.v1n6.OpinCustomerBusinessDataApiTestModuleV1n6;
import net.openid.conformance.opin.testmodule.v1.customers.v1n6.OpinCustomerBusinessWrongPermissionsTestModuleV1n6;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance customer business api test-v1.6.0",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.CUSTOMERS_BUSINESS_API_TEST_PLAN_V1_6_0,
	summary = PlanNames.CUSTOMERS_BUSINESS_API_TEST_PLAN_V1_6_0
)

public class CustomersBusinessApiTestPlanV1n6 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinCustomerBusinessDataApiTestModuleV1n6.class,
					OpinCustomerBusinessWrongPermissionsTestModuleV1n6.class,
					OpinCustomerBusinessApiGranularPermissionsTestModuleV1n6.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
