package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.endorsement.housing.v1n3.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "Endorsement Housing api test-v1n3",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        displayName = PlanNames.ENDORSEMENT_HOUSING_API_TEST_PLAN_V1_3,
        summary = PlanNames.ENDORSEMENT_HOUSING_API_TEST_PLAN_V1_3
)
public class EndorsementHousingApiTestPlanV1n3 implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinEndorsementHousingApiAlteracaoTestModuleV1n3.class,
                                OpinEndorsementHousingApiCancelamentoTestModuleV1n3.class,
                                OpinEndorsementHousingApiInclusaoTestModuleV1n3.class,
                                OpinEndorsementHousingApiExclusaoTestModuleV1n3.class,
                                OpinEndorsementHousingApiInvalidEndorsementTypeTestModuleV1n3.class,
                                OpinEndorsementHousingApiInvalidPolicyNumberTestModuleV1n3.class,
                                OpinEndorsementHousingApiMultipleEndorsementsTestModuleV1n3.class,
                                OpinEndorsementHousingApiWrongDateTestModuleV1n3.class,
                                OpinEndorsementHousingApiWrongPolicyIDTestModuleV1n3.class,
                                OpinEndorsementHousingWrongPermissionsTestModuleV1n3.class,
                                OpinEndorsementHousingIdempotencyTestModuleV1n3.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
