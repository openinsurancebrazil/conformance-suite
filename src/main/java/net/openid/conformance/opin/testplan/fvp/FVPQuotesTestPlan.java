package net.openid.conformance.opin.testplan.fvp;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.fvp.quote.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;

import java.util.List;

@PublishTestPlan(
        testPlanName = "fvp-quotes_test-plan",
        profile = OBBProfile.OPIN_PROFILE_PROD_FVP,
        displayName = PlanNames.FVP_QUOTES_API_TEST_PLAN,
        summary = PlanNames.FVP_QUOTES_API_TEST_PLAN
)
public class FVPQuotesTestPlan implements TestPlan{

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinFvpQuotePatrimonialHomeLeadTestModule.class,
                                OpinFvpQuotePatrimonialHomeCoreCancTestModule.class,
                                OpinFvpQuotePatrimonialHomeNegativeTestModule.class,
                                OpinFvpQuotePatrimonialBusinessLeadTestModule.class,
                                OpinFvpQuotePatrimonialBusinessCoreCancTestModule.class,
                                OpinFvpQuotePatrimonialBusinessNegativeTestModule.class,
                                OpinFvpQuotePatrimonialCondominiumLeadTestModule.class,
                                OpinFvpQuotePatrimonialCondominiumCoreCancTestModule.class,
                                OpinFvpQuotePatrimonialCondominiumNegativeTestModule.class,
                                OpinFvpQuotePatrimonialLostProfitLeadTestModule.class,
                                OpinFvpQuotePatrimonialEngineeringLeadTestModule.class,
                                OpinFvpQuotePatrimonialAssistanceGeneralAssetsLeadTestModule.class,
                                OpinFvpQuoteAcceptanceAndBranchesAbroadLeadTestModule.class,
                                OpinFvpQuoteFinancialRiskLeadTestModule.class,
                                OpinFvpQuoteResponsibilityLeadTestModule.class,
                                OpinFvpQuoteRuralLeadApiCoreTestModule.class,
                                OpinFvpQuoteAutoLeadApiCoreTestModule.class,
                                OpinFvpQuoteAutoApiCoreCancTestModule.class,
                                OpinFvpQuoteAutoApiNegativeQuoteTestModule.class,
                                OpinFvpQuoteTransportLeadApiCoreTestModule.class,
                                OpinFvpQuotePatrimonialDiverseRisksLeadTestModule.class,
                                OpinFvpQuotePatrimonialDiverseRisksCoreCancTestModule.class,
                                OpinFvpQuotePatrimonialDiverseRisksNegativeTestModule.class,
                                OpinFvpQuoteHousingLeadApiCoreTestModule.class,
                                OpinFvpQuotePatrimonialExtendedWarrantyLeadTestModule.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"))));
    }

}
