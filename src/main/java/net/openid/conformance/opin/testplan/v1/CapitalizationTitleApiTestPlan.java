package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.capitalization.CapitalizationTitleApiCoreTestModuleV1;
import net.openid.conformance.opin.testmodule.v1.capitalization.CapitalizationTitleApiGranularPermissionsTestModule;
import net.openid.conformance.opin.testmodule.v1.capitalization.CapitalizationTitleApiResourcesTestModuleV1;
import net.openid.conformance.opin.testmodule.v1.capitalization.CapitalizationTitleApiWrongPermissionsTestModuleV1;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "capitalization-title_test-plan_v1",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        displayName = PlanNames.CAPITALIZATION_TITLE_API_TEST_PLAN,
        summary = PlanNames.CAPITALIZATION_TITLE_API_TEST_PLAN
)
public class CapitalizationTitleApiTestPlan implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                CapitalizationTitleApiCoreTestModuleV1.class,
                                CapitalizationTitleApiWrongPermissionsTestModuleV1.class,
                                CapitalizationTitleApiResourcesTestModuleV1.class,
                                CapitalizationTitleApiGranularPermissionsTestModule.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
