package net.openid.conformance.opin.testplan.v1;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.financialRisk.v1n4.OpinFinancialRiskApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.financialRisk.v1n4.OpinFinancialRiskGranularPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.financialRisk.v1n4.OpinFinancialRiskResourcesApiTestModuleV1n4;
import net.openid.conformance.opin.testmodule.v1.financialRisk.v1n4.OpinFinancialRiskWrongPermissionsTestModuleV1n4;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance financial risks api test-V1n4",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN_V1_4,
	summary = PlanNames.FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN_V1_4
)

public class FinancialRisksApiTestPlanV1n4 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinFinancialRiskApiTestModuleV1n4.class,
					OpinFinancialRiskResourcesApiTestModuleV1n4.class,
					OpinFinancialRiskWrongPermissionsTestModuleV1n4.class,
					OpinFinancialRiskGranularPermissionsTestModuleV1n4.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
