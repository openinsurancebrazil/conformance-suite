package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.contractLifePension.v1n13.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "contract_life_pension_test-plan-v1.13",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        displayName = PlanNames.CONTRACT_LIFE_PENSION_API_TEST_PLAN_V1_13,
        summary = PlanNames.CONTRACT_LIFE_PENSION_API_TEST_PLAN_V1_13
)
public class ContractLifePensionApiTestPlanV1n13 implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinContractLifePensionLeadTestModuleV1n13.class,
                                OpinContractLifePensionCoreAcknTestModuleV1n13.class,
                                OpinContractLifePensionCoreCancTestModuleV1n13.class,
                                OpinContractLifePensionRjctTestModuleV1n13.class,
                                OpinContractLifePensionDynamicFieldsTestModuleV1n13.class,
                                OpinContractLifePensionNegativeConsentTestModuleV1n13.class,
                                OpinContractLifePensionNegativeTestModuleV1n13.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}