package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.PensionPlanAPICoreTestModule;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.PensionPlanApiGranularPermissionsTestModule;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.PensionPlanApiResourcesTestModuleV1;
import net.openid.conformance.opin.testmodule.v1.pensionPlan.PensionPlanApiWrongPermissionsTestModuleV1;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "pension-plan_test-plan_v1",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        displayName = PlanNames.PENSION_PLAN_API_TEST_PLAN,
        summary = PlanNames.PENSION_PLAN_API_TEST_PLAN
)
public class PensionPlanApiTestPlan implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                PensionPlanAPICoreTestModule.class,
                                PensionPlanApiWrongPermissionsTestModuleV1.class,
                                PensionPlanApiResourcesTestModuleV1.class,
                                PensionPlanApiGranularPermissionsTestModule.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
