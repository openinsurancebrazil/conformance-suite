package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.responsibility.OpinResponsibilityApiApiTestModule;
import net.openid.conformance.opin.testmodule.v1.responsibility.OpinResponsibilityGranularPermissionsTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.opin.testmodule.v1.responsibility.OpinResponsibilityResourcesApiTestModule;
import net.openid.conformance.opin.testmodule.v1.responsibility.OpinResponsibilityWrongPermissionsTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.*;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance responsibility api test",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.RESPONSIBILITY_API_PHASE_2_TEST_PLAN,
	summary = PlanNames.RESPONSIBILITY_API_PHASE_2_TEST_PLAN
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openinsurance_brazil", configurationFields = {
		"vp_fapi_response_mode.plain_response"
})

public class ResponsibilityApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinResponsibilityApiApiTestModule.class,
					OpinResponsibilityResourcesApiTestModule.class,
					OpinResponsibilityWrongPermissionsTestModule.class,
					OpinResponsibilityGranularPermissionsTestModule.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
