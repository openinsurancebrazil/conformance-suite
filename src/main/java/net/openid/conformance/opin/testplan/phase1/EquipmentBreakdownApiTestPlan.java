package net.openid.conformance.opin.testplan.phase1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase1.equipmentBreakdown.EquipmentBreakdownApiTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Open Insurance - ProductsServices - Equipment Breakdown API test plan",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1,
	displayName = PlanNames.EQUIPMENT_BREAKDOWN_API_TEST_PLAN,
	summary = "Structural and logical tests for Equipment Breakdown API"
)
public class EquipmentBreakdownApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(EquipmentBreakdownApiTestModule.class),
				List.of(new Variant(ClientAuthType.class, "none"))
			)
		);
	}

}
