package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.OpinAcceptanceAndBranchesAbroadGranularPermissionsTestModule;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.OpinAcceptanceAndBranchesAbroadApiTestModule;
import net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.OpinAcceptanceAndBranchesAbroadWrongPermissionsTestModule;
import net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad.OpinAcceptanceBranchesAbroadResourcesApiTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance acceptance and branches abroad api test",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.ACCEPTANCE_AND_BRANCHES_ABROAD_PHASE_2_TEST_PLAN,
	summary = PlanNames.ACCEPTANCE_AND_BRANCHES_ABROAD_PHASE_2_TEST_PLAN
)

public class AcceptanceAndBranchesAbroadApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinAcceptanceAndBranchesAbroadApiTestModule.class,
					OpinAcceptanceBranchesAbroadResourcesApiTestModule.class,
					OpinAcceptanceAndBranchesAbroadWrongPermissionsTestModule.class,
					OpinAcceptanceAndBranchesAbroadGranularPermissionsTestModule.class
				),
					List.of(
							new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
							new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
							new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
							new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);
	}
}
