package net.openid.conformance.opin.testplan.pcm;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.pcm.consent.funnel.PcmConsentFunnelApiClientBatchTestModule;
import net.openid.conformance.opin.testmodule.pcm.consent.funnel.PcmConsentFunnelApiClientEventTestModule;
import net.openid.conformance.opin.testmodule.pcm.consent.funnel.PcmConsentFunnelApiServerBatchTestModule;
import net.openid.conformance.opin.testmodule.pcm.consent.funnel.PcmConsentFunnelApiServerEventTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
        testPlanName = "pcm-consent-funnel_test-plan",
        profile = OBBProfile.OBB_PROFILE_OPIN_DCR,
        displayName = PlanNames.PCM_CONSENT_FUNNEL_TEST_PLAN,
        summary = PlanNames.PCM_CONSENT_FUNNEL_TEST_PLAN
)
public class PcmConsentFunnelTestPlan implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                PcmConsentFunnelApiServerEventTestModule.class,
                                PcmConsentFunnelApiServerBatchTestModule.class,
                                PcmConsentFunnelApiClientEventTestModule.class,
                                PcmConsentFunnelApiClientBatchTestModule.class
                        ),
                        List.of(new Variant(ClientAuthType.class, "none"))
                )
        );
    }
}
