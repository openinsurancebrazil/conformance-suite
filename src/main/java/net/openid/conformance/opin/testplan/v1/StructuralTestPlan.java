package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.structural.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance structural tests testplan",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.STRUCTURAL_TEST_PLAN,
	summary = PlanNames.STRUCTURAL_TEST_PLAN
)

public class StructuralTestPlan implements TestPlan{
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinConsentsStructuralTestModule.class,
					OpinResourcesStructuralTestModule.class,
					OpinCustomerPersonalStructuralTestModule.class,
					OpinCustomerBusinessStructuralTestModule.class,
					OpinPatrimonialStructuralTestModule.class,
					OpinFinancialRisksStructuralTestModule.class,
					OpinAcceptanceBranchesAbroadStructuralTestModule.class,
					OpinResponsibilitiesStructuralTestModule.class,
					OpinTransportStructuralTestModule.class,
					OpinRuralStructuralTestModule.class,
					OpinAutoStructuralTestModule.class,
					OpinHousingStructuralTestModule.class

				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
