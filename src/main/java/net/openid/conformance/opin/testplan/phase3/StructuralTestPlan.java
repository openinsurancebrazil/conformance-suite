package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.structural.phase3.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance structural tests testplan - phase 3",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
	displayName = PlanNames.STRUCTURAL_TEST_PLAN_PHASE3,
	summary = "Open Insurance Brasil Structural Tests for Phase 3 - Version 1"
)

public class StructuralTestPlan implements TestPlan{
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinClaimNotificationDamageApiStructuralTest.class,
					OpinClaimNotificationPersonApiStructuralTest.class,
					OpinEndorsementApiStructuralTestModule.class,
					OpinQuotePatrimonialApiCondominiumStructuralTestModule.class,
					OpinQuotePatrimonialApiBusinessStructuralTestModule.class,
					OpinQuotePatrimonialApiDiverseRisksStructuralTestModule.class,
					OpinQuotePatrimonialApiHomeStructuralTestModule.class,
					OpinQuotePatrimonialApiLeadStructuralTestModule.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
