package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.person.v1n6.PersonAPICoreTestModuleV1n6;
import net.openid.conformance.opin.testmodule.v1.person.v1n6.PersonApiGranularPermissionsTestModuleV1n6;
import net.openid.conformance.opin.testmodule.v1.person.v1n6.PersonApiResourcesTestModuleV1n6;
import net.openid.conformance.opin.testmodule.v1.person.v1n6.PersonApiWrongPermissionsTestModuleV1n6;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "person_test-plan_v1.6.0",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        displayName = PlanNames.PERSON_API_TEST_PLAN_V1_6,
        summary = PlanNames.PERSON_API_TEST_PLAN_V1_6
)
public class PersonApiTestPlanV1n6 implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                PersonAPICoreTestModuleV1n6.class,
                                PersonApiWrongPermissionsTestModuleV1n6.class,
                                PersonApiResourcesTestModuleV1n6.class,
                                PersonApiGranularPermissionsTestModuleV1n6.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))

                )
        );
    }
}
