package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialHome.v1n10.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "quote-patrimonial-home_test-plan-v1n10",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        displayName = PlanNames.QUOTE_PATRIMONIAL_HOME_API_TEST_PLAN_V1_10,
        summary = PlanNames.QUOTE_PATRIMONIAL_HOME_API_TEST_PLAN_V1_10
)
public class QuotePatrimonialHomeApiTestPlanV1n10 implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinQuotePatrimonialHomeLeadTestModuleV1n10.class,
                                OpinQuotePatrimonialHomeCoreAcknTestModuleV1n10.class,
                                OpinQuotePatrimonialHomeCoreCancTestModuleV1n10.class,
                                OpinQuotePatrimonialHomeRjctTestModuleV1n10.class,
                                OpinQuotePatrimonialHomeDynamicFieldsTestModuleV1n10.class,
                                OpinQuotePatrimonialHomeNegativeConsentTestModuleV1n10.class,
                                OpinQuotePatrimonialHomeNegativeTestModuleV1n10.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}