package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quotePatrimonialDiverseRisks.v1n10.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "quote-patrimonial-diverse-risks_test-plan-v1n10",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        displayName = PlanNames.QUOTE_PATRIMONIAL_DIVERSE_RISKS_API_TEST_PLAN_V1_10,
        summary = PlanNames.QUOTE_PATRIMONIAL_DIVERSE_RISKS_API_TEST_PLAN_V1_10
)
public class QuotePatrimonialDiverseRisksApiTestPlanV1n10 implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinQuotePatrimonialDiverseRisksLeadTestModuleV1n10.class,
                                OpinQuotePatrimonialDiverseRisksCoreAcknTestModuleV1n10.class,
                                OpinQuotePatrimonialDiverseRisksCoreCancTestModuleV1n10.class,
                                OpinQuotePatrimonialDiverseRisksRjctTestModuleV1n10.class,
                                OpinQuotePatrimonialDiverseRisksDynamicFieldsTestModuleV1n10.class,
                                OpinQuotePatrimonialDiverseRisksNegativeConsentTestModuleV1n10.class,
                                OpinQuotePatrimonialDiverseRisksNegativeTestModuleV1n10.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
