package net.openid.conformance.opin.testplan.phase3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase3.quoteAuto.*;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "quote-auto_test-plan",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE3,
        displayName = PlanNames.QUOTE_AUTO_API_TEST_PLAN,
        summary = PlanNames.QUOTE_AUTO_API_TEST_PLAN
)
public class QuoteAutoApiTestPlan implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new TestPlan.ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinQuoteAutoLeadApiCoreTestModuleV1.class,
                                OpinQuoteAutoApiCoreAcknTestModuleV1.class,
                                OpinQuoteAutoApiCoreCancTestModuleV1.class,
                                OpinQuoteAutoApiCoreRejectedTestModuleV1.class,
                                OpinQuoteAutoApiConditionalDynamicFieldsTestModuleV1.class,
                                OpinQuoteApiNegativeConsentTestModuleV1.class,
                                OpinQuoteAutoApiNegativeQuoteTestModuleV1.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
