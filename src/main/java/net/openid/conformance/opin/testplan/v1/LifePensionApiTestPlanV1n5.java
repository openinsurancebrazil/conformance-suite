package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.lifePension.v1n5.LifePensionAPICoreTestModuleV1n5;
import net.openid.conformance.opin.testmodule.v1.lifePension.v1n5.LifePensionApiGranularPermissionsTestModuleV1n5;
import net.openid.conformance.opin.testmodule.v1.lifePension.v1n5.LifePensionApiResourcesTestModuleV1n5;
import net.openid.conformance.opin.testmodule.v1.lifePension.v1n5.LifePensionApiWrongPermissionsTestModuleV1n5;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "life-pension_test-plan_v1n5",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        displayName = PlanNames.LIFE_PENSION_PLAN_API_TEST_PLAN_PHASE2_V1_5,
        summary = PlanNames.LIFE_PENSION_PLAN_API_TEST_PLAN_PHASE2_V1_5
)
public class LifePensionApiTestPlanV1n5 implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                LifePensionAPICoreTestModuleV1n5.class,
                                LifePensionApiWrongPermissionsTestModuleV1n5.class,
                                LifePensionApiResourcesTestModuleV1n5.class,
                                LifePensionApiGranularPermissionsTestModuleV1n5.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
                                new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
                                new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
                                new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
                )
        );
    }
}
