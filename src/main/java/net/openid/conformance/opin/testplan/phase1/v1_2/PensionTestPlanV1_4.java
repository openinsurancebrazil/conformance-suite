package net.openid.conformance.opin.testplan.phase1.v1_2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase1.pension.PensionPlanApiTestModuleV1_4;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;


@PublishTestPlan(
        testPlanName = "products-and-services-pension_test-plan_v1-4",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1,
        displayName = PlanNames.STRUCTURAL_PENSION_PLAN_API_TEST_PLAN_v1_4,
        summary = PlanNames.STRUCTURAL_PENSION_PLAN_API_TEST_PLAN_v1_4
)
public class PensionTestPlanV1_4 implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(PensionPlanApiTestModuleV1_4.class),
                        List.of(new Variant(ClientAuthType.class, "none"))
                )

        );
    }
}
