pipeline-run: build test-coverage mutation-coverage

build:
	@mvn clean package

test-coverage:
	@mvn diff-coverage:diffCoverage

mutation-coverage:
	@DIFF_PARAMS=origin/main ./scripts/run-mutation-tests.sh

mutation-coverage-full:
	@mvn pitest:mutationCoverage

