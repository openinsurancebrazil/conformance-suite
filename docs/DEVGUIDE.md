# Open Insurance Conformance Suite Developer Guide

This guide outlines how developers may work on this code to modify the Open Insurance conformance suite. It will outline how code
sharing with the Open Banking Conformance Suite is implemented, and what benefits and tradeoffs that brings.

## Why this exists

In the past, the Open Insurance Conformance Suite - hereafter referred to as OPIN CS - has been a deployment of the same codebase
as the Open Banking Brasil Conformance Suite - hereafter referred to as OBB CS - with different configuration parameters to surface 
different test plans. While this was a useful mechanism it tied two discrete development lifecycles together in ways which were undesirable. 
One project would often be impacted by bugs in another stream, the deployment of OPIN CS was tied to OBB CS work and so forth.

As OBB CS is itself a fork of another FOSS project run by the OpenID Foundation, it was considered that we could simply fork that project again, 
or fork the OBB CS, but maintaining a single fork is problematic enough, so a spike was performed to see if the OPIN-specific code could be 
pulled out into effectively a library project. 

That is this project.

## How is OBB CS code re-used?

The OBB CS code is commonly packaged as a Spring Boot fatjar, which is subsequently packaged as a docker image which then gets deployed
onto EKS clusters. The build pipeline has been modified so that the Java code is also packaged as a simple Java jar which is published to a 
Maven repositoru. This jar is available for use by the OPIN CS project, which first of all uses it as a compile-time dependency, then also 
packages itself as a Spring Boot fatjar with the OPIN code included. This then gets packaged as a docker image which we can deploy
independently of the OBB CS however we see fit.

## What are the advantages of this?

Stability of build. The OPIN CS project depends on a specific, tested release of the OBB CS code, so any breaking changes to OBB CS will 
not impact this project at all. 

Much less code to maintain. The bulk of the code lives in dependencies, so the source code of this is sparse and consists almost entirely 
of OPIN test plans, modules and their supporting code.

We do not need to wait for the often-flaky, always long-winded OBB functional tests to complete before we can deploy.

## What are the disadvantages of this?

The main tradeoff for this stability is that features introduced into the OBB codebase aren't immediately available to us. As we have
a dependency expressed through Maven co-ordinates on a specific release to master of the OBB code, we need to wait until a change is
merged to master before we can then pull that version in to our builds. 

In effect, the OBB conformance suite is a dependency of this project, in exactly the same way that JUnit is.

## What's a Maven co-ordinate?

If you look inside the dependencies section of pom.xml you will see something like this:

    <dependency>
        <groupId>net.openid</groupId>
        <artifactId>fapi-conformance-suite</artifactId>
        <version>${fapi.conformance.version}</version>
        <classifier>tests</classifier>
        <scope>test</scope>
    </dependency>

The maven co-ordinate is the combination of group ID, artifact ID, version and classifier.

## How do we bring in changes from OBB which we want?

The version co-ordinate used for this is, to keep things simple, a version number which rarely changes, followed by the short sha of the 
git revision which represents the master branch of the OBB project at any given time.
 
So in order to pull in a more recent version of the OBB CS, we simply find out the short sha of the revision we want, and update the OPIN CS pom with it. 
This is the fapi.conformance.version property.