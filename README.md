# Open Insurance Conformance Suite

The conformance suite for the Open Insurance is a standalone project which has the the 
Open Banking Brasil Conformance Suite - which can be found on: https://gitlab.com/obb1/certification/-/tree/master - as a binary dependency. 
That is to say, this project only contains source code related to Open Insurance conformance test plans. More on this integration can
 be found in docs/DEVGUIDE.md

The Swagger specifications which are used to build the tests can be found on https://gitlab.com/obb1/certification/-/tree/master/src/main/resources/swagger/openinsurance

All tests are built based on the swaggers and will be updated in case any divergence has been found

